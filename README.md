#Koreparent
Ce brique contient le socle technique des site Kosmos. Elle est nécéssaire sur tous les projets Kosmos.

##Questions, problèmes, suggestions
Les produits K-Portal et K-Sup sont Open-Source. Vous pouvez ainsi participer à leur évolution : [Guide de contribution Open-Source](http://www.ksup.org/fr/communaute/contribuez/).   
En cas d'anomalie, merci de saisir un ticket sur la plate-forme d'assistance [JIRA](https://issues.kosmos.fr/browse/CORE)

##Contribution au développement
Si vous souhaitez proposer des développements merci de consulter le [processus de contribution](http://www.ksup.org/fr/communaute/contribuez/guide-de-contribution-sur-bitbucket-20805.kjsp). 

##License
Ce projet est sous licence [Apache2](LICENSE.md). 

