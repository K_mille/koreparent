package com.jsbsoft.jtf.core;

import java.lang.annotation.Annotation;
import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.ClassPathBeanDefinitionScanner;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.type.filter.AssignableTypeFilter;

public class ClassBeanManager {

    public static final String ID_BEAN = "classBeanManager";

    /** The Constant LOG. */
    private static final Logger LOG = LoggerFactory.getLogger(ClassBeanManager.class);

    private List<String> packages;

    private List<String> classes;

    private GenericApplicationContext contexte;

    public static ClassBeanManager getInstance() {
        return (ClassBeanManager) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    public List<String> getPackages() {
        return packages;
    }

    public void setPackages(final List<String> packages) {
        this.packages = packages;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(final List<String> classes) {
        this.classes = classes;
    }

    public void refresh() {
        contexte = new GenericApplicationContext();
        try {
            final ClassPathBeanDefinitionScanner scanner = new ClassPathBeanDefinitionScanner(contexte, false);
            for (final String classe : classes) {
                scanner.addIncludeFilter(new AssignableTypeFilter(Class.forName(classe)));
            }
            scanner.setIncludeAnnotationConfig(false);
            scanner.scan(packages.toArray(new String[packages.size()]));
            contexte.refresh();
        } catch (final ClassNotFoundException e) {
            LOG.error("Erreur de chargement des beans utils", e);
        }
    }

    public <T> Collection<T> getBeanOfType(final Class<T> classe) {
        return contexte.getBeansOfType(classe).values();
    }

    public Collection<?> getBeansWithAnnotation(final Class<? extends Annotation> annotationType) {
        return contexte.getBeansWithAnnotation(annotationType).values();
    }
}
