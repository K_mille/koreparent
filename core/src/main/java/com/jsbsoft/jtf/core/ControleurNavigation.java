package com.jsbsoft.jtf.core;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Détermine le flux (JSP, objet, etc.) en fonction de l'écran logique renvoyé par le processus . <BR>
 * (classe interne jtf)
 */
class ControleurNavigation {

    private static final Logger LOG = LoggerFactory.getLogger(ControleurNavigation.class);

    /** The procedure. */
    private final ProcedureBean procedure;

    /**
     * constructeur unique.
     *
     * @param _procedure
     *            the _procedure
     */
    public ControleurNavigation(final ProcedureBean _procedure) {
        procedure = _procedure;
    }

    /**
     * Renvoie le descriptif du flux (JSP, HTML, objet, etc.) à afficher en fonction des données demandées.<BR>
     *
     * @return the prochain flux
     *
     */
    public DescriptifPageRetour getProchainFlux() {
        LOG.debug("Calcul de l'écran physique...");
        DescriptifPageRetour desc = null;
        // récupération de l'écran logique associé à l'écran physique dans le
        // fichier paramètre
        final InfoBean infoBean = procedure.getInfoBean();
        final InfosEnchainementProcessus infoProcessus = ProcessusHelper.getInfosEnchainementProcessus(infoBean);
        infoBean.setEcranConteneur(infoProcessus.conteneur);
        ProcessusHelper.positionneComposantDansInfoBean(infoBean, infoBean.getNomProcessus());
        if (infoProcessus.type == InfosEnchainementProcessus.TYPE_JSP) {
            desc = DescriptifPageRetour.descriptifPourJSP(infoProcessus.retour);
        } else if (infoProcessus.type == InfosEnchainementProcessus.TYPE_URL_REDIRECTION) {
            desc = DescriptifPageRetour.descriptifPourUrl(infoProcessus.retour);
        }
        return desc;
    }
}
