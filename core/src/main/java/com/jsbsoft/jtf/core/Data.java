package com.jsbsoft.jtf.core;

import java.io.Serializable;

/**
 * Classe qui encapsule toute donnée échangée entre l'IHM et le processus. (classe interne jtf)
 */
class Data implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /** The name. */
    String name = null;

    /** The required. */
    boolean required = false;

    /** The controled. */
    boolean controled = true; // Variable controlée et déformattée

    /** The value. */
    Object value = null;

    /** The code. */
    private boolean code = false;

    /**
     * Instantiates a new data.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @param required
     *            the required
     */
    Data(String name, Object value, boolean required) {
        this.name = name;
        this.value = value;
        this.required = required;
    }

    /**
     * Instantiates a new data.
     *
     * @param name
     *            the name
     * @param value
     *            the value
     * @param required
     *            the required
     * @param controled
     *            the controled
     */
    Data(String name, Object value, boolean required, boolean controled) {
        this.name = name;
        this.value = value;
        this.required = required;
        this.controled = controled;
    }

    /**
     * Gets the name.
     *
     * @return the name
     */
    String getName() {
        return name;
    }

    /**
     * Gets the value.
     *
     * @return the value
     */
    Object getValue() {
        return value;
    }

    /**
     * Checks if is code.
     *
     * @return true, if is code
     */
    public boolean isCode() {
        return code;
    }

    /**
     * Checks if is controled.
     *
     * @return true, if is controled
     */
    boolean isControled() {
        return controled;
    }

    /**
     * Checks if is required.
     *
     * @return true, if is required
     */
    boolean isRequired() {
        return required;
    }

    /**
     * Sets the code.
     *
     * @param newCode
     *            the new code
     */
    public void setCode(boolean newCode) {
        code = newCode;
    }

    /**
     * Sets the controled.
     *
     * @param controled
     *            the new controled
     */
    void setControled(boolean controled) {
        this.controled = controled;
    }

    /**
     * Sets the required.
     *
     * @param required
     *            the new required
     */
    void setRequired(boolean required) {
        this.required = required;
    }

    /**
     * Sets the value.
     *
     * @param value
     *            the new value
     */
    void setValue(Object value) {
        this.value = value;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return (required ? "?\t" : "\t") + name + " = [" + value + "]";
    }
}