package com.jsbsoft.jtf.core;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyHelper;

/**
 * Descriptif d'un flux de retour<BR>
 * (classe interne jtf).
 */
public class DescriptifPageRetour {

    /** The type de flux. */
    private TypeFlux typeDeFlux;

    /** The nom. */
    private String nom = null;

    /** The objet serialisable. */
    private Serializable objetSerialisable = null;

    /** The url string. */
    private String urlString = null;

    /**
     * Constructeur.
     */
    public DescriptifPageRetour() {
        super();
    }

    /**
     * Renvoie une instance qui décrit une page HTML.
     *
     * @param nom
     *            java.lang.String
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    public static DescriptifPageRetour descriptifPourHTML(String nom) {
        DescriptifPageRetour desc = new DescriptifPageRetour();
        desc.setNom(nom);
        desc.setTypeDeFlux(TypeFlux.HTML);
        return desc;
    }

    /**
     * Renvoie une instance qui décrit une JSP.
     *
     * @param nomJsp
     *            java.lang.String
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    public static DescriptifPageRetour descriptifPourJSP(String nomJsp) {
        DescriptifPageRetour desc = new DescriptifPageRetour();
        desc.setNom(nomJsp);
        desc.setTypeDeFlux(TypeFlux.JSP);
        return desc;
    }

    /**
     * Renvoie une instance qui décrit un objet sérialisable.
     *
     * @param obj
     *            java.io.Serializable
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    public static DescriptifPageRetour descriptifPourObjet(Serializable obj) {
        DescriptifPageRetour desc = new DescriptifPageRetour();
        desc.setObjetSerialisable(obj);
        desc.setTypeDeFlux(TypeFlux.OBJET_SERIALISE);
        return desc;
    }

    /**
     * Crée un descriptif qui correspond à une URL Date de création : (14/02/00 17:57:45).
     *
     * @param chaineURL
     *            java.lang.String
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    public static DescriptifPageRetour descriptifPourUrl(String chaineURL) {
        DescriptifPageRetour desc = new DescriptifPageRetour();
        desc.setUrlString(chaineURL);
        desc.setTypeDeFlux(TypeFlux.URL);
        return desc;
    }

    /**
     * Renvoie une instance qui décrit un document XML.
     *
     * @param nomXml
     *            java.lang.String
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    public static DescriptifPageRetour descriptifPourXML(String nomXml) {
        DescriptifPageRetour desc = new DescriptifPageRetour();
        desc.setNom(nomXml);
        desc.setTypeDeFlux(TypeFlux.XML);
        return desc;
    }

    /**
     * Renvoie le descriptif de la page par défaut.
     *
     * @return com.jsbsoft.jtf.session.DescriptifPageRetour
     */
    public static DescriptifPageRetour descriptifPageDefaut(String nom, String extension) {
        String pageFin = PropertyHelper.getProperty(extension, "processus." + nom + ".page_fin");
        if (pageFin == null) {
            pageFin = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.page_fin");
            if (pageFin == null) {
                pageFin = ProcedureBean.CSTE_NOM_PAGE_DEFAUT;
            }
        }
        return DescriptifPageRetour.descriptifPourJSP(pageFin);
    }

    /**
     * Renvoie null ou le nom de la page.
     *
     * @return java.lang.String
     */
    public String getNom() {
        return nom;
    }

    /**
     * Renvoie null ou l'objet à sérialiser.
     *
     * @return java.io.Serializable
     */
    public Serializable getObjetSerialisable() {
        return objetSerialisable;
    }

    /**
     * Renvoie le type de flux (JSP, objet, etc.)
     *
     * @return int
     */
    public TypeFlux getTypeDeFlux() {
        return typeDeFlux;
    }

    /**
     * Accesseur Date de création : (14/02/00 17:55:42).
     *
     * @return java.net.URL
     */
    public String getUrlString() {
        return urlString;
    }

    /**
     * Renseigne le nom (de la JSP, de la page html, etc.)
     *
     * @param newValue
     *            java.lang.String
     */
    public void setNom(String newValue) {
        this.nom = newValue;
    }

    /**
     * Renseigne l'objet à sérialiser.
     *
     * @param newValue
     *            java.io.Serializable
     */
    public void setObjetSerialisable(Serializable newValue) {
        this.objetSerialisable = newValue;
    }

    /**
     * Renseigne le type de flux (JSP, objet, etc.)
     *
     * @param newValue
     *            int
     */
    public void setTypeDeFlux(TypeFlux newValue) {
        this.typeDeFlux = newValue;
    }

    /**
     * Accesseur Date de création : (14/02/00 17:55:42).
     *
     * @param newUrlString
     *            the new url string
     */
    public void setUrlString(String newUrlString) {
        urlString = newUrlString;
    }

    /**
     * Renvoie le descriptif sous forme de chaîne ("la description du descriptif !").
     *
     * @return a string representation of the object.
     */
    @Override
    public String toString() {
        String rep;
        switch (getTypeDeFlux()) {
            case JSP:
                rep = "JSP " + getNom();
                break;
            default:
                rep = "Objet " + getObjetSerialisable();
        }
        return rep;
    }

    public enum TypeFlux {
        JSP(), OBJET_SERIALISE(), HTML(), URL(), XML()
    }
}
