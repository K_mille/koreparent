package com.jsbsoft.jtf.core;

import java.sql.Time;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Classe de formatage (classe interne jtf).
 */
public class Formateur {

    private static final Logger LOG = LoggerFactory.getLogger(Formateur.class);

    /**
     * Formater.
     *
     * @param _o
     *            the _o
     *
     * @return the string
     */
    public static String formater(final Object _o) {
        return formater(_o, "");
    }

    /**
     * Formater.
     *
     * @param _o
     *            the _o
     * @param format
     *            the format
     *
     * @return the string
     */
    public static String formater(final Object _o, final String format) {
        String res = "";
        if (_o instanceof Date) {
            res = formaterDate(_o, format);
        }
        if (_o instanceof Time) {
            res = formaterTime(_o, format);
        }
        if (_o instanceof Integer) {
            res = formaterNombre(_o, format);
        }
        if (_o instanceof Long) {
            res = formaterNombre(_o, format);
        }
        if (_o instanceof Double) {
            res = formaterDecimal(_o, format);
        }
        if (_o instanceof String) {
            res = _o.toString();
        }
        return res;
    }

    /**
     * Formater date.
     *
     * @param _date
     *            the _date
     * @param format
     *            the format
     *
     * @return the string
     */
    public static String formaterDate(final Object _date, final String format) {
        String value;
        final Date date = (Date) _date;
        if (estSaisie(date)) {
            final GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            final Object[] arguments = {cal.get(Calendar.DAY_OF_MONTH), cal.get(Calendar.MONTH) + 1, cal.get(Calendar.YEAR)};
            value = MessageFormat.format("{0,number,00}/{1,number,00}/{2,number,0000}", arguments);
        } else {
            value = "";
        }
        return value;
    }

    /**
     * Formater time.
     *
     * @param time
     *            the time
     * @param format
     *            the format
     *
     * @return the string
     */
    public static String formaterTime(final Object time, final String format) {
        String value = StringUtils.EMPTY;
        if (time != null && time instanceof Time) {
            final Time timeConverted = (Time) time;
            if (timeConverted.getTime() != -3599000) {
                final Date date = new Date(timeConverted.getTime());
                final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                value = sdf.format(date);
            }
        }
        return value;
    }

    /**
     * Formater libelle.
     *
     * @param code
     *            the _code
     * @param nomTable
     *            the nom table
     *
     * @return the string
     */
    public static String formaterLibelle(final String idExt, final Object code, final String nomTable) {
        return CodeLibelle.lireLibelle(idExt, nomTable, (String) code);
    }

    /**
     * Formater nombre.
     *
     * @param nombre
     *            the _nombre
     * @param format
     *            the format
     *
     * @return the string
     */
    public static String formaterNombre(final Object nombre, final String format) {
        return  nombre.toString();
    }

    /**
     * Renvoie true si une date est valorisée.
     *
     * @param date
     *            the _date
     *
     * @return true, if est saisie
     */
    public static boolean estSaisie(final Date date) {
        boolean res = false;
        if (date != null) {
            final GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(date);
            res = cal.get(Calendar.YEAR) != 1970 || cal.get(Calendar.MONTH) != 0 || cal.get(Calendar.DAY_OF_MONTH) != 1;
        }
        return res;
    }

    /**
     * Renvoie true si un champ type "site web" est valorisé.
     *
     * @param siteWeb
     *            the _site web
     *
     * @return true, if est saisi
     */
    public static boolean estSaisi(final String siteWeb) {
        return StringUtils.isNotEmpty(siteWeb) && !"http://".equals(siteWeb);
    }

    /**
     * Formater decimal.
     *
     * @param _nombre
     *            the _nombre
     * @param format
     *            the format
     *
     * @return the string
     */
    public static String formaterDecimal(final Object _nombre, final String format) {
        String value = StringUtils.EMPTY;
        if (_nombre != null) {
            value = _nombre.toString();
        }
        return value;
    }

    /**
     * Parser date.
     *
     * @param valeur
     *            the valeur
     *
     * @return the date
     *
     * @throws ParseException lours du parsing de la date.
     */
    public static Date parserDate(final String valeur) throws ParseException {
        if (StringUtils.isNotBlank(valeur)) {
            final SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            final GregorianCalendar cal = new GregorianCalendar();
            //Force le parser à générer une erreur en cas de date invalide
            dateFormat.setLenient(false);
            cal.setTime(dateFormat.parse(valeur));
            cal.set(Calendar.HOUR_OF_DAY, 0);
            cal.set(Calendar.MINUTE, 0);
            cal.set(Calendar.SECOND, 0);
            //transformation en date SQL
            return new Date(cal.getTimeInMillis());
        } else {
            return new Date(0);
        }
    }

    /**
     * Renvoyer date jour.
     *
     * @param valeur
     *            the valeur
     * @param plus
     *            the plus
     *
     * @return the date
     *
     */
    public static Date renvoyerDateJour(final String valeur, final boolean plus) {
        Date res = null;
        int jour;
        if (StringUtils.isNoneBlank(valeur)) {
            try {
                jour = Integer.parseInt(valeur);
            } catch (final NumberFormatException e) {
                LOG.debug("unable to parse the given value", e);
                return res;
            }
            final Date dateAujourdhui = new Date(System.currentTimeMillis());
            final GregorianCalendar cal = new GregorianCalendar();
            cal.setTime(dateAujourdhui);
            if (plus) {
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) + jour);
            } else {
                cal.set(Calendar.DAY_OF_YEAR, cal.get(Calendar.DAY_OF_YEAR) - jour);
            }
            res = new Date(cal.getTimeInMillis());
        }
        return res;
    }
}
