/*
 * Created on 30 oct. 2005
 * JSS 20051030
 *
*/
package com.jsbsoft.jtf.core;

import java.io.File;
import java.io.FilenameFilter;

/**
 * The Class InfoBeanFilter.
 *
 * @author jean-sébastien steux
 *
 *         Filtre des sauvegardes des InfosBean dans fichiers (pagination des listes)
 */
public class InfoBeanFilter implements FilenameFilter {

    /** The pattern. */
    String pattern = "";

    /**
     * Instantiates a new info bean filter.
     *
     * @param ksession
     *            the ksession
     * @param type
     *            the type
     */
    public InfoBeanFilter(String ksession, String type) {
        super();
        pattern = "datas_";
        if (ksession != null && ksession.length() > 0) {
            pattern += ksession + "_" + type;
        }
    }

    /* (non-Javadoc)
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    @Override
    public boolean accept(File dir, String name) {
        return name.indexOf(pattern) == 0;
    }
}