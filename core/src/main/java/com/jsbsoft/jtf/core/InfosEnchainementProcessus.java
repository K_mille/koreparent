package com.jsbsoft.jtf.core;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe de stockage de la page à afficher.
 */
public class InfosEnchainementProcessus {

    /** The TYP e_ aucune. */
    public static final int TYPE_AUCUNE = 0;

    /** The TYP e_ jsp. */
    public static final int TYPE_JSP = 1;

    /** The TYPE URL. */
    public static final int TYPE_URL_REDIRECTION = 3;

    /** The type action. */
    public int type = 0;

    /** The nom action. */
    public String retour = StringUtils.EMPTY;

    public String conteneur = StringUtils.EMPTY;

    /**
     * Commentaire relatif au constructeur InfosEnchainementProcessus.
     */
    public InfosEnchainementProcessus() {}

    /**
     * Commentaire relatif au constructeur InfosEnchainementProcessus.
     *
     * @param type
     *            the _type action
     * @param retour
     *            the _nom action
     */
    public InfosEnchainementProcessus(final int type, final String retour) {
        this.type = type;
        this.retour = retour;
    }

    public InfosEnchainementProcessus(final int type, final String retour, final String conteneur) {
        this.type = type;
        this.retour = retour;
        this.conteneur = conteneur;
    }
}