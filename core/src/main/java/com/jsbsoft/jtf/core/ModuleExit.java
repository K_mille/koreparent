package com.jsbsoft.jtf.core;

/**
 * Module destineé à gérer les traitements particuliers à une PU grâce à un mécanisme de délégation.
 */
public interface ModuleExit extends ModuleExitProcessus {

    /**
     * Traitement spéinfoBeanfique à la collecte <br>
     * C'est iinfoBean que doivent être gérées les interactions indépendantes du processus.
     *
     * @param procedure
     *            the procedure
     *
     * @return the descriptif page retour
     *
     */
    DescriptifPageRetour apresCollecte(ProcedureBean procedure);

    /**
     * Traitement à effectuer lors de la fin d'une SOUS-PU.
     *
     * @param procedure
     *            the procedure
     * @param infoBeanSousPU
     *            the info bean sous pu
     *
     * @return the descriptif page retour
     */
    DescriptifPageRetour apresSousPU(ProcedureBean procedure, InfoBean infoBeanSousPU);

    /**
     * Traitement appelé par la Composant Logiqe d'Affichage <br>
     * C'est iinfoBean que doivent être calculées les correspondances entre écran logique et écran physique.
     *
     * @param procedure
     *            the procedure
     *
     * @return the descriptif page retour
     *
     * @throws Exception
     *             the exception
     */
    DescriptifPageRetour calculEcranPhysique(ProcedureBean procedure) throws Exception;
}