package com.jsbsoft.jtf.core;

/**
 * Module destineé à gérer les traitements à effectuer au retour d(un processus <br>
 * . Il peut être implémenté dans une procédure (processus de 1er niveau) ou dans un processus (retour du sous-processus)
 */
public interface ModuleExitProcessus {

    /**
     * Apres processus.
     *
     * @param gp
     *            the gp
     * @param cloneCI
     *            the clone ci
     *
     * @return the descriptif page retour
     *
     */
    DescriptifPageRetour apresProcessus(ProcessusManager gp, InfoBean cloneCI);
}