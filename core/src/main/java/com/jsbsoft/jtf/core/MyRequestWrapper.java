package com.jsbsoft.jtf.core;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;

/**
 * Cette classe permet de propager le cookie de session entre la session http et la session https C'est ainsi le même id de session qui est utilisé, et la session n'est pas perdue.
 *
 * @author alice
 */
public class MyRequestWrapper extends HttpServletRequestWrapper {

    private static final Boolean isSecurise = "1".equals(PropertyHelper.getCoreProperty("https.cookies.securise"));

    private static final Logger LOG = LoggerFactory.getLogger(MyRequestWrapper.class);

    private final HttpServletRequest request = null;

    /** The response. */
    private HttpServletResponse response = null;

    /**
     * Instantiates a new my request wrapper.
     *
     * @param request
     *            the request
     */
    public MyRequestWrapper(HttpServletRequest request) {
        super(request);
    }

    /**
     * Sets the response.
     *
     * @param response
     *            the new response
     */
    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServletRequestWrapper#getSession()
     */
    @Override
    public HttpSession getSession() {
        HttpSession session = super.getSession();
        if (!isSecurise) {
            processSessionCookie(session);
        }
        return session;
    }

    /* (non-Javadoc)
     * @see javax.servlet.http.HttpServletRequestWrapper#getSession(boolean)
     */
    @Override
    public HttpSession getSession(boolean create) {
        HttpSession session = null;
        if (!isSecurise) {
            session = getCookieSessionNonSecurise(create);
        } else {
            session = super.getSession(create);
        }
        return session;
    }

    private HttpSession getCookieSessionNonSecurise(boolean create) {
        HttpSession session = super.getSession(create);
        processSessionCookie(session);
        return session;
    }

    /**
     * Process session cookie.
     *
     * @param session
     *            the session
     */
    private void processSessionCookie(HttpSession session) {
        if (null == response || null == session) {
            LOG.debug("response or session is null");
            // No response or session object attached, skip the pre processing
            return;
        }
        // cookieOverWritten - Flag to filter multiple "Set-Cookie" headers
        Object cookieOverWritten = getAttribute("COOKIE_OVERWRITTEN_FLAG");
        LOG.debug("cookieOverWritten" + cookieOverWritten + " isSecure" + isSecure() + " isNew" + session.isNew() + " id = " + session.getId());
        if (null == cookieOverWritten && isSecure() && session.isNew()) {
            // Might have created the cookie in SSL protocol and tomcat will loose the session
            // if there is change in protocol from HTTPS to HTTP. To avoid this, trick the browser
            // using the HTTP and HTTPS session cookie.
            Cookie cookie = new Cookie("JSESSIONID", session.getId());
            cookie.setMaxAge(-1); // Life of the browser or timeout
            String contextPath = getContextPath();
            if ((contextPath != null) && (contextPath.length() > 0)) {
                cookie.setPath(contextPath);
            } else {
                cookie.setPath("/");
            }
            response.addCookie(cookie); // Adding an "Set-Cookie" header to the response
            setAttribute("COOKIE_OVERWRITTEN_FLAG", "true");// To avoid multiple "Set-Cookie" header
            LOG.debug("cookie overwritten");
        }
    }

    //PATCH Double identification (CAS + KDB)
    //PCO 23/04/2009
    // Ajout de deux fonction qui permettent de retourner les éléments
    // HTTPServlet. On se sert de ces éléments dans la double identification
    // pour mettre un cookie dnas la page.
    public HttpServletResponse getResponse() {
        return this.response;
    }

    public HttpServletRequest getHttpServletRequest() {
        return this.request;
    }
}
