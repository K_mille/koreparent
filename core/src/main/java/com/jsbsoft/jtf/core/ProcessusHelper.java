package com.jsbsoft.jtf.core;

import java.util.Collection;
import java.util.HashMap;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.ProcessusBean;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.composant.ComposantHelper;
import com.kportal.extension.module.composant.IComposant;
import com.kportal.util.URLUtils;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.url.FrontOfficeMgr;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;

public class ProcessusHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessusHelper.class);

    /**
     * Détermine en fonction de l'écran logique renvoyé par le processus le type d'action à effectuer : - enchainement sur un autre processus - envoi d'une page à l'utilisateur.
     *
     * @param infoBean
     *            the info bean
     *
     * @return the infos enchainement processus
     */
    public static InfosEnchainementProcessus getInfosEnchainementProcessus(final InfoBean infoBean) {
        final String nomProcessus = infoBean.getNomProcessus();
        final String nomExtension = infoBean.getNomExtension();
        InfosEnchainementProcessus ret = new InfosEnchainementProcessus();
        /* Récupération de l'écran logique à partir du CI */
        final String ecranLogique = infoBean.getEcranLogique();
        String ecranRedirection = infoBean.getEcranRedirection();
        final String ecranPhysique = infoBean.getEcranPhysique();
        final boolean isFront = isProcessusFrontOffice(nomProcessus, nomExtension);
        // erreur dans le traitement
        if (StringUtils.isNotBlank(infoBean.getMessageErreur())) {
            ret = getInfosEnchainementErreur(infoBean);
        }
        // 3 - Calcul de l'ecran
        else if (ecranLogique != null) {
            final String nomJSP = getNomJSP(nomProcessus, nomExtension, ecranLogique, isFront);
            if ("[ecran_redirection]".equals(nomJSP)) {
                if (StringUtils.isEmpty(ecranRedirection)) {
                    ecranRedirection = calculerEcranRedirection(infoBean);
                }
                if (StringUtils.isNotEmpty(ecranRedirection)) {
                    ret.type = InfosEnchainementProcessus.TYPE_URL_REDIRECTION;
                    ecranRedirection = setMessagesDansURL(ecranRedirection, infoBean);
                    if (StringUtils.isNotEmpty(nomExtension)) {
                        ecranRedirection = URLUtils.addParameter(ecranRedirection, InfoBean.EXT, nomExtension);
                    }
                    ret.retour = ecranRedirection;
                }
            } else if ("[ecran_physique]".equals(nomJSP) && StringUtils.isNotEmpty(ecranPhysique)) {
                ret.type = InfosEnchainementProcessus.TYPE_JSP;
                ret.retour = ecranPhysique;
            } else {
                ret.type = InfosEnchainementProcessus.TYPE_JSP;
                ret.retour = nomJSP;
            }
        }
        // 4 - On recherche une url de redirection car fin de la procédure et aucun écran logique dispo
        else if (!isFront) {
            ecranRedirection = calculerEcranRedirection(infoBean);
            if (StringUtils.isNotEmpty(ecranRedirection)) {
                ret.type = InfosEnchainementProcessus.TYPE_URL_REDIRECTION;
                ret.retour = ecranRedirection;
            }
        }
        if (ret.type == InfosEnchainementProcessus.TYPE_AUCUNE && StringUtils.isBlank(ret.retour)) {
            ret.type = InfosEnchainementProcessus.TYPE_JSP;
            ret.retour = PropertyHelper.getProperty(StringUtils.EMPTY, "processus." + ErrorProcessus.NOM_PROCESSUS + ".ecran_physique." + ErrorProcessus.ECRAN_PRINCIPAL);
        }
        if (StringUtils.isBlank(ret.conteneur)) {
            ret.conteneur = getEcranConteneur(nomExtension, nomProcessus, ecranLogique);
        }
        return ret;
    }

    private static String getNomJSP(final String nomProcessus, final String nomExtension, final String ecranLogique, final boolean isFront) {
        String nomJSP = PropertyHelper.getProperty(nomExtension, "processus." + nomProcessus + ".ecran_physique." + ecranLogique);
        if (nomJSP == null) {
            if (isFront) {
                nomJSP = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.ecran_physique_front." + ecranLogique);
            } else {
                nomJSP = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.ecran_physique." + ecranLogique);
            }
        }
        return nomJSP;
    }

    private static String calculerEcranRedirection(final InfoBean infoBean) {
        final String nomProcessus = infoBean.getNomProcessus();
        String ecranRedirection = StringUtils.EMPTY;
        final IComposant composantCourant = ProcessusHelper.getComposantParNomProcessus(nomProcessus);
        if (composantCourant != null) {
            ecranRedirection = setMessagesDansURL(composantCourant.getUrlAccueilBo(), infoBean);
        }
        return ecranRedirection;
    }

    /**
     * Rajoute les messages de confirmation si present dans l'url pour les avoir sur l'accueil du processus.
     *
     * @param url
     * @param infoBean
     * @return
     */
    private static String setMessagesDansURL(String url, final InfoBean infoBean) {
        url = URLUtils.addParameter(url, TypeMessage.ALERTE.getCode(), infoBean.getMessageAlerte());
        url = URLUtils.addParameter(url, TypeMessage.CONFIRMATION.getCode(), infoBean.getMessageConfirmation());
        url = URLUtils.addParameter(url, TypeMessage.ERREUR.getCode(), infoBean.getMessageErreur());
        return url;
    }

    /**
     * Calcule l' {@link InfosEnchainementProcessus} lorsque l'on a une erreur sur le processus.
     *
     * @param infoBean
     * @return la page d'erreur correspondante
     */
    private static InfosEnchainementProcessus getInfosEnchainementErreur(final InfoBean infoBean) {
        InfosEnchainementProcessus infosEnchainement = null;
        String nomJSP = StringUtils.EMPTY;
        FicheUniv fiche = null;
        final String referer = (String) infoBean.get("PAGE_REFERER");
        if (infoBean.getModeEnchainement() == ProcedureBean.MODE_SEQUENTIEL) {
            final Boolean isFront = isProcessusFrontOffice(infoBean.getNomProcessus(), infoBean.getNomExtension());
            nomJSP = getNomJSP(infoBean.getNomProcessus(), infoBean.getNomExtension(), infoBean.getEcranLogique(), isFront);
        } else {
            nomJSP = PropertyHelper.getProperty(infoBean.getNomExtension(), "processus." + infoBean.getNomProcessus() + ".ecran_erreur." + infoBean.getEcranLogique());
            if (StringUtils.isEmpty(nomJSP)) {
                nomJSP = PropertyHelper.getProperty(infoBean.getNomExtension(), "processus." + infoBean.getNomProcessus() + ".page_erreur");
            }
        }
        if ("[erreur_front]".equals(nomJSP)) {
            if (referer != null) {
                fiche = getFicheDepuisUrl(referer);
            } else {
                nomJSP = StringUtils.EMPTY;
            }
        }
        if (fiche != null) {
            infosEnchainement = new InfosEnchainementProcessus(InfosEnchainementProcessus.TYPE_URL_REDIRECTION, referer);
        } else if (StringUtils.isNotBlank(nomJSP)) {
            infosEnchainement = new InfosEnchainementProcessus(InfosEnchainementProcessus.TYPE_JSP, nomJSP);
        }
        if (infosEnchainement == null) {
            infosEnchainement = getDefautPageErreur(isProcessusFrontOffice(infoBean.getNomProcessus(), infoBean.getNomExtension()));
        }
        return infosEnchainement;
    }

    /**
     * Retourne la page d'erreur par défaut de l'application
     *
     * @param isFront
     *            est qu'on est en front ou pas
     * @return la page d'erreur paramétré
     */
    private static InfosEnchainementProcessus getDefautPageErreur(final boolean isFront) {
        String pageErreur = StringUtils.EMPTY;
        String ecranConteneur = StringUtils.EMPTY;
        if (isFront) {
            pageErreur = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.page_erreur_front");
            ecranConteneur = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.ecran_conteneur_front");
        } else {
            pageErreur = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.page_erreur");
            ecranConteneur = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.ecran_conteneur");
        }
        return new InfosEnchainementProcessus(InfosEnchainementProcessus.TYPE_JSP, pageErreur, ecranConteneur);
    }

    /**
     * Est ce que c'est un processus front ou pas
     *
     * @param nomExtension
     * @param nomProcessus
     * @return
     */
    public static boolean isProcessusFrontOffice(final String nomProcessus, final String nomExtension) {
        return Boolean.valueOf(PropertyHelper.getProperty(nomExtension, "processus." + nomProcessus + ".is_front"));
    }

    /**
     * Calcule depuis l'url fourni en paramètre la {@link FicheUniv} correspondante.
     *
     * @param url
     * @return Si la fiche n'est pas accessible (permission/fiche hors ligne...), on retourne une fiche null
     */
    private static FicheUniv getFicheDepuisUrl(final String url) {
        FicheUniv fiche = null;
        try {
            fiche = FrontOfficeMgr.lireFiche(ContexteUtil.getContexteUniv(), url, new HashMap<String, String[]>());
        } catch (final Exception e) {
            LOGGER.debug("unable to find the FicheUniv from the given URL", e);
        }
        return fiche;
    }

    /**
     * Récupère si dispo le composant courant par rapport au processus sur le quel on se trouve. Une fois récupérer, on ajoute l'id du composant dans l'infoBean
     *
     * @param infoBean
     * @param nomProcessus
     */
    public static void positionneComposantDansInfoBean(final InfoBean infoBean, final String nomProcessus) {
        final Collection<IComposant> composantsDefinis = ComposantHelper.getComposants();
        for (final IComposant composantCourant : composantsDefinis) {
            if (composantCourant.getParametreProcessus().equals(nomProcessus)) {
                infoBean.setIdComposant(composantCourant.getId());
                return;
            }
        }
    }

    /**
     * Retrouve le composant lié au Processus fourni en paramètre
     *
     * @param nomProcessus
     * @return
     */
    public static IComposant getComposantParNomProcessus(final String nomProcessus) {
        final Collection<IComposant> composantsDefinis = ComposantHelper.getComposants();
        for (final IComposant composantCourant : composantsDefinis) {
            if (composantCourant.getParametreProcessus().equals(nomProcessus)) {
                return composantCourant;
            }
        }
        return null;
    }

    /**
     * Retourne l'écran conteneur si il en existe un.
     *
     * @return
     */
    public static String getEcranConteneur(final String nomExtension, final String nomProcessus, final String ecranLogique) {
        String nomConteneur = PropertyHelper.getProperty(nomExtension, "processus." + nomProcessus + ".ecran_conteneur." + ecranLogique);
        if (nomConteneur == null) {
            nomConteneur = PropertyHelper.getProperty(nomExtension, "processus." + nomProcessus + ".ecran_conteneur");
            if (nomConteneur == null) {
                nomConteneur = PropertyHelper.getProperty(nomExtension, "processus.ecran_conteneur." + ecranLogique);
                if (nomConteneur == null) {
                    final boolean isFront = Boolean.valueOf(PropertyHelper.getProperty(nomExtension, "processus." + nomProcessus + ".is_front"));
                    if (isFront) {
                        nomConteneur = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.ecran_conteneur_front");
                    } else {
                        nomConteneur = PropertyHelper.getProperty(StringUtils.EMPTY, "processus.ecran_conteneur");
                    }
                }
            }
        }
        return nomConteneur;
    }

    /**
     * Renvoie la classe associée au processus.
     *
     * @param processus
     *            the processus
     *
     * @return the classe processus
     */
    public static Class<? extends ProcessusBean> getClasseProcessus(final String processus, final String extension) {
        Class<? extends ProcessusBean> classe = null;
        final String nomClasse = PropertyHelper.getProperty(extension, "processus." + processus + ".classe");
        if (nomClasse == null) {
            return classe;
        }
        try {
            classe = Class.forName(nomClasse).asSubclass(ProcessusBean.class);
        } catch (final ClassNotFoundException e) {
            LOGGER.error("Le processus " + processus + "[" + nomClasse + "] n'a pas été trouvé", e);
        }
        return classe;
    }

    public static String getUrlProcessAction(final InfoBean infoBean, final String extension, final String processus, final String action, final String[][] param) {
        final StringBuilder url = new StringBuilder(WebAppUtil.SG_PATH);
        url.append("?");
        final String ext = StringUtils.isNotEmpty(extension) ? extension : infoBean.getNomExtension();
        if (StringUtils.isNotEmpty(ext)) {
            url.append("EXT=").append(ext).append("&amp;");
        }
        url.append("PROC=").append(StringUtils.isNotEmpty(processus) ? processus : infoBean.getNomProcessus());
        url.append("&amp;ACTION=").append(action);
        if (param != null) {
            for (final String[] strings : param) {
                if (strings.length > 1) {
                    url.append("&amp;").append(strings[0]).append("=").append(EscapeString.escapeURL(strings[1]));
                }
            }
        }
        return url.toString();
    }
}
