package com.jsbsoft.jtf.core;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.ldap.ISynchroLdapUtilisateur;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.utils.ISynchroniseurUtilisateurMetier;

/**
 * Singleton prennant en charge la synchro des utilisateurs vers une source externe (LDAP) et vers les utilisateurs métier.
 *
 * @author jbiard
 */
public class SynchroniseurUtilisateur {

    /** The Constant SYNCHRO_USER_ADD. */
    public final static int SYNCHRO_USER_ADD = 0;

    /** The Constant SYNCHRO_USER_UPDATE. */
    public final static int SYNCHRO_USER_UPDATE = 1;

    /** The Constant SYNCHRO_USER_DELETE. */
    public final static int SYNCHRO_USER_DELETE = 2;

    private static final Logger LOG = LoggerFactory.getLogger(SynchroniseurUtilisateur.class);

    /** The _instance. */
    private static SynchroniseurUtilisateur _instance;

    /** The map synchro utilisateur metier. */
    private Map<String, Object> mapSynchroUtilisateurMetier;

    /** The synchro ldap. */
    private ISynchroLdapUtilisateur synchroLdap;

    /**
     * Instantiates a new synchroniseur utilisateur.
     */
    private SynchroniseurUtilisateur() {
        chargeObjetsSessionUserMetier();
        chargeSynchroniseurLdap();
    }

    /**
     * Accesseur singleton.
     *
     * @return the instance
     */
    public static synchronized SynchroniseurUtilisateur getInstance() {
        if (_instance == null) {
            _instance = new SynchroniseurUtilisateur();
        }
        return _instance;
    }

    /**
     * Gets the map synchro utilisateur metier.
     *
     * @return the map synchro utilisateur metier
     */
    public Map<String, Object> getMapSynchroUtilisateurMetier() {
        return mapSynchroUtilisateurMetier;
    }

    /**
     * Gets the synchro ldap.
     *
     * @return the synchro ldap
     */
    public ISynchroLdapUtilisateur getSynchroLdap() {
        return synchroLdap;
    }

    /**
     * Appelee par depuis l'utilisateur. Prend en charge la synchro vers la source externe et vers les utilisateurs métier.
     *
     * @param typeSynchro
     *            type de synchro (voir constantes)
     * @param utilisateur
     *            utilisateur servant de base à la synchro
     * @param ctx
     *            contexte
     * @param majOnlySourceExterne
     *            boolean : true syncbronisation de la source EXTERNE exclusivement; false maj de toutes les sources
     *
     * @throws Exception
     *             the exception
     */
    public void synchroUtilisateur(final int typeSynchro, final UtilisateurBean utilisateur, final OMContext ctx, final boolean majOnlySourceExterne) throws Exception {
        if (!majOnlySourceExterne) {
            synchroUserObjetsMetier(typeSynchro, utilisateur, ctx);
        }
        synchroUserSourceExterne(typeSynchro, utilisateur, ctx);
    }

    /**
     * Appelee par depuis l'utilisateur. Prend en charge la synchro vers la source externe et vers les utilisateurs métier.
     *
     * @param typeSynchro
     *            type de synchro (voir constantes)
     * @param utilisateur
     *            utilisateur servant de base à la synchro
     * @param ctx
     *            contexte
     *
     * @throws Exception
     *             the exception
     */
    public void synchroUtilisateur(final int typeSynchro, final UtilisateurBean utilisateur, final OMContext ctx) throws Exception {
        synchroUtilisateur(typeSynchro, utilisateur, ctx, false);
    }

    /**
     * Synchro user source externe.
     *
     * @param typeSynchro
     *            the type synchro
     * @param utilisateur
     *            the utilisateur
     * @param ctx
     *            the ctx
     *
     * @throws Exception
     *             the exception
     */
    protected void synchroUserSourceExterne(final int typeSynchro, final UtilisateurBean utilisateur, final OMContext ctx) throws Exception {
        if (synchroLdap != null) {
            synchroLdap.synchroniseUtilisateur(typeSynchro, utilisateur, ctx);
        }
    }

    /**
     * Lance la synchronisation utilisateur -> utilisateur(s) metier.
     *
     * @param utilisateur
     *            utilisateur a partir duquel on va faire la maj.
     * @param ctx
     *            contexte
     * @param typeSynchro
     *            the type synchro
     *
     * @return vrai si une synchro a été effectuée
     *
     * @throws Exception
     *             the exception
     */
    protected boolean synchroUserObjetsMetier(final int typeSynchro, final UtilisateurBean utilisateur, final OMContext ctx) throws Exception {
        if (mapSynchroUtilisateurMetier != null) {
            if (typeSynchro == SYNCHRO_USER_UPDATE) {
                ISynchroniseurUtilisateurMetier uMetier;
                boolean bSynchronisation = false;
                for (final Object o : mapSynchroUtilisateurMetier.values()) {
                    uMetier = (ISynchroniseurUtilisateurMetier) o;
                    //uMetier.setCtx( ctx );
                    // synchro utilisateur -> utilisateur(s) metier
                    bSynchronisation = uMetier.majUtilisateurMetier(utilisateur, ctx) || bSynchronisation;
                }
                return bSynchronisation;
            }
        }
        return false;
    }

    /**
     * Charge les objets prennant en charge le chargement des infos de session de l'utilisateur.
     */
    protected void chargeObjetsSessionUserMetier() {
        // on charge les objets en charge de renseigner les infos de sessions
        // pour les applicatifs métier
        if (mapSynchroUtilisateurMetier == null) {
            mapSynchroUtilisateurMetier = new HashMap<>();
            final Enumeration<Object> enumProperties = PropertyHelper.getAllProperties().elements();
            String clef, nomClasse, nomApplicatif;
            final int debutNomApplicatif = ISynchroniseurUtilisateurMetier.SESSION_UTILISATEUR_CHARGEMENT_CLASSE_LIBELLE_JTF.length() + 1;
            while (enumProperties.hasMoreElements()) {
                clef = (String) enumProperties.nextElement();
                // on repere les entrées
                if (clef.startsWith(ISynchroniseurUtilisateurMetier.SESSION_UTILISATEUR_CHARGEMENT_CLASSE_LIBELLE_JTF)) {
                    nomClasse = PropertyHelper.getCoreProperty(clef);
                    nomApplicatif = clef.substring(debutNomApplicatif);
                    nomApplicatif = nomApplicatif.substring(0, nomApplicatif.lastIndexOf('.'));
                    try {
                        // ajout des objets de traitement
                        mapSynchroUtilisateurMetier.put(nomApplicatif, Class.forName(nomClasse).newInstance());
                    } catch (final ReflectiveOperationException e) {
                        LOG.error("erreur lors de l'ajout des objets de traitement", e);
                    }
                }
            }
        }
    }

    /**
     * Charge synchroniseur ldap.
     */
    private void chargeSynchroniseurLdap() {
        final String nomClasse = PropertyHelper.getCoreProperty(ISynchroLdapUtilisateur.SYNCHRO_LDAP_CLASSE_LIBELLE_JTF);
        if (nomClasse != null) {
            try {
                synchroLdap = (ISynchroLdapUtilisateur) Class.forName(nomClasse).newInstance();
            } catch (final ReflectiveOperationException e) {
                LOG.debug("unable to instanciate the given classe", e);
                LOG.error("** Echec chargement " + nomClasse);
                synchroLdap = null;
            }
        } else {
            synchroLdap = null;
        }
    }
}