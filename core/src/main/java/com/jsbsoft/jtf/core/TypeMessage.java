package com.jsbsoft.jtf.core;

public enum TypeMessage {
    ERREUR("ERREUR"), CONFIRMATION("CONFIRMATION"), ALERTE("ALLERTE"), APPLICATIF("APPLICATIF");

    private String code;

    TypeMessage(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String toString() {
        return code.toLowerCase();
    }
}
