/*
 * Created on 22 févr. 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.jsbsoft.jtf.database;

import java.util.Vector;

/**
 * The Class EtatRequete.
 *
 * @author jean-sébastien steux
 *
 *
 *
 *         Modif le 22 févr. 2004
 */
public class EtatRequete {

    /** The is termine. */
    private boolean isTermine = false;

    /** The timestamp debut. */
    private long timestampDebut = System.currentTimeMillis();

    /** The timestamp fin. */
    private long timestampFin = 0;

    /** The evenements. */
    private Vector<String> evenements = new Vector<>();

    /**
     * Ajouter evenement.
     *
     * @param evenement
     *            the evenement
     */
    public void ajouterEvenement(String evenement) {
        evenements.add(evenement);
    }

    /**
     * Terminer.
     */
    public void terminer() {
        timestampFin = System.currentTimeMillis();
        isTermine = true;
    }

    /**
     * Checks if is terminee.
     *
     * @return true, if is terminee
     */
    public boolean isTerminee() {
        return isTermine;
    }

    /**
     * Gets the evenements.
     *
     * @return the evenements
     */
    public Vector<String> getEvenements() {
        return evenements;
    }

    /**
     * Gets the timestamp debut.
     *
     * @return the timestamp debut
     */
    public long getTimestampDebut() {
        return timestampDebut;
    }

    /**
     * Sets the evenements.
     *
     * @param vector
     *            the vector
     */
    public void setEvenements(Vector<String> vector) {
        evenements = vector;
    }

    /**
     * Sets the timestamp debut.
     *
     * @param l
     *            the l
     */
    public void setTimestampDebut(long l) {
        timestampDebut = l;
    }

    /**
     * Gets the timestamp fin.
     *
     * @return the timestamp fin
     */
    public long getTimestampFin() {
        return timestampFin;
    }

    /**
     * Sets the timestamp fin.
     *
     * @param l
     *            the l
     */
    public void setTimestampFin(long l) {
        timestampFin = l;
    }
}