package com.jsbsoft.jtf.database;

import java.sql.Connection;
import java.util.Locale;
import java.util.Map;

import com.univ.multisites.InfosSite;

/**
 * Le contexte métier (propagé entre processus et objets métiers).
 */
public interface OMContext {

    /** nom du paramètre représentant la servletConfig dans la table du contexte. */
    String CLE_SERVLET_CONFIG = "_servlet_config";

    /** The CL e_ servle t_ s g_ requet e_ http. */
    String CLE_SERVLET_SG_REQUETE_HTTP = "_sg_requete_http";

    /**
     * Gets the connection.
     *
     * @return the connection
     */
    Connection getConnection();

    /**
     * Gets the locale.
     *
     * @return the locale
     */
    Locale getLocale();

    /**
     * Gets the id requete.
     *
     * @return the id requete
     */
    String getIdRequete();

    /**
     * Gets the datas.
     *
     * @return the datas
     */
    Map<String, Object> getDatas();

    /**
     * Sets the infos site.
     *
     * @param infosSite
     *            the new infos site
     */
    void setInfosSite(InfosSite infosSite);

    /**
     * Gets the infos site.
     *
     * @return the infos site
     */
    InfosSite getInfosSite();

    /**
     * Sets the secure.
     *
     * @param secure
     *            the new secure
     */
    void setSecure(boolean secure);

    /**
     * Checks if is secure.
     *
     * @return true, if is secure
     */
    boolean isSecure();
}
