package com.jsbsoft.jtf.datasource.dao;

import java.util.List;

import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;

/**
 * Interface de base d'un DAO pour le produit.
 * @param <T> le bean devant etre retourné lors d'un add/delete/select après l'exécution d'une requete
 */
public interface CommonDAO<T> {

    /**
     * Ajoute un bean en base de données et retourne le même bean avec son id initialisé.
     * @param bean le bean à ajouter à la datasource
     * @return le bean avec son id initialisé
     * @throws AddToDataSourceException une exception Runtime lorsque la requete n'abouti pas (accès BDD, erreur SQL...)
     */
    T add(T bean) throws AddToDataSourceException;

    /**
     * Modifie un bean en base de données et retourne le même bean avec son id initialisé.
     * @param bean le bean à modifier à la datasource
     * @return le bean venant d'etre updaté
     * @throws UpdateToDataSourceException une exception Runtime lorsque la requete n'abouti pas (accès BDD, erreur SQL...)
     */
    T update(T bean) throws UpdateToDataSourceException;

    /**
     * Supprime une donnée via son ID.
     * @param id l'id du bean à supprimer
     * @throws DeleteFromDataSourceException une exception Runtime lorsque la requete n'abouti pas (accès BDD, erreur SQL...)
     */
    void delete(Long id) throws DeleteFromDataSourceException;

    /**
     * Récupère un bean depuis son ID.
     * @param id l'id du bean à récupérer
     * @return le bean retrouvé ou NULL si aucune donnée n'est retrouvé
     * @throws DataSourceException une exception Runtime lorsque la requete n'abouti pas (accès BDD, erreur SQL...)
     */
    T getById(Long id) throws DataSourceException;

    /**
     * Requête une datasource pour retourner un ensemble de valeur correspondant aux critères fournis en paramètre.
     * @param request la requête à exécuter
     * @param parameters les paramètres (en named parameter si possible) nécessaire à l'exécution de la requête
     * @return Une liste de bean répondant aux critères de la requête ou une liste vide si rien ne correspond.
     */
    List<T> select(final String request, SqlParameterSource parameters);
}
