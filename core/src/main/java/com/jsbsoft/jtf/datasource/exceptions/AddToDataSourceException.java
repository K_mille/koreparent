package com.jsbsoft.jtf.datasource.exceptions;

public class AddToDataSourceException extends DataSourceException {

    /**
     *
     */
    private static final long serialVersionUID = 7147924856734922832L;

    public AddToDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddToDataSourceException(String message) {
        super(message);
    }
}
