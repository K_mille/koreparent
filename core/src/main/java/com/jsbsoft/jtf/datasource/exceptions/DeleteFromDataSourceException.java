package com.jsbsoft.jtf.datasource.exceptions;

public class DeleteFromDataSourceException extends DataSourceException {

    /**
     *
     */
    private static final long serialVersionUID = -1874468841924291341L;

    public DeleteFromDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public DeleteFromDataSourceException(String message) {
        super(message);
    }
}
