package com.jsbsoft.jtf.datasource.exceptions;

public class UpdateToDataSourceException extends DataSourceException {

    /**
     *
     */
    private static final long serialVersionUID = -2631415946992190459L;

    public UpdateToDataSourceException(String message, Throwable cause) {
        super(message, cause);
    }
}
