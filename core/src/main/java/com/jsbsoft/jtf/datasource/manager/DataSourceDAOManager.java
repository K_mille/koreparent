package com.jsbsoft.jtf.datasource.manager;

import com.jsbsoft.jtf.datasource.dao.CommonDAO;

public interface DataSourceDAOManager {

    String ID_BEAN = "daoManager";

    <T> CommonDAO<T> getDao(Class<T> clazz);
}
