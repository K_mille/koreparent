package com.jsbsoft.jtf.datasource.naming;

/**
 * Created on 22/09/15.
 */
public interface ColumnNamingStrategy {

    String getColumnId(String tableName);

    String getParamId();

    String getParamName(String tableName, String columnName);

    String getColumnName(String tableName, String paramName);

}
