package com.jsbsoft.jtf.datasource.naming.impl;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.datasource.naming.ColumnNamingStrategy;
import com.univ.utils.json.NamingStrategyHelper;

/**
 * Created on 22/09/15.
 */
public class DefaultColumnNamingStategy implements ColumnNamingStrategy{

    @Override
    public String getColumnId(String tableName) {
        return "ID_" + tableName.toUpperCase();
    }

    @Override
    public String getParamId() {
        return "id";
    }

    @Override
    public String getParamName(String tableName, final String columnName) {
        if(StringUtils.isNotBlank(columnName)) {
            if(getColumnId(tableName).equals(columnName)) {
                return ":" + getParamId();
            } else {
                return ":" + NamingStrategyHelper.translateUnderscoreToUppercase(columnName.toLowerCase());
            }
        }
        return null;
    }

    @Override
    public String getColumnName(String tableName, final String paramName) {
        if(StringUtils.isNotBlank(paramName)) {
            if(getParamId().equals(paramName)) {
                return getColumnId(tableName);
            } else {
                return NamingStrategyHelper.translateUppercaseToUnderscore(paramName).toUpperCase();
            }
        }
        return null;
    }
}
