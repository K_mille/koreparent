package com.jsbsoft.jtf.datasource.pool;

import javax.sql.DataSource;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import com.jsbsoft.jtf.core.ApplicationContextManager;

/**
 * Permet d'écouter sur le chargement du context pour pouvoir réinitialiser le dataSource au rechargement des contextes.
 */
public class DataSourceListener implements ApplicationListener<ContextRefreshedEvent> {

    public static final String ID_BEAN = "datasourceListener";

    private DataSource dataSource;

    /**
     * Initialise le datasource.
     */
    public DataSourceListener() {
        dataSource = ApplicationContextManager.getCoreContextBean("mainDataSource", DataSource.class);
    }

    public DataSource getDataSource() {
        return dataSource;
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        dataSource = ApplicationContextManager.getCoreContextBean("mainDataSource", DataSource.class);
    }
}
