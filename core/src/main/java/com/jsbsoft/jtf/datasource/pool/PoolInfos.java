package com.jsbsoft.jtf.datasource.pool;

import java.lang.management.ManagementFactory;

import javax.management.JMException;
import javax.management.MBeanServer;
import javax.management.ObjectName;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created on 07/04/15.
 */
public class PoolInfos {

    private static final Logger LOG = LoggerFactory.getLogger(PoolInfos.class);

    public static int getPoolSize() {
        final Integer poolSize = (Integer) getPoolProperty("maximumPoolSize");
        return poolSize == null ? -1 : poolSize;
    }

    public static int getActiveConnections() {
        final Integer activeConnections = (Integer) getPoolProperty("ActiveConnections");
        return activeConnections == null ? -1 : activeConnections;
    }

    public static int getIdleConnections() {
        final Integer idleConnections = (Integer) getPoolProperty("IdleConnections");
        return idleConnections == null ? -1 : idleConnections;
    }

    public static int getThreadsAwaitingConnection() {
        final Integer threadsAwaitingConnections = (Integer) getPoolProperty("ThreadsAwaitingConnection");
        return threadsAwaitingConnections == null ? -1 : threadsAwaitingConnections;
    }

    private static Object getPoolProperty(String property) {
        try {
            final MBeanServer mBeanServer = ManagementFactory.getPlatformMBeanServer();
            final ObjectName poolName = new ObjectName("com.zaxxer.hikari:type=Pool (springHikariCP)");
            return mBeanServer.getAttribute(poolName, property);
        } catch (JMException e) {
            LOG.error("unable to compute the pool property", e);
            return null;
        }
    }
}
