package com.jsbsoft.jtf.datasource.utils;

import java.lang.reflect.Method;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;

import com.jsbsoft.jtf.datasource.dao.CommonDAO;
import com.jsbsoft.jtf.datasource.naming.ColumnNamingStrategy;

/**
 * Classe utilitaire permettant de retrouver les champs d'une table via son nom et la {@link ColumnNamingStrategy} choisi.
 * Created on 22/09/15.
 */
public final class DaoUtils {

    private static final Logger LOG = LoggerFactory.getLogger(DaoUtils.class);

    private static final String VALUE_SEPARATOR = ", ";

    private static final String ADD_TEMPLATE = "INSERT INTO %s (%s) VALUES (%s)";

    private static final String UPDATE_TEMPLATE = "UPDATE %s SET %s WHERE ID_%s = :id";

    private DaoUtils() {
        // constructeur privé pour ne pas instancier cette classe utilitaire
    }

    /**
     * Permet de retrouver le bean gérer par le DAO fourni en paramètre.
     * @param dao le dao dont on souhaite connaitre le bean qu'il gère.
     * @param <T> le Type que l'on souhaite retrouver
     * @return la classe correspondant au bean souhaité ou null si non trouvé.
     */
    public static <T> Class<T> getGenericType(CommonDAO<T> dao) {
        try {
            final Method method = dao.getClass().getMethod("getById", Long.class);
            return (Class<T>) GenericTypeResolver.resolveReturnType(method, dao.getClass());
        } catch (final NoSuchMethodException e) {
            LOG.error(String.format("An error occured trying to determine generic type for dao \"%s\"", dao.getClass().getName()), e);
        }
        return null;
    }

    /**
     * Permet de construire une requête add pour la table fourni en paramètre.
     * @param tableName la table dont on souhaite créer une requête add
     * @param columns les colonnes de la table dont on souhaite rajouter une entrée
     * @param columnNamingStrategy la façon dont les namedParameters vont être setter
     * @return la requête add avec les namedParameter initialiser.
     */
    public static String getAddQuery(String tableName, Set<String> columns, ColumnNamingStrategy columnNamingStrategy) {
        final Set<String> columnsValue = new LinkedHashSet<>(columns);
        columnsValue.remove(columnNamingStrategy.getColumnId(tableName));
        return getAddWithForcedIdQuery(tableName, columnsValue, columnNamingStrategy);
    }

    /**
     * Permet de construire une requête add pour la table fourni en paramètre.
     * Contrairement à {@link #getAddQuery(String, Set, ColumnNamingStrategy)} ici, on ajoute aussi la valeur pour le champ id de la table.
     * @param tableName la table dont on souhaite créer une requête add
     * @param columns les colonnes de la table dont on souhaite rajouter une entrée
     * @param columnNamingStrategy la façon dont les namedParameters vont être setter
     * @return la requête add avec les namedParameter initialiser.
     */
    public static String getAddWithForcedIdQuery(String tableName, Set<String> columns, ColumnNamingStrategy columnNamingStrategy) {
        final Set<String> paramsValue = new LinkedHashSet<>();
        for (String currentColumn : columns) {
            paramsValue.add(columnNamingStrategy.getParamName(tableName, currentColumn));
        }
        return String.format(ADD_TEMPLATE, tableName, StringUtils.join(columns, VALUE_SEPARATOR), StringUtils.join(paramsValue, VALUE_SEPARATOR));
    }

    /**
     *
     * Permet de construire une requête update pour la table fourni en paramètre.
     * @param tableName la table dont on souhaite créer une requête update
     * @param columns les colonnes de la table dont on souhaite modifier une entrée
     * @param columnNamingStrategy la façon dont les namedParameters vont être setter
     * @return la requête SQL avec les namedParameter initialiser pour faire un update.
     */
    public static String getUpdateQuery(String tableName, Set<String> columns, ColumnNamingStrategy columnNamingStrategy) {
        final Set<String> setValue = new LinkedHashSet<>();
        for (String currentColumn : columns) {
            if (!StringUtils.equals(currentColumn, columnNamingStrategy.getColumnId(tableName))) {
                setValue.add(currentColumn + "=" + columnNamingStrategy.getParamName(tableName, currentColumn));
            }
        }
        return String.format(UPDATE_TEMPLATE, tableName, StringUtils.join(setValue, VALUE_SEPARATOR), tableName);
    }
}
