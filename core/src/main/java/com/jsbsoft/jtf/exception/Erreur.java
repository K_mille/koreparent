package com.jsbsoft.jtf.exception;

/**
 * This type was created in VisualAge.
 */
public class Erreur extends JsbException {

    /**
     *
     */
    private static final long serialVersionUID = 8492813180971656217L;

    /**
     * Instantiates a new erreur.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     */
    public Erreur(final int num, final String mes) {
        super(num, mes);
    }

    /**
     * Instantiates a new Erreur.
     *
     * @param mes
     *            Le message de l'exception
     * @param t
     *            La cause de l'exception
     */
    public Erreur(final String mes, final Throwable t) {
        super(mes, t);
    }

    /**
     * Instantiates a new erreur.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     * @param t
     *            the t
     */
    public Erreur(final int num, final String mes, final Throwable t) {
        super(num, mes, t);
    }

    /**
     * Instantiates a new erreur.
     *
     * @param mes
     *            the mes
     */
    public Erreur(final String mes) {
        super(mes);
    }
}