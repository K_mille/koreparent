package com.jsbsoft.jtf.exception;

/**
 * This type was created in VisualAge.
 */
public class ErreurApplicative extends Erreur {

    /**
     *
     */
    private static final long serialVersionUID = -3638997188837466353L;

    /**
     * Instantiates a new erreur applicative.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     */
    public ErreurApplicative(final int num, final String mes) {
        super(num, mes);
    }

    /**
     * Instantiates a new ErreurApplicative.
     *
     * @param mes
     *            Le message de l'exception
     * @param t
     *            La cause de l'exception
     */
    public ErreurApplicative(final String mes, final Throwable t) {
        super(mes, t);
    }

    /**
     * Instantiates a new erreur applicative.
     *
     * @param mes
     *            the mes
     */
    public ErreurApplicative(final String mes) {
        super(mes);
    }
}