package com.jsbsoft.jtf.exception;

/**
 * Type d'exception devant être traiter par tomcat et pas catcher par l'application
 *
 * @author olivier.camon
 *
 */
public class ErreurAsyncException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 7772536010738837953L;

    public ErreurAsyncException(final String mes) {
        super(mes);
    }

    public ErreurAsyncException(final String mes, final Throwable t) {
        super(mes, t);
    }
}
