package com.jsbsoft.jtf.exception;

public class ErreurBeanNonTrouve extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -8238707337929563823L;

    public ErreurBeanNonTrouve(final String mes) {
        super(mes);
    }
}
