package com.jsbsoft.jtf.exception;

/**
 *
 * Exception levée lorsqu'une données à récupérée n'est pas trouvée. <br/>
 * <br/>
 * <strong>REMARQUE : </strong> permet d'éviter de revoyer des valeurs <code>null</code> et de créer de possible {@link NullPointerException} dans le reste de l'application. Avec
 * cette exception, l'anomalie remonte jusqu'à temps qu'elle soit gérée. De plus, héritant de {@link ErreurApplicative} elle est gérée par le produit.
 *
 * @author pierre.cosson
 *
 */
public class ErreurDonneeNonTrouve extends ErreurApplicative {

    /**
     * ID
     */
    private static final long serialVersionUID = -4192935337733417145L;

    public ErreurDonneeNonTrouve(int num, String mes) {
        super(num, mes);
    }

    public ErreurDonneeNonTrouve(String mes) {
        super(mes);
    }
}
