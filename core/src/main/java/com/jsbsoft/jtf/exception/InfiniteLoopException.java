package com.jsbsoft.jtf.exception;

/**
 * Exception utilisée pour signaler une boucle infinie dans les structures parent/enfant (garde-fou).
 *
 */
public class InfiniteLoopException extends RuntimeException {

    private static final long serialVersionUID = -3229336286806930761L;

    public InfiniteLoopException(String message) {
        super(message);
    }

    public InfiniteLoopException(String message, Throwable t) {
        super(message, t);
    }
}
