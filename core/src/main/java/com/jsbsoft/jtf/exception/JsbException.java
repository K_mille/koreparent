package com.jsbsoft.jtf.exception;

/**
 * This type was created in VisualAge.
 */
public class JsbException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 8251246964936754932L;

    /**
     * JsbException constructor comment.
     */
    public JsbException() {
        super();
    }

    /**
     * JsbException constructor comment.
     *
     * @param num
     *            the num
     */
    public JsbException(int num) {
        super("[" + num + "]");
    }

    /**
     * JsbException constructor comment.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     */
    public JsbException(int num, String mes) {
        super("[" + num + "] : " + mes);
    }

    /**
     * Instantiates a new jsb exception.
     *
     * @param num
     *            the num
     * @param mes
     *            the mes
     * @param t
     *            the t
     */
    public JsbException(int num, String mes, Throwable t) {
        super("[" + num + "] : " + mes, t);
    }

    /**
     * Instantiates a new jsb exception.
     *
     * @param mes
     *            the mes
     * @param cause
     *            the cause
     */
    public JsbException(String mes, Throwable cause) {
        super(mes, cause);
    }

    /**
     * Instantiates a new jsb exception.
     *
     * @param cause
     *            the cause
     */
    public JsbException(Throwable cause) {
        super(cause);
    }

    /**
     * Instantiates a new jsb exception.
     *
     * @param mes
     *            the mes
     */
    public JsbException(String mes) {
        super(mes);
    }

    /* (non-Javadoc)
     * @see java.lang.Throwable#toString()
     */
    @Override
    public String toString() {
        return getMessage();
    }
}