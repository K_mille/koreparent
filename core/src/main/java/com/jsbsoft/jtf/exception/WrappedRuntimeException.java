package com.jsbsoft.jtf.exception;

/**
 * Exception utilisée pour wrapper en exception runtime les "checked" exceptions ne correspondant pas à des cas métiers.
 *
 */
public class WrappedRuntimeException extends RuntimeException {

    /**
     *
     */
    private static final long serialVersionUID = 7914355727162012888L;

    public WrappedRuntimeException(String message, Throwable t) {
        super(message, t);
    }
}
