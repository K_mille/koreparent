package com.jsbsoft.jtf.identification;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.lang3.StringUtils;
import org.jasig.cas.client.validation.AbstractCasProtocolUrlBasedTicketValidator;
import org.jasig.cas.client.validation.Assertion;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

/**
 * Prend en charge la validation de ticket aupres de CAS.
 *
 * @author jbiard
 */
public class ValidateurCAS implements ISourceAuth {

    /** The Constant SOURCE_LIBELLE_CAS. */
    public static final String SOURCE_LIBELLE_CAS = "cas";

    /** The Constant SOURCE_LIBELLE_AFFICHABLE. */
    private static final String SOURCE_LIBELLE_AFFICHABLE = "Central Authentication Server (CAS)";
    // JB 20050601 libelle de la source d'auth

    /** Mode CAS supporté */
    private static final String DEFAULT_CAS_MODE_BASIC = "basic";


    private static final String PARAM_TICKET_VALIDATOR = "cas.ticketValidator.classe";

    /** The Constant PARAM_JTF_CAS_HOTE. */
    private static final String PARAM_JTF_CAS_HOTE = "cas.hote";

    /** The Constant PARAM_JTF_CAS_URI_LOGIN. */
    private static final String PARAM_JTF_CAS_URI_LOGIN = "cas.uriLogin";

    /** The Constant PARAM_JTF_CAS_URI_LOGOUT. */
    private static final String PARAM_JTF_CAS_URI_LOGOUT = "cas.uriLogout";

    private static final Logger LOG = LoggerFactory.getLogger(ValidateurCAS.class);

    private static final String HTTPS = "https://";

    private static final String PARAM_JTF_CAS_REDIRECTION_FRONT = "cas.redirection_front";

    /** The url service cas login. */
    private String urlCasValidate;
    private String urlServiceCasLogin;

    private String ticketValidatorClass = null;

    /**
     * Constructeur non visible en dehors du package.
     *
     */
    ValidateurCAS() {
        initialisationURLsCas();
    }

    /**
     * Gets the url cas service login.
     *
     * @param urlService
     *            the url service
     *
     * @return the url cas service login
     *
     * @throws UnsupportedEncodingException
     *             the unsupported encoding exception
     */
    public String getUrlCasServiceLogin(String urlService) throws UnsupportedEncodingException {
        return urlServiceCasLogin + URLEncoder.encode(urlService, CharEncoding.DEFAULT);
    }

    /**
     * Gets the url cas login.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas login
     */
    public String getUrlCasLogin(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogin(ctx.getInfosSite(), false);
    }

    /**
     * Gets the url cas login front.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas login front
     */
    public String getUrlCasLoginFront(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogin(ctx.getInfosSite(), true);
    }

    /**
     * Gets the url cas logout.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas logout
     */
    public String getUrlCasLogout(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogout(ctx.getInfosSite(), "1".equals(PropertyHelper.getCoreProperty(PARAM_JTF_CAS_REDIRECTION_FRONT)));
    }

    /**
     * Gets the url cas logout front.
     *
     * @param ctx
     *            the ctx
     *
     * @return the url cas logout front
     */
    public String getUrlCasLogoutFront(OMContext ctx) {
        initialiserInfosSiteDansContexte(ctx);
        return getURLCasLogout(ctx.getInfosSite(), true);
    }

    /**
     * Jamais utilisé : l'authentification se fait sur le serveur CAS.
     *
     * @param codeUtilisateur
     *            the code utilisateur
     * @param mdp
     *            the mdp
     * @param ctx
     *            the ctx
     *
     * @return true, if connecte
     *
     */
    @Override
    public boolean connecte(String codeUtilisateur, String mdp, OMContext ctx) {
        return false;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.jsbsoft.jtf.identification.ISourceAuth#getCodeSource()
     */
    @Override
    public String getCodeSource() {
        return SOURCE_LIBELLE_CAS;
    }

    /**
     * Gets the libelle source.
     *
     * @return the libelle source
     *
     * @see ISourceAuth
     */
    @Override
    public String getLibelleSource() {
        return SOURCE_LIBELLE_AFFICHABLE;
    }

    private void initialisationURLsCas() {

        ticketValidatorClass = PropertyHelper.getCoreProperty(PARAM_TICKET_VALIDATOR);

        final String hoteCasPort = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE);
        // url de validation de service (basic et proxy)
        urlCasValidate = HTTPS + hoteCasPort;
        // url de login pour un service CASsifie appele depuis le portail
        final String uriCasLogin = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_LOGIN);
        urlServiceCasLogin = HTTPS + hoteCasPort + uriCasLogin;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.jsbsoft.jtf.identification.ISourceAuth#parametrageCharge()
     */
    @Override
    public String parametrageCharge() {
        return "| Mode CAS : " + DEFAULT_CAS_MODE_BASIC + "\n";
    }

    /**
     * Lance la validation du ticket CAS.
     *
     * @param ctx
     *            the ctx
     * @param infoBean
     *            the info bean
     *
     * @return un utilisateurBean si la validation du ticket s'est faite correctement
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean validation(final OMContext ctx, final InfoBean infoBean) throws Exception {
        boolean bIdentificationBack = true;
        // on identifie de quel côté on souhaite se connecter
        if ("IDENTIFICATION_FRONT".equals(infoBean.getNomProcessus())) {
            bIdentificationBack = false;
        }
        String urlService = getURLCasAuthentification(ctx.getInfosSite(), !bIdentificationBack);
        return validationModeBasic(ctx, urlService, infoBean.getString("ticket"), bIdentificationBack);
    }

    /**
     * Lance la validation du ticket CAS pour une url de service donnée.
     *
     * @param ctx
     *            the ctx
     * @param ticket
     *            the ticket
     * @param urlService
     *            L'URL du service demandant la validation
     * @param bIdentificationBack
     *            flag sur une connexion front/back
     *
     * @return un utilisateurBean si la validation du ticket s'est faite correctement
     *
     * @throws Exception
     *             the exception
     */
    public UtilisateurBean validation(final OMContext ctx, final String urlService, final String ticket, final boolean bIdentificationBack) throws Exception {
        return validationModeBasic(ctx, urlService, ticket, bIdentificationBack);
    }

    /**
     * Instancie un validateurCAS du type définit dans le CAS.Properties
     *
     * @return un validateur CAS ou une exception
     *
     * @throws ErreurApplicative
     *              En cas d'erreur de configuration du service CAS
     */
    private AbstractCasProtocolUrlBasedTicketValidator getTicketValidator()  throws ErreurApplicative {
        AbstractCasProtocolUrlBasedTicketValidator ticketValidator;
        try {
            if (ticketValidatorClass != null) {
                final Object o = Class.forName(ticketValidatorClass).getDeclaredConstructor(String.class).newInstance(urlCasValidate);
                if (o instanceof AbstractCasProtocolUrlBasedTicketValidator) {
                    ticketValidator = (AbstractCasProtocolUrlBasedTicketValidator) o;
                    LOG.debug("Instanciation réussie d'un validateur de type [{}]",ticketValidatorClass);
                } else {
                    LOG.debug("La classe [{}] n'est pas une instance de AbstractCasProtocolUrlBasedTicketValidator",ticketValidatorClass);
                    throw new ErreurApplicative("Erreur de configuration du connecteur CAS");
                }
            } else {
                LOG.error("Impossible d'instancier le validateur CAS : clé [{}] non définie",PARAM_TICKET_VALIDATOR);
                throw new ErreurApplicative("Erreur de configuration du connecteur CAS");
            }
        } catch (final ReflectiveOperationException e) {
            LOG.error("Impossible d'instancier le validateur CAS de type [{}]",ticketValidatorClass, e);
            throw new ErreurApplicative("Erreur de configuration du connecteur CAS");
        }
        return ticketValidator;
    }

    /**
     * Prend en charge la validation du ticket CAS en mode simple.
     *
     * @param ctx
     *            contexte
     * @param ticket
     *            ticket CAS à valider (ST ou PT)
     * @param bIdentificationBack
     *            indique si l'identification est faite en back ou non
     *
     * @return utilisateur loggue
     *
     * @throws Exception
     *             levee si l'utilisateur est incorrect ou s'il y a eu echec lors de la connexion vers le serveur
     */
    protected UtilisateurBean validationModeBasic(OMContext ctx, String urlService, String ticket, boolean bIdentificationBack) throws Exception {
        if (StringUtils.isEmpty(ticket)) {
            return null;
        }

        // prend en charge la validation du ticket aupres de CAS
        final AbstractCasProtocolUrlBasedTicketValidator sv = getTicketValidator();

        // url de notre servlet (ici)
        initialiserInfosSiteDansContexte(ctx);
        Assertion result = sv.validate(ticket, urlService );
        return chargeUtilisateur(ctx, result, bIdentificationBack);
    }

    /**
     * Charge l'utilisateur K-Portal.
     *
     * @param ctx
     *            contexte
     * @param casResponse
     *            reponse CAS
     * @param bIdenticationBack
     *            indique si on est en back
     *
     * @return l'utilisateur loggue
     *
     * @throws Exception
     *             levee si l'utilisateur est incorrect ou s'il y a eu echec lors de la connexion vers le serveur
     */
    private UtilisateurBean chargeUtilisateur(OMContext ctx, Assertion casResponse, boolean bIdenticationBack) throws Exception {
        UtilisateurBean utilisateur;
        // on recupere la reponse
        String codeUtilisateur = casResponse.getPrincipal().getName();
        utilisateur = GestionnaireIdentification.getInstance().estUtilisateurKportal(codeUtilisateur, "", ctx);
        // l'utilisateur n'a pas ete trouve dans notre BD
        if (utilisateur == null) {
            // on peut etre connecte sur CAS, sans que le user soit dans
            // la base KPortal
            String messageException = MessageHelper.getCoreMessage(ctx.getLocale(), "ST_CAS_ERREUR_MSG1") + " " + codeUtilisateur + " " + MessageHelper.getCoreMessage(ctx.getLocale(), "ST_CAS_ERREUR_MSG2") + "<br />" + MessageHelper.getCoreMessage(ctx.getLocale(), "ST_CAS_ERREUR_MSG3");
            initialiserInfosSiteDansContexte(ctx);
            final String urlCasLogout = getURLCasLogout(ctx.getInfosSite(), !bIdenticationBack);
            if (!bIdenticationBack) {
                messageException += "<br /><a href=\"" + urlCasLogout + "\">Veuillez vous déconnecter de CAS.</a>";
            }
            throw new ErreurApplicative(messageException);
        }
        return utilisateur;
    }

    /**
     * Initialiser le {@link ContexteUniv} avec un {@link InfosSite}. Eviter les {@link NullPointerException}. Si le contexte ne contient aucun site c'est le site principal qui
     * sera forcé.
     *
     * @param ctx
     *            Le contexte à initialiser.
     */
    private void initialiserInfosSiteDansContexte(OMContext ctx) {
        if (ctx.getInfosSite() == null) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            ctx.setInfosSite(serviceInfosSite.getPrincipalSite());
        }
    }

	private String getURLCasAuthentification(InfosSite infosSite, boolean front) {
		if (front){
			return URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT", infosSite);
		}else{
			return URLResolver.getAbsoluteBoUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION", infosSite);
		}
	}

    private String getURLCasLogin(InfosSite infosSite, boolean front) {
        final String uriCasLogin = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_LOGIN);
        final String hoteCasPort = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE);
        final String urlCasAuth = getURLCasAuthentification(infosSite, front);
        try {
            return HTTPS + hoteCasPort + uriCasLogin + URLEncoder.encode(urlCasAuth, CharEncoding.DEFAULT);
        } catch (UnsupportedEncodingException e) {
            LOG.error("Erreur durant l'encodage de l'URL CAS de login" + (front ? " front" : ""), e);
            return StringUtils.EMPTY;
        }
    }

    private String getURLCasLogout(InfosSite infosSite, boolean front) {
        final String uriCasLogoutCallback = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_URI_LOGOUT);
        if (StringUtils.isEmpty(uriCasLogoutCallback)) {
            return StringUtils.EMPTY;
        } else {
            final String hoteCasPort = PropertyHelper.getCoreProperty(PARAM_JTF_CAS_HOTE);
            try {
                String url = URLEncoder.encode(URLResolver.getAbsoluteBoUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION&ACTION=DECONNECTER", infosSite), CharEncoding.DEFAULT);
                if (front) {
                    url = URLEncoder.encode(URLResolver.getAbsoluteUrl(WebAppUtil.SG_PATH + "?PROC=IDENTIFICATION_FRONT&ACTION=DECONNECTER", infosSite), CharEncoding.DEFAULT);
                }
                return HTTPS + hoteCasPort + uriCasLogoutCallback + url;
            } catch (UnsupportedEncodingException e) {
                LOG.error("Erreur durant l'encodage de l'URL CAS de logout" + (front ? " front" : ""), e);
                return StringUtils.EMPTY;
            }
        }
    }
}
