package com.jsbsoft.jtf.lang;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class CharEncoding {

    public static final String DEFAULT = StandardCharsets.UTF_8.name();

    public static final Charset DEFAULT_CHARSET = Charset.forName(DEFAULT);
}
