package com.jsbsoft.jtf.ldap;

import javax.naming.NamingException;

/**
 * The Interface IConnexionLdap.
 */
public interface IConnexionLdap {

    /**
     * Gets the unique id.
     *
     * @param valueAttributeLogin
     *            the value attribute login
     * @param nameAttributeLogin
     *            the name attribute login
     * @param nameAnnuaireAttribute
     *            the name annuaire attribute
     * @param nameEtuAttribute
     *            the name etu attribute
     * @param critereEtudiant
     *            the critere etudiant
     * @param valueCritereEtudiant
     *            the value critere etudiant
     *
     * @return the unique id
     *
     * @throws NamingException
     *             the naming exception
     * @throws NullPointerException
     *             the null pointer exception
     * @throws Exception
     *             the exception
     */
    String getUniqueId(String valueAttributeLogin, String nameAttributeLogin, String nameAnnuaireAttribute, String nameEtuAttribute, String critereEtudiant, String valueCritereEtudiant) throws NamingException, NullPointerException, Exception;

    /**
     * Gets the liste alias.
     *
     * @return the liste alias
     */
    String[] getListeAlias();

    /**
     * Release context.
     *
     * @throws NamingException
     *             the naming exception
     * @throws Exception
     *             the exception
     */
    void releaseContext() throws NamingException, Exception;
}
