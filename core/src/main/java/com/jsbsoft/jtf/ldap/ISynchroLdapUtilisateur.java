package com.jsbsoft.jtf.ldap;

import java.lang.reflect.InvocationTargetException;
import java.util.Hashtable;

import javax.naming.NamingException;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.bean.UtilisateurBean;

/**
 * The Interface ISynchroLdapUtilisateur.
 */
public interface ISynchroLdapUtilisateur {

    /** The SYNCHR o_ lda p_ class e_ libell e_ jtf. */
    String SYNCHRO_LDAP_CLASSE_LIBELLE_JTF = "synchroniseur_ldap.classe";

    /**
     * Synchronise utilisateur.
     *
     * @param typeSynchro
     *            the type synchro
     * @param utilisateur
     *            the utilisateur
     * @param ctx
     *            the ctx
     *
     * @throws IllegalArgumentException
     *             the illegal argument exception
     * @throws NamingException
     *             the naming exception
     * @throws IllegalAccessException
     *             the illegal access exception
     * @throws InvocationTargetException
     *             the invocation target exception
     * @throws Exception
     *             the exception
     */
    void synchroniseUtilisateur(int typeSynchro, UtilisateurBean utilisateur, OMContext ctx) throws IllegalArgumentException, NamingException, IllegalAccessException, InvocationTargetException, Exception;

    /**
     * Gets the all user from ldap.
     *
     * @return the all user from ldap
     *
     * @throws Exception
     *             the exception
     */
    Hashtable getAllUserFromLdap() throws Exception;

    /**
     * Retrieve code ldap professeur.
     *
     * @param _CodeNumen
     *            the _ code numen
     * @param _codeLdap
     *            the _code ldap
     *
     * @return the string
     *
     * @throws NamingException
     *             the naming exception
     * @throws Exception
     *             the exception
     */
    String retrieveCodeLdapProfesseur(String _CodeNumen, String _codeLdap) throws NamingException, Exception;

    /**
     * Checks if is unique.
     *
     * @param _uid
     *            the _uid
     *
     * @return true, if is unique
     *
     * @throws Exception
     *             the exception
     */
    boolean isUnique(String _uid) throws Exception;
}
