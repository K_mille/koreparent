package com.jsbsoft.jtf.session;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.servlet.http.HttpSession;

import com.jsbsoft.jtf.core.ProcedureBean;

/**
 * Session utilisateur qui permet d'accéder à toutes ses données Seul objet placé dans la session http.
 */
public class SessionUtilisateur {

    /** clé utilisée pour indexer l'objet Session dans une session HTTP. */
    public static final String CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP = "session";
    //liste des données stockées dans la session (table Infos)

    /** The Constant LISTE_COMPOSANTS_INFORMATIONS. */
    public static final String LISTE_COMPOSANTS_INFORMATIONS = "LISTE_COMPOSANTS_INFORMATIONS";

    /** The Constant CODE. */
    public static final String CODE = "CODE";

    /** The Constant CODE_GESTION. */
    public static final String CODE_GESTION = "CODE_GESTION";

    /** The Constant AUTORISATIONS. */
    public static final String AUTORISATIONS = "AUTORISATIONS";

    /** The Constant NOM. */
    public static final String NOM = "NOM";

    /** The Constant PRENOM. */
    public static final String PRENOM = "PRENOM";

    /** The Constant CODE_STRUCTURE. */
    public static final String CODE_STRUCTURE = "CODE_STRUCTURE";

    /** The Constant ADRESSE_MAIL. */
    public static final String ADRESSE_MAIL = "ADRESSE_MAIL";

    /** The Constant CENTRES_INTERET. */
    public static final String CENTRES_INTERET = "CENTRES_INTERET";

    /** The Constant GROUPES_DSI. */
    public static final String GROUPES_DSI = "GROUPES_DSI";

    /** The Constant PROFILS_DSI. */
    public static final String PROFILS_DSI = "PROFILS_DSI";

    /** The Constant PROFIL_DSI. */
    public static final String PROFIL_DSI = "PROFIL_DSI";

    /** The Constant KSESSION. */
    public static final String KSESSION = "KSESSION";

    /** The Constant TIMESTAMP. */
    public static final String TIMESTAMP = "TIMESTAMP";

    /** The Constant CODE_PAGE_ACCUEIL. */
    public static final String CODE_PAGE_ACCUEIL = "CODE_PAGE_ACCUEIL";

    /** The Constant LANGUE_PAGE_ACCUEIL. */
    public static final String LANGUE_PAGE_ACCUEIL = "LANGUE_PAGE_ACCUEIL";

    /** The Constant DATE_DERNIERE_SESSION. */
    public static final String DATE_DERNIERE_SESSION = "DATE_DERNIERE_SESSION";

    /** The Constant ESPACE. */
    public static final String ESPACE = "ESPACE";

    /** Nb de resultats affichés par page. */
    public static final String NB_RESULTATS_PAGE = "NB_RESULTATS_PAGE";

    /** Clé utilisées pour retrouver les préférences utilisateurs dans la table contenue dans l'instance de la session utilisateur. */
    public final static String CLE_USER_PREFS = "USER_PREFS";

    public static final String RUBRIQUE_ACCUEIL = "RUBRIQUE_ACCUEIL";

    /** sous-session qui n'est liée à aucun partenaire. */
    private final SessionParDefaut sessionparDefaut = new SessionParDefaut(this);

    /** The infos. */
    private final Map<String, Object> infos = new HashMap<>();

    /** The http session. */
    private HttpSession httpSession = null;

    /** The locale. */
    private Locale locale = Locale.FRANCE;

    /**
     * Le nom du dernier processus appelé durant cette session
     */
    private String nomDernierProcessusAppele;

    /**
     * Renvoie la sous-session qui n'est rattachée à aucun partenaire.
     *
     * @param _httpSession
     *            the _http session
     */
    public SessionUtilisateur(final HttpSession _httpSession) {
        super();
        setHttpSession(_httpSession);
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (21/05/01 10:32:26)
     *
     * @return javax.servlet.http.HttpSession
     */
    public HttpSession getHttpSession() {
        return httpSession;
    }

    /**
     * Renvoie la sous-session par défaut.
     *
     * @return the session par defaut
     */
    public SousSession getSessionParDefaut() {
        return sessionparDefaut;
    }

    /**
     * retrouve un bean de la classe specifiee, quelle que soit la sous-session Date de création : (27/01/00 15:09:06).
     *
     * @param idBean
     *            the id bean
     *
     * @return com.jsbsoft.jtf.session.ProcedureBean
     */
    public ProcedureBean retrouverProcedureBeanparID(final String idBean) {
        final ProcedureBean bean = this.getSessionParDefaut().retrouverProcedureBeanparID(idBean);
        return bean;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (21/05/01 10:32:26)
     *
     * @param newHttpSession
     *            javax.servlet.http.HttpSession
     */
    private void setHttpSession(final HttpSession newHttpSession) {
        httpSession = newHttpSession;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (24/09/2001 09:11:47)
     *
     * @return java.lang.String
     */
    public Map<String, Object> getInfos() {
        return infos;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (24/09/2001 09:12:51)
     *
     * @return java.util.Locale
     */
    public Locale getLocale() {
        return locale;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (24/09/2001 09:12:51)
     *
     * @param newLocale
     *            java.util.Locale
     */
    public void setLocale(final Locale newLocale) {
        locale = newLocale;
    }

    /**
     * Retourne le nom du dernier processus appelé durant cette session
     *
     * @return the nomDernierProcessusAppele
     */
    public String getNomDernierProcessusAppele() {
        return nomDernierProcessusAppele;
    }

    /**
     * Affecte le nom du dernier processus appelé durant cette session
     *
     * @param nomDernierProcessusAppele
     *            the nomDernierProcessusAppele to set
     */
    public void setNomDernierProcessusAppele(final String nomDernierProcessusAppele) {
        this.nomDernierProcessusAppele = nomDernierProcessusAppele;
    }
}
