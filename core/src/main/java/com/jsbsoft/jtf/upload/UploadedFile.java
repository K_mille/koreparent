package com.jsbsoft.jtf.upload;

/*
    GnuJSP Toolkit - useful classes for JSP and servlet development
    Copyright (C) 1998, Vincent Partington <vincentp@xs4all.nl>

    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    as published by the Free Software Foundation; either version 2
    of the License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;

/**
 * The UploadedFile class represents files uploaded with a multipart/form-data form. The files are stored either in memory or on disk. UploadedFile objects are returned by
 * ExtendedRequest.getFileParameter().
 *
 * @see ExtendedRequest#getFileParameter
 * @see ExtendedRequest#getFileParameterValues
 */
public class UploadedFile {

    /** The Constant PARAM_JTF_EXCEPTION_FILE_UPLOAD. */
    public final static String PARAM_JTF_EXCEPTION_FILE_UPLOAD = "EXCEPTION_FILE_UPLOAD";

    /** The Constant KEY_MAX_FILE_SIZE. */
    public final static String KEY_MAX_FILE_SIZE = "MAX_FILE_SIZE";

    /** The Constant KEY_FILE_EXTENSIONS. */
    public final static String KEY_FILE_EXTENSIONS = "KEY_FILE_EXTENSIONS";

    /** The Constant MAX_SIZE. */
    public final static long MAX_SIZE = 2000000;

    /** The Constant EXCEPTION_FILE_UPLOAD_FICHIER_INEXISTANT. */
    public final static String EXCEPTION_FILE_UPLOAD_FICHIER_INEXISTANT = "1";

    /** The Constant EXCEPTION_FILE_UPLOAD_TAILLE_FICHIER. */
    public final static String EXCEPTION_FILE_UPLOAD_TAILLE_FICHIER = "2";

    /** The content filename. */
    String contentType, contentFilename;

    /** The content length. */
    int contentLength;

    /** The file. */
    File file;

    /** The file has been deleted. */
    boolean fileHasBeenDeleted = false;

    /**
     * Instantiates a new uploaded file.
     *
     * @param contentType
     *            the content type
     * @param contentLength
     *            the content length
     * @param contentFilename
     *            the content filename
     * @param file
     *            the file
     */
    public UploadedFile(String contentType, int contentLength, String contentFilename, File file) {
        this.contentType = contentType;
        this.contentLength = contentLength;
        this.contentFilename = contentFilename;
        this.file = file;
    }

    /**
     * Deletes any temporary file assoinfoBeanated with this uploaded file. When called a second time or for an uploaded file whose contents are stored in memory, nothing is done.
     */
    public void deleteTemporaryFile() {
        if (file != null && !fileHasBeenDeleted) {
            fileHasBeenDeleted = true;
            file.delete();
        }
    }

    /**
     * Returns the client-side filename of the uploaded file or <CODE>null</CODE> if unknown. If the filename contains backslashes or forward slashes, only the part after the last
     * slash is returned.
     *
     * @return the useful part of the client-side filename
     */
    public String getContentFilename() {
        if (contentFilename != null) {
            int i;
            i = contentFilename.lastIndexOf('\\');
            if (i != -1) {
                return contentFilename.substring(i + 1);
            }
            i = contentFilename.lastIndexOf('/');
            if (i != -1) {
                return contentFilename.substring(i + 1);
            }
        }
        return contentFilename;
    }

    /**
     * Returns the full client-side filename of the uploaded file as it was sent to the server or <CODE>null</CODE> if unknown.
     *
     * @return the full client-side filename
     */
    public String getContentFullFilename() {
        return contentFilename;
    }

    /**
     * Returns the length of the file. This is the actual length, not the value of a header.
     *
     * @return the length
     */
    public int getContentLength() {
        return contentLength;
    }

    /**
     * Returns the content-type of the file or <CODE>null</CODE> if unknown. Some browsers don't send content-types for plain text files.
     *
     * @return the content-type
     */
    public String getContentType() {
        return contentType;
    }

    /**
     * Creates an <CODE>InputStream</CODE> that can be used to read the contents of the uploaded file.
     *
     * @return an <CODE>InputStream</CODE> for the contents
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public InputStream getInputStream() throws IOException {
        return new FileInputStream(file);
    }

    /**
     * Creates a <CODE>Reader</CODE> that can be used to read the contents of the uploaded file.
     *
     * @return a <CODE>Reader</CODE> for the contents
     *
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    public Reader getReader() throws IOException {
        return new FileReader(file);
    }

    /**
     * Returns a handle to the temporary file that stores the uploaded file. This file may disappear when ExtendedRequest.deleteTemporaryFiles() or
     * UploadedFile.deleteTemporaryFile() is called, so use with care.
     *
     * @return the temporary file
     *
     * @exception java.lang.IllegalStateException
     *                when the contents have been saved in memory.
     */
    public File getTemporaryFile() {
        if (file == null) {
            throw new IllegalStateException("no temporary file");
        }
        return file;
    }

    /**
     * Returns the full client-side filename of the uploaded file.
     *
     * @return the full client-side filename of the uploaded file
     */
    @Override
    public String toString() {
        return contentFilename;
    }
}
