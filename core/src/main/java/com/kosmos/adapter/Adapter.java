package com.kosmos.adapter;

/**
 * Created on 31/12/14.
 */
public interface Adapter<T> {

    <E> E retrieveData(T dataSource, Class<E> clazz);
}
