package com.kosmos.adapter.converter;

import org.apache.commons.beanutils.ConvertUtilsBean;
import org.apache.commons.beanutils.Converter;

/**
 * Converter permettant de mapper une Enum via une String dans l'adapter
 */
public class EnumConvertUtilsBean extends ConvertUtilsBean {

    private final EnumConverter enumConverter = new EnumConverter();

    public Converter lookup(Class clazz) {
        final Converter converter = super.lookup(clazz);
        if (converter == null && clazz.isEnum()) {
            return enumConverter;
        } else {
            return converter;
        }
    }

    private static class EnumConverter implements Converter {

        public Object convert(Class type, Object value) {
            return Enum.valueOf(type, (String) value);
        }
    }
}
