package com.kosmos.cache;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.cache.ehcache.EhCacheCacheManager;

import net.sf.ehcache.Cache;
import net.sf.ehcache.config.CacheConfiguration;
import net.sf.ehcache.config.PersistenceConfiguration;

public class CacheConfigurator implements InitializingBean {

    private EhCacheCacheManager cacheManager;

    private static CacheConfiguration defaultCacheConfiguration;
    private CacheConfiguration cacheConfiguration;

    private String cacheName;
    private boolean disabled = false;
    private PersistenceConfiguration persistenceConfiguration;

    @Override
    public void afterPropertiesSet() throws Exception {

        if ( cacheName!=null ) {
            //Remove cache from the native cache manager
            if (cacheManager.getCacheManager().cacheExists(cacheName)) {
                cacheManager.getCacheManager().removeCache(cacheName);
            }
            //Update cache configuration
            if ( cacheConfiguration==null  ) {
                cacheConfiguration = defaultCacheConfiguration.clone();
            }
            if ( persistenceConfiguration!=null ) {
                cacheConfiguration.addPersistence(persistenceConfiguration);
            }
            cacheConfiguration.setName(cacheName);
            Cache cache = new Cache(cacheConfiguration);
            cache.setDisabled(disabled);

            //Add cache to the native cache manager
            cacheManager.getCacheManager().addCache(cache);

            //Force spring cache manager to update his local cacheMap
            cacheManager.getCache(cacheName);
        }
    }

    public void setDefaultCacheConfiguration(final CacheConfiguration defaultCacheConfiguration) {
        CacheConfigurator.defaultCacheConfiguration = defaultCacheConfiguration;
    }

    public void setCacheName(final String cacheName) {
        this.cacheName = cacheName;
    }

    public void setCacheConfiguration(final CacheConfiguration configuration) {
        this.cacheConfiguration = configuration;
    }

    public void setCacheManager(final EhCacheCacheManager cacheManager) {
        this.cacheManager = cacheManager;
    }

    public void setDisabled(final boolean disabled) {
        this.disabled = disabled;
    }

    public void setPersistenceConfiguration(final PersistenceConfiguration persistenceConfiguration) {
        this.persistenceConfiguration = persistenceConfiguration;
    }
}
