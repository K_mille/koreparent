package com.kosmos.components.i18n;

import java.util.Locale;

import javax.servlet.ServletRequest;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.jsbsoft.jtf.core.LangueUtil;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

import fr.kosmos.core.i18n.LocaleManager;

/**
 * Classe utile pour KFC afin de récupérer la locale dans les tags pour l'I18N.
 */
@Component
@Primary
public class KSupLocaleManager implements LocaleManager {

    /**
     * Méthode permettant de déterminer la locale de la requête courante. Sur K-Sup, on la récupère depuis le contexte.
     * @param servletRequest la requête courante (sur kfc la locale est récupérer depuis la request)
     * @return la locale correspondant à la page.
     */
    @Override
    public Locale getLocale(final ServletRequest servletRequest) {
        Locale result = LangueUtil.getDefaultLocale();
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx != null) {
            result = ctx.getLocale();
        }
        return result;
    }
}
