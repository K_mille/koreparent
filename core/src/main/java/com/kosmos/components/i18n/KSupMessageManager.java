package com.kosmos.components.i18n;

import java.util.Locale;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import com.kportal.core.config.MessageHelper;

import fr.kosmos.core.i18n.MessageManager;

/**
 * Classe permettant de récupérer les messages dans les tags de kfc
 */
@Component
@Primary
public class KSupMessageManager implements MessageManager {

    /**
     * Permet de récupérer un message depuis les properties via les paramètres fournis
     * @param locale la locale dans laquelle on veut le message
     * @param key la clé du message
     * @param extension la potentielle extension dans lequel il se trouve
     * @param number un nombre potentiel (à la base, pour gérer les pluriels)
     * @param args des arguments potentiels à la chaine, ils seront traités via un {@link String#format(Locale, String, Object...)}
     * @return le label configuré ou la clé si non trouvé
     */
    @Override
    public String format(Locale locale, String key, String extension, Integer number, Object... args) {
        // Résultat par défaut : la clé
        String result = key;
        if (StringUtils.isNotEmpty(key)) {
            result = MessageHelper.getMessage(extension, locale, key);
            if (ArrayUtils.isNotEmpty(args)) {
                result = String.format(locale, result, args);
            }
        }
        return result;
    }
}
