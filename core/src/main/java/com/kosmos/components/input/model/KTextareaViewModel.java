package com.kosmos.components.input.model;

import com.kosmos.components.view.model.ComponentViewModel;

/**
 * Created by alexandre.baillif on 29/12/16.
 */
public class KTextareaViewModel extends ComponentViewModel {

	private String value;

	private String formatValue;

	private int max;

	private int cols;

	private int rows;

	private boolean required;

	private int editOption;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFormatValue() {
		return formatValue;
	}

	public void setFormatValue(String formatValue) {
		this.formatValue = formatValue;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}


	public boolean isRequired() {
		return required;
	}

	public void setRequired(boolean required) {
		this.required = required;
	}

	public int getEditOption() {
		return editOption;
	}

	public void setEditOption(int editOption) {
		this.editOption = editOption;
	}

	public int getCols() {
		return cols;
	}

	public void setCols(int cols) {
		this.cols = cols;
	}

	public int getRows() {
		return rows;
	}

	public void setRows(int rows) {
		this.rows = rows;
	}
}
