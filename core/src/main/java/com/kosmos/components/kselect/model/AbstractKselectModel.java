package com.kosmos.components.kselect.model;

import com.kosmos.components.view.model.ComponentViewModel;

/**
 * Created on 06/02/15.
 */
public class AbstractKselectModel extends ComponentViewModel{

    private static final long serialVersionUID = -8074887693820182772L;

    private String action;

    private String popinTitle;

    private int popinWidth;

    private boolean popinValidate;

    private boolean required;

    private int editOption;

    public String getAction() {
        return action;
    }

    public void setAction(final String action) {
        this.action = action;
    }

    public String getPopinTitle() {
        return popinTitle;
    }

    public void setPopinTitle(final String popinTitle) {
        this.popinTitle = popinTitle;
    }

    public int getPopinWidth() {
        return popinWidth;
    }

    public void setPopinWidth(final int popinWidth) {
        this.popinWidth = popinWidth;
    }

    public boolean isPopinValidate() {
        return popinValidate;
    }

    public void setPopinValidate(final boolean popinValidate) {
        this.popinValidate = popinValidate;
    }

    public boolean isRequired() {
        return required;
    }

    public void setRequired(final boolean required) {
        this.required = required;
    }

    public int getEditOption() {
        return editOption;
    }

    public void setEditOption(final int editOption) {
        this.editOption = editOption;
    }
}
