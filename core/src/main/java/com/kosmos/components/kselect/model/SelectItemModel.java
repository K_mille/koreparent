package com.kosmos.components.kselect.model;

import java.util.Map;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.kosmos.components.view.model.ComponentViewModel;

/**
 * Created on 06/02/15.
 */
public class SelectItemModel extends ComponentViewModel {

    private static final long serialVersionUID = 2387305733416971852L;

    private boolean selectable;

    private String title;

    private String value;

    private String tip;

    private Map<String, String> data;

    public boolean isSelectable() {
        return selectable;
    }

    public void setSelectable(final boolean selectable) {
        this.selectable = selectable;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public void setValue(final String value) {
        this.value = value;
    }

    public void setTip(final String tip) {
        this.tip = tip;
    }

    public void setData(final Map<String, String> data) {
        this.data = data;
    }

    public String getTitle() {
        return title;
    }

    public String getValue() {
        return value;
    }

    public String getTip() {
        return tip;
    }

    public Map<String, String> getData() {
        return data;
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 31).append(selectable).append(title).append(value).append(tip).toHashCode();
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj == null || !(obj.getClass() == this.getClass())) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        SelectItemModel model = (SelectItemModel) obj;
        return new EqualsBuilder().append(selectable, model.selectable).append(title, model.title).append(value, model.value).append(tip, model.tip).isEquals();
    }
}
