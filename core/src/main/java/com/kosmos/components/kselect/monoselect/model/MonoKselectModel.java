package com.kosmos.components.kselect.monoselect.model;

import com.kosmos.components.kselect.model.AbstractKselectModel;
import com.kosmos.components.kselect.model.SelectItemModel;

/**
 * Created on 06/02/15.
 */
public class MonoKselectModel extends AbstractKselectModel {

    private static final long serialVersionUID = 5306779112899693357L;

    private SelectItemModel selectItemModel;

    private String editLabel;

    private String clearLabel;

    public SelectItemModel getSelectItemModel() {
        return selectItemModel;
    }

    public void setSelectItemModel(final SelectItemModel selectItemModel) {
        this.selectItemModel = selectItemModel;
    }

    public String getEditLabel() {
        return editLabel;
    }

    public void setEditLabel(final String editLabel) {
        this.editLabel = editLabel;
    }

    public String getClearLabel() {
        return clearLabel;
    }

    public void setClearLabel(final String clearLabel) {
        this.clearLabel = clearLabel;
    }
}
