package com.kosmos.components.kselect.tag;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.kosmos.components.kselect.monoselect.model.MonoKselectModel;
import com.kosmos.components.kselect.utils.KmonoSelectUtils;
import com.kosmos.components.kselect.utils.KselectContext;
import com.kosmos.components.tags.AbstractComponentTag;

public class KmonoSelectTag extends AbstractComponentTag<MonoKselectModel> {

    private static final long serialVersionUID = -1842578617799617691L;

    public static final String KMONO_TEMPLATE = "/adminsite/components/kselect/mono/mono.jsp";

    /**
     * The context to compute the rendering {@link KselectContext}
     */
    private KselectContext context;

    /**
     * Optional parameters
     */
    private String params = StringUtils.EMPTY;

    public void setContext(final KselectContext context) {
        this.context = context;
    }

    public void setParams(final String params) {
        this.params = params;
    }

    public KmonoSelectTag() {
        path = KMONO_TEMPLATE;
    }

    @Override
    protected MonoKselectModel buildModel() {
        final MonoKselectModel model = KmonoSelectUtils.buildMonoSelectModel(context, retrieveInfoBean(), fieldName, params);
        model.setRequired(editOption == FormateurJSP.SAISIE_OBLIGATOIRE || editOption == FormateurJSP.SAISIE_OBLIGATOIRE_SELECTION_DEFAUT);
        model.setEditOption(editOption);
        return model;
    }
}
