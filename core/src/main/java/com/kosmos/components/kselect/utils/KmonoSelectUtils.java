package com.kosmos.components.kselect.utils;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.components.kselect.model.SelectItemModel;
import com.kosmos.components.kselect.monoselect.model.MonoKselectModel;
import com.kportal.core.config.MessageHelper;

public class KmonoSelectUtils {

    public static MonoKselectModel buildMonoSelectModel(final KselectContext context, final InfoBean infoBean, final String nomDonnee, final String params) {
        final String titleTip = StringUtils.defaultIfBlank(infoBean.getString("LIBELLE_" + nomDonnee), MessageHelper.getCoreMessage("KSELECT.MONO.EMPTY"));
        final SelectItemModel selectItemModel = new SelectItemModel();
        selectItemModel.setTitle(titleTip);
        selectItemModel.setValue(infoBean.getString(nomDonnee));
        selectItemModel.setTip(titleTip);
        final MonoKselectModel model = new MonoKselectModel();
        model.setSelectItemModel(selectItemModel);
        model.setEditLabel(MessageHelper.getCoreMessage("KSELECT.MONO.SELECT"));
        model.setClearLabel(MessageHelper.getCoreMessage("KSELECT.MONO.CLEAR"));
        KselectUtils.fillModel(context, model, infoBean, nomDonnee, params, true);
        return model;
    }
}
