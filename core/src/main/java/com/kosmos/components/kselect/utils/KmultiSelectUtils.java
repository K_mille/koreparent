package com.kosmos.components.kselect.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.jsbsoft.jtf.core.FormateurJSP;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.components.kselect.comparators.ValueComparator;
import com.kosmos.components.kselect.model.SelectItemModel;
import com.kosmos.components.kselect.multiselect.MultiSelectMode;
import com.kosmos.components.kselect.multiselect.model.MultiKselectModel;
import com.kportal.core.config.MessageHelper;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;

public class KmultiSelectUtils {

    public static MultiKselectModel buildMultiSelectModel(String extension, final KselectContext context, final InfoBean infoBean, final String dataSource, final String nomDonnee,
        int editOption, final String params, String label, String tooltip, MultiSelectMode type, boolean editable) {
        final MultiKselectModel model = new MultiKselectModel();
        model.setMode(type != null ? type : getMode(dataSource));
        final Map<String, String> mapDatasource = getDataSource(infoBean, dataSource);
        final Collection<SelectItemModel> selectedItems = getSelectedValues(infoBean, dataSource, nomDonnee);
        final Collection<SelectItemModel> items = getItems(mapDatasource);
        final String helpMessage = StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension, tooltip), tooltip);
        //items.removeAll(selectedItems);
        model.setName(nomDonnee);
        model.setItems(items);
        model.setSelectedItems(selectedItems);
        model.setValue(EscapeString.escapeAttributHtml(infoBean.get(nomDonnee, String.class)));
        model.setLabel(StringUtils.defaultIfBlank(MessageHelper.getExtensionMessage(extension,label),label));
        model.setTooltip(helpMessage);
        model.setRequired(editOption == FormateurJSP.SAISIE_OBLIGATOIRE || editOption == FormateurJSP.SAISIE_OBLIGATOIRE_SELECTION_DEFAUT);
        model.setEditOption(editOption);
        String classCss = editable ? " editable" : StringUtils.EMPTY;
        if(editOption == 0){
            classCss +=" locked";
        }
        model.setClassCss(classCss);
        KselectUtils.fillModel(context, model, infoBean, nomDonnee, params, false);
        KselectUtils.fillValues(model);
        if(model.getMode() == MultiSelectMode.ttl) {
            String ariane = KselectUtils.generateAriane(infoBean, nomDonnee, context);
            model.setAriane(ariane);
        }
        computeAttributes(model);
        return model;
    }

    private static MultiSelectMode getMode(final String dataSource) {
        return StringUtils.isBlank(dataSource) || dataSource.startsWith("TMP_") ? MultiSelectMode.ttl : MultiSelectMode.ltl;
    }


    private static void computeAttributes(final MultiKselectModel model) {
        final StringBuilder attributes = new StringBuilder(StringUtils.EMPTY);
        if(model.getMode() == MultiSelectMode.ttl) {
            attributes.append("data-popintitle=\"").append(model.getPopinTitle()).append("\"");
            attributes.append(" data-popinWidth=\"").append(model.getPopinWidth()).append("\"");
            attributes.append(" data-popinvalidate=\"").append(model.isPopinValidate()).append("\"");
        }
        model.setAttributes(attributes.toString());
    }

    private static Collection<SelectItemModel> getItems(Map<String, String> data) {
        final List<SelectItemModel> items = new ArrayList<>();
        for(Map.Entry<String, String> valueEntry : data.entrySet()) {
            final SelectItemModel model = new SelectItemModel();
            model.setTitle(valueEntry.getValue());
            model.setValue(valueEntry.getKey());
            model.setTip(valueEntry.getValue());
            items.add(model);
        }
        Collections.sort(items, new ValueComparator());
        return items;
    }

    private static Map<String, String> getDataSource(final InfoBean infoBean, final String dataSource) {
        final Locale localeCourante = ContexteUtil.getContexteUniv().getLocale();
        Map<String, String> values = new HashMap<>();
        if(StringUtils.isNotBlank(dataSource)) {
            if(infoBean.getMap(dataSource) != null){
                values = infoBean.getMap(dataSource);
            } else {
                values.putAll(CodeLibelle.lireTable(infoBean.getNomExtension(), dataSource, localeCourante));
            }
        }
        return values;
    }

    private static Map<String, String> getSelectedData(final InfoBean infoBean, final String dataSource, final String nomDonnee) {
        final Map<String, String> selectedData = getDataSource(infoBean, dataSource);
        final String labelData = infoBean.get("LIBELLE_" + nomDonnee, String.class);
        final String valueData = infoBean.get(nomDonnee, String.class);
        if(StringUtils.isNotBlank(labelData) && StringUtils.isNotBlank(valueData)) {
            final String[] labels = labelData.split(";");
            final String[] values = valueData.split(";");
            for(int i = 0 ; i < values.length ; i++) {
                if(!selectedData.containsKey(values[i])) {
                    final String currentLabel = i < labels.length ? labels[i] : MessageHelper.getCoreMessage("BO_LIBELLE_VIDE");
                    selectedData.put(values[i], currentLabel);
                }
            }
        }
        return selectedData;
    }

    private static Collection<SelectItemModel> getSelectedValues(final InfoBean infoBean, final String dataSource, final String nomDonnee) {
        final Collection<SelectItemModel> selectedValues = new ArrayList<>();
        final String data = infoBean.get(nomDonnee, String.class);
        if(data != null) {
            final Map<String, String> selectedData = getSelectedData(infoBean, dataSource, nomDonnee);
            for(String currentValue : data.split(";")) {
                if(StringUtils.isNotBlank(currentValue)) {
                    final String titleTip = selectedData.get(currentValue);
                    final SelectItemModel model = new SelectItemModel();
                    model.setValue(currentValue);
                    model.setTitle(titleTip);
                    model.setTip(titleTip);
                    selectedValues.add(model);
                }
            }
        }
        return selectedValues;
    }
}
