package com.kosmos.components.kselect.utils;

public enum KselectContext {

    CONTEXT_DEFAULT,
    CONTEXT_STRUCTURE,
    CONTEXT_ZONE,
    CONTEXT_GROUPEDSI_RESTRICTION,
    CONTEXT_GROUPEDSI_PUBLIC_VISE;

    public static KselectContext getFromInt(int value) {
        for(KselectContext currentValue : values()) {
            if(currentValue.ordinal() == value) {
                return currentValue;
            }
        }
        return CONTEXT_DEFAULT;
    }
}
