package com.kosmos.components.media.bean;

/**
 * Created on 20/11/14.
 */
public enum ComponentMediaType {
    PHOTO("PHOTO"), VIDEO("VIDEO"), AUDIO("AUDIO"), FLASH("FLASH"), FICHIER("FICHIER");

    private String type;

    ComponentMediaType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return type;
    }
}
