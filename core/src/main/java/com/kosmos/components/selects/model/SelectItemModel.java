package com.kosmos.components.selects.model;

import java.util.Map;

/**
 * Created on 06/02/15.
 */
public class SelectItemModel {

    private String title;

    private String value;

    private String tip;

    private Map<String, String> data;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }
}
