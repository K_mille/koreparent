package com.kosmos.components.tags;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TryCatchFinally;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.components.view.model.ComponentViewModel;
import com.univ.utils.ContexteUtil;

/**
 * Created on 10/05/2015.
 */
public abstract class AbstractComponentTag<T extends ComponentViewModel> extends AbstractJspIncludeTag implements TryCatchFinally {

    public static final String ID_EXTENSION = "idExtension";

    public static final String VIEW_MODEL = "viewModel";

    private static final long serialVersionUID = -2668170603087638695L;

    protected T viewModel;

    protected String retrieveKey;

    protected String extension;

    /**
     * Name of the field as it will appear in the current {@link com.jsbsoft.jtf.core.InfoBean}.
     */
    protected String fieldName;

    /**
     * Can be a computed label or a key to compute.
     */
    protected String label;

    protected String tooltip;

    protected int editOption;



    public void setEditOption(final int editOption) {
        this.editOption = editOption;
    }


    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    protected boolean front = false;

    public void setViewModel(T viewModel) {
        this.viewModel = viewModel;
    }

    public void setFront(boolean front) {
        this.front = front;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public int doStartTag() throws JspException {
        if (viewModel == null) {
            viewModel = retrieveModel(retrieveKey);
        }
        pageContext.getRequest().setAttribute(VIEW_MODEL, viewModel);
        pageContext.getRequest().setAttribute(ID_EXTENSION, extension);
        super.doStartTag();
        return 0;
    }

    private T retrieveModel(String retrieveKey) {
        if(StringUtils.isNotBlank(retrieveKey)) {
            T model = retrieveDataFromContext(retrieveKey);
            if(model != null) {
                return model;
            }
        }
        return buildModel();
    }

    protected InfoBean retrieveInfoBean() {
        final Object test = pageContext.getRequest().getAttribute("infoBean");
        if (test != null && test instanceof InfoBean) {
            return (InfoBean) test;
        }
        return null;
    }

    protected T retrieveDataFromContext(String key) {
        if (StringUtils.isNotBlank(key)) {
            if (this.front) {
                return (T) ContexteUtil.getContexteUniv().getData(key);
            } else {
                final InfoBean infoBean = retrieveInfoBean();
                if (infoBean != null) {
                    return (T) infoBean.get(key);
                }
            }
        }
        return null;
    }

    protected abstract T buildModel();

    @Override
    public void doCatch(Throwable throwable) throws Throwable {
        throw throwable;
    }

    @Override
    public void doFinally() {
        viewModel = null;
        front = false;
        extension = null;
    }
}
