package com.kosmos.components.view.model;

import java.io.Serializable;

/**
 * Created on 10/05/2015.
 */
public class ComponentViewModel implements Serializable{

    private static final long serialVersionUID = -3986020357421779143L;

    private String label;

    private String name;

    private String tooltip;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setTooltip(String tooltip) {
        this.tooltip = tooltip;
    }

    public String getTooltip() {
        return tooltip;
    }

}
