package com.kosmos.content.link.view.model;

import java.util.List;

import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.model.TagViewDescriptor;

/**
 * Created on 21/06/2015.
 */
public class LinkInputViewModel {

    private boolean textMandatory;

    private String linkLabel;

    private TagValue tagValue;

    private List<TagViewDescriptor> tagViewDescriptors;

    public boolean isTextMandatory() {
        return textMandatory;
    }

    public void setTextMandatory(final boolean textMandatory) {
        this.textMandatory = textMandatory;
    }

    public String getLinkLabel() {
        return linkLabel;
    }

    public void setLinkLabel(String linkLabel) {
        this.linkLabel = linkLabel;
    }

    public TagValue getTagValue() {
        return tagValue;
    }

    public void setTagValue(final TagValue tagValue) {
        this.tagValue = tagValue;
    }

    public List<TagViewDescriptor> getTagViewDescriptors() {
        return tagViewDescriptors;
    }

    public void setTagViewDescriptors(List<TagViewDescriptor> tagViewDescriptors) {
        this.tagViewDescriptors = tagViewDescriptors;
    }
}
