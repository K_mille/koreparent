package com.kosmos.log;

import org.slf4j.MDC;

/**
 * Classe utilitaire d'initialisation du contexte de log MDC log-context (sl4j).
 * @see MDC
 *
 * @author cpoisnel
 *
 */
public final class LoggerContextUtils {

    public static final String KEY_LOG_CONTEXT = "log-context";

    public static final String KEY_DEFAULT_CONTEXT = "default";

    /**
     * Constructeur privé.
     */
    private LoggerContextUtils() {
        super();
    }

    /**
     * Initialisation du contexte de log.
     * @param context
     *      Nom du contexte de log
     */
    public static void initContextLog(final String context) {
        MDC.put(KEY_LOG_CONTEXT, context);
    }

    /**
     * Initialisation du contexte de log si ce n'est pas le contexte par défaut ("default")
     * @param context
     *      Nom du contexte de log
     */
    public static void initContextLogIfNotDefault(final String context) {
        if (KEY_DEFAULT_CONTEXT.equals(getLogContext())){
            initContextLog(context);
        }
    }

    /**
     * Fermeture du contexte de log (par thread).
     */
    public static void closeContextLog() {
        MDC.remove(KEY_LOG_CONTEXT);
    }

    /**
     * Récupération du contexte de log.
     *
     * @return le contexte de log
     */
    public static String getLogContext() {
        return MDC.get(KEY_LOG_CONTEXT);
    }

}
