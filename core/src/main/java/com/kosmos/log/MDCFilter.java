package com.kosmos.log;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.slf4j.MDC;

/**
 * Filtre permettant d'intégrer l'identifiant de la session HTTP dans le contexte de diagnostic (MDC) de SLF4J.
 * Cela permet notamment d'afficher dans les fichiers de log l'identifiant de la session de l'utilisateur qui réalise une action.
 *
 * @author josse
 *
 */
public class MDCFilter implements Filter {

    /**
     * Nom de la clé du MDC qui contiendra l'identifiant de session.
     */
    private static final String ID_SESSION_MDC_KEY = "sessionId";

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final HttpServletRequest httpRequest = (HttpServletRequest) request;
        if (httpRequest != null) {
            final HttpSession httpSession = httpRequest.getSession(false);
            // Récupération de l'identifiant de la session (si elle existe) et stockage dans le MDC
            if (httpSession != null) {
                MDC.put(ID_SESSION_MDC_KEY, httpSession.getId());
            }
        }
        // Application des filtres suivants
        chain.doFilter(request, response);
    }

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        // Rien à faire
    }

    @Override
    public void destroy() {
        // Rien à faire
    }
}