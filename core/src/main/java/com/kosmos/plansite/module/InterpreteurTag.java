package com.kosmos.plansite.module;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.plansite.PlanSite;
import com.kportal.tag.interpreteur.impl.AbstractInterpreteurTag;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUtil;

public class InterpreteurTag extends AbstractInterpreteurTag {

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        final String param = StringUtils.substringBetween(texteAInterpreter, baliseOuvrante, baliseFermante);
        int indRubrique = param.indexOf("CODE_RUBRIQUE");
        int indNiveau = param.indexOf("NB_NIVEAUX");
        String codeRubriqueMere = "";
        if (indRubrique != -1) {
            indRubrique += 14;
            if (indRubrique < indNiveau) {
                codeRubriqueMere = param.substring(indRubrique, param.indexOf(";", indRubrique));
            } else {
                codeRubriqueMere = param.substring(indRubrique);
            }
        }
        int niveauMax = 0;
        if (indNiveau != -1) {
            indNiveau += 11;
            if (indNiveau < indRubrique) {
                niveauMax = Integer.parseInt(param.substring(indNiveau, param.indexOf(";", indNiveau)));
            } else {
                niveauMax = Integer.parseInt(param.substring(indNiveau));
            }
        }
        final RubriqueBean topRubrique = serviceRubrique.getRubriqueByCode(codeRubriqueMere);
        String tagResult = StringUtils.EMPTY;
        if (topRubrique != null) {
            tagResult = "<div class=\"plan-site\">" + PlanSite.buildPlan(topRubrique, ContexteUtil.getContexteUniv(), niveauMax) + "</div><!-- .plan-site -->";
        }
        return tagResult;
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        // aucune référence à calculer
        return null;
    }
}
