package com.kosmos.publish;

import java.util.Collection;
import java.util.Map;

import org.springframework.integration.aggregator.AbstractAggregatingMessageGroupProcessor;
import org.springframework.integration.store.MessageGroup;
import org.springframework.messaging.Message;
import org.springframework.util.Assert;

/**
 * This implementation of MessageGroupProcessor will take the messages from the
 * MessageGroup and return the first message
 *
 * @author Frédéric RAPIN
 */
public class DeduplicatingMessageGroupProcessor extends AbstractAggregatingMessageGroupProcessor {

    @Override
    protected final Object aggregatePayloads(MessageGroup group, Map<String, Object> headers) {
        Collection<Message<?>> messages = group.getMessages();
        Assert.notEmpty(messages, this.getClass().getSimpleName() + " cannot process empty message groups");
        return messages.iterator().next();
    }
}
