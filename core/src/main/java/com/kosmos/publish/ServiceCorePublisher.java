package com.kosmos.publish;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.integration.annotation.Publisher;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.Payload;

/**
 * Publish on queues.
 *
 * Created by cpoisnel on 12/12/16.
 */
public class ServiceCorePublisher {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceCorePublisher.class);

    /**
     * Publish a message on "channel.out.core".
     * @param bean
     * Payload
     * @param target
     * Header "target"
     * @param action
     * Header "action"
     * @param targetId
     * Header "targetId"
     *
     * @param <T> "bean" type
     * @return the bean
     */
    @Publisher(channel = "channel.out.core")
    @Payload
    public <T> T publish(T bean, @Header("target") String target, @Header("action") String action, @Header("targetId") Long targetId) {
        LOG.debug("Publish a message {}", bean);
        return bean;
    }


    /**
     * Publish a message on "channel.out.core".
     * @param bean
     * Payload
     * @param target
     * Header "target"
     * @param action
     * Header "action"
     *
     * @param <T> "bean" type
     * @return the bean
     */
    @Publisher(channel = "channel.out.core")
    @Payload
    public <T> T publish(T bean, @Header("target") String target, @Header("action") String action) {
        LOG.debug("Publish a message {}", bean);
        return bean;
    }
}
