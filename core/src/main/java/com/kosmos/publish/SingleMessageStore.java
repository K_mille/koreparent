package com.kosmos.publish;

import org.springframework.integration.store.MessageGroup;
import org.springframework.integration.store.SimpleMessageStore;
import org.springframework.messaging.Message;

/**
 * Created by frederic.rapin on 13/07/17.
 */
public class SingleMessageStore extends SimpleMessageStore {

    @Override
    public MessageGroup addMessageToGroup(Object groupId, Message<?> message) {
        MessageGroup group = this.getMessageGroup(groupId);
        if (group.getMessages().isEmpty() ) {
            super.addMessageToGroup(groupId,message);
        }
        return this.getMessageGroup(groupId);
    }
}
