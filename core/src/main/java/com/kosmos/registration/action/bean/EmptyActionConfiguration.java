package com.kosmos.registration.action.bean;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.registration.action.ActionConfiguration;

/**
 * Created on 07/01/15.
 */
public class EmptyActionConfiguration implements ActionConfiguration {

    @Override
    public String getActionId() {
        return StringUtils.EMPTY;
    }
}
