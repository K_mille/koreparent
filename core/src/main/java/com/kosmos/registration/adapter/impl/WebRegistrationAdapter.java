package com.kosmos.registration.adapter.impl;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.adapter.AbstractMapAdapter;
import com.kosmos.registration.action.Action;
import com.kosmos.registration.action.ActionConfiguration;
import com.kosmos.registration.bean.Model;
import com.kosmos.registration.bean.ModelDescriptor;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.exception.RegistrationModelNotFoundException;
import com.kosmos.registration.service.impl.ModelService;
import com.kosmos.registration.service.impl.RegistrationService;
import com.kosmos.registration.utils.ModelDescriptorUtils;
import com.kosmos.registration.utils.RegistrationUtils;

/**
 * Created on 31/12/14.
 */
public class WebRegistrationAdapter extends AbstractMapAdapter {

    public static final String ID_BEAN = "webAdapter";

    private static final Logger LOG = LoggerFactory.getLogger(WebRegistrationAdapter.class);

    private ModelService modelService;

    private RegistrationService registrationService;

    public void setModelService(final ModelService modelService) {
        this.modelService = modelService;
    }

    public void setRegistrationService(RegistrationService registrationService) {
        this.registrationService = registrationService;
    }

    public Map<String, ActionConfiguration> retrieveActionConfigurationDatas(Map<String, Object> dataSource, String modelDescriptorId) throws RegistrationModelNotFoundException {
        final ModelDescriptor modelDescriptor = ModelDescriptorUtils.getModelDescriptor(modelDescriptorId);
        final Map<String, ActionConfiguration> data = new HashMap<>();
        for (Map.Entry<String, Action> actionEntry : modelDescriptor.getActions().entrySet()) {
            final String actionId = actionEntry.getKey();
            final Action action = actionEntry.getValue();
            final ActionConfiguration configuration = getConfigurationData(dataSource, action);
            data.put(actionId, configuration);
        }
        return data;
    }

    public Map<String, RegistrationData> retrieveCreationData(Map<String, Object> dataSource, Long modelId) throws Exception {
        final ModelDescriptor modelDescriptor = getModelDescriptorFromModel(modelId);
        final Map<String, RegistrationData> data = new HashMap<>();
        for (Map.Entry<String, Action> actionEntry : modelDescriptor.getActions().entrySet()) {
            final String actionId = actionEntry.getKey();
            final Action action = actionEntry.getValue();
            final RegistrationData registrationData = getRegistrationData(dataSource, action);
            data.put(actionId, registrationData);
        }
        return data;
    }

    public Map<String, RegistrationData> retrieveUpdateData(Map<String, Object> dataSource, Long registrationId) throws RegistrationModelNotFoundException {
        final Registration registration = registrationService.getById(registrationId);
        if (registration == null) {
            throw new RegistrationModelNotFoundException("unable to find any entry for this request");
        }
        final Long modelId = registration.getModelId();
        final Model model = modelService.get(modelId);
        final String modelDescriptorId = model.getModelDescriptorId();
        final ModelDescriptor modelDescriptor = ModelDescriptorUtils.getModelDescriptor(modelDescriptorId);
        Map<String, RegistrationData> data = new HashMap<>();
        if (registration.getState() == RegistrationState.ERROR || registration.getState() == RegistrationState.WAITING) {
            String lastActionId = RegistrationUtils.getLastActionIdFromHistory(registration);
            if (StringUtils.isNotBlank(lastActionId)) {
                final Action action = modelDescriptor.getActions().get(lastActionId);
                registration.getDataByAction().put(lastActionId, getRegistrationData(dataSource, action));
                data = registration.getDataByAction();
            }
        } else {
            for (Map.Entry<String, Action> currentAction : modelDescriptor.getActions().entrySet()) {
                final RegistrationData registrationData = getRegistrationData(dataSource, currentAction.getValue());
                data.put(currentAction.getKey(), registrationData);
            }
        }
        return data;
    }

    private ModelDescriptor getModelDescriptorFromModel(final Long modelId) throws RegistrationModelNotFoundException {
        final Model model = modelService.get(modelId);
        final String modelDescriptorId = model.getModelDescriptorId();
        return ModelDescriptorUtils.getModelDescriptor(modelDescriptorId);
    }

    private RegistrationData getRegistrationData(Map<String, Object> dataSource, Action action) {
        final Class<? extends RegistrationData> registrationDataType = RegistrationUtils.getActionDataType(action, RegistrationData.class);
        RegistrationData data = null;
        if (registrationDataType != null) {
            data = retrieveData(dataSource, registrationDataType);
        } else {
            // TODO
            LOG.error(String.format("Le type de RegistrationData pour l'action %s n'a pas pu être résolu.", action));
        }
        return data;
    }

    private ActionConfiguration getConfigurationData(Map<String, Object> dataSource, Action action) {
        final Class<? extends ActionConfiguration> configurationDataType = RegistrationUtils.getActionDataType(action, ActionConfiguration.class);
        ActionConfiguration data = null;
        if (configurationDataType != null) {
            data = retrieveData(dataSource, configurationDataType);
        } else {
            // TODO
            LOG.error(String.format("Le type de ConfigurationData pour l'action %s n'a pas pu être résolu.", action));
        }
        return data;
    }
}
