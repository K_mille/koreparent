package com.kosmos.registration.bean;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.kosmos.registration.action.ActionConfiguration;
import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Created on 24/12/14.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class Model extends AbstractPersistenceBean {

    private static final long serialVersionUID = -7006503252731649198L;

    private String modelDescriptorId;

    private Map<String, ? extends ActionConfiguration> actionConfigurationById;

    public String getModelDescriptorId() {
        return modelDescriptorId;
    }

    public void setModelDescriptorId(String modelDescriptorId) {
        this.modelDescriptorId = modelDescriptorId;
    }

    public Map<String, ? extends ActionConfiguration> getActionConfigurationById() {
        return actionConfigurationById;
    }

    public void setActionConfigurationById(Map<String, ? extends ActionConfiguration> actionConfigurationById) {
        this.actionConfigurationById = actionConfigurationById;
    }

}
