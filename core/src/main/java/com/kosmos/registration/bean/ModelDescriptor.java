package com.kosmos.registration.bean;

import java.util.Map;

import com.kosmos.registration.action.Action;

/**
 * Created by olivier.camon on 06/01/15.
 */
public class ModelDescriptor {

    private String id;

    private Map<String, Action> actions;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Map<String, Action> getActions() {
        return actions;
    }

    public void setActions(Map<String, Action> actions) {
        this.actions = actions;
    }
}
