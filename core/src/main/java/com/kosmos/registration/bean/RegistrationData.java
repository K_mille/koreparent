package com.kosmos.registration.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonTypeInfo;

/**
 * Interface devant être implémentée pour contenir les infos nécessaire aux != étapes de validation, modération
 * etc.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public interface RegistrationData extends Serializable {}
