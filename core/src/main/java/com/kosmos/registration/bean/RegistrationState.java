package com.kosmos.registration.bean;

/**
 * Enumération listant l'ensemble des états possible d'une Registration
 */
public enum RegistrationState {
    /**
     * Lorsque l'action à finalisé son traitement
     */
    FINISHED,
    /**
     * Lorsque le traitement est en cours de réalisation.
     */
    IN_PROGRESS,
    /**
     * Lorque l'action attends une réponse de l'utilisateur
     */
    WAITING,
    /**
     * Lorsque la registration ne peut être traiter
     */
    ERROR
}
