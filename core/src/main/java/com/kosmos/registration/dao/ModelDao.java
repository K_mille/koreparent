package com.kosmos.registration.dao;

import java.util.List;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.kosmos.registration.bean.Model;

/**
 * Created on 06/01/15.
 */
public abstract class ModelDao extends AbstractCommonDAO<Model> {

    public abstract List<Model> getModelByDescriptorID(String modelDescriptorID);
}
