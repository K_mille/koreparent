package com.kosmos.registration.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

/**
 * Created by olivier.camon on 17/12/14.
 */
public class AdapterException extends ErreurApplicative {

    private static final long serialVersionUID = -8650282068298594653L;

    public AdapterException(final int num, final String mes) {
        super(num, mes);
    }

    public AdapterException(final String mes, final Throwable t) {
        super(mes, t);
    }

    public AdapterException(final String mes) {
        super(mes);
    }
}
