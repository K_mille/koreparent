package com.kosmos.registration.exception;

/**
 * Created on 17/03/15.
 */
public class RegistrationModelNotFoundException extends RegistrationException {

    private static final long serialVersionUID = -8674055546332445933L;

    public RegistrationModelNotFoundException(final String message) {
        super(message);
    }
}
