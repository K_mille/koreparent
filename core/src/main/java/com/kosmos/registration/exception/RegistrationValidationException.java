package com.kosmos.registration.exception;

/**
 * Created by olivier.camon on 08/01/15.
 */
public class RegistrationValidationException extends RegistrationException {

    private static final long serialVersionUID = 7192998552758243020L;

    public RegistrationValidationException(final String message) {
        super(message);
    }

    public RegistrationValidationException(final String message, final String extensionId) {
        super(message, extensionId);
    }

    public RegistrationValidationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public RegistrationValidationException(final String message, final String extensionId, final Throwable cause) {
        super(message, extensionId, cause);
    }
}