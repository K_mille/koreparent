package com.kosmos.registration.exception;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;

/**
 * Created by olivier.camon on 09/01/15.
 */
public class RegistrationViolationValidationException extends RegistrationValidationException {

    private static final long serialVersionUID = -557287973219797731L;

    private Set<ConstraintViolation<?>> constraintViolations = new HashSet<>();

    public RegistrationViolationValidationException(final String message) {
        super(message);
    }

    public RegistrationViolationValidationException(final String message, final String extensionId) {
        super(message, extensionId);
    }

    public RegistrationViolationValidationException(final String message, final String extensionId, final Throwable cause) {
        super(message, extensionId, cause);
    }

    public RegistrationViolationValidationException(final String message, final Throwable cause) {
        super(message, cause);
    }

    public RegistrationViolationValidationException(final String mes, final Set<? extends ConstraintViolation<?>> constraintViolations) {
        super(mes);
        this.constraintViolations = new HashSet<>(constraintViolations);
    }

    public RegistrationViolationValidationException(final String mes, final String extensionId, final Set<? extends ConstraintViolation<?>> constraintViolations) {
        super(mes, extensionId);
        this.constraintViolations = new HashSet<>(constraintViolations);
    }

    public Set<ConstraintViolation<?>> getConstraintViolations() {
        return constraintViolations;
    }
}
