/**
 * Package gérant l'API de registration servant à gérer un worklow d'action sur des éléments, principalement des formulaires
 */
package com.kosmos.registration;