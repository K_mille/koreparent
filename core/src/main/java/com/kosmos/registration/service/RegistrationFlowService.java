package com.kosmos.registration.service;

import java.util.Map;

import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;

/**
 * Created on 07/01/15.
 */
public interface RegistrationFlowService {

    // TODO : supprimer le throws Exception
    Registration create(Map<String, RegistrationData> data, Long modelId, String identity) throws Exception;

    // TODO : supprimer le throws Exception
    Registration update(Map<String, RegistrationData> data, Long registrationId, String identity) throws Exception;
}
