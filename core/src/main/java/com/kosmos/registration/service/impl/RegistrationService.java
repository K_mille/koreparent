package com.kosmos.registration.service.impl;

import java.util.Collection;

import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.registration.bean.Model;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.registration.dao.RegistrationDao;
import com.kosmos.service.impl.AbstractServiceBean;

/**
 * Service permettant de gérer les {@link Registration}. Il sert pour les opérations CRUD
 */
public class RegistrationService extends AbstractServiceBean<Registration, RegistrationDao> {

    /**
     * Permet de créer une nouvelle registration
     *
     * @param registration la registration à enregistrer
     * @return la registration enregistré avec son nouvel id généré par la bdd
     * @deprecated Utiliser la méthode "save"
     */
    @Deprecated
    public Registration create(Registration registration) {
        return dao.add(registration);
    }

    /**
     * Met à jour la registration fourni en paramètre
     *
     * @param registration la registration à mettre à jour
     * @return La registration mise à jour
     * @deprecated Utiliser la méthode "save"
     */
    @Deprecated
    public Registration update(Registration registration) {
        return dao.update(registration);
    }

    /**
     * Récupère les {@link Registration} lié au {@link Model} dont l'id est fourni en paramètre.
     *
     * @param modelId l'id du modèle dont on souhaite récupérer les Registration
     * @return L'ensemble des Registration liés au {@link Model} ou une collection vide si non trouvé
     */
    public Collection<Registration> getRegistrationsByModel(Long modelId) {
        return dao.getByModel(modelId);
    }

    /**
     * Récupère les {@link Registration} lié au {@link Model} dont l'id est fourni en paramètre et dont le {@link RegistrationState} correspond à celui fourni.
     *
     * @param modelId l'id du modèle dont on souhaite récupérer les Registration
     * @param state   l'état dans lesquels on veut récupérer les Registrations
     * @return L'ensemble des Registration liés au {@link Model} ou une collection vide si non trouvé
     * @throws DataSourceException lors de la requête en bdd
     */
    public Collection<Registration> getRegistrationsByModelAndState(Long modelId, RegistrationState state) {
        return dao.getByModelAndState(modelId, state);
    }

    public void deleteByModelId(Long modelId) {
        dao.deleteByModelId(modelId);
    }
}
