package com.kosmos.registration.wrapper;

import org.apache.commons.lang3.tuple.MutablePair;

import com.kosmos.registration.action.history.ActionHistory;

/**
 * Created by olivier.camon on 22/01/15.
 */
public class ActionHistoryByActionID extends MutablePair<String, ActionHistory> {

    private static final long serialVersionUID = -8663038377305700618L;
}
