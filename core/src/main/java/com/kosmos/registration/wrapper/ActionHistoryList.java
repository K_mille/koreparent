package com.kosmos.registration.wrapper;

import java.util.ArrayList;

import org.apache.commons.lang3.tuple.Pair;

import com.kosmos.registration.action.history.ActionHistory;

/**
 * wrapper permettant de figer le type paramétrable mis la list pour pouvoir le récuperer au moment de la sérialisation/déserialisation
 */
public class ActionHistoryList extends ArrayList<Pair<String, ActionHistory>> {

    private static final long serialVersionUID = 3636772877127171902L;

    public ActionHistoryList() {
        super();
    }
}
