package com.kosmos.registration.wrapper;

import java.util.HashMap;

import com.kosmos.registration.bean.RegistrationData;

/**
 * wrapper permettant de figer le type paramétrable mis la map pour pouvoir le récuperer au moment de la sérialisation/déserialisation
 */
public class RegistrationDataMap extends HashMap<String, RegistrationData> {

    private static final long serialVersionUID = -3621372575766424808L;
}
