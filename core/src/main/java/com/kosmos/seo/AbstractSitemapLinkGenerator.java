package com.kosmos.seo;

public abstract class AbstractSitemapLinkGenerator {

    /**
     * Génère le lien vers le sitemap.
     * @return le lien vers le sitemap
     */
    public abstract String getSitemapLink();
}
