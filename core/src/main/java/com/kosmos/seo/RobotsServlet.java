package com.kosmos.seo;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.servlet.ExtensionServlet;
import com.univ.multisites.InfosSite;
import com.univ.utils.ContexteUtil;

@WebServlet(name = "robotsServlet", urlPatterns = "/robots.txt", loadOnStartup = 0)
public class RobotsServlet extends ExtensionServlet {

    private final String DEFAULT_ROBOTS_PATH = "default-robots.txt";
    private final String SURCHARGE_STORAGE_ROBOTS_FILE_NAME = "robots.txt";

    private static final Logger LOG = LoggerFactory.getLogger(RobotsServlet.class);

    /**
     * Méthode standard, sous-classée si besoin.
     *
     * @param req
     *            la requete http en GET
     * @param res
     *            la réponse à renvoyer
     *
     */
    @Override
    public void doGet(final HttpServletRequest req, final HttpServletResponse res) {
        try{
            File robotsFile = getRobotsBaseFile();
            res.setContentType("text");
            Files.copy(robotsFile.toPath(), res.getOutputStream());

            InfosSite infosSite = ContexteUtil.getContexteUniv().getInfosSite();
            if(infosSite != null && !infosSite.isNonIndexe()) {
                AbstractSitemapLinkGenerator sitemapLinkGenerator = getSiteMapLinkGenerator();
                if (sitemapLinkGenerator != null) {
                    res.getOutputStream().print("\n\nSitemap: " + sitemapLinkGenerator.getSitemapLink());
                }
            }
        } catch (IOException e) {
            LOG.error("Unable to forward the request", e);
        }
    }

    /**
     * Récupère le générateur de lien sitemap définit dans le contexte.
     * @return le générateur de lien sitemap définit dans le contexte.
     */
    private AbstractSitemapLinkGenerator getSiteMapLinkGenerator(){
        Map<String, AbstractSitemapLinkGenerator> sitemapLinkGeneratorMap =ApplicationContextManager.getAllBeansOfType(AbstractSitemapLinkGenerator.class);
        for(Map.Entry<String, AbstractSitemapLinkGenerator> entry : sitemapLinkGeneratorMap.entrySet()){
            AbstractSitemapLinkGenerator sitemapLinkGenerator = entry.getValue();
            if(sitemapLinkGenerator != null){
                return sitemapLinkGenerator;
            }
        }
        return null;
    }

    /**
     * Renvoie le fichier robots.txt qui sera utilisé comme base. Par défaut renvoie le robots.txt présent dans les ressources du core, si fichier présent dans storage/conf renvoie celui-ci.
     * @return le fichier robots.txt qui sera utilisé comme base
     */
    private File getRobotsBaseFile(){
        File surchargeRobots = new File(WebAppUtil.getConfDir(), SURCHARGE_STORAGE_ROBOTS_FILE_NAME);
        if(surchargeRobots != null && surchargeRobots.exists() && surchargeRobots.isFile()){
            return surchargeRobots;
        }
        return new File(WebAppUtil.getClassPath() + DEFAULT_ROBOTS_PATH);
    }
}
