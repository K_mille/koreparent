package com.kosmos.service;

/**
 * Created on 03/11/15.
 */
public interface ServiceBean<T> extends Service{

    void save(T bean);

    void delete(Long id);

    T getById(Long id);
}
