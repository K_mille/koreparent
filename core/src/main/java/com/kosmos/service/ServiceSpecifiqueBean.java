package com.kosmos.service;

import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Représente un service spécifique à un bean.
 */
public interface ServiceSpecifiqueBean<T extends PersistenceBean> extends Service {}
