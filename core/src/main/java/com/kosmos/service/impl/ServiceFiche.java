package com.kosmos.service.impl;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.AbstractPersistenceBean;
import com.univ.objetspartages.dao.AbstractFicheDAO;
import com.univ.objetspartages.om.EtatFiche;

/**
 * Service des fiches
 */
public class ServiceFiche<T extends AbstractFicheBean, D extends AbstractFicheDAO<T>> extends AbstractServiceBean<T, D> {

    /**
     * /!\ ATTENTION A L UTILISATION DE CETTE METHODE /!\
     * Cette méthode est une migration des méthodes getFiches présente sur les objets OM par exple : Article#getFicheArticle(String, String)
     * Or ces méthodes ne font pas ce qu'elles disent.
     * Elle récupères les fiches sur le code et la langue fourni en paramètre.
     * Si aucun résultat n'est trouvé, cela refait la requête mais sans l'état.
     * Si toujours aucun résultat n'est trouvé cela requête sans la langue en état en ligne...
     * @param code le code de la fiche à récupérer
     * @param langue la langue de la fiche à récupérer
     * @return Un Bean d'un objet Fiche ou null si non trouvé.
     */
    public T getFiche(String code, String langue) {
        List<T> queryResult = dao.selectCodeLangueEtat(code, langue, EtatFiche.EN_LIGNE.getEtat());
        if (queryResult.size() == 0) {
            queryResult = dao.selectCodeLangueEtat(code, langue, StringUtils.EMPTY);
            if (queryResult.size() == 0) {
                queryResult = dao.selectCodeLangueEtat(code, StringUtils.EMPTY, EtatFiche.EN_LIGNE.getEtat());
            }
        }
        T result = null;
        if (!queryResult.isEmpty()) {
            result = queryResult.get(0);
        }
        return result;
    }

    /**
     * Retourne le libellé affichable de la fiche
     * @param bean : {@link AbstractPersistenceBean}
     * @return le libellé
     */
    public String getLibelleAffichable(T bean){
        return StringUtils.defaultIfEmpty(bean.getTitre(), "-");
    }

}
