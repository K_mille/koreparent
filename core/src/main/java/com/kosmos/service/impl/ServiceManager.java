package com.kosmos.service.impl;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.commons.collections.map.MultiKeyMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.GenericTypeResolver;
import org.springframework.util.ClassUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.Service;
import com.kosmos.service.ServiceBean;
import com.kosmos.service.ServiceSpecifiqueBean;
import com.kportal.extension.ExtensionManager;
import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Created on 29/09/15.
 */
public class ServiceManager {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceManager.class);

    public static final String ID_BEAN = "serviceManager";

    private final Map<Class<?>, ServiceBean<?>> beanServices = new HashMap<>();

    private final Map<Class<? extends Service>, Service> services = new HashMap<>();

    /**
     * Map des services spécifiques à un type de bean.
     */
    private MultiKeyMap servicesSpecifiquesBean = new MultiKeyMap();

    public ExtensionManager extensionManager;

    public void init() {
        refresh();
    }

    public void setExtensionManager(ExtensionManager extensionManager) {
        this.extensionManager = extensionManager;
    }

    private static ServiceManager getServiceManager() {
        return ApplicationContextManager.getCoreContextBean(ID_BEAN, ServiceManager.class);
    }

    public static <S> S getServiceForBean(final Class<?> beanClass) {
        return getServiceManager().getInternalServiceForBean(beanClass);
    }

    /**
     * Retourne l'implémentation spécifique à un bean d'un service à partir de sa classe.
     * @param serviceClass la classe du service dont on souhaite obtenir une implémentation
     * @param beanClass la classe du bean pour laquelle on souhaite obtenir une implémentation spécifique
     * @return l'implémentation du {@link ServiceSpecifiqueBean} ou l'implémentation standard du service si l'implémentation spécifique n'existe pas
     */
    public static <S extends Service> S getServiceSpecifiqueForBean(final Class<S> serviceClass, final Class<? extends PersistenceBean> beanClass) {
        S serviceSpecifiqueForFiche = getServiceManager().getInternalServiceSpecifiqueForFiche(serviceClass, beanClass);
        // S'il n'existe pas d'implémentation spécifique pour la fiche, on retourne le service parent
        if (serviceSpecifiqueForFiche == null) {
            serviceSpecifiqueForFiche = getService(serviceClass);
        }
        return serviceSpecifiqueForFiche;
    }

    public static <S> S getService(final Class<?> serviceClass) {
        return getServiceManager().getInternalService(serviceClass);
    }

    protected <S extends ServiceBean> S getInternalServiceForBean(final Class<?> beanClass) {
        return (S) beanServices.get(beanClass);
    }

    /**
     * Retourne l'implémentation spécifique à un bean d'un service à partir de sa classe
     * @param serviceClass la classe du service dont on souhaite obtenir une implémentation
     * @param beanClass la classe du bean pour laquelle on souhaite obtenir une implémentation spécifique
     * @param <S> l'interface du service spécifique
     * @param <T> la classe du bean
     * @return l'implémentation du {@link ServiceSpecifiqueBean}
     */
    protected <S extends ServiceSpecifiqueBean<T>, T extends PersistenceBean> S getInternalServiceSpecifiqueForFiche(final Class<?> serviceClass, final Class<T> beanClass) {
        return (S) servicesSpecifiquesBean.get(new MultiKey(serviceClass, beanClass));
    }

    protected <S extends Service> S getInternalService(final Class<?> serviceClass) {
        return (S) services.get(serviceClass);
    }

    private static <T> Class<T> getGenericType(ServiceBean<T> service) {
        Class<?> serviceClass = ClassUtils.getUserClass(service.getClass());
        try {
            final Method method = serviceClass.getMethod("getById", Long.class);
            return (Class<T>) GenericTypeResolver.resolveReturnType(method, service.getClass());
        } catch (final NoSuchMethodException e) {
            LOG.error(String.format("Une erreur est survenue lors de la tentative de récupération de la classe gérée par le service \"%s\"", service.getClass().getName()), e);
        }
        return null;
    }

    public void refresh() {
        final Map<String, Service> foundServices = ApplicationContextManager.getAllBeansOfType(Service.class);
        for (Service currentService : foundServices.values()) {
            if (currentService instanceof ServiceBean) {
                final ServiceBean<?> currentServiceBean = (ServiceBean) currentService;
                beanServices.put(getGenericType(currentServiceBean), currentServiceBean);
            }
            services.put((Class<? extends Service>) ClassUtils.getUserClass(currentService.getClass()), currentService);
        }
        final Map<String, ServiceSpecifiqueBean> foundServicesSpecifiquesFiche = ApplicationContextManager.getAllBeansOfType(ServiceSpecifiqueBean.class);
        for (ServiceSpecifiqueBean currentServiceSpecifiqueFiche : foundServicesSpecifiquesFiche.values()) {
            // Ajout dans la map des services spécifiques avec une clé multiple classe du service / classe du bean
            final Class<?> serviceClass = extractServiceClass(currentServiceSpecifiqueFiche);
            final Class<?> beanClass = GenericTypeResolver.resolveTypeArgument(currentServiceSpecifiqueFiche.getClass(), ServiceSpecifiqueBean.class);
            if (serviceClass != null && beanClass != null) {
                final MultiKey multiKey = new MultiKey(serviceClass, beanClass);
                // Si aucun bean n'est référencé pour cette clé ou que le bean référencé est un parent du bean actuel, on le référence pour la clé
                final ServiceSpecifiqueBean serviceSpecifiqueBean = (ServiceSpecifiqueBean) servicesSpecifiquesBean.get(multiKey);
                if (serviceSpecifiqueBean == null || serviceSpecifiqueBean.getClass().isAssignableFrom(currentServiceSpecifiqueFiche.getClass())) {
                    LOG.debug(String.format("Référencement de la classe '%s' comme implémentation spécifique au bean '%s' pour le service '%s'", currentServiceSpecifiqueFiche, beanClass, serviceClass));
                    servicesSpecifiquesBean.put(multiKey, currentServiceSpecifiqueFiche);
                }
            }
        }
    }

    public void updateContext() {
        beanServices.clear();
        services.clear();
        servicesSpecifiquesBean.clear();
        refresh();
    }

    /**
     * Extrait la classe qui implémente {@link Service} et qui est une mère du service passé en paramètre.
     * @param serviceSpecifiqueBean le service dont on souhaite connaître la superclasse
     * @return la superclasse du service passé en paramètre implémentant {@link Service}
     */
    private Class<?> extractServiceClass(final ServiceSpecifiqueBean serviceSpecifiqueBean) {
        if (serviceSpecifiqueBean == null) {
            return null;
        }
        Class<?> serviceClass = serviceSpecifiqueBean.getClass().getSuperclass();
        while (serviceClass != null && !Arrays.asList(serviceClass.getInterfaces()).contains(Service.class)) {
            serviceClass = serviceClass.getSuperclass();
        }
        return serviceClass;
    }
}
