package com.kosmos.tiles;

/**
 * Created by camille.lebugle on 04/01/17.
 */
public class DefinitionModel {

    String name;

    Object model;

    public DefinitionModel() {
    }

    public DefinitionModel(final String name) {
        this.name = name;
    }

    public DefinitionModel(final String name, final Object model) {
        this.name = name;
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public Object getModel() {
        return model;
    }

    public void setModel(final Object model) {
        this.model = model;
    }
}
