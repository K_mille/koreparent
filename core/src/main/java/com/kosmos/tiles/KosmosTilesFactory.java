package com.kosmos.tiles;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import org.apache.tiles.extras.complete.CompleteAutoloadTilesContainerFactory;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.ApplicationResource;

/**
 * Factory pour récupérer les fichier à traiter par Tiles.
 *
 */
public class KosmosTilesFactory extends CompleteAutoloadTilesContainerFactory {

    private static final String TILES_EXTENSION_LOCATION = "/extensions/**/WEB-INF/**/tiles*.xml";


    /**
     * Récupération des fichiers sources de tiles
     */
    @Override
    protected List<ApplicationResource> getSources(ApplicationContext applicationContext) {
        List<ApplicationResource> filteredResources = super.getSources(applicationContext);
        Collection<ApplicationResource> extensionWebINFSet = applicationContext.getResources(TILES_EXTENSION_LOCATION);
        Iterator<ApplicationResource> sourcesIterator;
        ApplicationResource resource;
        if(extensionWebINFSet != null) {
            sourcesIterator = extensionWebINFSet.iterator();
            while(sourcesIterator.hasNext()) {
                resource = (ApplicationResource) sourcesIterator.next();
                if(Locale.ROOT.equals(resource.getLocale())) {
                    filteredResources.add(resource);
                }
            }
        }
        return filteredResources;
    }

}
