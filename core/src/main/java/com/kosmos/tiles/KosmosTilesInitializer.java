package com.kosmos.tiles;

import org.apache.tiles.extras.complete.CompleteAutoloadTilesInitializer;
import org.apache.tiles.factory.AbstractTilesContainerFactory;
import org.apache.tiles.request.ApplicationContext;

/**
 * Created by camille.lebugle on 04/01/17.
 */
public class KosmosTilesInitializer extends CompleteAutoloadTilesInitializer {

    @Override
    protected AbstractTilesContainerFactory createContainerFactory(final ApplicationContext applicationContext) {
        return new KosmosTilesFactory();
    }
}
