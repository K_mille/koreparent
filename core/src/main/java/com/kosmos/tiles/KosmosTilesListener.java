package com.kosmos.tiles;

import org.apache.tiles.factory.AbstractTilesContainerFactory;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.startup.TilesInitializer;
import org.apache.tiles.web.startup.AbstractTilesListener;

/**
 * Created by camille.lebugle on 04/01/17.
 */
public class KosmosTilesListener extends AbstractTilesListener {

    public KosmosTilesListener() {
    }

    @Override
    protected TilesInitializer createTilesInitializer() {
        return new KosmosTilesInitializer();
    }

}