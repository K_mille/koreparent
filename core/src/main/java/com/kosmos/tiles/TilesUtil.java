package com.kosmos.tiles;

/**
 * Created by camille.lebugle on 04/01/17.
 */
public final class TilesUtil {

    public static final String DEFAULT_TILES_SEARCH_DEFINITION = "tiles.search";

    public static final String TILES_DEFINITION_SEPARATOR = ".";

}
