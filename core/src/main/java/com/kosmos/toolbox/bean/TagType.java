package com.kosmos.toolbox.bean;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

import com.kportal.core.autorisation.Permission;

/**
 * Created by olivier.camon on 24/06/15.
 */
public class TagType {

    private String id;

    private Collection<Permission> permissions = new ArrayList<>();

    private String inputFragment;

    private String labelKey;

    private String category;

    private String tagPattern;

    private String validationPattern;

    private String jsScript;

    private Map<String, String> htmlAttributes;

    private Pair<String, String> icons;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public Collection<Permission> getPermissions() {
        return permissions;
    }

    public void setPermissions(final Collection<Permission> permissions) {
        this.permissions = permissions;
    }

    public String getInputFragment() {
        return inputFragment;
    }

    public void setInputFragment(final String inputFragment) {
        this.inputFragment = inputFragment;
    }

    public String getLabelKey() {
        return labelKey;
    }

    public void setLabelKey(final String labelKey) {
        this.labelKey = labelKey;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(final String category) {
        this.category = category;
    }

    public String getTagPattern() {
        return tagPattern;
    }

    public void setTagPattern(final String tagPattern) {
        this.tagPattern = tagPattern;
    }

    public String getValidationPattern() {
        return validationPattern;
    }

    public void setValidationPattern(final String validationPattern) {
        this.validationPattern = validationPattern;
    }

    public String getJsScript() {
        return jsScript;
    }

    public void setJsScript(final String jsScript) {
        this.jsScript = jsScript;
    }

    public Map<String, String> getHtmlAttributes() {
        return htmlAttributes;
    }

    public void setHtmlAttributes(final Map<String, String> htmlAttributes) {
        this.htmlAttributes = htmlAttributes;
    }

    public Pair<String, String> getIcons() {
        return icons;
    }

    public void setIcons(final Pair<String, String> icons) {
        this.icons = icons;
    }
}
