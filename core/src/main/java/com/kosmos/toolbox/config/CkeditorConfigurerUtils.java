package com.kosmos.toolbox.config;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;

public class CkeditorConfigurerUtils {

    private static final String DEFAULT_CONF_KEY = "DEFAULT";

    public static String buildConfKey(InfoBean infoBean, String fieldName) {
        return String.format("%s.%s", infoBean.getNomProcessus(), fieldName);
    }

    public static String getProperty(final String key) {
        return getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key);
    }

    public static String getProperty(String idCtx, final String key) {
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        for (final CkeditorConfigurer loader : ApplicationContextManager.getBeansOfType(idCtx, CkeditorConfigurer.class).values()) {
            final String message = loader.getProperties().getProperty(key);
            if (message != null) {
                // le helper permet de substituer la valeur d'une propriété ${key} par la valeur de 'key'
                return loader.getPlaceHolderHelper().replacePlaceholders(message, loader.getProperties());
            }
        }
        return null;
    }

    public static Properties getProperties(String idCtx) {
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        final Properties prop = new Properties();
        for (final CkeditorConfigurer loader : ApplicationContextManager.getBeansOfType(idCtx, CkeditorConfigurer.class).values()) {
            prop.putAll(loader.getProperties());
        }
        return prop;
    }

    public static String getConfPathForKey(String idExtension, String key){
        final Collection<String> keyComponents = buildKeyComponents(key);
        String path = null;
        for(String keyComponent : keyComponents) {
            path = getProperty(idExtension, keyComponent);
            if(StringUtils.isNotBlank(path)) {
                break;
            }
        }
        path = StringUtils.defaultIfBlank(path, getProperty(idExtension, DEFAULT_CONF_KEY));
        return StringUtils.defaultIfBlank(path, getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, DEFAULT_CONF_KEY));
    }

    private static Collection<String> buildKeyComponents(String key) {
        final Collection<String> components = new HashSet<>();
        components.add(key);
        components.addAll(Arrays.asList(StringUtils.split(key, ".")));
        return components;
    }

    public static String getCoreConfPathForKey(String key) {
        return StringUtils.defaultIfBlank(getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key), getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, DEFAULT_CONF_KEY));
    }

    public static String getDefaultConfPath() {
        return getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, DEFAULT_CONF_KEY);
    }
}
