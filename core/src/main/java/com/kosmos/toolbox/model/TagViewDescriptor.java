package com.kosmos.toolbox.model;

import java.util.Map;

import org.apache.commons.lang3.tuple.Pair;

/**
 * Created on 21/06/2015.
 */
public class TagViewDescriptor {

    private String label;

    private String id;

    private String formatterPattern;

    private String validationPattern;

    private String inputFragment;

    private String jsScript;

    private Map<String, String> data;

    private String htmlAttributes;

    private Pair<String, String> icons;

    private boolean selected;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFormatterPattern() {
        return formatterPattern;
    }

    public void setFormatterPattern(String formatterPattern) {
        this.formatterPattern = formatterPattern;
    }

    public String getValidationPattern() {
        return validationPattern;
    }

    public void setValidationPattern(String validationPattern) {
        this.validationPattern = validationPattern;
    }

    public String getInputFragment() {
        return inputFragment;
    }

    public void setInputFragment(String inputFragment) {
        this.inputFragment = inputFragment;
    }

    public String getJsScript() {
        return jsScript;
    }

    public void setJsScript(String jsScript) {
        this.jsScript = jsScript;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public Map<String, String> getData() {
        return data;
    }

    public void setData(Map<String, String> data) {
        this.data = data;
    }

    public String getHtmlAttributes() {
        return htmlAttributes;
    }

    public void setHtmlAttributes(final String htmlAttributes) {
        this.htmlAttributes = htmlAttributes;
    }

    public Pair<String, String> getIcons() {
        return icons;
    }

    public void setIcons(final Pair<String, String> icons) {
        this.icons = icons;
    }
}
