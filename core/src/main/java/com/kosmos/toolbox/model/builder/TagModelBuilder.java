package com.kosmos.toolbox.model.builder;

import java.util.Map;

/**
 * Created on 22/06/15.
 */
public interface TagModelBuilder {

    String getForId();

    void populateData(Map<String, String> data);

}
