package com.kosmos.toolbox.model.builder.impl;

import com.kosmos.toolbox.model.builder.TagModelBuilder;

/**
 * Created on 22/06/15.
 */
public abstract class AbstractTagModelBuilder implements TagModelBuilder {

    private String forId;

    public void setForId(final String forId) {
        this.forId = forId;
    }

    @Override
    public String getForId() {
        return forId;
    }
}
