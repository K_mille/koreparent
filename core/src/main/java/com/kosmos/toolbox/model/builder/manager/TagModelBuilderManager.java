package com.kosmos.toolbox.model.builder.manager;

import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.toolbox.model.builder.TagModelBuilder;
import com.kportal.extension.module.AbstractBeanManager;

/**
 * Created on 15/06/15.
 */
public class TagModelBuilderManager extends AbstractBeanManager {

    private Map<String, TagModelBuilder> builders = new HashMap<>();

    public Map<String, TagModelBuilder> getBuilders() {
        return builders;
    }

    @Override
    public void refresh() {
        builders.clear();
        final Map<String, TagModelBuilder> beans = ApplicationContextManager.getAllBeansOfType(TagModelBuilder.class);
        for(TagModelBuilder currentBuilder : beans.values()) {
            builders.put(currentBuilder.getForId(), currentBuilder);
        }
    }

    public TagModelBuilder getForId(String id) {
        return builders.get(id);
    }
}
