package com.kosmos.toolbox.processus;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.toolbox.service.ServiceTagToolbox;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.AutorisationBean;

/**
 * Processus servant de base à la saisie de tag spécifique dans les toolbox.
 */
public class SaisieTagToolbox extends ProcessusBean {

    public static final String SAISIE_TAG = "formulaireSaisie";
    /**
     * Interface processus d'aide à la codification.
     *
     * @param infoBean com.jsbsoft.jtf.core.InfoBean
     */
    public SaisieTagToolbox(InfoBean infoBean) {
        super(infoBean);
    }

    @Override
    protected boolean traiterAction() {
        final AutorisationBean autorisations = (AutorisationBean) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            String idTagToolbox = infoBean.get("idTag", String.class);
            String extension = infoBean.getNomExtension();
            ServiceTagToolbox serviceTagToolbox = ApplicationContextManager.getBean(extension, idTagToolbox, ServiceTagToolbox.class);
            if (serviceTagToolbox != null) {
                infoBean.set(SAISIE_TAG, serviceTagToolbox.generateUrlTag(infoBean));
                infoBean.setEcranLogique("SAISIE");
            }
        }
        return false;
    }
}
