package com.kosmos.toolbox.provider;

import java.util.Map;

import com.kosmos.toolbox.bean.TagValue;

/**
 * Created by olivier.camon on 24/06/15.
 */
public interface TagDataProvider {

    String getForId();

    void populateData(Map<String, String> data, TagValue tagValue);

}
