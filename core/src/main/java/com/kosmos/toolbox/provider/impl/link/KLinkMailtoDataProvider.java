package com.kosmos.toolbox.provider.impl.link;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.provider.impl.AbstractTagDataProvider;

/**
 * Created on 22/06/15.
 */
public class KLinkMailtoDataProvider extends AbstractTagDataProvider {

    final static Pattern emailPattern = Pattern.compile("(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*)@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])");

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        final boolean process = StringUtils.isNotBlank(tagValue.getValue());
        if (StringUtils.isBlank(data.get("EMAIL")) && process) {
            final int endIndex = !tagValue.getValue().contains("?") ? tagValue.getValue().length() : tagValue.getValue().indexOf("?");
            if(endIndex != -1) {
                final String email = tagValue.getValue().substring(0, endIndex).replace("mailto:", StringUtils.EMPTY);
                final Matcher emailMatcher = emailPattern.matcher(email);
                if (emailMatcher.matches()) {
                    data.put("EMAIL", email);
                }
            }
        }
        final Map<String, String> params = getParams(tagValue.getValue());
        if (StringUtils.isBlank(data.get("SUBJECT")) && process) {
            data.put("SUBJECT", params.get("SUBJECT"));
        }
        if (StringUtils.isBlank(data.get("BODY")) && process) {
            data.put("BODY", params.get("BODY"));
        }
    }

    private Map<String, String> getParams(final String email) {
        final Map<String, String> realParams = new HashMap<>();
        final int startIndex = email.indexOf("?");
        if(startIndex != -1) {
            final String stringParams = email.substring(startIndex + 1);
            final String[] params = stringParams.split("&");
            for(String currentPair : params) {
                final String[] keyValue = currentPair.split("=");
                if(keyValue.length == 2) {
                    realParams.put(keyValue[0].toUpperCase(), keyValue[1]);
                }
            }
        }
        return realParams;
    }
}
