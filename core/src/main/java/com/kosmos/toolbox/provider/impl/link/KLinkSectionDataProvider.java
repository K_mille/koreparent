package com.kosmos.toolbox.provider.impl.link;

import java.util.Map;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.provider.impl.AbstractTagDataProvider;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Created on 22/06/15.
 */
public class KLinkSectionDataProvider extends AbstractTagDataProvider {

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(data.get("CODE_RUBRIQUE"));
        if (rubriqueBean != null) {
            data.put("LIBELLE_CODE_RUBRIQUE", rubriqueBean.getIntitule());
        }
    }
}
