package com.kosmos.toolbox.provider.impl.tag;

import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.provider.impl.AbstractTagDataProvider;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * Created on 04/08/15.
 */
public class AbstractKTagDataProvider extends AbstractTagDataProvider {

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        splitValues(data, tagValue.getValue());
    }

    protected void splitValues(final Map<String, String> data, final String tagValue) {
        final String[] values = StringUtils.split(StringUtils.defaultString(tagValue),"#");
        for (String currentValue : values) {
            final String[] keyValue = currentValue.split("=");
            if (keyValue.length == 2) {
                data.put(keyValue[0], keyValue[1]);
            }
        }
    }

    protected void retrieveSectionInfosExceptDynamik(final Map<String, String> data) {
        final String sectionCode = data.get("CODE_RUBRIQUE");
        if (StringUtils.isNotBlank(sectionCode) && !"DYNAMIK".equals(sectionCode)) {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(sectionCode);
            if (rubriqueBean != null) {
                data.put("LIBELLE_CODE_RUBRIQUE", rubriqueBean.getIntitule());
            }
        }
    }

    protected void retrieveStructureInfosExceptDynamik(final Map<String, String> data) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final String structureCode = data.get("CODE_RATTACHEMENT");
        if (StringUtils.isNotBlank(structureCode) && !"DYNAMIK".equals(structureCode)) {
            final StructureModele structure = serviceStructure.getByCodeLanguage(structureCode, LangueUtil.getIndiceLocaleDefaut());
            if(structure != null) {
                data.put("LIBELLE_CODE_RATTACHEMENT", structure.getLibelleAffichable());
            }
        }
    }

    protected void retrieveDsiInfosExceptDynamik(final Map<String, String> data) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final String dsiCodes = data.get("DIFFUSION_PUBLIC_VISE");
        if (StringUtils.isNotBlank(dsiCodes) && !"DYNAMIK".equals(dsiCodes)) {
            data.put("LIBELLE_DIFFUSION_PUBLIC_VISE", serviceGroupeDsi.getIntitules(dsiCodes));
        }
    }
}
