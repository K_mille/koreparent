package com.kosmos.toolbox.provider.impl.tag;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.provider.impl.AbstractTagDataProvider;

/**
 * Created on 03/08/15.
 */
public class KTagServiceDataProvider extends AbstractTagDataProvider {

    private static final Pattern SERVICE_PATTERN = Pattern.compile("\\[service(_debut)*;([A-Za-z0-9_\\-]+)\\](\\[service_(link|contenu);*([^\\[\\];]+)*)*");

    @Override
    public void populateData(final Map<String, String> data, final TagValue tagValue) {
        final Matcher matcher = SERVICE_PATTERN.matcher(tagValue.getValue());
        if (matcher.find()) {
            data.put("code_service", matcher.group(2));
            data.put("choix_tag_service", matcher.group(4));
            data.put("nom", matcher.group(5));
        }
    }
}
