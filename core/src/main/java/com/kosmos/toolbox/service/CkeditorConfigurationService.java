package com.kosmos.toolbox.service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import com.kosmos.toolbox.config.CkeditorConfigurerUtils;

/**
 * Created on 10/05/2015.
 */
public class CkeditorConfigurationService {

    public static final String ID_BEAN = "ckeditorConfigurationService";

    private final Map<String, Map<UUID, String>> usersConfigurationsMap;

    public CkeditorConfigurationService(){
        usersConfigurationsMap = new HashMap<>();
    }

    public UUID getWeakConfigurationKey(String userCode, String idExtension, String confKey) {
        final String confPath = CkeditorConfigurerUtils.getConfPathForKey(idExtension, confKey);
        if (!usersConfigurationsMap.containsKey(userCode)) {
            usersConfigurationsMap.put(userCode, new HashMap<UUID, String>());
        }
        final Map<UUID, String> currentUserConfigurationsMap = usersConfigurationsMap.get(userCode);
        final UUID weakKey = UUID.randomUUID();
        currentUserConfigurationsMap.put(weakKey, confPath);
        return weakKey;
    }

    public String getConfPathFromWeakKey(String userCode, UUID weakKey) {
        if (usersConfigurationsMap.containsKey(userCode)) {
            final Map<UUID, String> userConfigurationsMap = usersConfigurationsMap.get(userCode);
            if (userConfigurationsMap.containsKey(weakKey)) {
                final String weakConfPath = userConfigurationsMap.get(weakKey);
                userConfigurationsMap.remove(weakKey);
                return weakConfPath;
            }
        }
        return CkeditorConfigurerUtils.getDefaultConfPath();
    }

}
