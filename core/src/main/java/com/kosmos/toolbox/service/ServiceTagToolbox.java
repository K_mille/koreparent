package com.kosmos.toolbox.service;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.core.autorisation.ActionPermission;
import com.kportal.core.autorisation.Permission;
import com.kportal.extension.module.DefaultModuleImpl;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Service gérant les tags toolbox nécessitant une jsp
 */
public abstract class ServiceTagToolbox extends DefaultModuleImpl {


    private String path;

    public void setPath(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }

    /**
     * Calcule l'url d'une JSP permettant de saisir le tag.
     * Il peut se charger de récupérer depuis l'infobean les informations pour savoir si le lien est un lien
     * de création ou de modification du tag.
     * @param infoBean l'infobean sert d'une part à récupérer les infos de l'appel courant, et malheureusement à setter les infos pour les jsp de saisie
     * @return l'url du tag à saisir
     */
    public abstract String generateUrlTag(InfoBean infoBean);

    public boolean isAccessible() {
        boolean hasAccess = Boolean.TRUE;
        for (Permission permission : getPermissions()) {
            hasAccess = Boolean.FALSE;
            ContexteUniv ctx = ContexteUtil.getContexteUniv();
            if (ctx != null && ctx.getAutorisation() != null) {
                if (permission.getActions().isEmpty()) {
                    hasAccess = hasAllActionAccess(permission, ctx);
                } else {
                    hasAccess = hasGenericAccess(permission, ctx);
                }
            }
        }
        return hasAccess;
    }

    private boolean hasGenericAccess(Permission permission, ContexteUniv ctx) {
        boolean hasAcces = Boolean.TRUE;
        int i = 0;
        while (hasAcces && i < permission.getActions().size()) {
            ActionPermission action = permission.getActions().get(i);
            PermissionBean permissionBean = new PermissionBean(permission.getId(), permission.getCode(), action.getCode());
            hasAcces = hasAcces(permissionBean, ctx);
            i++;
        }
        return hasAcces;
    }

    protected boolean hasAllActionAccess(Permission permission, ContexteUniv ctx) {
        PermissionBean permissionBean = new PermissionBean(permission.getId(), permission.getCode(), "");
        return hasAcces(permissionBean, ctx);
    }

    private boolean hasAcces(PermissionBean permissionBean, ContexteUniv ctx) {
        return ctx.getAutorisation().getListePermissions().get(permissionBean.getChaineSerialisee()) != null;
    }

}
