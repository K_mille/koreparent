package com.kosmos.toolbox.service.impl;

import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.toolbox.bean.TagValue;
import com.kosmos.toolbox.service.ServiceTagTypeToolbox;
import com.kportal.extension.module.ModuleHelper;
import com.univ.utils.EscapeString;

/**
 * Service permettant de gérer les tags d'insertion de fiche
 */
public class ServiceKList extends ServiceTagTypeToolbox {

    public static final String TYPE_TAG = "list";

    public static final String TAG_VALUE = "tagValue";

    public static final String LIST_TYPES = "LIST_TYPE";

    @Override
    public String generateUrlTag(InfoBean infoBean) {
        infoBean.set("TOOLBOX", "LIEN_REQUETE");
        infoBean.set("LISTE_INCLUSE", "1");
        infoBean.set("FCK_PLUGIN", "true");
        infoBean.set("LANGUE_FICHE", "0");
        final String value = EscapeString.unescapeURL(StringUtils.defaultString(infoBean.get("FORMATTER_VALUE", String.class)));
        final TagValue listValue = new TagValue();
        listValue.setValue(value);
        identifyTag(listValue, TYPE_TAG);
        infoBean.set(TAG_VALUE, listValue);
        infoBean.set(LIST_TYPES, getTagViewDescriptors(listValue, TYPE_TAG));
        return ModuleHelper.getRelativePathExtension(this) + getPath();
    }

    @Override
    protected Pattern getPatternFromFormatter(String formatter) {
        String stringPattern = String.format("\\Q%s\\E", formatter.replaceFirst("#\\([A-Za-z0-9\\.\\-_]*\\)", "\\\\E([^#]*)\\\\Q"));
        stringPattern = stringPattern.replaceAll("#\\([A-Za-z0-9\\.\\-_]*\\)", "\\\\E(.*)\\\\Q");
        stringPattern = stringPattern.replace("\\Q\\E", StringUtils.EMPTY);
        return Pattern.compile(stringPattern);
    }

}
