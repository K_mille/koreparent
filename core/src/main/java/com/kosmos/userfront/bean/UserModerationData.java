package com.kosmos.userfront.bean;

import java.util.Date;

import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.validation.constraint.NotNullNorEmpty;
import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Bean contenant les informations permettant de gérer la modération. Il contient l'ensemble des informations de l'utilisateur (sauf le mdp heureusement).
 */
public class UserModerationData extends AbstractPersistenceBean implements RegistrationData {

    private static final long serialVersionUID = -5636829679933723994L;

    @NotNullNorEmpty
    private String email;

    @NotNullNorEmpty
    private String login;

    private String firstName;

    private String lastName;

    private String idLocaleKportal;

    private String aliasSite;

    private ModerationState moderationState;

    private String moderationMessage;

    private Date creationDate;

    private Long parentIdRegistration;

    public String getModerationMessage() {
        return moderationMessage;
    }

    public void setModerationMessage(final String moderationMessage) {
        this.moderationMessage = moderationMessage;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(final Date creationDate) {
        this.creationDate = creationDate;
    }

    public ModerationState getModerationState() {
        return moderationState;
    }

    public void setModerationState(final ModerationState moderationState) {
        this.moderationState = moderationState;
    }

    public Long getParentIdRegistration() {
        return parentIdRegistration;
    }

    public void setParentIdRegistration(final Long parentIdRegistration) {
        this.parentIdRegistration = parentIdRegistration;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getIdLocaleKportal() {
        return idLocaleKportal;
    }

    public void setIdLocaleKportal(final String idLocaleKportal) {
        this.idLocaleKportal = idLocaleKportal;
    }

    public String getAliasSite() {
        return aliasSite;
    }

    public void setAliasSite(final String aliasSite) {
        this.aliasSite = aliasSite;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }
}
