package com.kosmos.userfront.bean;

import java.util.Date;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.userfront.validation.constraint.PasswordConfirmation;
import com.kosmos.validation.constraint.NotNullNorEmpty;
import com.kosmos.validation.constraint.PasswordSize;

/**
 * Bean contenant les informations permettant de créer un utilisateur kportal ainsi que les contraintes nécessaire.
 */
@PasswordConfirmation
public class UserRegistrationData implements RegistrationData {

    private static final long serialVersionUID = 8462249282596891924L;

    @Size(max = 64)
    @NotNullNorEmpty
    private String login;

    @NotNullNorEmpty
    @Size(max = 255)
    private String firstName;

    @NotNullNorEmpty
    @Size(max = 255)
    private String lastName;

    @Email
    @NotNullNorEmpty
    @Size(max = 255)
    private String email;

    @PasswordSize
    private String password;

    private String passwordConfirmation;

    private String idLocaleKportal;

    private String aliasSite;

    private Date birthdate;

    private String ldapCode;

    private String groups;

    private String validationRestriction;

    private String EditionExtension;

    private String title;

    private String institutionsCodes;

    private String interests;

    private String profiles;

    private String dsiGroups;

    private String dsiGroupsFromImport;

    private String roles;

    private String defaultProfile;

    private String mailFormat;

    private String expertMode;

    public String getFirstName() {
        return firstName;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(final Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getLdapCode() {
        return ldapCode;
    }

    public void setLdapCode(final String ldapCode) {
        this.ldapCode = ldapCode;
    }

    public String getGroups() {
        return groups;
    }

    public void setGroups(final String groups) {
        this.groups = groups;
    }

    public String getValidationRestriction() {
        return validationRestriction;
    }

    public void setValidationRestriction(final String validationRestriction) {
        this.validationRestriction = validationRestriction;
    }

    public String getEditionExtension() {
        return EditionExtension;
    }

    public void setEditionExtension(final String editionExtension) {
        EditionExtension = editionExtension;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(final String title) {
        this.title = title;
    }

    public String getInstitutionsCodes() {
        return institutionsCodes;
    }

    public void setInstitutionsCodes(final String institutionsCodes) {
        this.institutionsCodes = institutionsCodes;
    }

    public String getInterests() {
        return interests;
    }

    public void setInterests(final String interests) {
        this.interests = interests;
    }

    public String getProfiles() {
        return profiles;
    }

    public void setProfiles(final String profiles) {
        this.profiles = profiles;
    }

    public String getDsiGroups() {
        return dsiGroups;
    }

    public void setDsiGroups(final String dsiGroups) {
        this.dsiGroups = dsiGroups;
    }

    public String getDsiGroupsFromImport() {
        return dsiGroupsFromImport;
    }

    public void setDsiGroupsFromImport(final String dsiGroupsFromImport) {
        this.dsiGroupsFromImport = dsiGroupsFromImport;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(final String roles) {
        this.roles = roles;
    }

    public String getDefaultProfile() {
        return defaultProfile;
    }

    public void setDefaultProfile(final String defaultProfile) {
        this.defaultProfile = defaultProfile;
    }

    public String getMailFormat() {
        return mailFormat;
    }

    public void setMailFormat(final String mailFormat) {
        this.mailFormat = mailFormat;
    }

    public String getExpertMode() {
        return expertMode;
    }

    public void setExpertMode(final String expertMode) {
        this.expertMode = expertMode;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(final String login) {
        this.login = login;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(final String password) {
        this.password = password;
    }

    public String getPasswordConfirmation() {
        return passwordConfirmation;
    }

    public void setPasswordConfirmation(final String passwordConfirmation) {
        this.passwordConfirmation = passwordConfirmation;
    }

    public String getIdLocaleKportal() {
        return idLocaleKportal;
    }

    public void setIdLocaleKportal(final String idLocaleKportal) {
        this.idLocaleKportal = idLocaleKportal;
    }

    public String getName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getAliasSite() {
        return aliasSite;
    }

    public void setAliasSite(final String aliasSite) {
        this.aliasSite = aliasSite;
    }
}
