package com.kosmos.userfront.module;

import com.kportal.extension.module.composant.Composant;
import com.univ.objetspartages.om.AutorisationBean;

/**
 * Composant gérant le menu du back pour la création de compte utilisateur en front office.
 */
public class UserFrontComposant extends Composant {

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        return autorisation != null && autorisation.possedePermission(UserFrontModule.getPermissionGestion());
    }
}