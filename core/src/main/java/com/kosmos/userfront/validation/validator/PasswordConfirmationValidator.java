package com.kosmos.userfront.validation.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.userfront.bean.UserRegistrationData;
import com.kosmos.userfront.validation.constraint.PasswordConfirmation;

/**
 * Created by olivier.camon on 16/02/15.
 */
public class PasswordConfirmationValidator implements ConstraintValidator<PasswordConfirmation, UserRegistrationData> {

    @Override
    public void initialize(final PasswordConfirmation constraintAnnotation) {
    }

    @Override
    public boolean isValid(final UserRegistrationData value, final ConstraintValidatorContext context) {
        return value != null && StringUtils.equals(value.getPassword(), value.getPasswordConfirmation());
    }
}
