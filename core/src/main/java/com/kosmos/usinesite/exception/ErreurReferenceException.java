package com.kosmos.usinesite.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

public class ErreurReferenceException extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -2773526251216698341L;

    public ErreurReferenceException(final String mes) {
        super(mes);
    }

    public ErreurReferenceException(final String mes, final Throwable t) {
        super(mes, t);
    }
}
