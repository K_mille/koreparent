package com.kosmos.usinesite.exception;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.univ.multisites.InfosSite;

/**
 * Exception qui peut contenir plusieurs messages d'erreur.
 *
 * @author pierre.cosson
 *
 */
public class ErreursSaisieInfosSite extends ErreurApplicative {

    public static final String SEPARATEUR_MESSAGE = ";";

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Liste des messages d'erreur contenus dans l'application.
     */
    private List<String> listeMessagesErreur = new ArrayList<>();

    private InfosSite infosSiteOrigineErreurs = null;

    private transient TemplateSite templateInfossiteOrigineErreurs = null;

    /**
     * Constructeur simple. A utiliser lorsqu'il y a qu'une seule erreur.
     *
     * @param mes
     */
    public ErreursSaisieInfosSite(String mes) {
        super(mes);
        listeMessagesErreur = new ArrayList<>(1);
        listeMessagesErreur.add(mes);
    }

    public ErreursSaisieInfosSite(String mes, InfosSite infosSiteOrigineErreurs, TemplateSite templateInfossiteOrigineErreurs) {
        this(mes);
        this.infosSiteOrigineErreurs = infosSiteOrigineErreurs;
        this.templateInfossiteOrigineErreurs = templateInfossiteOrigineErreurs;
    }

    /**
     * Construire l'exception avec une liste de messages d'erreur. <br/>
     * Lors d'un {@link #getMessage()} la liste des erreurs sera séparé par
     * {@link #SEPARATEUR_MESSAGE}.
     *
     * @param messagesErreur
     */
    public ErreursSaisieInfosSite(List<String> messagesErreur, InfosSite infosSiteOrigineErreurs, TemplateSite templateInfossiteOrigineErreurs) {
        super(StringUtils.join(messagesErreur, SEPARATEUR_MESSAGE));
        listeMessagesErreur = messagesErreur;
        this.infosSiteOrigineErreurs = infosSiteOrigineErreurs;
        this.templateInfossiteOrigineErreurs = templateInfossiteOrigineErreurs;
    }

    /**
     * Récuéprer la liste des messages d'ereur.
     *
     * @return La liste des messages d'erreur. Cette liste contient au minimum
     *         un message d'erreur.
     */
    public List<String> getListeMessagesErreur() {
        return listeMessagesErreur;
    }

    public InfosSite getInfosSiteOrigineErreurs() {
        return infosSiteOrigineErreurs;
    }

    public TemplateSite getTemplateInfossiteOrigineErreurs() {
        return templateInfossiteOrigineErreurs;
    }
}
