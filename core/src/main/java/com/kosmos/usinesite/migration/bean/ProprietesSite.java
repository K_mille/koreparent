package com.kosmos.usinesite.migration.bean;

import java.io.IOException;
import java.util.Collections;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.json.CodecJSon;

public class ProprietesSite {

    private static final Logger LOG = LoggerFactory.getLogger(ProprietesSite.class);

    public int sslMode = 0;

    public int boSslMode = 0;

    public int restriction = 0;

    public int modeReecritureRubrique = 0;

    public int niveauMaxReecritureRubrique = 0;

    public int niveauMinReecritureRubrique = 0;

    public boolean sso = Boolean.FALSE;

    public boolean sitePrincipal = Boolean.FALSE;

    public String urlAccueil = StringUtils.EMPTY;

    public Set<Integer> httpActions = Collections.emptySet();

    public Set<Integer> httpsActions = Collections.emptySet();

    public Set<String> listeHostAlias = Collections.emptySet();

    @Override
    public String toString() {
        String retour = StringUtils.EMPTY;
        try {
            retour = CodecJSon.encodeObjectToJSonInString(this);
        } catch (final IOException e) {
            LOG.debug("unable to encode object to json", e);
        }
        return retour;
    }
}
