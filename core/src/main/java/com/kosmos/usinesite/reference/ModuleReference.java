package com.kosmos.usinesite.reference;

import java.util.Collection;
import java.util.Map;

import com.kportal.extension.module.IModule;

public interface ModuleReference {

    /**
     * méthode d'initialisation pour charger les références
     */
    void init();

    /**
     *  la liste des modules sur lesquels on va calculer les références
     *  si null = tous les modules
     *  si vide = aucun
     */
    Collection<IModule> getModules();

    /**
     * la liste des modules à mettre à jour pour les codes de fiche
     */
    Map<String, Collection<String>> getModulesBySearchStringForCode();

    /**
     * la liste des modules à mettre à jour pour les ids de fiche
     */
    Map<String, Collection<String>> getModulesBySearchStringForId();
}
