package com.kosmos.usinesite.reference;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;

public interface ServiceBeanReference {

    String S1 = "(?<value>%1$s)";

    Collection<BeanReference> check(Map<String, ? extends Serializable> beansOrig, ReferenceProcessor processor) throws ErreurReferenceException;
}
