package com.kosmos.usinesite.reference.impl;

import java.io.Serializable;
import java.util.*;

import com.kosmos.usinesite.exception.ErreurReferenceException;
import com.kosmos.usinesite.reference.BeanReference;
import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kosmos.usinesite.reference.processor.ReferenceProcessor;
import com.kportal.extension.module.bean.EncadreBeanExport;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.services.ServiceEncadre;

public class EncadreServiceBeanReference implements ServiceBeanReference {

    private ServiceEncadre serviceEncadre;

    public void setServiceEncadre(ServiceEncadre serviceEncadre) {
        this.serviceEncadre = serviceEncadre;
    }

    @Override
    public Collection<BeanReference> check(final Map<String, ? extends Serializable> beansOrig, final ReferenceProcessor processor) throws ErreurReferenceException {
        // cette méthode regarde si le meta existe et si c'est le cas, créé un nouvel id
        final EncadreBeanExport encadreBean = (EncadreBeanExport) beansOrig.values().iterator().next();
        final Collection<BeanReference> beansReferences = new ArrayList<>();
        final BeanReference referenceById = new BeanReference();
        referenceById.addModulesBySearchString("\"id_encadre\":\"" + S1 + "\"", Collections.singletonList(encadreBean.getIdModule()));
        final BeanReference referenceByCode = new BeanReference();
        referenceByCode.addModulesBySearchString("\"code\":\"" + S1 + "\"", Collections.singletonList(encadreBean.getIdModule()));
        for (final String codeEncadre : beansOrig.keySet()) {
            EncadreBean encadre = serviceEncadre.getByCode(codeEncadre);
            // si l'encadre n'existe pas
            if (encadre == null) {
                encadre = ((EncadreBeanExport) beansOrig.get(codeEncadre)).getBean();
                referenceById.addNewValueByOldValue(encadre.getId().toString(), "0");
                referenceByCode.addNewValueByOldValue(encadre.getCode(), String.valueOf(System.nanoTime()));
            }else{
                //On passe -1 à l'encadre pour ne pas l'enregistrer en bdd lors de l'étape d'import.
                referenceById.addNewValueByOldValue(encadre.getId().toString(), "-1");
            }
        }
        beansReferences.add(referenceById);
        beansReferences.add(referenceByCode);
        return beansReferences;
    }
}
