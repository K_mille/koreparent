package com.kosmos.usinesite.reference.impl;

import java.util.ArrayList;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;

/**
 * Cette classe initialise les modules de type StructureModele pour les calculs de reférence sur les attributs code rattachement, code structure
 *
 */
public class StructureModeleReference extends DefaultModuleReference {

    @Override
    public void init() {
        modules = new ArrayList<>();
        // les modules cibles pour ajouter les références sont toutes les fiches structures
        for (String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv instanceof StructureModele) {
                modules.add(ReferentielObjets.getObjetByCode(codeObjet));
            }
        }
    }
}
