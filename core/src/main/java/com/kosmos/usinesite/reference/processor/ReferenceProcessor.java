package com.kosmos.usinesite.reference.processor;

import com.kosmos.usinesite.exception.ErreurReferenceException;

public interface ReferenceProcessor {

    void preProcess(String newCodeSite, String oldCodeSite) throws ErreurReferenceException;

    void process(String newCode, String oldCode, String codeObjet, Long newIdMeta, Long oldIdMeta) throws ErreurReferenceException;

    void postProcess(String... args) throws ErreurReferenceException;
}
