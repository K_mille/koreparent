package com.kosmos.usinesite.template.bean;

import java.util.List;

import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.service.ServiceTemplateSite;

/**
 * Représentation d'un template de site.<br/>
 * Un template de site est une intégration graphique qu'il est possible d'appliquer à un site géré par l'usine à site. La personnalisation de ce template est possible via le jeu de
 * JSPs<br/>
 * <br/>
 * <strong>Un dossier de JSPs ne peut être utilisé que par un seul template.</strong> Si cette contrainte n'est pas respecté, l'usine à site ne fonctionnera pas correctement
 * (méthode {@link ServiceTemplateSite#getTemplateSiteParDossier(String)} non fonctionnelle)
 *
 * @author pierre.cosson
 *
 */
public interface TemplateSite {

    /**
     * Identifiant unique d'un template de site.
     *
     * @return
     */
    String getCode();

    /**
     * Intitulé du template.
     *
     * @return
     */
    String getIntitule();

    /**
     * Description du template.
     *
     * @return
     */
    String getDescription();

    /**
     * URL relative de la vignette de présentation du template.
     *
     * @return
     */
    String getUrlVignette();

    /**
     * Dossier de JSP utilisé par le template.<br/>
     * <strong>Un dossier de JSPs ne peut être utilisé que par un seul template.</strong>
     *
     * @return
     */
    String getDossierJSP();

    /**
     * Récupère la propriété du site identifié par le code fourni en paramètre
     *
     * @param code
     *            l'identifiant de la propriété
     * @return la valeur de la propriété ou null si non trouvé.
     */
    TemplateSiteProperty getProprieteComplementaire(String code);

    /**
     * Récupérer l'ensemble des propriétés définies sur le sites
     *
     * @return
     */
    List<TemplateSiteProperty> getListeProprietesComplementaires();
}