package com.kosmos.usinesite.template.bean.impl;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;

public class TemplateSiteImpl implements TemplateSite {

    public String code = StringUtils.EMPTY;

    public String intitule = StringUtils.EMPTY;

    public String description = StringUtils.EMPTY;

    public String urlVignette = StringUtils.EMPTY;

    public String dossierJSP = StringUtils.EMPTY;

    public boolean cssSpecifique = Boolean.FALSE;

    public List<TemplateSiteProperty> listeProprietesComplementaires = Collections.emptyList();

    public Map<String, TemplateSiteProperty> mapProprietesComplementaires = Collections.emptyMap();

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSite#getCode()
     */
    @Override
    public String getCode() {
        return code;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSite#getIntitule()
     */
    @Override
    public String getIntitule() {
        return intitule;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSite#getDescription()
     */
    @Override
    public String getDescription() {
        return description;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSite#getUrlVignette()
     */
    @Override
    public String getUrlVignette() {
        return urlVignette;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSite#getDossierJSP()
     */
    @Override
    public String getDossierJSP() {
        return dossierJSP;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.bean.TemplateSite#isCssSpecifique()
     */
    public boolean isCssSpecifique() {
        return cssSpecifique;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.template.bean.TemplateSite#
     * getListeProprietesComplementaires()
     */
    @Override
    public List<TemplateSiteProperty> getListeProprietesComplementaires() {
        return listeProprietesComplementaires;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.bean.TemplateSite#getProprieteComplementaire
     * (java.lang.String)
     */
    @Override
    public TemplateSiteProperty getProprieteComplementaire(final String code) {
        return mapProprietesComplementaires.get(code);
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setUrlVignette(final String urlVignette) {
        this.urlVignette = urlVignette;
    }

    public void setDossierJSP(final String dossierJSP) {
        this.dossierJSP = dossierJSP;
    }

    public void setCssSpecifique(final boolean cssSpecifique) {
        this.cssSpecifique = cssSpecifique;
    }

    public void setListeProprietesComplementaires(final List<TemplateSiteProperty> listeProprieteComplementaire) {
        this.listeProprietesComplementaires = listeProprieteComplementaire;
        initialiserMapProprietesComplementaire();
    }

    private void initialiserMapProprietesComplementaire() {
        // acces concurent
        mapProprietesComplementaires = new HashMap<>(listeProprietesComplementaires.size());
        for (final TemplateSiteProperty property : listeProprietesComplementaires) {
            mapProprietesComplementaires.put(property.getCode(), property);
        }
    }
}
