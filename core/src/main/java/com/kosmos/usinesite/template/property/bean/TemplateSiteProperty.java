package com.kosmos.usinesite.template.property.bean;

import java.util.List;

/**
 * Propriété de template de site. Ces propriétés servent à lister les informations complémentaires nécéssaires au bon affichage du template.
 *
 * @author pierre.cosson
 *
 */
public interface TemplateSiteProperty {

    String getCode();

    String getLibelle();

    String getDescription();

    boolean isObligatoire();

    /**
     * Retourne la liste des codes des templates auxquels la propriété est restreinte.
     * Une liste vide permet d'indiquer que tous les templates pourront bénéficier de la propriété.
     */
    List<String> getRestrictionTemplates();
}