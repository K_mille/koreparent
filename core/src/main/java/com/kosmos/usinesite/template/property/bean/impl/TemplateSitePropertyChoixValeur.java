package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;

public class TemplateSitePropertyChoixValeur extends AbstractTemplateSiteProperty {

    private List<Entry<String, String>> listeValeurs = Collections.emptyList();

    public List<Entry<String, String>> getListeValeurs() {
        return listeValeurs;
    }

    public void setListeValeurs(final List<Entry<String, String>> listeValeurs) {
        this.listeValeurs = listeValeurs;
    }
}
