package com.kosmos.usinesite.template.property.bean.impl;

import java.util.Collection;
import java.util.Collections;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyHelper;

public class TemplateSitePropertyFichier extends AbstractTemplateSiteProperty {

    private static final long TAILLE_MAXIMUM_FICHIER;

    static {
        final String tailleMaxJTF = PropertyHelper.getCoreProperty("fichiergw.maxsize");
        if (StringUtils.isNotEmpty(tailleMaxJTF)) {
            TAILLE_MAXIMUM_FICHIER = Long.parseLong(tailleMaxJTF);
        } else {
            TAILLE_MAXIMUM_FICHIER = 1024;
        }
    }

    /**
     * taille exprimée en Ko
     */
    private long tailleMaximumFichier = TAILLE_MAXIMUM_FICHIER;

    private Collection<String> listeExtensionsAutorisees = Collections.emptyList();

    public long getTailleMaximumFichier() {
        return tailleMaximumFichier;
    }

    public void setTailleMaximumFichier(final long tailleMaximumFichier) {
        this.tailleMaximumFichier = tailleMaximumFichier;
    }

    public Collection<String> getListeExtensionsAutorisees() {
        return listeExtensionsAutorisees;
    }

    public void setListeExtensionsAutorisees(final Collection<String> listeExtensionsAutorisees) {
        this.listeExtensionsAutorisees = listeExtensionsAutorisees;
    }
}
