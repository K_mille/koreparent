package com.kosmos.usinesite.template.property.bean.impl;

import org.apache.commons.lang3.StringUtils;

public class TemplateSitePropertyMultilignes extends AbstractTemplateSiteProperty {

    private String valeurDefaut = StringUtils.EMPTY;

    private int tailleMaximum;

    public String getValeurDefaut() {
        return valeurDefaut;
    }

    public void setValeurDefaut(final String valeurDefaut) {
        this.valeurDefaut = valeurDefaut;
    }

    public int getTailleMaximum() {
        return tailleMaximum;
    }

    public void setTailleMaximum(final int tailleMaximum) {
        this.tailleMaximum = tailleMaximum;
    }
}
