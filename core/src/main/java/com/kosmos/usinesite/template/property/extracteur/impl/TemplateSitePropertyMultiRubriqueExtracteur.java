package com.kosmos.usinesite.template.property.extracteur.impl;

import java.util.Collection;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyMultiRubrique;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyMultiRubriqueExtracteur implements TemplateSitePropertyExtracteur<TemplateSitePropertyMultiRubrique, Collection<String>> {

    @Override
    public Collection<String> extraire(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyMultiRubrique property, final InfoBean data) throws Exception {
        return TemplateSiteHelper.getListeStringTemplateSiteProperty(template, property, data);
    }
}
