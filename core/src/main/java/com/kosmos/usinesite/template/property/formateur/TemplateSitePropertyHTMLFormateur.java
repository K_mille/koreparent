package com.kosmos.usinesite.template.property.formateur;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.univ.multisites.InfosSite;

/**
 * Elément permettant de formatter les champs de saisie ou autre servant à
 * représenter la propriété de template.
 *
 * @author pierre.cosson
 *
 * @param <T>
 *            Le {@link TemplateSiteProperty} auquel s'applique ce formateur
 *            HTML.
 */
public interface TemplateSitePropertyHTMLFormateur<T extends TemplateSiteProperty> {

    String formater(InfosSite infosSite, TemplateSite template, T property, FormateurJSP fmt, InfoBean data) throws Exception;
}
