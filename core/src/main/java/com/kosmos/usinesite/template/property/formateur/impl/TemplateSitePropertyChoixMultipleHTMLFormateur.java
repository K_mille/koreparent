package com.kosmos.usinesite.template.property.formateur.impl;

import java.util.Hashtable;
import java.util.List;
import java.util.Set;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyChoixMultiple;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.template.utils.ComposantChoixMultipleInputCheckbox;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyChoixMultipleHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyChoixMultiple> {

    @Override
    public String formater(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyChoixMultiple property, final FormateurJSP fmt, final InfoBean data) throws Exception {
        final String nomComposant = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        final StringBuilder out = new StringBuilder("<div>");
        out.append("<label class=\"colonne\" for=\"").append(nomComposant).append("\">");
        if (property.isObligatoire()) {
            out.append("<span class=\"obligatoire\">").append(property.libelle).append(" * </span>");
        } else {
            out.append(property.libelle);
        }
        out.append(FrontUASHelper.genererMessageInformatif(property.getDescription()));
        out.append("</label>");
        final Hashtable<String, String> listeValeurs = new Hashtable<>(property.getValeurs());
        List<String> valeursDefinies = infosSite.getProprieteComplementaireListString(property.getCode());
        if (valeursDefinies == null) {
            valeursDefinies = property.getValeurDefaut();
        }
        out.append(ComposantChoixMultipleInputCheckbox.genererListeCasesACocher(nomComposant, listeValeurs, valeursDefinies));
        out.append("</div>");
        nettoyerInfoBean(data, listeValeurs.keySet());
        return out.toString();
    }

    private void nettoyerInfoBean(final InfoBean data, final Set<String> listeValeurs) {
        for (final String clef : listeValeurs) {
            data.remove(clef);
        }
    }
}
