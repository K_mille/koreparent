package com.kosmos.usinesite.template.property.formateur.impl;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.FormateurJSP;
import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyMultilignes;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyMultilignesHTMLFormateur implements TemplateSitePropertyHTMLFormateur<TemplateSitePropertyMultilignes> {

    @Override
    public String formater(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyMultilignes property, final FormateurJSP fmt, final InfoBean data) throws Exception {
        final String nomComponsant = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        final StringBuilder out = new StringBuilder();
        out.append("<p><label class=\"colonne\" for=\"").append(nomComponsant).append("\">");
        if (property.isObligatoire()) {
            out.append("<span class=\"obligatoire\">").append(property.libelle).append(" * </span>");
        } else {
            out.append(property.libelle);
        }
        out.append(FrontUASHelper.genererMessageInformatif(property.getDescription()));
        out.append("</label>");
        final String valeur = StringUtils.defaultString(infosSite.getProprieteComplementaireString(property.getCode()), property.getValeurDefaut());
        out.append(FrontUASHelper.genererTexteareaHTML(nomComponsant, nomComponsant, valeur, property.getTailleMaximum(), StringUtils.EMPTY, property.getValeurDefaut(), Boolean.FALSE));
        out.append("</p>");
        return out.toString();
    }
}
