package com.kosmos.usinesite.template.property.service;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.property.formateur.TemplateSitePropertyHTMLFormateur;
import com.kosmos.usinesite.template.property.traitement.TemplateSitePropertyTraitement;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;

public interface ServiceTemplateSiteProperty {

    /**
     * Récupérer l'extracteur de données correspondant à la propriété.
     *
     * @param property la property dont on souhaite connaitre l'extracteur
     * @return l'extracteur du type de la property fourni en parametre
     * @throws ErreurDonneeNonTrouve
     *             une {@link ErreurDonneeNonTrouve} si aucun extracteur ne correspond à la propriété.
     */
    TemplateSitePropertyExtracteur<TemplateSiteProperty, Object> getTemplateSitePropertyExtracteur(TemplateSiteProperty property) throws ErreurDonneeNonTrouve;

    /**
     * Permet de récupérer la classe validant la property fourni en paramètre
     * @param property la property dont on souhaite connaitre le validateur
     * @return le validateur du type de la property fourni en parametre
     * @throws ErreurDonneeNonTrouve
     *             une {@link ErreurDonneeNonTrouve} si aucun validateur ne correspond à la propriété.
     */
    TemplateSitePropertyValidateur<TemplateSiteProperty> getTemplateSitePropertyValidateur(TemplateSiteProperty property) throws ErreurDonneeNonTrouve;

    /**
     * Permet de récupérer la classe de traitement supplémentaire de la property fourni en paramètre
     * @param property la property dont on souhaite connaitre le traitement supplémentaire à faire
     * @return le traitement supplémentaire du type de la property fourni en parametre
     * @throws ErreurDonneeNonTrouve
     *             une {@link ErreurDonneeNonTrouve} si aucun traitement ne correspond à la propriété.
     */
    TemplateSitePropertyTraitement<TemplateSiteProperty> getTemplateSitePropertyTraitement(TemplateSiteProperty property) throws ErreurDonneeNonTrouve;

    /**
     * Permet de récupérer la classe du formateur de la property fourni en paramètre
     * @param property la property dont on souhaite connaitre le formateur pour l'affichage
     * @return le formateur du type de la property fourni en parametre
     * @throws ErreurDonneeNonTrouve
     *             une {@link ErreurDonneeNonTrouve} si aucun formateur ne correspond à la propriété.
     */
    TemplateSitePropertyHTMLFormateur<TemplateSiteProperty> getTemplateSitePropertyFormateur(TemplateSiteProperty property) throws ErreurDonneeNonTrouve;
}