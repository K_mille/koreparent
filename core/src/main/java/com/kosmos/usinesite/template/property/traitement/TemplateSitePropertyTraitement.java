package com.kosmos.usinesite.template.property.traitement;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;

/**
 * Lancer des traitements extérieurs lié à la propriété et qu'il faut exécuter
 * la sauvegarde d'un {@link InfosSite}.
 *
 * @author pierre.cosson
 *
 * @param <T>
 *            Le {@link TemplateSiteProperty} auquel s'applique ce traitement.
 */
public interface TemplateSitePropertyTraitement<T extends TemplateSiteProperty> {

    /**
     * Traitements liés à une propriété de template.
     *
     * @param infosSite
     * @param template
     * @param property
     * @param data
     * @throws Exception
     *             Erreur durant les dtraitements.
     */
    void traiter(InfosSiteImpl infosSite, TemplateSite template, T property, InfoBean data) throws Exception;
}
