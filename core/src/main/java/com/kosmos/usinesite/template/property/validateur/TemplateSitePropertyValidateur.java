package com.kosmos.usinesite.template.property.validateur;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.univ.multisites.InfosSite;

/**
 * Valider les données saisies pour une propriété donnée.
 *
 * @author pierre.cosson
 *
 * @param <T>
 *            Le {@link TemplateSiteProperty} auquel s'applique ce validateur de
 *            donnée.
 */
public interface TemplateSitePropertyValidateur<T extends TemplateSiteProperty> {

    String MSG_ERREUR_PROPRIETE_OBLIGATOIRE = "La propriété du template '%s' est obligatoire.";

    /**
     * Valider les données d'une {@link TemplateSiteProperty} saisies et
     * contenues dans le {@link InfoBean}
     *
     * @param infosSite
     * @param template
     * @param property
     * @param data
     * @throws Exception
     *             Une {@link ErreursSaisieInfosSite} est retournée contenant
     *             toutes les erreurs sur les données de la property.
     */
    void valider(InfosSite infosSite, TemplateSite template, T property, InfoBean data) throws Exception;
}
