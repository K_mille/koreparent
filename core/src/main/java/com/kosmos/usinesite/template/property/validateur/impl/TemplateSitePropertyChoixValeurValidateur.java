package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;
import java.util.Map.Entry;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyChoixValeur;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

/**
 * Valider des données saisies dans le cadre d'une propiété de template de type
 * fichier.
 *
 * @author pierre.cosson
 *
 */
public class TemplateSitePropertyChoixValeurValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyChoixValeur> {

    protected static final String MSG_ERREUR_PROPRIETE_NON_AUTORISEE = "La propriété '%s' du template contient une valeur non autorisée : '%s'";

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.template.validateur.TemplateSitePropertyValidateur
     * #valider(com.univ.multisites.InfosSite,
     * com.kosmos.usinesite.template.bean.site.TemplateSite,
     * com.kosmos.usinesite.template.bean.property.TemplateSiteProperty,
     * com.jsbsoft.jtf.core.InfoBean)
     */
    public void valider(InfosSite infosSite, TemplateSite template, TemplateSitePropertyChoixValeur property, InfoBean data) throws Exception {
        String valeur = TemplateSiteHelper.getStringTemplateSiteProperty(template, property, data);
        ArrayList<String> fluxErreurs = new ArrayList<>();
        testChampObligatoire(valeur, property, fluxErreurs);
        testListeValeurs(valeur, property, fluxErreurs);
        traiterFluxErreurs(fluxErreurs, infosSite, template);
    }

    protected void testChampObligatoire(String valeur, TemplateSitePropertyChoixValeur property, ArrayList<String> fluxErreurs) {
        if (property.isObligatoire() && StringUtils.isEmpty(valeur)) {
            String messageErreur = String.format(TemplateSitePropertyValidateur.MSG_ERREUR_PROPRIETE_OBLIGATOIRE, property.getLibelle());
            fluxErreurs.add(messageErreur);
        }
    }

    protected void testListeValeurs(String valeur, TemplateSitePropertyChoixValeur property, ArrayList<String> fluxErreurs) {
        if (StringUtils.isEmpty(valeur)) {
            return;
        }
        for (Entry<String, String> choixPossible : property.getListeValeurs()) {
            if (choixPossible.getKey().equals(valeur)) {
                return;
            }
        }
        String messageErreur = String.format(MSG_ERREUR_PROPRIETE_NON_AUTORISEE, property.getLibelle(), valeur);
        fluxErreurs.add(messageErreur);
    }

    protected void traiterFluxErreurs(ArrayList<String> fluxErreurs, InfosSite infosSite, TemplateSite template) throws ErreursSaisieInfosSite {
        if (!fluxErreurs.isEmpty()) {
            throw new ErreursSaisieInfosSite(fluxErreurs, infosSite, template);
        }
    }
}
