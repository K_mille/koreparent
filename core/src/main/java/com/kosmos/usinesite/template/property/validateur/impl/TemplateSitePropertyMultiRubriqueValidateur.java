package com.kosmos.usinesite.template.property.validateur.impl;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyMultiRubrique;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyMultiRubriqueValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyMultiRubrique> {

    @Override
    public void valider(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyMultiRubrique property, final InfoBean data) throws Exception {
        final String valeur = TemplateSiteHelper.getStringTemplateSiteProperty(template, property, data);
        if (property.isObligatoire() && StringUtils.isEmpty(valeur)) {
            final String messageErreur = String.format(TemplateSitePropertyValidateur.MSG_ERREUR_PROPRIETE_OBLIGATOIRE, property.getLibelle());
            throw new ErreursSaisieInfosSite(messageErreur, infosSite, template);
        }
    }
}
