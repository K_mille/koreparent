package com.kosmos.usinesite.template.property.validateur.impl;

import java.util.ArrayList;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyMultilignes;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kosmos.usinesite.template.utils.TemplateSiteHelper;
import com.univ.multisites.InfosSite;

public class TemplateSitePropertyMultilignesValidateur extends AbstractTemplateSitePropertyStringValidateur implements TemplateSitePropertyValidateur<TemplateSitePropertyMultilignes> {

    @Override
    public void valider(final InfosSite infosSite, final TemplateSite template, final TemplateSitePropertyMultilignes property, final InfoBean data) throws Exception {
        final String valeur = TemplateSiteHelper.getStringTemplateSiteProperty(template, property, data);
        final ArrayList<String> fluxErreurs = new ArrayList<>();
        testChampObligatoire(valeur, property, fluxErreurs);
        testTailleMaximum(valeur, property.getTailleMaximum(), property.getLibelle(), fluxErreurs);
        traiterFluxErreurs(fluxErreurs, infosSite, template);
    }
}
