package com.kosmos.usinesite.template.service;

import java.util.List;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.bean.impl.TemplateSiteImpl;

/**
 * Effectuer des opération sur les {@link TemplateSite} de l'application.
 *
 * @author pierre.cosson
 *
 */
public interface ServiceTemplateSite {

    /**
     * Récupérer la liste des {@link TemplateSiteImpl} de l'application.
     *
     * @return La liste des {@link TemplateSite} de l'application
     */
    List<TemplateSite> getListeTemplatesSite() throws Exception;

    /**
     * Récupérer un {@link TemplateSite} via son code (insensible à la casse).
     *
     * @param code
     *            Code du {@link TemplateSite} à récupérer
     * @return Le {@link TemplateSite} correspondant au code.
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de trouver le {@link TemplateSite} correspondant au code.</li>
     *             </ul>
     */
    TemplateSite getTemplateSiteParCode(String code) throws Exception;

    /**
     * Récupérer le template de site utilisé par un site.
     *
     * @param codeSite
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurDonneeNonTrouve} : impossible de trouver le {@link TemplateSite} correspondant au site.</li>
     *             </ul>
     */
    TemplateSite getTemplateSiteParCodeSite(String codeSite) throws Exception;

    /**
     * Récupérer un template de site via le dossier de JSP qu'il utilisé par ce dernier.
     *
     * @deprecated Cette méthode sert uniquement pour migrer un site déclaré façon 5.1 vers les nouveaux site.
     *
     * @param jspFo
     *            path du jeu de JSP utilisé par le template (exemple : /jsp)
     * @return le {@link TemplateSite} utilisant le jeu de JSP
     * @throws Exception <ul>
     *            <li> {@link ErreurDonneeNonTrouve} : impossible de trouver le {@link TemplateSite} correspondant au dossier.</li>
     *            </ul>
     *
     */
    @Deprecated
    TemplateSite getTemplateSiteParDossier(String jspFo) throws Exception;
}