package com.kosmos.usinesite.template.service;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class ServiceTemplateSiteFactory {

    public static final String ID_BEAN = "serviceTemplateSiteFactory";

    private ServiceTemplateSite serviceTemplateSiteInstance = null;

    /**
     * Récupérer le singleton de la factory.
     *
     * @return
     */
    private static ServiceTemplateSiteFactory getInstance() {
        return (ServiceTemplateSiteFactory) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    /**
     * Récupérer l'instance de {@link ServiceTemplateSite} de l'application.
     *
     * @return L'instance (singleton).
     */
    public static ServiceTemplateSite getServiceTemplateSite() {
        return getInstance().serviceTemplateSiteInstance;
    }

    public void setServiceTemplateSite(final ServiceTemplateSite serviceTemplateSite) {
        serviceTemplateSiteInstance = serviceTemplateSite;
    }
}
