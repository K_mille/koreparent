package com.kosmos.usinesite.template.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections4.multimap.AbstractListValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.apache.commons.lang3.StringUtils;
import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.ServiceActivator;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.bean.impl.TemplateSiteImpl;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.service.ServiceTemplateSite;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.AbstractBeanManager;
import com.univ.multisites.InfosSite;
import com.univ.multisites.dao.InfosSiteDao;

/**
 * Service permettant de récupérer les {@link TemplateSite} injectés via spring. De plus, ce service met en cache les {@link TemplateSite}.
 *
 * @author pierre.cosson
 *
 */
public class ServiceTemplateSiteImpl extends AbstractBeanManager implements ServiceTemplateSite {

    /**
     * Dao utilisé pour récupérer les sauvegarde de site
     */
    private InfosSiteDao infosSiteDao = null;

    /**
     * Cache utilisé pour contenir la liste de tous les sites de l'application.
     */
    private List<TemplateSite> listeTemplatesSite = Collections.emptyList();

    @Override
    @ServiceActivator(inputChannel = CHANNEL_MODULE)
    @Order(ORDER_PROJECT)
    public void update() {
        super.update();
    }

    @Override
    public void refresh() {
        // Récupération des templates de site associées aux extensions actives
        listeTemplatesSite = new ArrayList<>(ApplicationContextManager.getAllActivatedBeansOfType(TemplateSiteImpl.class).values());
        // Récupération des propriétés de template associées aux extensions actives
        final Collection<TemplateSiteProperty> listeProprietesTemplates = ApplicationContextManager.getAllActivatedBeansOfType(TemplateSiteProperty.class).values();
        // Affectation des propriétés aux templates
        AbstractListValuedMap<String, TemplateSiteProperty> mapProprietesParTemplate = new ArrayListValuedHashMap<>();
        for (TemplateSiteProperty propriete : listeProprietesTemplates) {
            for (TemplateSite templateSite : listeTemplatesSite) {
                if (propriete.getRestrictionTemplates().isEmpty() || propriete.getRestrictionTemplates().contains(templateSite.getCode())) {
                    mapProprietesParTemplate.put(templateSite.getCode(), propriete);
                }
            }
        }
        // Ajout des propriétés aux templates
        for (TemplateSite templateSite : listeTemplatesSite) {
            if (templateSite instanceof TemplateSiteImpl) {
                ((TemplateSiteImpl) templateSite).setListeProprietesComplementaires(mapProprietesParTemplate.get(templateSite.getCode()));
            }
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.kosmos.usinesite.service.impl.ServiceTemplateSite#
     * getListeTemplateSiteApplication()
     */
    @Override
    public List<TemplateSite> getListeTemplatesSite() {
        return listeTemplatesSite;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.service.impl.ServiceTemplateSite#getTemplateSiteParCode
     * (java.lang.String)
     */
    @Override
    public TemplateSite getTemplateSiteParCode(final String code) throws ErreurDonneeNonTrouve {
        for (final TemplateSite templateSite : listeTemplatesSite) {
            if (StringUtils.equalsIgnoreCase(templateSite.getCode(), code)) {
                return templateSite;
            }
        }
        throw new ErreurDonneeNonTrouve(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_AUCUN_TEMPLATE_CODE") + code);
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.service.ServiceTemplateSite#getTemplateSiteParCodeSite
     * (java.lang.String)
     */
    @Override
    public TemplateSite getTemplateSiteParCodeSite(final String codeSite) throws Exception {
        final InfosSite sauvegarde = infosSiteDao.getInfosSite(codeSite);
        return getTemplateSiteParCode(sauvegarde.getCodeTemplate());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.usinesite.service.ServiceTemplateSite#getTemplateSiteParDossier
     * (java.lang.String)
     */
    @Override
    public TemplateSite getTemplateSiteParDossier(final String dossierJsp) throws ErreurDonneeNonTrouve {
        for (final TemplateSite template : getListeTemplatesSite()) {
            if (StringUtils.equalsIgnoreCase(template.getDossierJSP(), dossierJsp)) {
                return template;
            }
        }
        throw new ErreurDonneeNonTrouve("Aucun TemplateSite pour le dossier : " + dossierJsp);
    }

    /**
     * @param infosSiteDao
     */
    public void setInfosSiteDao(final InfosSiteDao infosSiteDao) {
        this.infosSiteDao = infosSiteDao;
    }
}
