package com.kosmos.usinesite.template.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.property.bean.impl.TemplateSitePropertyChoixMultiple;
import com.kosmos.usinesite.utils.FrontUASHelper;
import com.kosmos.usinesite.utils.SaisieInfosSiteInfoBeanHelper;

public abstract class TemplateSiteHelper {

    /**
     * Extraire la valeur d'une {@link TemplateSiteProperty} de l'infoBean.
     *
     * @param template
     *            le template contenant la propriété
     * @param property
     *            la propriété
     * @param data
     *            la source de données contenant la valeur de la propriété.
     * @return La valeur de la propriété si elle est définie sinon <code>null</code>.
     */
    public static Object getValeurTemplateSiteProperty(final TemplateSite template, final TemplateSiteProperty property, final InfoBean data) {
        final String cleInfoBean = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        return data.get(cleInfoBean);
    }

    /**
     * Extraire une chaine de caractéres du {@link InfoBean} correspondant à la valeur de la {@link TemplateSiteProperty}.
     *
     * @param template
     *            le template contenant la propriété
     * @param property
     *            la propriété
     * @param data
     *            la source de données contenant la valeur de la propriété.
     * @return une chaine de caractéres correspondant à la valeur de al propriété. Si cette propriété est vide ou non définie c'est la valeur vide qui est retournée.
     */
    public static String getStringTemplateSiteProperty(final TemplateSite template, final TemplateSiteProperty property, final InfoBean data) {
        final Object valeur = getValeurTemplateSiteProperty(template, property, data);
        if (valeur != null && valeur instanceof String) {
            return (String) valeur;
        } else {
            return StringUtils.EMPTY;
        }
    }

    /**
     * Extraire un {@link FichierSimpleUpload} du {@link InfoBean} correspondant à la valeur de la {@link TemplateSiteProperty}.
     *
     * @param template
     *            le template contenant la propriété
     * @param property
     *            la propriété
     * @param data
     *            la source de données contenant la valeur de la propriété.
     * @return le {@link FichierSimpleUpload} correspondant à la {@link TemplateSiteProperty}. S'il n'y a pas de fichier c'est uen instance vide qui sera retournée.
     * @see ComposantFichierSimpleUpload#getFichierSimpleUploadDepuisInfoBean(String, InfoBean)
     */
    public static FichierSimpleUpload getFichierSimpleUploadTemplateSiteProperty(final TemplateSite template, final TemplateSiteProperty property, final InfoBean data) {
        final String cleInfoBean = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        return ComposantFichierSimpleUpload.getFichierSimpleUploadDepuisInfoBean(cleInfoBean, data);
    }

    /**
     * Extraire la liste de chaine de caractéres correspondant à la valeur de la {@link TemplateSiteProperty}.
     *
     * @param template
     *            le template contenant la propriété
     * @param property
     *            la propriété
     * @param data
     *            la source de données contenant la valeur de la propriété.
     * @return la liste de valeurs ou une liste vide si le composant ne contient aucune valeur ou n'est pas défini dans le {@link InfoBean}
     */
    public static Collection<String> getListeStringTemplateSiteProperty(final TemplateSite template, final TemplateSiteProperty property, final InfoBean data) {
        final String cleInfoBean = FrontUASHelper.genererNameInputProprieteTemplate(template, property);
        return SaisieInfosSiteInfoBeanHelper.getListeText(data, cleInfoBean);
    }

    public static Collection<String> getChoixMultipleTemplateSiteProperty(final TemplateSite template, final TemplateSiteProperty property, final InfoBean data) {
        final List<String> ids = new ArrayList<>(((TemplateSitePropertyChoixMultiple) property).getValeurs().keySet());
        return ComposantChoixMultipleInputCheckbox.getListeClefs(data, ids);
    }
}
