package com.kosmos.usinesite.utils;

import java.io.File;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.multisites.InfosSite;

/**
 * Classe utilitaire contenant toutes les listes de
 *
 * @author pierre.cosson
 *
 */
public abstract class InfosSiteHelper {

    private static final Logger LOGGER = LoggerFactory.getLogger(InfosSiteHelper.class);

    public static final List<Entry<Integer, String>> MODE_REECRITURE_RUBRIQUE;

    private static final Pattern codeMetierPattern = Pattern.compile("[a-zA-Z0-9\\-_@\\.]+");

    private static final String URL_SEPARATEUR = "/";

    static {
        final ArrayList<Entry<Integer, String>> reecritureRubrique = new ArrayList<>(4);
        reecritureRubrique.add(new ImmutablePair<>(0, "BO_USINESITE_UNIQUEMENT_NOM_PAGE"));
        reecritureRubrique.add(new ImmutablePair<>(1, "BO_USINESITE_NOM_PAGE_RUBRIQUE"));
        MODE_REECRITURE_RUBRIQUE = Collections.unmodifiableList(reecritureRubrique);
    }

    public static boolean isHttpHostNameParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpHostname() == null;
    }

    public static boolean isHttpPortParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpPort() == -1;
    }

    public static boolean isHttpsPortParDefaut(final InfosSite infosSite) {
        return infosSite.getHttpsPort() == -1;
    }

    public static boolean isModeReecritureValide(final int valeuModeReecriture) {
        return isCleContenuDansListeValeursAutorisee(valeuModeReecriture, InfosSiteHelper.MODE_REECRITURE_RUBRIQUE);
    }

    public static String getPathAbsoluFichiersTemplateDuSite(final InfosSite infosSite) {
        return getPathAbsoluFichiersTemplateDuSite(infosSite.getAlias());
    }

    public static String getPathAbsoluFichiersTemplateDuSite(final String codeSite) {
        return WebAppUtil.getAbsoluteFichiersSitesPath() + File.separator + codeSite + File.separator;
    }

    /**
     * Calcule le path du fichier sur le serveur concernant la propriété fourni en paramètre
     *
     * @param infosSite
     *            L'infossite sur lequel on souhaite chercher le fichier
     * @param nomPropertyTemplate
     *            le nom de la propriété contenant le fichier
     * @return Le path absolu du fichier à récupérer
     * @throws ErreurDonneeNonTrouve
     *             si la propriété n'est pas trouvé ou si elle ne peut pas être trouvé
     */
    public static String getPathAbsoluFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
        if (infosSite == null) {
            throw new ErreurDonneeNonTrouve("Impossible de récupérer la propriété : " + nomPropertyTemplate + " sur un InfosSite null");
        }
        final String codeSite = infosSite.getAlias();
        final String intituleFichier = infosSite.getProprieteComplementaireString(nomPropertyTemplate);
        if (StringUtils.isEmpty(intituleFichier)) {
            throw new ErreurDonneeNonTrouve("Intitulé de fichier introuvable pour la property de template : " + nomPropertyTemplate + ", du site : " + codeSite);
        }
        return WebAppUtil.getAbsoluteFichiersSitesPath() + File.separator + codeSite + File.separator + nomPropertyTemplate + File.separator + intituleFichier;
    }

    /**
     * Calcule l'url relative du fichier concernant la propriété fourni en paramètre
     *
     * @param infosSite
     *            L'infossite sur lequel on souhaite chercher le fichier
     * @param nomPropertyTemplate
     *            le nom de la propriété contenant le fichier
     * @return L'url relative du fichier à récupérer
     * @throws ErreurDonneeNonTrouve
     *             si la propriété n'est pas trouvé ou si elle ne peut pas être trouvé
     */
    public static String getURLRelativeFichierPropertyTemplate(final InfosSite infosSite, final String nomPropertyTemplate) throws ErreurDonneeNonTrouve {
        if (infosSite == null) {
            throw new ErreurDonneeNonTrouve("Impossible de récupérer la propriété : " + nomPropertyTemplate + " sur un InfosSite null");
        }
        final String codeSite = infosSite.getAlias();
        final String intituleFichier = infosSite.getProprieteComplementaireString(nomPropertyTemplate);
        if (StringUtils.isEmpty(intituleFichier)) {
            throw new ErreurDonneeNonTrouve("Intitulé de fichier introuvable pour la property de template : " + nomPropertyTemplate + ", du site : " + codeSite);
        }
        return WebAppUtil.getRelatifFichiersSitesPath() + codeSite + URL_SEPARATEUR + nomPropertyTemplate + URL_SEPARATEUR + intituleFichier;
    }

    /**
     * Vérifier que la valeur passée en paramétre est présente dans la {@link Collection} de {@link Pair} en tant que clé (élèment à gauche du Tuple)
     *
     * @param cle
     * @param listeValeurs
     * @return
     */
    private static boolean isCleContenuDansListeValeursAutorisee(final int cle, final Collection<Entry<Integer, String>> listeValeurs) {
        for (final Entry<Integer, String> valeurValide : listeValeurs) {
            if (valeurValide.getKey() == cle) {
                return Boolean.TRUE;
            }
        }
        return Boolean.FALSE;
    }

    public static void controlerCode(final String code) throws ErreurApplicative {
        final Matcher matcher = codeMetierPattern.matcher(code);
        if (!matcher.matches()) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("BO_USINESITE_ERREUR_CODE_INCORRECT"));
        }
    }

    /**
     * Test à l'aide d'une regex si un host est valide.
     *
     * @param host
     *            le host à tester
     * @return true si le host est valide
     */
    public static Boolean isHostValid(final String host) {
        if(StringUtils.isBlank(host)){
            return false;
        }

        String hostToTest = "http://" + host;
        try {
            URI uri = new URI(hostToTest);
            String uriHost = uri.getHost();
            return StringUtils.isNotEmpty(uriHost) && host.equals(uri.getHost());
        }catch (URISyntaxException e){
            LOGGER.debug("Host non valide",e);
        }
        return false;
    }

    /**
     * Retourne le port paramétré dans les propriétés du core sur la valeur «site.port.http».
     * @return par défaut retourne -1 (converti après en 80...) sinon la valeur de la property
     */
    public static int getHttpPort() {
        return PropertyHelper.getCorePropertyAsInt("site.port.http", -1);
    }

    /**
     * Retourne le port paramétré dans les propriétés du core sur la valeur «site.port.https».
     * @return par défaut retourne -1 (converti après en 443...) sinon la valeur de la property
     */
    public static int getHttpsPort() {
        return PropertyHelper.getCorePropertyAsInt("site.port.https", -1);
    }

}
