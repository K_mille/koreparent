package com.kosmos.usinesite.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.processus.SaisieInfosSite;
import com.kosmos.usinesite.template.bean.TemplateSite;
import com.kosmos.usinesite.template.property.bean.TemplateSiteProperty;
import com.kosmos.usinesite.template.property.extracteur.TemplateSitePropertyExtracteur;
import com.kosmos.usinesite.template.property.service.ServiceTemplateSiteProperty;
import com.kosmos.usinesite.template.property.service.ServiceTemplateSitePropertyFactory;
import com.kosmos.usinesite.template.property.validateur.TemplateSitePropertyValidateur;
import com.kportal.core.config.MessageHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.InfosRubriques;
import com.univ.objetspartages.services.ServiceRubrique;

public class SaisieInfosSiteInfoBeanHelper {

    private static final Logger LOG = LoggerFactory.getLogger(SaisieInfosSiteInfoBeanHelper.class);

    private static final String MESSAGE_ERREUR_CODE_OBLIGATOIRE = "BO_USINESITE_ERREUR_CODE_OBLIGATOIRE";

    private static final String MESSAGE_ERREUR_CODE_TROP_LONG = "BO_USINESITE_ERREUR_CODE_TROP_LONG";

    private static final String MESSAGE_ERREUR_INTITULE_OBLIGATOIRE = "BO_USINESITE_ERREUR_INTITULE_OBLIGATOIRE";

    private static final String MESSAGE_ERREUR_INTITULE_TROP_LONG = "BO_USINESITE_ERREUR_INTITULE_TROP_LONG";

    private static final String MESSAGE_ERREUR_RUBRIQUE_OBLIGATOIRE = "BO_USINESITE_ERREUR_RUBRIQUE_OBLIGATOIRE";

    private static final String MESSAGE_ERREUR_CODE_RUBRIQUE_INVALIDE = "BO_USINESITE_ERREUR_CODE_RUBRIQUE_INVALIDE";

    private static final String MESSAGE_ERREUR_HOSTNAME_HTTP_TROP_LONG = "BO_USINESITE_ERREUR_HOSTNAME_HTTP_TROP_LONG";

    private static final String MESSAGE_ERREUR_MODE_REECRITURE_INVALIDE = "BO_USINESITE_ERREUR_MODE_REECRITURE_INVALIDE";

    private static final String MESSAGE_ERREUR_NOM_DONNEE_NIVEAU_MIN = "BO_USINESITE_ERREUR_NOM_DONNEE_NIVEAU_MIN";

    private static final String MESSAGE_ERREUR_NOM_DONNEE_NIVEAU_MAX = "BO_USINESITE_ERREUR_NOM_DONNEE_NIVEAU_MAX";

    private static final String MESSAGE_ERREUR_INT_INFERIEUR_ZERO = "BO_USINESITE_ERREUR_INT_INFERIEUR_ZERO";

    private static final String MESSAGE_ERREUR_TEMPLATE = "BO_USINESITE_ERREUR_TEMPLATE";

    private static final String INFOBEAN_BOOLEAN_STRING_TRUE = "1";

    private static final String INFOBEAN_BOOLEAN_ON = "on";

    private static final String UNDERSCORE = "_";

    /**
     * Initialiser un {@link InfosSite} avec les données contenues dans le {@link InfoBean}. <br/>
     * Même si une exception est levée le {@link InfosSite} est initialisé avec les données contenues dans le {@link InfoBean}. Ces données peuvent donc ne pas être correctes.
     *
     * @param infoBean
     *            source de données
     * @param infosSite
     *            destination des données
     * @param template
     *            template à utiliser pour récupérer les données du template dans le {@link InfoBean}
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreursSaisieInfosSite} : erreur de saisie des données du {@link InfosSite}. Cette exception contient toutes les erreur rencontrée durant le
     *             peuplement du {@link InfosSite}</li>
     *             <li>Autre : erreur de récupération du {@link InfosRubriques}</li>
     *             </ul>
     */
    public static void peuplerInfosSiteDepuisInfoBean(final InfoBean infoBean, final InfosSiteImpl infosSite, final TemplateSite template) throws Exception {
        final List<String> fluxMessagesErreurs = new ArrayList<>();
        gestionDonneesSite(infosSite, infoBean, fluxMessagesErreurs);
        gestionDonneesTemplate(infosSite, template, infoBean, fluxMessagesErreurs);
        gestionDesErreurs(infosSite, template, fluxMessagesErreurs);
    }

    /**
     * Insérer toutes les données issue de l'infoBean dans el {@link InfosSiteImpl}.
     *
     * @param infosSite le site que l'on souhaite allimenter
     * @param infoBean les données du formulaire de saisie
     * @param fluxMessagesErreurs les potentiels messages d'erreur de traitement
     */
    @SuppressWarnings("deprecation")
    private static void gestionDonneesSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        setCodeInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setIntituleInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setRubriqueInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setHostnameHttpInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        infosSite.setSitePrincipal(getSitePrincipal(infoBean, infosSite));
        infosSite.setUrlAccueil(infoBean.getString(SaisieInfosSite.INFOBEAN_PAGE_ACCUEIL));
        infosSite.setSso(getBoolean(infoBean, SaisieInfosSite.INFOBEAN_SSO));
        infosSite.setActif(getBoolean(infoBean, SaisieInfosSite.INFOBEAN_ACTIF));
        infosSite.setSecure(getBoolean(infoBean, SaisieInfosSite.INFOBEAN_SECURE));
        infosSite.setHttpPort(InfosSiteHelper.getHttpPort());
        infosSite.setHttpsPort(InfosSiteHelper.getHttpsPort());
        setListeHostAliasInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setModeReecritureRubriqueInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setNiveauMaxReecritureRubriqueInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setNiveauMinReecritureRubriqueInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setRestrictionInfosSite(infosSite, infoBean, fluxMessagesErreurs);
        setNonIndexeInfosSite(infosSite, infoBean);
    }

    private static boolean getSitePrincipal(final InfoBean infoBean, final InfosSite site) {
        boolean sitePrincipal = site.isSitePrincipal();
        final String valeurSitePrincipal = StringUtils.defaultString(infoBean.getString(SaisieInfosSite.INFOBEAN_PRINCIPAL));
        if (StringUtils.isNotBlank(valeurSitePrincipal)) {
            sitePrincipal = StringUtils.equals(valeurSitePrincipal, INFOBEAN_BOOLEAN_STRING_TRUE);
        }
        return sitePrincipal;
    }

    public static void setRestrictionInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        if (getBoolean(infoBean, SaisieInfosSite.INFOBEAN_RESTREINT)) {
            infosSite.setRestriction(1);
        } else {
            infosSite.setRestriction(0);
        }
    }

    public static void setNonIndexeInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean) {
        if (getBoolean(infoBean, SaisieInfosSite.INFOBEAN_NON_INDEXE)) {
            infosSite.setNonIndexe(1);
        } else {
            infosSite.setNonIndexe(0);
        }
    }

    public static void setNiveauMinReecritureRubriqueInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        final int niveauMin = getIntSuperieurEgalZero(infoBean, SaisieInfosSite.INFOBEAN_NIVEAU_REECRITURE_MIN, InfosSite.NIVEAUMAXREECRITURERUBRIQUE_DEFAUT, MessageHelper.getCoreMessage(MESSAGE_ERREUR_NOM_DONNEE_NIVEAU_MIN), fluxMessagesErreurs);
        infosSite.setNiveauMinReecritureRubrique(niveauMin);
    }

    public static void setNiveauMaxReecritureRubriqueInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        final int niveauMax = getIntSuperieurEgalZero(infoBean, SaisieInfosSite.INFOBEAN_NIVEAU_REECRITURE_MAX, InfosSite.NIVEAUMAXREECRITURERUBRIQUE_DEFAUT, MessageHelper.getCoreMessage(MESSAGE_ERREUR_NOM_DONNEE_NIVEAU_MAX), fluxMessagesErreurs);
        infosSite.setNiveauMaxReecritureRubrique(niveauMax);
    }

    public static void setModeReecritureRubriqueInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        final String modeReecriture = infoBean.getString(SaisieInfosSite.INFOBEAN_MODE_REECRITURE);
        if (StringUtils.isEmpty(modeReecriture)) {
            infosSite.setModeReecritureRubrique(InfosSite.MODEREECRITURERUBRIQUE_DEFAUT);
        } else if (!StringUtils.isNumeric(modeReecriture)) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_MODE_REECRITURE_INVALIDE));
            infosSite.setModeReecritureRubrique(InfosSite.MODEREECRITURERUBRIQUE_DEFAUT);
        } else {
            final int valeuModeReecriture = Integer.parseInt(modeReecriture);
            if (!InfosSiteHelper.isModeReecritureValide(valeuModeReecriture)) {
                fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_MODE_REECRITURE_INVALIDE));
            }
            infosSite.setModeReecritureRubrique(valeuModeReecriture);
        }
    }

    public static void setListeHostAliasInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        final String[] alias = StringUtils.split(infoBean.getString(SaisieInfosSite.INFOBEAN_ALIAS), ",");
        infosSite.setListeHostAlias(new TreeSet<>(Arrays.asList(alias)));
    }

    public static void setHostnameHttpInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        String hostNameHttp = infoBean.getString(SaisieInfosSite.INFOBEAN_HTTP_HOSTNAME);
        hostNameHttp = StringUtils.trimToEmpty(hostNameHttp);
        if (hostNameHttp.length() > 255) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_HOSTNAME_HTTP_TROP_LONG));
        }
        infosSite.setHttpHostname(hostNameHttp);
    }

    public static void setRubriqueInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        String codeRubrique = infoBean.getString(SaisieInfosSite.INFOBEAN_CODE_RUBRIQUE);
        codeRubrique = StringUtils.trimToEmpty(codeRubrique);
        if (StringUtils.isEmpty(StringUtils.trimToEmpty(codeRubrique))) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_RUBRIQUE_OBLIGATOIRE));
        } else {
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            final RubriqueBean rubrique = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (rubrique == null) {
                fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_CODE_RUBRIQUE_INVALIDE));
            }
        }
        infosSite.setCodeRubrique(codeRubrique);
    }

    public static void setIntituleInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        String intitule = infoBean.getString(SaisieInfosSite.INFOBEAN_INTITULE);
        intitule = StringUtils.trimToEmpty(intitule);
        if (StringUtils.isEmpty(intitule)) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_INTITULE_OBLIGATOIRE));
        } else if (intitule.length() > 255) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_INTITULE_TROP_LONG));
        }
        infosSite.setIntitule(intitule);
    }

    public static void setCodeInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final List<String> fluxMessagesErreurs) {
        String code = infoBean.getString(SaisieInfosSite.INFOBEAN_CODE);
        code = StringUtils.trimToEmpty(code);
        if (StringUtils.isEmpty(code)) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_CODE_OBLIGATOIRE));
        } else if (code.length() > 64) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_CODE_TROP_LONG));
        } else {
            try {
                InfosSiteHelper.controlerCode(code);
            } catch (final ErreurApplicative e) {
                LOG.debug("site code is not valid", e);
                fluxMessagesErreurs.add(e.getMessage());
            }
        }
        infosSite.setAlias(code);
    }

    public static void gestionDonneesTemplate(final InfosSiteImpl infosSite, final TemplateSite template, final InfoBean infoBean, final List<String> fluxMessagesErreurs) throws Exception {
        if (template == null) {
            fluxMessagesErreurs.add(MessageHelper.getCoreMessage(MESSAGE_ERREUR_TEMPLATE));
            return;
        }
        infosSite.setCodeTemplate(template.getCode());
        infosSite.setJspFo(template.getDossierJSP());
        final ServiceTemplateSiteProperty service = ServiceTemplateSitePropertyFactory.getServiceTemplateSite();
        for (final TemplateSiteProperty property : template.getListeProprietesComplementaires()) {
            validerDonneeTemplateDansInfosSite(infosSite, infoBean, template, property, service, fluxMessagesErreurs);
            injecterDonneeTemplateDansInfosSite(infosSite, infoBean, template, property, service);
        }
    }

    private static void validerDonneeTemplateDansInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final TemplateSite template, final TemplateSiteProperty property, final ServiceTemplateSiteProperty service, final List<String> fluxMessagesErreurs) throws Exception {
        try {
            final TemplateSitePropertyValidateur<TemplateSiteProperty> validateur = service.getTemplateSitePropertyValidateur(property);
            validateur.valider(infosSite, template, property, infoBean);
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("pas de validteur trouvé ", e);
        } catch (final ErreursSaisieInfosSite e) {
            LOG.debug("site data are not valid", e);
            fluxMessagesErreurs.addAll(e.getListeMessagesErreur());
        }
    }

    private static void injecterDonneeTemplateDansInfosSite(final InfosSiteImpl infosSite, final InfoBean infoBean, final TemplateSite template, final TemplateSiteProperty property, final ServiceTemplateSiteProperty service) throws Exception {
        try {
            final TemplateSitePropertyExtracteur<TemplateSiteProperty, Object> extracteur = service.getTemplateSitePropertyExtracteur(property);
            final Object valeur = extracteur.extraire(infosSite, template, property, infoBean);
            if (valeur != null) {
                infosSite.putProperty(property.getCode(), valeur);
            }
        } catch (final ErreurDonneeNonTrouve e) {
            LOG.debug("pas d'extracteur trouvé", e);
        }
    }

    private static void gestionDesErreurs(final InfosSiteImpl infosSite, final TemplateSite template, final List<String> fluxMessagesErreurs) throws ErreursSaisieInfosSite {
        if (!fluxMessagesErreurs.isEmpty()) {
            throw new ErreursSaisieInfosSite(fluxMessagesErreurs, infosSite, template);
        }
    }

    /**
     * Récupérer la donnée concernant l'activation ou non du site. Récupération de la donnée {@link SaisieInfosSite#INFOBEAN_ACTIF} du {@link InfoBean}
     *
     * @param infoBean les valeurs du formulaire de
     * @return vrai si les données saisies indiquent que le site est actif
     */
    public static boolean getInfosSiteActif(final InfoBean infoBean) {
        return getBoolean(infoBean, SaisieInfosSite.INFOBEAN_ACTIF);
    }

    /**
     * Fonction qui permet de récupérer depuis un {@link InfoBean} un entier. Dans le cas où il ne s'agit pas d'un entier ou que l'entier n'est pas supèrieur ou égal à 0 une erreur
     * est écrite dans le flux d'erreur. De plus, si la clé infoBean ne retourne aucune valeur c'est la valeur par défaut (voir paramètre) qui est retournée.
     *
     * @param infoBean
     *            Conteneur des données.
     * @param cle
     *            La clé permettant de récupérer la valeur.
     * @param valeurDefaut
     *            La valeur par défaut utilisée dans le cas où la clé ne retourne aucune valeur.
     * @param intituleDonnee
     *            Intitulé de la donnée. Cette valeur est utilisée pour construire les messages d'erreur.
     * @param fluxMessagesErreurs les potentiels messages d'erreur de traitement
     * @return la valeur entière saisie dans l'infobean ou la valeur par défaut fourni en paramètre en cas d'erreur
     */
    private static int getIntSuperieurEgalZero(final InfoBean infoBean, final String cle, final int valeurDefaut, final String intituleDonnee, final List<String> fluxMessagesErreurs) {
        final String niveauMin = infoBean.getString(cle);
        if (StringUtils.isEmpty(niveauMin)) {
            return valeurDefaut;
        }
        if (!StringUtils.isNumeric(niveauMin)) {
            fluxMessagesErreurs.add(intituleDonnee + MessageHelper.getCoreMessage(MESSAGE_ERREUR_INT_INFERIEUR_ZERO));
            return valeurDefaut;
        }
        final int valeurNiveauMin = Integer.parseInt(niveauMin);
        if (valeurNiveauMin < 0) {
            fluxMessagesErreurs.add(intituleDonnee + MessageHelper.getCoreMessage(MESSAGE_ERREUR_INT_INFERIEUR_ZERO));
        }
        return valeurNiveauMin;
    }

    /**
     * Extraire du {@link InfoBean} la liste des valeurs saisies sans le composant de liste de textes.
     *
     * @param data
     *            la source de données
     * @param nomComposant
     *            le nom du composant
     * @return La liste de valeurs. Cette liste est vide si le composant n'est pas référencé dans le {@link InfoBean}.
     */
    public static List<String> getListeText(final InfoBean data, final String nomComposant) {
        final String valeurs = data.getString(nomComposant);
        if (StringUtils.isEmpty(valeurs)) {
            return Collections.emptyList();
        }
        return Arrays.asList(StringUtils.split(valeurs, ","));
    }

    /**
     * Récupérer l'ensemble des valeurs contenues dans le {@link InfoBean} ayant pour clé le prefixeCleInfoBean (suivi de '_'). Seules les valeur non vide sont retournées.
     *
     * @param infoBean
     *            Conteneur des données.
     * @param prefixeCleInfoBean
     *            Préfixe des clés de l'infoBean à récupérer. Toutes les clés commençant par ce préfixe et suivi de {@link #UNDERSCORE} seront récupérées.
     * @return La {@link Map} clé-valeur contenant toutes les clés infoBean liées à leur valeur.
     */
    public static Map<String, String> getListeValeurs(final InfoBean infoBean, String prefixeCleInfoBean) {
        prefixeCleInfoBean += UNDERSCORE;
        final SortedMap<String, String> mapListeValeurs = new TreeMap<>();
        for (final String cleInfoBean : infoBean.getDataKeys()) {
            if (!StringUtils.startsWith(cleInfoBean, prefixeCleInfoBean)) {
                continue;
            }
            final Object valeur = infoBean.get(cleInfoBean);
            if (valeur != null && valeur instanceof String && StringUtils.isNotEmpty((String) valeur)) {
                mapListeValeurs.put(cleInfoBean, (String) valeur);
            }
        }
        return mapListeValeurs;
    }

    /**
     * Récupération/transformation d'une valeur boolean depuis l'infoBean.
     *
     * @param cleInfoBean
     *            Conteneur des données.
     * @return <code>false</code> si la propriété n'existe pas ou si sa valeur n'est pas égal à "1". Dans le cas contraire c'est <code>true</code>
     */
    public static boolean getBoolean(final InfoBean infoBean, final String cleInfoBean) {
        final String actif = infoBean.getString(cleInfoBean);
        return StringUtils.equals(actif, INFOBEAN_BOOLEAN_STRING_TRUE);
    }

    public static boolean isChecked(final InfoBean infoBean, final String cleInfoBean) {
        final String actif = infoBean.getString(cleInfoBean);
        return StringUtils.equals(actif, INFOBEAN_BOOLEAN_ON);
    }
}
