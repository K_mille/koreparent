package com.kosmos.usinesite.utils;

import java.util.Arrays;
import java.util.Collection;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.om.PermissionBean;

public final class UsineSiteComposantUtil {

    public static final String ID_BEAN = "composantUsineASite";

    /**
     * La permission permettant de modifier des sites
     */
    public static final String CODE_ACTION_MODIFICATION = "M";

    public static final String CODE_PERMISSION = "site";

    /**
     * La permission permettant de gérer les sites (Create Update Delete)
     */
    public static final String CODE_ACTION_GESTION = "G";

    /**
     * Les actions possible avec les droits de modification (lister & modifier)
     */
    public static final Collection<String> ACTIONS_MODIFICATION = Arrays.asList("MODIFIER", "ACCUEIL", "LISTE", "VALIDER_MODIFICATION");

    public static IModule getModule() {
        return ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    /**
     * Retourne la permission de gestion des sites.
     */
    public static PermissionBean getPermissionGestion() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_GESTION);
    }

    /**
     * Retourne la permission de modification des sites.
     */
    public static PermissionBean getPermissionModification() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_MODIFICATION);
    }
}
