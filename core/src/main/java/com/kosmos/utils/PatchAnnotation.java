package com.kosmos.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

import org.apache.commons.lang3.StringUtils;

@Target({ElementType.METHOD, ElementType.TYPE})
public @interface PatchAnnotation {

    String commentaire();

    String ticket() default StringUtils.EMPTY;

    String date();
}