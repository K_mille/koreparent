package com.kosmos.utils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.TYPE})
public @interface PatchList {

    PatchAnnotation[] value();
}
