package com.kosmos.validation.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.kosmos.validation.validator.PhoneNumberValidator;

/**
 * Interface permettant d'être sur qu'un champ ne soit ni vide ni avec la valeur par défaut de kportal (aka le magique
 * "0000"
 */
@Constraint(validatedBy = {PhoneNumberValidator.class})
@Target(value = ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PhoneNumber {

    String message() default "{com.kosmos.validation.constraint.PhoneNumber.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
