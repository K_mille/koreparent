package com.kosmos.validation.exception;

import java.util.HashSet;
import java.util.Set;

import javax.validation.ConstraintViolation;

import com.jsbsoft.jtf.exception.ErreurApplicative;

/**
 * Exception pouvant être lever lors de la validation d'un bean.
 */
public class ConstraintValidationException extends ErreurApplicative {

    private static final long serialVersionUID = -1519829638387929571L;

    private Set<ConstraintViolation<?>> constraintViolations = new HashSet<>();

    public ConstraintValidationException(String mes, Set<? extends ConstraintViolation<?>> constraintViolations) {
        super(mes);
        this.constraintViolations = new HashSet<>(constraintViolations);
    }

    public ConstraintValidationException(int num, String mes) {
        super(num, mes);
    }

    public ConstraintValidationException(String mes, Throwable t) {
        super(mes, t);
    }

    public ConstraintValidationException(String mes) {
        super(mes);
    }

    public Set<ConstraintViolation<?>> getConstraintViolations() {
        return constraintViolations;
    }
}
