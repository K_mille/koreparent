package com.kosmos.validation.message;

import java.util.Locale;

import org.hibernate.validator.messageinterpolation.ResourceBundleMessageInterpolator;
import org.hibernate.validator.spi.resourceloading.ResourceBundleLocator;

/**
 * Created by olivier.camon on 22/12/14.
 */
public class LocaleMessageInterpolator extends ResourceBundleMessageInterpolator {

    private final Locale defaultLocale;

    public LocaleMessageInterpolator(Locale locale, ResourceBundleLocator userResourceBundleLocator) {
        super(userResourceBundleLocator);
        this.defaultLocale = locale;
    }

    public LocaleMessageInterpolator(Locale locale) {
        this.defaultLocale = locale;
    }

    @Override
    public String interpolate(String messageTemplate, Context context) {
        return super.interpolate(messageTemplate, context, this.defaultLocale);
    }

    @Override
    public String interpolate(String messageTemplate, Context context, Locale locale) {
        return super.interpolate(messageTemplate, context, locale);
    }
}
