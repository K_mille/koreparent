/**
 * Package contenant les classes permettant de valider les beans via la JSR 303
 */
package com.kosmos.validation;