package com.kosmos.validation.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.validation.constraint.PhoneNumber;

/**
 * Validateur servant à vérifier qu'un champ ne correspond pas à la valeur par défaut de kportal/ksup. À savoir non vide
 * et != 0000
 */
public class PhoneNumberValidator implements ConstraintValidator<PhoneNumber, String> {

    final Pattern phonePattern = Pattern.compile("^((\\+)*[0-9]{1,3}|0)[0-9]{9}$");

    @Override
    public void initialize(PhoneNumber constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isBlank(value)) {
            return true;
        }
        final Matcher matcher = phonePattern.matcher(StringUtils.deleteWhitespace(value));
        return matcher.matches();
    }
}
