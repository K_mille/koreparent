package com.kosmos.validation.validator;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.validation.constraint.PostalCode;

/**
 * Validateur servant à vérifier qu'un champ ne correspond pas à la valeur par défaut de kportal/ksup. À savoir non vide
 * et != 0000
 */
public class PostalCodeValidator implements ConstraintValidator<PostalCode, String> {

    final Pattern postalCodePattern = Pattern.compile("^((0[1-9])|([1-8][0-9])|(9[0-8])|(2A)|(2B))[0-9]{3}$");

    @Override
    public void initialize(PostalCode constraintAnnotation) {
    }

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (StringUtils.isBlank(value)) {
            return true;
        }
        final Matcher matcher = postalCodePattern.matcher(value);
        return matcher.matches();
    }
}
