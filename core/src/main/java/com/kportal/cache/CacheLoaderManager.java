package com.kportal.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.objetspartages.cache.CacheRoleManager;
import com.univ.objetspartages.cache.CacheServiceManager;

/*
 * Cette classe permet de charger les caches applicatifs au démarrage de tomcat une fois l'application initialisée
 * Tous les caches ne sont pas présents ici car d'autre Bean Manager charge également les caches. cf PageAccueilRubriqueManager qui gere les urls de rubrique
 */
public class CacheLoaderManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheLoaderManager.class);

    private CacheRoleManager cacheRoleManager;

    private CacheServiceManager cacheServiceManager;

    public void refresh() {
        LOGGER.debug("rechargement des services");
        cacheServiceManager.refresh();
        LOGGER.debug("rechargement des rôles");
        cacheRoleManager.flush();
        cacheRoleManager.getListeRoles();
    }

    public void setCacheRoleManager(final CacheRoleManager cacheRoleManager) {
        this.cacheRoleManager = cacheRoleManager;
    }

    public void setCacheServiceManager(CacheServiceManager cacheServiceManager) {
        this.cacheServiceManager = cacheServiceManager;
    }

}
