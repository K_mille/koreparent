package com.kportal.cache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.CacheManager;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class CacheUtil {

    private static final Logger LOG = LoggerFactory.getLogger(CacheUtil.class);

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "cacheManager";

    public static CacheManager getCacheManager() {
        return (CacheManager) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    public static void flushCache(final String cacheName) {
        final CacheManager cacheMgr = getCacheManager();
        if (cacheMgr != null && cacheMgr.getCache(cacheName) != null) {
            cacheMgr.getCache(cacheName).clear();
        }
    }

    public static Object getObjectValue(final String cacheName, final Object keyValue) {
        final CacheManager cacheMgr = getCacheManager();
        if (cacheMgr != null && cacheMgr.getCache(cacheName) != null && cacheMgr.getCache(cacheName).get(keyValue) != null) {
            return cacheMgr.getCache(cacheName).get(keyValue).get();
        }
        return null;
    }

    public static boolean updateObjectValue(final String cacheName, final Object keyValue, final Object value) {
        final CacheManager cacheMgr = getCacheManager();
        if (cacheMgr != null) {
            cacheMgr.getCache(cacheName).put(keyValue, value);
            return true;
        }
        return false;
    }

    public static boolean flush(final String cacheName, final Object objectKey) {
        final CacheManager cacheMgr = getCacheManager();
        if (cacheMgr != null && cacheMgr.getCache(cacheName) != null) {
            cacheMgr.getCache(cacheName).evict(objectKey);
            LOG.debug(cacheName + " flushed for value : " + objectKey);
            return true;
        }
        LOG.error("no cache manager found on this application");
        return false;
    }
}
