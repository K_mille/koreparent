package com.kportal.cms.mail;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.FicheObjet;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivMgr;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

public class DefaultMailNotificationFormater implements MailNotificationFormater {

    public List<String> classes;

    private EspaceCollaboratifDAO espaceCollaboratifDAO;

    public void setEspaceCollaboratifDAO(final EspaceCollaboratifDAO espaceCollaboratifDAO) {
        this.espaceCollaboratifDAO = espaceCollaboratifDAO;
    }

    @Override
    public String getMessageRetourMiseEnLigne(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean validateur) throws Exception {
        FicheObjet ficheNonLisible = null;
        if (ficheUniv instanceof FicheObjet && !FicheAnnotationHelper.isFicheFrontOffice(ficheUniv)) {
            ficheNonLisible = (FicheObjet) ficheUniv;
            ficheUniv = ficheNonLisible.renvoyerFicheParente();
        }
        String nomObjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv));
        String url_fiche = UnivWebFmt.determinerUrlFiche(ContexteUtil.getContexteUniv(), nomObjet.toLowerCase(), ficheUniv.getCode(), ficheUniv.getLangue(), false);
        // on ajoute l'espace dans l'url pour récupérer le contexte graphique
        if (infoBean.get("ESPACE") != null && ((String) infoBean.get("ESPACE")).length() > 0) {
            url_fiche += (url_fiche.contains("?") ? "&" : "?") + "ESPACE=" + infoBean.get("ESPACE");
        }
        // l'ancre doit être placée après le paramètre de l'espace
        if (ficheNonLisible != null) {
            url_fiche += "#forum" + ficheNonLisible.getCode();
        }
        return String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.VALIDATED"), validateur.getPrenom(), validateur.getNom(), ficheUniv.getLibelleAffichable(), URLResolver.getAbsoluteUrl(url_fiche, ContexteUtil.getContexteUniv()));
    }

    @Override
    public String getMessageRetourNonValide(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean validateur) {
        String nomObjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv));
        String message = String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.REFUSED_NO_REASON"), validateur.getPrenom(), validateur.getNom(), ficheUniv.getLibelleAffichable());
        if (infoBean.get("MOTIF_RETOUR") != null && ((String) infoBean.get("MOTIF_RETOUR")).length() > 0) {
            message = String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.REFUSED"), validateur.getPrenom(), validateur.getNom(), ficheUniv.getLibelleAffichable(), infoBean.get("MOTIF_RETOUR"));
        }
        if ("1".equals(meta.getMetaSaisieFront()) && FicheAnnotationHelper.isSaisieFrontOffice(ficheUniv)) {
            String url_fiche = WebAppUtil.SG_PATH + "?EXT=" + ReferentielObjets.getExtension(ficheUniv) + "+&ACTION=MODIFIER&ID_FICHE=";
            url_fiche += ficheUniv.getIdFiche() + "&PROC=SAISIE_" + nomObjet.toUpperCase();
            if ("1".equals(meta.getMetaSaisieFront())) {
                url_fiche += "_FRONT&SAISIE_FRONT=TRUE";
            }
            //on ajoute l'espace dans l'url pour récupérer le contexte graphique
            if (infoBean.get("ESPACE") != null && ((String) infoBean.get("ESPACE")).length() > 0) {
                url_fiche += "&ESPACE=" + infoBean.get("ESPACE");
            }
            if (!ServiceUser.UTILISATEUR_ANONYME.equals(ficheUniv.getCodeRedacteur())) {
                message += String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.REFUSED_MODIF_FRONT"), URLResolver.getAbsoluteUrl(url_fiche, ContexteUtil.getContexteUniv()));
            }
        } else {
            message += String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.REFUSED_MODIF_BACK"), URLResolver.getAbsoluteBoUrl("/adminsite/", ContexteUtil.getContexteUniv().getInfosSite()));
        }
        return message;
    }

    @Override
    public String getMessageDemandeValidation(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean utilisateur_demandeur) {
        String nomprenom = "";
        /* la demande de validation peut etre faite par un anonyme (forum ou saisie front) */
        if (utilisateur_demandeur == null || Long.valueOf(0L).equals(utilisateur_demandeur.getId())) {
            nomprenom = (String) infoBean.get("AUTEUR");
            if (StringUtils.isEmpty(nomprenom)) {
                nomprenom = MessageHelper.getCoreMessage("MAIL.NOTIFICATION.AN_ANON");
            }
            String emailano = (String) infoBean.get("EMAIL_AUTEUR");
            if (StringUtils.isNotEmpty(emailano)) {
                nomprenom += " ( " + emailano + " )";
            } else {
                emailano = meta.getMetaMailAnonyme();
                if (StringUtils.isNotEmpty(emailano)) {
                    nomprenom += " ( " + emailano + " )";
                }
            }
        } else {
            nomprenom = String.format("%s %s", utilisateur_demandeur.getPrenom(), utilisateur_demandeur.getNom());
        }
        String codeObjet = (String) infoBean.get("CODE_OBJET");
        final String codeEspace;
        Object codeEspaceInfoBean =  infoBean.get("ESPACE");
        if (null != codeEspaceInfoBean) {
            codeEspace = String.valueOf(codeEspaceInfoBean);
        }
        else {
            codeEspace = StringUtils.EMPTY;
        }
        String message = String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.VALIDATE"), nomprenom, ficheUniv.getLibelleAffichable(), URLResolver.getAbsoluteBoUrl("/adminsite/", ContexteUtil.getContexteUniv().getInfosSite()));
        if (FicheUnivMgr.isFicheCollaborative(ficheUniv) && StringUtils.isNotEmpty(codeEspace)) {
            String urlfiche = WebAppUtil.SG_PATH + "?ACTION=MODIFIER" + "&ID_FICHE=" + ficheUniv.getIdFiche() + "&PROC=SAISIE_" + ReferentielObjets.getNomObjet(codeObjet).toUpperCase() + "_FRONT" + "&SAISIE_FRONT=TRUE" + "&ESPACE=" + codeEspace + "&LIEN_DIRECT=TRUE";
            urlfiche = URLResolver.getAbsoluteUrl(urlfiche, ContexteUtil.getContexteUniv());
            message = String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.VALIDATE"), nomprenom, ficheUniv.getLibelleAffichable(), espaceCollaboratifDAO.getByCode(codeEspace).getIntitule(), urlfiche);
        }
        return message;
    }

    @Override
    public String getMessageNotificationCollaboratif(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean utilisateur) throws Exception {
        String codeEspace = (String) infoBean.get("ESPACE");
        FicheObjet ficheNonLisible = null;
        if (ficheUniv instanceof FicheObjet && !FicheAnnotationHelper.isFicheFrontOffice(ficheUniv)) {
            ficheNonLisible = (FicheObjet) ficheUniv;
            ficheUniv = ficheNonLisible.renvoyerFicheParente();
        }
        String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
        String nomObjet = ReferentielObjets.getNomObjet(codeObjet);
        String url_fiche = UnivWebFmt.determinerUrlFiche(ContexteUtil.getContexteUniv(), nomObjet.toLowerCase(), ficheUniv.getCode(), ficheUniv.getLangue(), false);
        // on ajoute l'espace dans l'url pour récupérer le contexte graphique
        url_fiche += (url_fiche.contains("?") ? "&" : "?") + "ESPACE=" + codeEspace;
        // l'ancre doit être placée après le paramètre de l'espace
        if (ficheNonLisible != null) {
            url_fiche += "#forum" + ficheNonLisible.getCode();
        }
        final String nomEspace = espaceCollaboratifDAO.getByCode(codeEspace).getIntitule();
        return String.format(MessageHelper.getCoreMessage(Espacecollaboratif.MAIL_MESSAGE_NOTIFICATION), String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom()), ficheUniv.getLibelleAffichable(), nomEspace, URLResolver.getAbsoluteUrl(url_fiche, ContexteUtil.getContexteUniv()));
    }

    @Override
    public String getMessageNotification(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta, UtilisateurBean utilisateur_demandeur) throws Exception {
        String codeObjet = ReferentielObjets.getCodeObjet(ficheUniv);
        String nomObjet = ReferentielObjets.getNomObjet(codeObjet);
        FicheObjet ficheNonLisible = null;
        if (ficheUniv instanceof FicheObjet && !FicheAnnotationHelper.isFicheFrontOffice(ficheUniv)) {
            ficheNonLisible = (FicheObjet) ficheUniv;
            ficheUniv = ficheNonLisible.renvoyerFicheParente();
            nomObjet = ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv));
        }
        String url_fiche = UnivWebFmt.determinerUrlFiche(ContexteUtil.getContexteUniv(), nomObjet.toLowerCase(), ficheUniv.getCode(), ficheUniv.getLangue(), false);
        if (ficheNonLisible != null) {
            url_fiche += "#forum" + ficheNonLisible.getCode();
        }
        return String.format(MessageHelper.getCoreMessage("MAIL.NOTIFICATION.ONLINE"), utilisateur_demandeur.getPrenom(), utilisateur_demandeur.getNom(), ficheUniv.getLibelleAffichable(), URLResolver.getAbsoluteUrl(url_fiche, ContexteUtil.getContexteUniv()));
    }

    @Override
    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }
}
