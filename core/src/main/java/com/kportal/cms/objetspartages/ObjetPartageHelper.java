package com.kportal.cms.objetspartages;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.jsbsoft.jtf.core.CodeLibelle;
import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.kportal.extension.module.ModuleHelper;
import com.kportal.extension.module.plugin.objetspartages.PluginRechercheHelper;
import com.univ.objetspartages.om.RechercheExterne;
import com.univ.objetspartages.om.ReferentielObjets;

public class ObjetPartageHelper {

    public static final String TEMPLATE_ENTETE_FICHE = "entete_fiche";

    public static final String TEMPLATE_SAISIE = "saisie";

    public static final String TEMPLATE_ENCADRE_RECHERCHE = "encadre_recherche";

    public static final String TEMPLATE_ENCADRE_AUTOFICHE = "encadre_fiche";

    public static final String TEMPLATE_REQUETE_RESULTAT = "requete_resultat";

    public static final String TEMPLATE_RECHERCHE_CALCUL = "recherche_calcul";

    public static final String TEMPLATE_RECHERCHE_RESULTAT = "recherche_resultat";

    public static final String TEMPLATE_RECHERCHE = "recherche";

    public static final String PATH_TEMPLATE_REQUETE_RESULTAT_DEFAULT = "default";

    private static final String FICHE = "fiche";

    public static final String TEMPLATE_FICHE = FICHE;

    private static final String STYLE = "STYLE";

    private static final String STYLE_AFFICHAGE = "style_affichage";

    public static String getTemplateObjet(final String templateJsp, final String objet) {
        String path = "";
        final Objetpartage op = ReferentielObjets.getObjetByNom(objet);
        IExtension extension = null;
        if (op != null) {
            extension = ModuleHelper.getExtensionModule(op);
        } else if (objet.equals(PATH_TEMPLATE_REQUETE_RESULTAT_DEFAULT)) {
            extension = ExtensionHelper.getCoreExtension();
        }
        if (extension != null) {
            path = ExtensionHelper.PATH_WEB_INF_JSP + "/" + objet + ExtensionHelper.PATH_FO + "/" + templateJsp + ExtensionHelper.EXTENSION_TEMPLATE;
            return ExtensionHelper.getTemplateExtension(extension.getId(), path, Boolean.TRUE);
        }
        return path;
    }

    // Renvoie la liste des styles d'affichage pour l'extension de l'objet en paramètre
    public static Map<String, String> getStylesAffichage(final String objet) {
        final Objetpartage op = ReferentielObjets.getObjetByNom(objet);
        return CodeLibelle.lireTable(op.getIdExtension(), objet.toLowerCase() + "_" + STYLE_AFFICHAGE, null);
    }

    public static List<String> getCriteresRequete(final String objet, final boolean listeIncluse) {
        final Objetpartage op = ReferentielObjets.getObjetByNom(objet);
        List<String> criteres = new ArrayList<>();
        for (final RechercheExterne bean : ClassBeanManager.getInstance().getBeanOfType(RechercheExterne.class)) {
            // critères métiers propres à chaque objet
            criteres = bean.getCriteresRequete(objet.toUpperCase(), listeIncluse);
            if (criteres.size() > 0) {
                break;
            }
        }
        if (listeIncluse) {
            // critères de style sur l'objet
            if ("1".equals(PropertyHelper.getProperty(op.getIdExtension(), FICHE + "." + objet.toUpperCase() + "." + STYLE_AFFICHAGE))) {
                criteres.add(STYLE);
            }
            // critères des plugins de recherche activés sur l'objet
            criteres.addAll(PluginRechercheHelper.getCriteresRequete(objet));
        }
        // Ajout des critères communs
        criteres.add("CODE_RUBRIQUE");
        criteres.add("CODE_RATTACHEMENT");
        // NB : certains critères sont automatiquement parsés dans le fichier js : recherche.js
        // GROUPE_DSI, LANGUE, NOMBRE
        return criteres;
    }
}
