package com.kportal.cms.objetspartages.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//Annotation accessible à l'execution
@Retention(RetentionPolicy.RUNTIME)
// Annotation associé à un type (Classe, interface)
@Target(ElementType.TYPE)
public @interface FicheAnnotation {

    boolean isFicheFrontOffice() default true;

    boolean isSaisieFrontOffice() default true;

    boolean isLienInterne() default true;

    boolean isLienRequete() default true;

    boolean isIndexable() default true;

    boolean isEncadreRecherche() default false;

    boolean isAccessibleBo() default true;

    boolean isEncadreRechercheEmbarquable() default true;

    boolean isContenuDuplicable() default true;
}
