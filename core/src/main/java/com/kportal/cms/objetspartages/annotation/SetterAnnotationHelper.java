package com.kportal.cms.objetspartages.annotation;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import com.univ.objetspartages.om.FicheUniv;

/**
 * The Class GetterAnnotationHelper.
 */
public class SetterAnnotationHelper {

    /**
     * teste la présense de l'annotation SetterAnnotation et si la méthode set un contenu de toolbox.
     *
     * @param setterMethod
     *            the setter method
     * @return true, if is toolbox
     */
    public static boolean isToolbox(Method setterMethod) {
        SetterAnnotation setterAnnotation = setterMethod.getAnnotation(SetterAnnotation.class);
        return setterAnnotation != null && setterAnnotation.isToolbox();
    }

    /**
     * Renvoit la liste des méthodes d'une fiche correspondant à des setter de contenu toolbox.
     *
     * @param ficheUniv
     *            the fiche univ
     * @return the liste toolbox
     */
    public static List<Method> getMethodToolbox(FicheUniv ficheUniv) {
        ArrayList<Method> liste = new ArrayList<>();
        for (Method method : ficheUniv.getClass().getMethods()) {
            if (isToolbox(method)) {
                liste.add(method);
            }
        }
        return liste;
    }
}
