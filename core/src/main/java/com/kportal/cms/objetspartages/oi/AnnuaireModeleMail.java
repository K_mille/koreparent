package com.kportal.cms.objetspartages.oi;

import com.univ.objetspartages.om.AnnuaireModele;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Classe renvoyant le libelle mailto et l'adresse mail d'une fiche annuaire
 *
 * @author yacouba.kone
 *
 */
public class AnnuaireModeleMail implements ObjetMail {

    /**
     * Renvoie le nom de la personne à qui on envoie l'email.
     *
     * @return the libelle mailto
     */
    @Override
    public String getLibelle(final FicheUniv ficheUniv) {
        return ficheUniv.getLibelleAffichable();
    }

    /**
     * Renvoie l'email de la personne à qui on écrit.
     *
     * @return the mailto
     */
    @Override
    public String getAdresse(final FicheUniv ficheUniv) {
        return ((AnnuaireModele) ficheUniv).getAdresseMail();
    }
}
