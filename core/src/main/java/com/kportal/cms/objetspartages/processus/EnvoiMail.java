package com.kportal.cms.objetspartages.processus;

import javax.mail.MessagingException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.mail.EmailException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.email.JSBMailbox;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.oi.ObjetMail;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceUser;

/**
 * Processus abstrait d'envoi de mail.
 */
public class EnvoiMail extends ProcessusBean {

    /** The Constant ECRAN_PRINCIPAL. */
    private static final String ECRAN_PRINCIPAL = "PRINCIPAL";

    private static final String ECRAN_CONFIRMATION = "CONFIRMATION";

    private static final Logger LOG = LoggerFactory.getLogger(EnvoiMail.class);

    private final ServiceUser serviceUser;

    private ObjetMail objetMail;

    /**
     * Constructeur.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public EnvoiMail(final InfoBean ciu) {
        super(ciu);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Affichage de l'écran de saisie d'un mail.
     *
     * @throws Exception
     *             the exception
     */
    protected void preparerPRINCIPAL() throws Exception {
        final String userCode = (String) getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.CODE);
        final UtilisateurBean user = serviceUser.getByCode(userCode);
        if (user != null) {
            infoBean.set("FROM", user.getAdresseMail());
        }
        final String libelleMailto = objetMail.getLibelle(getFicheUniv(this, infoBean.getString("MAILTO"), infoBean.getString("TYPE"), getLangue()));
        if (libelleMailto.length() == 0) {
            throw new ErreurApplicative("Utilisateur inexistant");
        }
        infoBean.set("LIBELLE_MAILTO", libelleMailto);
        ecranLogique = ECRAN_PRINCIPAL;
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        ecranLogique = infoBean.getEcranLogique();
        action = infoBean.getString("ACTION");
        // Instanciation de l'objet mail
        objetMail = (ObjetMail) ApplicationContextManager.getEveryContextBean(infoBean.getString("ID_BEAN"));
        // Entrée par lien hyper-texte
        if (StringUtils.isBlank(ecranLogique) && "CREER_MAIL".equals(action)) {
            preparerPRINCIPAL();
        } else if (ECRAN_PRINCIPAL.equals(ecranLogique)) {
            traiterPRINCIPAL();
        }
        infoBean.setEcranLogique(ecranLogique);
        return etat == FIN;
    }

    /**
     * Gestion de l'envoi du mail.
     *
     * @throws Exception
     *             the exception
     */
    protected void traiterPRINCIPAL() throws Exception {
        if (action.equals(InfoBean.ACTION_ENREGISTRER)) {
            final String from = (String) infoBean.get("FROM");
            final String mailto = objetMail.getAdresse(getFicheUniv(this, infoBean.getString("MAILTO"), infoBean.getString("TYPE"), getLangue()));
            if (mailto.length() == 0) {
                throw new ErreurApplicative(MessageHelper.getCoreMessage(getLocale(), "UTILISATEUR.ERREUR.INEXISTANT"));
            }
            final String objet = (String) infoBean.get("SUJET");
            final String message = (String) infoBean.get("MESSAGE");
            LOG.info("envoi mail de : " + from + " ip : " + infoBean.getString("#remote-addr") + " UA : " + infoBean.getString("#user-agent"));
            LOG.info("objet : " + objet);
            LOG.info("message : " + message);
            envoyerEmail(from, mailto, objet, message);
            infoBean.set("MESSAGE_CONFIRMATION", MessageHelper.getCoreMessage("ST_ENVOI_MAIL_MESSAGE_CONFIRMATION"));
            ecranLogique = ECRAN_CONFIRMATION;
        }
        if (StringUtils.isBlank(ecranLogique)) {
            etat = FIN;
        }
    }

    /**
     * Envoi d'un email.
     *
     * @param from
     *            the from
     * @param to
     *            the to
     * @param object
     *            the object
     * @param content
     *            the content
     */
    private void envoyerEmail(final String from, final String to, final String object, final String content) {
        final JSBMailbox mailbox = new JSBMailbox(false);
        try {
            mailbox.sendTxtMsg(from, to, object, content);
        } catch (final MessagingException | EmailException e) {
            LOG.error("Exception occurred", e);
        }
    }

    /**
     * Renvoie la langue de l'application ou 0 (langue par défaut).
     *
     * @return the langue
     *
     */
    protected String getLangue() {
        final String langue = infoBean.getString("LANGUE");
        return langue == null ? "0" : langue;
    }

    /**
     * Récupération de l'objet annuaire.
     *
     * @param _ctx
     *            the _ctx
     * @param _code
     *            the _code
     * @param _langue
     *            the _langue
     *
     * @return the fiche
     *
     * @throws Exception
     *             the exception
     */
    protected FicheUniv getFicheUniv(final OMContext _ctx, final String _code, final String type, String _langue) throws Exception {
        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(type);
        ficheUniv.init();
        ficheUniv.setCtx(_ctx);
        if (StringUtils.isEmpty(_code)) {
            return ficheUniv;
        }
        if (StringUtils.isEmpty(_langue)) {
            _langue = "0";
        }
        // On cherche d'abord la version en ligne puis les autres versions
        int count = ficheUniv.selectCodeLangueEtat(_code, _langue, "0003");
        if (count == 0) {
            count = ficheUniv.selectCodeLangueEtat(_code, _langue, "");
        }
        if (count > 0) {
            ficheUniv.nextItem();
        }
        return ficheUniv;
    }
}
