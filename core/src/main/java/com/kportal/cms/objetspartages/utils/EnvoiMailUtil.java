package com.kportal.cms.objetspartages.utils;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

public class EnvoiMailUtil {

    private static final String NOM_PROCESSUS = "ENVOIMAIL";

    /**
     * Retourne l'url du processus envoiMail. Cette méthode rajoute l'url de la page courante en paramètre pour rediriger l'utilisateur vers cette page après saisi de son mail.
     *
     * @param idBean
     * @param idFiche
     * @param typeFiche
     * @return l'url complété de tout ses paramètres
     */
    public static String getAbsoluteUrlEnvoiMailPageCourante(final String idBean, final String idFiche, final String typeFiche) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        return getAbsoluteUrlEnvoiMail(idBean, idFiche, typeFiche, ctx.getUrlPageCourante(), ctx.getCodeRubriquePageCourante());
    }

    /**
     * Retourne l'url du processus envoiMail
     *
     * @param idBean
     *            l'id du bean permettant d'envoyer le mail
     * @param idFiche
     *            l'id de la fiche annuaire / annuaireksup... à laquelle on envoi le mail
     * @param typeFiche
     *            le type de fiche
     * @param urlRedirection
     *            l'url vers laquelle l'utilisateur sera redirigé après saisi de son mail
     * @return l'url complété de tout ses paramètres
     */
    public static String getAbsoluteUrlEnvoiMail(final String idBean, final String idFiche, final String typeFiche, final String urlRedirection, final String codeRubriquePageCourante) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String[][] parametres = new String[6][2];
        parametres[0][0] = "ID_BEAN";
        parametres[0][1] = StringUtils.defaultString(idBean);
        parametres[1][0] = "LANGUE";
        parametres[1][1] = LangueUtil.getLangueLocale(ContexteUtil.getContexteUniv().getLocale());
        parametres[2][0] = "TYPE";
        parametres[2][1] = StringUtils.defaultString(typeFiche);
        parametres[3][0] = "MAILTO";
        parametres[3][1] = StringUtils.defaultString(idFiche);
        parametres[4][0] = "URL_REDIRECT";
        parametres[4][1] = StringUtils.defaultString(urlRedirection);
        parametres[5][0] = "RH";
        parametres[5][1] = StringUtils.defaultString(codeRubriquePageCourante);
        final String url = ProcessusHelper.getUrlProcessAction(null, ApplicationContextManager.DEFAULT_CORE_CONTEXT, NOM_PROCESSUS, "CREER_MAIL", parametres);
        return URLResolver.getAbsoluteUrl(url, ctx);
    }
}
