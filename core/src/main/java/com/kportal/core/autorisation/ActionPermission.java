package com.kportal.core.autorisation;

import java.io.Serializable;

public class ActionPermission implements Serializable {

    private static final long serialVersionUID = 4997895680331127197L;

    /** The code. */
    public String code = "";

    /** The libelle. */
    public String libelle = "";

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code
     *            the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    public String getLibelle() {
        return libelle;
    }

    /**
     * Sets the libelle.
     *
     * @param libelle
     *            the new libelle
     */
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }
}
