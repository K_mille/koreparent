package com.kportal.core.config;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.Resource;

import com.kportal.extension.ExtensionConfigurer;

/**
 * Pre-load context.
 * <p>Enable Environment variables (Spring)</p>
 *
 * @since CORE-1729
 * @author cpoisnel
 * @see ExtensionConfigurer#initAllApplicationContext(org.springframework.context.ApplicationContext)
 *
 *
 */
public class ExtensionContextInitializer extends CoreContextInitializer {

    /**
     * Constructor with context ID.
     * @param idContext
     * ID context
     */
    public ExtensionContextInitializer(final String idContext) {
        super(idContext);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<Resource> findProperties(final ConfigurableApplicationContext applicationContext) {
        final List<Resource> resources = new ArrayList<>();final String pathProperties = applicationContext.getEnvironment().getProperty("path.properties");
        if (StringUtils.isNotEmpty(pathProperties)) {
            resources.addAll(getResources(String.format("%senv_%s.properties", pathProperties, idContext)));
        }
        resources.addAll(getResources(String.format("classpath*:application_%s.properties", idContext)));
        resources.addAll(getResources(String.format("classpath*:%s.properties", idContext)));
        return resources;
    }
}
