package com.kportal.core.config;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.PropertyPlaceholderConfigurer;
import org.springframework.core.io.Resource;
import org.springframework.util.PropertyPlaceholderHelper;

import com.jsbsoft.jtf.core.ApplicationContextManager;

/**
 * The Class MessageLoader.
 */
public class MessageLoader extends PropertyPlaceholderConfigurer {

    /** The Constant ID_BEAN. */
    protected static final String ID_BEAN = "messageLoader";

    /** The locations. */
    private Resource[] locations;

    /** The properties. */
    private Map<String, Properties> properties;

    private PropertyPlaceholderHelper placeHolderHelper;

    public static MessageLoader getInstance() {
        return (MessageLoader) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    public void init() throws IOException {
        placeHolderHelper = new PropertyPlaceholderHelper(placeholderPrefix, placeholderSuffix, valueSeparator, ignoreUnresolvablePlaceholders);
        properties = mergePropertiesByLocale();
    }

    /**
     * Gets the properties.
     *
     * @param locale
     *            the locale
     * @return the properties
     */
    public Properties getProperties(String locale) {
        return properties.get(locale);
    }

    /* (non-Javadoc)
     * @see org.springframework.core.io.support.PropertiesLoaderSupport#setLocations(org.springframework.core.io.Resource[])
     */
    @Override
    public void setLocations(Resource... locs) {
        super.setLocations(locs);
        locations = locs;
    }

    /**
     * Merge les différents fichiers de message en les regroupant par locale
     *
     * @return the map
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected Map<String, Properties> mergePropertiesByLocale() throws IOException {
        HashMap<String, Properties> hProperties = new HashMap<>();
        HashMap<String, List<Resource>> hLocations = new HashMap<>();
        // parcours des fichiers chargés au démarrage pour les regrouper par locale
        if (locations != null) {
            for (Resource location : locations) {
                String path = location.getFilename();
                String locale = StringUtils.substringAfter(path, "_");
                while (locale.contains("_") && locale.split("_")[0].length() > 2) {
                    locale = StringUtils.substringAfter(locale, "_");
                }
                locale = StringUtils.substringBeforeLast(locale, ".");
                if (isValidLocale(locale)) {
                    if (hLocations.get(locale) == null) {
                        hLocations.put(locale, new ArrayList<Resource>());
                    }
                    hLocations.get(locale).add(location);
                }
            }
        }
        // merge des fichiers par locale dans l'ordre de surcharge
        if (!hLocations.isEmpty()) {
            for (String locale : hLocations.keySet()) {
                List<Resource> l = hLocations.get(locale);
                super.setLocations(l.toArray(new Resource[l.size()]));
                hProperties.put(locale, mergeProperties());
            }
        }
        return hProperties;
    }

    /**
     * Checks si la locale trouvée est bien valide
     *
     * @param value
     *            the value
     * @return true, if is valid locale
     */
    boolean isValidLocale(String value) {
        Locale[] locales = Locale.getAvailableLocales();
        for (Locale l : locales) {
            if (value.equals(l.toString())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Cette méthode permet de recharger les properties à partir de la liste des fichiers #locations, liste qui pourrait être mise à jour avec un module de surcharge par ex
     *
     * @throws IOException
     */
    public synchronized void reload() throws IOException {
        properties = mergePropertiesByLocale();
    }

    public PropertyPlaceholderHelper getPlaceHolderHelper() {
        return placeHolderHelper;
    }
}
