package com.kportal.core.config;

import java.util.Locale;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class MessageLoaderUtil {

    public static String getMessage(String idCtx, final String langue, final String key) {
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        for (final MessageLoader loader : ApplicationContextManager.getBeansOfType(idCtx, MessageLoader.class).values()) {
            if (loader.getProperties(langue) != null) {
                final String message = loader.getProperties(langue).getProperty(key);
                if (message != null) {
                    // le helper permet de substituer la valeur d'une propriété ${key} par la valeur de 'key'
                    return loader.getPlaceHolderHelper().replacePlaceholders(message, loader.getProperties(langue));
                }
            }
        }
        return StringUtils.EMPTY;
    }

    public static Properties getMessages(String idCtx, final Locale locale) {
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        final Properties prop = new Properties();
        for (final MessageLoader loader : ApplicationContextManager.getBeansOfType(idCtx, MessageLoader.class).values()) {
            String langue = locale.getLanguage();
            if (StringUtils.isNotEmpty(locale.getCountry())) {
                langue += "_" + locale.getCountry();
            }
            if (loader.getProperties(langue) != null) {
                prop.putAll(loader.getProperties(langue));
            }
        }
        return prop;
    }
}
