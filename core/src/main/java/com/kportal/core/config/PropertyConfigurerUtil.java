package com.kportal.core.config;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.PlaceholderConfigurerSupport;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class PropertyConfigurerUtil {

    public static String getProperty(final String key) {
        return getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key);
    }

    public static String getProperty(String idCtx, final String key) {
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        String result = null;
        for (final PropertyConfigurer loader : ApplicationContextManager.getBeansOfType(idCtx, PropertyConfigurer.class).values()) {
            result = loader.getProperties().getProperty(key);
            if (result != null && result.contains(PlaceholderConfigurerSupport.DEFAULT_PLACEHOLDER_PREFIX)) {
                // le helper permet de substituer la valeur d'une propriété ${key} par la valeur de 'key'
                result = loader.getPlaceHolderHelper().replacePlaceholders(result, loader.getProperties());
                if (result.contains(PlaceholderConfigurerSupport.DEFAULT_PLACEHOLDER_PREFIX)) {
                    result = loader.getPlaceHolderHelper().replacePlaceholders(result, System.getProperties());
                }
            }
        }
        return result;
    }

    public static Properties getProperties(String idCtx) {
        if (StringUtils.isEmpty(idCtx)) {
            idCtx = ApplicationContextManager.DEFAULT_CORE_CONTEXT;
        }
        final Properties prop = new Properties();
        for (final PropertyConfigurer loader : ApplicationContextManager.getBeansOfType(idCtx, PropertyConfigurer.class).values()) {
            prop.putAll(loader.getProperties());
        }
        return prop;
    }
}