package com.kportal.core.config;

import java.util.Properties;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.univ.multisites.InfosSite;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

public class PropertyHelper {

    /** Logger. */
    private static final Logger LOG = LoggerFactory.getLogger(PropertyHelper.class);

    /**
     * Renvoit une propriété
     *
     * @param idCtx
     *            l'id de l'Extension
     * @param infosSite
     *            l'infoSite courant
     * @param key
     *            la clé de la propriété
     * @param searchCore
     *            recherche sur le core si propriété non trouvée sur l'extension
     * @return la valeur
     */
    public static String getProperty(String idCtx, final InfosSite infosSite, final String key, final boolean searchCore) {
        String res = null;
        final IExtension extension = ExtensionHelper.getExtension(idCtx) == null ? ExtensionHelper.getCoreExtension() : ExtensionHelper.getExtension(idCtx);
        if (extension != null) {
            // on va chercher la propriete associee au site ou au template
            if (infosSite != null) {
                // alias
                if (StringUtils.isNotBlank(infosSite.getAlias())) {
                    res = extension.getProperty(infosSite.getAlias() + "." + key);
                }
                // code template
                if (StringUtils.isEmpty(res) && StringUtils.isNotBlank(infosSite.getCodeTemplate())) {
                    res = extension.getProperty(infosSite.getCodeTemplate() + "." + key);
                }
            }
            // sinon, la propriete par défaut
            if (StringUtils.isEmpty(res)) {
                res = extension.getProperty(key);
            }
            // si vide
            if (StringUtils.isEmpty(res)) {
                LOG.debug("Propriété " + key + " introuvable pour l'extension id=" + idCtx);
                // appel recursif sur le core
                if (!idCtx.equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT) && searchCore) {
                    res = getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, infosSite, key, searchCore);
                }
            }
        }
        // l'extension n'existe pas ou n'est pas encore chargée
        else {
            res = PropertyConfigurerUtil.getProperty(idCtx, key);
        }
        return res;
    }

    /**
     * Renvoit une propriété de l'extension, pas de recherche sur le cores i propriété non trouvée
     *
     * @param idCtx
     *            l'id de l'Extension
     * @param key
     *            la clé de la propriété
     * @return la propriété de clé key de l'extension fourni en paramètre ou null si non trouvé
     */
    public static String getExtensionProperty(final String idCtx, final String key) {
        return getProperty(idCtx, getInfosSite(), key, false);
    }

    /**
     * Renvoit une propriété du core
     *
     * @param key
     *            la clé de la propriété
     * @return la valeur
     */
    public static String getCoreProperty(final String key) {
        return getProperty(ApplicationContextManager.DEFAULT_CORE_CONTEXT, getInfosSite(), key, true);
    }

    public static String getProperty(final String idCtx, final String key) {
        return getProperty(idCtx, getInfosSite(), key, true);
    }

    /**
     * Retourne la property caster en int
     *
     * @param key
     *            la clé de la properties
     * @param defaultValue
     *            la valeur par défaut si il n'est pas possible de caster en int
     * @return la valeur caster en int ou defaultValue
     */
    public static int getCorePropertyAsInt(final String key, final int defaultValue) {
        return getPropertyAsInt(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key, defaultValue);
    }

    /**
     * Retourne la property caster en int
     *
     * @param idCtx
     *            l'id de l'extension dont on souhaite récupérer la properties
     * @param key
     *            la clé de la properties
     * @param defaultValue
     *            la valeur par défaut si il n'est pas possible de caster en int
     * @return la valeur caster en int ou defaultValue
     */
    public static int getPropertyAsInt(final String idCtx, final String key, final int defaultValue) {
        final String value = getProperty(idCtx, getInfosSite(), key, true);
        int intValue = defaultValue;
        try {
            intValue = Integer.valueOf(value);
        } catch (final NumberFormatException ignore) {
        }
        return intValue;
    }

    /**
     * Retourne la property caster en double
     *
     * @param key
     *            la clé de la properties
     * @param defaultValue
     *            la valeur par défaut si il n'est pas possible de caster en double
     * @return la valeur caster en double ou defaultValue
     */
    public static double getCorePropertyAsDouble(final String key, final double defaultValue) {
        return getPropertyAsDouble(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key, defaultValue);
    }

    /**
     * Retourne la property caster en double
     *
     * @param idCtx
     *            l'id de l'extension dont on souhaite récupérer la properties
     * @param key
     *            la clé de la properties
     * @param defaultValue
     *            la valeur par défaut si il n'est pas possible de caster en double
     * @return la valeur caster en double ou defaultValue
     */
    public static double getPropertyAsDouble(final String idCtx, final String key, final double defaultValue) {
        final String value = getProperty(idCtx, getInfosSite(), key, true);
        double doubleValue = defaultValue;
        try {
            doubleValue = Double.valueOf(value);
        } catch (final NumberFormatException ignore) {
        }
        return doubleValue;
    }

    /**
     * Retourne la property caster en long
     *
     * @param key
     *            la clé de la properties
     * @param defaultValue
     *            la valeur par défaut si il n'est pas possible de caster en long
     * @return la valeur caster en long ou defaultValue
     */
    public static long getCorePropertyAsLong(final String key, final long defaultValue) {
        return getPropertyAsLong(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key, defaultValue);
    }

    /**
     * Retourne la property caster en long
     *
     * @param idCtx
     *            l'id de l'extension dont on souhaite récupérer la properties
     * @param key
     *            la clé de la properties
     * @param defaultValue
     *            la valeur par défaut si il n'est pas possible de caster en long
     * @return la valeur caster en long ou defaultValue
     */
    public static long getPropertyAsLong(final String idCtx, final String key, final long defaultValue) {
        final String value = getProperty(idCtx, getInfosSite(), key, true);
        long doubleValue = defaultValue;
        try {
            doubleValue = Long.valueOf(value);
        } catch (final NumberFormatException ignore) {
        }
        return doubleValue;
    }

    /**
     * Retourne la property en boolean
     *
     * @param key
     *            la clé de la properties
     * @return vrai si la properties = true, false sinon
     */
    public static boolean getCorePropertyAsBoolean(final String key) {
        return getPropertyAsBoolean(ApplicationContextManager.DEFAULT_CORE_CONTEXT, key);
    }

    /**
     * Retourne la property en boolean
     *
     * @param idCtx
     *            l'id de l'extension dont on souhaite récupérer la properties
     * @param key
     *            la clé de la properties
     * @return vrai si la properties = true, false sinon
     */
    public static boolean getPropertyAsBoolean(final String idCtx, final String key) {
        final String value = getProperty(idCtx, getInfosSite(), key, true);
        return Boolean.valueOf(value);
    }

    public static Properties getProperties(final String idCtx) {
        final IExtension extension = ExtensionHelper.getExtension(idCtx) == null ? ExtensionHelper.getExtension("core") : ExtensionHelper.getExtension(idCtx);
        return extension.getProperties();
    }

    /**
     *
     * @return l'ensemble des properties de l'application (y compris sur les extensions)
     */
    public static Properties getAllProperties() {
        final Properties allProperties = new Properties();
        for (final String idCtx : ExtensionHelper.getExtensionManager().getExtensions().keySet()) {
            allProperties.putAll(getProperties(idCtx));
        }
        return allProperties;
    }

    private static InfosSite getInfosSite() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx != null) {
            return ctx.getInfosSite();
        } else {
            return null;
        }
    }
}
