package com.kportal.core.context;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;

public class AttributeToOverrideBean implements MergedContextBean {

    boolean mandatory = false;

    String idBeanToMerge = "";

    String idExtensionToMerge = "";

    Map<String, Object> attributes = new HashMap<>();

    @Override
    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(final boolean mandatory) {
        this.mandatory = mandatory;
    }

    @Override
    public String getIdBeanToMerge() {
        return idBeanToMerge;
    }

    @Override
    public void setIdBeanToMerge(final String idBeanToMerge) {
        this.idBeanToMerge = idBeanToMerge;
    }

    @Override
    public String getIdExtensionToMerge() {
        return idExtensionToMerge;
    }

    @Override
    public void setIdExtensionToMerge(final String idExtensionToMerge) {
        this.idExtensionToMerge = idExtensionToMerge;
    }

    public Map<String, Object> getAttributes() {
        return attributes;
    }

    public void setAttributes(final Map<String, Object> attributes) {
        this.attributes = attributes;
    }

    @Override
    public void merge() throws Exception {
        final Object bean = ApplicationContextManager.getBean(idExtensionToMerge, idBeanToMerge);
        for (final String setter : attributes.keySet()) {
            final String methodName = "set" + setter.substring(0, 1).toUpperCase() + setter.substring(1);
            final Object args[] = {attributes.get(setter)};
            // recherche de la méthode sur la classe
            for (final Method m : bean.getClass().getMethods()) {
                if (m.getName().equals(methodName)) {
                    m.invoke(bean, args);
                    break;
                }
            }
        }
    }
}
