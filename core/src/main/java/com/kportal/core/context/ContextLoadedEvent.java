package com.kportal.core.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;

/**
 * Custom Event permettant de signaler la fin de rechargement des contextes spring
 * Cet event est utilisé notamment au niveau de la classe JGroupsCluster (kosmos-cluster.jar)
 * qui bloque l'ajout de messageHandler jgroups une fois les contextes chargés
 */
public class ContextLoadedEvent extends ApplicationContextEvent {

    private static final long serialVersionUID = -7587893219497559564L;

    public ContextLoadedEvent(ApplicationContext source) {
        super(source);
    }
}
