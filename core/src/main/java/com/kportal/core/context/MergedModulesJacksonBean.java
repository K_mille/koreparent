package com.kportal.core.context;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.BeanNotOfRequiredTypeException;

import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsbsoft.jtf.core.ApplicationContextManager;

/**
 * Fusion des modules de Jackson pour un bean de type ObjectMapper.
 *
 * <p>
 *     Une exception de {@link BeanNotOfRequiredTypeException} est soulevée si le bean n'est pas du bon type.
 * </p>
 *
 * @author cpoisnel
 */
public class MergedModulesJacksonBean implements MergedContextBean {

    boolean mandatory = false;

    String idBeanToMerge = "";

    String idExtensionToMerge = "";

    private Collection<Module> modules;

    /**
     * Fusion les modules d'un objet ObjectMapper.
     * @throws Exception bean non trouvé ou d'un autre type qu'ObjectMapper.
     */
    @Override
    public void merge() throws Exception {
        final Object bean = ApplicationContextManager.getBean(idExtensionToMerge, idBeanToMerge);
        if (ObjectMapper.class.isAssignableFrom(bean.getClass())) {
            ObjectMapper beanObjectMapper = (ObjectMapper) bean;
            if (CollectionUtils.isNotEmpty(modules)) {
                beanObjectMapper.registerModules(modules);
            }
        } else {
            throw new BeanNotOfRequiredTypeException(idBeanToMerge, ObjectMapper.class, bean.getClass());
        }
    }

    @Override
    public String getIdBeanToMerge() {
        return idBeanToMerge;
    }

    @Override
    public String getIdExtensionToMerge() {
        return idExtensionToMerge;
    }

    @Override
    public void setIdBeanToMerge(final String idBeanToMerge) {
        this.idBeanToMerge = idBeanToMerge;
    }

    @Override
    public void setIdExtensionToMerge(final String idExtensionToMerge) {
        this.idExtensionToMerge = idExtensionToMerge;
    }

    public Collection<Module> getModules() {
        return modules;
    }

    public void setModules(final Collection<Module> modules) {
        this.modules = modules;
    }

    @Override
    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(final boolean mandatory) {
        this.mandatory = mandatory;
    }
}
