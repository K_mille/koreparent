package com.kportal.core.context;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;

import com.jsbsoft.jtf.core.ApplicationContextManager;

/**
 * Created on 28/06/17.
 */
public class SetToAddBean implements MergedContextBean {

    private boolean mandatory = false;

    private String idBeanToMerge = "";

    private String idExtensionToMerge = "";

    private String setToMerge = "";

    private Set<Object> add = new HashSet<>();

    @Override
    public String getIdBeanToMerge() {
        return idBeanToMerge;
    }

    @Override
    public void setIdBeanToMerge(final String idBeanToMerge) {
        this.idBeanToMerge = idBeanToMerge;
    }

    @Override
    public String getIdExtensionToMerge() {
        return idExtensionToMerge;
    }

    @Override
    public void setIdExtensionToMerge(final String idExtensionToMerge) {
        this.idExtensionToMerge = idExtensionToMerge;
    }

    public String getSetToMerge() {
        return setToMerge;
    }

    public void setSetToMerge(final String setNameToMerge) {
        this.setToMerge = setNameToMerge;
    }

    public void setAdd(final Set<Object> add) {
        this.add = add;
    }

    @Override
    public void merge() throws Exception {
        getSet().addAll(add);
    }

    private Set<Object> getSet() throws Exception {
        final Object beanToMerge = ApplicationContextManager.getBean(idExtensionToMerge, idBeanToMerge);
        if (beanToMerge instanceof Set<?>) {
            return (Set<Object>) beanToMerge;
        }
        final String methodName = "get" + setToMerge.substring(0, 1).toUpperCase() + setToMerge.substring(1);
        final Class<?> noparams[] = {};
        final Object noargs[] = {};
        final Method method = beanToMerge.getClass().getMethod(methodName, noparams);
        return (Set<Object>) method.invoke(beanToMerge, noargs);
    }

    @Override
    public boolean isMandatory() {
        return mandatory;
    }

    public void setMandatory(final boolean mandatory) {
        this.mandatory = mandatory;
    }
}

