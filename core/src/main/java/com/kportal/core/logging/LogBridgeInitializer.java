package com.kportal.core.logging;

import java.util.logging.LogManager;

import org.slf4j.bridge.SLF4JBridgeHandler;

public class LogBridgeInitializer {

    public void init() {
        if (!SLF4JBridgeHandler.isInstalled()) {
            LogManager.getLogManager().reset();
            SLF4JBridgeHandler.install();
        }
    }
}
