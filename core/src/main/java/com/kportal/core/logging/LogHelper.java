package com.kportal.core.logging;

import java.io.BufferedReader;
import java.io.StringReader;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

import com.kportal.core.cluster.ClusterHelper;

/**
 * Classe utilitaire contenant les properties spécifique au logger de l'application
 */
public class LogHelper {

    /**
     * L'extension classique d'un log.
     */
    public static final String LOG_EXTENSION = ".log";

    /**
     * Le chemin du dossier de log de l'appli
     */
    public static final String PROP_LOG_PATH = "logs.path";

    /**
     * Si {@link LogHelper#PROP_LOG_PATH} n'est pas setter dans les properties, le chemin est WEB-INF/logs
     */
    public static final String DEFAULT_LOG_PATH = "/logs";

    /**
     * Le nom du fichier de log par défaut de l'applicatiob (webapp)
     */
    public static final String PROP_LOG_FILE = "logs.defaultFile";

    /**
     * La forme des logs de l'appli : date [classe du log:ligne] [level de log] message
     */
    public static final String PROP_LOG_LAYOUT_PATTERN = "logs.layoutPattern";

    /**
     * Le changement de nom du fichier de log après la rotation du log (par défaut, on rajoute la date à la fin du fichier)
     */
    public static final String PROP_LOG_FILENAME_PATTERN = "logs.fileNamePattern";

    /**
     * Le nombre en mois de conservation des logs (par défaut 6 mois)
     */
    public static final String PROP_LOG_MAX_HISTORY = "logs.maxHistory";

    /**
     * La taille max de conservation des logs. Elle n'est pas setter par défaut car on gère la durée de consercation
     */
    public static final String PROP_LOG_MAX_SIZE = "logs.maxSize";

    /**
     * Les fichiers doivent ils être cleaner au démarage de l'appli (par défaut, non setter car les applis ne redémarre pas tout le temps)
     */
    public static final String PROP_LOG_CLEAN_HISTORY_ON_START = "logs.cleanHistoryOnStart";

    public static String getServerLogName(String name) {
        if (StringUtils.isNotBlank(ClusterHelper.getCurrentJvmRoute())) {
            return ClusterHelper.getCurrentJvmRoute() + "-" + name;
        }
        return name;
    }

    public static void logInfoFromLine(Logger logger, String texte) throws Exception {
        try (BufferedReader br = new BufferedReader(new StringReader(texte));) {
            String line;
            while ((line = br.readLine()) != null) {
                if (StringUtils.isNotEmpty(line)) {
                    logger.info(line);
                }
            }
        }
    }
}
