package com.kportal.core.resources.optimizer.manager;

import java.util.Map;
import java.util.Properties;

import com.kportal.core.resources.optimizer.model.CustomGroupExtractor;
import com.kportal.core.resources.optimizer.model.CustomXmlModelFactory;
import com.kportal.core.resources.optimizer.processor.PropertyMatcherProcessor;

import ro.isdc.wro.manager.factory.ConfigurableWroManagerFactory;
import ro.isdc.wro.model.factory.WroModelFactory;
import ro.isdc.wro.model.group.GroupExtractor;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;

/**
 * Ajoute le preprocesseur {@link PropertyMatcherProcessor} à la liste des pré-processeurs à executer
 *
 * @author olivier.camon
 *
 */
public class CustomWroManagerFactory extends ConfigurableWroManagerFactory {

    private Properties properties;

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }

    @Override
    protected Properties newConfigProperties() {
        return properties;
    }

    @Override
    protected GroupExtractor newGroupExtractor() {
        return new CustomGroupExtractor();
    }

    @Override
    protected WroModelFactory newModelFactory() {
        return new CustomXmlModelFactory();
    }

    @Override
    protected void contributePreProcessors(Map<String, ResourcePreProcessor> map) {
        map.put("propertyMatcher", new PropertyMatcherProcessor());
        super.contributePreProcessors(map);
    }
}
