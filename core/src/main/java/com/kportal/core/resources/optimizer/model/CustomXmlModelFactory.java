package com.kportal.core.resources.optimizer.model;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Locale;

import org.apache.commons.io.input.AutoCloseInputStream;

import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;

import ro.isdc.wro.model.WroModel;
import ro.isdc.wro.model.WroModelInspector;
import ro.isdc.wro.model.factory.XmlModelFactory;
import ro.isdc.wro.model.group.Group;
import ro.isdc.wro.model.resource.Resource;

/**
 * Classe permettant de gérer les groupes WRO du back et du front.
 * Pour le BO, on va chercher un fichier spécifiques {@link CustomXmlModelFactory#WRO_BO_FILE} puis on va
 * chercher dans chaque extensions si elle possède ou non un group équivalent. Si c'est le cas, on rajoute les scripts
 * déclarés.
 * Pour le FO c'est le même mécanisme mais le ficheir WRO du produit est {@link CustomXmlModelFactory#WRO_FILE} et est
 * surchargeable par les projets.
 */
public class CustomXmlModelFactory extends XmlModelFactory {

    private static final String WRO_XML = "wro.xml";

    public static final String WRO_FILE = "classes/" + WRO_XML;

    public static final String WRO_BO_FILE = "com/kportal/ihm/" + WRO_XML;

    @Override
    protected String getDefaultModelFilename() {
        return WRO_FILE;
    }

    @Override
    public synchronized WroModel create() {
        // wro FO du core
        // chargement automatique du fichier (classpath :classes/wro.xml)
        final WroModel modelWebApp = super.create();
        WroModelInspector inspectorWebApp = new WroModelInspector(modelWebApp);
        // boucle sur les groupes du fichier
        for (final String groupName : inspectorWebApp.getGroupNames()) {
            // on recupere le group sur la webapp
            final Group webAppGroup = inspectorWebApp.getGroupByName(groupName);
            final Locale locale = MessageHelper.parseLocaleFromFileName(groupName);
            // on boucle sur les resources du FO par groupe
            for (final Resource resource : inspectorWebApp.getGroupByName(groupName).getResources()) {
                if (locale != null) {
                    final IExtension core = ExtensionHelper.getCoreExtension();
                    final ResourceLangueDecorator resourceLocale = ResourceLangueDecorator.create(core, locale, core.getRelativePath() + resource.getUri(), resource.getType());
                    final List<Resource> bind = new ArrayList<>();
                    bind.add(resourceLocale);
                    webAppGroup.replace(resource, bind);
                }
            }
        }
        // wro BO du core
        // chargement du fichier wro du BO (classpath :com/kportal/ihm/wro.xml)
        final XmlModelFactory importedModelFactoryBO = new XmlModelFactory() {

            @Override
            protected InputStream getModelResourceAsStream() throws IOException {
                return new AutoCloseInputStream(this.getClass().getClassLoader().getResourceAsStream(WRO_BO_FILE));
            }
        };
        final WroModel modelBO = importedModelFactoryBO.create();
        final WroModelInspector inspectorBO = new WroModelInspector(modelBO);
        // boucle sur les groupes du fichier
        for (final String groupName : inspectorBO.getGroupNames()) {
            // on ajoute le nouveau groupe si il n'existe pas
            if (!inspectorWebApp.getGroupNames().contains(groupName)) {
                modelWebApp.addGroup(inspectorBO.getGroupByName(groupName));
                inspectorWebApp = new WroModelInspector(modelWebApp);
            }
            // on recupere le group sur la webapp
            final Group webAppGroup = inspectorWebApp.getGroupByName(groupName);
            final Locale locale = MessageHelper.parseLocaleFromFileName(groupName);
            // on boucle sur les resources du BO par groupe
            for (final Resource resource : inspectorBO.getGroupByName(groupName).getResources()) {
                if (locale != null) {
                    final IExtension core = ExtensionHelper.getCoreExtension();
                    final ResourceLangueDecorator resourceLocale = ResourceLangueDecorator.create(core, locale, core.getRelativePath() + resource.getUri(), resource.getType());
                    final List<Resource> bind = new ArrayList<>();
                    bind.add(resourceLocale);
                    webAppGroup.replace(resource, bind);
                }
            }
        }
        // wro par extension
        // chargement des fichiers wro par extension
        for (final IExtension extension : ExtensionHelper.getExtensionManager().getExtensions().values()) {
            // on regarde d'abord à la racine du projet /WEB-INF/classes/idExtenxion_wro.xml
            String path = WebAppUtil.getClassPath() + extension.getId() + "_" + WRO_XML;
            // si le fichier n'existe pas on regarde dans l'extension (si != du core) : extensions/idExtension/WEB-INF/classes/wro.xml
            if (!(new File(path).exists()) && !WebAppUtil.getExtensionClassPath(extension.getId()).equals(WebAppUtil.getClassPath())) {
                path = WebAppUtil.getExtensionClassPath(extension.getId()) + WRO_XML;
            }
            if (new File(path).exists()) {
                final String wroPath = path;
                final XmlModelFactory importedModelFactory = new XmlModelFactory() {

                    @Override
                    protected InputStream getModelResourceAsStream() throws IOException {
                        return new AutoCloseInputStream(new FileInputStream(wroPath));
                    }
                };
                final WroModel modelExtension = importedModelFactory.create();
                final WroModelInspector inspectorExtension = new WroModelInspector(modelExtension);
                // boucle sur les groupes de l'extension
                for (final String groupName : inspectorExtension.getGroupNames()) {
                    // on ajoute le nouveau groupe si il n'existe pas
                    if (!inspectorWebApp.getGroupNames().contains(groupName)) {
                        modelWebApp.addGroup(inspectorExtension.getGroupByName(groupName));
                        inspectorWebApp = new WroModelInspector(modelWebApp);
                    }
                    // on recupere le group sur la webapp
                    final Group webAppGroup = inspectorWebApp.getGroupByName(groupName);
                    final Locale locale = MessageHelper.parseLocaleFromFileName(groupName);
                    // on boucle sur les resources du groupe de l'extension
                    for (final Resource resource : inspectorExtension.getGroupByName(groupName).getResources()) {
                        final String uri = resource.getUri().startsWith(extension.getRelativePath()) ? resource.getUri() : extension.getRelativePath() + resource.getUri();
                        Resource nextResource;
                        if (locale != null) {
                            nextResource = ResourceLangueDecorator.create(extension, locale, uri, resource.getType());
                        } else {
                            nextResource = Resource.create(uri, resource.getType());
                        }
                        // on teste que la ressource n'existe pas deja avant de l'ajouter
                        if (!webAppGroup.getResources().contains(nextResource)) {
                            webAppGroup.addResource(nextResource);
                        } else {
                            webAppGroup.replace(resource, Collections.singletonList(nextResource));
                        }
                    }
                }
            }
        }
        return modelWebApp;
    }
}
