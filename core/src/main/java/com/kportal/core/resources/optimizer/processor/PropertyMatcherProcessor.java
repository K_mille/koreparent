package com.kportal.core.resources.optimizer.processor;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.MessageHelper;
import com.kportal.core.resources.optimizer.model.ResourceLangueDecorator;

import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.processor.ResourcePreProcessor;
import ro.isdc.wro.util.WroUtil;

/**
 * Permet de parser le contenu d'une ressource & de remplacer les expressions "${CLE}" par leur valeurs dans les fichiers properties de l'application. Ce traitement est effectué
 * uniquement si la ressource est de type {@link ResourceLangueDecorator}
 *
 * @author olivier.camon
 *
 */
public class PropertyMatcherProcessor implements ResourcePreProcessor {

    private static final Pattern PATTERN_PLACEHOLDER = Pattern.compile(WroUtil.loadRegexpWithKey("placeholder"));

    private static final Logger LOG = LoggerFactory.getLogger(PropertyMatcherProcessor.class);

    @Override
    public void process(Resource resource, Reader reader, Writer writer) throws IOException {
        if (resource instanceof ResourceLangueDecorator) {
            final String content = IOUtils.toString(reader);
            final Matcher matcher = PATTERN_PLACEHOLDER.matcher(content);
            final StringBuffer sb = new StringBuffer();
            ResourceLangueDecorator resourceLangue = (ResourceLangueDecorator) resource;
            while (matcher.find()) {
                final String variableName = matcher.group(1);
                LOG.debug("found placeholder: {}", variableName);
                matcher.appendReplacement(sb, replaceVariable(MessageHelper.getMessage(resourceLangue.getIdExtension(), resourceLangue.getLocale(), variableName), variableName));
            }
            matcher.appendTail(sb);
            writer.write(sb.toString());
        } else {
            writer.write(IOUtils.toString(reader));
        }
    }

    /**
     * @param variableName
     * @return
     */
    private String replaceVariable(final String variableValue, final String variableName) {
        final String result = variableValue == null ? StringUtils.EMPTY : variableValue;
        LOG.debug("replacing: [{}] with [{}]", variableName, result);
        return result;
    }
}
