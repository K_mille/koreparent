package com.kportal.core.resources.optimizer.web;

import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.ServiceActivator;

import com.kportal.extension.module.AbstractBeanManager;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

import ro.isdc.wro.http.support.ServletContextAttributeHelper;

public class CacheWroManager extends AbstractBeanManager {

    /**
     * {@inheritDoc}
     */
    @Override
    @ServiceActivator(inputChannel = CHANNEL_MODULE)
    @Order(ORDER_CORE + 2)
    public void update() {
        super.update();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        ContexteUniv ctx = ContexteUtil.getContexteUniv();
        if (ctx != null) {
            ServletContextAttributeHelper helper = new ServletContextAttributeHelper(ctx.getServletContext());
            helper.getWroConfiguration().reloadCache();
            helper.getWroConfiguration().reloadModel();
        }
    }
}
