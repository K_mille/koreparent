package com.kportal.core.resources.optimizer.web;

import java.io.IOException;
import java.io.Writer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.URLResolver;

/**
 * Tag générant un élément link pour charger une CSS.
 *
 * @author jean-noel.ribette
 */
public class LinkResourceTag extends TagSupport {

    /**
     *
     */
    private static final long serialVersionUID = 5957986833659620923L;

    private static final String DEFAULT_TYPE = "text/css";

    /**
     * Nom du groupe.
     */
    private String group;

    /**
     * Type de la ressource (par défaut text/css)
     */
    private String type = DEFAULT_TYPE;

    /**
     * Media
     */
    private String media;

    /**
     * Title.
     */
    private String title;

    @Override
    public int doStartTag() throws JspException {
        try {
            final Writer writer = pageContext.getOut();
            for (final String url : ResourceUtils.getResourceUrl(pageContext.getServletContext(), (HttpServletRequest) pageContext.getRequest(), (HttpServletResponse) pageContext.getResponse(), group, type, StringUtils.EMPTY)) {
                writer.write("<link rel=\"stylesheet\" type=\"");
                writer.write(type);
                if (media != null) {
                    writer.write("\" media=\"");
                    writer.write(media);
                }
                // Calcule l'URL de la ressource.
                writer.write("\" href=\"");
                writer.write(URLResolver.getRequestBase((HttpServletRequest) pageContext.getRequest()));
                writer.write(url);
                // Ajoute le titre.
                if (title != null) {
                    writer.write("\" title=\"");
                    writer.write(title);
                }
                writer.write("\"/>");
            }
        } catch (final IOException e) {
            throw new JspException(e);
        }
        return SKIP_BODY;
    }

    @Override
    public void release() {
        group = null;
        title = null;
        type = DEFAULT_TYPE;
        media = null;
    }

    public void setGroup(final String group) {
        this.group = group;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public void setMedia(final String media) {
        this.media = media;
    }

    public void setTitle(final String title) {
        this.title = title;
    }
}
