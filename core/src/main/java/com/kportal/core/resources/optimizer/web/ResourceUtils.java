package com.kportal.core.resources.optimizer.web;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;

import ro.isdc.wro.WroRuntimeException;
import ro.isdc.wro.cache.CacheKey;
import ro.isdc.wro.cache.CacheValue;
import ro.isdc.wro.config.Context;
import ro.isdc.wro.config.jmx.WroConfiguration;
import ro.isdc.wro.http.support.ServletContextAttributeHelper;
import ro.isdc.wro.model.WroModelInspector;
import ro.isdc.wro.model.factory.WroModelFactory;
import ro.isdc.wro.model.group.Group;
import ro.isdc.wro.model.resource.Resource;
import ro.isdc.wro.model.resource.ResourceType;

/**
 * Helper.
 */
public class ResourceUtils {

    private static final Logger LOG = LoggerFactory.getLogger(ResourceUtils.class);

    /**
     * Retourne le mapping du servlet, sans / devant.
     */
    static String getMapping() {
        // TODO lire le mapping dans web.xml pour ne pas l'avoir en dur.
        return "wro";
    }

    /**
     * Calcule l'URL d'un élément d'un groupe
     */
    public static String computeGroupMemberUrl(ServletContext context, HttpServletRequest request, String groupName, String relativePath) throws JspException {
        String checksum = "";
        // Calcule l'URL.
        // Retourne.
        return request.getContextPath() + '/' + getMapping() + '/' + groupName + '/' + checksum + relativePath;
    }

    private static String getCheckSum(ServletContext context, String groupName, String extension) {
        ServletContextAttributeHelper helper = new ServletContextAttributeHelper(context);
        CacheKey cacheKey = new CacheKey(groupName, ResourceType.get(extension.toUpperCase()), false);
        CacheValue cacheValue = helper.getManagerFactory().create().getCacheStrategy().get(cacheKey);
        return cacheValue.getHash();
    }

    public static List<String> getResourceUrl(ServletContext context, HttpServletRequest request, HttpServletResponse response, String groupName, String type, String locale) throws JspException {
        ArrayList<String> res = new ArrayList<>();
        WroConfiguration config = Context.get().getConfig();
        if (config.isDebug()) {
            ServletContextAttributeHelper helper = new ServletContextAttributeHelper(context);
            WroModelFactory model = helper.getManagerFactory().create().getModelFactory();
            WroModelInspector inspector = new WroModelInspector(model.create());
            if (StringUtils.isNotBlank(locale)) {
                groupName = groupName + "_" + locale;
            }
            Group group = inspector.getGroupByName(groupName);
            if (group != null) {
                for (Resource resource : group.getResources()) {
                    res.add(resource.getUri());
                }
            }
        } else {
            res.add(computeUrl(context, request, response, groupName, type, locale));
        }
        return res;
    }

    /**
     * Calcule l'URL d'un groupe
     *
     * @param context
     *            Le servlet content
     * @param request
     *            La requête HTTP courante
     * @param groupName
     *            Le nom du groupe
     * @param type
     *            Le type mime du groupe
     * @return L'URL du groupe
     * @throws JspException
     *             En cas d'erreur sur le nom du groupe ou du type
     */
    private static String computeUrl(ServletContext context, HttpServletRequest request, HttpServletResponse response, String groupName, String type, String locale) throws JspException {
        StringBuilder builder = new StringBuilder();
        // Récupère l'extension.
        String extension;
        if ("text/javascript".equals(type)) {
            extension = "js";
        } else if ("text/css".equals(type)) {
            extension = "css";
        } else {
            throw new JspException("type " + type + " non géré");
        }
        try {
            String checksum = StringUtils.EMPTY;
            if (StringUtils.isNotBlank(locale)) {
                try {
                    checksum = getCheckSum(context, groupName + "_" + locale, extension);
                    groupName += "_" + locale;
                } catch (WroRuntimeException e) {
                    LOG.debug("wro error", e);
                    checksum = getCheckSum(context, groupName + "_" + LangueUtil.getDefaultLocale(), extension);
                    groupName += "_" + LangueUtil.getDefaultLocale();
                }
            } else {
                checksum = getCheckSum(context, groupName, extension);
            }
            // calcul l'URL.
            builder.append(request.getContextPath());
            builder.append('/');
            builder.append(getMapping());
            builder.append('/');
            builder.append(groupName);
            builder.append('/');
            builder.append(checksum);
            builder.append(".").append(extension);
        } catch (Exception e) {
            LOG.error("groupe " + extension + " " + groupName + " inconnu", e);
        }
        // Retourne.
        return builder.toString();
    }
}
