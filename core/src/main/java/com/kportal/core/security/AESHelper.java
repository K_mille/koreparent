package com.kportal.core.security;

import java.security.GeneralSecurityException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;

/**
 * Classe utilitaire permettant d'encoder des valeurs en AES
 */
public class AESHelper {

    private static final Logger LOG = LoggerFactory.getLogger(AESHelper.class);

    private static final String ALGO_AES = "AES";

    private static final String ALGO_MD5 = "MD5";

    private static final String PROP_AES_KEY = "aes.key";

    /**
     * Encode la valeur data en AES
     * @param data la valeur à encoder
     * @param localKey la clé de chiffrement à utiliesr
     * @return la valeur encodée
     */
    public static String encrypt(String data, String localKey) {
        try {
            Cipher cipher = Cipher.getInstance(ALGO_AES);
            cipher.init(Cipher.ENCRYPT_MODE, generateKey(localKey));
            return Base64.encodeBase64String(cipher.doFinal(data.getBytes()));
        } catch (GeneralSecurityException e) {
            LOG.error("Error while encrypting", e);
        }
        return null;
    }

    /**
     * Décode la valeur fourni en paramètre
     * @param data la valeur à décoder
     * @param localKey la clé de chiffrement
     * @return la valeur décodée
     */
    public static String decrypt(String data, String localKey) {
        try {
            Cipher cipher = Cipher.getInstance(ALGO_AES);
            cipher.init(Cipher.DECRYPT_MODE, generateKey(localKey));
            return new String(cipher.doFinal(Base64.decodeBase64(data.getBytes())));
        } catch (GeneralSecurityException e) {
            LOG.error("Error while decrypting AES", e);
        }
        return null;
    }

    private static SecretKeySpec generateKey(String key) throws NoSuchAlgorithmException {
        String sKey = StringUtils.defaultIfEmpty(key, StringUtils.defaultIfEmpty(PropertyHelper.getCoreProperty(PROP_AES_KEY), PROP_AES_KEY));
        MessageDigest digest = MessageDigest.getInstance(ALGO_MD5);
        return new SecretKeySpec(digest.digest(sKey.getBytes()), ALGO_AES);
    }
}
