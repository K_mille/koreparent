package com.kportal.extension;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.context.support.XmlWebApplicationContext;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.impl.ServiceManager;

public class ExtensionListener implements ApplicationListener<ContextRefreshedEvent> {

    public ExtensionListener() {
        super();
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getSource() instanceof XmlWebApplicationContext) {
            if ("Root WebApplicationContext".equalsIgnoreCase(((XmlWebApplicationContext) event.getSource()).getDisplayName())) {
                ServiceManager serviceManager = ApplicationContextManager.getCoreContextBean(ServiceManager.ID_BEAN, ServiceManager.class);
                serviceManager.refresh();
                ExtensionConfigurer extConfig = ExtensionHelper.getExtensionConfigurer();
                extConfig.refresh();
            }
        }
    }
}
