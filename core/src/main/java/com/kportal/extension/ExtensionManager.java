package com.kportal.extension;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.io.FileDeleteStrategy;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.FlywayException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.publish.ServiceCorePublisher;
import com.kportal.core.webapp.WebAppUtil;
import com.kosmos.publish.ServiceCorePublisher;
import com.kportal.extension.bean.ExtensionBean;
import com.kportal.extension.service.ServiceExtension;
import com.kportal.extension.service.ServiceModule;
import com.kportal.util.compress.Zip;

import fr.kosmos.cluster.api.Cluster;
import fr.kosmos.cluster.api.MessageHandler;

/**
 * The Class ModuleManager.
 */
public class ExtensionManager implements MessageHandler<Serializable> {

    protected static final String ID_BEAN = "extensionManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtensionManager.class);

    public ExtensionConfigurer extensionConfigurer;

    private Cluster cluster;

    private Map<String, IExtension> extensions = new HashMap<>();

    private Version core;

    private ServiceExtension serviceExtension;

    private ServiceModule serviceModule;

    private static final String FLYWAY_DEFAULT_BASELINE = "0.0";

    /**
     * Service de publication (pas d'interception AOP des services, publication effectuée de manière programmatique)
     */
    private ServiceCorePublisher serviceCorePublisher;

    public void init() {
        if (cluster != null) {
            cluster.registerService(this);
        }
    }

    public synchronized void refresh() {
        extensions = new HashMap<>();
        // chargement du core en premier
        loadExtension(ApplicationContextManager.DEFAULT_CORE_CONTEXT);
        // Si problème de chargement du core on arrete le tomcat
        if (extensions.get(ApplicationContextManager.DEFAULT_CORE_CONTEXT) == null) {
            throw new RuntimeException("Impossible de démarrer l'application sans extension core");
        }
        // boucle sur les autres contexes
        for (final String idAppCtx : extensionConfigurer.getExtensionNames().keySet()) {
            if (!idAppCtx.equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT)) {
                loadExtension(idAppCtx);
            }
        }
        // nettoyage de la base contenant les extensions et les modules
        cleanDB();

        LOGGER.info("Chargement des extensions OK");

        serviceCorePublisher.publish("extensionManager","extensionManager","refresh");
    }

    private void loadExtension(final String idAppCtx) {
        // on ne charge qu'une extension par contexte
        final ApplicationContext appCtx = ApplicationContextManager.getAllApplicationContext().get(idAppCtx);
        final Map<String, IExtension> exts = appCtx.getBeansOfType(IExtension.class);
        for (final String name : exts.keySet()) {
            // l'extension à le même id que le contexte
            if (name.equals(idAppCtx)) {
                final IExtension extension = exts.get(name);
                extension.setId(name);
                extension.setExterne(isExterne(name));
                // calcul automatique du path pour les extensions "externes" (dans un répertoire à part ex /extensions)
                if (extension.isExterne()) {
                    extension.setRelativePath(WebAppUtil.getRelativeExtensionPath(name));
                } else {
                    // les extensions "internes" ne sont pas parametrables (desactivables et supprimables)
                    extension.setType(IExtension.TYPE_NON_PARAMETRABLE_AFFICHABLE);
                }
                try {
                    // chargement
                    initExtension(extension);
                    // référencement de l'extension dans la liste des extensions chargées
                    if (isCore(extension) || extension.getEtat() == IExtension.ETAT_ACTIF) {
                        extensions.put(name, extension);
                    }
                } catch (final Exception e) {
                    LOGGER.error("Erreur de chargement de l'extension id=" + extension.getId(), e);
                }
            }
        }
    }

    /**
     * <p>
     * Execution des différents scripts par flyway.
     * Seront exécutés tous les scripts non joués et présents dans les répertoires de configuration
     * </p>
     * <p>l
     * Règles :
     *      <ul>
     *          <li>FLyway ne va pas exécuter les scripts plus ancien que la version déjà en base (i.e. si l'extension est déjà en 1.6 en base et qu'elle passe en 1.7, flyway ne jouera que les scripts > 1.6).</li>
     *          <li>Dans le cas d'une snapshot, Flyway jouera les scripts supérieurs à la version de base de la snapshot, ex : version Snapshot 1.7.0-SNAPSHOST, Flyway ne jouera pas V1_7_0_0__monscript.sql, mais Flyway jouera V1_7_0_1__monscript.sql . Cela signifie que
     *          les scripts d'une version doivent commencer avec le numéro 1 et pas 0 => V1_7_0_1__monscript.sql au lieu de V1_7_0_0__monscript.sql</li>
     *          <li>Flyway ne joue pas les scripts déjà joués (i.e. Il vérfie via un système de checksum si le script est déjà passé ou non).</li>
     *          <li>Flyway ne jouera pas les scripts suivants si un script est en échec</li>
     *          <li>Flyway ne rejoue pas les scripts en échec (même si le checksum change). Il faut supprimer l'entrée en base.</li>
     *      </ul>
     * </p>
     *
     * @param baseLineVersion : version baseline de la base de données.
     */
    private void updateDataBaseWithFlyway(IExtension extension, String baseLineVersion) {
        if(LOGGER.isDebugEnabled()){
            LOGGER.debug(String.format("Exécution des scripts de migration avec flyway pour l'extension %s", extension.getId()));
        }
        Flyway flyway = getExtensionFlywayBean(extension);
        if (flyway != null) {
            try {
                if(StringUtils.isNotBlank(baseLineVersion)) {
                    flyway.setBaselineVersionAsString(baseLineVersion);
                }
                flyway.migrate();
            } catch (FlywayException fwe) {
                LOGGER.error(String.format("Error during flyway migration for extension %s", extension.getId()), fwe);
            }
        }
    }

    /**
     * Récupération du bean Flyway permettant d'effectuer la migration.
     * Chargement du bean par défaut si pas de surcharge du dans l'extension en cours de chargement.
     * @param extension : nom de l'extension
     * @return {@link Flyway}
     */
    private Flyway getExtensionFlywayBean(IExtension extension) {
        Flyway flyway = null;
        if (ApplicationContextManager.containsBean(extension.getId(), "flyway")) {
            //Il existe un bean surchargé par le projet, on le recupère
            flyway = (Flyway) ApplicationContextManager.getBean(extension.getId(), "flyway");
        }
        if (flyway != null) {
            //Le bean est surchargé, on ajoute l'emplacement par défaut des fichiers sql
            List<String> flywayLocations = new ArrayList<>(Arrays.asList(flyway.getLocations()));
            flywayLocations.add("filesystem:" + WebAppUtil.getExtensionSQLPath(extension.getId()));
            flyway.setLocations(flywayLocations.toArray(new String[flywayLocations.size()]));
        } else {
            //Pas de bean spécifique. On clone alors le bean par défaut.
            try {
                Flyway defaultBean = (Flyway) ApplicationContextManager.getCoreContextBean("defaultFlyway");
                flyway = (Flyway) BeanUtils.cloneBean(defaultBean);
                flyway.setTable(defaultBean.getTable() + "_" + extension.getId().toUpperCase());
                flyway.setLocations("filesystem:" + WebAppUtil.getExtensionSQLPath(extension.getId()));
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException | NoSuchMethodException e) {
                LOGGER.error(String.format("Error during copying default flyway configuration for extension %s", extension.getId()), e);
            }
        }
        return flyway;
    }

    private boolean checkVersion(final IExtension extension) {
        boolean versionCompatible = Boolean.TRUE;
        final Version v = new Version(extension.getCoreVersion());
        // si la version du core est bien renseignée
        if (v.isValid()) {
            if (core.equalsMineur(v)) {
                // Différence de numéro de version de maintenance : une alerte indique qu'une version plus récente est disponible
                if (core.greaterMaintenance(v)) {
                    LOGGER.warn("Une version plus récente de l'extension " + extension.getId() + " est disponible");
                }
            }
            // Différence de numéro de version mineure / majeure : l'extension est désactivée
            else {
                LOGGER.error("La version de l'extension " + extension.getId() + " n'est plus compatible avec celle du socle, l'extension a été automatiquement désactivée");
                versionCompatible = Boolean.FALSE;
            }
        } else {
            LOGGER.warn("Impossible de valider la compatibilité de la version de l'extension " + extension.getId());
        }
        return versionCompatible;
    }

    private boolean isExterne(final String name) {
        final File repExtension = new File(WebAppUtil.getAbsoluteExtensionsPath() + name);
        return repExtension.exists();
    }

    private boolean isCore(final IExtension extension) {
        return extension.getId().equals(ApplicationContextManager.DEFAULT_CORE_CONTEXT);
    }

    private void cleanDB() {
        List<ExtensionBean> allExtensions = serviceExtension.getAll();
        for (ExtensionBean extensionBean : allExtensions) {
            if (!ApplicationContextManager.containsBean(extensionBean.getIdBean(), extensionBean.getIdBean())) {
                serviceModule.deleteByExtensionId(extensionBean.getIdExtension());
                serviceExtension.delete(extensionBean.getIdExtension());
            }
        }
    }

    private void initExtension(final IExtension extension) throws Exception {
        ExtensionBean extensionBean = null;
        // chargement en base
        try {
            extensionBean = serviceExtension.getById((long) extension.getId().hashCode());
        } catch (DataSourceException e) {
            LOGGER.debug(String.format("Une erreur est survenue lors de la récupération de l'extension %d", extension.getId().hashCode()), e);
        }
        if (extensionBean == null) {
            // si l'extension n'existe pas en base on crée un enregistrement en executant les scripts SQL d'initialisation
            extensionBean = new ExtensionBean();
            extensionBean.setIdExtension((long) extension.getId().hashCode());
            extensionBean.setEtat(extension.getEtat());
            extensionBean.setType(extension.getType());
            extensionBean.setIdBean(extension.getId());
            extensionBean.setLibelle(StringUtils.isNotEmpty(extension.getLibelle()) ? extension.getLibelle() : extension.getId());
            extensionBean.setVersion(extension.getVersion());
            final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(System.currentTimeMillis());
            // la date de création du core est volontairement ramené à 0h pour être en premier dans la liste
            if (isCore(extension)) {
                calendar.set(Calendar.HOUR, 0);
                calendar.set(Calendar.MINUTE, 0);
                calendar.set(Calendar.SECOND, 0);
            }
            extensionBean.setDateCreation(new Date(calendar.getTimeInMillis()));
            extensionBean.setDateModification(new Date(calendar.getTimeInMillis()));
            updateDataBaseWithFlyway(extension, FLYWAY_DEFAULT_BASELINE);
            serviceExtension.add(extensionBean);
        }
        // sauvegarde de la version précedente
        final String oldVersion = extensionBean.getVersion();
        // check extension version
        boolean versionCompatible = Boolean.TRUE;
        if (isCore(extension)) {
            core = new Version(extension.getVersion());
        } else {
            versionCompatible = checkVersion(extension);
        }
        // restauration ou mise à jour de l'état, du type et/ou version
        if (!versionCompatible || extensionBean.getEtat() != extension.getEtat() || extensionBean.getType() != extension.getType() || !extensionBean.getVersion().equals(extension.getVersion())) {
            if (extensionBean.getEtat() == IExtension.ETAT_A_RESTAURER) {
                extensionBean.setEtat(extension.getEtat());
                extensionBean.setIdBean(extension.getId());
                extensionBean.setLibelle(StringUtils.isNotEmpty(extension.getLibelle()) ? extension.getLibelle() : extension.getId());
            }
            if (versionCompatible) {
                if (!extensionBean.getVersion().equals(extension.getVersion()) || extensionBean.getEtat() == IExtension.ETAT_NON_VALIDE) {

                    extensionBean.setVersion(extension.getVersion());
                    if (extensionBean.getEtat() == IExtension.ETAT_NON_VALIDE) {
                        extensionBean.setEtat(extension.getEtat());
                    }
                }
                if (extensionBean.getType() != extension.getType()) {
                    extensionBean.setType(extension.getType());
                }
            } else {
                extensionBean.setEtat(IExtension.ETAT_NON_VALIDE);
            }
            extensionBean.setDateModification(new Date(System.currentTimeMillis()));
            serviceExtension.update(extensionBean);
        }
        if (versionCompatible) {
            // mise à jour de la base de données (si nouveaux scripts flyway)
            updateDataBaseWithFlyway(extension, StringUtils.substringBefore(oldVersion, "-"));
        }
        // on met à jour l'état en mémoire
        extension.setEtat(extensionBean.getEtat());
        // point d'extension pour configuration automatique
        if (extension.getEtat() != IExtension.ETAT_NON_VALIDE) {
            for (final IExtensionConfig config : ApplicationContextManager.getBeansOfType(extension.getId(), IExtensionConfig.class).values()) {
                config.init(extension, new Version(oldVersion));
            }
        }
    }

    /**
     * Gets the modules.
     *
     * @return the modules
     */
    public Map<String, IExtension> getExtensions() {
        return extensions;
    }

    /**
     * Gets the extension.
     *
     * @param name
     *            the name
     * @return the extension
     */
    public IExtension getExtension(final String name) {
        return extensions.get(StringUtils.defaultIfBlank(name, ApplicationContextManager.DEFAULT_CORE_CONTEXT));
    }

    /**
     * Méthode appelée lors du rafraichissment du contexte (publish / subscribe)
     */
    public void updateContext() {
        ApplicationContextManager.refresh(extensionConfigurer);
        refresh();
    }

    public ExtensionConfigurer getExtensionConfigurer() {
        return extensionConfigurer;
    }

    public void setExtensionConfigurer(final ExtensionConfigurer extensionConfigurer) {
        this.extensionConfigurer = extensionConfigurer;
    }

    private void saveFolder(final IExtension extension) throws IOException {
        try {
            final File rep = new File(WebAppUtil.getAbsolutePath() + extension.getRelativePath());
            final String archiveDir = WebAppUtil.getSauvegardePath() + ExtensionConfigurer.SAVE_PATH;
            FileUtils.forceMkdir( new File(archiveDir) );
            final File archive = new File(archiveDir + extension.getId());
            Zip.compress(rep, archive);
            // suppression du repertoire de l'extension
            FileDeleteStrategy.FORCE.delete(rep);
        } catch (final IOException e) {
            throw new IOException("Impossible de sauvegarder le répertoire de l'extension id=" + extension.getId(), e);
        }
    }

    @Override
    public void handleMessage(final Serializable message) {
        refresh();
    }

    public Cluster getCluster() {
        return cluster;
    }

    public void setCluster(final Cluster cluster) {
        this.cluster = cluster;
    }

    public void setServiceExtension(ServiceExtension serviceExtension) {
        this.serviceExtension = serviceExtension;
    }

    public void setServiceModule(ServiceModule serviceModule) {
        this.serviceModule = serviceModule;
    }

    public void setServiceCorePublisher(final ServiceCorePublisher serviceCorePublisher) {
        this.serviceCorePublisher = serviceCorePublisher;
    }
}
