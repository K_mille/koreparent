package com.kportal.extension;

public interface IExtensionConfig {

    void init(IExtension extension, Version old) throws Exception;

    void clean(IExtension extension) throws Exception;
}
