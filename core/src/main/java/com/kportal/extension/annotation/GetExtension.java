package com.kportal.extension.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.jsbsoft.jtf.core.ApplicationContextManager;

//Annotation accessible à l'execution
@Retention(RetentionPolicy.RUNTIME)
//Annotation associé à un type (Classe, interface)
@Target(ElementType.TYPE)
@Inherited
public @interface GetExtension {

    String getId() default ApplicationContextManager.DEFAULT_CORE_CONTEXT;
}
