package com.kportal.extension.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

public class ExtensionBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = 1192174391041278220L;

    private String idBean = null;

    private String libelle = null;

    private Date dateCreation = null;

    private Date dateModification = null;

    private Integer etat = null;

    private Integer type = null;

    private String tables = null;

    private String description = StringUtils.EMPTY;

    private String auteur = StringUtils.EMPTY;

    private String copyright = StringUtils.EMPTY;

    private String version = StringUtils.EMPTY;

    private String coreVersion = StringUtils.EMPTY;

    private String url = StringUtils.EMPTY;

    private String logo = StringUtils.EMPTY;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAuteur() {
        return auteur;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public String getCopyright() {
        return copyright;
    }

    public void setCopyright(String copyright) {
        this.copyright = copyright;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setIdExtension(Long idExtension) {
        setId(idExtension);
    }

    public void setIdBean(String idBean) {
        this.idBean = idBean;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public void setDateModification(Date dateModification) {
        this.dateModification = dateModification;
    }

    public void setEtat(Integer etat) {
        this.etat = etat;
    }

    public void setTables(String tables) {
        this.tables = tables;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Long getIdExtension() {
        return getId();
    }

    public String getIdBean() {
        return idBean;
    }

    public String getLibelle() {
        return libelle;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public Integer getEtat() {
        return etat;
    }

    public String getTables() {
        return tables;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getCoreVersion() {
        return coreVersion;
    }

    public void setCoreVersion(String coreVersion) {
        this.coreVersion = coreVersion;
    }
}
