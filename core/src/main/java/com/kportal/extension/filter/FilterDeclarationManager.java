package com.kportal.extension.filter;

import java.util.Collection;

import javax.servlet.Filter;

import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.ServiceActivator;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.core.context.ContextLoaderListener;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.filter.ExtensionFilter;

public class FilterDeclarationManager extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "filterDeclarationManager";

    /**
     * {@inheritDoc}
     */
    @Override
    @ServiceActivator(inputChannel = CHANNEL_MODULE)
    @Order(ORDER_CORE + 2)
    public void update() {
        super.update();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        final Collection<ExtensionFilter> filterDeclarations = ClassBeanManager.getInstance().getBeanOfType(ExtensionFilter.class);
        for (final Filter filterDeclaration : filterDeclarations) {
            ContextLoaderListener.ajouterFilter(filterDeclaration);
        }
    }
}
