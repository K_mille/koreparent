package com.kportal.extension.listener;

import java.util.Collection;

import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.ServiceActivator;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.core.context.ContextLoaderListener;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.listener.ExtensionWebListener;

public class ListenerDeclarationManager extends AbstractBeanManager {

    public static final String ID_BEAN = "listenerDeclarationManager";

    @Override
    @ServiceActivator(inputChannel = CHANNEL_MODULE)
    @Order(ORDER_CORE + 2)
    public void update() {
        super.update();
    }

    public void refresh() {
        final Collection<ExtensionWebListener> listenerDeclarations = ClassBeanManager.getInstance().getBeanOfType(ExtensionWebListener.class);
        for (final ExtensionWebListener listenerDeclaration : listenerDeclarations) {
            ContextLoaderListener.ajouterListener(listenerDeclaration);
        }
    }
}