package com.kportal.extension.module;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.MessageEndpoint;

import com.kosmos.publish.ServiceCorePublisher;

/**
 * Gestion des beans produit / projet.
 */
@MessageEndpoint
public abstract class AbstractBeanManager {

    /**
     * Channel Module
     */
    protected static final String CHANNEL_MODULE = "channel.in.core.moduleManager.refresh";

    /**
     * Ordre projet.
     */
    protected static final int ORDER_PROJECT = 1000;

    /**
     * Ordre Extension.
     */
    protected static final int ORDER_EXTENSION = 2000;

    /**
     * Ordre des modules du produit.
     */
    protected static final int ORDER_CORE = 3000;


    private static final Logger LOG = LoggerFactory.getLogger(AbstractBeanManager.class);

    public ModuleManager moduleManager;

    /**
     * Service de publication (pas d'interception AOP des services, publication effectuée de manière programmatique)
     */
    @Autowired
    private ServiceCorePublisher serviceCorePublisher;

    public void init() {
    }

    public ModuleManager getModuleManager() {
        return moduleManager;
    }

    public void setModuleManager(ModuleManager moduleManager) {
        this.moduleManager = moduleManager;
    }

    /**
     * Mise à jour de l'instance du bean via Spring-Integration et l'utilisation de l'annotation {@link org.springframework.integration.annotation.ServiceActivator}.
     *
     * <p>
     *     La méthode implémentée doit être annotée de deux annotations :
     * </p>
     * <ul>
     *     <li>{@literal @}ServiceActivator : définition du channel d'écoute (publish / subscribe). <b>Il est obligatoire de définir l'annotation {@literal @}ServiceActivator(inputChannel = CHANNEL_MODULE) sur chaque méthode update</b></li>
     *     <li>{@literal @}Order (facultatif) : Ordre de traitement du message sur le channel d'écoute.
     *     On distingue trois familles d'ordres, par ordre de priorité (traité dans l'ordre croissant des priorités) :
     *     <ul>
     *         <li>{@link #ORDER_PROJECT} : traitement en premier des messages issus de l'extension projet (ordre par défaut)</li>
     *         <li>{@link #ORDER_EXTENSION} : traitement des messages d'extension (autres que projet)</li>
     *         <li>{@link #ORDER_CORE} : traitement des messages du produit</li>
     *     </ul>
     *     Il est ensuite possible d'ordonner les traitements des messages en diminuant ou augmentant la priorité de manière relative par rapport à ces valeurs.
     *     Exemple :
     *     <pre>
     *         // Traitement du message prioritaire par rapport à l'ordre {@link #ORDER_CORE}
     *        {@literal @}Order(ORDER_CORE - 2)
     *         public void update() {
     *                 //...
     *         }
     *     </pre>
     *     </li>
     *
     * </ul>
     */
    @Order(ORDER_PROJECT)
    public void update() {
        LOG.info("Chargement du bean manager : '{}'", this.getClass());
        refresh();
    }

    /**
     * Rafraîchissement des données liées.
     */
    public abstract void refresh();

    public ServiceCorePublisher getServiceCorePublisher() {
        return serviceCorePublisher;
    }
}
