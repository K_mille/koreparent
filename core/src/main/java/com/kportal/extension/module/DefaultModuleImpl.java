package com.kportal.extension.module;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.kportal.core.autorisation.Permission;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.kportal.util.StatutsMessage;

/**
 * The Class Module.
 */
public class DefaultModuleImpl implements IModule {

    /** The permissions. */
    public List<Permission> permissions = new ArrayList<>();

    /** The id. */
    public String id = "";

    /** The libelle. */
    public String libelle = "";

    /** The description. */
    public String description = "";

    /** The id. */
    public String idExtension = "";

    /** The libelle. */
    public String libelleExtension = "";

    public int etat = IModule.ETAT_ACTIF;

    public int type = IModule.TYPE_NON_PARAMETRABLE_NON_AFFICHABLE;

    public String url = "";

    /**
     * Gets the permissions.
     *
     * @return the permissions
     */
    @Override
    public List<Permission> getPermissions() {
        return permissions;
    }

    /**
     * Sets the permissions.
     *
     * @param permissions
     *            the new permissions
     */
    public void setPermissions(List<Permission> permissions) {
        this.permissions = permissions;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#getIdExtension()
     */
    @Override
    public String getIdExtension() {
        return idExtension;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#setIdExtension(java.lang.String)
     */
    @Override
    public void setIdExtension(String idExtension) {
        this.idExtension = idExtension;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#getLibelleExtension()
     */
    @Override
    public String getLibelleExtension() {
        return libelleExtension;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#setLibelleExtension(java.lang.String)
     */
    @Override
    public void setLibelleExtension(String libelleExtension) {
        this.libelleExtension = libelleExtension;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#getId()
     */
    @Override
    public String getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#setId(java.lang.String)
     */
    @Override
    public void setId(String id) {
        this.id = id;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#getLibelle()
     */
    @Override
    public String getLibelle() {
        return libelle;
    }

    @Override
    public String getLibelleAffichable() {
        return ExtensionHelper.getMessage(getIdExtension(), libelle);
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    /* (non-Javadoc)
     * @see com.kportal.module.IModule#setLibelle(java.lang.String)
     */
    @Override
    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public int getEtat() {
        return etat;
    }

    @Override
    public void setEtat(int etat) {
        this.etat = etat;
    }

    @Override
    public int getType() {
        return type;
    }

    @Override
    public void setType(int type) {
        this.type = type;
    }

    @Override
    public String getMessage(Locale locale, String key) {
        return MessageHelper.getMessage(idExtension, locale, key);
    }

    @Override
    public String getMessage(String key) {
        return MessageHelper.getMessage(idExtension, key);
    }

    @Override
    public String getProperty(String key) {
        return PropertyHelper.getProperty(idExtension, key);
    }

    @Override
    public IExtension getExtension() {
        return ExtensionHelper.getExtension(getIdExtension());
    }

    @Override
    public List<StatutsMessage> getStatuts() {
        return new ArrayList<>();
    }

    @Override
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
