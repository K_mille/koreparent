package com.kportal.extension.module;

import java.util.List;
import java.util.Locale;

import com.kportal.core.autorisation.Permission;
import com.kportal.extension.IExtension;
import com.kportal.util.StatutsMessage;

/**
 * The Interface IComposant.
 */
public interface IModule {

    int ETAT_NON_VALIDE = -1;

    int ETAT_NON_ACTIF = 0;

    int ETAT_ACTIF = 1;

    int ETAT_SURCHARGE = 2;

    int ETAT_A_RESTAURER = -2;

    int TYPE_NON_PARAMETRABLE_NON_AFFICHABLE = -1;

    int TYPE_NON_PARAMETRABLE_AFFICHABLE = 0;

    int TYPE_PARAMETRABLE = 1;

    /**
     * Gets the id extension.
     *
     * @return the id extension
     */
    String getIdExtension();

    IExtension getExtension();

    /**
     * Sets the id extension.
     *
     * @param extension
     *            the new id extension
     */
    void setIdExtension(String extension);

    /**
     * Gets the libelle extension.
     *
     * @return the libelle extension
     */
    String getLibelleExtension();

    /**
     * Sets the libelle extension.
     *
     * @param libelle
     *            the new libelle extension
     */
    void setLibelleExtension(String libelle);

    /**
     * Gets the permissions.
     *
     * @return the permissions
     */
    List<Permission> getPermissions();

    /**
     * Gets the id.
     *
     * @return the id
     */
    String getId();

    /**
     * Sets the id.
     *
     * @param id
     *            the new id
     */
    void setId(String id);

    /**
     * Sets the libelle.
     *
     * @param Libelle
     *            the new libelle
     */
    void setLibelle(String Libelle);

    /**
     * Gets the description.
     *
     * @return the description
     */
    String getDescription();

    /**
     * Gets the libelle.
     *
     * @return the libelle
     */
    String getLibelle();

    String getLibelleAffichable();

    int getEtat();

    void setEtat(int etat);

    int getType();

    void setType(int type);

    /**
     * Gets the message.
     *
     * @param locale
     *            the locale
     * @param key
     *            the key
     * @return the message
     */
    String getMessage(Locale locale, String key);

    String getMessage(String key);

    /**
     * Gets the property.
     *
     * @param key
     *            the key
     * @return the property
     */
    String getProperty(String key);

    List<StatutsMessage> getStatuts();

    String getUrl();
}
