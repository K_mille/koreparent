package com.kportal.extension.module;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.context.BeanUtil;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;

public class ModuleHelper {

    public static ModuleManager getModuleManager() {
        return (ModuleManager) ApplicationContextManager.getCoreContextBean(ModuleManager.ID_BEAN);
    }

    public static IExtension getExtensionModule(IModule module) {
        IExtension result = null;
        if (module != null) {
            result = ExtensionHelper.getExtensionManager().getExtension(module.getIdExtension());
        }
        return result;
    }

    /** retourne un module chargé avec l'id extension et id bean */
    public static IModule getModule(String idExtension, String idBean) {
        return getModuleManager().getModules().get(BeanUtil.getBeanKey(idBean, idExtension));
    }

    /** retourne un module chargé uniquement avec l'id du bean */
    public static IModule getModule(String idBean) {
        // recherche du bean
        for (String key : getModuleManager().getModules().keySet()) {
            if (idBean.equals(BeanUtil.getIdBeanFromKey(key))) {
                return getModuleManager().getModules().get(key);
            }
        }
        return null;
    }

    public static String getRelativePathExtension(IModule module) {
        return (getExtensionModule(module)).getRelativePath();
    }
}
