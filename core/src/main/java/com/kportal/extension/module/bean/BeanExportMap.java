package com.kportal.extension.module.bean;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Surcouche à la map pour que la déserialisation se passe correctement
 *
 * @param <T>
 */
public class BeanExportMap<T extends Serializable> extends HashMap<String, T> {

    /**
     *
     */
    private static final long serialVersionUID = -508886673616992778L;
}
