package com.kportal.extension.module.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;

/**
 * Plugin indexé (wrapper).
 * @author cpoisnel
 *
 */
public class PluginIndexableBean implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -3509563649295065194L;

    /**
     * Type de plugin.
     */
    @JsonIgnore
    private String type;

    public final String getType() {
        return type;
    }

    public final void setType(final String type) {
        this.type = type;
    }

}
