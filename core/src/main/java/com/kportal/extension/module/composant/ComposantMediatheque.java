package com.kportal.extension.module.composant;

import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ComposantMediatheque extends Composant {

    @Override
    public boolean isVisible(AutorisationBean autorisation) {
        return autorisation != null && autorisation.isAdministrateurPhototheque();
    }

    @Override
    public boolean isActionVisible(AutorisationBean autorisation, String code) {
        return autorisation.possedePermission(new PermissionBean("TECH", "pho", "C"));
    }
}
