package com.kportal.extension.module.composant;

import com.univ.objetspartages.om.AutorisationBean;

/**
 * Ce composant est visible uniquement si l'un de ses fils est visible
 *
 * @author olivier.camon
 *
 */
public class ComposantPremierNiveau extends Composant {

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        boolean isVisible = Boolean.FALSE;
        for (final IComposant composant : ComposantHelper.getComposants()) {
            if (this.getId().equals(composant.getIdMenuBoParent())) {
                isVisible = composant.isVisible(autorisation);
                if (isVisible) {
                    break;
                }
            }
        }
        return isVisible;
    }
}
