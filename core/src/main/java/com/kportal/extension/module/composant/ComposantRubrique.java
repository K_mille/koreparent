package com.kportal.extension.module.composant;

import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ComposantRubrique extends Composant {

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        return autorisation != null && autorisation.isAdministrateurRubrique();
    }

    @Override
    public boolean isActionVisible(final AutorisationBean autorisation, final String code) {
        if (!isVisible(autorisation)) {
            return Boolean.FALSE;
        }
        boolean isActionVisible = Boolean.FALSE;
        if (isActionCreation(code)) {
            isActionVisible = autorisation.possedePermission(new PermissionBean("TECH", "rub", "C"));
        } else {
            isActionVisible = autorisation.possedePermission(new PermissionBean("TECH", "rub", "M"));
            if (!isActionVisible) {
                isActionVisible = autorisation.possedePermission(new PermissionBean("TECH", "rub", "S"));
            }
        }
        return isActionVisible;
    }

    private boolean isActionCreation(final String code) {
        return "AJOUTER".equals(code);
    }
}
