package com.kportal.extension.module.composant;

import java.util.Arrays;
import java.util.Collection;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;

public class ComposantUtilisateur extends ComposantAdministration {

    public static final String ID_BEAN = "composantUtilisateur";

    /**
     * La permission permettant de lister les utilisateurs
     */
    public static final String CODE_ACTION_CONSULTATION = "C";

    /**
     * La permission permettant de lister les utilisateurs
     */
    public static final String CODE_PERMISSION = "user";

    /**
     * La permission permettant de gérer les utilisateur (Create Update Delete)
     */
    public static final String CODE_ACTION_GESTION = "G";

    public static final Collection<String> ECRANS_LOGIQUES_CONSULTATION = Arrays.asList("RECHERCHE", "LISTE");

    /**
     * Les actions possible avec les droits de consultation
     */
    public static final Collection<String> ACTIONS_CONSULTATION = Arrays.asList("LISTE", "ACCUEIL", "RECHERCHE", "RECHERCHER");

    public static IModule getModule() {
        return ModuleHelper.getModule(ApplicationContextManager.DEFAULT_CORE_CONTEXT, ID_BEAN);
    }

    /**
     * Retourne la permission de gestion des utilisateurs.
     */
    public static PermissionBean getPermissionGestion() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_GESTION);
    }

    /**
     * Retourne la permission de recherche des utilisateurs.
     */
    public static PermissionBean getPermissionConsultation() {
        return PermissionUtil.getPermissionBean(getModule(), CODE_PERMISSION, CODE_ACTION_CONSULTATION);
    }

    /**
     * En fonction des autorisations et de l'action fourni on retourne vrai si l'utilisateur a les droits en fonction de l'action qu'il entreprend.
     */
    public static boolean isAutoriseParActionProcessusEtEcranLogique(final AutorisationBean autorisations, final String action, final String ecranLogique) {
        boolean isAutorise = autorisations != null && autorisations.isWebMaster();
        if (!isAutorise && autorisations != null) {
            if (ACTIONS_CONSULTATION.contains(action) || ECRANS_LOGIQUES_CONSULTATION.contains(ecranLogique)) {
                isAutorise = autorisations.possedePermission(getPermissionConsultation());
            } else {
                isAutorise = autorisations.possedePermission(getPermissionGestion());
            }
        }
        return isAutorise;
    }

    @Override
    public boolean isVisible(final AutorisationBean autorisation) {
        boolean isVisible = Boolean.FALSE;
        if (isAccessibleBO() && autorisation != null) {
            isVisible = autorisation.possedePermission(getPermissionGestion());
        }
        return isVisible;
    }

    @Override
    public boolean isActionVisible(final AutorisationBean autorisation, final String code) {
        return isVisible(autorisation);
    }
}
