package com.kportal.extension.module.composant;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Bean de représentation d'un menu utilisé dans les jsp
 *
 * @author olivier.camon
 *
 */
public class Menu implements Comparable<Menu>, Serializable {

    private static final long serialVersionUID = -135455518835949748L;

    private String idMenu;

    private String idMenuParent;

    private String code;

    private String url;

    private String libelle;

    private String visuel;

    private Integer ordre;

    private String langue;

    private String type;

    private String codeRubriqueOrigine;

    private String picto;

    private String accroche;

    private boolean menuCourant;

    private List<Menu> sousMenu = new ArrayList<>();

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(final String libelle) {
        this.libelle = libelle;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getVisuel() {
        return visuel;
    }

    public void setVisuel(final String visuel) {
        this.visuel = visuel;
    }

    public Integer getOrdre() {
        return ordre;
    }

    public void setOrdre(final Integer ordre) {
        this.ordre = ordre;
    }

    public List<Menu> getSousMenu() {
        return sousMenu;
    }

    public void setSousMenu(final List<Menu> sousMenu) {
        this.sousMenu = sousMenu;
    }

    public void addSousMenu(final Menu menuAAjouter) {
        this.sousMenu.add(menuAAjouter);
    }

    public void addAllSousMenu(final Collection<Menu> menusAAjouter) {
        this.sousMenu.addAll(menusAAjouter);
    }

    public String getIdMenuParent() {
        return idMenuParent;
    }

    public void setIdMenuParent(final String idMenuParent) {
        this.idMenuParent = idMenuParent;
    }

    public String getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(final String idMenu) {
        this.idMenu = idMenu;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getCodeRubriqueOrigine() {
        return codeRubriqueOrigine;
    }

    public void setCodeRubriqueOrigine(final String codeRubriqueOrigine) {
        this.codeRubriqueOrigine = codeRubriqueOrigine;
    }

    public String getPicto() {
        return picto;
    }

    public void setPicto(final String picto) {
        this.picto = picto;
    }

    public String getAccroche() {
        return accroche;
    }

    public void setAccroche(final String accroche) {
        this.accroche = accroche;
    }

    public boolean isMenuCourant() {
        return menuCourant;
    }

    public void setMenuCourant(final boolean isMenuCourant) {
        this.menuCourant = isMenuCourant;
    }

    @Override
    public int compareTo(final Menu menu) {
        return this.ordre.compareTo(menu.ordre) != 0 ? this.ordre.compareTo(menu.ordre) : this.libelle.compareTo(menu.libelle);
    }
}
