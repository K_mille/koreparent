package com.kportal.extension.module.plugin.objetspartages;

import java.util.Map;

import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;

/**
 * The Interface IPluginContenu.
 */
public interface IPluginSaisieFiche extends IPluginSaisieFicheLegacy {

    /**
     * Traiter pre action. Pre processor au traitement action par defaut
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @throws Exception
     *             the exception
     */
    void preTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;

    /**
     * Traiter action. Pre processor au traitement action par defaut
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @throws Exception
     *             the exception
     */
    void traiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;

    /**
     * Traiter post action. Post processor au traitement action par defaut
     *
     * @param infoBean
     *            the info bean
     * @param ficheUniv
     *            the fiche univ
     * @throws Exception
     *             the exception
     */
    void postTraiterAction(Map<String, Object> infoBean, FicheUniv ficheUniv, MetatagBean meta) throws Exception;
}
