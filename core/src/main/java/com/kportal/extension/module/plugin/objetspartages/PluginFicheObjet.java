package com.kportal.extension.module.plugin.objetspartages;

import java.util.List;

import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheObjet;
import com.univ.objetspartages.om.FicheObjetHelper;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.ContexteDao;

public class PluginFicheObjet extends DefaultPluginFiche {

    /**
     * méthode pour supprimer les objets utilisés par le plugin
     */
    @Override
    public void supprimerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        try (ContexteDao ctx = new ContexteDao()) {
            FicheObjetHelper.deleteAll(ctx, classeObjet, meta.getId());
        }
    }

    /**
     * méthode pour synchroniser les objets utilisés par le plugin
     */
    @Override
    public void synchroniserObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        super.synchroniserObjets(ficheUniv, meta, classeObjet);
        try (ContexteDao ctx = new ContexteDao()) {
            FicheObjetHelper.updateAll(ctx, ficheUniv, classeObjet, meta.getId());
        }
    }

    /**
     * méthode pour créer les objets utilisés par le plugin et leurs métas associés
     */
    @Override
    public void ajouterObjets(final FicheUniv ficheUniv, final MetatagBean meta, final List<ObjetPluginContenu> listeObjet, final String classeObjetCible) throws Exception {
        for (final String classeObjet : super.getListeObjets(classeObjetCible)) {
            for (final ObjetPluginContenu objetPluginContenu : listeObjet) {
                if (objetPluginContenu != null && objetPluginContenu.getClass().getName().equals(classeObjet)) {
                    try (ContexteDao ctx = new ContexteDao()) {
                        objetPluginContenu.setIdMeta(meta.getId());
                        objetPluginContenu.setCtx(ctx);
                        objetPluginContenu.add();
                        if (objetPluginContenu instanceof FicheObjet) {
                            final MetatagBean metatag = MetatagUtils.creerMeta((FicheObjet) objetPluginContenu);
                            metatag.setMetaSourceImport(meta.getMetaSourceImport());
                            serviceMetatag.save(metatag);
                        }
                    }
                }
            }
        }
    }
}
