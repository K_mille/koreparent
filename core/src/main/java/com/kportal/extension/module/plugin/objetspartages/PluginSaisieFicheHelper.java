package com.kportal.extension.module.plugin.objetspartages;

import java.util.Collection;
import java.util.Map;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.module.ModuleHelper;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Metatag;

/**
 * The Class PluginContenuManager.
 */
public class PluginSaisieFicheHelper {

    public static Collection<IPluginSaisieFiche> getPlugins() {
        return ModuleHelper.getModuleManager().getModules(IPluginSaisieFiche.class);
    }

    /**
     * Traiter pre action. Pre processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     */
    public static void preTraiterAction(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginSaisieFiche plugin : getPlugins()) {
            final Map<String, Object> map = infoBean.getValues();
            plugin.preTraiterAction(map, ficheUniv, meta);
            infoBean.setValues(map);
        }
    }

    /**
     * Traiter action. Pre processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     */
    public static void traiterAction(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginSaisieFiche plugin : getPlugins()) {
            final Map<String, Object> map = infoBean.getValues();
            plugin.traiterAction(map, ficheUniv, meta);
            infoBean.setValues(map);
        }
    }

    /**
     * Traiter post action. Post processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     */
    public static void postTraiterAction(final InfoBean infoBean, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        for (final IPluginSaisieFiche plugin : getPlugins()) {
            final Map<String, Object> map = infoBean.getValues();
            plugin.postTraiterAction(map, ficheUniv, meta);
            infoBean.setValues(map);
        }
    }

    //FIXME : rétro-compatibilité

    /**
     * Traiter pre action. Pre processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     * @deprecated Utilisez {@link #preTraiterAction(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void preTraiterAction(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        for (final IPluginSaisieFiche plugin : getPlugins()) {
            final Map<String, Object> map = infoBean.getValues();
            plugin.preTraiterAction(map, ficheUniv, meta);
            infoBean.setValues(map);
        }
    }

    /**
     * Traiter action. Pre processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     * @deprecated Utilisez {@link #traiterAction(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void traiterAction(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        for (final IPluginSaisieFiche plugin : getPlugins()) {
            final Map<String, Object> map = infoBean.getValues();
            plugin.traiterAction(map, ficheUniv, meta);
            infoBean.setValues(map);
        }
    }

    /**
     * Traiter post action. Post processor au traitement action par defaut
     *
     * @param infoBean  the info bean
     * @param ficheUniv the fiche univ
     * @throws Exception the exception
     * @deprecated Utilisez {@link #postTraiterAction(InfoBean, FicheUniv, MetatagBean)}
     */
    @Deprecated
    public static void postTraiterAction(final InfoBean infoBean, final FicheUniv ficheUniv, final Metatag meta) throws Exception {
        for (final IPluginSaisieFiche plugin : getPlugins()) {
            final Map<String, Object> map = infoBean.getValues();
            plugin.postTraiterAction(map, ficheUniv, meta);
            infoBean.setValues(map);
        }
    }
}
