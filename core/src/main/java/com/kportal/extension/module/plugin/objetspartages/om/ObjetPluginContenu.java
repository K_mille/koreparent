package com.kportal.extension.module.plugin.objetspartages.om;

import java.util.Collection;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Interface decrivant un sous-objet. Un sous objet doit être utilisé encapsulé dans un objet k-portal et sera automatiquement mis à jour par le framework en même temps que l'objet
 * principal
 */
public interface ObjetPluginContenu extends Cloneable {

    /**
     * Gets the id fiche parent.
     *
     * @return the id fiche parent
     */
    Long getIdMeta();

    /**
     * Sets the id fiche parent.
     *
     * @param _idMeta
     *            the new id fiche parent
     */
    void setIdMeta(Long _idMeta);

    /**
     * Sets the ctx.
     *
     * @param ctx
     *            the new ctx
     */
    @Deprecated
    void setCtx(OMContext ctx);

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void add() throws Exception;

    /**
     * update .
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void update() throws Exception;

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void delete() throws Exception;

    /**
     * Select.
     *
     * @param s
     *            the s
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    int select(String s) throws Exception;

    /**
     * Retrieve.
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void retrieve() throws Exception;

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    boolean nextItem() throws Exception;

    /**
     * Inits the.
     */
    @Deprecated
    void init();

    /**
     * Gets the id fiche.
     *
     * @return the id fiche
     */
    @JsonIgnore
    Long getIdObjet();

    /**
     * Sets the id objet.
     *
     * @param idObjet
     *            the new id objet
     */
    @JsonIgnore
    void setIdObjet(Long idObjet);

    /**
     * Gets the cle. la cle permet les synchronisations au niveau des imports XML
     *
     * @return the cle
     */
    String getCle();

    /**
     * Renvoyer fiche parente.
     *
     * @return the fiche univ
     * @throws Exception
     *             the exception
     */
    FicheUniv renvoyerFicheParente() throws Exception;

    /**
     * Clone objet.
     *
     * @return the objet plugin contenu
     */
    @Deprecated
    ObjetPluginContenu cloneObjet();

    int selectByMeta(Long idMetatag) throws Exception;

    Collection<SousObjet> getSousObjets();
}
