package com.kportal.extension.module.plugin.objetspartages.om;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.FicheUnivHelper;

public class ObjetPluginContenuHelper {

    /**
     * A partir de l'objet, renvoie la ficheUniv parente
     *
     * @return the fiche univ
     *
     * @throws Exception
     *             the exception
     */
    public static FicheUniv renvoyerFicheParente(final ObjetPluginContenu objet) throws Exception {
        return FicheUnivHelper.getFicheParIdMeta(objet.getIdMeta());
    }
}
