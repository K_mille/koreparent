package com.kportal.extension.module.plugin.objetspartages.om;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsbsoft.jtf.database.OMContext;

public interface SousObjet extends Serializable {

    @JsonIgnore
    Long getIdObjet();

    @JsonIgnore
    void setIdObjet(Long id);

    /**
     * Sets the ctx.
     *
     * @param ctx
     *            the new ctx
     */
    @Deprecated
    void setCtx(OMContext ctx);

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void add() throws Exception;

    /**
     * update .
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void update() throws Exception;

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void delete() throws Exception;

    /**
     * Select.
     *
     * @param s
     *            the s
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    int select(String s) throws Exception;

    /**
     * Retrieve.
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    void retrieve() throws Exception;

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    @Deprecated
    boolean nextItem() throws Exception;

    /**
     * Inits the.
     */
    @Deprecated
    void init();
}
