package com.kportal.extension.module.plugin.rubrique;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface BeanPageAccueil {

    /**
     * Renvoie l'url de la rubrique en fonction de son type
     * @param codeRubrique
     * @param langue
     * @param ampersands
     * @return l'url
     */
    String getUrlRubrique(String codeRubrique, String langue, boolean ampersands) throws Exception;

    /**
     * Renvoie l'url de modification de la cible SI elle existe
     * @param codeRubrique
     * @param langue
     * @param ampersands
     * @return l'url
     */
    String getUrlModification(String codeRubrique, String langue, boolean ampersands);

    @JsonIgnore
    String getLibelleAffichable();
}
