package com.kportal.extension.module.plugin.rubrique;

import java.util.Hashtable;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.rubrique.impl.BeanListeFichePageAccueil;
import com.univ.objetspartages.om.ReferentielObjets;

public class ListeFichePageAccueilRubrique extends DefaultPageAccueilRubrique<BeanListeFichePageAccueil> {

    /**
     *
     */
    private static final long serialVersionUID = -6558789002484644694L;

    @Override
    public void preparerPRINCIPAL(final Map<String, Object> infoBean, final BeanListeFichePageAccueil bean) {
        if (StringUtils.isNotEmpty(bean.getObjet())) {
            infoBean.put("CODE_OBJET_LISTE", bean.getObjet());
        }
        final Hashtable<String, String> table = new Hashtable<>();
        table.put(" ", "");
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            if (!objet.isCollaboratif()) {
                table.put(objet.getNomObjet(), objet.getLibelleObjet());
            }
        }
        infoBean.put("LISTE_OBJET_LISTE", table);
    }

    @Override
    public void traiterPRINCIPAL(final Map<String, Object> infoBean, final BeanListeFichePageAccueil bean) throws ErreurApplicative {
        final String codeObjet = (String) infoBean.get("CODE_OBJET_LISTE");
        if (StringUtils.isEmpty(codeObjet) || ReferentielObjets.instancierFiche(codeObjet) == null) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("RUBRIQUE.PAGE_TETE.ERREUR.OBJET_OBLIGATOIRE"));
        }
        bean.setObjet((String) infoBean.get("CODE_OBJET_LISTE"));
    }
}
