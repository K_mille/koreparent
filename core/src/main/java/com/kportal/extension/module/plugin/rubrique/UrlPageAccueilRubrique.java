package com.kportal.extension.module.plugin.rubrique;

import java.util.Map;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.module.plugin.rubrique.impl.BeanUrlPageAccueil;

public class UrlPageAccueilRubrique extends DefaultPageAccueilRubrique<BeanUrlPageAccueil> {

    /**
     *
     */
    private static final long serialVersionUID = 6741876413479873359L;

    @Override
    public void preparerPRINCIPAL(final Map<String, Object> infoBean, final BeanUrlPageAccueil bean) {
        infoBean.put("URL", bean.getUrl());
    }

    @Override
    public void traiterPRINCIPAL(final Map<String, Object> infoBean, final BeanUrlPageAccueil bean) throws ErreurApplicative {
        if (((String) infoBean.get("URL")).length() == 0) {
            throw new ErreurApplicative(MessageHelper.getCoreMessage("RUBRIQUE.PAGE_TETE.ERREUR.URL_OBLIGATOIRE"));
        }
        bean.setUrl((String) infoBean.get("URL"));
    }
}
