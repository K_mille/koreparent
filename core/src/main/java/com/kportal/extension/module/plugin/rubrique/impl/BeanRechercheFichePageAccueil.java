package com.kportal.extension.module.plugin.rubrique.impl;

import org.apache.commons.lang3.StringUtils;

import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

/**
 * Created on 30/10/15.
 */
public class BeanRechercheFichePageAccueil implements BeanPageAccueil {

    private String objet = StringUtils.EMPTY;

    public String getObjet() {
        return objet;
    }

    public void setObjet(final String objet) {
        this.objet = objet;
    }

    @Override
    public String getUrlRubrique(final String codeRubrique, final String langue, final boolean ampersands) {
        return UnivWebFmt.determinerUrlFormulaire(ContexteUtil.getContexteUniv(), objet, langue, ampersands, codeRubrique);
    }

    @Override
    public String getUrlModification(final String codeRubrique, final String langue, final boolean ampersands) {
        return StringUtils.EMPTY;
    }

    @Override
    public String getLibelleAffichable() {
        return ReferentielObjets.getLibelleObjet(objet);
    }
}

