package com.kportal.extension.module.plugin.rubrique.impl;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.webapp.WebAppUtil;
import com.kportal.extension.module.plugin.rubrique.BeanPageAccueil;
import com.univ.objetspartages.om.ServiceBean;
import com.univ.utils.ContexteUtil;
import com.univ.utils.ServicesUtil;

/**
 * Created on 30/10/15.
 */
public class BeanServicePageAccueil implements BeanPageAccueil {

    private String code = StringUtils.EMPTY;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    @Override
    public String getUrlRubrique(final String codeRubrique, final String langue, final boolean ampersands) throws Exception {
        final ServiceBean service = ServicesUtil.getService(code);
        if (service != null) {
            return ServicesUtil.determinerUrlServiceTypeUrl(ContexteUtil.getContexteUniv(), service);
        }
        return StringUtils.EMPTY;
    }

    @Override
    public String getUrlModification(final String codeRubrique, final String langue, final boolean ampersands) {
        return WebAppUtil.SG_PATH + "?EXT=core&PROC=SAISIE_PREFERENCES&ACTION=MODIFIERPARCODE&CODE_SERVICE=" + code;
    }

    @Override
    public String getLibelleAffichable() {
        String libelle = "Inconnu";
        final ServiceBean service = ServicesUtil.getService(code);
        if (service != null) {
            libelle = service.getIntitule();
        }
        return libelle;
    }
}
