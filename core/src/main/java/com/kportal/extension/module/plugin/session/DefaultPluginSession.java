package com.kportal.extension.module.plugin.session;

import javax.servlet.http.HttpSession;

import com.kportal.extension.module.DefaultModuleImpl;

public class DefaultPluginSession extends DefaultModuleImpl implements IPluginSession {

    @Override
    public void setDonneesSession(HttpSession session) throws Exception {}
}
