package com.kportal.extension.module.plugin.session;

import javax.servlet.http.HttpSession;

import com.kportal.extension.module.IModule;

public interface IPluginSession extends IModule {

    void setDonneesSession(HttpSession session) throws Exception;
}
