package com.kportal.extension.module.plugin.session;

import java.util.Collection;

import javax.servlet.http.HttpSession;

import com.kportal.extension.module.ModuleHelper;

public class PluginSessionHelper {

    public static Collection<IPluginSession> getPlugins() {
        return ModuleHelper.getModuleManager().getModules(IPluginSession.class);
    }

    public static void setDonneesSession(final HttpSession session) throws Exception {
        for (final IPluginSession plugin : getPlugins()) {
            plugin.setDonneesSession(session);
        }
    }
}
