package com.kportal.extension.module.plugin.toolbox;

import java.util.Collection;

import com.kportal.extension.module.IModule;

public interface IPluginTag extends IModule {

    String extraitTagDuContenu(String texte);

    String interpreteTag(String texteAInterpreter) throws Exception;

    String getReferenceTag(String texteAInterpreter);

    String getIdentifiantTag();

    Collection<String> getContexte();

    void setContexteTag(Collection<String> contexteTag);
}
