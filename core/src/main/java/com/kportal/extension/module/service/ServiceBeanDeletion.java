package com.kportal.extension.module.service;

import java.util.Collection;

public interface ServiceBeanDeletion {

    void deleteByRubrique(Collection<String> codesRubrique, final String... params) throws Exception;
}
