package com.kportal.extension.module.service;

import java.io.Serializable;
import java.util.List;

import com.kportal.extension.module.bean.BeanExportMap;

public interface ServiceBeanExport<T extends Serializable> {

    BeanExportMap<T> getBeansByRubrique(List<String> codesRubrique, String idModule, String pathExport, String... params) throws Exception;
}
