package com.kportal.extension.module.service;

import java.io.Serializable;

import com.kportal.extension.module.bean.AbstractBeanExport;

public interface ServiceBeanImport<T extends Serializable> {

    String importBeanExport(AbstractBeanExport<?> beanExport, String pathImport);
}
