package com.kportal.extension.module.service;

import java.util.Collection;

import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kportal.extension.module.IModule;

/**
 * Service Permettant d'exporter/importer/supprimer des contenus modules (fiches, rubriques, formulaire...)
 *
 * @author olivier.camon
 *
 */
public interface ServiceContenuModule {

    /**
     * Méthode d'intialisation du service. Sert principalement à initialiser la liste des modules du service.
     */
    void init();

    /**
     * @return le type du contenu du service
     */
    TypeContenu getTypeContenu();

    /**
     *
     * @return la liste des modules concernés par ce service
     */
    Collection<? extends IModule> getModules();

    /**
     *
     * @return Le service d'export concernant ces modules
     */
    ServiceBeanExport<?> getServiceBeanExport();

    String[] recupereParamsExport(IModule moduleAExporter, Collection<IModule> modulesExporter);

    /**
     *
     * @return Le service d'import concernant ces modules
     */
    ServiceBeanImport<?> getServiceBeanImport();

    /**
     *
     * @return Le service de suppression concernant ces modules
     */
    ServiceBeanDeletion getServiceBeanDeletion();

    /**
     *
     * @return Le service de référence concernant ces modules
     */
    ServiceBeanReference getServiceBeanReference();

    /**
     * A quel type de contenu correspond le module (technique : rubrique; Fiche, Plugin de fiche ...; Autre : module formulaire...)
     *
     * @author olivier.camon
     *
     */
    enum TypeContenu {
        TECHNIQUE, FICHE, AUTRE_CONTENU
    }
}
