package com.kportal.extension.module.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.usinesite.reference.ModuleReference;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;

public class ServiceContenuModuleManager {

    public static final String ID_BEAN = "serviceContenuModuleManager";

    private Collection<ServiceContenuModule> servicesContenus;

    private Collection<ModuleReference> modulesReferences;

    public void refresh() {
        servicesContenus = new ArrayList<>();
        for (final IExtension extension : ExtensionHelper.getExtensionManager().getExtensions().values()) {
            final Map<String, ServiceContenuModule> servicesContenusAMettreAJour = ApplicationContextManager.getBeansOfType(extension.getId(), ServiceContenuModule.class);
            for (final ServiceContenuModule serviceContenu : servicesContenusAMettreAJour.values()) {
                serviceContenu.init();
                servicesContenus.add(serviceContenu);
            }
        }
        modulesReferences = new ArrayList<>();
        for (final IExtension extension : ExtensionHelper.getExtensionManager().getExtensions().values()) {
            final Map<String, ModuleReference> modulesFicheReference = ApplicationContextManager.getBeansOfType(extension.getId(), ModuleReference.class);
            for (final ModuleReference module : modulesFicheReference.values()) {
                module.init();
                modulesReferences.add(module);
            }
        }
    }

    public Collection<ServiceContenuModule> getServiceContenuModules() {
        return servicesContenus;
    }

    public Collection<ModuleReference> getModulesReferences() {
        return modulesReferences;
    }
}
