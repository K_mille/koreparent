package com.kportal.extension.module.service.impl;

import java.util.Collection;

import com.kosmos.usinesite.reference.ServiceBeanReference;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.service.ServiceBeanDeletion;
import com.kportal.extension.module.service.ServiceBeanExport;
import com.kportal.extension.module.service.ServiceBeanImport;
import com.kportal.extension.module.service.ServiceContenuModule;

public class DefaultServiceContenuModule implements ServiceContenuModule {

    protected Collection<? extends IModule> modules;

    private ServiceBeanExport<?> serviceBeanExport;

    private ServiceBeanImport<?> serviceBeanImport;

    private ServiceBeanDeletion serviceBeanDeletion;

    private ServiceBeanReference serviceBeanReference;

    public void setModules(final Collection<IModule> modules) {
        this.modules = modules;
    }

    @Override
    public Collection<? extends IModule> getModules() {
        return modules;
    }

    public void setServiceBeanExport(final ServiceBeanExport<?> serviceBeanExport) {
        this.serviceBeanExport = serviceBeanExport;
    }

    @Override
    public ServiceBeanExport<?> getServiceBeanExport() {
        return serviceBeanExport;
    }

    public void setServiceBeanImport(final ServiceBeanImport<?> serviceBeanImport) {
        this.serviceBeanImport = serviceBeanImport;
    }

    @Override
    public ServiceBeanImport<?> getServiceBeanImport() {
        return serviceBeanImport;
    }

    public void setServiceBeanDeletion(final ServiceBeanDeletion serviceBeanDeletion) {
        this.serviceBeanDeletion = serviceBeanDeletion;
    }

    @Override
    public ServiceBeanDeletion getServiceBeanDeletion() {
        return serviceBeanDeletion;
    }

    public void setServiceBeanReference(final ServiceBeanReference serviceBeanReference) {
        this.serviceBeanReference = serviceBeanReference;
    }

    @Override
    public ServiceBeanReference getServiceBeanReference() {
        return serviceBeanReference;
    }

    @Override
    public void init() {}

    @Override
    public TypeContenu getTypeContenu() {
        return TypeContenu.AUTRE_CONTENU;
    }

    @Override
    public String[] recupereParamsExport(final IModule moduleAExporter, final Collection<IModule> modulesExporter) {
        return new String[] {};
    }
}
