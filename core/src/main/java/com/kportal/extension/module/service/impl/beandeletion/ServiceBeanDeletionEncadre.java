package com.kportal.extension.module.service.impl.beandeletion;

import java.util.ArrayList;
import java.util.Collection;

import com.kportal.extension.module.service.ServiceBeanDeletion;
import com.univ.objetspartages.services.ServiceEncadre;

public class ServiceBeanDeletionEncadre implements ServiceBeanDeletion {

    private ServiceEncadre serviceEncadre;

    public void setServiceEncadre(final ServiceEncadre serviceEncadre) {
        this.serviceEncadre = serviceEncadre;
    }

    @Override
    public void deleteByRubrique(final Collection<String> codesRubrique, final String... params) {
        serviceEncadre.deleteByCodesRubriques(new ArrayList<>(codesRubrique));
    }
}
