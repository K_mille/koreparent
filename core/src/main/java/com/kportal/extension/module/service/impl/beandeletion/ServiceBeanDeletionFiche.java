package com.kportal.extension.module.service.impl.beandeletion;

import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.extension.module.service.ServiceBeanDeletion;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.ContexteDao;

public class ServiceBeanDeletionFiche implements ServiceBeanDeletion {

    private ServiceMetatag serviceMetatag;

    public void setServiceMetatag(final ServiceMetatag serviceMetatag) {
        this.serviceMetatag = serviceMetatag;
    }

    @Override
    public void deleteByRubrique(final Collection<String> codesRubrique, final String... params) throws Exception {
        if (params == null || StringUtils.isEmpty(params[0])) {
            throw new ErreurApplicative("impossible de supprimer les fiches des rubriques sans spécifier le type de fiche à supprimer");
        }
        final List<MetatagBean> metas = serviceMetatag.getMetasForRubriquesAndObjet(codesRubrique, params[0]);
        for (MetatagBean metatag : metas) {
            try (ContexteDao ctxFiche = new ContexteDao()) {
                final FicheUniv fiche = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(metatag.getMetaCodeObjet()));
                fiche.setCtx(ctxFiche);
                fiche.setIdFiche(metatag.getMetaIdFiche());
                fiche.retrieve();
                fiche.delete();
                serviceMetatag.delete(metatag.getId());
            }
        }
    }
}
