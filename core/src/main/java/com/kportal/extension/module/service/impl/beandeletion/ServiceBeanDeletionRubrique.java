package com.kportal.extension.module.service.impl.beandeletion;

import java.util.Collection;

import com.kportal.extension.module.service.ServiceBeanDeletion;
import com.univ.objetspartages.services.ServiceRubrique;

public class ServiceBeanDeletionRubrique implements ServiceBeanDeletion {

    private ServiceRubrique serviceRubrique;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    @Override
    public void deleteByRubrique(final Collection<String> codesRubrique, final String... params) {
        serviceRubrique.deleteByCodes(codesRubrique);
    }
}
