package com.kportal.extension.module.utils;

import java.util.ArrayList;
import java.util.Collection;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.module.IModule;
import com.kportal.extension.module.service.ServiceContenuModule;
import com.kportal.extension.module.service.ServiceContenuModuleManager;

public class ServiceContenuModuleHelper {

    public static ServiceContenuModuleManager getServiceContenuModuleManager() {
        return (ServiceContenuModuleManager) ApplicationContextManager.getCoreContextBean(ServiceContenuModuleManager.ID_BEAN);
    }

    public static Collection<ServiceContenuModule> getServiceContenuModules() {
        return getServiceContenuModuleManager().getServiceContenuModules();
    }

    public static Collection<IModule> getModulesExportables() {
        final Collection<IModule> modulesExportables = new ArrayList<>();
        final Collection<ServiceContenuModule> servicesContenuModule = getServiceContenuModules();
        for (final ServiceContenuModule service : servicesContenuModule) {
            modulesExportables.addAll(service.getModules());
        }
        return modulesExportables;
    }

    public static ServiceContenuModule getServiceContenuModulesParModule(final IModule module) {
        ServiceContenuModule serviceContenu = null;
        final Collection<ServiceContenuModule> serviceContenuModule = getServiceContenuModules();
        for (final ServiceContenuModule service : serviceContenuModule) {
            if (service.getModules().contains(module)) {
                serviceContenu = service;
            }
        }
        return serviceContenu;
    }
}
