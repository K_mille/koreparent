package com.kportal.extension.servlet;

import java.util.Collection;

import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.ServiceActivator;

import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.core.context.ContextLoaderListener;
import com.kportal.extension.module.AbstractBeanManager;
import com.kportal.servlet.ExtensionServlet;

public class ServletDeclarationManager extends AbstractBeanManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "servletDeclarationManager";

    /**
     * {@inheritDoc}
     */
    @Override
    @ServiceActivator(inputChannel = CHANNEL_MODULE)
    @Order(ORDER_CORE + 2)
    public void update() {
        super.update();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        final Collection<ExtensionServlet> servletDeclarations = ClassBeanManager.getInstance().getBeanOfType(ExtensionServlet.class);
        for (final ExtensionServlet servletDeclaration : servletDeclarations) {
            if (servletDeclaration.isActive()) {
                ContextLoaderListener.ajouterServlet(servletDeclaration);
            }
        }
    }
}
