package com.kportal.ihm;

/**
 * Interface permettant par son implémentation d'ajouter des éléments meta dans meta.jsp
 */
public interface HTMLMetaElement {

    /**
     * Retourne le nom de l'attribut metatag.
     * @return le nom de l'attribut metatag
     */
    String getMetaName();

    /**
     * Retourne la valeur à mettre dans l'attribut metatag.
     * @return la valeur du metatag
     */
    String getMetaContent();
}
