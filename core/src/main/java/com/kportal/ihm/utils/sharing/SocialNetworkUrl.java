package com.kportal.ihm.utils.sharing;

import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.ContexteUniv;

public interface SocialNetworkUrl {

    String getSocialNetworkName();

    String getSharingUrl(final FicheUniv fiche, final ContexteUniv ctx);
}
