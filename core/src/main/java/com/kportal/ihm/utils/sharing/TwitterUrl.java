package com.kportal.ihm.utils.sharing;

import java.text.MessageFormat;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.ContexteUniv;
import com.univ.utils.EscapeString;
import com.univ.utils.URLResolver;

public class TwitterUrl extends DefaultSocialNetworkUrl {

    private static final String TWITTER_VIA_PROPERTIES = "twitter.via";

    private static final String TWITTER_MAX_LENGTH_PROPERTIES = "twitter.max.length";

    private static final String TWITTER_URL_WITHOUT_VIA_PROPERTIES = "twitter.url.without.via";

    private static final int MAX_LENGTH_DEFAULT = 50;

    @Override
    public String getSharingUrl(final FicheUniv fiche, final ContexteUniv ctx) {
        String sharingUrl = StringUtils.EMPTY;
        String networkUrl = getSocialNetworkUrl();
        final String via = StringUtils.defaultString(PropertyHelper.getCoreProperty(TWITTER_VIA_PROPERTIES));
        if (StringUtils.isBlank(via)) {
            networkUrl = PropertyHelper.getCoreProperty(TWITTER_URL_WITHOUT_VIA_PROPERTIES);
        }
        if (StringUtils.isNotBlank(networkUrl) && fiche != null) {
            final String urlFiche = URLResolver.getAbsoluteUrlFiche(fiche, ctx);
            final String maxLength = PropertyHelper.getCoreProperty(TWITTER_MAX_LENGTH_PROPERTIES);
            int max = MAX_LENGTH_DEFAULT;
            if (StringUtils.isNotBlank(maxLength) && StringUtils.isNumeric(maxLength)) {
                max = Integer.valueOf(maxLength);
            }
            final String title = EscapeString.escapeURL(StringUtils.abbreviate(fiche.getLibelleAffichable(), max));
            if (StringUtils.isNotBlank(via)) {
                sharingUrl = MessageFormat.format(networkUrl, EscapeString.escapeURL(urlFiche), EscapeString.escapeURL(via), title);
            } else {
                sharingUrl = MessageFormat.format(networkUrl, EscapeString.escapeURL(urlFiche), title);
            }
        }
        return sharingUrl;
    }
}
