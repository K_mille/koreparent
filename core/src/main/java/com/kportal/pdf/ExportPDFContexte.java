package com.kportal.pdf;

import java.io.File;
import java.time.LocalDateTime;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.univ.multisites.InfosSite;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.URLResolver;

/**
 * Représente un contexte d'export PDF d'une fiche.
 */
public class ExportPDFContexte {

    /**
     * Date de l'export.
     */
    private final String date;

    /**
     * La fiche exportée.
     */
    private FicheUniv fiche;

    /**
     * Les propriétés du PDF.
     */
    private final PDFproperties pdfProperties;

    /**
     * Nom de l'objet correspondant à la fiche.
     */
    private String nomObjet;

    /**
     * URL de base de la requête.
     */
    private String urlBase;

    /**
     * URL d'origine de la requête.
     */
    private String urlSource;

    /**
     * Le site d'affichage.
     */
    private InfosSite infosSite;

    /**
     * Le flux HTML servant de base à l'export.
     */
    private String fluxHTML;

    /**
     * Le fichier PDF résultant de l'export.
     */
    private File fichierPDF;

    /**
     * Le fichier HTML résultant de l'export.
     */
    private File fichierHTML;

    /**
     * Constructeur par défaut.
     * @param fiche la fiche à exporter
     * @param pdfProperties les propriétés du PDF
     * @param request la requête de demande d'export
     * @param infosSite le site d'affichage
     */
    public ExportPDFContexte(final FicheUniv fiche, final PDFproperties pdfProperties, final HttpServletRequest request, final InfosSite infosSite) {
        // Calcul du pattern d'affichage de la date en fonction de la langue de la fiche
        final Locale locale = LangueUtil.getLocale(fiche.getLangue());
        String pattern = DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.SHORT, FormatStyle.MEDIUM, IsoChronology.INSTANCE, locale);
        pattern = pattern.replace("yy", "yyyy");
        this.date = LocalDateTime.now().format(DateTimeFormatter.ofPattern(pattern));
        this.fiche = fiche;
        this.pdfProperties = pdfProperties;
        this.nomObjet = ReferentielObjets.getNomObjet(fiche);
        this.urlBase = StringUtils.appendIfMissing(URLResolver.getBasePath(request), "/");
        this.urlSource = request.getRequestURL().toString();
        this.infosSite = infosSite;
    }

    public String getDate() {
        return date;
    }

    public FicheUniv getFiche() {
        return fiche;
    }

    public PDFproperties getPdfProperties() {
        return pdfProperties;
    }

    public String getNomObjet() {
        return nomObjet;
    }

    public String getUrlBase() {
        return urlBase;
    }

    public String getUrlSource() {
        return urlSource;
    }

    public InfosSite getInfosSite() {
        return infosSite;
    }

    public String getFluxHTML() {
        return fluxHTML;
    }

    public void setFluxHTML(final String fluxHTML) {
        this.fluxHTML = fluxHTML;
    }

    public File getFichierPDF() {
        return fichierPDF;
    }

    public void setFichierPDF(final File fichierPDF) {
        this.fichierPDF = fichierPDF;
    }

    public File getFichierHTML() {
        return fichierHTML;
    }

    public void setFichierHTML(final File fichierHTML) {
        this.fichierHTML = fichierHTML;
    }
}
