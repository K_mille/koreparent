package com.kportal.pdf;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.allcolor.yahp.converter.CYaHPConverter;
import org.allcolor.yahp.converter.IHtmlToPdfTransformer;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.pdf.service.ServicePDF;
import com.kportal.servlet.BufferedHttpResponseWrapper;
import com.univ.objetspartages.om.AbstractFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.url.FrontOfficeMgr;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Filter permettant de gérer les impressions des pages en PDF.
 */
public class PDFFilter implements Filter {

    private static final Logger LOG = LoggerFactory.getLogger(PDFFilter.class);

    /**
     * Propriétés du PDF.
     */
    private PDFproperties pdfProperties;

    /**
     * {@inheritDoc}
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        pdfProperties = ApplicationContextManager.getEveryContextBean(PDFproperties.ID_BEAN, PDFproperties.class);
        if (pdfProperties == null) {
            pdfProperties = new PDFproperties();
        }
        initLogs();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        final HttpServletRequest request = (HttpServletRequest) servletRequest;
        if (isRequestForPdf(request)) {
            final HttpServletResponse response = (HttpServletResponse) servletResponse;
            final BufferedHttpResponseWrapper wrapper = new BufferedHttpResponseWrapper(response);
            filterChain.doFilter(servletRequest, wrapper);
            if (wrapper.getStatus() == HttpServletResponse.SC_OK) {
                genererPDF(request, response, wrapper.getOutput());
            } else {
                final OutputStream os = response.getOutputStream();
                os.write(wrapper.getOutput().getBytes());
                os.flush();
                os.close();
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy() {
        // Rien à faire
    }

    /**
     * Indique si la requête concerne une demande d'export PDF.
     * @param request la requête
     * @return true si la requête contient un paramètre indiquant une demande d'export PDF, false sinon
     */
    private boolean isRequestForPdf(final HttpServletRequest request) {
        return "true".equals(request.getParameter(PDFUtils.TO_PDF_PARAMETER));
    }

    /**
     * Génère le PDF dans le flux de la réponse à partir de la requête et du flux HTML d'affichage la fiche.
     * @param request la requête
     * @param response la réponse
     * @param fluxHTMLFiche le flux HTML d'affichage de la fiche
     */
    private void genererPDF(final HttpServletRequest request, final HttpServletResponse response, final String fluxHTMLFiche) throws IOException {
        // Préparation des entêtes
        response.setHeader("Content-Disposition", getContentDispositionHeader(request.getRequestURL().toString()));
        response.setContentType("application/pdf");
        // Chargement de la fiche
        ServicePDF servicePDF = null;
        FicheUniv fiche = null;
        try {
            fiche = FrontOfficeMgr.lireFiche(request);
            if (fiche instanceof AbstractFiche) {
                servicePDF = ServiceManager.getServiceSpecifiqueForBean(ServicePDF.class, ((AbstractFiche) fiche).getPersistenceBean().getClass());
            }
        } catch (Exception e) {
            LOG.error("Erreur lors de la lecture de la fiche", e);
        }
        // Si un export PDF spécifique à la fiche existe, on l'utilise
        if (servicePDF != null && servicePDF.existeExportPDFSpecifiqueFiche(fiche)) {
            LOG.info("Utilisation d'un export PDF spécifique pour la fiche '{}' (id='{}')", fiche.getLibelleAffichable(), fiche.getIdFiche());
            // Chargement du fichier PDF
            final ContexteUniv contexteUniv = ContexteUtil.getContexteUniv();
            ExportPDFContexte exportPDFContexte = new ExportPDFContexte(fiche, pdfProperties, request, contexteUniv.getInfosSite());
            try {
                final Thread t = new Thread(new PDFThread(exportPDFContexte, servicePDF, contexteUniv));
                t.start();
                t.join();
            } catch (final InterruptedException e) {
                LOG.error("Interruption du thread de génération PDF", e);
            }
            // Ecriture du fichier PDF dans la réponse
            try (final OutputStream os = response.getOutputStream()) {
                Files.copy(exportPDFContexte.getFichierPDF().toPath(), os);
            } catch (IOException e) {
                LOG.error("Erreur lors de l'envoi du PDF dans la réponse", e);
            }
        } else {
            // Sinon on utilise l'export basé sur le flux HTML d'affichage
            response.setHeader("Content-Disposition", getContentDispositionHeader(request.getRequestURL().toString()));
            response.setContentType("application/pdf");
            try (OutputStream os = response.getOutputStream()) {
                final String urlWithoutParameter = request.getRequestURL().toString();
                final String title = getTitle(urlWithoutParameter);
                final Thread t = new Thread(new YaHPPDFThread(urlWithoutParameter, fluxHTMLFiche, title, os));
                t.start();
                t.join();
                os.flush();
            } catch (final InterruptedException e) {
                LOG.error("Interruption du thread de génération PDF", e);
            }
        }
    }

    private String getTitle(final String url) {
        String title;
        if (StringUtils.endsWith(url, "/")) {
            title = StringUtils.substringAfterLast(url.substring(0, url.length() - 1), "/");
        } else {
            title = StringUtils.substringAfterLast(url, "/");
            title = StringUtils.substringBeforeLast(title, "-");
        }
        return title;
    }

    /**
     * Retourne l'en-tête "Content-Disposition" pour le fichier PDF en fonction du paramétrage.
     * @param url l'URL de la fiche
     * @return la valeur de l'en-tête "Content-Disposition"
     */
    private String getContentDispositionHeader(final String url) {
        final String typeDisposition = PropertyHelper.getCoreProperty("telechargement.disposition");
        String nomFichier = getNomFichier(url);
        String contentDisposition = "attachment;filename=\"" + nomFichier + ".pdf" + "\"";
        if ("inline".equals(typeDisposition)) {
            contentDisposition = "inline;filename=\"" + nomFichier + ".pdf" + "\"";
        }
        return contentDisposition;
    }

    /**
     * Retourne le nom du fichier à partir de l'URL de la fiche.
     * @param url l'URL de la fiche
     * @return le nom du fichier sans extension
     */
    private String getNomFichier(final String url) {
        String nomFichier;
        if (StringUtils.endsWith(url, "/")) {
            nomFichier = StringUtils.substringAfterLast(url.substring(0, url.length() - 1), "/");
        } else {
            nomFichier = StringUtils.substringAfterLast(url, "/");
            nomFichier = StringUtils.substringBeforeLast(nomFichier, "-");
        }
        return nomFichier;
    }

    /**
     * Initialise la gestion des logs avec le pont JUL - SL4J, permettant de loguer la conversion PDF réalisée par FlyingSaucer.
     * <ul>
     *     <li>Nécessite la présence ce jul-to-sl4j pour être initialisée</li>
     * </ul>
     *
     */
    private void initLogs() {
        try {
            Class bridgeHandler = Class.forName("org.slf4j.bridge.SLF4JBridgeHandler");
            System.setProperty("xr.util-logging.loggingEnabled", "true");
            System.setProperty("xr.util-logging.handlers", "org.slf4j.bridge.SLF4JBridgeHandler");
            LOG.debug("Initialisation de la gestion des logs (pont : {})", bridgeHandler);
        } catch (ClassNotFoundException e) {
            LOG.warn("Impossible d'initialiser la gestion des logs de conversion PDF. Vérifiez que la classe 'org.slf4j.bridge.SLF4JBridgeHandler' est bien chargée",e);
        }
    }

    /**
     * Classe exécutable par un thread permettant de décorréler la génération PDF du thread principal pour éviter les problèmes de libération des objets créés par les librairies sous-jacentes.
     * @see <a href="https://issues.kosmos.fr/browse/CORE-1978">CORE-1978</a>
     */
    private class PDFThread implements Runnable {

        /**
         * Contexte d'export.
         */
        private ExportPDFContexte exportPDFContexte;

        /**
         * Service d'export.
         */
        private ServicePDF servicePDF;

        /**
         * Contexte d'affichage.
         */
        private ContexteUniv contexteUniv;

        /**
         * Constructeur par défaut.
         * @param exportPDFContexte le contexte d'export
         * @param servicePDF le service d'export
         * @param contexteUniv le contexte d'affichage
         */
        public PDFThread(final ExportPDFContexte exportPDFContexte, final ServicePDF servicePDF, final ContexteUniv contexteUniv) {
            this.exportPDFContexte = exportPDFContexte;
            this.servicePDF = servicePDF;
            this.contexteUniv = contexteUniv;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void run() {
            ContexteUtil.ajoutDansThreadLocal(contexteUniv);
            servicePDF.chargerPDF(exportPDFContexte);
        }
    }

    /**
     * Classe exécutable par un thread permettant de décorréler la génération PDF du thread principal pour éviter les problèmes de libération des objets créés par YaHP.
     * @see <a href="https://issues.kosmos.fr/browse/CORE-1978">CORE-1978</a>
     */
    private class YaHPPDFThread implements Runnable {

        /**
         * URL de la fiche.
         */
        String url;

        /**
         * Contenu HTML de la fiche.
         */
        String content;

        /**
         * Titre de la fiche.
         */
        String title;

        /**
         * Flux de sortie.
         */
        OutputStream os;

        /**
         * Constructeur par défaut.
         * @param url l'URL de la fiche
         * @param content le contenu HTML
         * @param title le titre de la fiche
         * @param os le flux de sortie
         */
        public YaHPPDFThread(final String url, final String content, final String title, final OutputStream os) {
            this.url = url;
            this.title = title;
            this.content = content;
            this.os = os;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void run() {
            try {
                final CYaHPConverter converter = new CYaHPConverter();
                // contains configuration properties
                final Map<String, String> properties = new HashMap<>();
                properties.put(IHtmlToPdfTransformer.PDF_RENDERER_CLASS, IHtmlToPdfTransformer.FLYINGSAUCER_PDF_RENDERER);
                // ajout de fonts spécifiques
                if (pdfProperties.getFontsPath().length() > 0) {
                    properties.put(IHtmlToPdfTransformer.FOP_TTF_FONT_PATH, pdfProperties.getFontsPath());
                }
                // add header/footer
                final List<IHtmlToPdfTransformer.CHeaderFooter> headerFooterList = new ArrayList<>();
                if (pdfProperties.getFooterHtml().length() > 0) {
                    headerFooterList.add(new IHtmlToPdfTransformer.CHeaderFooter(format(pdfProperties.getFooterHtml(), title, url), IHtmlToPdfTransformer.CHeaderFooter.FOOTER));
                }
                if (pdfProperties.getHeaderHtml().length() > 0) {
                    headerFooterList.add(new IHtmlToPdfTransformer.CHeaderFooter(format(pdfProperties.getHeaderHtml(), title, url), IHtmlToPdfTransformer.CHeaderFooter.HEADER));
                }
                converter.convertToPdf(Chaine.encodeSpecialEntities(content), pdfProperties.getPageSize(), headerFooterList, url, os, properties);
            } catch (Exception e) {
                LOG.error("impossible de générer le PDF", e);
            }
        }

        private String format(String contenu, final String title, final String url) {
            final SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
            contenu = StringUtils.replace(contenu, PDFproperties.DATE_CONTENT, sdf.format(new Date(System.currentTimeMillis())));
            contenu = StringUtils.replace(contenu, PDFproperties.URL_CONTENT, url);
            contenu = StringUtils.replace(contenu, PDFproperties.TITLE_CONTENT, title);
            return contenu;
        }
    }
}
