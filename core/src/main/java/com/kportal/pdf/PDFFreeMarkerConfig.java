package com.kportal.pdf;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

import com.kportal.pdf.service.ServicePDF;

import freemarker.ext.beans.BeansWrapper;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;

/**
 * Permet la configuration de FreeMarker pour l'export de fiches au format PDF.
 */
@org.springframework.context.annotation.Configuration
public class PDFFreeMarkerConfig {

    /**
     * Version FreeMarker utilisée.
     */
    private static final Version VERSION_FREEMARKER = Configuration.VERSION_2_3_27;

    /**
     * Délai de rafraîchissement du cache des templates.
     * Par défaut : 1 minute.
     */
    @Value("${pdf.template.cache.updateDelayMillis:60000}")
    private int templateUpdateDelayMilliseconds;

    @Bean
    public Configuration pdfFreeMarkerConfiguration() {
        Configuration configuration = new Configuration(VERSION_FREEMARKER);
        configuration.setClassForTemplateLoading(ServicePDF.class, "/" + ServicePDF.TEMPLATES_DIRECTORY);
        configuration.setDefaultEncoding("utf-8");
        configuration.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        configuration.setTemplateUpdateDelayMilliseconds(templateUpdateDelayMilliseconds);
        return configuration;
    }

    /**
     * Retourne une référence vers une classe pour permettre l'appel de méthodes statiques.
     * @param clazz la classe
     * @return une référence qui peut être ajoutée au modèle FreeMarker
     */
    public static TemplateHashModel getReferencePourMethodesStatiques(Class clazz) throws TemplateModelException {
        BeansWrapper wrapper = new BeansWrapperBuilder(VERSION_FREEMARKER).build();
        TemplateHashModel staticModels = wrapper.getStaticModels();
        return (TemplateHashModel) staticModels.get(clazz.getCanonicalName());
    }
}