package com.kportal.pdf;

import org.allcolor.yahp.converter.IHtmlToPdfTransformer;
import org.allcolor.yahp.converter.IHtmlToPdfTransformer.PageSize;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PDFproperties {

    private static final Logger LOG = LoggerFactory.getLogger(PDFproperties.class);

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "pdfProperties";

    public static final String DATE_CONTENT = "<date>";

    public static final String TITLE_CONTENT = "<title>";

    public static final String URL_CONTENT = "<url>";

    public String fontsPath = "";

    public String headerHtml = "";

    public String footerHtml = "<p style=\"text-align:center\">Page <pagenumber></p>";

    public String width = "20.8";

    public String height = "29.6";

    public String lmargin = "1";

    public String rmargin = "1";

    public String bmargin = "1";

    public String tmargin = "1";

    public PageSize pageSize = IHtmlToPdfTransformer.A4P;

    public PDFproperties() {
        super();
    }

    public void init() {
        try {
            pageSize = new PageSize(Double.parseDouble(width + "d"), Double.parseDouble(height + "d"), Double.parseDouble(lmargin + "d"), Double.parseDouble(rmargin + "d"), Double.parseDouble(bmargin + "d"), Double.parseDouble(tmargin + "d"));
        } catch (NumberFormatException e) {
            LOG.debug("unable to parse the given value", e);
        }
    }

    public PageSize getPageSize() {
        return pageSize;
    }

    public void setPageSize(PageSize pageSize) {
        this.pageSize = pageSize;
    }

    public String getFontsPath() {
        return fontsPath;
    }

    public void setFontsPath(String fontPath) {
        this.fontsPath = fontPath;
    }

    public String getHeaderHtml() {
        return headerHtml;
    }

    public void setHeaderHtml(String headerHtml) {
        this.headerHtml = headerHtml;
    }

    public String getFooterHtml() {
        return footerHtml;
    }

    public void setFooterHtml(String footerHtml) {
        this.footerHtml = footerHtml;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getLmargin() {
        return lmargin;
    }

    public void setLmargin(String lmargin) {
        this.lmargin = lmargin;
    }

    public String getRmargin() {
        return rmargin;
    }

    public void setRmargin(String rmargin) {
        this.rmargin = rmargin;
    }

    public String getBmargin() {
        return bmargin;
    }

    public void setBmargin(String bmargin) {
        this.bmargin = bmargin;
    }

    public String getTmargin() {
        return tmargin;
    }

    public void setTmargin(String tmargin) {
        this.tmargin = tmargin;
    }
}
