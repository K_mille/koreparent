package com.kportal.pdf.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.htmlcleaner.CleanerProperties;
import org.htmlcleaner.HtmlCleaner;
import org.htmlcleaner.SimpleXmlSerializer;
import org.htmlcleaner.TagNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.xhtmlrenderer.pdf.ITextRenderer;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.service.Service;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.ihm.utils.FrontUtil;
import com.kportal.pdf.ExportPDFContexte;
import com.kportal.pdf.PDFFreeMarkerConfig;
import com.lowagie.text.DocumentException;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModelException;

/**
 * Service permettant la manipulation des exports de fiche en PDF.
 */
public class ServicePDF implements Service {

    @Autowired
    private ServiceMetatag serviceMetatag;

    /**
     * Emplacement des templates FreeMarker.
     */
    public static final String TEMPLATES_DIRECTORY = "pdf/template/";

    /**
     * Logger.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(ServicePDF.class);

    /**
     * Configuration pour le moteur de template FreeMarker.
     */
    protected Configuration freeMarkerConfiguration;

    /**
     * Durée de vie (Time To Live) du fichier PDF (en secondes).
     */
    private int cacheTTL;

    /**
     * Forçage de la génération du PDF à chaque demande (= mode debug).
     */
    private boolean forcerGeneration;

    /**
     * Constructeur.
     */
    protected ServicePDF() {
        super();
        freeMarkerConfiguration = ApplicationContextManager.getBean(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "pdfFreeMarkerConfiguration", Configuration.class);
    }

    /**
     * Indique si un export PDF spécifique existe pour la fiche passée en paramètre.
     * @param fiche la fiche
     * @return true si un export PDF spécifique existe, false sinon
     */
    public boolean existeExportPDFSpecifiqueFiche(final FicheUniv fiche) {
        // On considère qu'un export PDF est possible si un template existe pour le type de la fiche
        return fiche != null && Thread.currentThread().getContextClassLoader().getResource(getTemplateLocation(fiche)) != null;
    }

    /**
     * Charge le fichier PDF correspondant au contexte d'export. Crée le fichier PDF s'il n'existe pas déjà.
     * @param exportPDFContexte le contexte d'export
     */
    public void chargerPDF(ExportPDFContexte exportPDFContexte) {
        // Calcul du chemin du fichier PDF
        final String alias = exportPDFContexte.getInfosSite().getAlias();
        File fichierPDF = getFichierEditionPDF(exportPDFContexte.getFiche(), alias);
        File fichierHTML = getFichierEditionHTML(exportPDFContexte.getFiche(), alias);
        exportPDFContexte.setFichierPDF(fichierPDF);
        exportPDFContexte.setFichierHTML(fichierHTML);
        // Génération du fichier si nécessaire
        if (estGenerationNecessaire(fichierPDF)) {
            try {
                LOGGER.info("Génération du PDF pour la fiche '{}' vers le fichier '{}'", exportPDFContexte.getFiche().getIdFiche(), exportPDFContexte.getFichierPDF().getAbsolutePath());
                // Chargement du flux HTML
                chargerFluxHTML(exportPDFContexte);
                if (forcerGeneration) {
                    genererFichierHTMLDepuisFluxHTML(exportPDFContexte);
                }
                // Génération du PDF depuis le flux HTML
                genererFichierPDFDepuisFluxHTML(exportPDFContexte);
            } catch (IOException e) {
                LOGGER.error("Erreur lors de la génération du PDF", e);
            }
        }
    }

    /**
     * Supprime le fichier PDF correspondant au métatag passé en paramètre.
     * @param idMetatag l'id de l'entrée métatag
     */
    public void supprimerFichierPDF(final Long idMetatag) {
        final MetatagBean metatag = serviceMetatag.getById(idMetatag);
        if(metatag == null){
            LOGGER.debug("Aucune entrée métatag pour l'id '{}'", idMetatag);
            return;
        }
        File repertoireEdition = getRepertoireEdition(metatag);
        try {
            FileUtils.deleteDirectory(repertoireEdition);
            LOGGER.debug("Suppression du répertoire pour le métatag '{}'", idMetatag);
        } catch (IOException e) {
            LOGGER.error("Erreur lors de la suppression du répertoire pour le métatag '{}'", idMetatag, e);
        }
    }

    /**
     * Ajoute des classes spécifiques à l'objet dans le modèle du template.
     * @param exportPDFContexte le contexte de l'export
     * @param modele le modèle du template
     */
    protected void ajouterDonneesSpecifiques(final ExportPDFContexte exportPDFContexte, final Map<String, Object> modele) {
        // Ne fait rien par défaut, destiné à être surchargé par les services spécifiques à une fiche
    }

    /**
     * Ajoute des classes utilitaires dans le modèle du template pour permettre l'appel de méthodes Java au sein du template.
     *
     * @param modele le modèle du template
     */
    protected void ajouterUtilitairesDansModeleTemplate(Map<String, Object> modele) {
        try {
            modele.put("MessageHelper", PDFFreeMarkerConfig.getReferencePourMethodesStatiques(MessageHelper.class));
        } catch (TemplateModelException e) {
            LOGGER.error("Erreur lors l'ajout de la classe MessageHelper au modèle.", e);
        }
    }

    /**
     * Calcule si la génération du fichier PDF demandé est nécessaire.
     * @param fichierPDF le fichier PDF demandé
     * @return true si la génération est nécessaire, false sinon
     */
    private boolean estGenerationNecessaire(final File fichierPDF) {
        // On génère...
        // Si le fichier n'existe pas
        if (!fichierPDF.exists()) {
            return true;
        }
        // Si le fichier est expiré (i.e. il est plus vieux que la date actuelle moins sa durée de vie)
        if (FileUtils.isFileOlder(fichierPDF, new Date().getTime() - 1000 * cacheTTL)) {
            return true;
        }
        // Si le forçage de la génération est demandé
        if (forcerGeneration) {
            return true;
        }
        return false;
    }

    /**
     * Retourne le fichier PDF correspondant à l'édition de la fiche passée en paramètre.
     *
     * @param fiche la fiche
     * @param codeSite le code du site d'affichage
     * @return le chemin absolu vers le fichier PDF
     */
    private static File getFichierEditionPDF(FicheUniv fiche, final String codeSite) {
        return getFichierEditionPDF(ReferentielObjets.getNomObjet(fiche), fiche.getIdFiche(), codeSite);
    }

    /**
     * Retourne le fichier HTML correspondant à l'édition de la fiche passée en paramètre.
     *
     * @param fiche la fiche
     * @param codeSite le code du site d'affichage
     * @return le chemin absolu vers le fichier HTML
     */
    private static File getFichierEditionHTML(FicheUniv fiche, final String codeSite) {
        String typeFiche = ReferentielObjets.getNomObjet(fiche);
        Long idFiche = fiche.getIdFiche();
        return getFichierEdition(typeFiche, idFiche, codeSite, "html");
    }

    /**
     * Retourne le répertoire correspondant aux éditions de la fiche associée au métatag passé en paramètre.
     * @param metatag le métatag
     * @return le chemin absolu vers le répertoire
     */
    private static File getRepertoireEdition(MetatagBean metatag) {
        return getRepertoireEdition(ReferentielObjets.getNomObjet(metatag.getMetaCodeObjet()), metatag.getMetaIdFiche(), null);
    }

    /**
     * Retourne le fichier PDF à partir des infos passées en paramètre.
     *
     * @param typeFiche le nom de l'objet représenté par la fiche
     * @param idFiche l'identifiant de la fiche
     * @param codeSite le code du site d'affichage
     * @return le chemin absolu vers le fichier PDF
     */
    private static File getFichierEditionPDF(String typeFiche, Long idFiche, final String codeSite) {
        return getFichierEdition(typeFiche, idFiche, codeSite, "pdf");
    }

    /**
     * Retourne le fichier à partir des infos passées en paramètre.
     *
     * @param typeFiche le nom de l'objet représenté par la fiche
     * @param idFiche l'identifiant de la fiche
     * @param codeSite le code du site d'affichage
     * @param extension l'extension souhaitée pour le fichier
     * @return le chemin absolu vers le fichier
     */
    private static File getFichierEdition(String typeFiche, Long idFiche, final String codeSite, String extension) {
        // Le fichier est <repertoire>/<id_fiche>_<code_fiche>.<extension>
        String cheminAbsoluFichier = getRepertoireEdition(typeFiche, idFiche, codeSite) + "/" + idFiche + "_" + codeSite + "." + extension;
        return new File(cheminAbsoluFichier);
    }

    /**
     * Retourne le répertoire contenant l'édition de la fiche pour un site à partir des infos passées en paramètre.
     *
     * @param typeFiche le nom de l'objet représenté par la fiche
     * @param idFiche l'identifiant de la fiche
     * @param codeSite le code du site d'affichage
     * @return le chemin absolu vers le fichier
     */
    private static File getRepertoireEdition(String typeFiche, Long idFiche, final String codeSite) {
        // Le répertoire est /pdf/<type_fiche>/<id_fiche>/<code_site>/
        String cheminAbsoluRepertoire = WebAppUtil.getCheminAbsoluRepertoireFichiersPDF() + "/" + typeFiche + "/" + idFiche;
        if (StringUtils.isNotEmpty(codeSite)) {
            cheminAbsoluRepertoire += "/" + codeSite;
        }
        return new File(cheminAbsoluRepertoire);
    }

    /**
     * Charge le flux HTML correspondant au contexte d'export.
     * @param exportPDFContexte le contexte d'export
     */
    private void chargerFluxHTML(ExportPDFContexte exportPDFContexte) throws IOException {
        // Génération du flux à partir du template
        Map<String, Object> modele = new HashMap<>();
        // La fiche sera accessible dans le template depuis une variable nommé comme l'objet correspondant (ex : formation, article...)
        modele.put(exportPDFContexte.getNomObjet(), exportPDFContexte.getFiche());
        modele.put("properties", exportPDFContexte.getPdfProperties());
        modele.put("date", exportPDFContexte.getDate());
        modele.put("url", exportPDFContexte.getUrlSource());
        modele.put("urlSite", URLResolver.getAbsoluteUrl("/", ContexteUtil.getContexteUniv()));
        modele.put("intituleSite", exportPDFContexte.getInfosSite().getIntitule());
        modele.put("mainColor", FrontUtil.getCouleurPrincipale());
        modele.put("logo", URLResolver.getAbsoluteUrl(FrontUtil.getLogoUrl(), ContexteUtil.getContexteUniv()));
        // Chargement des données spécifiques à un objet
        ajouterDonneesSpecifiques(exportPDFContexte, modele);
        ajouterUtilitairesDansModeleTemplate(modele);
        final String templateName = getTemplateName(exportPDFContexte.getFiche());
        String fluxHTML = StringUtils.EMPTY;
        try {
            fluxHTML = FreeMarkerTemplateUtils.processTemplateIntoString(freeMarkerConfiguration.getTemplate(templateName), modele);
        } catch (IOException | TemplateException e) {
            LOGGER.error("Erreur lors de la génération depuis le template '{}'", templateName, e);
        }
        // Nettoyage du flux HTML
        exportPDFContexte.setFluxHTML(nettoyerFluxHTML(fluxHTML));
    }

    /**
     * Exporte le flux HTML dans un fichier.
     * @param fluxHTML le flux HTML
     * @return le flux HTML nettoyé
     * @throws IOException en cas d'erreur I/O
     */
    private String nettoyerFluxHTML(final String fluxHTML) {
        // Nettoyage du flux HTML pour le rendre compatible XHTML
        HtmlCleaner cleaner = new HtmlCleaner();
        final TagNode tagNode = cleaner.clean(fluxHTML);
        CleanerProperties props = new CleanerProperties();
        props.setTranslateSpecialEntities(true);
        props.setTransResCharsToNCR(true);
        props.setOmitComments(true);
        return new SimpleXmlSerializer(props).getAsString(tagNode, "utf-8");
    }

    /**
     * Génère le fichier PDF à partir du flux HTML pour le contexte d'export passé en paramètre.
     * @param exportPDFContexte le contexte d'export
     * @throws IOException en cas d'erreur I/O
     */
    private void genererFichierPDFDepuisFluxHTML(ExportPDFContexte exportPDFContexte) throws IOException {
        // Conversion HTML -> PDF
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocumentFromString(exportPDFContexte.getFluxHTML(), exportPDFContexte.getUrlBase());
        // Création du fichier PDF
        final File fichierPDF = exportPDFContexte.getFichierPDF();
        fichierPDF.getParentFile().mkdirs();
        if (!fichierPDF.createNewFile()) {
            LOGGER.error("Erreur lors de la création du fichier '{}'", fichierPDF);
        }
        // Génération du PDF
        try (FileOutputStream os = new FileOutputStream(fichierPDF)) {
            renderer.layout();
            renderer.createPDF(os);
        } catch (DocumentException e) {
            LOGGER.error(String.format("Erreur lors de la génération du fichier PDF pour la fiche '%s' de type '%s'", exportPDFContexte.getFiche().getIdFiche(), exportPDFContexte.getNomObjet()), e);
        }
    }

    /**
     * Génère le fichier HTML à partir du flux HTML pour le contexte d'export passé en paramètre. Les CSS externes sont injectées dans le fichier.
     * @param exportPDFContexte le contexte d'export
     * @throws IOException en cas d'erreur I/O
     */
    private void genererFichierHTMLDepuisFluxHTML(ExportPDFContexte exportPDFContexte) throws IOException {
        // Création du fichier HTML
        final File fichierHTML = exportPDFContexte.getFichierHTML();
        fichierHTML.getParentFile().mkdirs();
        if (!fichierHTML.createNewFile()) {
            LOGGER.error("Erreur lors de la création du fichier '{}'", fichierHTML);
        }
        // Génération du fichier HTML
        String htmlAvecCSSInclus = inclureCSSDansHTML(exportPDFContexte.getFluxHTML());
        FileUtils.write(fichierHTML, htmlAvecCSSInclus, Charset.defaultCharset());
    }

    /**
     * Injecte le contenu des sources externes de CSS dans le flux HTML sous forme de balises <style>
     * @param html Le flux HTML à examiner
     * @return Flux HTML incluant les styles externes
     */
    private String inclureCSSDansHTML(String html) {
        HtmlCleaner htmlCleaner = new HtmlCleaner();
        TagNode root = htmlCleaner.clean(html);
        TagNode[] links = root.getElementsByName("link", true);
        for (TagNode link : links) {
            if (link.hasAttribute("href") && link.hasAttribute("rel") && "stylesheet".equals(link.getAttributeByName("rel"))) {
                String cssFilename = WebAppUtil.getAbsolutePath() + link.getAttributeByName("href");
                TagNode style = new TagNode("style");
                style.addAttribute("media", "print");
                try {
                    htmlCleaner.setInnerHtml(style, FileUtils.readFileToString(new File(cssFilename), Charset.defaultCharset()));
                } catch (IOException e) {
                    LOGGER.warn(String.format("Erreur lors de l'inclusion du fichier CSS '%s' dans le flux HTML de débogage.", cssFilename), e);
                }
                TagNode[] heads = root.getElementsByName("head", true);
                for (TagNode head : heads) {
                    head.addChild(style);
                }
                link.removeFromTree();
            }
        }
        return htmlCleaner.getInnerHtml(root);
    }

    /**
     * Retourne le nom du template correspondant à la fiche passée en paramètre.
     * @param fiche la fiche
     * @return le nom du template associé à ce type de fiche
     */
    private String getTemplateName(final FicheUniv fiche) {
        return ReferentielObjets.getNomObjet(fiche) + ".ftl";
    }

    /**
     * Retourne le chemin relatif du template correspondant à la fiche passée en paramètre.
     * @param fiche la fiche
     * @return le chemin relatif du template associé à ce type de fiche, null si pas de template associé
     */
    private String getTemplateLocation(final FicheUniv fiche) {
        return TEMPLATES_DIRECTORY + getTemplateName(fiche);
    }

    public void setCacheTTL(final int cacheTTL) {
        this.cacheTTL = cacheTTL;
    }

    public void setForcerGeneration(final boolean forcerGeneration) {
        this.forcerGeneration = forcerGeneration;
    }
}
