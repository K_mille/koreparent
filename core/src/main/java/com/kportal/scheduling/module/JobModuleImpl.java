package com.kportal.scheduling.module;

import java.util.ArrayList;
import java.util.List;

import org.quartz.JobDetail;

import com.kportal.extension.module.DefaultModuleImpl;

public class JobModuleImpl extends DefaultModuleImpl implements IJobModule {

    public List<JobDetail> jobDetails = new ArrayList<>();

    @Override
    public List<JobDetail> getJobDetails() {
        return jobDetails;
    }

    public void setJobDetails(List<JobDetail> jobDetail) {
        this.jobDetails = jobDetail;
    }
}
