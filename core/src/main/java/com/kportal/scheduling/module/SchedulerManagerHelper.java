package com.kportal.scheduling.module;

import java.io.File;
import java.io.FileFilter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.ClassBeanManager;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.scheduling.bean.JobParameter;
import com.univ.batch.LogReporter;

public class SchedulerManagerHelper {

    private static final String DEFAULT_NAME_TRIGGER = "Trigger";

    private static final String DEFAULT_NAME_JOB = "Sequence";

    public static SchedulerManager getSchedulerManager() {
        return (SchedulerManager) ApplicationContextManager.getCoreContextBean(SchedulerManager.ID_BEAN);
    }

    public static boolean isJobActif(JobKey jobKey) {
        return getSchedulerManager().isJobActif(jobKey);
    }

    public static String getJobNameFromTrigger(String triggerName) {
        return StringUtils.replace(triggerName, DEFAULT_NAME_TRIGGER, DEFAULT_NAME_JOB);
    }

    public static String getTriggerName(Long idSequenceTrigger) {
        return idSequenceTrigger + DEFAULT_NAME_TRIGGER;
    }

    public static IJobModule getModuleForJob(String jobName) {
        final Map<String, IJobModule> modules = getSchedulerManager().getJobModules();
        return modules.get(jobName);
    }

    /**
     * Récupère la liste de tous les jobs
     */
    public static Map<JobKey, JobDetail> getAllJobs() {
        return getSchedulerManager().getJobDetails();
    }

    /**
     * Récupère les détails d'un job donné
     */
    public static JobDetail getJob(final String idJob) {
        final JobKey job = new JobKey(idJob);
        return getAllJobs().get(job);
    }

    /**
     * Récupère la liste des paramètres pour un job donné
     */
    public static List<JobParameter> getParametersForJob(final String idJob) {
        final List<JobParameter> params = new ArrayList<>();
        final JobDetail details = getJob(idJob);
        final JobDataMap jdm = details.getJobDataMap();
        for (final String key : jdm.keySet()) {
            final JobParameter param = new JobParameter(key, jdm.get(key));
            params.add(param);
        }
        return params;
    }

    /**
     * Récupère la liste des valeurs possibles pour un paramètre de job donné.
     * @param jobParameter le paramètre du job
     */
    public static List<String> getValuesForJobParameter(JobParameter jobParameter) {
        List<String> values = Collections.emptyList();
        if (jobParameter != null && jobParameter.getValue() != null) {
            values = Arrays.asList(jobParameter.getValue().toString().split(";"));
        }
        return values;
    }

    /**
     * Indique si un job possède des paramètres qui doivent être renseignés.
     * @param jobName le nom du job
     * @return true si le job possède au moins un paramètre avec plus d'une valeur possible
     */
    public static boolean hasParamsWithMultipleValues(String jobName) {
        return hasParamsWithMultipleValues(getParametersValuesForJob(jobName));
    }

    /**
     * Récupère la liste des valeurs possibles pour chaque paramètre d'un job donné.
     * @param jobName le nom du job
     * @return une map contenant la liste des valeurs possibles pour chaque paramètre identifié par son nom
     */
    private static Map<String, List<String>> getParametersValuesForJob(String jobName) {
        Map<String, List<String>> values = new HashMap<>();
        // Récupération des paramètres du job
        final List<JobParameter> parametersForJob = getParametersForJob(jobName);
        if (CollectionUtils.isNotEmpty(parametersForJob)) {
            for (JobParameter jobParameter : parametersForJob) {
                values.put(jobParameter.getName(), getValuesForJobParameter(jobParameter));
            }
        }
        return values;
    }

    /**
     * Indique si un job possède des paramètres qui doivent être renseignés.
     * @param valuesByParameter la map des valeurs possibles par paramètre
     * @return true si le job possède au moins un paramètre avec plus d'une valeur possible
     */
    private static boolean hasParamsWithMultipleValues(Map<String, List<String>> valuesByParameter) {
        boolean result = false;
        for (List<String> values : valuesByParameter.values()) {
            if (CollectionUtils.isNotEmpty(values) && values.size() > 1) {
                result = true;
                break;
            }
        }
        return result;
    }

    /**
     * Renvoie le nom du fichier de log pour un job donné.
     * @param jobClass la classe du job
     * @return le nom de son fichier de log
     */
    public static String getLogFileName(Class jobClass){
        String logFileName = StringUtils.EMPTY;
        for (final LogReporter reporter : ClassBeanManager.getInstance().getBeanOfType(LogReporter.class)) {
            if(reporter.getClass().isAssignableFrom(jobClass)){
                logFileName = reporter.getLogFileName();
            }
        }
        if(StringUtils.isNotEmpty(logFileName)){
            File dir = new File(WebAppUtil.getLogsPath());
            FileFilter fileFilter = new WildcardFileFilter("*" + logFileName + ".log");
            File[] files = dir.listFiles(fileFilter);
            if(files.length == 1){
                return files[0].getName();
            } else if(files.length > 1){
                return "*" + logFileName + ".log";
            }
        }
        return StringUtils.EMPTY;
    }
}
