package com.kportal.scheduling.monitoring;

import java.util.Map;

import org.quartz.JobDataMap;
import org.quartz.JobKey;
import org.quartz.SchedulerException;

import com.kportal.scheduling.monitoring.exception.BatchMonitoringException;

/**
 * Interface du service permettant de controler et de monitorer les batchs
 *
 * @author aga
 *
 */
public interface BatchMonitoringService {

    /**
     * Le nom de ce bean dans la factory spring
     */
    static final String ID_BEAN = "batchMonitoringService";

    /**
     * Retourne une instance de JobInfos contenant les détails d'un job
     *
     * @param jobName
     *            Le nom du job
     * @return Une instance de JobInfos
     * @throws BatchMonitoringException
     */
    JobInfos getJobInfos(final String jobName) throws BatchMonitoringException;

    /**
     * Retourne une instance de JobInfos contenant les détails d'un job
     *
     * @param jobName
     *            Le nom du job
     * @param groupName
     *            Le nom du groupe dont fait partie le job
     * @return
     * @throws BatchMonitoringException
     */
    JobInfos getJobInfos(final String jobName, final String groupName) throws BatchMonitoringException;

    /**
     * Démarre un job
     *
     * @param jobInfos
     *            Une instance de JobInfos décrivant le job
     * @return Une instance de JobInfos updatée après le démarrage du Job
     * @throws BatchMonitoringException
     */
    JobInfos startJob(JobInfos jobInfos) throws BatchMonitoringException;

    /**
     * Démarre un job
     *
     * @param jobInfos
     *            Une instance de JobInfos décrivant le job
     * @param additionalParameters
     *            Une map<String,Object> contenant les couples nom/valeur des paramètres supplémentaires à passer au job
     * @return Une instance de JobInfos updatée après le démarrage du Job
     * @throws BatchMonitoringException
     */
    JobInfos startJob(JobInfos jobInfos, Map<String, Object> additionalParameters) throws BatchMonitoringException;

    /**
     * Met un job en pause
     *
     * @param jobInfos
     *            Une instance de JobInfos décrivant le job
     * @return Une instance de JobInfos updatée après la mise en pause du Job
     * @throws BatchMonitoringException
     */
    JobInfos pauseJob(JobInfos jobInfos) throws BatchMonitoringException;

    /**
     * Reprend l'éxécution d'un job en pause
     *
     * @param jobInfos
     *            Une instance de JobInfos décrivant le job
     * @return Une instance de JobInfos updatée après la reprise du Job
     * @throws BatchMonitoringException
     */
    JobInfos resumeJob(JobInfos jobInfos) throws BatchMonitoringException;

    /**
     * Retourne vrai si le job s'est terminé depuis la dernière fois
     *
     * @param name
     *            Le nom du job
     * @param group
     *            Le groupe du job
     * @return Un booleen
     */
    boolean isJobEnded(String name, String group);

    /**
     * Job exists.
     *
     * @param jobName
     *            the job name
     * @return true, if successful
     * @throws SchedulerException
     *             the scheduler exception
     */
    public boolean jobExists(final String jobName) throws SchedulerException;

    /**
     * Job exists.
     *
     * @param jobName
     *            the job name
     * @param groupName
     *            the group name
     * @return true, if successful
     * @throws SchedulerException
     *             the scheduler exception
     */
    public boolean jobExists(final String jobName, final String groupName) throws SchedulerException;

    public boolean areJobsRunning() throws SchedulerException;

    public String getFirstRunningJobName() throws SchedulerException;

    public String getJobDescription(String jobName) throws SchedulerException;

    boolean isJobStarted(String jobName, String groupName);

    void startJob(JobKey jobKey, JobDataMap jobDataMap, boolean resumeAll) throws SchedulerException;
}