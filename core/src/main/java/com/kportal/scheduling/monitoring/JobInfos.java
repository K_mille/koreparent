package com.kportal.scheduling.monitoring;

import java.io.Serializable;
import java.util.Date;

/**
 * pojo encapsulant les informations d'un job (batch)
 *
 * @author aga
 *
 */
public class JobInfos implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * Le nom du job
     */
    private String jobName;

    /**
     * Le groupe du job
     */
    private String groupName;

    /**
     * vrai si le job est en cours d'éxécution
     */
    private boolean running;

    /**
     * Date à laquelle le job a été éxécuté pour l'éxécution en cours
     */
    private Date currentFireTime;

    /**
     * Prochaine date d'éxécution du job
     */
    private Date nextFireDate;

    /**
     * Dernière date d'éxécution du job
     */
    private Date lastFireDate;

    /**
     * @return the jobName
     */
    public String getJobName() {
        return jobName;
    }

    /**
     * @param jobName
     *            the jobName to set
     */
    public void setJobName(String jobName) {
        this.jobName = jobName;
    }

    /**
     * @return the groupName
     */
    public String getGroupName() {
        return groupName;
    }

    /**
     * @param groupName
     *            the groupName to set
     */
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    /**
     * @return the running
     */
    public boolean isRunning() {
        return running;
    }

    /**
     * @param running
     *            the running to set
     */
    public void setRunning(boolean running) {
        this.running = running;
    }

    /**
     * @return the currentFireTime
     */
    public Date getCurrentFireTime() {
        return currentFireTime;
    }

    /**
     * @param currentFireTime
     *            the currentFireTime to set
     */
    public void setCurrentFireTime(Date currentFireTime) {
        this.currentFireTime = currentFireTime;
    }

    /**
     * @return the nextFireDate
     */
    public Date getNextFireDate() {
        return nextFireDate;
    }

    /**
     * @param nextFireDate
     *            the nextFireDate to set
     */
    public void setNextFireDate(Date nextFireDate) {
        this.nextFireDate = nextFireDate;
    }

    /**
     * @return the lastFireDate
     */
    public Date getLastFireDate() {
        return lastFireDate;
    }

    /**
     * @param lastFireDate
     *            the lastFireDate to set
     */
    public void setLastFireDate(Date lastFireDate) {
        this.lastFireDate = lastFireDate;
    }
}
