package com.kportal.scheduling.monitoring.impl.quartz;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.listeners.JobListenerSupport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Listener quartz permettant d'observer si un job a été éxécuté par le scheduler.
 * Le statut du job peut être observé via la méthode isJobEnded qui renvoie vrai si le job est terminé
 *
 * @author alexandre.garbe
 *
 */
public class BatchExecutionEndHelper extends JobListenerSupport {

    /**
     * Le nom du listener
     */
    private static final String NAME = "BatchExecutionEndHelper";

    /**
     * Map contenant les statuts des différents jobs observés par le listener
     */
    private final Set<JobKey> jobNamesAndStatus = Collections.synchronizedSet(new HashSet<JobKey>(1));

    /**
     * Le logger de la classe
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(BatchExecutionEndHelper.class);

    /*
     * (non-Javadoc)
     *
     * @see org.quartz.JobListener#getName()
     */
    @Override
    public String getName() {
        return NAME;
    }

    /*
     * (non-Javadoc)
     *
     * @seeorg.quartz.listeners.JobListenerSupport#jobWasExecuted(org.quartz.
     * JobExecutionContext, org.quartz.JobExecutionException)
     */
    @Override
    public void jobWasExecuted(JobExecutionContext context, JobExecutionException jobException) {
        if (jobNamesAndStatus.contains(context.getJobDetail().getKey())) {
            jobNamesAndStatus.remove(context.getJobDetail().getKey());
            LOGGER.debug("Fin du job : " + context.getJobDetail().getKey().getName());
        }
        super.jobWasExecuted(context, jobException);
    }

    /** Permet de nettoyer les jobs **/
    public void jobWasExecuted(JobKey key) {
        if(jobNamesAndStatus.contains(key)){
            jobNamesAndStatus.remove(key);
            LOGGER.debug("Fin du job : " + key);
        }
    }

    /**
     * Retourne vrai si le job s'est terminé depuis la dernière fois
     *
     * @param jobName
     *            Le nom du job
     * @param groupName
     *            Le groupe du job
     * @return Un booleen
     */
    public boolean isJobEnded(String jobName, String groupName) {
        return !(jobNamesAndStatus.contains(new JobKey(jobName, groupName)));
    }

    public void jobToBeExecuted(String jobName, String groupName) {
        jobNamesAndStatus.add(new JobKey(jobName, groupName));
        LOGGER.debug("Execution du job : " + jobName);
    }

    public boolean isJobStarted(String jobName, String groupName) {
        return jobNamesAndStatus.contains(new JobKey(jobName, groupName));
    }

    public boolean isJobRunning() {
        return jobNamesAndStatus.size() > 0;
    }

    public JobKey getJobKeyRunning() {
        for (JobKey key : jobNamesAndStatus) {
            return key;
        }
        return null;
    }
}
