package com.kportal.scheduling.monitoring.impl.quartz;

import org.quartz.JobExecutionContext;
import org.quartz.SchedulerException;
import org.quartz.Trigger;
import org.quartz.Trigger.CompletedExecutionInstruction;
import org.quartz.TriggerListener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BatchTriggerListener implements TriggerListener {

    public static final String NO_RESUME_AFTER_COMPLETE = "noResumeAfterComplete";

    private static final Logger LOGGER = LoggerFactory.getLogger(BatchTriggerListener.class);

    public static final Object LOCK = new Object();

    @Override
    public String getName() {
        return "batchTriggerListener";
    }

    @Override
    public void triggerFired(Trigger trigger, JobExecutionContext context) {
        // nothing
    }

    @Override
    public boolean vetoJobExecution(Trigger trigger, JobExecutionContext context) {
        try {
            synchronized (LOCK) {
                // on met en pause tous les triggers existants
                if (!context.getMergedJobDataMap().getBooleanValue(NO_RESUME_AFTER_COMPLETE)) {
                    context.getScheduler().pauseAll();
                }
                return false;
            }
        } catch (SchedulerException e) {
            LOGGER.error(e.getMessage(), e);
        }
        LOGGER.warn("veto sur l'execution du JOB : impossible mettre en pause les triggers");
        return true;

    }

    @Override
    public void triggerMisfired(Trigger trigger) {
        // nothing
    }

    @Override
    public void triggerComplete(Trigger trigger, JobExecutionContext context, CompletedExecutionInstruction triggerInstructionCode) {
        try {
            synchronized (LOCK) {
                if (!context.getMergedJobDataMap().getBooleanValue(NO_RESUME_AFTER_COMPLETE)) {
                    context.getScheduler().resumeAll();;
                }
            }
        } catch (SchedulerException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }
}
