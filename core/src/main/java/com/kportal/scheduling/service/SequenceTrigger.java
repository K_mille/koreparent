package com.kportal.scheduling.service;

import java.util.Map;

import com.kportal.scheduling.spring.quartz.SequenceJobDetailDecorator;

public interface SequenceTrigger {

    static final int ETAT_NON_ACTIF = 0;

    static final int ETAT_ACTIF = 1;

    static final int TYPE_NON_PARAMETRABLE_NON_AFFICHABLE = -1;

    static final int TYPE_NON_PARAMETRABLE_AFFICHABLE = 0;

    static final int TYPE_PARAMETRABLE_NON_SUPPRIMABLE = 1;

    static final int TYPE_PARAMETRABLE_SUPPRIMABLE = 2;

    public String getCronExpression();

    public Map<String, SequenceJobDetailDecorator> getSequence();

    public Integer getEtat();

    public Integer getType();

    public String getDescription();
}
