package com.kportal.scheduling.service;

import java.util.HashMap;
import java.util.Map;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.extension.ExtensionHelper;

public class SequenceTriggerServiceImpl implements SequenceTriggerService {

    @Override
    public Map<String, SequenceTrigger> getSequenceTriggers() {
        final Map<String, SequenceTrigger> res = new HashMap<>();
        for (final String idCtx : ExtensionHelper.getExtensionManager().getExtensions().keySet()) {
            final Map<String, SequenceTrigger> map = ApplicationContextManager.getBeansOfType(idCtx, SequenceTrigger.class);
            for (final String key : map.keySet()) {
                if (map.get(key).getEtat() == SequenceTrigger.ETAT_ACTIF) {
                    res.put(key, map.get(key));
                }
            }
        }
        return res;
    }

    @Override
    public void init() {
        // TODO Auto-generated method stub
    }
}
