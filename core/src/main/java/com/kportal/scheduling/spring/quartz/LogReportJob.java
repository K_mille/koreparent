package com.kportal.scheduling.spring.quartz;

import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang3.StringUtils;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.PersistJobDataAfterExecution;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.scheduling.module.SchedulerManagerHelper;
import com.univ.batch.LogReport;
import com.univ.batch.LogReporter;

@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public abstract class LogReportJob implements Job, LogReporter {

    private static final Logger LOG = LoggerFactory.getLogger(LogReportJob.class);

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        // initialisation des variables d'éxécution
        init(context);
        LogReport.initLoggerValue(this);
        // indication automatique du début du batch
        Long start = System.currentTimeMillis();
        LOG.info("DEBUT " + this.getClass().getSimpleName());
        // exécution du batch
        perform();
        // indication automatique de la fin du batch et durée
        Long duree = System.currentTimeMillis() - start;
        LOG.info("FIN " + this.getClass().getSimpleName() + ", durée : " + String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(duree), TimeUnit.MILLISECONDS.toMinutes(duree) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(duree)), TimeUnit.MILLISECONDS.toSeconds(duree) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duree))));
        // finalisation du report
        if (isReportByMail()) {
            LogReport.sendReportByMail((ch.qos.logback.classic.Logger) LOG, this);

        }
        LogReport.clearLoggerContext();
    }

    public abstract void perform();

    /**
     * Par défaut on revoit le nom de la classe en minuscule
     */
    @Override
    public String getLogFileName() {
        return this.getClass().getSimpleName().toLowerCase();
    }

    /**
     * Par défaut on revoit vide
     */
    @Override
    public String getLogDescription() {
        final Map<JobKey, JobDetail> jobDetails = SchedulerManagerHelper.getSchedulerManager().getJobDetails();
        for (final JobKey key : jobDetails.keySet()) {
            final JobDetail value = jobDetails.get(key);
            if (value.getJobClass().equals(this.getClass())) {
                return value.getDescription();
            }
        }
        return StringUtils.EMPTY;
    }


    /**
     * Par défaut la méthode init ne fait rien
     */
    public void init(JobExecutionContext context) {}

    @Override
    public String getReportSubject() {
        return StringUtils.EMPTY;
    }

    @Override
    public boolean isReportByMail() {
        return false;
    }
}
