package com.kportal.scheduling.spring.quartz;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.JobListener;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.scheduling.module.SchedulerManagerHelper;
import com.kportal.scheduling.monitoring.BatchMonitoringService;
import com.kportal.scheduling.monitoring.JobInfos;
import com.kportal.scheduling.monitoring.exception.BatchMonitoringException;
import com.kportal.scheduling.monitoring.impl.quartz.BatchExecutionEndHelper;
import com.kportal.scheduling.monitoring.impl.quartz.BatchMonitoringServiceImplQuartz;
import com.kportal.scheduling.monitoring.impl.quartz.BatchTriggerListener;

/**
 * Façade permettant de déclarer un job quartz composé d'une séquence d'autres jobs. Elle destinée à être utilisée en tant que jobClass d'un
 * org.springframework.scheduling.quartz.JobDetailBean
 *
 * Elle est destinée à être déclarée au sein d'un JobDetailBean. Les jobs constituant la séquence doivent être définis sous la forme d'un bean spring de type List<JobDetail> .Le
 * nom de ce bean soit être injecté dans la propriété sequence de l'instance de JobSequence. Si les jobDetails de la sequence sont des instances de BlockingJobDetail il est alors
 * possible de spécifier si une exception durant leur éxécution est bloquante pour la séquence.
 *
 * exemple de définition spring :
 *
 * <bean id="myJobDetail" class="org.springframework.scheduling.quartz.JobDetailBean"> <property name="jobClass" value="com.kportal.scheduling.spring.quartz.JobSequence" />
 * <property name="jobDataAsMap"> <map> <entry key="sequence" value="mySequence" /> </map> </property> </bean>
 *
 * <bean id="mySequence" class="org.springframework.beans.factory.config.ListFactoryBean"> <property name="sourceList"> <list> <ref=idPremierJobDetailBean/>
 * <ref=idSecondJobDetailBean/> </list> </property> </bean>
 *
 * @author aga
 *
 */
@PersistJobDataAfterExecution
@DisallowConcurrentExecution
public class SequenceJob implements Job {

    public static final String PREVIOUS_FIRE_TIME = "previousFireTime";

    public static final String FIRE_TIME = "fireTime";

    public static final String NEXT_FIRE_TIME = "nextFireTime";

    /**
     * Le logger de la classe
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(SequenceJob.class);

    /*
     * (non-Javadoc)
     *
     * @see org.quartz.Job#execute(org.quartz.JobExecutionContext)
     */
    @Override
    public void execute(final JobExecutionContext jobExecutionContext) throws JobExecutionException {
        try {
            final JobDetail sequence = jobExecutionContext.getJobDetail();
            final JobKey sequenceKey = sequence.getKey();
            LOGGER.debug("Execution de la séquence : " + sequenceKey.getName());
            // tri sur la clé (String to Integer) pour l'odre d'execution
            final SortedSet<Integer> keys = new TreeSet<>();
            for (final String key : sequence.getJobDataMap().keySet()) {
                try {
                    keys.add(Integer.parseInt(key));
                } catch (final NumberFormatException e) {
                    LOGGER.error("Impossible de parser la clé du sous job " + ((JobDetail) sequence.getJobDataMap().get(key)).getKey().getName(), e);
                }
            }
            // on récupère la liste de SequenceJobDetailDecorator constituant la sequence
            // pour chaque job de la séquence ...
            for (final Integer key : keys) {
                final SequenceJobDetail jobDetail = (SequenceJobDetail) sequence.getJobDataMap().get(key.toString());
                // les donnees associees au job dans la sequence
                JobDataMap jobDataMap = jobDetail.getSequenceJobDataMap();
                if (jobDataMap == null) {
                    jobDataMap = new JobDataMap();
                }
                // on ajoute les propriétés de date du trigger, utilisables ds les sous jobs
                jobDataMap.put(PREVIOUS_FIRE_TIME, jobExecutionContext.getPreviousFireTime());
                jobDataMap.put(FIRE_TIME, jobExecutionContext.getFireTime());
                jobDataMap.put(NEXT_FIRE_TIME, jobExecutionContext.getNextFireTime());
                jobDataMap.put(BatchTriggerListener.NO_RESUME_AFTER_COMPLETE, true);
                // merge des propriétés, on ajoute les propriétés par defaut du job si elle ne sont pas encore dans celles de la séquence
                if (jobDetail.getJobDataMap() != null) {
                    for (final String keyData : jobDetail.getJobDataMap().keySet()) {
                        if (!jobDataMap.containsKey(keyData)) {
                            jobDataMap.put(keyData, jobDetail.getJobDataMap().get(keyData));
                        }
                    }
                }
                final JobKey jobKey = jobDetail.getKey();
                if (SchedulerManagerHelper.isJobActif(jobKey)) {
                    final boolean blockingJob = jobDetail.isBlocking();
                    try {
                        final BatchMonitoringService batchMonitoringService = (BatchMonitoringService) ApplicationContextManager.getCoreContextBean(BatchMonitoringService.ID_BEAN);
                        batchMonitoringService.startJob(jobKey, jobDataMap, true);
						// Permet de définir si nous avons eu accès à la variable running
						boolean isRunningRenseigne = false;
						// JobInfo init à null car pas toujours récupérable
						JobInfos jobInfos = null;
						// Tentative de récupération du job Info
						// AN: Pas toujours récupérable et dans le cas d'un job en doublon,
						// vu que la clé permettant de le récupérer n'est pas unique nous pouvons récupérer le mauvais.
						try {
							jobInfos = batchMonitoringService.getJobInfos(jobKey.getName(), jobKey.getGroup());
							// Si nous l'avons récupéré, nous vérifions qu'il était à running. Il arrive que nous ne l'avons pas.
							isRunningRenseigne = jobInfos.isRunning();
						} catch (BatchMonitoringException e) {
							LOGGER.warn("Impossible de récupérer le jobInfo", e);
						}
						int cpt = 0;
                        while (!batchMonitoringService.isJobEnded(jobKey.getName(), jobKey.getGroup())) {
                            Thread.sleep(1000);
							cpt++;
							if (cpt >= 60){
								if (nettoyageProcessus(jobKey,jobInfos)){
									LOGGER.warn("Nettoyage des jobs");
									break;
								}
								cpt = 0;
							}
                        }
                    } catch (final SchedulerException | InterruptedException e) {
                        handleException(e, blockingJob);
                    }
                } else {
                    LOGGER.debug("Execution du job" + jobKey.getName() + " désactivée");
                }
            }
        } catch (final SchedulerException e) {
            throw new JobExecutionException(e);
        }
        LOGGER.debug("Fin de l'execution de la séquence : " + jobExecutionContext.getJobDetail().getKey().getName());
    }

	/**
	 *  Permet de définir et nettoyer si les jobs lancés et ceux en mémoire sotn synchro
	 * **/
	private boolean nettoyageProcessus(JobKey jobKey, JobInfos jobInfos) throws SchedulerException {
		boolean retour = Boolean.FALSE;
		Map<String, List<JobExecutionContext>> lstJobRun = new HashMap<>();
		BatchMonitoringService monitor = (BatchMonitoringService) ApplicationContextManager.getCoreContextBean(BatchMonitoringService.ID_BEAN);
		if (monitor instanceof BatchMonitoringServiceImplQuartz){
			for (JobExecutionContext jobExecutionContext : ((BatchMonitoringServiceImplQuartz) monitor).getSchedulerManager().getScheduler().getCurrentlyExecutingJobs()){
				String jobKeyInstance = jobExecutionContext.getJobDetail().getKey().toString();
				List<JobExecutionContext> lst = null;
				if (lstJobRun.get(jobKeyInstance) != null){
					lst = lstJobRun.get(jobKeyInstance);
				}else{
					lst = new ArrayList<>();
				}
				lst.add(jobExecutionContext);
				lstJobRun.put(jobKeyInstance, lst);
			}
			LOGGER.debug("LISTE DES JOBS EN COURS D EXECUTION");
			for (Map.Entry<String, List<JobExecutionContext>> entry : lstJobRun.entrySet()){
				LOGGER.debug("-> {} ; nombre de job: {} ",entry.getKey(),entry.getValue().size());
			}
			if (lstJobRun.get(jobKey.toString())==null){
				retour = Boolean.TRUE;
				for (JobListener listener : ((BatchMonitoringServiceImplQuartz) monitor).getSchedulerManager().getScheduler().getListenerManager().getJobListeners()){
					if (listener instanceof BatchExecutionEndHelper){
						((BatchExecutionEndHelper) listener).jobWasExecuted(jobKey);
					}
				}
			}else if (lstJobRun.get(jobKey.toString()).size()>1){
				LOGGER.warn("le job {} est lancé deux fois", jobKey.toString());
			}
		}
		return retour;
	}

    /**
     * Gestion des exceptions. Si le job n'est pas bloquant on ne fait que logger l'exception. Si le job est blocquant et que l'exception est une RuntimeException ou une
     * JobExecutionException on la rethrow, dans les autres cas on la rethrow wrappée par une JobExecutionException
     *
     * @param t
     *            L'exception
     * @param blockingJob
     *            vrai si le job est bloquant
     * @throws JobExecutionException
     */
    private void handleException(final Throwable t, final boolean blockingJob) throws JobExecutionException {
        // si l'éxécution du job est bloquante en cas d'erreur on
        // rethrow l'exception.
        if (blockingJob) {
            if (t instanceof JobExecutionException) {
                throw (JobExecutionException) t;
            } else if (t instanceof RuntimeException) {
                throw (RuntimeException) t;
            } else {
                throw new JobExecutionException(t);
            }
        } else {
            // si l'éxécution du job n'est pas bloquante en cas d'erreur
            // on se contente de logger l'exception
            LOGGER.error("Non blocking exception: " + t.getMessage(), t);
        }
    }
}
