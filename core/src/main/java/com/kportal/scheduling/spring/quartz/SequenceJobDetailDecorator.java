package com.kportal.scheduling.spring.quartz;

import org.quartz.Job;
import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.JobKey;

/**
 * Décorateur pour la classe JobDetail. Permet d'exposer une facade etendant SequenceJobDetail sans avoir à l'étendre directement et donc de pouvoir déclarer si une erreur durant
 * l'execution du job est bloquante pour la sequence dont il fait partie
 *
 * exemple de déclaration Spring :
 *
 * En référençant un job déjà déclaré :
 *
 * <bean class="com.kportal.scheduling.spring.quartz.SequenceJobDetailDecorator"> <property name="decoratedJobDetail" ref="scanSiteJob"/> <property name="blocking" value="true"/>
 * <property name="type" ref="SEQUENCE_TYPE_NON_PARAMETRABLE_AFFICHABLE"/> </bean>
 *
 */
public class SequenceJobDetailDecorator extends SequenceJobDetail {

    /**
     *
     */
    private static final long serialVersionUID = 3262319515805524374L;

    private Long idJob;

    JobDetail decoratedJobDetail;

    public SequenceJobDetailDecorator() {
        super();
    }

    public static SequenceJobDetailDecorator newInstance(final JobDetail decoratedJobDetail) {
        final SequenceJobDetailDecorator instance = new SequenceJobDetailDecorator();
        instance.setDecoratedJobDetail(decoratedJobDetail);
        return instance;
    }

    public Long getIdJob() {
        return idJob;
    }

    public void setIdJob(final Long idJob) {
        this.idJob = idJob;
    }

    @Override
    public boolean isPersistJobDataAfterExecution() {
        return decoratedJobDetail.isPersistJobDataAfterExecution();
    }

    @Override
    public boolean isConcurrentExectionDisallowed() {
        return decoratedJobDetail.isConcurrentExectionDisallowed();
    }

    @Override
    public JobBuilder getJobBuilder() {
        return decoratedJobDetail.getJobBuilder();
    }

    @Override
    public Object clone() {
        return decoratedJobDetail.clone();
    }

    @Override
    public boolean equals(final Object obj) {
        return decoratedJobDetail.equals(obj);
    }

    @Override
    public String getDescription() {
        return decoratedJobDetail.getDescription();
    }

    @Override
    public String getGroup() {
        return decoratedJobDetail.getKey().getGroup();
    }

    @Override
    public Class<? extends Job> getJobClass() {
        return decoratedJobDetail.getJobClass();
    }

    @Override
    public JobDataMap getJobDataMap() {
        return decoratedJobDetail.getJobDataMap();
    }

    @Override
    public JobKey getKey() {
        return decoratedJobDetail.getKey();
    }

    @Override
    public String getName() {
        return decoratedJobDetail.getKey().getName();
    }

    @Override
    public int hashCode() {
        return decoratedJobDetail.hashCode();
    }

    @Override
    public boolean isDurable() {
        return decoratedJobDetail.isDurable();
    }

    @Override
    public boolean requestsRecovery() {
        return decoratedJobDetail.requestsRecovery();
    }

    @Override
    public String toString() {
        return decoratedJobDetail.toString();
    }

    public JobDetail getDecoratedJobDetail() {
        return decoratedJobDetail;
    }

    public void setDecoratedJobDetail(final JobDetail jobDetail) {
        this.decoratedJobDetail = jobDetail;
    }
}
