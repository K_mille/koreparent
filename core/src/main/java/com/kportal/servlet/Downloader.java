package com.kportal.servlet;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Map;

import javax.activation.MimetypesFileTypeMap;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.kportal.scheduling.util.ScriptsAutomatisesUtil;
import com.kportal.util.compress.Zip;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.utils.SessionUtil;

@WebServlet(name = "downloader", urlPatterns = "/servlet/com.kportal.servlet.Downloader", loadOnStartup = 0)
public class Downloader extends ExtensionServlet {

    /**
     *
     */
    private static final long serialVersionUID = -2400014178654169915L;

    public static final String FILE_PARAM = "FILE";

    public static final String TYPE_PARAM = "TYPE";

    private static final String OCTETSTREAM_MIMETYPE = "application/octet-stream";

    private static final String CONTENT_DISPOSITION_FIELD = "Content-Disposition";

    private static final String DROITS_SUPERVISION = "SUPV";

    private static final String DROITS_SUPERVISION_PARAMETRAGE_APPLICATION = "para";

    private static final String DROITS_SUPERVISION_FICHIERS_JOURNAUX = "ficj";

    private static final String DROITS_SUPERVISION_PARAMETRAGE_APPLICATION_TELECHARGEMENT = "T";

    private static final String DROITS_SUPERVISION_FICHIERS_JOURNAUX_TELECHARGEMENT = "T";

    public static final String TYPE_FICHIER_LOG = "LOG";

    private static final String PROPERTIES_ZIP_OUT = "PROPERTIES_ZIP";

    /**
     * Méthode d'exécution de la servlet.
     * @param req la requête reçue
     * @param resp la réponse renvoyée
     * @throws ServletException Exception dans le traitement de la servlet
     * @throws IOException Exception d'écriture sur la sortie
     */
    public void process(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        // Vérification des rôles
        if (!isAllowed(req)) {
            throw new ServletException(MessageHelper.getCoreMessage("BO_DL_ERR_DROITS_INSUFFISANTS"));
        }
        // Récupération des paramètres obligatoires de requête
        final String fileName = req.getParameter(FILE_PARAM);
        if (StringUtils.isEmpty(fileName) || fileName.contains("/")) {
            throw new ServletException(MessageHelper.getCoreMessage("BO_DL_ERR_NOM_FICHIER_INVALIDE"));
        }
        final File file;
        final String basePath = getBasePath(req.getParameter(TYPE_PARAM));
        if(fileName.contains("*")){
            file = Zip.getZipFile(basePath, fileName);
        }else{
            file = new File(basePath, fileName);
        }
        // On vérifie que le fichier existe bien
        if (file == null || !file.exists()) {
            throw new ServletException(MessageHelper.getCoreMessage("BO_DL_ERR_FICHIER_NON_EXISTANT"));
        }
        final String mimetype = (new MimetypesFileTypeMap()).getContentType(file);
        resp.setContentType(StringUtils.isNotEmpty(mimetype) ? mimetype : OCTETSTREAM_MIMETYPE);
        resp.setContentLength((int) file.length());
        resp.setHeader(CONTENT_DISPOSITION_FIELD, "attachment; filename=\"" + FilenameUtils.getName(file.getName()) + "\"");
        Files.copy(file.toPath(), resp.getOutputStream());
    }

    /**
     * Récupération du chemin de base du fichier en fonction du type de fichier à télécharger.
     * @param typeParameter le paramètre type de la requête
     * @return le chemin de base du fichier
     */
    private final String getBasePath(String typeParameter){
        if(StringUtils.isNotEmpty(typeParameter) && PROPERTIES_ZIP_OUT.equals(typeParameter)){
            return WebAppUtil.getWorkDefaultPath();
        }
        return WebAppUtil.getLogsPath();
    }

    /**
     * Vérifie que l'utilisateur possède les droits pour le téléchargement
     *
     * @param req
     *            Objet requête HTTP reçu
     * @return true si l'utilisateur possède les droits
     */
    private boolean isAllowed(final HttpServletRequest req) throws ServletException {
        boolean isAutorise = false;

        final Map<String, Object> infosSessions = SessionUtil.getInfosSession(req);
        final AutorisationBean autorisation = (AutorisationBean) infosSessions.get(SessionUtilisateur.AUTORISATIONS);
        if (autorisation == null) {
            return isAutorise;
        }
        final String type = req.getParameter(TYPE_PARAM);
        PermissionBean supervisionPersmissionsParametrage = new PermissionBean(DROITS_SUPERVISION, DROITS_SUPERVISION_PARAMETRAGE_APPLICATION, DROITS_SUPERVISION_PARAMETRAGE_APPLICATION_TELECHARGEMENT);
        PermissionBean supervisionPersmissionsJournaux = new PermissionBean(DROITS_SUPERVISION, DROITS_SUPERVISION_FICHIERS_JOURNAUX, DROITS_SUPERVISION_FICHIERS_JOURNAUX_TELECHARGEMENT);
        PermissionBean permisssionTelechargement = ScriptsAutomatisesUtil.getPermissionTelechargement();

        isAutorise = autorisation.possedePermission(supervisionPersmissionsParametrage) && PROPERTIES_ZIP_OUT.equals(type);
        isAutorise |= autorisation.possedePermission(supervisionPersmissionsJournaux) && TYPE_FICHIER_LOG.equals(type);
        isAutorise |= autorisation.possedePermission(permisssionTelechargement) && TYPE_FICHIER_LOG.equals(type);
        return isAutorise;
    }

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) throws ServletException, IOException {
        process(req, resp);
    }

    @Override
    public void init() {}
}
