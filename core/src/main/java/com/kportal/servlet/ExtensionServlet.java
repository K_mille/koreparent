package com.kportal.servlet;

import javax.servlet.http.HttpServlet;

/**
 * Classe a surchargé pour déclarer des Servlets dans les extensions. Toute autre servlet ne sera pas mappé.
 *
 */
public class ExtensionServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    public boolean isActive() {
        return Boolean.TRUE;
    }
}
