package com.kportal.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

@WebServlet(name = "servletRedirectProfil", urlPatterns = "/servlet/com.kportal.servlet.ServletRedirectProfil", loadOnStartup = 0)
public class ServletRedirectProfil extends ExtensionServlet {

    /**
     *
     */
    private static final long serialVersionUID = 294149678142598381L;

    private static final Logger LOG = LoggerFactory.getLogger(ServletRedirectProfil.class);

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) {
        // Initialisation du contexte (infos utilisateur, connexion BdD,...)
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final String urlRedirect = ctx.getInfosSite().getJspFo() + "/error/404.jsp";
        String urlAccueilProfil = "";
        final String codeProfil = request.getParameter("code");
        if (codeProfil != null && ctx.getListeProfilsDsi().contains(codeProfil)) {
            urlAccueilProfil = UnivWebFmt.getUrlAccueilDsi(ctx) + "&amp;PROFIL=" + codeProfil;
        }
        try {
            if (StringUtils.isNotBlank(urlAccueilProfil)) {
                urlAccueilProfil = EscapeString.unescapeHtml(urlAccueilProfil);
                response.sendRedirect(URLResolver.getAbsoluteUrl(urlAccueilProfil, ctx));
            } else {
                getServletContext().getRequestDispatcher(urlRedirect).forward(request, response);
            }
        } catch (final IOException | ServletException e) {
            LOG.error("unable to redirect the user", e);
        }
    }
}
