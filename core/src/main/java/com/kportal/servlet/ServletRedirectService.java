package com.kportal.servlet;

import java.io.IOException;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.objetspartages.om.ServiceBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.ServicesUtil;
import com.univ.utils.URLResolver;

@WebServlet(name = "servletRedirectService", urlPatterns = "/servlet/com.kportal.servlet.ServletRedirectService", loadOnStartup = 0)
public class ServletRedirectService extends ExtensionServlet {

    /**
     *
     */
    private static final long serialVersionUID = 5851363204347573567L;

    private static final Logger LOGGER = LoggerFactory.getLogger(ServletRedirectService.class);

    @Override
    protected void doPost(final HttpServletRequest request, final HttpServletResponse response)  {
        // Initialisation du contexte (infos utilisateur, connexion BdD,...)
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String urlRedirect = ctx.getInfosSite().getJspFo() + "/error/404.jsp";
        String urlService = "";
        final String codeservice = request.getParameter("code");
        ServiceBean service = null;
        if (codeservice != null) {
            for (final ServiceBean s : ctx.calculerListeServicesPush()) {
                if (codeservice.equals(s.getCode())) {
                    service = s;
                    break;
                }
            }
            if (service != null) {
                final StringBuilder sb = new StringBuilder(ServicesUtil.determinerUrlAccueilService(service));
                final Map<String, String[]> parametreRequete = request.getParameterMap();
                for (final Entry<String, String[]> param : parametreRequete.entrySet()) {
                    if (!"code".equals(param.getKey())) {
                        for (final String valeurAttribut : param.getValue()) {
                            sb.append(urlService.contains("?") ? "&" : "?");
                            sb.append(param.getKey()).append("=").append(valeurAttribut);
                        }
                    }
                }
                if (sb.toString().length() > 0) {
                    urlService = sb.toString();
                }
            } else {
                final ServiceBean serviceBean = ServicesUtil.getService(codeservice);
                if (serviceBean != null && StringUtils.isNotBlank(serviceBean.getCode())) {
                    urlRedirect = ctx.getInfosSite().getJspFo() + "/error/403.jsp";
                }
            }
        }
        try {
            if (urlService.length() > 0) {
                response.sendRedirect(URLResolver.getAbsoluteUrl(urlService, ctx));
            } else {
                    getServletContext().getRequestDispatcher(urlRedirect).forward(request, response);
            }
        } catch (final ServletException | IOException e) {
            LOGGER.error("unable to redirect the user to the service URL", e);
        }
    }
}
