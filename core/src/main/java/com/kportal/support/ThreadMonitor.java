package com.kportal.support;

public interface ThreadMonitor {

    String getFullThreadDump();
}
