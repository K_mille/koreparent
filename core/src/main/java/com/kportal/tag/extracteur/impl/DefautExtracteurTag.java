package com.kportal.tag.extracteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.kportal.tag.extracteur.ExtracteurTag;

public class DefautExtracteurTag implements ExtracteurTag {

    @Override
    public String getContenuTagPresentDansTexte(String texte, String baliseOuvrante, String baliseFermante) {
        String contenuTag = StringUtils.substringBetween(texte, baliseOuvrante, baliseFermante);
        if (contenuTag != null) {
            contenuTag = baliseOuvrante + contenuTag + baliseFermante;
        }
        return contenuTag;
    }
}
