package com.kportal.tag.interpreteur;

import java.util.List;

/**
 * Permet à partir d'un texte à interpreter (généralement de la toolbox) d'interpreter un tag et de retourner le contenu HTML
 *
 * @author olivier.camon
 *
 */
public interface InterpreteurTag {

    /**
     * Interprete le tag fourni en paramètre pour retourner généralement du code html à afficher en FO
     *
     * @param texteAInterpreter
     *            le contenu du tag à interpreter
     * @param baliseOuvrante
     *            la balise de début du tag
     * @param baliseFermante
     *            la balise de fin du tag
     * @return le tag interpreté
     * @throws Exception
     *             hu...
     */
    String interpreterTag(String texteAInterpreter, String baliseOuvrante, String baliseFermante) throws Exception;

    /**
     * Permet pour certains tag (liens de fiches par exemple) de calculer si des pages sont référencés dans d'autre page
     *
     * @param texteAInterpreter
     *            le contenu du tag
     * @param baliseOuvrante
     *            la balise de début du tag
     * @param baliseFermante
     *            la balise de fin du tag
     * @return la / les références du tags
     */
    String getReferenceTag(String texteAInterpreter, String baliseOuvrante, String baliseFermante);

    /**
     * Permet de connaitre le contexte dans lequel le tag doit être utilisé.
     *
     * @return la liste des cas dans lequel il doit être utilisé.
     */
    List<String> getTypesInterpreteur();
}
