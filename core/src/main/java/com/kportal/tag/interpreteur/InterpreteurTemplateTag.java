package com.kportal.tag.interpreteur;

import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspWriter;

import com.kportal.extension.module.plugin.toolbox.IPluginTag;

public interface InterpreteurTemplateTag {

    String getPathJsp();

    void setPathJsp(String pathJsp);

    IPluginTag getPluginTag();

    void setPluginTag(IPluginTag pluginTag);

    String getOutputJsp(JspWriter out, ServletContext context, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
}
