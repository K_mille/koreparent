package com.kportal.tag.interpreteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;

public class InterpreteurInsertionFormulaire extends AbstractInterpreteurTag {

    @Override
    public String interpreterTag(String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        String urlDuLien = StringUtils.EMPTY;
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        texteAInterpreter = supprimerBalisesDuTexte(texteAInterpreter, baliseOuvrante, baliseFermante);
        urlDuLien = URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFormulaire(ctx, texteAInterpreter, ctx.getLangue(), Boolean.TRUE, ctx.getCodeRubriquePageCourante()), ctx);
        return urlDuLien;
    }

    @Override
    public String getReferenceTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        return StringUtils.EMPTY;
    }
}
