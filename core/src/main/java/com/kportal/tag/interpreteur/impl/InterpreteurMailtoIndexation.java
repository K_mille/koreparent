package com.kportal.tag.interpreteur.impl;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.RequeteUtil;

public class InterpreteurMailtoIndexation extends InterpreteurMailto {

    @Override
    public String interpreterTag(final String texteAInterpreter, final String baliseOuvrante, final String baliseFermante) {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        String contenuTagMailto = StringUtils.substringBetween(texteAInterpreter, baliseInterneOuvrante, baliseInterneFermante);
        contenuTagMailto = StringUtils.defaultString(contenuTagMailto);
        String texteInterprete = StringUtils.EMPTY;
        // nouveau tag de la forme [mailto]email=annuaire;549805613&subject=[...]&body=[...][/mailto]
        if (contenuTagMailto.startsWith("email=")) {
            final String mail = RequeteUtil.renvoyerParametre(contenuTagMailto, "email");
            if (mail.contains(";")) {
                texteInterprete = getAdresseMailFromAnnuaire(ctx, mail);
            }
        } else {
            texteInterprete = getAdresseMailFromAnnuaire(ctx, contenuTagMailto);
        }
        if (StringUtils.isNotBlank(texteInterprete)) {
            texteInterprete = StringUtils.replace(texteAInterpreter, baliseInterneOuvrante + contenuTagMailto + baliseInterneFermante, texteInterprete);
        }
        return texteInterprete;
    }
}
