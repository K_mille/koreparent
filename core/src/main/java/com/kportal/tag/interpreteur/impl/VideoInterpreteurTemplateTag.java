package com.kportal.tag.interpreteur.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.services.ServiceMedia;
import com.univ.objetspartages.util.MediaUtils;

public class VideoInterpreteurTemplateTag extends DefaultInterpreteurTemplateTag {

    private static Pattern IMAGEID_PATTERN = Pattern.compile("\\[id-image]([0-9]+)\\[/id-image]");

    private ServiceMedia serviceMedia;

    public void setServiceMedia(final ServiceMedia serviceMedia) {
        this.serviceMedia = serviceMedia;
    }

    @Override
    public String interpreterTag(String texteAInterpreter, String baliseOuvrante, String baliseFermante) throws Exception {
        String decoratedText = texteAInterpreter;
        if(!decoratedText.contains("IMGURL")) {
            final Matcher idMatcher = IMAGEID_PATTERN.matcher(decoratedText);
            if(idMatcher.find()) {
                final String id = idMatcher.group(1);
                final Long mediaId = Long.parseLong(id);
                final MediaBean mediaBean = serviceMedia.getById(mediaId);
                decoratedText = decoratedText.replace("URL", "IMGURL=" + MediaUtils.getUrlVignetteAbsolue(mediaBean) + "#URL");
            }
        }
        return super.interpreterTag(decoratedText, baliseOuvrante, baliseFermante);
    }
}
