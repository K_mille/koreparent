package com.kportal.util;

public class StatutsMessage {

    public final static String ERROR = "error";

    public final static String WARNING = "warning";

    public final static String SUCCESS = "success";

    public final static String NO_STATE = "nostate";

    private String type = NO_STATE;

    private String titre;

    private String message = "";

    private String etat;

    private String module = "";

    private String lien = "";

    private String texteLien = "Cliquez ici pour résoudre le problème";

    public String getType() {
        return type;
    }

    public StatutsMessage setType(String type) {
        this.type = type;
        return this;
    }

    public String getTitre() {
        return titre;
    }

    public StatutsMessage setTitre(String titre) {
        this.titre = titre;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public StatutsMessage setMessage(String message) {
        this.message = message;
        return this;
    }

    public String getEtat() {
        return etat;
    }

    public StatutsMessage setEtat(String etat) {
        this.etat = etat;
        return this;
    }

    public String getModule() {
        return module;
    }

    public StatutsMessage setModule(String module) {
        this.module = module;
        return this;
    }

    public String getLien() {
        return lien;
    }

    public StatutsMessage setLien(String lien) {
        this.lien = lien;
        return this;
    }

    public String getTexteLien() {
        return texteLien;
    }

    public StatutsMessage setTexteLien(String texteLien) {
        this.texteLien = texteLien;
        return this;
    }
}
