package com.univ.autocomplete.bean;

import java.io.Serializable;

public class AutoCompleteSuggestion implements Serializable {

    private static final long serialVersionUID = 1L;

    private String value;

    private boolean forced = false;

    private Object data;

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public boolean isForced() {
        return forced;
    }

    public void setForced(final boolean forced) {
        this.forced = forced;
    }
}
