package com.univ.autocomplete.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize()
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class AutoCompletionObject {

    private String query;

    private List<AutoCompleteSuggestion> suggestions;

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public List<AutoCompleteSuggestion> getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(List<AutoCompleteSuggestion> suggestions) {
        this.suggestions = suggestions;
    }
}
