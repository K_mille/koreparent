package com.univ.autocomplete.processus;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.core.config.MessageHelper;
import com.univ.autocomplete.bean.AutoCompleteSuggestion;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteUtil;
import com.univ.utils.SessionUtil;

public class UserAutoCompletion extends GestionAutoCompletion {

    public static final String ID_BEAN = "userAutoComplete";

    private static final Logger LOG = LoggerFactory.getLogger(UserAutoCompletion.class);

    private ServiceUser serviceUser;

    public void setServiceUser(final ServiceUser serviceUser) {
        this.serviceUser = serviceUser;
    }

    /**
     * A partir de l'objet FicheUniv, on retrouve l'ensemble des fiches et on les map dans une liste de résultat exploitable
     * @return
     */
    private List<AutoCompleteSuggestion> getResults(final AutorisationBean autorisations, final List<UtilisateurBean> users) {
        final List<AutoCompleteSuggestion> resultats = new ArrayList<>();
        final UtilisateurBean me = serviceUser.getByCode(ContexteUtil.getContexteUniv().getCode());
        String meCode = StringUtils.EMPTY;
        // Rajout de l'utilisateur courant pour toutes les suggestions
        if(me != null) {
            meCode = me.getCode();
            final AutoCompleteSuggestion meAuto = buildSuggestion(me, MessageHelper.getCoreMessage("USER_AUTOCOMPLETION.ME"));
            meAuto.setForced(true);
            resultats.add(meAuto);
        }
        for (final UtilisateurBean currentUser : users) {
            if(!currentUser.getCode().equals(meCode)) {
                final AutoCompleteSuggestion auto = buildSuggestion(currentUser, MessageHelper.getCoreMessage("USER_AUTOCOMPLETION.USERS"));
                resultats.add(auto);
            }
        }
        return resultats;
    }

    private AutoCompleteSuggestion buildSuggestion(UtilisateurBean user, String category) {
        final AutoCompleteSuggestion suggestion = new AutoCompleteSuggestion();
        final Map<String, String> data = new HashMap<>();
        data.put("category", category);
        data.put("value", user.getCode());
        suggestion.setData(data);
        suggestion.setValue(String.format("%s (%s)", serviceUser.getLibelle(user), user.getCode()));
        return suggestion;
    }

    @Override
    public List<AutoCompleteSuggestion> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        List<AutoCompleteSuggestion> resultats = new ArrayList<>();
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (autorisations == null) {
            return resultats;
        }
        return getResults(autorisations, serviceUser.getAllUsers());
    }

    @Override
    public String getKeyForCache() {
        String code = "anonyme";
        if (ContexteUtil.getContexteUniv() != null) {
            if (StringUtils.isNotEmpty(ContexteUtil.getContexteUniv().getCode())) {
                code = ContexteUtil.getContexteUniv().getCode();
            }
        }
        return String.format("%s_%s", ID_BEAN, code);
    }
}
