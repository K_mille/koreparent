package com.univ.batch.dao;

import java.util.Date;

public interface SpringBatchDao {

    /**
     * Nettoie les données antérieur à la dateNettoyage de la table BATCH_STEP_EXECUTION_CONTEXT
     * @param dateNettoyage date jusqu'à laquelle les données doivent être supprimées
     * @return le nombre de lignes supprimées
     */
    int cleanStepExecutionContext(Date dateNettoyage);

    /**
     * Nettoie les données antérieur à la dateNettoyage de la table BATCH_STEP_EXECUTION
     * @param dateNettoyage date jusqu'à laquelle les données doivent être supprimées
     * @return le nombre de lignes supprimées
     */
    int cleanStepExecution(Date dateNettoyage);

    /**
     * Nettoie les données antérieur à la dateNettoyage de la table BATCH_JOB_EXECUTION_CONTEXT
     * @param dateNettoyage date jusqu'à laquelle les données doivent être supprimées
     * @return le nombre de lignes supprimées
     */
    int cleanJobExecutionContext(Date dateNettoyage);

    /**
     * Nettoie les données antérieur à la dateNettoyage de la table BATCH_JOB_EXECUTION_PARAMS
     * @param dateNettoyage date jusqu'à laquelle les données doivent être supprimées
     * @return le nombre de lignes supprimées
     */
    int cleanJobExecutionParams(Date dateNettoyage);

    /**
     * Nettoie les données antérieur à la dateNettoyage de la table BATCH_JOB_EXECUTION
     * @param dateNettoyage date jusqu'à laquelle les données doivent être supprimées
     * @return le nombre de lignes supprimées
     */
    int cleanJobExecution(Date dateNettoyage);

    /**
     * Nettoie les données antérieur à la dateNettoyage de la table BATCH_JOB_INSTANCE
     * @param dateNettoyage date jusqu'à laquelle les données doivent être supprimées
     * @return le nombre de lignes supprimées
     */
    int cleanJobInstance(Date dateNettoyage);
}
