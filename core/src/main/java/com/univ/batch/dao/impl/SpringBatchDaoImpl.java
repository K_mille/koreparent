package com.univ.batch.dao.impl;

import java.util.Date;

import javax.sql.DataSource;

import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import com.univ.batch.dao.SpringBatchDao;

public class SpringBatchDaoImpl implements SpringBatchDao {

    protected NamedParameterJdbcTemplate namedParameterJdbcTemplate;

    @Override
    public int cleanStepExecutionContext(Date dateNettoyage) {
        String requete = "DELETE FROM C USING BATCH_STEP_EXECUTION_CONTEXT C JOIN BATCH_STEP_EXECUTION S ON C.STEP_EXECUTION_ID = S.STEP_EXECUTION_ID JOIN BATCH_JOB_EXECUTION J ON S.JOB_EXECUTION_ID = J.JOB_EXECUTION_ID WHERE J.CREATE_TIME < :dateNettoyage";
        return executeSimpleDeleteByDate(requete, dateNettoyage);
    }

    @Override
    public int cleanStepExecution(final Date dateNettoyage) {
        String requete = "DELETE FROM S USING BATCH_STEP_EXECUTION S JOIN BATCH_JOB_EXECUTION J on S.JOB_EXECUTION_ID = J.JOB_EXECUTION_ID WHERE J.CREATE_TIME < :dateNettoyage";
        return executeSimpleDeleteByDate(requete, dateNettoyage);
    }

    @Override
    public int cleanJobExecutionContext(final Date dateNettoyage) {
        String requete = "DELETE FROM C USING BATCH_JOB_EXECUTION_CONTEXT C JOIN BATCH_JOB_EXECUTION E ON C.JOB_EXECUTION_ID = E.JOB_EXECUTION_ID WHERE CREATE_TIME < :dateNettoyage";
        return executeSimpleDeleteByDate(requete, dateNettoyage);
    }

    @Override
    public int cleanJobExecutionParams(final Date dateNettoyage) {
        String requete = "DELETE FROM P USING BATCH_JOB_EXECUTION_PARAMS P JOIN BATCH_JOB_EXECUTION E on P.JOB_EXECUTION_ID=E.JOB_EXECUTION_ID WHERE CREATE_TIME < :dateNettoyage";
        return executeSimpleDeleteByDate(requete, dateNettoyage);
    }

    @Override
    public int cleanJobExecution(final Date dateNettoyage) {
        String requete = "DELETE FROM BATCH_JOB_EXECUTION where CREATE_TIME < :dateNettoyage";
        return executeSimpleDeleteByDate(requete, dateNettoyage);
    }

    @Override
    public int cleanJobInstance(final Date dateNettoyage) {
        String requete = "DELETE FROM I USING BATCH_JOB_INSTANCE I LEFT JOIN BATCH_JOB_EXECUTION E ON I.JOB_INSTANCE_ID = E.JOB_INSTANCE_ID WHERE E.JOB_INSTANCE_ID IS NULL";
        return this.namedParameterJdbcTemplate.update(requete, new MapSqlParameterSource());
    }

    /**
     * Lance une requête de suppression avec une "dateNettoyage" en paramètre.
     * @param requete la requête avec un attribut dateNettoyage
     * @param dateNettoyage la date à insérer dans la requête
     * @return le nombre de ligne supprimées
     */
    private int executeSimpleDeleteByDate(String requete, Date dateNettoyage){
        int retour = 0;
        if(dateNettoyage != null) {
            retour = this.namedParameterJdbcTemplate.update(requete, new MapSqlParameterSource("dateNettoyage", dateNettoyage));
        }
        return retour;
    }

    public void setDataSource(DataSource dataSource) {
        this.namedParameterJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
    }
}
