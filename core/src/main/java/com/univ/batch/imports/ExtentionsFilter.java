package com.univ.batch.imports;

import java.io.File;
import java.io.FilenameFilter;
import java.util.List;

/**
 * @author pierre.cosson
 *
 */
public class ExtentionsFilter implements FilenameFilter {

    /**
     * Liste des extentions de fichiers supportées par le filtre
     */
    private List<String> extentions;

    /**
     * Constructeur
     *
     * @param extentions
     *            Liste des extentions de fichiers supportées par le filtre
     */
    public ExtentionsFilter(List<String> extentions) {
        this.extentions = extentions;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.io.FilenameFilter#accept(java.io.File, java.lang.String)
     */
    @Override
    public boolean accept(File dir, String name) {
        int lastPointIndex = name.lastIndexOf(".");
        if (lastPointIndex > 0 && lastPointIndex < name.length()) {
            // le point ne doit pas être le premier caractére ni le dernier.
            String extention = name.substring(lastPointIndex + 1);
            return this.extentions.contains(extention);
        }
        return false;
    }
}
