package com.univ.batch.imports;

import java.io.File;
import java.io.FilenameFilter;
// TODO: Auto-generated Javadoc

/**
 * Filtre n'acceptant que les fichiers xml.
 */
public class XMLFilter implements FilenameFilter {

    /**
     * Renvoie true si le fichier est un fichier xml.
     *
     * @param dir
     *            the dir
     * @param name
     *            the name
     *
     * @return true, if accept
     */
    @Override
    public boolean accept(File dir, String name) {
        return name.toLowerCase().endsWith(".xml");
    }
}
