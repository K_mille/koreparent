package com.univ.batch.process;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.scheduling.spring.quartz.LogReportJob;
import com.univ.batch.service.SpringBatchService;
import com.univ.batch.service.impl.SpringBatchServiceImpl;

public class CleanBatch extends LogReportJob implements Runnable {

    private static final Logger LOGGER = LoggerFactory.getLogger(CleanBatch.class);

    private static final String PROPERTIES_HISTORIC_RETENTION = "batch.cleanBatch.historicRetention";
    private static final int DEFAULT_RENTENTION = 30;

    @Override
    public void perform() {
        run();
    }

    @Override
    public void run() {
        LOGGER.info("Début traitement CleanBatch");

        // Récupération de la propriété
        String retentionPropertyValue = PropertyHelper.getCoreProperty(PROPERTIES_HISTORIC_RETENTION);
        int retention = DEFAULT_RENTENTION;
        if(StringUtils.isNotEmpty(retentionPropertyValue) && StringUtils.isNumeric(retentionPropertyValue)){
            retention = Integer.valueOf(retentionPropertyValue);
        }

        // Lancement du traitement
        LOGGER.info(String.format("Lancement du traitement avec une durée de rétention de %d jours", retention));
        SpringBatchService aggregationFusion = (SpringBatchService) ApplicationContextManager.getBean(ApplicationContextManager.DEFAULT_CORE_CONTEXT, SpringBatchServiceImpl.ID_BEAN);
        int totalSuppressionLignes = aggregationFusion.nettoyerBatch(retention);
        LOGGER.info(String.format("Le traitement a supprimé %d lignes", totalSuppressionLignes));

        LOGGER.info("Fin traitement CleanBatch");
    }
}
