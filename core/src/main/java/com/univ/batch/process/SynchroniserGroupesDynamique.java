package com.univ.batch.process;

import java.util.Collection;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.scheduling.spring.quartz.LogReportJob;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.om.InfosRequeteGroupe;
import com.univ.objetspartages.om.RequeteGroupeDynamique;
import com.univ.objetspartages.om.RequeteGroupeDynamiqueSynchronisable;
import com.univ.objetspartages.om.SynchronisationGroupeDynamiqueEtat;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.utils.RequeteGroupeUtil;

/**
 * Classe qui peut être lancée par un batch dans le but de synchroniser les groupes dynamiques. Pour effectuer cette synchronisation on parcours tous les types de groupes
 * dynamiques en ne prennant que ceux synchronisables et ayant l'option d'auto-synchronisation mise à la valeur 1 (cette option est à complété dans le JTF :
 * <strong>requete_groupe.NOM_REQUETE.auto_synchronisation</strong>).<br>
 * Pour fonctionner cette classe lit le JTF puis instancie les classes des groupes dynamiques pour les synchroniser. Tous les groupes de la requete seront instanciés.
 *
 * @see RequeteGroupeDynamique
 * @see RequeteGroupeDynamiqueSynchronisable
 * @author Pierre Cosson
 */
public class SynchroniserGroupesDynamique extends LogReportJob implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(SynchroniserGroupesDynamique.class);

    /**
     * Fonction main lancée par un batch.
     *
     * @param args
     *            Les arguments.
     */
    public static void main(final String[] args) {
        final SynchroniserGroupesDynamique synchroniserGroupesDynamique = new SynchroniserGroupesDynamique();
        final Thread thread = new Thread(synchroniserGroupesDynamique);
        thread.start();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        // parcours de toutes les requetes de groupes dynamique
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final Map<String, String> groups = serviceGroupeDsi.getListeRequetesGroupesPourAffichage();
        for (String key : groups.keySet()) {
            final InfosRequeteGroupe infosRequete = serviceGroupeDsi.getGroupRequestList().get(key);
            final RequeteGroupeDynamique dynGroupe = RequeteGroupeUtil.instancierRequete(infosRequete);
            // on ne synchronise que les groupes qui sont synchronisables et
            // qui ont l'option de synchronisation automatique d'activée.
            if (dynGroupe != null && dynGroupe instanceof RequeteGroupeDynamiqueSynchronisable && ((RequeteGroupeDynamiqueSynchronisable) dynGroupe).isAutoSynchronise()) {
                LOG.info("   --- DEBUT de la synchronisation " + "de la requete : " + key);
                // Synchronisation du groupe
                final SynchronisationGroupeDynamiqueEtat etatSync = ((RequeteGroupeDynamiqueSynchronisable) dynGroupe).synchronize();
                // affichage du résultat de la requête dans le logger
                if (etatSync.isValide()) {
                    displayLogSynchronisation(etatSync);
                } else {
                    LOG.info("          Erreur durant la synchronisation ");
                }
                LOG.info("   --- FIN de la synchronisation" + " de la requete : " + key);
            }
        }
    }

    /**
     * Display logger synchronisation.
     *
     * @param etatSync
     *            the etat sync
     */
    private void displayLogSynchronisation(final SynchronisationGroupeDynamiqueEtat etatSync) {
        final Collection<String> tmGroupesAjoutes = etatSync.getLibellesGroupeSynchroAjoutes();
        String groupesAjoutes = "";
        if (!tmGroupesAjoutes.isEmpty()) {
            for (String tmGroupesAjoute : tmGroupesAjoutes) {
                if (!"".equals(groupesAjoutes)) {
                    groupesAjoutes += ", ";
                }
                groupesAjoutes += tmGroupesAjoute;
            }
        }
        final Collection<String> tmGroupesSupprimes = etatSync.getLibellesGroupeSynchroSupprimes();
        String groupesSupprimes = "";
        if (!tmGroupesSupprimes.isEmpty()) {
            for (String tmGroupesSupprime : tmGroupesSupprimes) {
                if (!"".equals(groupesSupprimes)) {
                    groupesSupprimes += ", ";
                }
                groupesSupprimes += tmGroupesSupprime;
            }
        }
        final Collection<String> tmGroupesModifies = etatSync.getLibellesGroupeSynchroModifies();
        String groupesModifies = "";
        if (!tmGroupesModifies.isEmpty()) {
            for (String tmGroupesModify : tmGroupesModifies) {
                if (!"".equals(groupesModifies)) {
                    groupesModifies += ", ";
                }
                groupesModifies += tmGroupesModify;
            }
        }
        LOG.info("          nombre de groupes dynamiques ajoutés : " + etatSync.getNbGroupesAjoutes());
        if (!"".equals(groupesAjoutes)) {
            LOG.info("          liste des groupes dynamiques ajoutés : " + groupesAjoutes);
        }
        LOG.info("          nombre de groupes dynamiques modifiés : " + etatSync.getNbGroupesModifies());
        if (!"".equals(groupesModifies)) {
            LOG.info("          liste des groupes dynamiques modifiés : " + groupesModifies);
        }
        LOG.info("          nombre de groupes dynamiques supprimés : " + etatSync.getNbGroupesSupprimes());
        if (!"".equals(groupesSupprimes)) {
            LOG.info("          liste des groupes dynamiques supprimés : " + groupesSupprimes);
        }
    }

    @Override
    public void perform() {
        run();
    }
}
