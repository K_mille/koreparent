package com.univ.batch.service;

public interface SpringBatchService {

    /**
     * Nettoie les tables de gestion des batchs.
     * @param historicRetention Durée de rétention des données en jour
     * @return nombre de ligne supprimées
     */
    int nettoyerBatch(int historicRetention);
}
