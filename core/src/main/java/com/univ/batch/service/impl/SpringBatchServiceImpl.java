package com.univ.batch.service.impl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.batch.dao.SpringBatchDao;
import com.univ.batch.service.SpringBatchService;

public class SpringBatchServiceImpl implements SpringBatchService {

    public static final String ID_BEAN = "springBatchService";

    private static final Logger LOGGER = LoggerFactory.getLogger(SpringBatchServiceImpl.class);

    private SpringBatchDao springBatchDao;

    @Override
    public int nettoyerBatch(int historicRetention) {
        DateFormat df = new SimpleDateFormat();

        // Calcul de la date de rétention
        Calendar cal = new GregorianCalendar();
        cal.add(GregorianCalendar.DAY_OF_WEEK, -historicRetention);
        Date date = cal.getTime();
        LOGGER.info("Suppression de l'historique Spring-Batch avant le {}", df.format(date));

        // Lancement des suppressions
        int totalSuppressionLignes = 0;
        int nbLignesSupprimees = springBatchDao.cleanStepExecutionContext(date);
        totalSuppressionLignes += nbLignesSupprimees;
        LOGGER.debug(String.format("Suppression de %d entrées dans BATCH_STEP_EXECUTION_CONTEXT", nbLignesSupprimees));

        nbLignesSupprimees = springBatchDao.cleanStepExecution(date);
        totalSuppressionLignes += nbLignesSupprimees;
        LOGGER.debug(String.format("Suppression de %d entrées dans BATCH_STEP_EXECUTION", nbLignesSupprimees));

        nbLignesSupprimees = springBatchDao.cleanJobExecutionContext(date);
        totalSuppressionLignes += nbLignesSupprimees;
        LOGGER.debug(String.format("Suppression de %d entrées dans BATCH_JOB_EXECUTION_CONTEXT", nbLignesSupprimees));

        nbLignesSupprimees = springBatchDao.cleanJobExecutionParams(date);
        totalSuppressionLignes += nbLignesSupprimees;
        LOGGER.debug(String.format("Suppression de %d entrées dans BATCH_JOB_EXECUTION_PARAMS", nbLignesSupprimees));

        nbLignesSupprimees = springBatchDao.cleanJobExecution(date);
        totalSuppressionLignes += nbLignesSupprimees;
        LOGGER.debug(String.format("Suppression de %d entrées dans BATCH_JOB_EXECUTION", nbLignesSupprimees));

        nbLignesSupprimees = springBatchDao.cleanJobInstance(date);
        totalSuppressionLignes += nbLignesSupprimees;
        LOGGER.debug(String.format("Suppression de %d entrées dans BATCH_JOB_INSTANCE", nbLignesSupprimees));

        return totalSuppressionLignes;
    }

    public void setSpringBatchDao(final SpringBatchDao springBatchDao) {
        this.springBatchDao = springBatchDao;
    }
}
