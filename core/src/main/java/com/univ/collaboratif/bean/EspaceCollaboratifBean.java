package com.univ.collaboratif.bean;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Created on 04/05/15.
 */
public class EspaceCollaboratifBean extends AbstractPersistenceBean{

    private static final long serialVersionUID = -5959104598441908777L;

    /** The code. */
    private String code = StringUtils.EMPTY;

    /** The actif. */
    private String actif = StringUtils.EMPTY;

    /** The theme. */
    private String theme = StringUtils.EMPTY;

    /** The inscription front. */
    private String inscriptionFront = StringUtils.EMPTY;

    /** The intitule. */
    private String intitule = StringUtils.EMPTY;

    /** The code structure. */
    private String codeStructure = StringUtils.EMPTY;

    /** The groupes membres. */
    private String groupesMembres = StringUtils.EMPTY;

    /** The roles membre. */
    private String rolesMembre = StringUtils.EMPTY;

    /** The description. */
    private String description = StringUtils.EMPTY;

    /** The inscriptions en cours. */
    private String inscriptionsEnCours = StringUtils.EMPTY;

    /** The services. */
    private String services = StringUtils.EMPTY;

    /** The date creation. */
    private Date dateCreation = null;

    /** The periodicite newsletter. */
    private String periodiciteNewsletter = StringUtils.EMPTY;

    /** The contenu newsletter. */
    private String contenuNewsletter = StringUtils.EMPTY;

    /** The date dernier envoi newsletter. */
    private Date dateDernierEnvoiNewsletter = null;

    /** The date envoi newsletter. */
    private Date dateEnvoiNewsletter = null;

    /** The groupes inscription. */
    private String groupesInscription = StringUtils.EMPTY;

    /** The groupes consultation. */
    private String groupesConsultation = StringUtils.EMPTY;

    /** The code rubrique. */
    private String codeRubrique = StringUtils.EMPTY;

    /** The langue. */
    private String langue = StringUtils.EMPTY;

    /** header */
    private String enteteEspace = StringUtils.EMPTY;

    /** footer */
    private String piedEspace = StringUtils.EMPTY;
    
    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getActif() {
        return actif;
    }

    public void setActif(final String actif) {
        this.actif = actif;
    }

    public String getTheme() {
        return theme;
    }

    public void setTheme(final String theme) {
        this.theme = theme;
    }

    public String getInscriptionFront() {
        return inscriptionFront;
    }

    public void setInscriptionFront(final String inscriptionFront) {
        this.inscriptionFront = inscriptionFront;
    }

    public String getIntitule() {
        return intitule;
    }

    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    public String getCodeStructure() {
        return codeStructure;
    }

    public void setCodeStructure(final String codeStructure) {
        this.codeStructure = codeStructure;
    }

    public String getGroupesMembres() {
        return groupesMembres;
    }

    public void setGroupesMembres(final String groupesMembres) {
        this.groupesMembres = groupesMembres;
    }

    public String getRolesMembre() {
        return rolesMembre;
    }

    public void setRolesMembre(final String rolesMembre) {
        this.rolesMembre = rolesMembre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getInscriptionsEnCours() {
        return inscriptionsEnCours;
    }

    public void setInscriptionsEnCours(final String inscriptionsEnCours) {
        this.inscriptionsEnCours = inscriptionsEnCours;
    }

    public String getServices() {
        return services;
    }

    public void setServices(final String services) {
        this.services = services;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getPeriodiciteNewsletter() {
        return periodiciteNewsletter;
    }

    public void setPeriodiciteNewsletter(final String periodiciteNewsletter) {
        this.periodiciteNewsletter = periodiciteNewsletter;
    }

    public String getContenuNewsletter() {
        return contenuNewsletter;
    }

    public void setContenuNewsletter(final String contenuNewsletter) {
        this.contenuNewsletter = contenuNewsletter;
    }

    public Date getDateDernierEnvoiNewsletter() {
        return dateDernierEnvoiNewsletter;
    }

    public void setDateDernierEnvoiNewsletter(final Date dateDernierEnvoiNewsletter) {
        this.dateDernierEnvoiNewsletter = dateDernierEnvoiNewsletter;
    }

    public Date getDateEnvoiNewsletter() {
        return dateEnvoiNewsletter;
    }

    public void setDateEnvoiNewsletter(final Date dateEnvoiNewsletter) {
        this.dateEnvoiNewsletter = dateEnvoiNewsletter;
    }

    public String getGroupesInscription() {
        return groupesInscription;
    }

    public void setGroupesInscription(final String groupesInscription) {
        this.groupesInscription = groupesInscription;
    }

    public String getGroupesConsultation() {
        return groupesConsultation;
    }

    public void setGroupesConsultation(final String groupesConsultation) {
        this.groupesConsultation = groupesConsultation;
    }

    public String getCodeRubrique() {
        return codeRubrique;
    }

    public void setCodeRubrique(final String codeRubrique) {
        this.codeRubrique = codeRubrique;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getEnteteEspace() {
        return enteteEspace;
    }

    public void setEnteteEspace(final String enteteEspace) {
        this.enteteEspace = enteteEspace;
    }

    public String getPiedEspace() {
        return piedEspace;
    }

    public void setPiedEspace(final String piedEspace) {
        this.piedEspace = piedEspace;
    }
}
