package com.univ.collaboratif.dao.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;

/**
 * Created on 04/05/15.
 */
public class EspaceCollaboratifDAO extends AbstractLegacyDAO<EspaceCollaboratifBean> {

    private static final Logger LOG = LoggerFactory.getLogger(EspaceCollaboratifDAO.class);

    public static final String ID_BEAN = "espaceCollaboratifDao";

    public EspaceCollaboratifDAO() {
        tableName = "ESPACECOLLABORATIF";
    }

    public EspaceCollaboratifBean getByCode(final String code) {
        EspaceCollaboratifBean result = null;
        try {
            final MapSqlParameterSource params = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject("SELECT * FROM ESPACECOLLABORATIF WHERE CODE = :code", params, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for code %s on table %s", code, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect resultset size for code %s", code));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured requesting for ESPACECOLLABORATIF row with code %s", code), dae);
        }
        return result;
    }

    public List<EspaceCollaboratifBean> getAll() {
        List<EspaceCollaboratifBean> results;
        try {
            results = namedParameterJdbcTemplate.query("SELECT * FROM ESPACECOLLABORATIF", rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException("Une erreur est survenue lors de la récupération des espaces", dae);
        }
        return results;
    }
}
