package com.univ.collaboratif.om;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.datasource.manager.impl.BasicDataSourceDAOManager;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.extension.ExtensionHelper;
import com.kportal.extension.IExtension;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.bean.UserRolesCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.dao.impl.UserRolesCollaboratifDAO;
import com.univ.collaboratif.service.CacheEspaceManager;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.InfosRole;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.services.ServiceGroupeUtilisateur;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.InfosRolesUtils;
import com.univ.objetspartages.util.LabelUtils;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUniv;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;

/**
 * The Class Espacecollaboratif.
 */
public class Espacecollaboratif extends AbstractOm<EspaceCollaboratifBean, EspaceCollaboratifDAO> implements Cloneable {

    public static final String ID_EXTENSION = "collaboratif";

    /** The Constant MAIL_MESSAGE_NOTIFICATION. */
    public static final String MAIL_MESSAGE_NOTIFICATION = "ST_COLLABORATIF_MAIL_MESSAGE_NOTIFICATION";

    /**
     * Instantiates a new espacecollaboratif.
     */
    public Espacecollaboratif() {
        super();
    }

    private static EspaceCollaboratifDAO getEspaceCollaboratifDao(){
        return ApplicationContextManager.getCoreContextBean(EspaceCollaboratifDAO.ID_BEAN, EspaceCollaboratifDAO.class);
    }

    /**
     * Vérifie si l'extension collaboratif est activée
     * @return True si l'extension est activée
     */
    public static boolean isExtensionActivated() {
        final IExtension extension = ExtensionHelper.getExtension(ID_EXTENSION);
        return extension != null && extension.getEtat() == IExtension.ETAT_ACTIF;
    }

    /**
     * Récupération de l'objet espace par son code.
     * @param ctx plus utiliser
     * @param code le code de l'espace à récupérer
     * @return l'espace ayant pour code celui fourni en paramètre ou null si non trouvé
     * @throws Exception lors de l'appel à la bdd
     * @deprecated le contexte ne sert plus, utiliser {@link Espacecollaboratif#getEspace(String)}
     */
    @Deprecated
    public static Espacecollaboratif getEspace(final OMContext ctx, final String code) throws Exception {
        return getEspace(code);
    }

    /**
     * Récupération de l'objet espace par son code.
     *
     * @param code le code de l'espace à récupérer
     * @return l'espace ayant pour code celui fourni en paramètre ou null si non trouvé
     * @throws Exception lors de l'appel à la bdd
     * @deprecated Utilisez ServiceEspaceCollaboratif.getEspace()
     */
    @Deprecated
    public static Espacecollaboratif getEspace(final String code) throws Exception {
        Espacecollaboratif res = null;
        if (StringUtils.isNotEmpty(code)) {
            Espacecollaboratif espace = new Espacecollaboratif();
            try (ContexteDao ctx = new ContexteDao()) {
                espace.setCtx(ctx);
                final ClauseWhere whereCode = new ClauseWhere(ConditionHelper.egalVarchar("CODE", code));
                final int count = espace.select(whereCode.formaterSQL());
                if (count == 1 && espace.nextItem()) {
                    res = espace;
                }
            }
        }
        return res;
    }

    /**
     * Renvoie la liste de tous les espaces Peut être affiché directement dans une Combo
     *
     * @param ctx the ctx
     *
     * @return the liste espaces collaboratifs
     *
     * @throws Exception
     *             the exception
     * @deprecated
     */
    @Deprecated
    public static Hashtable<String, String> getListeEspacesCollaboratifs(final OMContext ctx) throws Exception {
        return getListeEspacesCollaboratifs(ctx, false);
    }

    /**
     * Renvoie la liste de tous les espaces Peut être affiché directement dans une Combo
     *
     * @param ctx the ctx
     * @param avecTheme the avec theme
     * @return the liste espaces collaboratifs
     * @throws Exception the exception
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static Hashtable<String, String> getListeEspacesCollaboratifs(final OMContext ctx, final boolean avecTheme) throws Exception {
        final Hashtable<String, String> res = new Hashtable<>();
        for (final InfosEspaceCollaboratif espace : getListeEspaces()) {
            if (avecTheme) {
                String libelleTheme = Espacecollaboratif.getTheme(ctx, espace.getCodeTheme());
                res.put(espace.getCode(), libelleTheme + " - " + espace.getIntitule());
                if (!res.containsValue("T#" + espace.getCodeTheme())) {
                    res.put("T#" + espace.getCodeTheme(), libelleTheme + " - *");
                }
            } else {
                res.put(espace.getCode(), espace.getIntitule());
            }
        }
        return res;
    }

    /**
     * Récupération d'un espace stocké en mémoire.
     *
     * @param code the code
     * @return the infos espace collaboratif
     */
    public static EspaceCollaboratifBean renvoyerItemEspace(final String code) {
        return CacheEspaceManager.getInstance().getItem(code);
    }

    /**
     *
     * @param ctx le contexte ne sert plus
     * @param code le code de l'espace dont on souhaite récupérer le titre
     * @return l'intitulé de l'espace ou le message BO_ESPACE_COLLAB_INEXISTANT
     * @throws Exception lors de la requête en bdd...
     * @deprecated utiliser {@link Espacecollaboratif#getIntitule(String)}
     */
    @Deprecated
    public static String getIntitule(final OMContext ctx, final String code) throws Exception {
        return getIntitule(code);
    }

    /**
     * Récupération de l'intitulé.
     *
     * @param code le code de l'espace dont on souhaite récupérer le titre
     * @return l'intitulé de l'espace ou le message BO_ESPACE_COLLAB_INEXISTANT
     * @throws Exception lors de la requête en bdd...
     */
    public static String getIntitule(final String code) throws Exception {
        String res = MessageHelper.getCoreMessage("BO_ESPACE_COLLAB_INEXISTANT");
        if (StringUtils.isEmpty(code)) {
            return res;
        }
        return StringUtils.defaultIfBlank(renvoyerItemEspace(code).getIntitule(), MessageHelper.getCoreMessage("BO_ESPACE_COLLAB_INEXISTANT"));
    }

    /**
     * Gets the theme.
     *
     * @param ctx the ctx
     * @param codeTheme the code theme
     * @return Returns the theme.
     * @throws Exception the exception
     * @deprecated Méthode plus utilisée, à supprimer
     */
    @Deprecated
    public static String getTheme(final OMContext ctx, final String codeTheme) throws Exception {
        return LabelUtils.getLibelle("0110", codeTheme, ctx.getLocale());
    }

    /**
     * Renvoie la liste des espaces collaboratifs actifs.
     *
     * @return the liste espaces collaboratifs
     */
    public static Collection<InfosEspaceCollaboratif> getListeEspaces() {
        final Collection<InfosEspaceCollaboratif> listeEspaces = new ArrayList<>();
        final Espacecollaboratif espace = new Espacecollaboratif();
        espace.select(StringUtils.EMPTY);
        while (espace.nextItem()) {
            if ("1".equals(espace.getActif())) {
                InfosEspaceCollaboratif infosEspace = new InfosEspaceCollaboratif(espace.getIdEspacecollaboratif(), espace.getCode(), espace.getIntitule(), espace.getGroupesMembres(), espace.getGroupesConsultation(), espace.getGroupesInscription(), espace.getInscriptionFront(), espace.getRolesMembre(), espace.getTheme(), espace.getDescription(), espace.getCodeRubrique(), espace.getLangue(), "1".equals(espace.getActif()), new ArrayList<>(Chaine.getVecteurPointsVirgules(espace.getServices())));
                listeEspaces.add(infosEspace);
            }
        }
        return listeEspaces;
    }

    /**
     * Sélection des utilisateurs d'un espace rattachés par leur groupe (ne renvoie pas les membres affectés directement).
     *
     * @param codeEspace the code espace
     * @return the vector
     * @throws Exception the exception
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static Collection<String> renvoyerCodesUtilisateursParEspaceParGroupe(final String codeEspace) throws Exception {
        final TreeSet<String> membres = new TreeSet<>();
        EspaceCollaboratifBean infosEspace = getEspaceCollaboratifDao().getByCode(codeEspace);
        // Recherche parmi les groupes membres
        Vector<String> membresGroupes = Chaine.getVecteurAccolades(infosEspace.getGroupesMembres());
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        membres.addAll(serviceGroupeUtilisateur.getAllUserCodeByGroup(membresGroupes));
        return membres;
    }

    /**
     * Détermine la liste des utilisateurs ayant une permission donnée sur un espace
     * (utilisé dans 2 cas : droit de contribution / droit de validation sur un espace)
     *
     * Si renvoyerMails vaut true, ce ne sont pas les codes mais les mails qui sont renvoyés.
     *
     * @param permission
     *            the _permission
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     * @param renvoyerMails
     *            the renvoyer mails
     *
     * @return the liste utilisateurs possedant permission pour un espace
     *
     * @throws Exception
     *             the exception
     */
    public static TreeSet<String> getListeUtilisateursPossedantPermissionPourUnEspace(final PermissionBean permission, final String codeEspaceCollaboratif, final boolean renvoyerMails) throws Exception {
        final ServiceUser serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        final TreeSet<String> listeUtilisateurs = new TreeSet<>();
        final EspaceCollaboratifBean infosEspace = getEspaceCollaboratifDao().getByCode(codeEspaceCollaboratif);
        final String roleEspaceParDefaut = infosEspace.getRolesMembre();
        final Map<String, String> rolesCollab = InfosRolesUtils.getListeRolesEspaceCollaboratif();
        for (final String codeRole : rolesCollab.keySet()) {
            // On controle si le role contient la permission
            boolean roleOk = false;
            final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(codeRole);
            final Vector<PermissionBean> permissions = infosRole.getVecteurPermissions();
            for (final PermissionBean permissionRole : permissions) {
                if (permissionRole.getChaineSerialisee().equals(permission.getChaineSerialisee())) {
                    roleOk = true;
                    break;
                }
            }
            if (roleOk) {
                final UserRolesCollaboratifDAO userRolesCollaboratifDAO = (UserRolesCollaboratifDAO) ApplicationContextManager.getCoreContextBean(BasicDataSourceDAOManager.ID_BEAN,
                    BasicDataSourceDAOManager.class).getDao(
                    UserRolesCollaboratifBean.class);
                final List<UserRolesCollaboratifBean> userRoles = userRolesCollaboratifDAO.getByCodeRoleAndIdCollab(codeRole, infosEspace.getId());
                for (UserRolesCollaboratifBean currentUserRoles : userRoles) {
                    listeUtilisateurs.add(currentUserRoles.getCodeUser());
                }
                if (codeRole.equals(roleEspaceParDefaut)) {
                    // on ajoute aussi les utilisateurs des groupes membres
                    final Collection<String> groupesMembres = Chaine.getVecteurAccolades(infosEspace.getGroupesMembres());
                    final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
                    listeUtilisateurs.addAll(serviceGroupeUtilisateur.getAllUserCodeByGroup(groupesMembres));
                }
            }
        }
        final TreeSet<String> res = new TreeSet<>();
        if (renvoyerMails) {
            for (final String codeUtilisateur : listeUtilisateurs) {
                final UtilisateurBean user = serviceUser.getByCode(codeUtilisateur);
                if (user != null) {
                    res.add(user.getAdresseMail() + ";" + serviceUser.getLibelle(user) + ";" + codeUtilisateur);
                }
            }
        }
        return renvoyerMails ? res : listeUtilisateurs;
    }

    /**
     * Renvoie la liste des gestionnaires d'un espace
     *
     * @param codeEspace the code espace
     * @return the vector
     * @throws Exception the exception
     * @deprecated Méthode plus utilisée, à virer
     */
    @Deprecated
    public static Collection<String> renvoyerCodesUtilisateursParEspace(final String codeEspace) throws Exception {
        final TreeSet<String> listeMembres = new TreeSet<>();
        final UserRolesCollaboratifDAO userRolesCollaboratifDAO = (UserRolesCollaboratifDAO) ApplicationContextManager.getCoreContextBean(BasicDataSourceDAOManager.ID_BEAN,
            BasicDataSourceDAOManager.class).getDao(
            UserRolesCollaboratifBean.class);
        EspaceCollaboratifBean espace = getEspaceCollaboratifDao().getByCode(codeEspace);
        for (UserRolesCollaboratifBean currentUser : userRolesCollaboratifDAO.getByIdCollaboratif(espace.getId())) {
            listeMembres.add(currentUser.getCodeUser());
        }
        // Ajout des membres des groupes
        Collection<String> groupesMembres = Chaine.getVecteurAccolades(espace.getGroupesMembres());
        final ServiceGroupeUtilisateur serviceGroupeUtilisateur = ServiceManager.getServiceForBean(GroupeUtilisateurBean.class);
        listeMembres.addAll(serviceGroupeUtilisateur.getAllUserCodeByGroup(groupesMembres));
        //TODO FBI : gérer les sous-groupes
        return listeMembres;
    }

    /**
     * Supprimer l'utilisateur de ses espaces co.
     *
     * @param codeMembre the code membre
     * @throws Exception the exception
     */
    public static void supprimerUtilisateur(final String codeMembre) throws Exception {
        final UserRolesCollaboratifDAO userRolesCollaboratifDAO = (UserRolesCollaboratifDAO) ApplicationContextManager.getCoreContextBean(BasicDataSourceDAOManager.ID_BEAN,
            BasicDataSourceDAOManager.class).getDao(
            UserRolesCollaboratifBean.class);
        userRolesCollaboratifDAO.deleteByCodeUser(codeMembre);
    }

    /**
     * Renvoie la liste des espaces et rôles pour un code utilisateur et des groupes
     * @param userCode le code utilisateur
     * @param groupCodes la liste des groupes
     * @return la liste des espaces pour lesquels l'utilisateur est membre
     */
    public static Map<String, UserRolesCollaboratifBean> getRolesEspacesForUserAndGroup(String userCode, Collection<String> groupCodes) {
        final Map<String, UserRolesCollaboratifBean> results = new HashMap<>();
        final UserRolesCollaboratifDAO userRolesCollaboratifDAO = (UserRolesCollaboratifDAO) ApplicationContextManager.getCoreContextBean(BasicDataSourceDAOManager.ID_BEAN,
            BasicDataSourceDAOManager.class).getDao(
            UserRolesCollaboratifBean.class);
        final List<UserRolesCollaboratifBean> userRoles = userRolesCollaboratifDAO.getByCodeUser(userCode);
        for (UserRolesCollaboratifBean currentUserRoles : userRoles) {
            final EspaceCollaboratifBean espace = getEspaceCollaboratifDao().getById(currentUserRoles.getIdCollaboratif());
            if (espace != null) {
                results.put(espace.getCode(), currentUserRoles);
            }
        }
        for (InfosEspaceCollaboratif espace : Espacecollaboratif.getListeEspaces()) {
            for (String codeGroupe : groupCodes) {
                final Collection<String> groupesMembres = Chaine.getVecteurAccolades(espace.getGroupesMembres());
                if (groupesMembres.contains(codeGroupe)) {
                    final UserRolesCollaboratifBean currentRoleEspace = new UserRolesCollaboratifBean();
                    currentRoleEspace.setCodeUser(userCode);
                    currentRoleEspace.setIdCollaboratif(espace.getId());
                    currentRoleEspace.setCodeRole(espace.getRolesMembre());
                    if (results.get(espace.getCode()) == null) {
                        results.put(espace.getCode(), currentRoleEspace);
                    }
                }
            }
        }
        return results;
    }

    /**
     * Détermine si l'utilisateur a un droit de consultation de cet espace.
     *
     * @param infosEspace l'espace concerné
     * @return true, si l'utilisateur peut consulter l'espace
     * @deprecated Utilisez ServiceEspaceCollaboratif.isVisitor(EspaceCollaboratifBean)
     */
    @Deprecated
    public static boolean estVisiteurEspace(final ContexteUniv ctx, final EspaceCollaboratifBean infosEspace) {
        boolean isVisitor = false;
        // TODO FBI : gérer la récursivité
        final TreeSet<String> groupesUtilisateur = ctx.getGroupesDsiAvecAscendants();
        final Vector<String> groupesConsultationEspace = Chaine.getVecteurAccolades(infosEspace.getGroupesConsultation());
        for (final String groupeUtilisateur : groupesUtilisateur) {
            if (groupesConsultationEspace.contains(groupeUtilisateur)) {
                isVisitor = true;
                break;
            }
        }
        return isVisitor;
    }

    /**
     * Détermine si l'utilisateur est membre de l'espace.
     *
     * @param infosEspace l'espace concerné
     * @return true, si l'utilisateur est membre de l'espace
     * @deprecated Utilisez ServiceEspaceCollaboratif.isSpaceMember(EspaceCollaboratifBean)
     */
    @Deprecated
    public static boolean estMembreEspace(final ContexteUniv ctx, final EspaceCollaboratifBean infosEspace) {
        boolean isMembre = false;
        final UserRolesCollaboratifDAO userRolesCollaboratifDAO = (UserRolesCollaboratifDAO) ApplicationContextManager.getCoreContextBean(BasicDataSourceDAOManager.ID_BEAN,
            BasicDataSourceDAOManager.class).getDao(
            UserRolesCollaboratifBean.class);
        final UserRolesCollaboratifBean membre = userRolesCollaboratifDAO.getByCodeUserAndIdCollab(ctx.getCode(), infosEspace.getId());
        if (membre != null) {
            isMembre = true;
        }
        if (!isMembre) {
            final TreeSet<String> groupesUtilisateur = ctx.getGroupesDsiAvecAscendants();
            for (String groupeUtilisateur : groupesUtilisateur) {
                if (infosEspace.getGroupesMembres().contains(groupeUtilisateur)) {
                    isMembre = true;
                    break;
                }
            }
        }
        return isMembre;
    }

    /**
     * Set the context for data access purpose.
     * @param ctx : an {@link OMContext}
     * @deprecated This method is no longer necessary and should not be called. It actually does nothing.
     */
    @Deprecated
    public void setCtx(OMContext ctx) {
        // Not needed anymore
    }

    /**
     * Gets the theme.
     *
     * @return Returns the theme.
     * @throws Exception the exception
     * @deprecated Utilisez ServiceEspaceCollaboratif.getLibelleTheme
     */
    // TODO : à basculer sur l'extension collab
    @Deprecated
    public String getLibelleTheme(Locale locale) throws Exception {
        return LabelUtils.getLibelle("0110", getTheme(), locale);
    }

    /**
     * Checks if is actif.
     *
     * @return true, if is actif
     */
    public boolean isActif() {
        return "1".equals(getActif());
    }

    @Override
    public Espacecollaboratif clone() throws CloneNotSupportedException {
        return (Espacecollaboratif) super.clone();
    }

    // For retro-compatibility purposes
    public Long getId() {
        return persistenceBean.getId();
    }

    public void setId(Long id) {
        this.persistenceBean.setId(id);
    }

    public Long getIdEspacecollaboratif() {
        return persistenceBean.getId();
    }

    public void setIdEspacecollaboratif(Long id) {
        this.persistenceBean.setId(id);
    }

    public String getCode() {
        return persistenceBean.getCode();
    }

    public void setCode(final String code) {
        this.persistenceBean.setCode(code);
    }

    public String getActif() {
    return persistenceBean.getActif();
    }

    public void setActif(final String actif) {
        this.persistenceBean.setActif(actif);
    }

    public String getTheme() {
        return persistenceBean.getTheme();
    }

    public void setTheme(final String theme) {
        this.persistenceBean.setTheme(theme);
    }

    public String getInscriptionFront() {
        return persistenceBean.getInscriptionFront();
    }

    public void setInscriptionFront(final String inscriptionFront) {
        this.persistenceBean.setInscriptionFront(inscriptionFront);
    }

    public String getIntitule() {
        return persistenceBean.getIntitule();
    }

    public void setIntitule(final String intitule) {
        this.persistenceBean.setIntitule(intitule);
    }

    public String getCodeStructure() {
        return persistenceBean.getCodeStructure();
    }

    public void setCodeStructure(final String codeStructure) {
        this.persistenceBean.setCodeStructure(codeStructure);
    }

    public String getGroupesMembres() {
        return persistenceBean.getGroupesMembres();
    }

    public void setGroupesMembres(final String groupesMembres) {
        this.persistenceBean.setGroupesMembres(groupesMembres);
    }

    public String getRolesMembre() {
        return persistenceBean.getRolesMembre();
    }

    public void setRolesMembre(final String rolesMembre) {
        this.persistenceBean.setRolesMembre(rolesMembre);
    }

    public String getDescription() {
        return persistenceBean.getDescription();
    }

    public void setDescription(final String description) {
        this.persistenceBean.setDescription(description);
    }

    public String getInscriptionsEnCours() {
        return persistenceBean.getInscriptionsEnCours();
    }

    public void setInscriptionsEnCours(final String inscriptionsEnCours) {
        this.persistenceBean.setInscriptionsEnCours(inscriptionsEnCours);
    }

    public String getServices() {
        return persistenceBean.getServices();
    }

    public void setServices(final String services) {
        this.persistenceBean.setServices(services);
    }

    public Date getDateCreation() {
        return persistenceBean.getDateCreation();
    }

    public void setDateCreation(final Date dateCreation) {
        this.persistenceBean.setDateCreation(dateCreation);
    }

    public String getPeriodiciteNewsletter() {
        return persistenceBean.getPeriodiciteNewsletter();
    }

    public void setPeriodiciteNewsletter(final String periodiciteNewsletter) {
        this.persistenceBean.setPeriodiciteNewsletter(periodiciteNewsletter);
    }

    public String getContenuNewsletter() {
        return persistenceBean.getContenuNewsletter();
    }

    public void setContenuNewsletter(final String contenuNewsletter) {
        this.persistenceBean.setContenuNewsletter(contenuNewsletter);
    }

    public Date getDateDernierEnvoiNewsletter() {
        return persistenceBean.getDateDernierEnvoiNewsletter();
    }

    public void setDateDernierEnvoiNewsletter(final Date dateDernierEnvoiNewsletter) {
        this.persistenceBean.setDateDernierEnvoiNewsletter(dateDernierEnvoiNewsletter);
    }

    public Date getDateEnvoiNewsletter() {
        return persistenceBean.getDateEnvoiNewsletter();
    }

    public void setDateEnvoiNewsletter(final Date dateEnvoiNewsletter) {
        this.persistenceBean.setDateEnvoiNewsletter(dateEnvoiNewsletter);
    }

    public String getGroupesInscription() {
        return persistenceBean.getGroupesInscription();
    }

    public void setGroupesInscription(final String groupesInscription) {
        this.persistenceBean.setGroupesInscription(groupesInscription);
    }

    public String getGroupesConsultation() {
        return persistenceBean.getGroupesConsultation();
    }

    public void setGroupesConsultation(final String groupesConsultation) {
        this.persistenceBean.setGroupesConsultation(groupesConsultation);
    }

    public String getCodeRubrique() {
        return persistenceBean.getCodeRubrique();
    }

    public void setCodeRubrique(final String codeRubrique) {
        this.persistenceBean.setCodeRubrique(codeRubrique);
    }

    public String getLangue() {
        return persistenceBean.getLangue();
    }

    public void setLangue(final String langue) {
        this.persistenceBean.setLangue(langue);
    }

    public String getEnteteEspace() {
        return persistenceBean.getEnteteEspace();
    }

    public void setEnteteEspace(final String enteteEspace) {
        this.persistenceBean.setEnteteEspace(enteteEspace);
    }

    public String getPiedEspace() {
        return persistenceBean.getPiedEspace();
    }

    public void setPiedEspace(final String piedEspace) {
        this.persistenceBean.setPiedEspace(piedEspace);
    }
}
