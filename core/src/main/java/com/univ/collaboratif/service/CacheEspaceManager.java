package com.univ.collaboratif.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kportal.cache.CacheUtil;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;
import com.univ.collaboratif.dao.impl.EspaceCollaboratifDAO;
import com.univ.collaboratif.om.Espacecollaboratif;

/**
 * Cache manager for the espaces collaboratifs
 */
public class CacheEspaceManager {

    public static final String ID_BEAN = "cacheEspaceManager";

    public static final String CACHE_NAME = "CacheEspaceManager.cacheEspaces";

    private static final Logger LOG = LoggerFactory.getLogger(CacheEspaceManager.class);

    private EspaceCollaboratifDAO espaceCollaboratifDAO;

    public void setEspaceCollaboratifDAO(final EspaceCollaboratifDAO espaceCollaboratifDAO) {
        this.espaceCollaboratifDAO = espaceCollaboratifDAO;
    }

    /**
     * Gets the single instance of CacheEspaceManager.
     *
     * @return single instance of CacheEspaceManager
     */
    public static CacheEspaceManager getInstance() {
        return (CacheEspaceManager) ApplicationContextManager.getBean(Espacecollaboratif.ID_EXTENSION, ID_BEAN);
    }

    /**
     * Récupère l'info espace dans le cache, en le mettant à jour si nécessaire
     * @param code Le code de l'espace souhaité
     * @return Le bean infosEspace souhaité
     */
    public EspaceCollaboratifBean getItem(String code) {
        EspaceCollaboratifBean espacecollaboratif = (EspaceCollaboratifBean) CacheUtil.getObjectValue(CACHE_NAME, code);
        if (espacecollaboratif == null) {
            espacecollaboratif = espaceCollaboratifDAO.getByCode(code);
            if (espacecollaboratif != null) {
                CacheUtil.updateObjectValue(CACHE_NAME, code, espacecollaboratif);
            } else {
                LOG.error("Problème de récupération de l'espace " + code);
            }
        }
        return espacecollaboratif;
    }

    /**
     * Flush an espace cache item
     * @param code The code of the espace to flush
     */
    public void flushItem(String code) {
        CacheUtil.flush(CACHE_NAME, code);
    }
}
