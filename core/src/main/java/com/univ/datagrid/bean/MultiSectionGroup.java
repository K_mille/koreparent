package com.univ.datagrid.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Created on 03/05/16.
 */
public class MultiSectionGroup implements Serializable {

    private static final long serialVersionUID = -7575374689942869039L;

    private String label;

    private List<MultiSectionItem> items;

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public List<MultiSectionItem> getItems() {
        return items;
    }

    public void setItems(final List<MultiSectionItem> items) {
        this.items = items;
    }
}
