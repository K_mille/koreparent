package com.univ.datagrid.bean;

import java.io.Serializable;

/**
 * Created on 03/05/16.
 */
public class MultiSectionItem implements Serializable {

    private static final long serialVersionUID = 5996928773754426693L;

    private String code;

    private String label;

    private String path;

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getPath() {
        return path;
    }

    public void setPath(final String path) {
        this.path = path;
    }
}
