package com.univ.datagrid.bean;

import java.io.Serializable;

public abstract class ResultatDatagrid implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 7603215505150784373L;

    private String typeObjet;

    private Long id;

    private String libelle;

    private String urlModification;

    private String urlSuppression;

    public String getTypeObjet() {
        return typeObjet;
    }

    public void setTypeObjet(final String typeObjet) {
        this.typeObjet = typeObjet;
    }

    public Long getId() {
        return id;
    }

    public void setId(final Long id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(final String libelle) {
        this.libelle = libelle;
    }

    public String getUrlModification() {
        return urlModification;
    }

    public void setUrlModification(final String urlModification) {
        this.urlModification = urlModification;
    }

    public String getUrlSuppression() {
        return urlSuppression;
    }

    public void setUrlSuppression(final String urlSuppression) {
        this.urlSuppression = urlSuppression;
    }
}
