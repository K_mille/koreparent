package com.univ.datagrid.bean;

public class ResultatGroupeDSI extends ResultatDatagrid {

    /**
     *
     */
    private static final long serialVersionUID = -3351369348152152729L;

    private String typeGroupe;

    private String structure;

    public String getTypeGroupe() {
        return typeGroupe;
    }

    public void setTypeGroupe(final String typeGroupe) {
        this.typeGroupe = typeGroupe;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(final String structure) {
        this.structure = structure;
    }
}
