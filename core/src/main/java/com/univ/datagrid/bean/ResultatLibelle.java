package com.univ.datagrid.bean;

import java.io.Serializable;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collection;

import com.jsbsoft.jtf.core.LangueUtil;
import com.univ.utils.ContexteUtil;

public class ResultatLibelle extends ResultatDatagrid {

    /**
     *
     */
    private static final long serialVersionUID = -2785958866360075414L;

    private final Collection<LibelleParLangue> libelleParLangue = new ArrayList<>();

    private String type;

    private String code;

    public String getType() {
        return type;
    }

    public void setType(final String type) {
        this.type = type;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public Collection<LibelleParLangue> getLibelleParLangue() {
        return libelleParLangue;
    }

    public void addLibelleParLangue(final String langue, final String libelle) {
        final LibelleParLangue libelParLangue = new LibelleParLangue();
        libelParLangue.setLibelle(libelle);
        libelParLangue.setLangage(LangueUtil.getLocale(langue).getLanguage());
        libelParLangue.setLangue(LangueUtil.getDisplayName(langue));
        libelParLangue.setUrlLangue(LangueUtil.getPathImageDrapeau(langue));
        libelleParLangue.add(libelParLangue);
    }

    public static class LibelleParLangue implements Serializable, Comparable<LibelleParLangue> {

        /**
         *
         */
        private static final long serialVersionUID = 1816472748685126080L;

        private String libelle;

        private String langage;

        private String langue;

        private String urlLangue;

        public String getLibelle() {
            return libelle;
        }

        public void setLibelle(final String libelle) {
            this.libelle = libelle;
        }

        public String getLangage() {
            return langage;
        }

        public void setLangage(final String langage) {
            this.langage = langage;
        }

        public String getLangue() {
            return langue;
        }

        public void setLangue(final String langue) {
            this.langue = langue;
        }

        public String getUrlLangue() {
            return urlLangue;
        }

        public void setUrlLangue(final String urlLangue) {
            this.urlLangue = urlLangue;
        }

        @Override
        public int compareTo(LibelleParLangue libelleParLangue) {
            if (libelleParLangue == null) {
                return -1;
            }
            final Collator localeCollator = Collator.getInstance(ContexteUtil.getContexteUniv().getLocale());
            localeCollator.setStrength(Collator.SECONDARY);
            return localeCollator.compare(this.getLibelle(), libelleParLangue.getLibelle());
        }
    }
}
