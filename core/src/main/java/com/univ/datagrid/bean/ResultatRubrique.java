package com.univ.datagrid.bean;

public class ResultatRubrique extends ResultatDatagrid {

    /**
     *
     */
    private static final long serialVersionUID = -308942991898778779L;

    private String rubriqueMere;

    private String code;

    private String langue;

    private String urlDrapeauLangue;

    public String getRubriqueMere() {
        return rubriqueMere;
    }

    public void setRubriqueMere(final String rubriqueMere) {
        this.rubriqueMere = rubriqueMere;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getUrlDrapeauLangue() {
        return urlDrapeauLangue;
    }

    public void setUrlDrapeauLangue(final String urlDrapeauLangue) {
        this.urlDrapeauLangue = urlDrapeauLangue;
    }
}
