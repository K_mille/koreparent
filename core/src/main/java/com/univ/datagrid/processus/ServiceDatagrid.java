package com.univ.datagrid.processus;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.univ.datagrid.bean.CriteresDatagrid;
import com.univ.datagrid.bean.ResultatDatagrid;

public interface ServiceDatagrid {

    /**
     * Permet de traiter les critères de recherche reçu depuis un datagrid.
     *
     * @param req
     * @return
     */
    List<ResultatDatagrid> traiterRechercheDepuisRequete(HttpServletRequest req);

    /**
     * Permet de traiter des actions d'un datagrid (suppression de masse, archivage...).
     *
     */
    void traiterAction(final HttpServletRequest req);

    /**
     * Depuis la liste fourni en paramètre, on vérifie que les champs des résultats contienent le critère de recherche saisie par l'utilisateur et on ne retourne que ceux qui
     * l'ont.
     *
     *
     * @param resultats
     * @param criteres
     * @return
     */
    List<ResultatDatagrid> filtrerResultats(List<ResultatDatagrid> resultats, final CriteresDatagrid criteres);

    /**
     * La liste est trié en fonction de la colonne choisi et du sens de trie.
     *
     * @param resultats
     * @param criteres
     */
    void trierResultats(final List<ResultatDatagrid> resultats, final CriteresDatagrid criteres);

    List<ResultatDatagrid> postTraitementResultat(List<ResultatDatagrid> resultats);
}
