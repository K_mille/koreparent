package com.univ.datagrid.servlet;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.univ.datagrid.bean.CriteresDatagrid;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.datagrid.cache.CacheDatagridManager;
import com.univ.datagrid.processus.AbstractServiceDatagrid;
import com.univ.datagrid.utils.DatagridManager;
import com.univ.datagrid.utils.DatagridUtils;

/**
 * Servlet permettant de gérer les interactions avec le datagrid : recherche/actions de masse et d'orienter vers le bon composant pour réaliser les opérations.
 *
 * @author olivier.camon
 *
 */
public class DatagridServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -5619243850749934120L;

    private static final Logger LOG = LoggerFactory.getLogger(DatagridServlet.class);

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) {
        final AbstractServiceDatagrid gestionDatagrid = DatagridManager.getGestionDatagrid(req.getParameter(DatagridUtils.PARAM_BEAN_DATAGRID));
        final String idRequete = req.getParameter(DatagridUtils.PARAM_ID_REQUETE);
        List<ResultatDatagrid> resultats = Collections.emptyList();
        int total = 0;
        int totalAAfficher = 0;
        final CriteresDatagrid criteres = CriteresDatagrid.getCritereDepuisRequete(req);
        if (gestionDatagrid != null) {
            final String action = req.getParameter("ACTION");
            final CacheDatagridManager cacheDatagridManager = (CacheDatagridManager) ApplicationContextManager.getCoreContextBean(CacheDatagridManager.ID_BEAN);
            if (StringUtils.isNotBlank(action)) {
                cacheDatagridManager.flush(idRequete);
                gestionDatagrid.traiterAction(req);
            } else {
                resultats = cacheDatagridManager.traiterRecherche(idRequete, gestionDatagrid, req);
                gestionDatagrid.trierResultats(resultats, criteres);
                total = resultats.size();
                resultats = gestionDatagrid.filtrerResultats(resultats, criteres);
                totalAAfficher = resultats.size();
                resultats = DatagridUtils.paginerResultats(resultats, criteres);
                resultats = gestionDatagrid.postTraitementResultat(resultats);
            }
        }
        resp.setContentType("application/json");
        resp.setCharacterEncoding(CharEncoding.DEFAULT);
        try {
            resp.getWriter().write(DatagridUtils.mapperResultatEnJSON(resultats, total, totalAAfficher, criteres));
        } catch (final IOException e) {
            LOG.error("unable to write the json response", e);
        }
    }
}
