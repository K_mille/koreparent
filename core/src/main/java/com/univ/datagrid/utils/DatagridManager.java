package com.univ.datagrid.utils;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.datagrid.processus.AbstractServiceDatagrid;

public class DatagridManager {

    /** The Constant ID_BEAN. */
    public static final String ID_BEAN = "datagridManager";

    /**
     * Récupère le bean d'id passé en paramètre pour exécuter les actions à partir du datagrid
     *
     * @param idBean
     *            le bean à récuperer
     * @return le bean correspondant ou null si non trouvé
     */
    public static AbstractServiceDatagrid getGestionDatagrid(final String idBean) {
        return (AbstractServiceDatagrid) ApplicationContextManager.getEveryContextBean(idBean);
    }
}
