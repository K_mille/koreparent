package com.univ.datagrid.utils;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.core.ProcessusHelper;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;
import com.univ.datagrid.bean.CriteresDatagrid;
import com.univ.datagrid.bean.ResultatDatagrid;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.CritereRecherche;
import com.univ.utils.ContexteUtil;
import com.univ.utils.EscapeString;
import com.univ.utils.RechercheFicheHelper;
import com.univ.utils.json.CodecJSon;

public class DatagridUtils {

    public static final String PARAM_INFOBEAN_URL = "URL_RECHERCHE";

    public static final String PARAM_BEAN_DATAGRID = "BEAN_RECHERCHE";

    public static final String PARAM_ID_REQUETE = "ID_REQUETE";

    public static final String BASE_URL_DATAGRID = "/servlet/com.univ.datagrid.servlet.DatagridServlet";

    public static final String RECHERCHE = "sSearch";

    public static final String DEBUT_AFFICHAGE = "iDisplayStart";

    public static final String NOMBRE_A_AFFICHER = "iDisplayLength";

    public static final String INDICE_COLONNE_A_TRIER = "iSortCol_0";

    public static final String IS_COLONNE_TRIABLE = "bSortable_";

    public static final String S_ECHO = "sEcho";

    public static final String NOM_COLONNE_A_TRIER = "mDataProp_";

    public static final String SENS_DU_TRI = "sSortDir_0";

    private static final Logger LOG = LoggerFactory.getLogger(DatagridUtils.class);

    private static final int NOMBRE_MAX_RESULTAT_DEFAUT = 25000;

    public static int getNombreMaxDatagrid() {
        int nombreMaxResultat = NOMBRE_MAX_RESULTAT_DEFAUT;
        final String nombreMax = PropertyHelper.getCoreProperty("datagrid.nombremax");
        if (StringUtils.isNotBlank(nombreMax) && StringUtils.isNumeric(nombreMax)) {
            nombreMaxResultat = Integer.parseInt(nombreMax);
        }
        return nombreMaxResultat;
    }

    /**
     * permet de mapper la liste de {@link ResultatDatagrid} en JSON et de formatter ce json au format datatable
     *
     * @param resultats
     *            les résultats à envoyer à l'utilisateur
     * @param total
     *            le nombre total de résultat (5000 res de la requete SQL)
     * @param totalAAfficher
     *            le nombre total a afficher après filtrage via l'input de recherche
     * @return
     */
    public static String mapperResultatEnJSON(final List<ResultatDatagrid> resultats, final int total, final int totalAAfficher, final CriteresDatagrid criteres) {
        String json = StringUtils.EMPTY;
        try {
            final DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
            json = CodecJSon.encodeObjectToJSonInString(resultats, format);
        } catch (final IOException e) {
            LOG.info("impossible de sérialiser en JSON les données", e);
        }
        final StringBuilder reponse = new StringBuilder("{\"aaData\": ").append(json);
        reponse.append(",\"iTotalRecords\" : ").append(total);
        if (criteres.getsEcho() != 0) {
            reponse.append(",\"sEcho\" : ").append(criteres.getsEcho());
        }
        reponse.append(",\"iTotalDisplayRecords\" : ").append(totalAAfficher);
        final int nombreMax = getNombreMaxDatagrid();
        if (total == nombreMax) {
            final Locale locale = ContexteUtil.getContexteUniv().getLocale();
            String avertissement = MessageHelper.getCoreMessage(locale, "AVERTISSEMENT_DATAGRID");
            avertissement = String.format(avertissement, nombreMax);
            reponse.append(",\"sAlerte\" : \"").append(avertissement).append("\"");
        }
        reponse.append("}");
        return reponse.toString();
    }

    /**
     * retourne l'url servant à faire les requêtes Serveur pour le datagrid (recherche, action de masse ...) en ajoutant les paramètres à l'url fourni en paramètre
     *
     *
     * @param paramDatagrid
     *            les paramètres spécifiques déjà sous la forme FOO=BAR&BAR=FOO
     * @return l'url relative
     *
     */
    public static String getUrlTraitementDatagrid(final String paramDatagrid) {
        final StringBuilder url = new StringBuilder(BASE_URL_DATAGRID).append("?");
        if (StringUtils.isNotEmpty(paramDatagrid)) {
            url.append(paramDatagrid);
            url.append("&");
        }
        url.append(PARAM_ID_REQUETE).append("=").append(UUID.randomUUID());
        return url.toString();
    }

    public static String getUrlTraitementDatagrid(final InfoBean infoBean) {
        final Object critereDansInfoBean = infoBean.get(RechercheFicheHelper.ATTRIBUT_INFOBEAN_CRITERES);
        @SuppressWarnings("deprecation")
        final String requeteDansInfoBean = infoBean.getString(RechercheFicheHelper.ATTRIBUT_INFOBEAN_REQUETE);
        final StringBuilder url = new StringBuilder();
        if (critereDansInfoBean != null && critereDansInfoBean instanceof List<?>) {
            url.append(BASE_URL_DATAGRID).append("?");
            @SuppressWarnings("unchecked")
            final List<CritereRecherche> criteres = (List<CritereRecherche>) critereDansInfoBean;
            for (final CritereRecherche critere : criteres) {
                url.append(critere.getNomChamp()).append("=").append(EscapeString.escapeURL(critere.getValeurARechercher()));
                url.append("&");
            }
            url.append(PARAM_ID_REQUETE).append("=").append(UUID.randomUUID());
        } else if (StringUtils.isNotBlank(requeteDansInfoBean)) {
            url.append(getUrlTraitementDatagrid(requeteDansInfoBean));
        }
        return url.toString();
    }

    /**
     * Retourne l'url de modification d'une fiche en fonction de son id & de l'objet concerné
     *
     * @param codeObjet
     *            le code de l'objet (0001/0002...). Le code ne doit pas être null
     * @param idFiche
     *            l'id de la fiche à modifier. L'id ne doit pas être null
     * @param action
     *            l'action à réaliser sur la fiche (MODIFIER/SUPPRIMER..). L'action ne doit pas être null
     * @return l'url relative pour la modification
     */
    public static String getUrlActionFiche(final String codeObjet, final Long idFiche, final String action) {
        final String extension = ReferentielObjets.getExtension(codeObjet);
        final String processus = ReferentielObjets.getProcessus(codeObjet);
        return ProcessusHelper.getUrlProcessAction(null, extension, processus, action, new String[][] {{"ID_FICHE", String.valueOf(idFiche)}});
    }

    /**
     * Retourne l'url de modification d'une rubrique en fonction de son id & de l'objet concerné
     *
     * @param codeRubrique
     *            le code de la rubrique (par défaut un timestamp mais modifiable par le contributeur
     * @param action
     *            l'action à réaliser sur la fiche (MODIFIER/SUPPRIMER..)
     * @return l'url relative pour la modification
     */
    public static String getUrlActionRubrique(final String codeRubrique, final String action) {
        final InfoBean infoBean = new InfoBean();
        return ProcessusHelper.getUrlProcessAction(infoBean, "core", "SAISIE_RUBRIQUE", action, new String[][] {{"CODE_RUBRIQUE", codeRubrique}});
    }

    /**
     * Retourne l'url de modification d'un groupeDSI en fonction de son id & de l'objet concerné
     *
     * @param idGroupeDSI
     *            l'id du groupe (généré via la bdd
     * @param action
     *            l'action à réaliser sur la fiche (MODIFIER/SUPPRIMER..)
     * @return l'url relative pour la modification
     */
    public static String getUrlActionGroupeDSI(final String idGroupeDSI, final String action) {
        final InfoBean infoBean = new InfoBean();
        return ProcessusHelper.getUrlProcessAction(infoBean, "core", "SAISIE_GROUPEDSI", action, new String[][] {{"ID_GROUPE", idGroupeDSI}});
    }

    /**
     * Retourne l'url de modification d'un groupeDSI en fonction de son id & de l'objet concerné
     *
     * @param idLibelle
     *            l'id du groupe (généré via la bdd
     * @param action
     *            l'action à réaliser sur la fiche (MODIFIER/SUPPRIMER..)
     * @return l'url relative pour la modification
     */
    public static String getUrlActionLibelle(final String idLibelle, final String action) {
        final InfoBean infoBean = new InfoBean();
        return ProcessusHelper.getUrlProcessAction(infoBean, "core", "SAISIE_LIBELLE", action, new String[][] {{"ID_LIBELLE", idLibelle}});
    }

    /**
     * Retourne l'url de modification d'une fiche en fonction de son id & de l'objet concerné
     *
     * @param codeObjet
     *            le code de l'objet (0001/0002...)
     * @param action
     *            l'action à réaliser sur la fiche (MODIFIER/SUPPRIMER..)
     * @return l'url relative pour la modification
     */
    public static String getUrlActionSansIdFiche(final String codeObjet, final String action) {
        final InfoBean infoBean = new InfoBean();
        final String extension = ReferentielObjets.getExtension(codeObjet);
        final String processus = ReferentielObjets.getProcessus(codeObjet);
        return ProcessusHelper.getUrlProcessAction(infoBean, extension, processus, action, new String[][] {{}});
    }

    /**
     * Depuis la liste passé en paramètre, on retourne une nouvelle liste contenant uniquement les résutlats à afficher à l'utilisateur Si l'utilisateur est à la deuxieme page et
     * afficher 10 résutltats par page, on retourne les elements du 11eme au 20eme
     *
     * @param resultats
     *            les résultats avant pagination
     * @param criteres
     *            contient le nombre max à afficher et l'index de début
     * @return la sous liste à afficher
     */
    public static List<ResultatDatagrid> paginerResultats(final List<ResultatDatagrid> resultats, final CriteresDatagrid criteres) {
        final List<ResultatDatagrid> resultatsARetourner = new ArrayList<>();
        if (!resultats.isEmpty() && resultats.size() >= criteres.getDebutAffichage()) {
            int i = criteres.getDebutAffichage();
            while (i < criteres.getDebutAffichage() + criteres.getNombreAAfficher() && i < resultats.size()) {
                resultatsARetourner.add(resultats.get(i));
                i++;
            }
        } else {
            resultatsARetourner.addAll(resultats);
        }
        return resultatsARetourner;
    }

    /**
     * Permet de récuperer une map contenant la liste des langues par code de K-portal/K-Sup
     *
     * @return
     */
    public static Map<String, String> getLanguageParCodeLangue() {
        final Map<String, String> languageLocale = new HashMap<>();
        for (final Locale localeCourante : LangueUtil.getLocales()) {
            final String langueCourante = LangueUtil.getLangueLocale(localeCourante);
            languageLocale.put(langueCourante, localeCourante.getLanguage());
        }
        return languageLocale;
    }
}
