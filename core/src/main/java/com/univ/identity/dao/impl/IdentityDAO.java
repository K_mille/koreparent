package com.univ.identity.dao.impl;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.ParametersDataSourceException;
import com.univ.identity.bean.IdentityBean;
import com.univ.identity.om.Identity;
import com.univ.utils.sql.RequeteSQL;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;
import com.univ.utils.sql.operande.TypeOperande;

public class IdentityDAO<I extends IdentityBean> extends AbstractLegacyDAO<I> {

    private static final Logger LOG = LoggerFactory.getLogger(IdentityDAO.class);

    protected final ObjectMapper mapper = new ObjectMapper();

    public IdentityDAO() {
        this.tableName = "IDENTITY";
        this.rowMapper = new RowMapper<I>() {

            @Override
            public I mapRow(final ResultSet rs, final int rowNum) throws SQLException {
                I identityBean = null;
                final Class<?> className;
                try {
                    className = Class.forName(rs.getString("JAVA_TYPE"));
                    identityBean = (I) mapper.readValue(rs.getString("DATA"), className);
                    identityBean.setIdIdentity(rs.getLong("ID_IDENTITY"));
                    identityBean.setCivilite(rs.getString("CIVILITE"));
                    identityBean.setNom(rs.getString("NOM"));
                    identityBean.setPrenom(rs.getString("PRENOM"));
                    identityBean.setAdresse(rs.getString("ADRESSE"));
                    identityBean.setCodePostal(rs.getString("CODE_POSTAL"));
                    identityBean.setVille(rs.getString("VILLE"));
                    identityBean.setPays(rs.getString("PAYS"));
                    identityBean.setTelephone(rs.getString("TELEPHONE"));
                    identityBean.setEmail(rs.getString("EMAIL"));
                    identityBean.setData(rs.getString("DATA"));
                } catch (ClassNotFoundException | IOException e) {
                    LOG.error("An error occured instanciating an identityBean", e);
                }
                return identityBean;
            }
        };
        mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
    }

    public List<I> select(final Identity<I> identity, final String civilite, final String nom, final String prenom, final String adresse, final String codePostal, final String ville, final String pays, final String telephone, final String email, final String nombre) throws DataSourceException {
        final RequeteSQL requeteSelect = new RequeteSQL();
        final ClauseWhere where = new ClauseWhere();
        if (identity != null && identity.getIdIdentity() != null) {
            where.and(ConditionHelper.egal("ID_IDENTITY", identity.getIdIdentity(), TypeOperande.LONG));
        }
        if (StringUtils.isNotEmpty(civilite)) {
            where.and(ConditionHelper.egalVarchar("CIVILITE", civilite));
        }
        if (StringUtils.isNotEmpty(nom)) {
            where.and(ConditionHelper.egalVarchar("NOM", nom));
        }
        if (StringUtils.isNotEmpty(prenom)) {
            where.and(ConditionHelper.egalVarchar("PRENOM", prenom));
        }
        if (StringUtils.isNotEmpty(adresse)) {
            where.and(ConditionHelper.egalVarchar("ADRESSE", adresse));
        }
        if (StringUtils.isNotEmpty(codePostal)) {
            where.and(ConditionHelper.egalVarchar("CODE_POSTAL", codePostal));
        }
        if (StringUtils.isNotEmpty(ville)) {
            where.and(ConditionHelper.egalVarchar("VILLE", ville));
        }
        if (StringUtils.isNotEmpty(pays)) {
            where.and(ConditionHelper.egalVarchar("PAYS", pays));
        }
        if (StringUtils.isNotEmpty(telephone)) {
            where.and(ConditionHelper.egalVarchar("TELEPHONE", telephone));
        }
        if (StringUtils.isNotEmpty(email)) {
            where.and(ConditionHelper.egalVarchar("EMAIL", email));
        }
        requeteSelect.where(where);
        final ClauseOrderBy orderBy = new ClauseOrderBy();
        orderBy.orderBy("T1.ID_IDENTITY", ClauseOrderBy.SensDeTri.ASC);
        requeteSelect.orderBy(orderBy);
        //        final ClauseLimit limite = LimitHelper.ajouterCriteresLimitesEtOptimisation(ctx, nombre);
        //        requeteSelect.limit(limite);
        return select(requeteSelect.formaterRequete());
    }

    protected SqlParameterSource getParameters(final I identityBean) {
        MapSqlParameterSource param = new MapSqlParameterSource();
        param.addValue("civilite", identityBean.getCivilite(), Types.VARCHAR);
        param.addValue("nom", identityBean.getNom(), Types.VARCHAR);
        param.addValue("prenom", identityBean.getPrenom(), Types.VARCHAR);
        param.addValue("adresse", identityBean.getAdresse(), Types.VARCHAR);
        param.addValue("codePostal", identityBean.getCodePostal(), Types.VARCHAR);
        param.addValue("ville", identityBean.getVille(), Types.VARCHAR);
        param.addValue("pays", identityBean.getPays(), Types.VARCHAR);
        param.addValue("telephone", identityBean.getTelephone(), Types.VARCHAR);
        param.addValue("email", identityBean.getEmail(), Types.VARCHAR);
        param.addValue("javaType", identityBean.getClass().getName(), Types.BLOB);
        try {
            final String datas = mapper.writeValueAsString(identityBean);
            param.addValue("data", datas, Types.VARCHAR);
        } catch (JsonProcessingException e) {
            throw new ParametersDataSourceException(String.format("Une erreur est survenue lors de la génération des paramètres pour l'objet Identity portant l'id %d", identityBean.getId()), e);
        }
        param.addValue("id", identityBean.getIdIdentity(), Types.BIGINT);
        return param;
    }
}
