package com.univ.identity.om;

import java.lang.reflect.ParameterizedType;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.exceptions.AddToDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.UpdateToDataSourceException;
import com.jsbsoft.jtf.datasource.manager.DataSourceDAOManager;
import com.univ.identity.bean.IdentityBean;
import com.univ.identity.dao.impl.IdentityDAO;
import com.univ.utils.RequeteUtil;

/**
 * Classe representant un objet participation.
 */
public class Identity<I extends IdentityBean> {

    private static final Logger LOG = LoggerFactory.getLogger(Identity.class);

    protected I identity;

    protected IdentityDAO<I> identityDao;

    protected List<I> currentSelect;

    protected Iterator<I> currentSelectIt;

    @SuppressWarnings("unchecked")
    public Identity() {
        final DataSourceDAOManager manager = (DataSourceDAOManager) ApplicationContextManager.getCoreContextBean(DataSourceDAOManager.ID_BEAN);
        final Object dummy = manager.getDao(getGenericType());
        identityDao = (IdentityDAO<I>) dummy;
        try {
            identity = getGenericType().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOG.error("unable to instanciate Identity", e);
        }
    }

    @SuppressWarnings("unchecked")
    private Class<I> getGenericType() {
        return (Class<I>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Initialise l'objet metier.
     */
    public void init() {
        setCivilite("");
        setNom("");
        setPrenom("");
        setAdresse("");
        setCodePostal("");
        setVille("");
        setPays("");
        setTelephone("");
        setEmail("");
        setData("");
    }

    /**
     * Renvoie le libelle a afficher.
     */
    @JsonIgnore
    public String getLibelleAffichable() {
        return "Identity " + getIdIdentity();
    }

    /**
     * Traitement d'une requete sur l'objet.
     */
    public int traiterRequete(String requete) throws Exception {
        // Recuperation des parametres de la requete
        String civilite = RequeteUtil.renvoyerParametre(requete, "CIVILITE");
        String nom = RequeteUtil.renvoyerParametre(requete, "NOM");
        String prenom = RequeteUtil.renvoyerParametre(requete, "PRENOM");
        String adresse = RequeteUtil.renvoyerParametre(requete, "ADRESSE");
        String codePostal = RequeteUtil.renvoyerParametre(requete, "CODE_POSTAL");
        String ville = RequeteUtil.renvoyerParametre(requete, "VILLE");
        String pays = RequeteUtil.renvoyerParametre(requete, "PAYS");
        String telephone = RequeteUtil.renvoyerParametre(requete, "TELEPHONE");
        String email = RequeteUtil.renvoyerParametre(requete, "EMAIL");
        String nombre = RequeteUtil.renvoyerParametre(requete, "NOMBRE");
        return select(civilite, nom, prenom, adresse, codePostal, ville, pays, telephone, email, nombre);
    }

    public int select(String civilite, String nom, String prenom, String adresse, String codePostal, String ville, String pays, String telephone, String email, String nombre) {
        currentSelect = identityDao.select(this, civilite, nom, prenom, adresse, codePostal, ville, pays, telephone, email, nombre);
        currentSelectIt = currentSelect.iterator();
        return currentSelect.size();
    }

    public void retrieve() throws DataSourceException {
        identity = identityDao.getById(identity.getIdIdentity());
    }

    public void add() throws AddToDataSourceException {
        identityDao.add(identity);
    }

    public void delete() throws DeleteFromDataSourceException {
        identityDao.delete(identity.getIdIdentity());
    }

    public void update() throws UpdateToDataSourceException {
        identityDao.update(identity);
    }

    public Long getIdIdentity() {
        return identity.getIdIdentity();
    }

    public void setIdIdentity(Long idParticipation) {
        identity.setIdIdentity(idParticipation);
    }

    public String getCivilite() {
        return identity.getCivilite();
    }

    public void setCivilite(String civilite) {
        identity.setCivilite(civilite);
    }

    public String getNom() {
        return identity.getNom();
    }

    public void setNom(String nom) {
        identity.setNom(nom);
    }

    public String getPrenom() {
        return identity.getPrenom();
    }

    public void setPrenom(String prenom) {
        identity.setPrenom(prenom);
    }

    public String getAdresse() {
        return identity.getAdresse();
    }

    public void setAdresse(String adresse) {
        identity.setAdresse(adresse);
    }

    public String getCodePostal() {
        return identity.getCodePostal();
    }

    public void setCodePostal(String codePostal) {
        identity.setCodePostal(codePostal);
    }

    public String getVille() {
        return identity.getVille();
    }

    public void setVille(String ville) {
        identity.setVille(ville);
    }

    public String getPays() {
        return identity.getPays();
    }

    public void setPays(String pays) {
        identity.setPays(pays);
    }

    public String getTelephone() {
        return identity.getTelephone();
    }

    public void setTelephone(String telephone) {
        identity.setTelephone(telephone);
    }

    public String getEmail() {
        return identity.getEmail();
    }

    public void setEmail(String email) {
        identity.setEmail(email);
    }

    public String getData() {
        return identity.getData();
    }

    public void setData(String data) {
        identity.setData(data);
    }

    public String getJspComplementFragment() {
        return identity.getJspComplementFragment();
    }

    public void setJspComplementFragment(String complementFragment) {
        identity.setJspComplementFragment(complementFragment);
    }
}
