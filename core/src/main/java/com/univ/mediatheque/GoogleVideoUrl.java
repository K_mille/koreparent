package com.univ.mediatheque;

public class GoogleVideoUrl implements SpecificUrl {

    private static String URL_SOURCE = "http://video.google.com/googleplayer.swf?docid=";

    private static String TYP_RESSOURCE = "video";

    @Override
    public String processUrl(String url) {
        // Url entrée
        // Url sortie http://video.google.com/googleplayer.swf?docid=
        String res = url;
        String codeVideo = "";
        if (url.contains("docid=")) {
            codeVideo = url.substring(url.indexOf("docid=") + 6, url.length());
            if (codeVideo.contains("&")) {
                codeVideo = codeVideo.substring(0, codeVideo.indexOf("&"));
            }
            res = URL_SOURCE + codeVideo + "&amp;fs=true";
        }
        return res;
    }

    @Override
    public String getTypeRessource() {
        return TYP_RESSOURCE;
    }

    @Override
    public String getUrlSource() {
        return URL_SOURCE;
    }
}
