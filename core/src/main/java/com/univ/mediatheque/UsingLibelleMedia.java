package com.univ.mediatheque;

import java.util.List;

import com.jsbsoft.jtf.exception.ErreurApplicative;

public interface UsingLibelleMedia {

    List<String> getCodesLibelleMedia();

    void checkCodesLibelleMedia(long idMedia) throws ErreurApplicative;
}
