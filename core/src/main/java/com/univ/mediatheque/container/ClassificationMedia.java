package com.univ.mediatheque.container;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.univ.objetspartages.bean.MediaBean;

/**
 * Il s'agit d'un objet qui permet de contenir un ensemble de {@link com.univ.objetspartages.bean.MediaBean} ainsi que des informations sur l'affichage de ces derniers : intitule, tri et style. Cela veut dire que
 * le même style sera appliqué à tous les média de cette objet.
 *
 * @author Pierre Cosson
 * @see ClassifieurMedia
 */
public class ClassificationMedia implements Comparable<ClassificationMedia> {

    /**
     * L'identifiant auto-généré de la classification (regénrer à chaque initialisation).
     */
    private String code;

    /**
     * Le code de classification définissant les médias à insérer dans cette classification.
     */
    private String codeClassification;

    /**
     * L'intitulé de la classification.
     */
    private String intitule;

    /**
     * La liste de médias qui correspondent à la classifiaction. Cette liste est mise à jour par un {@link ClassifieurMedia} et vidée à chaque initialisation.
     */
    private List<MediaBean> medias;

    /**
     * Constructeur.
     */
    public ClassificationMedia() {
        this.init();
    }

    /**
     * Initialiser le code et les medias de l'insatnce.
     */
    public void init() {
        final String randomValue = String.valueOf(new Random().nextInt(10000));
        this.code = String.valueOf(System.currentTimeMillis() + randomValue);
        this.medias = new ArrayList<>();
    }

    /**
     * Gets the code classification.
     *
     * @return the codeClassification
     */
    public String getCodeClassification() {
        return codeClassification;
    }

    /**
     * Sets the medias.
     *
     * @param medias
     *            the medias to set
     */
    public void setMedias(final List<MediaBean> medias) {
        this.medias = medias;
    }

    /**
     * Gets the code.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Gets the intitule.
     *
     * @return the intitule
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Gets the medias.
     *
     * @return the medias
     */
    public List<MediaBean> getMedias() {
        return medias;
    }

    /**
     * Ajouter un média à la classification.
     *
     * @param media
     *            Le {@link com.univ.objetspartages.bean.MediaBean} à ajouter.
     */
    public void addMedia(final MediaBean media) {
        this.medias.add(media);
    }

    /**
     * Savoir si cette instance contient des {@link com.univ.objetspartages.bean.MediaBean}.
     *
     * @return TRUE s'il n'y a pas de {@link com.univ.objetspartages.bean.MediaBean} sinon FALSE.
     */
    public boolean isEmpty() {
        return this.medias.isEmpty();
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(final ClassificationMedia o) {
        return this.getIntitule().compareToIgnoreCase(o.getIntitule());
    }

    public void setCodeClassification(final String codeClassification) {
        this.codeClassification = codeClassification;
    }

    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }
}
