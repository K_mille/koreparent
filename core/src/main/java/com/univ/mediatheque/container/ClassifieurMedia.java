package com.univ.mediatheque.container;

import java.util.Collection;
import java.util.List;

import com.univ.objetspartages.bean.MediaBean;

/**
 * Un {@link ClassifieurMedia} est un élement qui permet de classer, trier, les {@link com.univ.objetspartages.bean.MediaBean} selon un critére afin de les afficher dans des {@link VisionneuseMultimedia}.
 *
 * @author Pierre Cosson
 */
public interface ClassifieurMedia {

    /**
     * Donner la liste d'élements de tri.
     *
     * @param classifications
     */
    void setClassifications(Collection<ClassificationMedia> classifications);

    /**
     * On remplie les elements de tri avec les médias correspondant au critéres de classement.
     *
     * @param medias
     *            La liste des médias à classer dans les différents élements de classement.
     * @return la liste des éléments de classement rempli de {@link com.univ.objetspartages.bean.MediaBean}.
     */
    Collection<ClassificationMedia> classifierMedias(List<MediaBean> medias);
}
