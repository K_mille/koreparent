package com.univ.mediatheque.container;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import com.univ.objetspartages.bean.MediaBean;

/**
 * Ce classifieur permet de classer les {@link com.univ.objetspartages.bean.MediaBean} selon leur type : video, audio, photo et fichier. <br/>
 * Les élements du JTF à définir :
 * <ul>
 * <li>mediatheque.classificateurParType.code. + CODE_CLASSIFICATION : ces elements servent à retrouver le type de ressources associé à la classification</li>
 * </ul>
 *
 * @author Pierre Cosson
 */
public class ClassifieurParTypeDeMedia implements ClassifieurMedia {

    /** La liste des éléments de classification. */
    private Map<String, ClassificationMedia> classifications = null;

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.mediatheque.container.ClassifieurMedia#setClassifications(
     * java.util.Collection)
     */
    @Override
    public void setClassifications(final Collection<ClassificationMedia> classifications) {
        this.classifications = new TreeMap<>();
        String mediaType;
        ClassificationMedia classification;
        for (final ClassificationMedia classification1 : classifications) {
            classification = classification1;
            mediaType = classification.getCodeClassification();
            this.classifications.put(mediaType, classification);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.kosmos.mediatheque.container.ClassifieurMedia#classifierMedias(java
     * .util.List)
     */
    @Override
    public Collection<ClassificationMedia> classifierMedias(final List<MediaBean> medias) {
        this.initClassifications();
        for (final MediaBean media : medias) {
            final ClassificationMedia classification = this.classifications.get(media.getTypeRessource().toLowerCase());
            if (classification != null) {
                classification.addMedia(media);
            }
        }
        return this.classifications.values();
    }

    /**
     * Initialiser tous les objets {@link ClassificationMedia} qui sont contenus dans l'instance.
     */
    private void initClassifications() {
        for (final ClassificationMedia classificationMedia : this.classifications.values()) {
            classificationMedia.init();
        }
    }
}
