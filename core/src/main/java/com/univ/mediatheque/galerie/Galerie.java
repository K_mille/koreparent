package com.univ.mediatheque.galerie;

import java.io.Writer;

import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.mediatheque.playlist.visionneuse.VisionneusePlaylist;
import com.univ.utils.ContexteUniv;

/**
 * Une galerie multimedia est un élément qui permet d'afficher des {@link com.univ.objetspartages.bean.MediaBean} à l'écran en s'abstrayant du type de ces médias. Avec cet élément on s'interesse qu'à la
 * présentation générale (c'est un élément de contenu) et l'arangement des {@link com.univ.objetspartages.bean.MediaBean} sur une page HTML.<br/>
 * Pour atteindre un niveau d'abstraction adéquat on à utiliser divers notion comme les {@link VisionneuseMedia}, les {@link MediaPlayer}, les {@link PlayerStyleStyle} et les
 * {@link PlaylistStyle}.
 *
 * @author Pierre Cosson
 */
public interface Galerie {

    GalerieType getType();

    String getName();

    void setTitre(String titre);

    void setVisionneusePlaylist(VisionneusePlaylist visionneusePlaylist);

    void afficherGalerieComplete(ContexteUniv ctx, Writer out, MediaPlaylist mediaList) throws Exception;

    void initialiserPlayer(ContexteUniv ctx, Writer out, MediaPlaylist mediaList) throws Exception;

    void initialiserPlaylist(ContexteUniv ctx, Writer out, MediaPlaylist mediaList) throws Exception;
}
