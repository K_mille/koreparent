package com.univ.mediatheque.player;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.style.PlayerStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;
// TODO: Auto-generated Javadoc

/**
 * Ce player n'en est pas vraiment un puisqu'il permet seulement d'offir le téléchargement d'un média de type fichier. De plus cette classe produit des éléments avec les class CSS
 * suivantes :
 * <ul>
 * <li>lanceur_media sur une balise A (la balise qui permet de jouer le média dans le player)</li>
 * <li>lanceur_media_fichier sur une balise A (la balise qui permet de jouer le média dans le player)</li>
 * <li>lanceur_media_play sur une balise A (quand c'est le media lié à ce lanceur qui est jouer) FAIT VIA DU JS</li>
 * </ul>
 *
 * @author Pierre Cosson
 */
public class FichierPlayer implements MediaPlayer {

    public static final MediaPlayerType PLAYER_TYPE = MediaPlayerType.FICHIER_PLAYER;

    private static String HTML_CLASS_CSS_A = "lanceur_media lanceur_media_fichier";

    /**
     * Style du player.
     */
    private PlayerStyle style;

    public void setStyle(PlayerStyle style) {
        this.style = style;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.MediaPlayer#getStyle()
     */
    @Override
    public PlayerStyle getStyle() {
        return this.style;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getLanceurMedia(com.univ.utils
     * .ContexteUniv, com.univ.objetspartages.om.Media, java.lang.String)
     */
    @Override
    public LanceurMedia getLanceurMedia(ContexteUniv ctx, MediaBean media, String playerId) {
        LanceurMedia lanceur = new LanceurMedia(media);
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, HTML_CLASS_CSS_A);
        lanceur.addAttributHTML(AttributBaliseHTML.ON_CLICK, "window.open(this.href);");
        return lanceur;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getURLPlayerDistant(com.univ.
     * utils.ContexteUniv, com.univ.objetspartages.om.Media)
     */
    @Override
    public String getURLPlayerDistant(ContexteUniv ctx, MediaBean media) {
        String urlMedia = URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx);
        if (!urlMedia.contains("?")) {
            urlMedia += "?";
        } else {
            urlMedia += "&amp;";
        }
        // pas d'affichage du une popup (voir chickbox)
        return urlMedia + "width=0&amp;height=0";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.MediaPlayer#getInialiserVuePlayer(com.univ
     * .utils.ContexteUniv, java.lang.String)
     */
    @Override
    public String getInialiserVuePlayer(ContexteUniv ctx, String playerId) {
        // pas d'initialisation pour ce type de player
        return "";
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.MediaPlayer#getType()
     */
    @Override
    public MediaPlayerType getType() {
        return PLAYER_TYPE;
    }
}
