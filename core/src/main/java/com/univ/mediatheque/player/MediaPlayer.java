package com.univ.mediatheque.player;

import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.style.PlayerStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.utils.ContexteUniv;

/**
 * Un {@link MediaPlayer} permet de construire le code HTML capable d'utiliser un élément permettant de jouer un média.
 *
 * Un player est un ensemble de ligne de code pouvant comprende du JS, de l'HTML et/ou du flash (la majeur partie du temps c'est les 3).
 *
 * @author Pierre Cosson
 */
public interface MediaPlayer {

    /**
     * Récupérer le type de Player dont il s'agit.
     *
     * @return Le type de player
     */
    MediaPlayerType getType();

    /**
     * Récupérer le code HTML permettant d'nitialiser le player de média.
     *
     * @param ctx
     *            Le contexte de l'application.
     *
     * @param playerId
     * @return le code HTML qui permet d'initialiser le player coté client.
     */
    String getInialiserVuePlayer(ContexteUniv ctx, String playerId);

    /**
     * L'URL du player distant (utilisé dans le mode popup, il s'agit d'une page JSP qu'il est possible d'appeler via une requête AJAX).
     *
     * @param ctx
     * @param media
     *            le MEDIA à jouer
     * @return L'URL du player distant.
     */
    String getURLPlayerDistant(ContexteUniv ctx, MediaBean media);

    /**
     * Récupérer l'élément ou les éléments (HTML ou JS) capable de faire jouer le média dans le player.
     *
     * @param ctx
     *            Le contexte de l'application pour l'accés à la base de données.
     * @param media
     *            Le média.
     * @param playerId
     *            L'Identifiant du player dans la galerie.
     * @return Le {@link LanceurMedia} qui peut lancer le média dans le player.
     */
    LanceurMedia getLanceurMedia(ContexteUniv ctx, MediaBean media, String playerId);

    /**
     * Récupérer le {@link PlayerStyle} d'affichage du player instancier.
     *
     * @return le style du player ou NULL si ce dernier n'en posséde pas.
     */
    PlayerStyle getStyle();
}
