package com.univ.mediatheque.player;

import com.kportal.core.config.PropertyHelper;

public enum MediaPlayerType {
    /**
     * Le player VIDEO capable de jouer des medias de type VIDEO.
     */
    VIDEO_PLAYER("mediatheque.video.player.media.type", "mediatheque.video.player.height", "mediatheque.video.player.width", "mediatheque.video.player.url", "mediatheque.video.player.urlDistante", "mediatheque.video.player.proprieteAutorise"),
    /**
     * Le player AUDIO capable de jouer des médias de type AUDIO.
     */
    AUDIO_PLAYER("mediatheque.audio.player.media.type", "mediatheque.audio.player.height", "mediatheque.audio.player.width", "mediatheque.audio.player.url", "mediatheque.audio.player.urlDistante", "mediatheque.audio.player.proprieteAutorise"),
    /**
     * Le player FICHIER capable de jouer des médias de type FICHIER.
     */
    FICHIER_PLAYER("mediatheque.fichier.player.media.type", "", "", "", "", ""),
    /**
     * Le player FLASH capable de jouer des médias de type FLASH.
     */
    FLASH_PLAYER("mediatheque.flash.player.media.type", "mediatheque.flash.player.height", "mediatheque.flash.player.width", "", "mediatheque.flash.player.urlDistante", ""),
    /**
     * Le player PHOTO capable de jouer des médias de type PHOTO.
     */
    PHOTO_PLAYER("mediatheque.photo.player.media.type", "mediatheque.photo.player.height", "mediatheque.photo.player.width", "", "", ""),
    /**
     *
     */
    FLIPBOOK_PLAYER("mediatheque.flipbook.player.media.type", "mediatheque.flipbook.player.height", "mediatheque.flipbook.player.width", "", "mediatheque.flipbook.player.urlDistante", "");

    private String propertyMediaType;

    private String propertyHeight;

    private String propertyWidth;

    private String propertyUrl;

    private String propertyUrlPlayerDistant;

    private String propertyProprieteAutorisee;

    private MediaPlayerType(final String propertyMediaType, final String propertyHeight, final String propertyWidth, final String propertyUrl, final String propertyUrlPlayerDistant, final String propertyProprieteAutorisee) {
        this.propertyMediaType = propertyMediaType;
        this.propertyHeight = propertyHeight;
        this.propertyWidth = propertyWidth;
        this.propertyUrl = propertyUrl;
        this.propertyUrlPlayerDistant = propertyUrlPlayerDistant;
        this.propertyProprieteAutorisee = propertyProprieteAutorisee;
    }

    /**
     * Récupérer le type de media qui peut joué par le player..
     *
     * @return Le type de MEDIA.
     */
    public String getMediaType() {
        return PropertyHelper.getCoreProperty(propertyMediaType);
    }

    /**
     * La hauteur par defaut du player.
     *
     * @return La hauteur par defaut du player si elle est définie sinon -1.
     */
    public int getHeight() {
        final String height = PropertyHelper.getCoreProperty(propertyHeight);
        if (height != null) {
            return Integer.parseInt(height);
        } else {
            return -1;
        }
    }

    /**
     * La largeur par defaut du player.
     *
     * @return La largeur par defaut du player si elle est définie sinon -1.
     */
    public int getWidth() {
        final String width = PropertyHelper.getCoreProperty(propertyWidth);
        if (width != null) {
            return Integer.parseInt(width);
        } else {
            return -1;
        }
    }

    /**
     * Récupérer l'URL du player distant.
     *
     * @return L'url du player distant (relative).
     */
    public String getPlayerUrl() {
        return PropertyHelper.getCoreProperty(propertyUrl);
    }

    /**
     * Récupérer l'URL du player distant.
     *
     * @return L'url du player distant (relative)
     */
    public String getUrlDistante() {
        return PropertyHelper.getCoreProperty(propertyUrlPlayerDistant);
    }

    /**
     * La liste des propriétés accépetées par le player;
     *
     * @return TRUE si la propriété est accépté par le player.
     */
    public boolean isAutorisee(final String propriete) {
        final String stringProprietes = PropertyHelper.getCoreProperty(propertyProprieteAutorisee);
        return stringProprietes != null && stringProprietes.contains("[" + propriete + "]");
    }
}
