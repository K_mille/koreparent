package com.univ.mediatheque.player.visionneuse;

import java.io.Writer;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

/**
 * Visionneuse qui permet d'afficher un player pour tous les médias.
 *
 * @author Pierre Cosson
 */
public class VisionneuseModeMulti implements VisionneusePlayer {

    /** Le mode d'affichage de cette visionneuse. */
    public static final VisionneusePlayerType MODE = VisionneusePlayerType.MULTI;

    private String vionneuseKey;

    private int indicePlayerInitialises;

    private int maxHeight = -1;

    private int maxWidth = -1;

    /**
     * Constructeur.
     */
    public VisionneuseModeMulti() {
        this.init();
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getMaxHeight()
     */
    @Override
    public int getMaxHeight() {
        return this.maxHeight;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getMaxWidth()
     */
    @Override
    public int getMaxWidth() {
        return this.maxWidth;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.visionneuse.VisionneusePlayer#init()
     */
    @Override
    public void init() {
        this.vionneuseKey = "v_player_multi_" + String.valueOf(System.currentTimeMillis());
        this.indicePlayerInitialises = 0;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getType()
     */
    @Override
    public VisionneusePlayerType getType() {
        return MODE;
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.player.visionneuse.VisionneusePlayer#
     * initialiserVisionneuse(com.univ.utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.player.MediaPlayer)
     */
    @Override
    public void initialiserVisionneuse(ContexteUniv ctx, Writer out, MediaPlayer player) throws Exception {
        // ne fait rien
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#setLanceurMedia
     * (com.univ.utils.ContexteUniv, com.univ.objetspartages.om.Media,
     * com.univ.mediatheque.player.MediaPlayer,
     * com.univ.mediatheque.style.MediaStyle)
     */
    @Override
    public void setLanceurMedia(ContexteUniv ctx, MediaBean media, MediaPlayer player, MediaStyle mediaStyle) {
        LanceurMedia lanceur = player.getLanceurMedia(ctx, media, this.getPlayerId());
        lanceur.addHTMLAvantLanceur(player.getInialiserVuePlayer(ctx, this.getPlayerId()));
        lanceur.addAttributHTML(AttributBaliseHTML.HREF, URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx));
        lanceur.addAttributHTML(AttributBaliseHTML.ON_CLICK, "return false;");
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, "lanceur_media");
        mediaStyle.setLanceurMedia(lanceur);
        this.indicePlayerInitialises++;
    }

    /**
     * Générer l'ID du player
     *
     * @return
     */
    private String getPlayerId() {
        return this.vionneuseKey + String.valueOf(this.indicePlayerInitialises);
    }
}
