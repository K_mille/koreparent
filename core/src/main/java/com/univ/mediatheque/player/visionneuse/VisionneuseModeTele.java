package com.univ.mediatheque.player.visionneuse;

import java.io.Writer;
import java.util.ArrayList;
import java.util.List;

import com.univ.mediatheque.container.AttributBaliseHTML;
import com.univ.mediatheque.container.LanceurMedia;
import com.univ.mediatheque.player.MediaPlayer;
import com.univ.mediatheque.style.MediaStyle;
import com.univ.mediatheque.utils.HTMLUtils;
import com.univ.objetspartages.bean.MediaBean;
import com.univ.objetspartages.util.MediaUtils;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;

/**
 * Visionneuse de player en mode TELE. Cela veut dire qu'il y a un player pour tous les médias affichés.
 *
 * @author Pierre Cosson
 */
public class VisionneuseModeTele implements VisionneusePlayer {

    /** Le mode d'affichage de cette visionneuse. */
    public static final VisionneusePlayerType MODE = VisionneusePlayerType.TELE;

    private static final String HTML_CSS_PLAYER = "visionneuse_tele_player";

    private static final String HTML_CSS_PLAYER_MEDIA_DEBUT = "visionneuse_tele_player_";

    /**
     * Liste des players ayant déjà fait l'objet d'une initilialisation dans la vionneuse.
     */
    private List<String> playerAlreadyInit;

    /**
     * Booleen qui indique que la vionneuse à déjà été initialisée. La vue du player intial.
     */
    private boolean visionneuseAlreadyInit;

    /**
     * La clé de la visionneuse pour cette instance
     */
    private String vionneuseKey;

    private int maxHeight = -1;

    private int maxWidth = -1;

    /**
     * Constructeur.
     */
    public VisionneuseModeTele() {
        this.init();
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.visionneuse.VisionneusePlayer#init()
     */
    @Override
    public void init() {
        this.playerAlreadyInit = new ArrayList<>();
        this.visionneuseAlreadyInit = false;
        this.vionneuseKey = String.valueOf(System.currentTimeMillis());
        this.maxHeight = -1;
        this.maxWidth = -1;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getMaxHeight()
     */
    @Override
    public int getMaxHeight() {
        return this.maxHeight;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getMaxWidth()
     */
    @Override
    public int getMaxWidth() {
        return this.maxWidth;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.player.visionneuse.VisionneusePlayer#getType()
     */
    @Override
    public VisionneusePlayerType getType() {
        return MODE;
    }

    /*
     * (non-Javadoc)
     *
     * @seecom.univ.mediatheque.player.visionneuse.VisionneusePlayer#
     * initialiserVisionneuse(com.univ.utils.ContexteUniv, java.io.Writer,
     * com.univ.mediatheque.player.MediaPlayer)
     */
    @Override
    public void initialiserVisionneuse(final ContexteUniv ctx, final Writer out, final MediaPlayer player) throws Exception {
        // si le player n'a pas déjà été initialisé on l'initialise.
        if (!playerAlreadyInit.contains(this.getVisonneuseKey(player))) {
            // initialisation des variable de taille max
            this.maxHeight = Math.max(this.maxHeight, player.getStyle().getHeight());
            this.maxWidth = Math.max(this.maxWidth, player.getStyle().getWidth());
            final String playerId = this.getJSPlayerId(player);
            out.write("<div id=\"");
            out.write(this.getVisonneuseKey(player));
            out.write("\" class=\"");
            out.write(HTML_CSS_PLAYER);
            out.write(" ");
            out.write(HTML_CSS_PLAYER_MEDIA_DEBUT);
            out.write(player.getType().getMediaType().toLowerCase());
            out.write("\">");
            out.write(player.getInialiserVuePlayer(ctx, playerId));
            out.write("</div>");
            out.write(HTMLUtils.HTML_SCRIPT_JS_OPEN);
            if (!this.visionneuseAlreadyInit) {
                // initialisation de la premiére vue.
                out.write("var ");
                out.write(this.getJSPlayerManagerName());
                out.write(" = new OngletManager();\n");
            }
            out.write(HTMLUtils.genererMediathequeLanceurJSAdd(this.getJSPlayerManagerName() + ".addNewView('" + this.getJSPlayerVueId(player) + "','" + this.getVisonneuseKey(player) + "','')", 1));
            // initialisation de la vue 1
            if (!this.visionneuseAlreadyInit) {
                out.write(HTMLUtils.genererMediathequeLanceurJSAdd(this.getJSPlayerManagerName() + ".initView('" + this.getJSPlayerVueId(player) + "')", 500));
                this.visionneuseAlreadyInit = true;
            }
            out.write(HTMLUtils.HTML_SCRIPT_JS_CLOSE);
            playerAlreadyInit.add(this.getVisonneuseKey(player));
        }
    }

    @Override
    public void setLanceurMedia(final ContexteUniv ctx, final MediaBean media, final MediaPlayer player, final MediaStyle mediaStyle) {
        final String playerId = this.getJSPlayerId(player);
        final LanceurMedia lanceur = player.getLanceurMedia(ctx, media, playerId);
        // ajout de l'action de changement de vue du player
        lanceur.addAttributHTML(AttributBaliseHTML.HREF, URLResolver.getAbsoluteUrl(MediaUtils.getUrlAbsolue(media), ctx));
        lanceur.addAttributHTML(AttributBaliseHTML.CLASS, "lanceur_media");
        mediaStyle.setLanceurMedia(lanceur);
    }

    private String getJSPlayerManagerName() {
        return "playerManager" + this.vionneuseKey;
    }

    private String getJSPlayerId(final MediaPlayer player) {
        return "player_" + this.getVisonneuseKey(player);
    }

    private String getJSPlayerVueId(final MediaPlayer player) {
        return "vue_" + this.getVisonneuseKey(player);
    }

    private String getVisonneuseKey(final MediaPlayer player) {
        return player.getType() + "_" + player.getStyle().getName() + "_" + this.vionneuseKey;
    }
}
