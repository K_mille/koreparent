package com.univ.mediatheque.player.visionneuse;

public enum VisionneusePlayerType {
    /**
     * Mode d'affichage popup : "popup". Les players s'affichent sous forme d'une "popin".
     */
    POPUP("popup"),
    /**
     * Mode d'affichage télé : "tele". Un player pour tous les média de la galerie.
     */
    TELE("tele"),
    /**
     * Mode d'affichage multiple : "multi". Un player par media.
     */
    MULTI("multi");

    /**
     * Le libellé correspondant au mode d'affichage.
     */
    private String libelle = null;

    /**
     * Le libellé du mode d'affichage.
     *
     * @param libelle
     *            le libellé.
     */
    private VisionneusePlayerType(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Récupérer un ENUM en fonction du libellé
     *
     * @param libelle
     *            Le libellé du {@link VisionneusePlayerType} à récupérer.
     * @return {@link VisionneusePlayerType} correspondant au libellé s'il existe sinon NULL.
     */
    public static VisionneusePlayerType getVisionneuseModeAffichage(String libelle) {
        int i;
        VisionneusePlayerType[] values = VisionneusePlayerType.values();
        for (i = 0; i < values.length; i++) {
            if (values[i].isSameLibelle(libelle)) {
                break;
            }
        }
        if (i >= values.length) {
            return null;
        } else {
            return values[i];
        }
    }

    /**
     * Tester un ENUM avec un libellé. C'est le libellé de l'ENUM qui est testé (le test est non case sensitive).
     *
     * @param libelle
     *            Le libellé de test.
     * @return TRUE si le libellé et l'ENUM sont similaire sinon FALSE.
     */
    public boolean isSameLibelle(String libelle) {
        return this.libelle.equalsIgnoreCase(libelle);
    }
}
