package com.univ.mediatheque.playlist.visionneuse;

import java.io.Writer;

import com.univ.mediatheque.playlist.MediaPlaylist;
import com.univ.utils.ContexteUniv;

public interface VisionneusePlaylist {

    /**
     * Récupérer le type de visionneuse de playlist.
     *
     * @return Le type de la visionneuse.
     */
    VisionneusePlaylistType getType();

    /**
     * Récupérer le nom de la visionneuse de play list. Ce nom peut servir d'identifiant pour distinguer entre elles plusieurs visionneuse de même type.
     *
     * @return Le nom de la visionneuse (cette valeur n'est jamais NULL)
     */
    String getName();

    /**
     * Générer le code HTML de la visionneuse de Playlist dans le flux de sortie. Cette fonction permet de générer l'ensemble de la visualisation d'une playlist en HTML et JS.
     *
     * @param ctx
     *            Le contexte de l'application.
     * @param out
     *            Le flux de sortie.
     * @param playList
     *            La playlist à afficher.
     * @throws Exception
     *             Erreur durnat l'écriture dnas le flux de sortie.
     */
    void genererVionneusePlaylist(ContexteUniv ctx, Writer out, MediaPlaylist playList) throws Exception;
}
