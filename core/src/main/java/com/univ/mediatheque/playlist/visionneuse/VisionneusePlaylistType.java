package com.univ.mediatheque.playlist.visionneuse;

import com.univ.mediatheque.player.visionneuse.VisionneusePlayerType;

/**
 * Présente l'ensemble des visionneuses de
 *
 * @author Pierre
 *
 */
public enum VisionneusePlaylistType {
    /**
     * Affichage de la playlist sous forme de page
     */
    PAGINATION("pagination"),
    /**
     * Affichage de la playlist sour forme d'une liste UL-LI.
     */
    DEFAUT("defaut");

    private String libelle;

    /**
     * Constructeur.
     *
     * @param libelle
     *            Le libellé de l'enumération.
     */
    private VisionneusePlaylistType(String libelle) {
        this.libelle = libelle;
    }

    /**
     * Récupérer un ENUM en fonction du libellé
     *
     * @param libelle
     *            Le libellé du {@link VisionneusePlayerType} à récupérer.
     * @return {@link VisionneusePlayerType} correspondant au libellé s'il existe sinon NULL.
     */
    public static VisionneusePlaylistType getVisionneuseModeAffichage(String libelle) {
        int i;
        VisionneusePlaylistType[] values = VisionneusePlaylistType.values();
        for (i = 0; i < values.length; i++) {
            if (values[i].isSameLibelle(libelle)) {
                break;
            }
        }
        if (i >= values.length) {
            return null;
        } else {
            return values[i];
        }
    }

    /**
     * Tester un ENUM avec un libellé. C'est le libellé de l'ENUM qui est testé (le test est non case sensitive).
     *
     * @param libelle
     *            Le libellé de test.
     * @return TRUE si le libellé et l'ENUM sont similaire sinon FALSE.
     */
    public boolean isSameLibelle(String libelle) {
        return this.libelle.equalsIgnoreCase(libelle);
    }
}
