package com.univ.mediatheque.playlist.visionneuse.pagination;

import java.io.Writer;

import com.univ.utils.ContexteUniv;

/**
 * Contexte d'une visionneuse avec pagination.<br/>
 * JTF : mediatheque.imagePath
 *
 * @author Pierre Cosson
 */
public class VisionneusePlaylistPaginationContexte {

    /** The nombre ligne. */
    private int nombreLigne;

    /** The nombre colonne. */
    private int nombreColonne;

    /** The key. */
    private String key;

    /** The ctx. */
    private ContexteUniv ctx;

    /** The out. */
    private Writer out;

    /** The nombre page. */
    private int nombrePage;

    /** The nombre element. */
    private int nombreElement;

    /**
     * Constructeur.
     *
     * @param ctx
     *            the ctx
     * @param out
     *            the out
     * @param style
     *            the style
     * @param nombreElement
     *            the nombre element
     */
    public VisionneusePlaylistPaginationContexte(ContexteUniv ctx, Writer out, int nombreElement, int nbLigne, int nbColonne) {
        // permet de rendre unique les elements (évite les conflit entre
        // deux éléments nommés pareils)
        this.key = Long.toString(System.currentTimeMillis());
        this.nombreElement = nombreElement;
        this.ctx = ctx;
        this.out = out;
        this.nombreColonne = this.getNombreColonne(nbColonne, nbLigne, nombreElement);
        this.nombreLigne = this.getNombreLigne(nbColonne, nbLigne, nombreElement);
        this.nombrePage = this.calculNombrePage(nbColonne, nbLigne, nombreElement);
    }

    /**
     * Calculer le nombre de page qui devra être produite pour afficher l'ensemble des médias. Si une valeur concernant le nombre de colonne ou de ligne est à 0 alors le nombre de
     * page sera 0.
     *
     * @param nbColonne
     *            Le nombre de colonnes à afficher.
     * @param nbLigne
     *            Le nombre de lignes à afficher.
     * @param nombreElement
     *            Le nombre de média à afficher.
     *
     * @return Le nombre de pages.
     */
    private int calculNombrePage(int nbColonne, int nbLigne, int nombreElement) {
        if (nbColonne != 0 && nbLigne != 0) {
            // si les nombre de colonnes et de lignes sont définis alors il y
            // aura plus d'une page
            int nbElementPerPage = this.getNombreColonne(nbColonne, nbLigne, nombreElement) * this.getNombreLigne(nbColonne, nbLigne, nombreElement);
            // calcul du nombre de page
            int nbPage = nombreElement / nbElementPerPage;
            // si on a un reste on ajoute une page
            if (nombreElement % nbElementPerPage > 0) {
                nbPage++;
            }
            return nbPage;
        } else if (nbColonne != 0 || nbLigne != 0) {
            // une page qui affiche tous les éléments
            return 1;
        } else {
            // pas de pagination
            return 0;
        }
    }

    /**
     * Calcule le nombre de colonnes en tenant compte du nombre de lignes et d'éléments. Si le nombre de colonnes est égal à 0 alors on retourne le nombre de colonnes qui permet
     * d'afficher tous les éléments (en prennat pour référence le nombre de lignes) sinon le nombre de colonnes.
     *
     * @param nbColonne
     *            Le nombre de colonnes à afficher.
     * @param nbLigne
     *            Le nombre de lignes à afficher.
     * @param nombreElement
     *            Le nombre d'élements total à afficher.
     *
     * @return Le nombre de colonnes.
     */
    private int getNombreColonne(int nbColonne, int nbLigne, int nombreElement) {
        if (nbColonne > 0) {
            return nbColonne;
        } else if (nbLigne > 0) {
            // le nombre de colonnes calculé pour afficher tous les élements
            // sur la même page
            int nbColonneNew = nombreElement / nbLigne;
            if (nombreElement % nbLigne > 0) {
                nbColonneNew++;
            }
            return nbColonneNew;
        } else {
            return 0;
        }
    }

    /**
     * Calcule le nombre de lignes de la même maniére qu'est calculer le nombre de colonnes.
     *
     * @param nbColonne
     *            Le nombre de colonnes à afficher.
     * @param nbLigne
     *            Le nombre de lignes à afficher.
     * @param nombreElement
     *            Le nombre d'élements total à afficher.
     *
     * @return Le nombre de colonnes.
     */
    private int getNombreLigne(int nbColonne, int nbLigne, int nombreElement) {
        if (nbLigne > 0) {
            return nbLigne;
        } else if (nbColonne > 0) {
            // le nombre de ligne est calculé pour afficher tous les élements
            // sur la même page
            int nbLigneNew = nombreElement / nbColonne;
            if (nombreElement % nbColonne > 0) {
                nbLigneNew++;
            }
            return nbLigneNew;
        } else {
            return 0;
        }
    }

    /**
     * Gets the nombre ligne.
     *
     * @return the nombreLigne
     */
    public int getNombreLigne() {
        return nombreLigne;
    }

    /**
     * Gets the nombre colonne.
     *
     * @return the nombreColonne
     */
    public int getNombreColonne() {
        return nombreColonne;
    }

    /**
     * Gets the key.
     *
     * @return the key
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the ctx.
     *
     * @return the ctx
     */
    public ContexteUniv getCtx() {
        return ctx;
    }

    /**
     * Gets the out.
     *
     * @return the out
     */
    public Writer getOut() {
        return out;
    }

    /**
     * Gets the nombre page.
     *
     * @return the nombrePage
     */
    public int getNombrePage() {
        return nombrePage;
    }

    /**
     * Gets the nombre element.
     *
     * @return the nombreElement
     */
    public int getNombreElement() {
        return nombreElement;
    }

    /**
     * Afficher boutons navigation.
     *
     * @return true, if successful
     */
    public boolean afficherBoutonsNavigation() {
        return this.nombrePage > 1;
    }
}
