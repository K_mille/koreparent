package com.univ.mediatheque.style;

import java.util.Collection;

public interface PlayerStyle {

    String getName();

    int getWidth();

    int getHeight();

    /**
     * Il est possible dans le style de surcharger via cette fonction l'URL du player distant du player qui utilisera ce style.
     *
     * @return retur l'URL du nouveau player distant ou NULL.
     */
    String getSpecificUrlPlayerDistant();

    /**
     * Récupérer la liste des noms de propriétés d'initialisations du player. Par exemple, pourront être défini pour certain player : autostart, bgcolor, ...
     *
     * @return L'ensemble des propriété d'initialisation.
     */
    Collection<String> getListeNomsProprieteInitialisation();

    /**
     * Récupérer la valeur d'une propriété d'initialisation.
     *
     * @param nomPropriete
     *            Le nom de la propriété dont on souhaite connaitre la valeur.
     * @return La valeur ou NULL si la propriété n'est pas définie.
     */
    String getValeurProprieteInitialisation(String nomPropriete);
}
