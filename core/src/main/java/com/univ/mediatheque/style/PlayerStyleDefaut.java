package com.univ.mediatheque.style;

import java.util.Collection;
import java.util.Map;

public class PlayerStyleDefaut implements PlayerStyle {

    private int width;

    private int height;

    private String name;

    private Map<String, String> listeProprieteInitialisation;

    private String specificUrlPlayerDistant;

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.style.PlayerStyle#getHeight()
     */
    @Override
    public int getHeight() {
        return this.height;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.PlayerStyle#getListeNomsProprieteInitialisation
     * ()
     */
    @Override
    public Collection<String> getListeNomsProprieteInitialisation() {
        if (listeProprieteInitialisation == null) {
            return null;
        } else {
            return this.listeProprieteInitialisation.keySet();
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.style.PlayerStyle#getName()
     */
    @Override
    public String getName() {
        return this.name;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.style.PlayerStyle#getSpecificUrlPlayerDistant()
     */
    @Override
    public String getSpecificUrlPlayerDistant() {
        return specificUrlPlayerDistant;
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * com.univ.mediatheque.style.PlayerStyle#getValeurProprieteInitialisation
     * (java.lang.String)
     */
    @Override
    public String getValeurProprieteInitialisation(String nomPropriete) {
        if (listeProprieteInitialisation == null) {
            return null;
        } else {
            return this.listeProprieteInitialisation.get(nomPropriete);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.mediatheque.style.PlayerStyle#getWidth()
     */
    @Override
    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setListeProprieteInitialisation(Map<String, String> listeProprieteInitialisation) {
        this.listeProprieteInitialisation = listeProprieteInitialisation;
    }

    public void setSpecificUrlPlayerDistant(String specificUrlPlayerDistant) {
        this.specificUrlPlayerDistant = specificUrlPlayerDistant;
    }
}
