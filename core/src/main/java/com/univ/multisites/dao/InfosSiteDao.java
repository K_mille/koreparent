package com.univ.multisites.dao;

import java.util.Collection;

import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.univ.multisites.InfosSite;

/**
 * Gestion des accés à la source de données qui contient les {@link InfosSite}
 *
 * @author pierre.cosson
 *
 */
public interface InfosSiteDao {

    /**
     * Récupérer un {@link InfosSite} en spécifiant son code (appelé aussi alias).
     *
     * @param code
     *            Code du {@link InfosSite} à récupérer. Ce paramétre était aussi appelé alias.
     * @return L'objet {@link InfosSite} chargé correspondant au code.
     * @throws Exception <ul>
     *            <li>Exception {@link ErreurDonneeNonTrouve} : le code ne correspond à aucun site</li>
     *            <li>Autres erreurs durant le chargement du site</li>
     *            </ul>
     */
    InfosSite getInfosSite(String code) throws Exception;

    /**
     * Récupérer tous les {@link InfosSite} gérés par la source de données.
     *
     * @return La liste des {@link InfosSite} contenus par la source de données. Cette liste peut être vide.
     * @throws Exception
     *             Erreur durant les traitements.
     */
    Collection<InfosSite> getListeInfosSites() throws Exception;

    /**
     * Crééer un {@link InfosSite} <br/>
     * <br/>
     * <br/>
     * REMARQUE : l'idenfiant du bean sera mis à jour par la source de données.
     *
     * @param infosSite
     *            l'élément à créer dans la source de données.
     * @throws Exception
     *             <ul>
     *             <li>Erreurs d'écriture dans la source de données</li>
     *             </ul>
     */
    void creer(InfosSite infosSite) throws Exception;

    /**
     * Mettre à jour la {@link InfosSite} en source de données.
     *
     * @param infosSite
     *            L' {@link InfosSite} à mettre à jour avec ses données à jour.
     * @throws Exception
     *             <ul>
     *             <li> {@link ErreurDonneeNonTrouve} : aucun {@link InfosSite} ne correspond à l'identifiant du bean passé en paramètre (pas de modification possible).</li>
     *             <li>Erreurs d'écriture dans la source de données</li>
     *             </ul>
     */
    void miseAJour(InfosSite infosSite) throws Exception;

    /**
     * Supprimer un {@link InfosSite} via son code.
     *
     * @param code
     *            code de l'{@link InfosSite} à supprimer.
     * @throws Exception
     *             <ul>
     *             <li>Erreurs d'écriture dans la source de données</li>
     *             </ul>
     */
    void supprimer(String code) throws Exception;
}
