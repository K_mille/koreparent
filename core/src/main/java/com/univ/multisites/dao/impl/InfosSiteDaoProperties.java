package com.univ.multisites.dao.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.configuration2.Configuration;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.builder.fluent.Parameters;
import org.apache.commons.configuration2.convert.DefaultListDelimiterHandler;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.commons.configuration2.sync.LockMode;
import org.apache.commons.configuration2.sync.ReadWriteSynchronizer;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.jsbsoft.jtf.exception.ErreurDonneeNonTrouve;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.kosmos.usinesite.exception.ErreursSaisieInfosSite;
import com.kosmos.usinesite.utils.InfosSiteHelper;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.multisites.dao.InfosSiteDao;
import com.univ.multisites.helper.InfosSitePropertiesHelper;
import com.univ.multisites.helper.InfosSitePropertiesHelper.FinNomProprieteSite;

import static com.univ.multisites.InfosSite.MODEREECRITURERUBRIQUE_DEFAUT;
import static com.univ.multisites.InfosSite.NIVEAUMAXREECRITURERUBRIQUE_DEFAUT;
import static com.univ.multisites.InfosSite.NIVEAUMINREECRITURERUBRIQUE_DEFAUT;
import static com.univ.multisites.InfosSite.NON_INDEXE_DEFAUT;
import static com.univ.multisites.InfosSite.RESTRICTION_DEFAUT;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_ACTIF;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_ALIAS;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HOST;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_HTTPS_PORT;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_INTITULE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_JSP_FO;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_NON_INDEXE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_PORT;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_PRINCIPAL;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_RESTRICTION;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_RUBRIQUE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_SECURE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_SSO;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_TEMPLATE;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.FIN_PROPERTIES_SITE_URL_ACCUEIL;
import static com.univ.multisites.helper.InfosSitePropertiesHelper.SLASH;

/**
 * DAO qui permet de LIRE (et uniquement) les {@link InfosSite} depuis les proerties chargée dans l'application.<br/>
 * <br/>
 *
 * <br/>
 * <strong>INFO sur le format des properties</strong><br/>
 * Déclaration du site principal :
 * <ul>
 * <li>site.principal=[ID]</li>
 * </ul>
 * Paramétrage pour chaque site dans le jtf :
 * <ul>
 * <li>site.[ID].intitule=[INTITULE]</li>
 * <li>
 * site.[ID].host=[HTTP_HOSTNAME]</li>
 * <li>
 * site.[ID].port=[HTTP_PORT]</li>
 * <li>
 * site.[ID].rubrique=[CODE_RUBRIQUE]</li>
 * <li>
 * site.[ID].alias=[ALIAS]</li>
 * </ul>
 * où :
 * <ul>
 * <li>[ID] est l'identifiant du site</li>
 * <li>
 * [INTITULE] est le nom du site</li>
 * <li>
 * [HTTP_HOSTNAME] est le nom du virtual host pour l'appli en http</li>
 * <li>
 * [HTTP_PORT] est le numéro de port pour l'appli en http (paramètre optionnel, par défaut = 80)</li>
 * <li>
 * [CODE_RUBRIQUE] est le code de la rubrique mère du site (paramètre optionnel, par défaut = "")</li>
 * <li>
 * [ALIAS] est la liste des différents alias du host principal séparés par des points virgules</li>
 * </ul>
 *
 * @author pierre.cosson
 *
 */
public class InfosSiteDaoProperties implements InfosSiteDao {

    private static final Logger LOG = LoggerFactory.getLogger(InfosSiteDaoProperties.class);

    private static final String PROPERTIES = ".properties";

    private static FileBasedConfigurationBuilder<PropertiesConfiguration> getBuilder() {
        return new FileBasedConfigurationBuilder<>(PropertiesConfiguration.class)
            .configure(new Parameters().properties()
                .setBasePath( InfosSitePropertiesHelper.getCheminDossiersProperties() )
                .setEncoding( CharEncoding.DEFAULT )
                .setListDelimiterHandler(new DefaultListDelimiterHandler( InfosSitePropertiesHelper.VALEUR_PROPERTIE_LISTE_DELIMITEUR)
            )
            .setThrowExceptionOnMissing(false)
            .setSynchronizer(new ReadWriteSynchronizer())
        );
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getInfosSite(java.lang.String)
     */
    @SuppressWarnings("deprecation")
    @Override
    public InfosSite getInfosSite(final String code) throws Exception {
        final InfosSiteImpl infosSite = new InfosSiteImpl();
        final FileBasedConfigurationBuilder<PropertiesConfiguration> builder = getBuilder();
        PropertiesConfiguration propsConfig;
        try {
            propsConfig = builder.getConfiguration();
            builder.getFileHandler().setFileName(code + PROPERTIES);
            builder.getFileHandler().load();
        } catch (ConfigurationException ce) {
            //On log exceptionnellement l'exception car en cas de fichier corrompu, il ne sera pas possible de le savoir.
            //L'erreur ErreurDonneeNonTrouve est fonctionnelement valable (dans verifierUnicite notamment)
            LOG.info(String.format("Erreur lors de la lecture de la configuration du site '%1$s'",code),ce);
            throw new ErreurDonneeNonTrouve("Aucun site ne correspond au code " + code);
        }

        final String proprieteSite = DEBUT_PROPERTIES_SITE + code;
        final String intitule = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_INTITULE);
        if (StringUtils.isNotEmpty(intitule)) {
            infosSite.setIntitule(intitule);
        }
        final String httpHost = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_HOST);
        if (StringUtils.isNotEmpty(httpHost)) {
            infosSite.setHttpHostname(httpHost);
        }
        final List<String> listeAliasHostsSite = propsConfig.getList(String.class, proprieteSite + FIN_PROPERTIES_SITE_ALIAS);
        if (CollectionUtils.isNotEmpty(listeAliasHostsSite)) {
            infosSite.setListeHostAlias(new HashSet<>(listeAliasHostsSite));
        }
        final String siteRubrique = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_RUBRIQUE);
        if (StringUtils.isNotEmpty(siteRubrique)) {
            infosSite.setCodeRubrique(siteRubrique);
        } else {
            infosSite.setCodeRubrique(StringUtils.EMPTY);
        }
        final String siteURLAccueil = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_URL_ACCUEIL);
        if (StringUtils.isNotEmpty(siteURLAccueil)) {
            infosSite.setUrlAccueil(siteURLAccueil);
        }
        final String jspFO = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_JSP_FO);
        if (StringUtils.isNotEmpty(jspFO)) {
            infosSite.setJspFo(StringUtils.removeEnd(jspFO, SLASH));
        }
        final String codeTemplate = propsConfig.getString(proprieteSite + FIN_PROPERTIES_SITE_TEMPLATE);
        if (StringUtils.isNotEmpty(codeTemplate)) {
            infosSite.setCodeTemplate(codeTemplate);
        }
        infosSite.setAlias(code);
        infosSite.setHttpPort(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_PORT, InfosSiteHelper.getHttpPort()));
        infosSite.setHttpsPort(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_HTTPS_PORT, InfosSiteHelper.getHttpsPort()));
        infosSite.setNiveauMinReecritureRubrique(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN, NIVEAUMINREECRITURERUBRIQUE_DEFAUT));
        infosSite.setNiveauMaxReecritureRubrique(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX, NIVEAUMAXREECRITURERUBRIQUE_DEFAUT));
        infosSite.setModeReecritureRubrique(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE, MODEREECRITURERUBRIQUE_DEFAUT));
        infosSite.setRestriction(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_RESTRICTION, RESTRICTION_DEFAUT));
        infosSite.setNonIndexe(propsConfig.getInt(proprieteSite + FIN_PROPERTIES_SITE_NON_INDEXE, NON_INDEXE_DEFAUT));
        infosSite.setSso(propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_SSO));
        infosSite.setSitePrincipal(propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_PRINCIPAL));
        infosSite.setActif(propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_ACTIF));
        infosSite.setSecure(getSecureProperty(propsConfig, proprieteSite));
        setListeProprietesComplementairesDansInfosSite(infosSite, propsConfig);
        return infosSite;
    }

    /**
     * Vérifie si le site est bien sécurisé. Si la propriété n'y est pas, il va chercher à récupérer les anciennes propriétés (ssl_mode ou bo.ssl_mode)
     * @param propsConfig la config du site dont on veut récupérer les infos
     * @param proprieteSite le début de la clé permettant de récupérer la propriété
     * @return vrai si le site est en https
     */
    private Boolean getSecureProperty(PropertiesConfiguration propsConfig, String proprieteSite) {
        Boolean result;
        if (propsConfig.containsKey(proprieteSite + FIN_PROPERTIES_SITE_SECURE)) {
            result = propsConfig.getBoolean(proprieteSite + FIN_PROPERTIES_SITE_SECURE);
        } else {
            result = propsConfig.getInt(proprieteSite + ".ssl_mode", 0) > 0 ||
                propsConfig.getInt(proprieteSite + ".bo.ssl_mode", 0) > 0;
        }
        return result;
    }

    /*
     * (non-Javadoc)
     *
     * @see com.univ.multisites.dao.InfosSiteDao#getListeInfosSites()
     */
    @Override
    public Collection<InfosSite> getListeInfosSites() throws Exception {
        final ArrayList<InfosSite> infosSites = new ArrayList<>();
        final Iterator<File> tousLesSites = FileUtils.iterateFiles(new File(InfosSitePropertiesHelper.getCheminDossiersProperties()), new String[] {"properties"}, Boolean.FALSE);
        while (tousLesSites.hasNext()) {
            final File fichierCourant = tousLesSites.next();
            infosSites.add(getInfosSite(FilenameUtils.getBaseName(fichierCourant.getName())));
        }
        return infosSites;
    }

    @Override
    public void creer(final InfosSite infosSite) throws Exception {
        final File fichierUAS = new File(InfosSitePropertiesHelper.getCheminDossiersProperties() + infosSite.getAlias() + PROPERTIES);
        if (fichierUAS.exists()) {
            throw new ErreursSaisieInfosSite("le site existe déjà");
        }
        sauvegardeProperties(infosSite);
    }

    @Override
    public void miseAJour(final InfosSite infosSite) throws Exception {
        sauvegardeProperties(infosSite);
    }

    @Override
    public void supprimer(final String code) throws Exception {
        FileBasedConfigurationBuilder<PropertiesConfiguration> builder = getBuilder();
        Configuration config = null;
        try {
            builder.getFileHandler().setFileName(code + PROPERTIES);
            config = builder.getConfiguration();
            config.lock(LockMode.WRITE);
            boolean result = builder.getFileHandler().getFile().delete();
            if (!result) {
                LOG.warn("Impossible de supprimer le fichier {}", builder.getFileHandler().getFile().getAbsolutePath());
            }
        } catch (ConfigurationException ce) {
            LOG.warn("Erreur lors de la lecture de la configuration du site {} : {}",code,ce.getMessage());
        } finally {
            if ( config!=null ) {
                config.unlock(LockMode.WRITE);
            }
        }
    }

    /**
     * Insérer dans l'infosSite toutes les propriétés en rapport avec le site.
     *
     * @param infosSite
     *              L'infos site
     * @param propsConfig
     *              La configuration du fichier properties
     */
    private void setListeProprietesComplementairesDansInfosSite(final InfosSiteImpl infosSite, final PropertiesConfiguration propsConfig) {
        final String debutCleProprieteSite = InfosSitePropertiesHelper.DEBUT_PROPERTIES_SITE + infosSite.getAlias() + ".";
        @SuppressWarnings("unchecked")
        final Iterator<String> ensembleProprietes = propsConfig.getKeys();
        while (ensembleProprietes.hasNext()) {
            final String clePropriete = ensembleProprietes.next();
            if (!StringUtils.startsWithIgnoreCase(clePropriete, debutCleProprieteSite)) {
                continue;
            }
            // site.code_site.cleProprieteSite === DEVIENT ==> cleProprieteSite
            // (il s'agit de la proriete site)
            final String cleProprieteSite = StringUtils.removeStartIgnoreCase(clePropriete, debutCleProprieteSite);
            if (StringUtils.isNotEmpty(cleProprieteSite) && !FinNomProprieteSite.isProprieteSite(cleProprieteSite)) {
                infosSite.putProperty(cleProprieteSite, propsConfig.getProperty(clePropriete));
            }
        }
    }

    /**
     * On supprime le warning deprecation car on garde ce paramètre pour la retro compatibilité...
     *
     * @param infosSite
     *              L' infos site
     * @throws ErreurApplicative
     *              Erreur applicative en cas d'erreur lors de la sauvegarde du fichier de properties
     */
    @SuppressWarnings("deprecation")
    private void sauvegardeProperties(final InfosSite infosSite) throws ErreurApplicative {
        PropertiesConfiguration propsConfig;
        FileBasedConfigurationBuilder<PropertiesConfiguration> builder = getBuilder();
        try {
            propsConfig = builder.getConfiguration();
            final String proprieteSite = DEBUT_PROPERTIES_SITE + infosSite.getAlias();
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_INTITULE, infosSite.getIntitule());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_HOST, infosSite.getHttpHostname());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_ALIAS, infosSite.getListeHostAlias());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_RUBRIQUE, infosSite.getCodeRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MIN, infosSite.getNiveauMinReecritureRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MAX, infosSite.getNiveauMaxReecritureRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_REECRITURE_RUBRIQUE_MODE, infosSite.getModeReecritureRubrique());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_URL_ACCUEIL, infosSite.getUrlAccueil());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_RESTRICTION, infosSite.getRestriction());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_NON_INDEXE, infosSite.getNonIndexe());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_SSO, infosSite.isSso());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_JSP_FO, infosSite.getJspFo());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_TEMPLATE, infosSite.getCodeTemplate());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_PRINCIPAL, infosSite.isSitePrincipal());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_ACTIF, infosSite.isActif());
            propsConfig.setProperty(proprieteSite + FIN_PROPERTIES_SITE_SECURE, infosSite.isSecure());
            setListeProprietesComplementairesDansProperties(infosSite, propsConfig);
            //Le builder est associé à un RWSynchronizer : plus besoin du lock manuel
            builder.getFileHandler().setFileName(infosSite.getAlias() + PROPERTIES);
            builder.save();
        } catch (final ConfigurationException e) {
            LOG.error("unable to save the given properties", e);
            throw new ErreursSaisieInfosSite("impossible de sauvegarder le site courant" + infosSite.getAlias());
        }
    }

    private void setListeProprietesComplementairesDansProperties(final InfosSite infosSite, final PropertiesConfiguration propsConfig) {
        final String prefixeProprieteSite = DEBUT_PROPERTIES_SITE + infosSite.getAlias() + ".";
        for (final Map.Entry<String, Object> propriete : infosSite.getProprietesComplementaires().entrySet()) {
            if(propriete.getValue() instanceof String){
                String value = StringUtils.replace((String)propriete.getValue(), String.valueOf(InfosSitePropertiesHelper.VALEUR_PROPERTIE_LISTE_DELIMITEUR), "\\"+InfosSitePropertiesHelper.VALEUR_PROPERTIE_LISTE_DELIMITEUR);
                propsConfig.setProperty(prefixeProprieteSite + propriete.getKey(), value );
            }else {
                propsConfig.setProperty(prefixeProprieteSite + propriete.getKey(), propriete.getValue());
            }
        }
    }
}
