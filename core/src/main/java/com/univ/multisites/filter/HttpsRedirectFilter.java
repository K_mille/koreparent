package com.univ.multisites.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.utils.URLResolver;

public class HttpsRedirectFilter implements Filter {

    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
        // on a rien à init...
    }

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest && response instanceof HttpServletResponse) {
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            InfosSite site = serviceInfosSite.getSiteByHost(request.getServerName());
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            if (site.isSecure() && !"https".equals(URLResolver.getRequestScheme(httpRequest))) {
                String url = httpRequest.getRequestURI();
                String queryString = httpRequest.getQueryString();
                if (StringUtils.isNotEmpty(queryString)) {
                    url += "?" + queryString;
                }
                ((HttpServletResponse)response).sendRedirect(URLResolver.getAbsoluteUrl(url, site));
            } else {
                filterChain.doFilter(request, response);
            }
        } else {
            throw new ServletException("HttpsRedirectFilter ne supporte que les requêtes HTTP");
        }
    }

    @Override
    public void destroy() {
        // et on a rien à destroy...
    }
}
