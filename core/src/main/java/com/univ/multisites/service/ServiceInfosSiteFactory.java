package com.univ.multisites.service;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.multisites.InfosSite;

/**
 * Factory à utiliser pour récupérer le {@link ServiceInfosSite} déclaré dans l'application.
 *
 * @author pierre.cosson
 *
 */
public class ServiceInfosSiteFactory {

    public static final String ID_BEAN = "serviceInfosSiteFactory";

    private ServiceInfosSite serviceInfosSiteInstance = null;

    /**
     * Récupérer le singleton de la factory.
     *
     * @return
     */
    private static ServiceInfosSiteFactory getInstance() {
        return (ServiceInfosSiteFactory) ApplicationContextManager.getCoreContextBean(ID_BEAN);
    }

    /**
     * Charger l'instance du service {@link ServiceInfosSite} permettant de lister les {@link InfosSite} de l'application.
     *
     * @return l'instance (le singleton)
     */
    public static ServiceInfosSite getServiceInfosSite() {
        return getInstance().serviceInfosSiteInstance;
    }

    public void setServiceInfosSite(ServiceInfosSite serviceInfosSite) {
        serviceInfosSiteInstance = serviceInfosSite;
    }
}
