package com.univ.objetspartages.bean;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 * Created on 08/04/15.
 */
public class AbstractRestrictionFicheBean extends AbstractFicheBean implements Serializable {

    private static final long serialVersionUID = -6961404388447404006L;

    /** The diffusion public vise. */
    protected String diffusionPublicVise = StringUtils.EMPTY;

    /** The diffusion mode restriction. */
    protected String diffusionModeRestriction = StringUtils.EMPTY;

    /** The diffusion public vise restriction. */
    protected String diffusionPublicViseRestriction = StringUtils.EMPTY;

    public void init(AbstractRestrictionFicheBean bean) {
        super.init(bean);
        this.diffusionPublicVise = bean.diffusionPublicVise;
        this.diffusionModeRestriction = bean.diffusionModeRestriction;
        this.diffusionPublicViseRestriction = bean.diffusionPublicViseRestriction;
    }

    public String getDiffusionPublicViseRestriction() {
        return diffusionPublicViseRestriction;
    }

    public void setDiffusionPublicViseRestriction(String diffusionPublicViseRestriction) {
        this.diffusionPublicViseRestriction = diffusionPublicViseRestriction;
    }

    public String getDiffusionModeRestriction() {
        return diffusionModeRestriction;
    }

    public void setDiffusionModeRestriction(String diffusionModeRestriction) {
        this.diffusionModeRestriction = diffusionModeRestriction;
    }

    public String getDiffusionPublicVise() {
        return diffusionPublicVise;
    }

    public void setDiffusionPublicVise(String diffusionPublicVise) {
        this.diffusionPublicVise = diffusionPublicVise;
    }
}
