package com.univ.objetspartages.bean;

import java.util.Date;
import java.util.UUID;

/**
 * Created on 20/04/15.
 */
public class PassRequestBean extends AbstractPersistenceBean{

    private static final long serialVersionUID = 5639713279150993833L;

    /** The code. */
    private String code = null;

    /** The email. */
    private String email = null;

    /** The id. */
    private UUID uuid = null;

    /** The date demande. */
    private Date dateDemande = null;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Date getDateDemande() {
        return dateDemande;
    }

    public void setDateDemande(Date dateDemande) {
        this.dateDemande = dateDemande;
    }
}
