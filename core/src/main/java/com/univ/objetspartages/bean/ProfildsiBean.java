package com.univ.objetspartages.bean;

import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;

/**
 * Bean contenant les informations des profils du produits
 */
public class ProfildsiBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -2697737884328934702L;

    private String code = null;

    private String libelle = null;

    private String codeRubriqueAccueil = null;

    private String codeRattachement = null;

    private String roles = StringUtils.EMPTY;

    private Collection<String> groupes = new ArrayList<>();

    public Long getIdProfildsi() {
        return getId();
    }

    public String getCode() {
        return code;
    }

    public String getLibelle() {
        return libelle;
    }

    public String getCodeRubriqueAccueil() {
        return codeRubriqueAccueil;
    }

    public String getCodeRattachement() {
        return codeRattachement;
    }

    public void setIdProfildsi(final Long idProfildsi) {
        setId(idProfildsi);
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public void setLibelle(final String libelle) {
        this.libelle = libelle;
    }

    public void setCodeRubriqueAccueil(final String codePageAccueil) {
        this.codeRubriqueAccueil = codePageAccueil;
    }

    public void setCodeRattachement(final String codeRattachement) {
        this.codeRattachement = codeRattachement;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Collection<String> getGroupes() {
        return groupes;
    }

    public void setGroupes(Collection<String> groupes) {
        this.groupes = groupes;
    }
}
