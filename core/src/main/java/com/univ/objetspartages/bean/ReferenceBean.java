package com.univ.objetspartages.bean;

import com.univ.objetspartages.om.EtatFiche;

public class ReferenceBean {

    // Stockage titre/type/etat/langue
    private String titre;

    private String type;

    private EtatFiche etat;

    private String langue;

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public EtatFiche getEtat() {
        return etat;
    }

    public void setEtat(EtatFiche etat) {
        this.etat = etat;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(String langue) {
        this.langue = langue;
    }
}
