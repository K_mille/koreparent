package com.univ.objetspartages.cache;

import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;

import com.univ.objetspartages.bean.RoleBean;
import com.univ.objetspartages.om.InfosRole;
import com.univ.objetspartages.services.ServiceRole;

public class CacheRoleManager {

    public static final String ID_BEAN = "cacheRoleManager";

    private static final Logger LOGGER = LoggerFactory.getLogger(CacheRoleManager.class);

    private ServiceRole serviceRole;

    public void setServiceRole(final ServiceRole serviceRole) {
        this.serviceRole = serviceRole;
    }

    @Cacheable(value = "CacheRoleManager.getListeRoles")
    public HashMap<String, InfosRole> getListeRoles() {
        final HashMap<String, InfosRole> listeRoles = new HashMap<>();
        List<RoleBean> allRoles = serviceRole.getAll();
        for (RoleBean role : allRoles) {
            final InfosRole infosRole = new InfosRole(role.getCode(), role.getLibelle(), role.getPerimetre(), role.getPermissions());
            listeRoles.put(infosRole.getCode(), infosRole);
        }
        LOGGER.info("Chargement de " + allRoles.size() + " roles OK");
        return listeRoles;
    }

    @CacheEvict(value = "CacheRoleManager.getListeRoles")
    public void flush() {}
}
