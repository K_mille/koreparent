package com.univ.objetspartages.dao;

import java.util.List;

import com.jsbsoft.jtf.datasource.dao.CommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.univ.objetspartages.bean.AbstractFicheBean;

/**
 * Interface des DAO de fiche, il permet de rajouter les méthodes utiliser sur l'ensemble des fiches
 * @param <T> Un bean contenant les informations d'une fiche, il doit ếtendre {@link AbstractFicheBean} pour être valide
 */
public interface FicheDAO<T extends AbstractFicheBean> extends CommonDAO<T> {

    /**
     * Renvoie la liste des fiches présentes dans la rubrique en paramètre
     *
     * @param codeRubrique Le code de la rubrique
     * @param langue La langue demandée
     * @return Une liste de fiches
     * @throws DataSourceException
     */
    List<T> selectParCodeRubrique(String codeRubrique, String langue) throws DataSourceException;

    /**
     * Renvoie une fiche à partir de la clé code, langue, état
     *
     * @param code Le code de la fiche
     * @param langue La langue de la fiche
     * @param etat L'état de la fiche
     * @return Une fiche ou une liste de fiches
     * @throws DataSourceException
     */
    List<T> selectCodeLangueEtat(String code, String langue, String etat) throws DataSourceException;
}
