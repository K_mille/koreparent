package com.univ.objetspartages.dao.impl;

import com.univ.objetspartages.bean.ArticleBean;
import com.univ.objetspartages.dao.AbstractFicheDAO;

public class ArticleDAO extends AbstractFicheDAO<ArticleBean> {

    public ArticleDAO() {
        tableName = "ARTICLE";
    }

}
