package com.univ.objetspartages.dao.impl;

import com.univ.objetspartages.bean.PagelibreBean;
import com.univ.objetspartages.dao.AbstractFicheDAO;

public class PagelibreDAO extends AbstractFicheDAO<PagelibreBean> {

    public PagelibreDAO() {
        tableName = "PAGELIBRE";
    }

}
