package com.univ.objetspartages.dao.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.BeanPropertyRowMapper;

import com.jsbsoft.jtf.datasource.naming.ColumnNamingStrategy;
import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Created on 22/09/15.
 */
public class CustomIdBeanPropertyRowMapper<T extends PersistenceBean> extends BeanPropertyRowMapper<T> {

    private ColumnNamingStrategy columnNamingStrategy;

    public CustomIdBeanPropertyRowMapper(Class<T> clazz, ColumnNamingStrategy columnNamingStrategy) {
        super(clazz);
        this.columnNamingStrategy = columnNamingStrategy;
    }

    @Override
    public T mapRow(ResultSet rs, int rowNumber) throws SQLException {
        final T mappedBean = super.mapRow(rs, rowNumber);
        mappedBean.setId(rs.getLong(columnNamingStrategy.getColumnId(rs.getMetaData().getTableName(1))));
        return mappedBean;
    }
}
