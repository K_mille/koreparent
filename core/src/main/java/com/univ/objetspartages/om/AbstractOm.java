package com.univ.objetspartages.om;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractLegacyDAO;
import com.jsbsoft.jtf.datasource.manager.DataSourceDAOManager;
import com.univ.objetspartages.bean.PersistenceBean;

/**
 * Classe permettant de gérer les anciennes méthodes des objets FicheUniv et autre Rubrique devant gérer l'accès au données (add/update/delete/select/retrieve)
 * @param <T> le bean devant être utiliser pour l'accès aux données
 * @param <Y> le DAO gérant l'accès aux données
 */
public class AbstractOm<T extends PersistenceBean, Y extends AbstractLegacyDAO<T>> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractOm.class);

    protected transient Y commonDao;

    protected transient Collection<T> currentSelect;

    protected transient Iterator<T> currentSelectIt;

    protected T persistenceBean;

    @SuppressWarnings("unchecked")
    public AbstractOm() {
        final DataSourceDAOManager manager = ApplicationContextManager.getCoreContextBean(DataSourceDAOManager.ID_BEAN, DataSourceDAOManager.class);
        commonDao = (Y) manager.getDao(getGenericType());
        try {
            persistenceBean = getGenericType().newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            LOG.error(String.format("Couldn't instanciate %s object", getGenericType().getName()), e);
        }
    }

    @SuppressWarnings("unchecked")
    private Class<T> getGenericType() {
        return (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    public T getPersistenceBean() {
        return persistenceBean;
    }

    public void setPersistenceBean(final T persistenceBean) {
        this.persistenceBean = persistenceBean;
    }

    public void add() {
        commonDao.add(persistenceBean);
    }

    public void delete() throws Exception {
        commonDao.delete(persistenceBean.getId());
    }

    public void update() {
        commonDao.update(persistenceBean);
    }

    public T getById(final Long id) {
        return commonDao.getById(id);
    }

    /**
     * Methode apportant le même comportement qu'avant les DAO. Il est préconisé de ne PAS l'utiliser car on récupère l'ensemble des objets en mémoire.
     * Le seul cas qui le nécessite encore est sur les FicheUniv lorsque certains objets sont encore en objet DB et que d'autre sont en DAO.
     * @param requete la requête à executer
     * @return La taille de la liste de résultats
     */
    public int select(final String requete) {
        currentSelect = commonDao.select(requete);
        currentSelectIt = currentSelect.iterator();
        return currentSelect.size();
    }

    public List<T> selectAll(final String requete) {
        return commonDao.select(requete);
    }

    /**
     * Methode apportant le même comportement qu'avant les DAO. Il est préconisé de ne PAS l'utiliser car on récupère l'ensemble des objets en mémoire.
     * Le seul cas qui le nécessite encore est sur les FicheUniv lorsque certains objets sont encore en objet DB et que d'autre sont en DAO.
     * @return vrai si il reste des résultats. Les valeurs des données sont setter dans le bean courrant.
     */
    public boolean nextItem() {
        if (currentSelectIt != null && currentSelectIt.hasNext()) {
            persistenceBean = currentSelectIt.next();
            return true;
        }
        return false;
    }
}
