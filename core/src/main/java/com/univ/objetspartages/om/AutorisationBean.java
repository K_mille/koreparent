package com.univ.objetspartages.om;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.PropertyHelper;
import com.univ.collaboratif.bean.UserRolesCollaboratifBean;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceMetatag;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.objetspartages.util.InfosRolesUtils;
import com.univ.utils.Chaine;

/**
 * The Class AutorisationBean.
 */
public class AutorisationBean implements Serializable {

    /** The Constant INDICE_CREATION. */
    public static final int INDICE_CREATION = 0;

    /** The Constant INDICE_TRADUCTION. */
    public static final int INDICE_TRADUCTION = 1;

    /** The Constant INDICE_MODIFICATION. */
    public static final int INDICE_MODIFICATION = 2;

    /** The Constant INDICE_VALIDATION. */
    public static final int INDICE_VALIDATION = 3;

    /** The Constant INDICE_SUPPRESSION. */
    public static final int INDICE_SUPPRESSION = 4;

    /** The Constant INDICE_APPROBATION. */
    public static final int INDICE_APPROBATION = 5;

    /** The Constant INDICE_MISE_EN_LIGNE_UNITAIRE. */
    public static final int INDICE_MISE_EN_LIGNE_UNITAIRE = 6;

    /** The Constant INDICE_SUPPRESSION_UNITAIRE. */
    public static final int INDICE_SUPPRESSION_UNITAIRE = 7;

    private static final Logger LOG = LoggerFactory.getLogger(AutorisationBean.class);

    /** The Constant listeNiveauxApprobation. */
    private static final Hashtable<String, String> listeNiveauxApprobation = chargerNiveauxApprobation();

    /** The Constant listePermissionsValidation. */
    private static final TreeSet<String> listePermissionsValidation = new TreeSet<>(listeNiveauxApprobation.keySet());

    private static final long serialVersionUID = -8151931217542681757L;

    /** The liste permissions. */
    private final Hashtable<String, Vector<Perimetre>> listePermissions = new Hashtable<>();

    /** The niveau approbation. */
    private String niveauApprobation = "";
    /** The liste groupes. */
    private Collection<String> listeGroupes = null;
    /** The possede mode expert. */
    private boolean possedeModeExpert = false;
    /** The code. */
    private String code = "";
    /** The code structure. */
    private String codeStructure = "";
    /** The liste objets redacteur. */
    private Map<String, String> listeObjetsRedacteur = null;
    /** The is web master. */
    private boolean isWebMaster = false;
    /** The is relai composante. */
    @Deprecated
    private boolean isRelaiComposante = false;
    /** The is validateur. */
    private boolean isValidateur = false;
    /** The is diffuseur dsi. */
    private boolean isDiffuseurDSI = false;
    /** The is administrateur rubrique. */
    private boolean isAdministrateurRubrique = false;
    /** The is administrateur encadre. */
    private boolean isAdministrateurEncadre = false;
    /** The is administrateur phototheque. */
    private boolean isAdministrateurPhototheque = false;
    /** The is redacteur fiche courante. */
    private boolean isRedacteurFicheCourante = false;

    /**
     * Construction d'un bean contenant les autorisations pour un utilisateur.
     *
     * @param utilisateur
     *            the utilisateur
     * @param groupesdsi
     *            the groupesdsi
     * @param _ctx
     *            the ctx
     * @deprecated Utilisez {@link AutorisationBean#AutorisationBean(UtilisateurBean, Collection, OMContext)}
     */
    @Deprecated
    public AutorisationBean(final Utilisateur utilisateur, final Vector<String> groupesdsi, final OMContext _ctx) {
        init(utilisateur.getPersistenceBean(), groupesdsi, _ctx);
    }

    public AutorisationBean(final UtilisateurBean utilisateur, final Collection<String> groupesdsi, final OMContext _ctx) {
        init(utilisateur, groupesdsi, _ctx);
    }

    /**
     * Renvoie les perimetres de type structure pour une fiche.
     *
     * @param fiche the fiche
     * @return the structures perimetre fiche
     */
    public static List<String> getStructuresPerimetreFiche(final FicheUniv fiche) {
        String codeRattachementSecondaire = null;
        if (fiche instanceof FicheRattachementsSecondaires) {
            codeRattachementSecondaire = ((FicheRattachementsSecondaires) fiche).getCodeRattachementAutres();
        }
        return getStructuresPerimetreFiche(ReferentielObjets.getCodeObjet(fiche), fiche.getCode(), fiche.getCodeRattachement(), codeRattachementSecondaire);
    }

    /**
     * Renvoie les perimetres de type structure pour une fiche.
     *
     * @param codeObjet
     * @param code
     * @param codeStructure
     * @param codeRattachementAutre
     * @return the structures perimetre fiche
     */
    private static List<String> getStructuresPerimetreFiche(final String codeObjet, final String code, String codeStructure, final String codeRattachementAutre) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final StructureModele structure = serviceStructure.getByCodeLanguage(code, LangueUtil.getIndiceLocaleDefaut());
        final ArrayList<String> codesStructures = new ArrayList<>();
        // dans le cas d'une structure on prend le code de la fiche
        if (structure != null) {
            codeStructure = code;
        }
        codesStructures.add(codeStructure);
        if (StringUtils.isNotEmpty(codeRattachementAutre)) {
            // nouveau parametre pour ne pas controler les structures de rattachement secondaires
            if (!"0".equals(PropertyHelper.getCoreProperty("fiche." + ReferentielObjets.getNomObjet(codeObjet) + ".filtre_rattachements_secondaires"))) {
                final Enumeration<String> e = Chaine.getVecteurPointsVirgules(codeRattachementAutre).elements();
                while (e.hasMoreElements()) {
                    codesStructures.add(e.nextElement());
                }
            }
        }
        return codesStructures;
    }

    /**
     * Formater mgs operation interdite.
     *
     * @return the string
     * @deprecated Méthode retournant juste une chaine de caractère en dur. A ne pas utiliser...
     */
    @Deprecated
    public static String formaterMgsOperationInterdite() {
        return "Opération non autorisée ";
    }

    /**
     * Formater mgs operation interdite.
     *
     * @param raison the raison
     * @return the string
     * @deprecated Méthode retournant juste une chaine de caractère en dur concaténer avec celle fourni en paramètre. A ne pas utiliser...
     */
    @Deprecated
    public static String formaterMgsOperationInterdite(final String raison) {
        return "Opération non autorisée (" + raison + ")";
    }

    /**
     * Charger niveaux approbation.
     *
     * @return the hashtable
     */
    private static Hashtable<String, String> chargerNiveauxApprobation() {
        final String activationMultiNiveaux = PropertyHelper.getCoreProperty("validation_multi_niveaux.activation");
        Hashtable<String, String> niveauxApprobation = new Hashtable<>();
        if ("1".equals(activationMultiNiveaux)) {
            niveauxApprobation = CodeLibelle.lireTable(null, "permission_validation_fiche", null);
        }
        return niveauxApprobation;
    }

    /**
     * Gets the liste niveaux approbation.
     *
     * @return the liste niveaux approbation
     */
    public static Hashtable<String, String> getListeNiveauxApprobation() {
        return listeNiveauxApprobation;
    }

    /**
     * Gets the liste permissions validation.
     *
     * @return the liste permissions validation
     */
    public static TreeSet<String> getListePermissionsValidation() {
        return listePermissionsValidation;
    }

    public void init(final UtilisateurBean utilisateur, final Collection<String> groupesdsi, final OMContext _ctx) {
        initialiserPermissions(utilisateur, groupesdsi);
        /*
         * dump des permissions uniquement si on est en mode denug
         * sinon le parcours des permissions est chronophage
         */
        if (LOG.isDebugEnabled()) {
            dumpPermissions();
        }
        code = utilisateur.getCode();
        listeGroupes = groupesdsi;
        codeStructure = utilisateur.getCodeRattachement();
        // droit de saisie en mode expert
        final PermissionBean permission = new PermissionBean("TECH", "mda", "");
        if (listePermissions.get(permission.getChaineSerialisee()) != null) {
            possedeModeExpert = true;
        }
        // Suppression de la mise en mémoire des fiches de l'utilisateur
        // On utilise désormais la table Metatag pour une requete transversale sur le redacteur
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        listeObjetsRedacteur = serviceMetatag.getListeObjetsRedacteur(code);
    }

    /**
     * Initialisation de la liste des permissions pour une liste de roles stockée sous la forme [role1;perimetre1][role2;perimetre2].
     *
     * @param roles
     *            the roles
     */
    private void initialiserPermissionsRoles(final String roles) {
        final HashSet<NiveauPerimetre> setNiveauPerimetre = new HashSet<>();
        final StringTokenizer st = new StringTokenizer(StringUtils.defaultString(roles), "[]");
        final ArrayList<Object[]> aRoles = new ArrayList<>(st.countTokens());
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            final int indexPointVirgule = val.indexOf(';');
            if (indexPointVirgule != -1) {
                final String codeRole = val.substring(0, indexPointVirgule);
                final String sPerimetre = val.substring(indexPointVirgule + 1);
                LOG.debug("initialiserPermissionsRoles role = {};{}",codeRole,sPerimetre);
                final Perimetre perimetreAffectation = new Perimetre(sPerimetre);
                /* Lecture du role pour récupérer les permissions */
                final InfosRole infosRole = InfosRolesUtils.renvoyerItemRole(codeRole);
                if (infosRole.getCode().length() > 0) {
                    final Perimetre perimetreRole = new Perimetre(infosRole.getPerimetre());
                    /* calcul du périmètre du role */
                    final Perimetre perimetreRoleEffectif = Perimetre.calculerPerimetrePermission(perimetreAffectation, perimetreRole);
                    // RP20070707 controle de l'existance du périmètre
                    // si n'existe pas on ignore le rôle associé
                    if (perimetreRoleEffectif != null) {
                        setNiveauPerimetre.add(new NiveauPerimetre(perimetreRoleEffectif));
                        final Object[] rolePerimetre = {infosRole, perimetreRoleEffectif};
                        aRoles.add(rolePerimetre);
                    }
                }
            }
        }
        final Hashtable<String, NiveauPerimetre> tableNiveauPerimetreInitialise = initialiserlisteNiveauPerimetre(setNiveauPerimetre);
        for (final Object[] oRolePerimetre : aRoles) {
            final InfosRole infosRole = (InfosRole) oRolePerimetre[0];
            final Perimetre perimetreRoleEffectif = (Perimetre) oRolePerimetre[1];
            final Enumeration<PermissionBean> en = infosRole.getVecteurPermissions().elements();
            PermissionBean permission;
            Perimetre perimetreCourant;
            Vector<Perimetre> listePerimetres;
            NiveauPerimetre niveauPerimetreCourant;
            final NiveauPerimetre niveauPerimetreEffectif = tableNiveauPerimetreInitialise.get(perimetreRoleEffectif.getIdentifiantNiveau());
            while (en.hasMoreElements()) {
                permission = (en.nextElement());
                // controle que la permission de type "FICHE" ou "TECH" est bien lié à un espace collaboratif
                if (perimetreRoleEffectif.getCodeEspaceCollaboratif().length() > 0 && ((isNotPermissionFicheCollab(permission)) || (isNotPermissionGestionCollab(permission)))) {
                    continue;
                }
                // Lecture de la permission dans la Hashtable (si déjà stockée)
                listePerimetres = listePermissions.get(permission.getChaineSerialisee());
                if (listePerimetres == null) {
                    listePerimetres = new Vector<>(0);
                }
                boolean perimetreAInserer = true;
                for (int i = 0; i < listePerimetres.size(); i++) {
                    if (perimetreAInserer) {
                        perimetreCourant = listePerimetres.get(i);
                        // La permission est un sous-périmètre ce celle
                        // dèja stockée : pas de stockage
                        // gestion des périmètres
                        final boolean estEnfant;
                        final boolean estParent;
                        if (perimetreRoleEffectif.getCodeEspaceCollaboratif().length() == 0) {
                            niveauPerimetreCourant = tableNiveauPerimetreInitialise.get(perimetreCourant.getIdentifiantNiveau());
                            estEnfant = niveauPerimetreCourant.getLstEnfant().contains(niveauPerimetreEffectif);
                            estParent = niveauPerimetreCourant.getLstParent().contains(niveauPerimetreEffectif);
                            if (estEnfant) {
                                perimetreAInserer = false;
                            }
                            if (estParent) {
                                if (!estEnfant) {
                                    listePerimetres.remove(i);
                                    // On décrémente pour ne pas sauter un élément
                                    i--;
                                }
                            }
                            //else DIFFERENT dc on ajoute
                        }
                    }
                }
                if (perimetreAInserer) {
                    listePerimetres.add(perimetreRoleEffectif);
                }
                listePermissions.put(permission.getChaineSerialisee(), listePerimetres);
            }
        }
    }

    private boolean isNotPermissionGestionCollab(final PermissionBean permission) {
        return "TECH".equals(permission.getType()) && !"ges".equals(permission.getObjet());
    }

    private boolean isNotPermissionFicheCollab(final PermissionBean permission) {
        final Objetpartage objet = ReferentielObjets.getObjetByCode(permission.getObjet());
        return "FICHE".equals(permission.getType()) && (objet == null || !objet.isCollaboratif());
    }

    /**
     * Construction des listes des perimètres fils et pères pour chaque périmètre effectif de l'utilisateur.
     *
     * @param setNiveauPerimetre
     *            the set niveau perimetre
     * @return the hashtable
     */
    private Hashtable<String, NiveauPerimetre> initialiserlisteNiveauPerimetre(final HashSet<NiveauPerimetre> setNiveauPerimetre) {
        final Hashtable<String, NiveauPerimetre> tableNiveauPerimetre = new Hashtable<>();
        NiveauPerimetre niveauPerimetreCourant;
        NiveauPerimetre niveauPerimetreTest;
        //on fait une copie de la liste des perimetres effectifs
        final HashSet<NiveauPerimetre> perimetresTests = new HashSet<>(setNiveauPerimetre);
        for (final NiveauPerimetre niveauPerimetre : setNiveauPerimetre) {
            niveauPerimetreCourant = niveauPerimetre;
            if (niveauPerimetreCourant.getCodeEspaceCollaboratif().length() == 0) {
                for (final NiveauPerimetre niveauPerimetre2 : perimetresTests) {
                    niveauPerimetreTest = niveauPerimetre2;
                    //pour chauqe perimetre effectifs, on va voir si il est fils, pere, egal ou different du perimetreCourant
                    final int niveau = niveauPerimetreCourant.getNiveauPerimetre(niveauPerimetreTest);
                    if (niveau == NiveauPerimetre.EGAL || niveau == NiveauPerimetre.DESSOUS) {
                        niveauPerimetreCourant.ajouteParent(niveauPerimetreTest);
                    }
                    if (niveau == NiveauPerimetre.EGAL || niveau == NiveauPerimetre.DESSUS) {
                        niveauPerimetreCourant.ajouteEnfant(niveauPerimetreTest);
                    }
                    //sinon DIFFERENT => on ne fait rien
                }
            }
            tableNiveauPerimetre.put(niveauPerimetreCourant.toString(), niveauPerimetreCourant);
        }
        return tableNiveauPerimetre;
    }

    /**
     * Initialisation de la liste des permissions pour l'utilisateur.
     *
     * @param utilisateur
     *            the utilisateur
     * @param groupesUtilisateurs
     *            the groupes utilisateurs
     *
     */
    private void initialiserPermissions(final UtilisateurBean utilisateur, final Collection<String> groupesUtilisateurs) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        String sRoles = utilisateur.getRoles();
        /* Permissions affectées à l'utilisateur */
        final HashSet<String> setGroupesParent = new HashSet<>();
        for (final String currentCode : groupesUtilisateurs) {
            GroupeDsiBean userGroup = serviceGroupeDsi.getByCode(currentCode);
            if (userGroup != null) {
                setGroupesParent.add(currentCode);
                LOG.debug("** initialiserPermissions groupe : " + currentCode + "role : " + userGroup.getRoles());
                final int niveauItemCourant = serviceGroupeDsi.getLevel(userGroup);
                int niveau = niveauItemCourant - 1;
                while (niveau > 0) {
                    if (userGroup != null && StringUtils.isNotBlank(userGroup.getCodeGroupePere())) {
                        setGroupesParent.add(userGroup.getCodeGroupePere());
                        userGroup = serviceGroupeDsi.getByCode(userGroup.getCodeGroupePere());
                    }
                    niveau--;
                }
            }
        }
        for (final String codeGroupe : setGroupesParent) {
            final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupe);
            sRoles += group.getRoles();
        }
        if (Espacecollaboratif.isExtensionActivated()) {
            final Map<String, UserRolesCollaboratifBean> rolesEspaces = Espacecollaboratif.getRolesEspacesForUserAndGroup(utilisateur.getCode(), groupesUtilisateurs);
            for (final String codeEspace : rolesEspaces.keySet()) {
                sRoles += "[" + rolesEspaces.get(codeEspace).getCodeRole() + ";" + new Perimetre("", "", "", "", codeEspace).getChaineSerialisee() + "]";
            }
        }
        initialiserPermissionsRoles(sRoles);
        /* Initialisation webmaster */
        PermissionBean permission = new PermissionBean("TECH", "wmg", "");
        isWebMaster = listePermissions.get(permission.getChaineSerialisee()) != null;
        /* Initialisation relai-composante */
        permission = new PermissionBean("TECH", "fna", "");
        isRelaiComposante = listePermissions.get(permission.getChaineSerialisee()) != null;
        /* Initialisation validateur */
        final Enumeration<String> keys = listePermissions.keys();
        while (keys.hasMoreElements() && !isValidateur) {
            final String key = keys.nextElement();
            permission = new PermissionBean(key);
            // ajout des permisssions d'approbation
            isValidateur = "FICHE".equals(permission.getType()) && ("V".equals(permission.getAction()) || permission.getAction().startsWith("A"));
        }
        /* Initialisation diffuseur DSI */
        permission = new PermissionBean("TECH", "dsi", "");
        isDiffuseurDSI = listePermissions.get(permission.getChaineSerialisee()) != null;
        /* Initialisation administrateur rubrique */
        permission = new PermissionBean("TECH", "rub", "C");
        isAdministrateurRubrique = listePermissions.get(permission.getChaineSerialisee()) != null;
        permission = new PermissionBean("TECH", "rub", "S");
        isAdministrateurRubrique = isAdministrateurRubrique || listePermissions.get(permission.getChaineSerialisee()) != null;
        permission = new PermissionBean("TECH", "rub", "M");
        isAdministrateurRubrique = isAdministrateurRubrique || listePermissions.get(permission.getChaineSerialisee()) != null;
        /* Initialisation administrateur encadrés */
        permission = new PermissionBean("TECH", "enc", "M");
        isAdministrateurEncadre = listePermissions.get(permission.getChaineSerialisee()) != null;
        /* Initialisation administrateur phototheque */
        permission = new PermissionBean("TECH", "pho", "C");
        isAdministrateurPhototheque = listePermissions.get(permission.getChaineSerialisee()) != null;
        permission = new PermissionBean("TECH", "pho", "S");
        isAdministrateurPhototheque = isAdministrateurPhototheque || listePermissions.get(permission.getChaineSerialisee()) != null;
        permission = new PermissionBean("TECH", "pho", "M");
        isAdministrateurPhototheque = isAdministrateurPhototheque || listePermissions.get(permission.getChaineSerialisee()) != null;
    }

    /**
     * Affichage des permissions pour tests.
     */
    private void dumpPermissions() {
        final Enumeration<String> e = listePermissions.keys();
        while (e.hasMoreElements()) {
            final String key = e.nextElement();
            LOG.debug("> permission " + key);
            final Vector<Perimetre> listePerimetres = listePermissions.get(key);
            for (final Perimetre perimetreCourant : listePerimetres) {
                LOG.debug("   périmètre " + perimetreCourant.getChaineSerialisee());
            }
        }
    }

    /**
     * Détermine si l'utilisateur a une permission donnée sur un périmètre donné.
     *
     * @param permission
     *            the _permission
     * @param perimetre
     *            the _perimetre
     * @return true, if possede permission
     */
    public boolean possedePermission(final PermissionBean permission, final Perimetre perimetre) {
        boolean res = false;
        Vector<Perimetre> listePerimetres = new Vector<>();
        String chaineSerialisee = permission.getChaineSerialisee();
        if ("*".equals(permission.getObjet())) {
            final Collection<Objetpartage> objetsPartages = ReferentielObjets.getObjetsPartagesTries();
            for (final Objetpartage objetPartage : objetsPartages) {
                chaineSerialisee = permission.getType() + "/" + objetPartage.getCodeObjet() + "/" + permission.getAction();
                if (listePermissions.get(chaineSerialisee) != null) {
                    listePerimetres.addAll(listePermissions.get(chaineSerialisee));
                }
            }
        } else {
            listePerimetres = listePermissions.get(chaineSerialisee);
        }
        if (listePerimetres != null) {
            int i = 0;
            while (!res && (i < listePerimetres.size())) {
                final Perimetre perimetreCourant = listePerimetres.get(i);
                if (perimetre.getCodeEspaceCollaboratif().length() > 0) {
                    res = perimetreCourant.getCodeEspaceCollaboratif().equals(perimetre.getCodeEspaceCollaboratif());
                } else {
                    // JSS 20051104 : gestion des périmètres
                    res = perimetre.estConformeAuPerimetre(perimetreCourant);
                }
                i++;
            }
        }
        return res;
    }

    /**
     * Détermine si l'utilisateur a une permission de type ("FICHE", action) sur un périmètre donné.
     *
     * @param action
     *            the _action
     * @param perimetre
     *            the _perimetre
     * @return true, if possede action fiche
     */
    public boolean possedeActionFICHE(final String action, final Perimetre perimetre) {
        boolean res = false;
        // Boucle sur les permissions
        final Enumeration<String> e = listePermissions.keys();
        while (e.hasMoreElements()) {
            final String key = e.nextElement();
            final PermissionBean permission = new PermissionBean(key);
            if ("FICHE".equals(permission.getType()) && permission.getAction().equals(action)) {
                // Boucle sur les périmètres
                final Vector<Perimetre> listePerimetres = listePermissions.get(permission.getChaineSerialisee());
                if (listePerimetres != null) {
                    int i = 0;
                    while (!res && (i < listePerimetres.size())) {
                        final Perimetre perimetreCourant = listePerimetres.get(i);
                        if (perimetre.getCodeEspaceCollaboratif().length() > 0) {
                            if (perimetreCourant.getCodeEspaceCollaboratif().equals(perimetre.getCodeEspaceCollaboratif())) {
                                res = true;
                            }
                        } else {
                            // JSS 20051104 : gestion des périmètres
                            if (perimetre.estConformeAuPerimetre(perimetreCourant)) {
                                res = true;
                            }
                        }
                        i++;
                    }
                }
            }
        }
        return res;
    }

    /**
     * Détermine si l'utilisateur a une permission dont le périmètre couvre au moins un champ du périmètre.
     *
     * @param permission
     *            the _permission
     * @param perimetre
     *            the _perimetre
     * @return true, if possede permission partielle sur perimetre
     */
    public boolean possedePermissionPartielleSurPerimetre(final PermissionBean permission, final Perimetre perimetre) {
        boolean res = false;
        Vector<Perimetre> listePerimetres = new Vector<>();
        String chaineSerialisee = permission.getChaineSerialisee();
        if ("*".equals(permission.getObjet())) {
            final Collection<Objetpartage> objetsPartages = ReferentielObjets.getObjetsPartagesTries();
            for (final Objetpartage objetPartage : objetsPartages) {
                chaineSerialisee = permission.getType() + "/" + objetPartage.getCodeObjet() + "/" + permission.getAction();
                if (listePermissions.get(chaineSerialisee) != null) {
                    listePerimetres.addAll(listePermissions.get(chaineSerialisee));
                }
            }
        } else {
            listePerimetres = listePermissions.get(chaineSerialisee);
        }
        if (listePerimetres != null) {
            int i = 0;
            while (!res && (i < listePerimetres.size())) {
                final Perimetre perimetreCourant = listePerimetres.get(i);
                // gestion des périmètres
                res = perimetre.estPartiellementConformeAuPerimetre(perimetreCourant);
                i++;
            }
        }
        return res;
    }

    /**
     * Renvoie la liste des périmètres pour une permission donnée.
     *
     * @param permission
     *            the permission
     * @return the liste perimetres
     */
    public Vector<Perimetre> getListePerimetres(final PermissionBean permission) {
        Vector<Perimetre> res = listePermissions.get(permission.getChaineSerialisee());
        if (res == null) {
            res = new Vector<>();
        }
        return res;
    }

    /**
     * Renvoie la liste des permissions.
     *
     * @return the liste permissions
     */
    public Hashtable<String, Vector<Perimetre>> getListePermissions() {
        return listePermissions;
    }

    /**
     * détermine si l'utilisateur a accès à toutes les fiches.
     *
     * @param _objet
     *            the _objet
     * @return true, if est autorise a tout modifier
     */
    public boolean estAutoriseAToutModifier(final String _objet) {
        return getAutorisation(_objet, INDICE_MODIFICATION);
    }

    /**
     * Indique si l'objet est à valider.
     *
     * @param _objet
     *            the _objet
     * @return true, if est a valider
     */
    public boolean estAValider(final String _objet) {
        return true;
    }

    /**
     * Lecture de l'autorisation pour un objet et un type d'accès.
     *
     * @param objet
     *            the _objet
     * @param indiceAutorisation
     *            the indice autorisation
     * @return the autorisation
     */
    public boolean getAutorisation(final String objet, final int indiceAutorisation) {
        String action = "";
        boolean parcoursNiveauxApprobation = false;
        boolean res = false;
        if (indiceAutorisation == INDICE_CREATION) {
            action = "C";
        }
        if (indiceAutorisation == INDICE_MISE_EN_LIGNE_UNITAIRE) {
            action = "U";
        }
        if (indiceAutorisation == INDICE_SUPPRESSION_UNITAIRE) {
            action = "R";
        }
        if (indiceAutorisation == INDICE_TRADUCTION) {
            action = "D";
        }
        if (indiceAutorisation == INDICE_MODIFICATION) {
            action = "M";
        }
        if (indiceAutorisation == INDICE_VALIDATION) {
            // on regarde si il y a des niveaux d'approbation intermédiaires avant la validation classique
            if (listePermissionsValidation.size() > 0) {
                parcoursNiveauxApprobation = true;
            } else {
                action = "V";
            }
        }
        if (indiceAutorisation == INDICE_SUPPRESSION) {
            action = "S";
        }
        // on boucle sur les niveaux d'approbation pour trouver le droit de validation
        if (parcoursNiveauxApprobation) {
            for (final String aListePermissionsValidation : listePermissionsValidation) {
                final PermissionBean permission = new PermissionBean("FICHE", objet, aListePermissionsValidation);
                if (listePermissions.get(permission.getChaineSerialisee()) != null) {
                    res = true;
                    break;
                }
            }
            // on teste quand même le droit classique
            action = "V";
        }
        final PermissionBean permission = new PermissionBean("FICHE", objet, action);
        return res || listePermissions.get(permission.getChaineSerialisee()) != null;
    }

    /**
     * Lecture de l'autorisation pour une permission (sans tenir compte des périmètres).
     *
     * @param permission
     *            the permission
     * @return true, if possede permission tech
     */
    public boolean possedePermissionTech(final String permission) {
        return possedePermission(new PermissionBean("TECH", permission, ""));
    }

    /**
     * Lecture de l'autorisation pour une permission (sans tenir compte des périmètres).
     *
     * @param permission
     *            the _permission
     * @return true, if possede permission
     */
    public boolean possedePermission(final PermissionBean permission) {
        return permission != null && listePermissions.get(permission.getChaineSerialisee()) != null;
    }

    /**
     * Lecture de l'autorisation pour une fiche et un type d'accès.
     *
     * @param fiche
     *            the fiche
     * @param indiceAutorisation
     *            the indice autorisation
     * @return the autorisation par fiche
     */
    public boolean getAutorisationParFiche(final FicheUniv fiche, final int indiceAutorisation) {
        return getAutorisationParFiche(fiche, indiceAutorisation, "");
    }

    public boolean getAutorisationParFiche(final FicheUniv fiche, final int indiceApprobation, final String codeApprobationCourant) {
        String publicVise = null;
        String modeRestriction = null;
        String publicViseRestriction = null;
        String codeRattachementSecondaire = null;
        if (fiche instanceof DiffusionSelective) {
            publicVise = ((DiffusionSelective) fiche).getDiffusionPublicVise();
            modeRestriction = ((DiffusionSelective) fiche).getDiffusionModeRestriction();
            publicViseRestriction = ((DiffusionSelective) fiche).getDiffusionPublicViseRestriction();
        }
        if (fiche instanceof FicheRattachementsSecondaires) {
            codeRattachementSecondaire = ((FicheRattachementsSecondaires) fiche).getCodeRattachementAutres();
        }
        return getAutorisation(ReferentielObjets.getCodeObjet(fiche), fiche.getCode(), fiche.getCodeRubrique(), fiche.getCodeRattachement(), codeRattachementSecondaire, publicVise, modeRestriction, publicViseRestriction, indiceApprobation, codeApprobationCourant);
    }

    /**
     * @deprecated Utilisez {@link #getAutorisationParMeta(MetatagBean, int, String)}
     */
    @Deprecated
    public boolean getAutorisationParMeta(final Metatag metatag, final int indiceApprobation, final String codeApprobationCourant) {
        return getAutorisationParMeta(metatag.getPersistenceBean(), indiceApprobation, codeApprobationCourant);
    }

    public boolean getAutorisationParMeta(final MetatagBean metatag, final int indiceApprobation, final String codeApprobationCourant) {
        final String publicVise = metatag.getMetaDiffusionPublicVise();
        final String modeRestriction = metatag.getMetaDiffusionModeRestriction();
        final String publicViseRestriction = metatag.getMetaDiffusionPublicViseRestriction();
        final String codeRattachementSecondaire = metatag.getMetaCodeRattachementAutres();
        return getAutorisation(metatag.getMetaCodeObjet(), metatag.getMetaCode(), metatag.getMetaCodeRubrique(), metatag.getMetaCodeRattachement(), codeRattachementSecondaire, publicVise, modeRestriction, publicViseRestriction, indiceApprobation, codeApprobationCourant);
    }

    /**
     * Gets the autorisation par fiche.
     *
     * @param indiceAutorisation
     *            the indice autorisation
     * @param codeApprobation
     *            the code approbation
     * @return the autorisation par fiche
     */
    private boolean getAutorisation(final String codeObjet, final String codeFiche, final String codeRubrique, final String codeRattachement, final String codeRattachementAutre, final String publicVise, final String modeRestriction, final String publicViseResctriction, final int indiceAutorisation, String codeApprobation) {
        boolean parcoursNiveauxApprobation = false;
        String action = "";
        if (indiceAutorisation == INDICE_CREATION) {
            action = "C";
        } else if (indiceAutorisation == INDICE_MISE_EN_LIGNE_UNITAIRE) {
            action = "U";
        } else if (indiceAutorisation == INDICE_SUPPRESSION_UNITAIRE) {
            action = "R";
        } else if (indiceAutorisation == INDICE_TRADUCTION) {
            action = "D";
        } else if (indiceAutorisation == INDICE_MODIFICATION) {
            action = "M";
        } else if (indiceAutorisation == INDICE_VALIDATION) {
            // RP 20050810
            // on regarge si il y a des niveaux d'approbation intermédiaires avant validation
            final Iterator<String> it = listePermissionsValidation.iterator();
            if (it.hasNext() && !"V".equals(codeApprobation)) {
                parcoursNiveauxApprobation = true;
                while (it.hasNext()) {
                    final String codePermission = it.next();
                    // si niveau d'approbation non spécifié on prend le premier et on prepare le suivant
                    if ("".equals(codeApprobation)) {
                        action = codePermission;
                        if (it.hasNext()) {
                            codeApprobation = it.next();
                        } else {
                            codeApprobation = "V";
                        }
                        break;
                    }
                    // sinon on recupere le niveau courant et on prepare le suivant
                    else if (codePermission.equals(codeApprobation)) {
                        action = codeApprobation;
                        if (it.hasNext()) {
                            codeApprobation = it.next();
                        } else {
                            codeApprobation = "V";
                        }
                    }
                }
            } else {
                action = "V";
            }
        }
        // RP 20050811 nouvel indice pour tester uniquement le droit courant d'approbation
        // si non spécifié on retrouve en fait INDICE_VALIDATION
        if (indiceAutorisation == INDICE_APPROBATION) {
            if ("".equals(codeApprobation)) {
                action = "V";
            } else {
                action = codeApprobation;
            }
        } else if (indiceAutorisation == INDICE_SUPPRESSION) {
            action = "S";
        }
        /* Calcul de la permission */
        final PermissionBean permission = new PermissionBean("FICHE", codeObjet, action);
        /*****************************************************
         * JSS 20051031 : Calcul des périmètres de la fiche *
         * ***************************************************/
        /* Structures */
        final List<String> codesStructures = getStructuresPerimetreFiche(codeObjet, codeFiche, codeRattachement, codeRattachementAutre);
        /* Rubriques */
        final ArrayList<String> codesRubriques = new ArrayList<>();
        if (codeRubrique != null) {
            codesRubriques.add(codeRubrique);
        }
        /* Groupes + espaces */
        final ArrayList<String> codesPublicsVises = new ArrayList<>();
        final ArrayList<String> codesEspaces = new ArrayList<>();
        if (StringUtils.isNotEmpty(modeRestriction)) {
            if ("4".equals(modeRestriction)) {
                //Restriction à un espace
                codesEspaces.add(publicViseResctriction);
            } else {
                // on ajoute à la place "*" pour ignorer le paramètre
                codesPublicsVises.add("/*");
            }
        }
        boolean res = getAutorisationPourPerimetresFiche(permission, codesStructures, codesRubriques, codesPublicsVises, codesEspaces);
        // dans le cas d'une validation si l'utilisateur a le droit sur le niveau d'approbation courant on réitère sur le suivant
        if (indiceAutorisation == INDICE_VALIDATION) {
            if (parcoursNiveauxApprobation && res) {
                res = getAutorisation(codeObjet, codeFiche, codeRubrique, codeRattachement, codeRattachementAutre, publicVise, modeRestriction, publicViseResctriction, indiceAutorisation, codeApprobation);
            } else {
                // si la fiche reste sur niveau d'approbation intermédiaire on le stocke
                if (parcoursNiveauxApprobation && !"V".equals(action)) {
                    this.niveauApprobation = action;
                } else {
                    this.niveauApprobation = "";
                }
            }
        }
        return res;
    }

    /**
     * JSS 20051031 Calcul d'une permission sur une liste de périmètres à combiner.
     *
     * @param permission
     *            the permission
     * @param _codesStructuresFiche
     *            the _codes structures fiche
     * @param _codesRubriquesFiche
     *            the _codes rubriques fiche
     * @param _codesPublicsVisesFiche
     *            the _codes publics vises fiche
     * @param _codesEspacesFiche
     *            the _codes espaces fiche
     * @return the autorisation pour perimetres fiche
     */
    public boolean getAutorisationPourPerimetresFiche(final PermissionBean permission, final List<String> _codesStructuresFiche, final List<String> _codesRubriquesFiche, final List<String> _codesPublicsVisesFiche, final List<String> _codesEspacesFiche) {
        boolean res = false;
        // On ajoute un élément à vide pour faciliter la gestion des boucles
        Object codesStructures[] = _codesStructuresFiche.toArray();
        if (codesStructures.length == 0) {
            codesStructures = new String[] {""};
        }
        Object codesRubriques[] = _codesRubriquesFiche.toArray();
        if (codesRubriques.length == 0) {
            codesRubriques = new String[] {""};
        }
        Object codesPublicsVises[] = _codesPublicsVisesFiche.toArray();
        if (codesPublicsVises.length == 0) {
            codesPublicsVises = new String[] {""};
        }
        Object codesEspaces[] = _codesEspacesFiche.toArray();
        if (codesEspaces.length == 0) {
            codesEspaces = new String[] {""};
        }
        for (int i = 0; i < codesStructures.length && !res; i++) {
            for (int j = 0; j < codesRubriques.length && !res; j++) {
                for (int k = 0; k < codesPublicsVises.length && !res; k++) {
                    final String publicVise = (String) codesPublicsVises[k];
                    String groupe = "";
                    String profil = "";
                    final int indiceSeparateur = publicVise.indexOf("/");
                    if (indiceSeparateur != -1) {
                        profil = publicVise.substring(0, indiceSeparateur);
                        groupe = publicVise.substring(indiceSeparateur + 1);
                    }
                    for (int l = 0; l < codesEspaces.length && !res; l++) {
                        final Perimetre perimetreFiche = new Perimetre((String) codesStructures[i], (String) codesRubriques[j], profil, groupe, (String) codesEspaces[l]);
                        if (possedePermission(permission, perimetreFiche)) {
                            res = true;
                        }
                    }
                }
            }
        }
        return res;
    }

    /**
     * Controle si l'utilisateur peut demander le niveau d'approbation spécifié.
     *
     * @param fiche
     *            the fiche
     * @param codeApprobationCourant
     *            the code approbation courant
     * @param codeApprobationDemande
     *            the _code approbation demande
     * @return true, if est autorise a demander approbation
     */
    public boolean estAutoriseADemanderApprobation(final FicheUniv fiche, final String codeApprobationCourant, final String codeApprobationDemande) {
        boolean res = true;
        boolean niveauDemande = false;
        // on teste que l'utilisateur possède tous les niveaux d'approbation du courant à celui demandé moins un
        for (final String codePermission : listePermissionsValidation) {
            // test sur le niveau courant
            if (!niveauDemande && (codeApprobationCourant.length() == 0 || codePermission.equals(codeApprobationCourant))) {
                niveauDemande = true;
                if (!getAutorisationParFiche(fiche, AutorisationBean.INDICE_APPROBATION, codePermission)) {
                    res = false;
                    break;
                }
            } else if (niveauDemande && !codePermission.equals(codeApprobationDemande) && !getAutorisationParFiche(fiche, AutorisationBean.INDICE_APPROBATION, codePermission)) {
                // test sur les niveaux intermédiaires
                res = false;
                break;
            }
            // sortie sur le niveau demandé
            if (codePermission.equals(codeApprobationDemande)) {
                break;
            }
        }
        if ("V".equals(codeApprobationDemande)) {
            this.niveauApprobation = "";
        } else {
            this.niveauApprobation = codeApprobationDemande;
        }
        return res;
    }

    /**
     * Controle si l'utilisateur possede un droit d'approbation sur la fiche.
     *
     * @param fiche
     *            the fiche
     * @param codeApprobationCourant
     *            the code approbation courant
     * @return true, if possede autorisation validation
     */
    public boolean possedeAutorisationValidation(final FicheUniv fiche, final String codeApprobationCourant) {
        boolean res = false;
        // si la fiche est à valider on teste le niveau courant
        if ("0002".equals(fiche.getEtatObjet())) {
            res = getAutorisationParFiche(fiche, INDICE_APPROBATION, codeApprobationCourant);
        } else {
            for (final String aListePermissionsValidation : listePermissionsValidation) {
                if (getAutorisationParFiche(fiche, INDICE_APPROBATION, aListePermissionsValidation)) {
                    res = true;
                    break;
                }
            }
            if (!res) {
                res = getAutorisationParFiche(fiche, INDICE_APPROBATION, "V");
            }
        }
        return res;
    }

    /**
     * Controle si l'utilisateur est habilité à modifier la fiche (soit auteur soit droit de modifs ou validation sur périmètre intégrant le fiche).
     *
     * @param fiche the fiche
     * @return true, if est autorise a modifier la fiche
     */
    public boolean estAutoriseAModifierLaFiche(final FicheUniv fiche) {
        return estAutoriseAModifierLaFiche(fiche, true);
    }

    /**
     * Controle si l'utilisateur est habilité à modifier la fiche Si enregistrementRubriqueCourante = true, on teste la modification de la fiche sinon on teste si la rubrique peut
     * etre déplacer.
     *
     * @param fiche
     *            the fiche
     * @param enregistrementRubriqueCourante
     *            the enregistrement rubrique courante
     * @return true, if est autorise a modifier la fiche
     */
    public boolean estAutoriseAModifierLaFiche(final FicheUniv fiche, final boolean enregistrementRubriqueCourante) {
        boolean res = false;
        if (fiche != null) {
            if (enregistrementRubriqueCourante && fiche.getCodeRedacteur().equals(code)) {
                res = true;
            } else {
                // en modification on autorise aussi le déplacement (enregistrementRubriqueCourante = false) vers le périmètre de création
                res = getAutorisationParFiche(fiche, INDICE_MODIFICATION);
                if (!res && !enregistrementRubriqueCourante) {
                    res = getAutorisationParFiche(fiche, INDICE_CREATION);
                }
            }
        }
        return res;
    }

    /**
     * Controle si l'utilisateur est habilité à modifier la fiche Si enregistrementRubriqueCourante = true, on teste la modification de la fiche sinon on teste si la rubrique peut
     * etre déplacer.
     *
     * @param codeObjet
     * @param codeFiche
     * @param codeRedacteur
     * @param codeRubrique
     * @param codeRattachement
     * @param codeRattachementAutre
     * @param publicVise
     * @param modeRestriction
     * @param publicViseResctriction
     * @param enregistrementRubriqueCourante
     * @return true if est autorise a modifier la fiche
     */
    public boolean estAutoriseAModifierLafiche(final String codeObjet, final String codeFiche, final String codeRedacteur, final String codeRubrique, final String codeRattachement, final String codeRattachementAutre, final String publicVise, final String modeRestriction, final String publicViseResctriction, final boolean enregistrementRubriqueCourante) {
        boolean res = false;
        if ((enregistrementRubriqueCourante) && (codeRedacteur.equals(code))) {
            res = true;
        } else {
            // en modification on autorise aussi le déplacement ( enregistrementRubriqueCourante = false) vers le périmètre de création
            res = getAutorisation(codeObjet, codeFiche, codeRubrique, codeRattachement, codeRattachementAutre, publicVise, modeRestriction, publicViseResctriction, INDICE_MODIFICATION, "");
            if (!res && !enregistrementRubriqueCourante) {
                res = getAutorisation(codeObjet, codeFiche, codeRubrique, codeRattachement, codeRattachementAutre, publicVise, modeRestriction, publicViseResctriction, INDICE_CREATION, "");
            }
        }
        return res;
    }

    /**
     * Controle si l'utilisateur est habilité à supprimer la fiche (soit auteur (si parametre à true) soit droit de suppression sur périmètre intégrant le fiche).
     *
     * @param fiche
     *            the fiche
     * @param inclureSuppressionBrouillons
     *            the inclure suppression brouillons
     * @return true, if est autorise a supprimer la fiche
     */
    public boolean estAutoriseASupprimerLaFiche(final FicheUniv fiche, final boolean inclureSuppressionBrouillons) {
        boolean res = false;
        if (fiche != null) {
            if (inclureSuppressionBrouillons && "0001".equals(fiche.getEtatObjet())) {
                res = true;
            } else if (fiche.getCodeRedacteur().equals(getCode()) && getAutorisationParFiche(fiche, AutorisationBean.INDICE_SUPPRESSION_UNITAIRE)) {
                res = true;
            } else if (getAutorisationParFiche(fiche, AutorisationBean.INDICE_SUPPRESSION)) {
                res = true;
            }
        }
        return res;
    }

    /**
     * Controle si l'utilisateur est habilité à supprimer la fiche (soit auteur (si parametre à true) soit droit de suppression sur périmètre intégrant le fiche).
     *
     * @param fiche the fiche
     * @return true, if est autorise a supprimer la fiche
     */
    public boolean estAutoriseASupprimerLaFiche(final FicheUniv fiche) {
        return estAutoriseASupprimerLaFiche(fiche, true);
    }

    /**
     * Renvoie le code de la personne qui s'est identifié.
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Lecture des objets pour lesquels il existe au moins un droit d'accès (permission ou rédacteur d'une fiche).
     *
     * @return the liste objets
     */
    public Vector<String> getListeObjets() {
        // Stockage dans hashtable pour éviter les doublons
        final Hashtable<String, String> liste = new Hashtable<>();
        // Ajout des objets pour lesquels il existe une permission
        Set<String> e = listePermissions.keySet();
        for (final String currentPerm : e) {
            final PermissionBean permission = new PermissionBean(currentPerm);
            if ("FICHE".equals(permission.getType())) {
                liste.put(permission.getObjet(), StringUtils.EMPTY);
            }
        }
        // Ajout des objets dont l'utilisateur est rédacteur
        e = listeObjetsRedacteur.keySet();
        for (final String currentCode : e) {
            liste.put(currentCode, StringUtils.EMPTY);
        }
        // Construction du vecteur
        final Vector<String> res = new Vector<>();
        res.addAll(liste.keySet());
        return res;
    }

    /**
     * Détermine si l'utilisateur est un WebMaster.
     *
     * @return the liste groupes
     */
    public Collection<String> getListeGroupes() {
        return listeGroupes;
    }

    /**
     * Renvoie la structure de l'utilisateur.
     *
     * @return the code structure
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Détermine si l'utilisateur est un relai-composante.
     *
     * @return true, if checks if is relai composante
     * @deprecated Plus utilisé, à supprimer
     */
    @Deprecated
    public boolean isRelaiComposante() {
        return isRelaiComposante;
    }

    /**
     * Détermine si l'utilisateur est un validateur.
     *
     * @return true, if checks if is validateur
     */
    public boolean isValidateur() {
        return isValidateur;
    }

    /**
     * Détermine si l'utilisateur est un WebMaster.
     *
     * @return true, if checks if is web master
     */
    public boolean isWebMaster() {
        return isWebMaster;
    }

    /**
     * Détermine si l'utilisateur est un diffuseur de DSI.
     *
     * @return true, if checks if is diffuseur dsi
     */
    public boolean isDiffuseurDSI() {
        return isDiffuseurDSI;
    }

    /**
     * Détermine si l'utilisateur a un menu 'outils'.
     *
     * @return true, if checks if is administrateur rubrique
     */
    public boolean isAdministrateurRubrique() {
        return isAdministrateurRubrique;
    }

    /**
     * Checks if is administrateur encadre.
     *
     * @return Returns the isAdministrateurFil.
     */
    public boolean isAdministrateurEncadre() {
        return isAdministrateurEncadre;
    }

    /**
     * Checks if is administrateur phototheque.
     *
     * @return Returns the isAdministrateurPhototheque.
     */
    public boolean isAdministrateurPhototheque() {
        return isAdministrateurPhototheque;
    }

    /**
     * Gets the niveau approbation.
     *
     * @return the niveau approbation
     */
    public String getNiveauApprobation() {
        return niveauApprobation;
    }

    /**
     * Possede mode expert.
     *
     * @return true, if successful
     */
    public boolean possedeModeExpert() {
        return possedeModeExpert;
    }

    /**
     * Checks if is redacteur fiche courante.
     *
     * @return true, if is redacteur fiche courante
     */
    public boolean isRedacteurFicheCourante() {
        return isRedacteurFicheCourante;
    }

    /**
     * Sets the redacteur fiche courante.
     *
     * @param isRedacteurFicheCourante
     *            the new redacteur fiche courante
     */
    public void setRedacteurFicheCourante(final boolean isRedacteurFicheCourante) {
        this.isRedacteurFicheCourante = isRedacteurFicheCourante;
    }
}
