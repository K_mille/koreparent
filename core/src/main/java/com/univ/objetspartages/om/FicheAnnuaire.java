/*
 * Created on 7 juin 2006
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.objetspartages.om;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.cms.objetspartages.Objetpartage;
import com.univ.utils.Chaine;
import com.univ.utils.ContexteDao;
import com.univ.utils.ContexteUniv;
import com.univ.utils.URLResolver;
import com.univ.utils.UnivWebFmt;
// TODO: Auto-generated Javadoc

/**
 * The Class FicheAnnuaire.
 */
public class FicheAnnuaire {

    /**
     * Contrôle le code des objets de type structure : - ne doit pas contenir des caractères interdits - ne doit pas déjà exister dans un autre objet de type Structure.
     *
     * @param _code
     *            the _code
     * @param _langue
     *            the _langue
     *
     * @return the string
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     * @throws IllegalArgumentException
     *             the illegal argument exception
     * @throws ClassNotFoundException
     *             the class not found exception
     * @throws InstantiationException
     *             the instantiation exception
     * @throws InvocationTargetException
     *             the invocation target exception
     * @throws NoSuchMethodException
     *             the no such method exception
     * @throws Exception
     *             the exception
     */
    public static String checkCode(final String _code, final String _langue) throws Exception {
        Chaine.controlerCodeMetier(_code);
        FicheUniv ficheUniv;
        AnnuaireModele annuaire;
        //controle de l'unicite
        for (final Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            if (objet.isRecherchable()) {
                final Class<?> classesParam[] = new Class[0];
                final Class<?> classeObjet = Class.forName(objet.getNomClasse());
                // Récupération du constructeur de l'interface processus
                final Constructor<?> constructeur = classeObjet.getConstructor(classesParam);
                // Instanciation de la fiche
                final Object[] params = new Object[0];
                ficheUniv = (FicheUniv) constructeur.newInstance(params);
                if (ficheUniv instanceof AnnuaireModele) {
                    annuaire = (AnnuaireModele) ficheUniv;
                    annuaire.init();
                    final int count = ficheUniv.selectCodeLangueEtat(_code, _langue, StringUtils.EMPTY);
                    if (count > 0) {
                        ficheUniv.nextItem();
                        throw new ErreurApplicative("Ce code est déjà utilisé pour la fiche " + ReferentielObjets.getNomObjet(ReferentielObjets.getCodeObjet(ficheUniv)) + " : " + ficheUniv.getLibelleAffichable());
                    }
                }
            }
        }
        return _code;
    }

    /**
     * Récupération de l'objet annuaire.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the _code
     * @param langue
     *            the _langue
     *
     * @return the fiche annuaire
     *
     * @throws Exception
     *             the exception
     * @deprecated utilise le contexte uniquement pour la connexion à la bdd. Utiliser {@link FicheAnnuaire#getFicheAnnuaire(String, String)}
     */
    public static AnnuaireModele getFicheAnnuaire(final OMContext ctx, String code, String langue) throws Exception {
        return getFicheAnnuaire(code, langue);
    }

    /**
     * Récupération en base de l'objet annuaire par son code et sa langue
     *
     * @param code le code de la fiche annuaire.
     * @param langue la langue de la fiche
     *
     * @return La fiche correspondant au code et à la langue dans l'état en ligne s'il existe, sinon, dans le premier état trouvé.
     *         Si la langue n'est pas renseignée, la première fiche trouvée, dans l'état en ligne s'il existe, sinon, dans le premier état trouvé.
     *         Si le code est vide (blank), retourne null
     *
     * */
    private static AnnuaireModele getFicheAnnuaireParCodeLangue(String code, String langue) throws Exception {
        AnnuaireModele res = null;
        AnnuaireModele annuaire;
        if (StringUtils.isBlank(code)) {
            return null;
        }
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv != null && ficheUniv instanceof AnnuaireModele) {
                try (ContexteDao ctx = new ContexteDao()) {
                    annuaire = (AnnuaireModele) ficheUniv;
                    annuaire.init();
                    annuaire.setCtx(ctx);
                    // On cherche d'abord la version en ligne dans la langue du l'utilisateur, puis dans la langue par defaut, puis les autres versions
                    int count = ficheUniv.selectCodeLangueEtat(code, langue, EtatFiche.EN_LIGNE.getEtat());
                    if (count == 0) {
                        count = ficheUniv.selectCodeLangueEtat(code, langue, StringUtils.EMPTY);
                    }
                    if (count > 0) {
                        if (annuaire.nextItem()) {
                            res = annuaire;
                        }
                    }
                }
            }
        }
        return res;
    }

    /**
     * Récupération de la première fiche annuaire correspondant à un code, sans critère de langue dans l'état en ligne s'il existe, sinon, dans le premier état trouvé
     *
     * @param code le code de la fiche annuaire.
     *
     * @return La la première fiche trouvée correspondant au code dans l'état en ligne s'il existe, sinon, dans le premier état trouvé.
     *         Si le code est vide (blank), retourne null
     */
    public static AnnuaireModele getFicheAnnuaire(String code) throws Exception {
        return getFicheAnnuaireParCodeLangue(code, null);
    }

    /**
     * Récupération de la fiche annuaire correspondant à un code et une langue dans l'état en ligne s'il existe, sinon, dans le premier état trouvé
     *
     * @param code le code de la fiche annuaire.
     * @param langue la langue de la fiche
     *
     * @return La fiche correspondant au code et à la langue dans l'état en ligne s'il existe, sinon, dans le premier état trouvé.
     *         Si le code est vide (blank), retourne null
     */
    public static AnnuaireModele getFicheAnnuaire(final String code, final String langue) throws Exception {
        String codeFiche = code;
        String langueFiche = langue;
        if (StringUtils.contains(code, ",LANGUE=")) {
            codeFiche = StringUtils.substringBefore(code, ",LANGUE=");
            langueFiche = StringUtils.substringAfter(code, ",LANGUE=");
        }
        if (StringUtils.isBlank(langueFiche)) {
            langueFiche = String.valueOf(LangueUtil.DEFAULT_LANGUE_INDICE);
        }
        return getFicheAnnuaireParCodeLangue(codeFiche, langueFiche);
    }

    /**
     * Récupération d'une liste de fiches annuaire, correspondant à un ensemble de références (code, langue, type responsable).
     * Les fiches sont dans l'état en ligne s'il existe, sinon, dans le premier état trouvé.
     *
     * @param references la chaîne des références aux fiches annuaire.
     *
     * @return Une {@link List} de fiches {@link AnnuaireModele}.
     */
    public static List<AnnuaireModele> getFichesAnnuaireDepuisReference(String references) throws Exception {
        List<AnnuaireModele> listeAnnuaires = new ArrayList<>();
        final Map<String, List<AnnuaireModele>> fichesAnnuaireDepuisReferenceParTypeResponsable = getFichesAnnuaireDepuisReferenceParTypeResponsable(references);
        for (List<AnnuaireModele> listeAnnuairesParTypeResponsable : fichesAnnuaireDepuisReferenceParTypeResponsable.values()) {
            listeAnnuaires.addAll(listeAnnuairesParTypeResponsable);
        }
        return listeAnnuaires;
    }

    /**
     * Récupération d'une map de fiches annuaire, correspondant à un ensemble de références (code, langue, type responsable), classées par type de responsable.
     * Les fiches sont dans l'état en ligne s'il existe, sinon, dans le premier état trouvé.
     *
     * @param references la chaîne des références aux fiches annuaire.
     *
     * @return Une {@link Map} de liste de fiches {@link AnnuaireModele} classées par code de {@link com.univ.objetspartages.bean.LabelBean} de type de responsable.
     */
    public static Map<String, List<AnnuaireModele>> getFichesAnnuaireDepuisReferenceParTypeResponsable(String references) throws Exception {
        Map<String, List<AnnuaireModele>> mapAnnuaires = new LinkedHashMap<>();
        // Extraction des références
        String[] listeReferences = StringUtils.split(references, ";");
        if (listeReferences != null) {
            for (String reference : listeReferences) {
                // Extraction des composants de la référence
                String codeFiche = null;
                String langue = null;
                String codeLibelleTypeResponsable = null;
                final StringTokenizer composantsReferenceTokenizer = new StringTokenizer(reference, ",");
                if (composantsReferenceTokenizer.hasMoreTokens()) {
                    codeFiche = composantsReferenceTokenizer.nextToken();
                }
                if (composantsReferenceTokenizer.hasMoreTokens()) {
                    langue = StringUtils.replace(composantsReferenceTokenizer.nextToken(), "LANGUE=", StringUtils.EMPTY);
                }
                if (composantsReferenceTokenizer.hasMoreTokens()) {
                    codeLibelleTypeResponsable = composantsReferenceTokenizer.nextToken();
                }
                final AnnuaireModele annuaire = getFicheAnnuaireParCodeLangue(codeFiche, langue);
                if (annuaire != null) {
                    List<AnnuaireModele> listeAnnuairesPourTypeResponsable = mapAnnuaires.get(codeLibelleTypeResponsable);
                    if (listeAnnuairesPourTypeResponsable == null) {
                        listeAnnuairesPourTypeResponsable = new ArrayList<>();
                    }
                    listeAnnuairesPourTypeResponsable.add(annuaire);
                    mapAnnuaires.put(codeLibelleTypeResponsable, listeAnnuairesPourTypeResponsable);
                }
            }
        }
        return mapAnnuaires;
    }

    /**
     * Calcule la luiste des objets partagés de type annuaire présent sur le projet
     *
     * @return la liste des objets partagés présent sur l'application ou une liste vide si il n'y en a pas.
     */
    public static List<Objetpartage> getObjetsAnnuaires() {
        final List<Objetpartage> fichesAnnuaires = new ArrayList<>();
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
            if (ficheUniv != null && ficheUniv instanceof AnnuaireModele) {
                fichesAnnuaires.add(ReferentielObjets.getObjetByCode(codeObjet));
            }
        }
        return fichesAnnuaires;
    }

    /**
     * Récupération de l'email.
     *
     * @param ctx
     *            the ctx
     * @param code
     *            the code
     * @param langue
     *            the langue
     *
     * @return the mailto
     *
     * @throws Exception
     *             the exception
     *
     * @deprecated Utiliser getFicheAnnuaire(ctx, code, langue).getAdresseMail()
     */
    @Deprecated
    public static String getMailto(final OMContext ctx, final String code, final String langue) throws Exception {
        return getFicheAnnuaire(code, langue).getAdresseMail();
    }

    /**
     * AM 200501 : Pour le LMD Retourne sous la forme d'une arraylist la liste des fiches annuaires trouvées par rapport au code: &lt;a href="lien fiche annuaire"
     * title="libellé affichable"&gt;libellé affichable fiche annuaire&lt;/a&gt;.
     *
     * @param _ctx
     *            the _ctx
     * @param _codes
     *            the _codes
     * @param _langue
     *            the _langue
     *
     * @return the libelles liens annuaires
     *
     * @throws Exception
     *             the exception
     */
    public static ArrayList<String> getLibellesLiensAnnuaires(final OMContext _ctx, final String _codes, final String _langue) throws Exception {
        final ArrayList<String> listeAnnuaires = new ArrayList<>();
        String langue = _langue;
        if (langue == null || langue.length() == 0) {
            langue = "0";
        }
        if (_codes == null || _codes.length() == 0) {
            return listeAnnuaires;
        }
        final StringTokenizer st = new StringTokenizer(_codes, ";");
        while (st.hasMoreTokens()) {
            final String code = st.nextToken();
            for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
                final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                if (ficheUniv != null && ficheUniv instanceof AnnuaireModele) {
                    final String[] values = code.split(",");
                    final AnnuaireModele annuaire = (AnnuaireModele) ficheUniv;
                    annuaire.init();
                    annuaire.setCtx(_ctx);
                    // On cherche d'abord la version en ligne puis les autres versions
                    final int count = ficheUniv.selectCodeLangueEtat(values[0], langue, EtatFiche.EN_LIGNE.getEtat());
                    // RP 20070831 cette méthode étant uniquement appelée en front on ne renvoit plus que les fiches en ligne
                    //if( count == 0)
                    //    count = ficheUniv.selectCodeLangueEtat(code, langue, "");
                    if (count > 0) {
                        while (annuaire.nextItem()) {
                            listeAnnuaires.add("<a href=\"" + URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche((ContexteUniv) _ctx, annuaire), _ctx) + "\" title=\"" + annuaire.getLibelleAffichable() + "\">" + annuaire.getLibelleAffichable() + "</a>");
                        }
                    }
                }
            }
        }
        return listeAnnuaires;
    }

    /**
     *
     * @param ctx ne sert pas
     * @param codes le ou les codes des fiches séparé par des ";"...
     * @param langue la langue dans laquelle on souhaite récupérer ça.
     * @return le ou les libellés des codes fournis séparé par des ";" si il y en a plusieurs...
     * @throws Exception lors des requêtes en BDD...
     * @deprecated le contexte ne sert à rien. utiliser {@link FicheAnnuaire#getLibelleAffichable(String, String)}
     */
    @Deprecated
    public static String getLibelleAffichable(final OMContext ctx, final String codes, final String langue) throws Exception {
        return getLibelleAffichable(codes, langue);
    }

    /**
     * Renvoie le libelle a afficher (methode statique utilisee pour les jointures entre fiches).
     *
     * @param codes le ou les codes des fiches séparé par des ";"...
     * @param langue la langue dans laquelle on souhaite récupérer ça.
     * @return le ou les libellés des codes fournis séparé par des ";" si il y en a plusieurs...
     * @throws Exception lors des requêtes en BDD...
     */
    public static String getLibelleAffichable(final String codes, String langue) throws Exception {
        final List<String> res = new ArrayList<>();
        AnnuaireModele annuaire;
        FicheUniv ficheUniv;
        if (StringUtils.isEmpty(codes)) {
            return StringUtils.EMPTY;
        }
        langue = StringUtils.defaultIfEmpty(langue, "0");
        final StringTokenizer st = new StringTokenizer(codes, ";");
        while (st.hasMoreTokens()) {
            final String code = st.nextToken();
            String libelle = null;
            for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
                ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                if (ficheUniv != null && ficheUniv instanceof AnnuaireModele) {
                    final String[] values = code.split(",");
                    try (ContexteDao ctx = new ContexteDao()) {
                        annuaire = (AnnuaireModele) ficheUniv;
                        annuaire.init();
                        annuaire.setCtx(ctx);
                        // On cherche d'abord la version en ligne puis les autres versions
                        int count = ficheUniv.selectCodeLangueEtat(values[0], langue, EtatFiche.EN_LIGNE.getEtat());
                        if (count == 0) {
                            count = ficheUniv.selectCodeLangueEtat(values[0], langue, StringUtils.EMPTY);
                        }
                        if (count > 0) {
                            if (annuaire.nextItem()) {
                                libelle = annuaire.getLibelleAffichable();
                            }
                            break;
                        }
                    }
                }
            }
            res.add(StringUtils.defaultIfBlank(libelle, "-"));
        }
        return StringUtils.join(res, ";");
    }
}
