package com.univ.objetspartages.om;

import com.kportal.extension.module.plugin.objetspartages.om.ObjetPluginContenu;

public interface FicheObjet extends ObjetPluginContenu, FicheUniv, DiffusionSelective, FicheRattachementsSecondaires {}
