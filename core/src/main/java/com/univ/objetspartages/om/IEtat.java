package com.univ.objetspartages.om;

import com.kportal.extension.annotation.GetExtension;

@GetExtension
public interface IEtat {

    /**
     * @return le code de l'etat
     */
    String getEtat();

    /**
     * @return si utilisé en front
     */
    boolean isFront();
}
