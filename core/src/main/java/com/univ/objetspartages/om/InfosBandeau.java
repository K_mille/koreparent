package com.univ.objetspartages.om;

import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

/**
 * Informations permettant d'afficher le bandeau d'une rubrique.
 */
public class InfosBandeau {

    /** The url bandeau. */
    private String urlBandeau = StringUtils.EMPTY;

    /** The couleur titre. */
    private String couleurTitre = StringUtils.EMPTY;

    /** The couleur fond. */
    private String couleurFond = StringUtils.EMPTY;

    /** The vecteur onglets. */
    private Vector<String> vecteurOnglets = new Vector<>();

    /**
     * Commentaire relatif au constructeur InfosBandeau.
     */
    public InfosBandeau() {
        super();
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:13:49)
     *
     * @return java.lang.String
     */
    public String getCouleurFond() {
        return couleurFond;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:13:28)
     *
     * @return java.lang.String
     */
    public String getCouleurTitre() {
        return couleurTitre;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:12:34)
     *
     * @return java.lang.String
     */
    public String getUrlBandeau() {
        return urlBandeau;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 16:01:08)
     *
     * @return java.util.Vector
     */
    public Vector<String> getVecteurOnglets() {
        return vecteurOnglets;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:13:49)
     *
     * @param newCouleurFond
     *            java.lang.String
     */
    public void setCouleurFond(final String newCouleurFond) {
        couleurFond = newCouleurFond;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:13:28)
     *
     * @param newCouleurTitre
     *            java.lang.String
     */
    public void setCouleurTitre(final String newCouleurTitre) {
        couleurTitre = newCouleurTitre;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 15:12:34)
     *
     * @param newUrlBandeau
     *            java.lang.String
     */
    public void setUrlBandeau(final String newUrlBandeau) {
        urlBandeau = newUrlBandeau;
    }

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (16/12/2001 16:01:08)
     *
     * @param newVecteurOnglets
     *            java.util.Vector
     */
    public void setVecteurOnglets(final Vector<String> newVecteurOnglets) {
        vecteurOnglets = newVecteurOnglets;
    }
}