/*
 * Created on 10 mai 2005
 *
 * Description des requêtes associées aux groupes dynamiques
 */
package com.univ.objetspartages.om;

import java.io.Serializable;

import org.apache.commons.lang3.StringUtils;

/**
 *
 *
 * @author jeanseb
 */
public class InfosRequeteGroupe implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = -2131607179174243523L;

    /** The alias. */
    private String alias = StringUtils.EMPTY;

    /** The intitule. */
    private String intitule = StringUtils.EMPTY;

    /** The nom classe. */
    private String nomClasse = StringUtils.EMPTY;

    private String templateJSP = StringUtils.EMPTY;

    /** The ts cache groupes utilisateur. */
    private long tsCacheGroupesUtilisateur = 0;

    /**
     * The Constructor.
     *
     * @param alias
     *            the alias
     * @param intitule
     *            the intitule
     * @param nomClasse
     *            the nom classe
     * @param tsCacheGroupesUtilisateur
     *            the ts cache groupes utilisateur
     */
    public InfosRequeteGroupe(final String alias, final String intitule, final String nomClasse, final long tsCacheGroupesUtilisateur, final String templateJSP) {
        super();
        this.alias = alias;
        this.intitule = intitule;
        this.nomClasse = nomClasse;
        this.tsCacheGroupesUtilisateur = tsCacheGroupesUtilisateur;
        this.templateJSP = templateJSP;
    }

    /**
     * Instantiates a new infos requete groupe.
     */
    public InfosRequeteGroupe() {
        super();
    }

    /**
     * Gets the alias.
     *
     * @return Returns the alias.
     */
    public String getAlias() {
        return alias;
    }

    /**
     * Sets the alias.
     *
     * @param alias
     *            The alias to set.
     */
    public void setAlias(final String alias) {
        this.alias = alias;
    }

    /**
     * Gets the intitule.
     *
     * @return Returns the intitule.
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Sets the intitule.
     *
     * @param intitule
     *            The intitule to set.
     */
    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    /**
     * Gets the nom classe.
     *
     * @return Returns the nomClasse.
     */
    public String getNomClasse() {
        return nomClasse;
    }

    /**
     * Sets the nom classe.
     *
     * @param nomClasse
     *            The nomClasse to set.
     */
    public void setNomClasse(final String nomClasse) {
        this.nomClasse = nomClasse;
    }

    /**
     * Gets the ts cache groupes utilisateur.
     *
     * @return the ts cache groupes utilisateur
     */
    public long getTsCacheGroupesUtilisateur() {
        return tsCacheGroupesUtilisateur;
    }

    public String getTemplateJSP() {
        return templateJSP;
    }

    public void setTemplateJSP(final String templateJSP) {
        this.templateJSP = templateJSP;
    }
}
