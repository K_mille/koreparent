package com.univ.objetspartages.om;

import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.config.PropertyHelper;

/**
 * Classe centralisant les traitements sur les paragraphes et les différentes lignes des toolbox
 *
 * @author olivier.camon
 *
 */
public class ParagrapheContenuHelper {

    public static Collection<LigneContenu> getLignes(final String contenu) {
        final TreeMap<Integer, LigneContenu> lignes = new TreeMap<>();
        ParagrapheBean paragraphe = null;
        int indexTexte = 0;
        int indexDebutMotCle = -1;
        int indexFinMotCle = 0;
        int indexFinParagraphe = 0;
        String itemTag = null;
        /* Boucle sur chaque paragraphe */
        while ((indexDebutMotCle = contenu.indexOf("[paragraphe", indexTexte)) != -1) {
            paragraphe = new ParagrapheBean();
            indexFinMotCle = contenu.indexOf(']', indexDebutMotCle);
            if (indexFinMotCle != -1) {
                // Analyse du tag paragraphe
                final String contenuTag = contenu.substring(indexDebutMotCle + 1, indexFinMotCle);
                final StringTokenizer st = new StringTokenizer(contenuTag, ";");
                int indiceToken = 0;
                while (st.hasMoreTokens()) {
                    itemTag = st.nextToken();
                    if (indiceToken == 1) {
                        paragraphe.setLigne(Integer.parseInt(itemTag));
                    }
                    if (indiceToken == 2) {
                        paragraphe.setColonne(Integer.parseInt(itemTag));
                    }
                    if (indiceToken == 3) {
                        paragraphe.setLargeur(Integer.parseInt(itemTag));
                    }
                    indiceToken++;
                }
                /* Recherche de la fin du paragraphe */
                indexFinParagraphe = contenu.indexOf("[/paragraphe]", indexDebutMotCle);
                if (indexFinParagraphe != -1) {
                    /* Stockage du paragraphe */
                    paragraphe.setContenu(contenu.substring(indexFinMotCle + 1, indexFinParagraphe));
                    LigneContenu ligne = lignes.get(paragraphe.getLigne());
                    if (ligne == null) {
                        ligne = new LigneContenu(paragraphe.getLigne());
                        lignes.put(paragraphe.getLigne(), ligne);
                    }
                    ligne.addParagraphe(paragraphe);
                }
                // SUPPRESSION DE LA BOUCLE QUI TUE LE SERVEUR
                // SI ON NE TROUVE PAS LA FIN DU PARAGRAPHE, ON CREE UN
                // PARAGRAPHE VIDE
                else {
                    paragraphe.setContenu("");
                    LigneContenu ligne = lignes.get(paragraphe.getLigne());
                    if (ligne == null) {
                        ligne = new LigneContenu(paragraphe.getLigne());
                        lignes.put(paragraphe.getLigne(), ligne);
                    }
                    ligne.addParagraphe(paragraphe);
                    indexFinParagraphe = contenu.length() - 13;
                }
            }
            indexTexte = indexFinParagraphe + 12;
        }
        return lignes.values();
    }

    /**
     * Renvoie la liste des paragraphes.
     *
     * @return the paragraphes
     * @deprecated
     *
     */
    @Deprecated
    public static Vector<ParagrapheBean> getParagraphes(final String contenu) {
        final Vector<ParagrapheBean> listeParagraphes = new Vector<>();
        if(StringUtils.isNotBlank(contenu)) {
            ParagrapheBean paragraphe = null;
            int indexTexte = 0;
            int indexDebutMotCle = -1;
            int indexFinMotCle = 0;
            int indexFinParagraphe = 0;
            String itemTag = null;
            /* Boucle sur chaque paragraphe */
            while ((indexDebutMotCle = contenu.indexOf("[paragraphe", indexTexte)) != -1) {
                paragraphe = new ParagrapheBean();
                indexFinMotCle = contenu.indexOf(']', indexDebutMotCle);
                if (indexFinMotCle != -1) {
                    /* Analyse du tag paragraphe */
                    final String contenuTag = contenu.substring(indexDebutMotCle + 1, indexFinMotCle);
                    final StringTokenizer st = new StringTokenizer(contenuTag, ";");
                    int indiceToken = 0;
                    while (st.hasMoreTokens()) {
                        itemTag = st.nextToken();
                        if (indiceToken == 1) {
                            paragraphe.setLigne(Integer.parseInt(itemTag));
                        }
                        if (indiceToken == 2) {
                            paragraphe.setColonne(Integer.parseInt(itemTag));
                        }
                        if (indiceToken == 3) {
                            paragraphe.setLargeur(Integer.parseInt(itemTag));
                        }
                        indiceToken++;
                    }
                   /* Recherche de la fin du paragraphe */
                    indexFinParagraphe = contenu.indexOf("[/paragraphe]", indexDebutMotCle);
                    if (indexFinParagraphe != -1) {
                        /* Stockage du paragraphe */
                        paragraphe.setContenu(contenu.substring(indexFinMotCle + 1, indexFinParagraphe));
                        listeParagraphes.add(paragraphe);
                    }
                    // suppression de la boucle qui tue le serveur
                    // si on ne trouve pas la fin du paragraphe, on cree un
                    // paragraphe vide
                    else {
                        paragraphe.setContenu("");
                        listeParagraphes.add(paragraphe);
                        indexFinParagraphe = contenu.length() - 13;
                    }
                }
                indexTexte = indexFinParagraphe + 12;
            }
        }
        return listeParagraphes;
    }

    /**
     * Stocke la liste des paragraphes triés.
     *
     * @param _listeParagraphes
     *            the _liste paragraphes
     *
     * @throws ErreurApplicative
     *             the exception
     */
    public static String formateParagraphes(final Collection<ParagrapheBean> _listeParagraphes) throws ErreurApplicative {
        /* Tri des paragraphes */
        ParagrapheBean paragraphe = null;
        final TreeMap<Integer, TreeMap<Integer, ParagrapheBean>> listeLigne = new TreeMap<>();
        TreeMap<Integer, ParagrapheBean> listeColonne = null;
        final Iterator<ParagrapheBean> it = _listeParagraphes.iterator();
        Iterator<ParagrapheBean> itCol = null;
        while (it.hasNext()) {
            paragraphe = it.next();
            listeColonne = listeLigne.get(Integer.valueOf(paragraphe.getLigne()));
            if (listeColonne == null) {
                listeColonne = new TreeMap<>();
                listeLigne.put(paragraphe.getLigne(), listeColonne);
            }
            // controle sur l'unicite du paragraphe
            if (listeColonne.containsKey(Integer.valueOf(paragraphe.getColonne()))) {
                throw new ErreurApplicative("Ce paragraphe existe déjà, vérifiez les numéros de ligne et colonne.");
            }
            // controle sur la largeur de la ligne
            int largeurLigne = paragraphe.getLargeur();
            if (!listeColonne.isEmpty()) {
                itCol = listeColonne.values().iterator();
                while (itCol.hasNext()) {
                    largeurLigne += itCol.next().getLargeur();
                }
            }
            if (largeurLigne > 100) {
                throw new ErreurApplicative("La largeur de la ligne " + paragraphe.getLigne() + " dépasse 100%.");
            }
            // on ajoute le paragraphe
            listeColonne.put(paragraphe.getColonne(), paragraphe);
        }
        /* Stockage des paragraphes */
        final Iterator<TreeMap<Integer, ParagrapheBean>> itBis = listeLigne.values().iterator();
        Iterator<ParagrapheBean> itColBis = null;
        final StringBuilder contenu = new StringBuilder();
        while (itBis.hasNext()) {
            itColBis = itBis.next().values().iterator();
            while (itColBis.hasNext()) {
                paragraphe = itColBis.next();
                contenu.append("[paragraphe;").append(paragraphe.getLigne()).append(";").append(paragraphe.getColonne()).append(";").append(paragraphe.getLargeur()).append("]").append(paragraphe.getContenu()).append("[/paragraphe]");
            }
        }
        final String nbMax = PropertyHelper.getCoreProperty("paragraphe.caracteremax");
        int caractereMax = 65535;
        if (StringUtils.isNotBlank(nbMax) && StringUtils.isNumeric(nbMax)) {
            caractereMax = Integer.valueOf(nbMax);
        }
        if (contenu.length() >= caractereMax) {
            throw new ErreurApplicative(String.format("%s %s%s %s", MessageHelper.getCoreMessage("BO_NB_CAR_MAX_PARAGRAPHE"), caractereMax, MessageHelper.getCoreMessage("JTF_ERR_FMT_NB_CAR_MAX_3"), contenu.length()));
        }
        return contenu.toString();
    }
}
