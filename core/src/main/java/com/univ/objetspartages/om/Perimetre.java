package com.univ.objetspartages.om;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;

/**
 * The Class Perimetre.
 *
 * @author jean-sébastien steux
 *
 *         stockage, formatage et recherche d'un périmètre de délégation Un périmètre est une combinaison (ET logique) d'une structure d'une rubrique et d'un groupe
 */
public class Perimetre {

    /** The code structure. */
    private String codeStructure = "";

    /** The code rubrique. */
    private String codeRubrique = "";

    /** The code profil. */
    private String codeProfil = "";

    /** The code groupe. */
    private String codeGroupe = "";

    /** The code espace collaboratif. */
    private String codeEspaceCollaboratif = "";

    /** The identifiant niveau. */
    private String identifiantNiveau = "";

    /**
     * Intialisation à partir de la chaine en base de donnée (sous la forme STRUCTURE/RUBRIQUE/PROFIL/GROUPE/ESPACE).
     *
     * @param chaineSerialisee
     *            the chaine serialisee
     */
    public Perimetre(final String chaineSerialisee) {
        super();
        codeStructure = "";
        codeRubrique = "";
        codeProfil = "";
        codeGroupe = "";
        codeEspaceCollaboratif = "";
        String chaineTmp = chaineSerialisee;
        int indice = 0;
        while (chaineTmp.length() > 0) {
            String item = "";
            // Extraction du premier item
            final int iSlash = chaineTmp.indexOf('/');
            if (iSlash != -1) {
                item = chaineTmp.substring(0, iSlash);
                chaineTmp = chaineTmp.substring(iSlash + 1);
            } else {
                item = chaineTmp;
                chaineTmp = "";
            }
            switch (indice) {
                case 0:
                    codeStructure = item;
                    break;
                case 1:
                    codeRubrique = item;
                    break;
                case 2:
                    codeProfil = item;
                    break;
                case 3:
                    codeGroupe = item;
                    break;
                case 4:
                    codeEspaceCollaboratif = item;
                    break;
            }
            indice++;
        }
        identifiantNiveau = codeStructure + "/" + codeRubrique + "/" + codeGroupe + "/" + codeEspaceCollaboratif;
    }

    /**
     * Intialisation à partir des valeurs saisies.
     *
     * @param _codeStructure
     *            the _code structure
     * @param _codeRubrique
     *            the _code rubrique
     * @param _codeProfil
     *            the _code profil
     * @param _codeGroupe
     *            the _code groupe
     * @param _codeEspaceCollaboratif
     *            the _code espace collaboratif
     */
    public Perimetre(final String _codeStructure, final String _codeRubrique, final String _codeProfil, final String _codeGroupe, final String _codeEspaceCollaboratif) {
        codeStructure = _codeStructure;
        codeRubrique = _codeRubrique;
        codeProfil = _codeProfil;
        codeGroupe = _codeGroupe;
        codeEspaceCollaboratif = _codeEspaceCollaboratif;
        identifiantNiveau = codeStructure + "/" + codeRubrique + "/" + codeGroupe + "/" + codeEspaceCollaboratif;
    }

    /**
     * Calcul du périmètre d'une permission . COnsiste à appliquer le masque de l'affectation sur le périmètre du role
     *
     * @param perimetreAffectation
     *            the perimetre affectation
     * @param perimetreRole
     *            the perimetre role
     *
     * @return the perimetre
     *
     */
    public static Perimetre calculerPerimetrePermission(final Perimetre perimetreAffectation, final Perimetre perimetreRole) {
        boolean isRubOk = false;
        boolean isStructOk = false;
        String codeStructure = perimetreRole.getCodeStructure();
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        if (perimetreAffectation.getCodeStructure().length() > 0 && !"-".equals(perimetreAffectation.getCodeStructure())) {
            codeStructure = perimetreAffectation.getCodeStructure();
            // RP20070707 controle de l'existance des données du périmètre
            // si une des données n'existe pas on ignore le périmètre et donc le rôle associé
            final StructureModele infosStruct = serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getIndiceLocaleDefaut());
            if (infosStruct != null && StringUtils.isNotBlank(infosStruct.getCode())) {
                isStructOk = true;
            }
        }
        String codeRubrique = perimetreRole.getCodeRubrique();
        if (perimetreAffectation.getCodeRubrique().length() > 0 && !"-".equals(perimetreAffectation.getCodeRubrique())) {
            codeRubrique = perimetreAffectation.getCodeRubrique();
            // RP20070707 controle de l'existance des données du périmètre
            // si une des données n'existe pas on ignore le périmètre et donc le rôle associé
            if (serviceRubrique.getRubriqueByCode(codeRubrique) != null) {
                isRubOk = true;
            }
        }
        String codeGroupe = perimetreRole.getCodeGroupe();
        if (perimetreAffectation.getCodeGroupe().length() > 0) {
            codeGroupe = perimetreAffectation.getCodeGroupe();
        }
        String codeProfil = perimetreRole.getCodeProfil();
        if (perimetreAffectation.getCodeProfil().length() > 0) {
            codeProfil = perimetreAffectation.getCodeProfil();
        }
        String codeEspaceCollabortif = perimetreRole.getCodeEspaceCollaboratif();
        if (perimetreAffectation.getCodeEspaceCollaboratif().length() > 0) {
            codeEspaceCollabortif = perimetreAffectation.getCodeEspaceCollaboratif();
        }
        // RP20070707 controle de l'existance des données du périmètre
        // si une des données n'existe pas on ignore le périmètre et donc le rôle associé
        if (!isRubOk) {
            final RubriqueBean rub = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (StringUtils.isNotBlank(codeRubrique) && rub == null && !"-".equals(codeRubrique)) {
                return null;
            }
        }
        if (!isStructOk) {
            final StructureModele infosStruct = serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getIndiceLocaleDefaut());
            if (codeStructure.length() > 0 && (infosStruct != null && infosStruct.getCode().length() == 0) && !"-".equals(codeStructure)) {
                return null;
            }
        }
        return new Perimetre(codeStructure, codeRubrique, codeProfil, codeGroupe, codeEspaceCollabortif);
    }

    /**
     * Formatage de la chaine sérialisée.
     *
     * @return the chaine serialisee
     */
    public String getChaineSerialisee() {
        String res = "";
        if (getCodeStructure().length() > 0) {
            res += getCodeStructure();
        }
        res += "/";
        if (getCodeRubrique().length() > 0) {
            res += getCodeRubrique();
        }
        res += "/";
        if (getCodeProfil().length() > 0) {
            res += getCodeProfil();
        }
        res += "/";
        if (getCodeGroupe().length() > 0) {
            res += getCodeGroupe();
        }
        res += "/";
        if (getCodeEspaceCollaboratif().length() > 0) {
            res += getCodeEspaceCollaboratif();
        }
        return res;
    }

    /**
     * Sets the code groupe.
     *
     * @param codeGroupe
     *            The codeGroupe to set.
     */
    public void setCodeGroupe(final String codeGroupe) {
        this.codeGroupe = codeGroupe;
    }

    /**
     * Gets the code groupe.
     *
     * @return Returns the codeGroupe.
     */
    public String getCodeGroupe() {
        return codeGroupe;
    }

    /**
     * Gets the code rubrique.
     *
     * @return Returns the codeRubrique.
     */
    public String getCodeRubrique() {
        return codeRubrique;
    }

    /**
     * Gets the code structure.
     *
     * @return Returns the codeStructure.
     */
    public String getCodeStructure() {
        return codeStructure;
    }

    /**
     * Gets the code profil.
     *
     * @return Returns the codeProfil.
     */
    public String getCodeProfil() {
        return codeProfil;
    }

    /**
     * Controle si le périmètre courant est conforme à un périmètre de rôle
     *
     * périmètre courant :
     *
     * "" signifie vide (pas de valeur) "*" signifie 'champ à ignorer'
     *
     *
     * périmètre du role
     *
     * "" signifie tous (toutes valeurs autorisées) "-" signifie aucune valeur autorisée.
     *
     * @param perimetre
     *            the perimetre
     *
     * @return true, if est conforme au perimetre
     *
     */
    public boolean estConformeAuPerimetre(final Perimetre perimetre) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        boolean estUnSousPerimetre = true;
        if (!"*".equals(getCodeStructure())) {
            if (StringUtils.isNotBlank(perimetre.getCodeStructure())) {
                estUnSousPerimetre = false;
                if ("-".equals(perimetre.getCodeStructure())) {
                    estUnSousPerimetre = StringUtils.isEmpty(getCodeStructure());
                } else if (perimetre.getCodeStructure().equals(getCodeStructure())) {
                    estUnSousPerimetre = true;
                } else {
                    final List<StructureModele> subStructures = serviceStructure.getAllSubStructures(perimetre.getCodeStructure(), LangueUtil.getIndiceLocaleDefaut(), true);
                    for(StructureModele currentStructure : subStructures) {
                        if (currentStructure.getCode().equals(getCodeStructure())) {
                            estUnSousPerimetre = true;
                            break;
                        }
                    }
                }
            }
            if (!estUnSousPerimetre) {
                return false;
            }
        }
        if (!"*".equals(getCodeRubrique())) {
            if (StringUtils.isNotBlank(perimetre.getCodeRubrique())) {
                estUnSousPerimetre = false;
                if ("-".equals(perimetre.getCodeRubrique())) {
                    estUnSousPerimetre = StringUtils.isEmpty(getCodeRubrique());
                } else {
                    if (perimetre.getCodeRubrique().equals(getCodeRubrique())) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-rubriques
                        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                        final RubriqueBean rubriquePerimetre = serviceRubrique.getRubriqueByCode(perimetre.getCodeRubrique());
                        final RubriqueBean rubriqueThis = serviceRubrique.getRubriqueByCode(getCodeRubrique());
                        estUnSousPerimetre = serviceRubrique.isParentSection(rubriquePerimetre, rubriqueThis);
                    }
                }
            }
            if (!estUnSousPerimetre) {
                return false;
            }
        }
        if (!"*".equals(getCodeGroupe())) {
            if (StringUtils.isNotBlank(perimetre.getCodeGroupe())) {
                estUnSousPerimetre = false;
                if ("-".equals(perimetre.getCodeGroupe())) {
                    estUnSousPerimetre = StringUtils.isEmpty(getCodeGroupe());
                } else {
                    if (perimetre.getCodeGroupe().equals(getCodeGroupe())) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-groupes
                        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                        final GroupeDsiBean perimeterGroup = serviceGroupeDsi.getByCode(perimetre.getCodeGroupe());
                        final GroupeDsiBean currentGroup = serviceGroupeDsi.getByCode(getCodeGroupe());
                        estUnSousPerimetre = serviceGroupeDsi.contains(perimeterGroup, currentGroup);
                    }
                }
                if (!estUnSousPerimetre) {
                    return false;
                }
            }
        }
        if (!"*".equals(getCodeEspaceCollaboratif())) {
            if (StringUtils.isNotBlank(perimetre.getCodeEspaceCollaboratif())) {
                if (!perimetre.getCodeEspaceCollaboratif().equals(getCodeEspaceCollaboratif())) {
                    estUnSousPerimetre = false;
                }
            }
        }
        return estUnSousPerimetre;
    }

    /**
     * Controle si le périmètre courant est un sous-périmètre du périmètre en paramètre
     *
     * périmètre courant :
     *
     * "" signifie tous (toutes valeurs autorisées) "-" signifie aucune valeur autorisée
     *
     *
     * périmètre du role
     *
     * "" signifie tous (toutes valeurs autorisées) "-" signifie aucune valeur autorisée.
     *
     * @param perimetre
     *            the perimetre
     * @param masque
     *            the masque
     *
     * @return true, if est un sous perimetre de
     *
     */
    public boolean estUnSousPerimetreDe(final Perimetre perimetre, final boolean masque) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        boolean estUnSousPerimetre = true;
        if ((!masque) || (codeStructure.length() > 0)) {
            final String codeStructurePerimetre = perimetre.getCodeStructure();
            if (codeStructurePerimetre.length() > 0) {
                estUnSousPerimetre = false;
                if ("-".equals(codeStructurePerimetre)) {
                    if ("-".equals(codeStructure)) {
                        estUnSousPerimetre = true;
                    }
                } else if (codeStructure.equals(codeStructurePerimetre)) {
                    estUnSousPerimetre = true;
                } else {
                    // Parcours des sous-structures
                    /* Iterator listeStructures = Structure.renvoyerItemStructure(perimetre.getCodeStructure())
                     .getListeSousStructuresTousNiveaux().iterator();
                     while (listeStructures.hasNext()) {
                     if (((InfosStructure)listeStructures.next()).getCode().equals(getCodeStructure()))
                     estUnSousPerimetre = true;
                     }*/
                    if (serviceStructure.contains(serviceStructure.getByCodeLanguage(codeStructurePerimetre, LangueUtil.getIndiceLocaleDefaut()), serviceStructure.getByCodeLanguage(codeStructure, LangueUtil.getIndiceLocaleDefaut()))) {
                        estUnSousPerimetre = true;
                    }
                }
            }
            if (!estUnSousPerimetre) {
                return false;
            }
        }
        if ((!masque) || (codeRubrique.length() > 0)) {
            final String codeRubriquePerimetre = perimetre.getCodeRubrique();
            if (codeRubriquePerimetre.length() > 0) {
                estUnSousPerimetre = false;
                if ("-".equals(codeRubriquePerimetre)) {
                    if ("-".equals(codeRubrique)) {
                        estUnSousPerimetre = true;
                    }
                } else {
                    if (codeRubrique.equals(codeRubriquePerimetre)) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-rubriques
                        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                        final RubriqueBean rubriquePerimetre = serviceRubrique.getRubriqueByCode(codeRubriquePerimetre);
                        final RubriqueBean rubriqueThis = serviceRubrique.getRubriqueByCode(getCodeRubrique());
                        estUnSousPerimetre = serviceRubrique.isParentSection(rubriquePerimetre, rubriqueThis);
                    }
                }
            }
            if (!estUnSousPerimetre) {
                return false;
            }
        }
        if ((!masque) || (codeGroupe.length() > 0)) {
            final String codeGroupePerimetre = perimetre.getCodeGroupe();
            if (codeGroupePerimetre.length() > 0) {
                estUnSousPerimetre = false;
                if ("-".equals(codeGroupePerimetre)) {
                    if ("-".equals(codeGroupe)) {
                        estUnSousPerimetre = true;
                    }
                } else {
                    if (codeGroupe.equals(codeGroupePerimetre)) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-groupes
                        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                        final GroupeDsiBean perimeterGroup = serviceGroupeDsi.getByCode(codeGroupePerimetre);
                        final GroupeDsiBean currentGroup = serviceGroupeDsi.getByCode(codeGroupe);
                        estUnSousPerimetre = serviceGroupeDsi.contains(perimeterGroup, currentGroup);
                    }
                }
            }
        }
        if ((!masque) || (getCodeEspaceCollaboratif().length() > 0)) {
            if (perimetre.getCodeEspaceCollaboratif().length() > 0) {
                if (!getCodeEspaceCollaboratif().equals(perimetre.getCodeEspaceCollaboratif())) {
                    estUnSousPerimetre = false;
                }
            }
        }
        return estUnSousPerimetre;
    }

    /**
     * Controle si le périmètre courant est conforme à un périmètre de rôle
     *
     * périmètre courant :
     *
     * "" signifie vide (pas de valeur)
     *
     *
     * périmètre du role
     *
     * "" signifie tous (toutes valeurs autorisées) "-" signifie aucune valeur autorisée.
     *
     * @param perimetre
     *            the perimetre
     *
     * @return true, if est partiellement conforme au perimetre
     *
     */
    public boolean estPartiellementConformeAuPerimetre(final Perimetre perimetre) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        boolean estUnSousPerimetre = false;
        if (getCodeStructure().length() > 0) {
            if (perimetre.getCodeStructure().length() > 0) {
                // JSS 20051104 : gestion des périmètres
                if (!"-".equals(perimetre.getCodeStructure())) {
                    if (getCodeStructure().equals(perimetre.getCodeStructure())) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-structures
                        final List<StructureModele> subStructures = serviceStructure.getAllSubStructures(perimetre.getCodeStructure(), LangueUtil.getIndiceLocaleDefaut(), true);
                        for (StructureModele infosStructure : subStructures) {
                            if (infosStructure.getCode().equals(getCodeStructure())) {
                                estUnSousPerimetre = true;
                            }
                        }
                    }
                }
            } else {
                estUnSousPerimetre = true;
            }
        }
        if (estUnSousPerimetre) {
            return true;
        }
        if (getCodeRubrique().length() > 0) {
            if (perimetre.getCodeRubrique().length() > 0) {
                // JSS 20051104 : gestion des périmètres
                if (!"-".equals(perimetre.getCodeRubrique())) {
                    if (getCodeRubrique().equals(perimetre.getCodeRubrique())) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-rubriques
                        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
                        final RubriqueBean rubriquePerimetre = serviceRubrique.getRubriqueByCode(perimetre.getCodeRubrique());
                        final RubriqueBean rubriqueThis = serviceRubrique.getRubriqueByCode(getCodeRubrique());
                        estUnSousPerimetre = serviceRubrique.isParentSection(rubriquePerimetre, rubriqueThis);
                    }
                }
            } else {
                estUnSousPerimetre = true;
            }
        }
        if (estUnSousPerimetre) {
            return true;
        }
        if (getCodeGroupe().length() > 0) {
            if (perimetre.getCodeGroupe().length() > 0) {
                // JSS 20051104 : gestion des périmètres
                if (!"-".equals(perimetre.getCodeGroupe())) {
                    if (getCodeGroupe().equals(perimetre.getCodeGroupe())) {
                        estUnSousPerimetre = true;
                    } else {
                        // Parcours des sous-groupes
                        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                        final GroupeDsiBean perimeterGroup = serviceGroupeDsi.getByCode(perimetre.getCodeGroupe());
                        final GroupeDsiBean currentGroup = serviceGroupeDsi.getByCode(getCodeGroupe());
                        estUnSousPerimetre = serviceGroupeDsi.contains(perimeterGroup, currentGroup);
                    }
                }
            } else {
                estUnSousPerimetre = true;
            }
        }
        if (getCodeEspaceCollaboratif().length() > 0) {
            if (perimetre.getCodeEspaceCollaboratif().length() > 0) {
                if (getCodeEspaceCollaboratif().equals(perimetre.getCodeEspaceCollaboratif())) {
                    estUnSousPerimetre = true;
                }
            } else {
                estUnSousPerimetre = true;
            }
        }
        return estUnSousPerimetre;
    }

    /**
     * Sets the code profil.
     *
     * @param codeProfil
     *            The codeProfil to set.
     */
    public void setCodeProfil(final String codeProfil) {
        this.codeProfil = codeProfil;
    }

    /**
     * Gets the code espace collaboratif.
     *
     * @return Returns the codeEspaceCollaboratif.
     */
    public String getCodeEspaceCollaboratif() {
        return codeEspaceCollaboratif;
    }

    /**
     * Sets the code espace collaboratif.
     *
     * @param codeEspaceCollaboratif
     *            The codeEspaceCollaboratif to set.
     */
    public void setCodeEspaceCollaboratif(final String codeEspaceCollaboratif) {
        this.codeEspaceCollaboratif = codeEspaceCollaboratif;
    }

    /**
     * Equals.
     *
     * @param perimetre
     *            the perimetre
     *
     * @return true, if successful
     */
    public boolean equals(final Perimetre perimetre) {
        return getChaineSerialisee().equals(perimetre.getChaineSerialisee());
    }

    /**
     * Gets the identifiant niveau.
     *
     * @return Returns the identifiantNiveau.
     */
    public String getIdentifiantNiveau() {
        return identifiantNiveau;
    }

    /**
     * Méthode toString
     *
     * @return toString
     */
    @Override
    public String toString() {
        return getChaineSerialisee();
    }
}
