package com.univ.objetspartages.om;

import java.util.List;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * Insérez la description du type à cet endroit. Date de création : (11/04/2003 09:11:13)
 *
 * @author :
 */
public interface RechercheExterne {

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/04/2003 09:11:40)
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @return true, if preparer recherche
     *
     * @throws Exception
     *             the exception
     */
    boolean preparerRECHERCHE(InfoBean infoBean, OMContext ctx) throws Exception;

    /**
     * Insérez la description de la méthode à cet endroit. Date de création : (11/04/2003 09:11:40)
     *
     * @param infoBean
     *            the info bean
     * @param ctx
     *            the ctx
     *
     * @return true, if traiter recherche
     *
     * @throws Exception
     *             the exception
     */
    boolean traiterRECHERCHE(InfoBean infoBean, OMContext ctx) throws Exception;

    List<String> getCriteresRequete(String objet, boolean listeIncluse);
}