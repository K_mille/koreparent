package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

/**
 * Bean de stockage des données SSO.
 */
public class SSOBean {

    /** The id. */
    private java.lang.String id = "";

    /** The nom. */
    private java.lang.String nom = "";

    /** The prenom. */
    private java.lang.String prenom = "";

    /** The civilite. */
    private java.lang.String civilite = "";

    /** The email. */
    private java.lang.String email = "";

    /** The structure. */
    private java.lang.String structure = "";

    /** The profil. */
    private java.lang.String profil = "";

    /** The groupes. */
    private java.lang.String groupes = "";

    /** The code kportal. */
    private java.lang.String codeKportal = "";

    /** The code gestion. */
    private java.lang.String codeGestion = "";

    /** The ksession. */
    private java.lang.String ksession = "";

    /** The code retour. */
    private java.lang.String codeRetour = "";

    /**
     * Commentaire relatif au constructeur SSOBean.
     */
    public SSOBean() {
        super();
    }

    /**
     * Gets the id.
     *
     * @return id
     */
    public java.lang.String getId() {
        return id;
    }

    /*
     * Set the id.
     */
    public void setId(java.lang.String id) {
        this.id = id;
    }

    /**
     * Gets the civilite.
     *
     * @return the civilite
     */
    public java.lang.String getCivilite() {
        return civilite;
    }

    /**
     * Gets the code gestion.
     *
     * @return the code gestion
     */
    public java.lang.String getCodeGestion() {
        return codeGestion;
    }

    /**
     * Gets the code kportal.
     *
     * @return the code kportal
     */
    public java.lang.String getCodeKportal() {
        return codeKportal;
    }

    /**
     * Gets the email.
     *
     * @return the email
     */
    public java.lang.String getEmail() {
        return email;
    }

    /**
     * Gets the groupes.
     *
     * @return the groupes
     */
    public java.lang.String getGroupes() {
        return groupes;
    }

    /**
     * Gets the nom.
     *
     * @return the nom
     */
    public java.lang.String getNom() {
        return nom;
    }

    /**
     * Gets the prenom.
     *
     * @return the prenom
     */
    public java.lang.String getPrenom() {
        return prenom;
    }

    /**
     * Gets the profil.
     *
     * @return the profil
     */
    public java.lang.String getProfil() {
        return profil;
    }

    /**
     * Gets the structure.
     *
     * @return the structure
     */
    public java.lang.String getStructure() {
        return structure;
    }

    /**
     * Sets the civilite.
     *
     * @param newCivilite
     *            the new civilite
     */
    public void setCivilite(java.lang.String newCivilite) {
        civilite = newCivilite;
    }

    /**
     * Sets the code gestion.
     *
     * @param newCodeGestion
     *            the new code gestion
     */
    public void setCodeGestion(java.lang.String newCodeGestion) {
        codeGestion = newCodeGestion;
    }

    /**
     * Sets the code kportal.
     *
     * @param newCodeKportal
     *            the new code kportal
     */
    public void setCodeKportal(java.lang.String newCodeKportal) {
        codeKportal = newCodeKportal;
    }

    /**
     * Sets the email.
     *
     * @param newEmail
     *            the new email
     */
    public void setEmail(java.lang.String newEmail) {
        email = newEmail;
    }

    /**
     * Sets the groupes.
     *
     * @param newGroupes
     *            the new groupes
     */
    public void setGroupes(java.lang.String newGroupes) {
        groupes = newGroupes;
    }

    /**
     * Sets the nom.
     *
     * @param newNom
     *            the new nom
     */
    public void setNom(java.lang.String newNom) {
        nom = newNom;
    }

    /**
     * Sets the prenom.
     *
     * @param newPrenom
     *            the new prenom
     */
    public void setPrenom(java.lang.String newPrenom) {
        prenom = newPrenom;
    }

    /**
     * Sets the profil.
     *
     * @param newProfil
     *            the new profil
     */
    public void setProfil(java.lang.String newProfil) {
        profil = newProfil;
    }

    /**
     * Sets the structure.
     *
     * @param newStructure
     *            the new structure
     */
    public void setStructure(java.lang.String newStructure) {
        structure = newStructure;
    }

    /**
     * Gets the ksession.
     *
     * @return the ksession
     */
    public java.lang.String getKsession() {
        return ksession;
    }

    /**
     * Sets the ksession.
     *
     * @param ksession
     *            the new ksession
     */
    public void setKsession(java.lang.String ksession) {
        this.ksession = ksession;
    }

    /**
     * Gets the code retour.
     *
     * @return the code retour
     */
    public java.lang.String getCodeRetour() {
        return codeRetour;
    }

    /**
     * Sets the code retour.
     *
     * @param codeRetour
     *            the new code retour
     */
    public void setCodeRetour(java.lang.String codeRetour) {
        this.codeRetour = codeRetour;
    }
}