package com.univ.objetspartages.om;
// TODO: Auto-generated Javadoc

/**
 * Un objet de ce type permet de fournir un certain nombre de données afin d'initialiser un groupe dynamique lors d'une synchronisation. L'ensemble des valeurs contenues dans cette
 * classe permet de fournir un ensemble de données utiles aux groupes. Les valeurs minimales sont : le CODE et l'INTITULE.<br/>
 * <br/>
 * <strong>Description de certaines données :</strong><br/>
 * <ul>
 * <li>Le CODE est en faite le code de l'objet qui entraine la synchronisation (exemple : espace collaboratif, structure, ...). Ce CODE sera retravaillé à l'enregistrement du
 * groupe dynamique.</li>
 * <li>Le CODE PARENT de l'ojet est le code du parent de l'objet entrainant la synchronisation et non le code du groupe parent, un traitement sera effectué dessus pour transformer
 * le code de groupe dynamique.</li>
 * </ul>
 * En ce qui concerne les autres données aucun traitement ne sera effectué dessus, il faut donc qu'elles soient correctement construite.
 *
 * @see RequeteGroupeDynamiqueSynchronisable
 * @author Pierre Cosson
 */
public class SynchronisationGroupeDynamiqueData {

    /** Le code est celui de l'objet qui est la source de la synchronisation. Par exemple il peut s'agir du code d'un espace collaboratif, d'une stucture... */
    private String code;

    /**
     * Code de l'objet parent de l'objet qui est la source de synchronisation. Ce code peut être null cela signifie que le groupe sera mis à la position par defaut de la
     * synchronisation.
     */
    private String codeDuParent;

    /** C'est l'intitulé que prendra le groupe synchronisé. */
    private String intitule;

    /** Il s'agit de fournir le code de la structure de rattachement que devra avoir le groupe synchronisé. */
    private String codeStructureCode;

    /** Il s'agit du code de la page de tête du groupe. */
    private String codePageDeTete;

    /** Roles par defaut des utilisateurs du groupe. */
    private String roles;

    /**
     * Constructeur.
     *
     * @param code
     *            Code de l'objet aentrainant la synchronisation.
     * @param intitule
     *            Intitulé du groupe dynamque.
     */
    public SynchronisationGroupeDynamiqueData(final String code, final String intitule) {
        this.code = code;
        this.intitule = intitule;
        this.codeDuParent = null;
        this.codePageDeTete = null;
        this.codeStructureCode = null;
        this.roles = null;
    }

    /**
     * Récupération du code entrainant la synchronisation. Il peut s'agir par exemple du code d'une structure, d'un espace collaboratif..
     *
     * @return le code de l'objet entrainant la synchronisation.
     */
    public String getCode() {
        return code;
    }

    /**
     * Récupération du code parent de l'objet étant la source de la synchronisation. Il s'agit bien d'un code d'objet et non d'un code de groupe dynamique. Ce code, s'il est
     * défini, pourra servir à transposer l'arborescence initiale des objets aux groupes.
     *
     * @return le code parent de l'objet entrainant la synchronisation.
     */
    public String getCodeDuParent() {
        return codeDuParent;
    }

    /**
     * Permet de donner le code du parent de l'obejt entrainant la synchronisation. L'utilisation de ce code peut permettre de transposer l'arborescence des objets aux groupes.
     *
     * @param codeDuParent
     *            le code du parent de l'objet entrainant la synchronisation.
     */
    public void setCodeDuParent(final String codeDuParent) {
        this.codeDuParent = codeDuParent;
    }

    /**
     * Récupérer l'intitulé du groupe.
     *
     * @return L'intitulé du groupe.
     */
    public String getIntitule() {
        return intitule;
    }

    /**
     * Spécifier l'intitulé du groupe.
     *
     * @param intitule
     *            L'intitulé qu'aura le groupe dynamique.
     */
    public void setIntitule(final String intitule) {
        this.intitule = intitule;
    }

    /**
     * Récupérer le code de structure à affecter au groupe dynamique.
     *
     * @return le code de structure.
     */
    public String getCodeStructureCode() {
        return codeStructureCode;
    }

    /**
     * Affecter un code de structure de rattachement à un groupe.
     *
     * @param codeStructureCode
     *            Le code de la structure à laquelle sera rattachée le goupe.
     */
    public void setCodeStructureCode(final String codeStructureCode) {
        this.codeStructureCode = codeStructureCode;
    }

    /**
     * Récuépérer le code de la page de tête à affecter au groupe.
     *
     * @return le code de la page de tête.
     */
    public String getCodePageDeTete() {
        return codePageDeTete;
    }

    /**
     * Affecter une page de tête au groupe.
     *
     * @param codePageDeTete
     *            Le code de la page de tête du groupe.
     */
    public void setCodePageDeTete(final String codePageDeTete) {
        this.codePageDeTete = codePageDeTete;
    }

    /**
     * Récupérer la "commande" définissant le ou les rôles à affecter au groupe.
     *
     * @return the roles
     */
    public String getRoles() {
        return roles;
    }

    /**
     * Affecter un ou plusieurs rôles aux utilisateur du groupe.
     *
     * @param roles
     *            la ligne de "commande" définissant le rôle à affecter au groupe.
     */
    public void setRoles(final String roles) {
        this.roles = roles;
    }
}
