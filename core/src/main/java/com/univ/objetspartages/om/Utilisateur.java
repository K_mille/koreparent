package com.univ.objetspartages.om;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.StringTokenizer;
import java.util.TreeSet;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.dao.impl.UtilisateurDAO;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.criterespecifique.ConditionHelper;


/**
 * The Class Utilisateur.
 * @deprecated Cette classe n'a plus lieu d'être. La classe {@link UtilisateurBean} doit être utilisée à la place à travers le service {@link ServiceUser}.
 */
@Deprecated
public class Utilisateur extends AbstractOm<UtilisateurBean, UtilisateurDAO> {
    // JSS 20040409 : délégation
    // Groupe temporaire pour mémoriser dans une fiche utilisateur
    // les groupes au cours d'un traitement impliquant plusieurs fiches utilisateurs
    // (n'est pas chargé ni sauvegardé automatiquement)
    // utilisé dans UpdateFromLdap

    /**
     * CODE DE l'utilisateur anonyme
     */
    public static final String UTILISATEUR_ANONYME = "ANONYME";

    /** The vecteur groupes dsi tmp. */
    private Vector<String> vecteurGroupesDsiTmp;

    // FIXME : garde-fou pour garder la rétro-compatibilité. A supprimer dans la prochaine version.
    // On tape systématiquement sur le contexte pour éviter les problèmes en case de rechargement (la propriété devrait être static...).
    private static ServiceUser getServiceUtilisateur() {
        return ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Renvoie l'adresse mail d'un utilisateur.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     *
     * @return the adresse mail
     *
     * @throws Exception
     *             the exception
     * @deprecated ne pas utiliesr car cette méthode utilise le contexte pour rien. Utiliser la même méthode sans le contexte {@link Utilisateur#getAdresseMail(String)}
     */
    @Deprecated
    public static String getAdresseMail(final OMContext ctx, final String codeUtilisateur) throws Exception {
        return getAdresseMail(codeUtilisateur);
    }

    /**
     *
     * Renvoie l'adresse mail d'un utilisateur.
     *
     * @param codeUtilisateur
     *            le code de l'utilisateur dont on souhaite récupérer le mail
     *
     * @return l'adresse mail de l'utilisateur associé
     *
     * @throws Exception
     *             Lorsque la requête ne peut aboutir
     *
     * @deprecated : utiliser le service "serviceUtilisateur" pour récupérer l'adresse mail {@link ServiceUser#getAdresseMail(String)}.
     *
     */
    @Deprecated
    public static String getAdresseMail(final String codeUtilisateur) throws Exception {
        return getAdresseMail(codeUtilisateur, null);
    }

    /**
     * Renvoie l'adresse mail d'un utilisateur, et si l'utilisateur est anonyme, recup du mail dans le metatag.
     *
     * @param ctx
     *            the _ctx
     * @param codeUtilisateur
     *            the _code utilisateur
     * @param meta
     *            the _meta
     *
     * @return the adresse mail
     *
     * @throws Exception
     *             the exception
     * @deprecated ne pas utiliser car cette méthode utilises le contexte pour rien. Utiliser {@link Utilisateur#getAdresseMail(String, Metatag)}
     */
    @Deprecated
    public static String getAdresseMail(final OMContext ctx, final String codeUtilisateur, final Metatag meta) throws Exception {
        return getServiceUtilisateur().getAdresseMail(codeUtilisateur, meta.getPersistenceBean());
    }

    /**
     * Renvoie l'adresse mail d'un utilisateur, et si l'utilisateur est anonyme, recup du mail dans le metatag.
     *
     * @param codeUtilisateur le code de l'utilisateur dont on souhaite recupérer l'adresse mail
     * @param meta en cas d'utilisateur anonyme, l'adresse mail est stocké dans le meta...
     *
     * @return l'adresse mail de l'utilisateur ou un chaine vide si non trouvé
     *
     * @throws Exception lors des accès en bdd
     *
     * @deprecated : utiliser le service "serviceUtilisateur" pour récupérer l'adresse mail {@link ServiceUser#getAdresseMail(String, com.univ.objetspartages.bean.MetatagBean)}.
     */
    @Deprecated
    public static String getAdresseMail(final String codeUtilisateur, final Metatag meta) throws Exception {
        if (meta != null) {
            return getServiceUtilisateur().getAdresseMail(codeUtilisateur, meta.getPersistenceBean());
        } else  {
            return getServiceUtilisateur().getAdresseMail(codeUtilisateur, null);
        }
    }

    /**
     * Récupération du libellé à afficher JSS 20040409 : gestion de plusieurs codes.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the _code
     *
     * @return the libelle
     *
     * @throws Exception
     *             the exception
     * @deprecated Utiliser {@link Utilisateur#getLibelle(String)}
     */
    @Deprecated
    public static String getLibelle(final OMContext ctx, final String code) throws Exception {
        return getLibelle(code);
    }

    /**
     * Récupération du libellé à afficher JSS 20040409 : gestion de plusieurs codes.
     *
     * @param codes le code de l'utilisateur ou une liste de code séparé par des ";" ...
     *
     * @return Une chaine vide si non trouvé ou le libellé du code utilisateur ou une liste de libéllé séparé par des ";"...
     *
     * @throws Exception lors de la récupération de l'utilisateur en bdd
     * @deprecated Utilisez {@link ServiceUser#getLibelle(String)}
     */
    @Deprecated
    public static String getLibelle(final String codes) throws Exception {
        String res = "";
        if ((codes == null) || (codes.length() == 0)) {
            return res;
        }
        final StringTokenizer st = new StringTokenizer(codes, ";");
        while (st.hasMoreTokens()) {
            final String code = st.nextToken();
            if (res.length() > 0) {
                res += ";";
            }
            final Utilisateur utilisateur = getUtilisateur(code);
            String intitule = "-";
            if (utilisateur != null) {
                intitule = utilisateur.getLibelle();
            }
            res += intitule;
        }
        return res;
    }

    /**
     * Récupération de l'objet utilisateur.
     *
     * @param ctx
     *            the _ctx
     * @param code
     *            the _code
     *
     * @return the utilisateur
     *
     * @throws Exception
     *             the exception
     * @deprecated le contexte ne sert que pour la requête SQL. Utilisez {@link Utilisateur#getUtilisateur(String)}
     */
    @Deprecated
    public static Utilisateur getUtilisateur(final OMContext ctx, final String code) throws Exception {
        return getUtilisateur(code);
    }

    /**
     * Récupération de l'objet utilisateur.
     *
     * @param code
     *            le code de l'utilisateur
     *
     * @return l'utilisateur correspondant au code ou un utilisateur vide
     *
     * @throws Exception
     *             lours de la requête en bdd
     * @deprecated Utilisez {@link ServiceUser#getByCode(String)}
     */
    @Deprecated
    public static Utilisateur getUtilisateur(final String code) throws Exception {
        final Utilisateur utilisateur = new Utilisateur();
        utilisateur.init();
        if (StringUtils.isNotBlank(code)) {
            final ClauseWhere whereCode = new ClauseWhere(ConditionHelper.egalVarchar("CODE", code));
            if (utilisateur.select(whereCode.formaterSQL()) > 0) {
                utilisateur.nextItem();
            }
        }
        return utilisateur;
    }

    /**
     * Despecialise chaine.
     *
     * @param chaine
     *            the chaine
     *
     * @return the string
     * @deprecated ne pas despécialiser les valeurs avec cette méthodes! Elle n'échape que certains caractères.
     * Pour échaper des chaines utiliser  @see EscapeString
     */
    @Deprecated
    public static String despecialiseChaine(String chaine) {
        if (chaine == null) {
            return chaine;
        }
        final char[] listeCars = {'\\', '\'', '"', '#', '(', ')', ';'};
        for (final char listeCar : listeCars) {
            int idxCar = chaine.indexOf(listeCar);
            while (idxCar != -1) {
                chaine = chaine.substring(0, idxCar) + "\\" + listeCar + chaine.substring(idxCar + 1);
                idxCar += 2;
                idxCar = chaine.indexOf(listeCar, idxCar);
            }
        }
        return chaine;
    }

    /**
     * Modification de l'adresse mail/password.
     *
     * @param code
     *            the _code
     * @param mail
     *            the _mail
     * @param password
     *            the _password
     *
     * @throws Exception
     *             the exception
     *
     * @deprecated : Utiliser la méthode {@link ServiceUser#changerMailPassword(String, String, String)}
     */
    @Deprecated
    public static void modifierMailPassword(final String code, final String mail, final String password) throws Exception {
        getServiceUtilisateur().changerMailPassword(code, mail, password);
    }

    /**
     * @deprecated utiliser {@link Espacecollaboratif#renvoyerCodesUtilisateursParEspaceParGroupe(String)}
     * @param ctx
     * @param codeEspace
     * @return
     * @throws Exception
     */
    @Deprecated
    public static Vector<String> renvoyerCodesUtilisateursParEspaceParGroupe(final OMContext ctx, final String codeEspace) throws Exception {
        return new Vector<>(Espacecollaboratif.renvoyerCodesUtilisateursParEspaceParGroupe(codeEspace));
    }

    /**
     * @deprecated utiliser {@link Espacecollaboratif#renvoyerCodesUtilisateursParEspace(String)}
     * @param ctx
     * @param codeEspace
     * @return
     * @throws Exception
     */
    @Deprecated
    public static Vector<String> renvoyerCodesUtilisateursParEspace(final OMContext ctx, final String codeEspace, final boolean tousLesMembres, final Vector<String> autresUtilisateurs) throws Exception {
        return new Vector<>(Espacecollaboratif.renvoyerCodesUtilisateursParEspace(codeEspace));
    }

    /**
     * Renvoie la liste des mails des personnes webmaster Chaque element de la liste est un tableau de string [mail][libelle][code].
     *
     * @param ctx
     *            the _ctx
     *
     * @return the array list
     *
     * @throws Exception
     *             the exception
     * @deprecated Méthode plus utilisée, à supprimer
     */
    @Deprecated
    public static ArrayList<String[]> renvoyerMailsWebmasters(final OMContext ctx) throws Exception {
        final Set<String> mailsWebmaster = Utilisateur.getListeUtilisateursPossedantPermission(new PermissionBean("TECH", "wmg", ""), new ArrayList<String>(), "", "", "", true);
        final ArrayList<String[]> res = new ArrayList<>();
        for (String aMailsWebmaster : mailsWebmaster) {
            res.add(aMailsWebmaster.split(";", -2));
        }
        return res;
    }

    /**
     * Verifie le mot de passe de l'utilisateur.
     *
     * @param motdepasse
     *            the _motdepasse
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     * @deprecated les messages sont en dur, les contrôles sont tout sauf sécurisés. A ne pas utiliser.
     * Utilisez juste les properties utilisateur.password.longueur.max et utilisateur.password.longueur.min pour contrôler la taille du mdp.
     */
    @Deprecated
    public static String verifieMotDePasse(final String motdepasse) throws Exception {
        String restriction = StringUtils.defaultString(PropertyHelper.getCoreProperty("utilisateur.password.restriction"), "0");
        //si le mot de passe est vide ou que la restriction n'est pas activée, on sort
        if (StringUtils.isEmpty(motdepasse) || "0".equals(restriction)) {
            return StringUtils.EMPTY;
        }
        /**** Longueur du mot de passe *****/
        int iMax = PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.max", -1);
        int iMin = PropertyHelper.getCorePropertyAsInt("utilisateur.password.longueur.min", -1);
        if (iMax != -1 && motdepasse.length() > iMax) {
            return "Votre mot de passe ne doit pas dépasser " + iMax + " caractères.";
        }
        if (iMin != -1 && motdepasse.length() < iMin) {
            return "Votre mot de passe doit contenir au minimum " + iMin + " caractères.";
        }
        /****** Récupération de la liste des caractères autorisés ******/
        String alphanum = StringUtils.defaultString(PropertyHelper.getCoreProperty("utilisateur.password.alphanum"));
        String accents = StringUtils.defaultString(PropertyHelper.getCoreProperty("utilisateur.password.accent"));
        String speciaux = StringUtils.defaultString(PropertyHelper.getCoreProperty("utilisateur.password.special"));
        final String liste = alphanum.toLowerCase() + alphanum.toUpperCase() + accents.toLowerCase() + accents.toUpperCase() + speciaux;
        boolean ok = true;
        /* parcours du mot de passe */
        for (int i = 0; i < motdepasse.length(); i++) {
            /* si le mot de passe contient un caractère qui n'est pas présent dans la liste, on sort*/
            if (liste.indexOf(motdepasse.charAt(i)) == -1) {
                ok = false;
                break;
            }
        }
        if (!ok) {
            return "Votre mot de passe comporte des caractères non autorisés";
        }
        return "";
    }

    /**
     * Détermine la liste des utilisateurs ayant une permission donnée sur un périmètre donné
     *
     * Par défaut, renvoie les codes
     *
     * Si renvoyerMails vaut true, ce ne sont pas les codes mais les mails qui sont renvoyés.
     *
     * @param ctx
     *            the _ctx
     * @param permission
     *            the _permission
     * @param codesStructures
     *            the _codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     * @param renvoyerMails
     *            the renvoyer mails
     *
     * @return the liste utilisateurs possedant permission
     *
     * @throws Exception
     *             the exception
     *
     * @deprecated utiliser {@link Utilisateur#getListeUtilisateursPossedantPermission(PermissionBean, List, String, String, String, boolean)}
     */
    @Deprecated
    public static TreeSet<String> getListeUtilisateursPossedantPermission(final OMContext ctx, final PermissionBean permission, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif, final boolean renvoyerMails) throws Exception {
        return getListeUtilisateursPossedantPermission(permission, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif, renvoyerMails);
    }

    /**
     * Récupère la liste des utilisateurs possédant la permission fourni en paramètre.
     * @param permission la permission dont on souhaite avoir la liste des utilisateurs.
     * @return la liste des utilisateurs possédant la permission.
     * @throws Exception les classiques, en gros BDD généralement...
     * @deprecated Utilisez {@link ServiceUser#getUtilisateursPossedantPermission(PermissionBean)}
     */
    @Deprecated
    public static List<UtilisateurBean> getUtilisateursPossedantPermission(final PermissionBean permission) throws Exception {
        return getServiceUtilisateur().getUtilisateursPossedantPermission(permission);
    }

    /**
     * Détermine la liste des utilisateurs ayant une permission donnée sur un périmètre donné
     *
     * Par défaut, renvoie les codes
     *
     * Si renvoyerMails vaut true, ce ne sont pas les codes mais les mails qui sont renvoyés.
     *
     * @param permission
     *            the _permission
     * @param codesStructures
     *            the _codes structures
     * @param codeRubrique
     *            the _code rubrique
     * @param publicsVises
     *            the _publics vises
     * @param codeEspaceCollaboratif
     *            the _code espace collaboratif
     * @param renvoyerMails
     *            the renvoyer mails
     *
     * @return the liste utilisateurs possedant permission
     *
     * @throws Exception
     *             the exception
     * @deprecated : Utilisez {@link ServiceUser#getListeUtilisateursPossedantPermission(PermissionBean, List, String, String, String, boolean)}
     */
    @Deprecated
    public static TreeSet<String> getListeUtilisateursPossedantPermission(final PermissionBean permission, final List<String> codesStructures, final String codeRubrique, final String publicsVises, final String codeEspaceCollaboratif, final boolean renvoyerMails) throws Exception {
        if (codeEspaceCollaboratif.length() > 0) {
            return Espacecollaboratif.getListeUtilisateursPossedantPermissionPourUnEspace(permission, codeEspaceCollaboratif, renvoyerMails);
        }
        return (TreeSet<String>) getServiceUtilisateur().getListeUtilisateursPossedantPermission(permission, codesStructures, codeRubrique, publicsVises, codeEspaceCollaboratif, renvoyerMails);
    }

    /**
     * Inits the.
     */
    public void init() {
        persistenceBean.setCode(StringUtils.EMPTY);
        persistenceBean.setMotDePasse(StringUtils.EMPTY);
        persistenceBean.setCivilite(StringUtils.EMPTY);
        persistenceBean.setNom(StringUtils.EMPTY);
        persistenceBean.setPrenom(StringUtils.EMPTY);
        persistenceBean.setCodeRattachement(StringUtils.EMPTY);
        persistenceBean.setGroupes(StringUtils.EMPTY);
        persistenceBean.setAdresseMail(StringUtils.EMPTY);
        persistenceBean.setExtensionModification(StringUtils.EMPTY);
        persistenceBean.setRestrictionValidation(StringUtils.EMPTY);
        persistenceBean.setCentresInteret(StringUtils.EMPTY);
        persistenceBean.setProfilDsi(StringUtils.EMPTY);
        persistenceBean.setGroupesDsiImport(StringUtils.EMPTY);
        persistenceBean.setCodeLdap(StringUtils.EMPTY);
        persistenceBean.setRoles(StringUtils.EMPTY);
        persistenceBean.setProfilDefaut(StringUtils.EMPTY);
        persistenceBean.setSourceImport(StringUtils.EMPTY);
        persistenceBean.setFormatEnvoi(StringUtils.EMPTY);
        persistenceBean.setModeSaisieExpert("1");
        persistenceBean.setTsCacheGroupes((long) 0);
        setVecteurGroupesDsiTmp(new Vector<String>());
    }

    /**
     * Récupération du libellé à afficher.
     *
     * @return the libelle
     *
     */
    public String getLibelle() {
        return String.format("%s %s", persistenceBean.getNom(), persistenceBean.getPrenom());
    }

    /**
     * Renvoie la liste des centres d'intéret sous forme de vecteur.
     *
     * @return the vecteur centres interet
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceUser#getCentresInteret(UtilisateurBean)}
     */
    @Deprecated
    public Vector<String> getVecteurCentresInteret() throws Exception {
        final Vector<String> centresInteret = new Vector<>();
        centresInteret.addAll(getServiceUtilisateur().getCentresInteret(persistenceBean));
        return centresInteret;
    }

    /**
     * Valorise la liste des centres d'interet sous forme de vecteur.
     *
     * @param v the new vecteur centres interet
     */
    public void setVecteurCentresInteret(final Vector<String> v) {
        persistenceBean.setCentresInteret(StringUtils.join(v, ";"));
    }

    /**
     * Controle si l'utilisateur courant appartient au groupe ou a ses sous-groupes.
     *
     * @param codeGroupe
     *            the _code groupe
     * @param groupesUtilisateurs
     *            the groupes utilisateurs
     *
     * @return true, if appartient a groupe ou sous groupes
     *
     */
    public boolean appartientAGroupeOuSousGroupes(final String codeGroupe, final Vector<String> groupesUtilisateurs) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        final GroupeDsiBean group = serviceGroupeDsi.getByCode(codeGroupe);
        for(String currentCode : groupesUtilisateurs) {
            if(serviceGroupeDsi.contains(group, serviceGroupeDsi.getByCode(currentCode))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Sélection d'un utilisateur
     *
     * Peut donner lieu à des jointures notamment lors des requêtes de groupe qui donnent
     *
     * SELECT DISTINCT T1.CODE FROM UTILISATEUR T1
     *
     * LEFT JOIN GROUPEUTILISATEUR T2 ON T1.CODE = T2.CODE_UTILISATEUR WHERE T2.CODE_GROUPE IN ('TEST_A') AND( T1.CODE IN('TEST_BB_1','TEST_BB_2'))
     *
     * @param code
     *            the _code
     * @param nom
     *            the _nom
     * @param prenom
     *            the _prenom
     * @param chaineDiffusion
     *            the _chaine diffusion
     * @param profil
     *            the _profil
     * @param groupe
     *            the _groupe
     * @param structure
     *            the _structure
     * @param adresseMail
     *            the _adresseMail
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceUser#select(String, String, String, String, String, String, String, String)}
     */
    @Deprecated
    public int select(final String code, final String nom, final String prenom, final String chaineDiffusion, final String profil, final String groupe, final String structure, final String adresseMail) throws Exception {
        return getServiceUtilisateur().select(code, nom, prenom, chaineDiffusion, profil, groupe, structure, adresseMail).size();
    }

    /**
     * Sélection d'un utilisateur par son code.
     *
     * @param code
     *            the code
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     * @deprecated Utilisez {@link ServiceUser#getByCode(String)}
     */
    @Deprecated
    public int selectParCode(final String code) throws Exception {
        return select(code, "", "", "", "", "", "", "");
    }

    /**
     * select nocount pour les listes avec pagination.
     *
     * @param code
     *            the _code
     * @param nom
     *            the _nom
     * @param prenom
     *            the _prenom
     * @param chaineDiffusion
     *            the _chaine diffusion
     * @param profil
     *            the _profil
     * @param groupe
     *            the _groupe
     * @param from
     *            the from
     * @param increment
     *            the increment
     * @param structure
     *            the _structure
     * @param adresseMail
     *            the _adresse mail
     *
     * @throws Exception
     *             the exception
     * @deprecated Cette méthode ne doit plus être utilisée. Elle ne sert tout simplement plus à rien.
     */
    @Deprecated
    public void selectNoCount(final String code, final String nom, final String prenom, final String chaineDiffusion, final String profil, final String groupe, final String structure, final String adresseMail, final int from, final int increment) throws Exception {
        //        String requete = construireRequete(code, nom, prenom, chaineDiffusion, profil, groupe, structure, adresseMail, true);
        //        if (increment != 0) {
        //            requete += " LIMIT " + from + "," + increment;
        //        }
        // TODO : FLE -> a voir si on les garde
        // commonDao.selectNoCount(requete);
    }

    /**
     * Gets the vecteur groupes dsi tmp.
     *
     * @return the vecteur groupes dsi tmp
     */
    public Vector<String> getVecteurGroupesDsiTmp() {
        return vecteurGroupesDsiTmp;
    }

    /**
     * Sets the vecteur groupes dsi tmp.
     *
     * @param vecteurGroupesDsiTmp
     *            the new vecteur groupes dsi tmp
     */
    public void setVecteurGroupesDsiTmp(final Vector<String> vecteurGroupesDsiTmp) {
        this.vecteurGroupesDsiTmp = vecteurGroupesDsiTmp;
    }

    /**
     * Retourne vrai si l'utilisateur provient d'un annuaire ldap. Basé sur l'assertion : si le code_ldap est non vide l'utilisateur provient du ldap.
     *
     * @return Vrai si l'utilsateur provient du ldap
     *
     * @deprecated Utilisez {@link ServiceUser#fromLdap(UtilisateurBean)}
     */
    @Deprecated
    public boolean provientDuLdap() {
        return StringUtils.isNotBlank(persistenceBean.getCodeLdap());
    }

    /**
     * retourne vrai si la source d'authenfication interdit la modification du mot de passe et que l'utilisateur ne provient pas du ldap
     *
     * @return vrai si la source d'authenfication interdit la modification du mot de passe et que l'utilisateur ne provient pas du ldap
     *
     * @deprecated Utilisez {@link ServiceUser#isAlterationForbiddenBySource(UtilisateurBean)}
     */
    @Deprecated
    public boolean isModificationMotDePasseInterditeParSource() {
        return getServiceUtilisateur().isAlterationForbiddenBySource(persistenceBean);
    }

    public Long getId() {
        return persistenceBean.getId();
    }

    public void setId(Long id) {
        persistenceBean.setId(id);
    }

    public String getCode() {
        return persistenceBean.getCode();
    }

    public void setCode(final String code) {
        persistenceBean.setCode(code);
    }

    public String getMotDePasse() {
        return persistenceBean.getMotDePasse();
    }

    public void setMotDePasse(final String motDePasse) {
        persistenceBean.setMotDePasse(motDePasse);
    }

    public Date getDateNaissance() {
        return persistenceBean.getDateNaissance();
    }

    public void setDateNaissance(final Date dateNaissance) {
        persistenceBean.setDateNaissance(dateNaissance);
    }

    public String getCivilite() {
        return persistenceBean.getCivilite();
    }

    public void setCivilite(final String civilite) {
        persistenceBean.setCivilite(civilite);
    }

    public String getNom() {
        return persistenceBean.getNom();
    }

    public void setNom(final String nom) {
        persistenceBean.setNom(nom);
    }

    public String getPrenom() {
        return persistenceBean.getPrenom();
    }

    public void setPrenom(final String prenom) {
        persistenceBean.setPrenom(prenom);
    }

    public String getCodeRattachement() {
        return persistenceBean.getCodeRattachement();
    }

    public void setCodeRattachement(final String codeRattachement) {
        persistenceBean.setCodeRattachement(codeRattachement);
    }

    public String getGroupes() {
        return persistenceBean.getGroupes();
    }

    public void setGroupes(final String groupes) {
        persistenceBean.setGroupes(groupes);
    }

    public String getAdresseMail() {
        return persistenceBean.getAdresseMail();
    }

    public void setAdresseMail(final String adresseMail) {
        persistenceBean.setAdresseMail(adresseMail);
    }

    public String getRestrictionValidation() {
        return persistenceBean.getRestrictionValidation();
    }

    public void setRestrictionValidation(final String restrictionValidation) {
        persistenceBean.setRestrictionValidation(restrictionValidation);
    }

    public String getExtensionModification() {
        return persistenceBean.getExtensionModification();
    }

    public void setExtensionModification(final String extensionModification) {
        persistenceBean.setExtensionModification(extensionModification);
    }

    public String getCentresInteret() {
        return persistenceBean.getCentresInteret();
    }

    public void setCentresInteret(final String centresInteret) {
        persistenceBean.setCentresInteret(centresInteret);
    }

    public String getProfilDsi() {
        return persistenceBean.getProfilDsi();
    }

    public void setProfilDsi(final String profilDsi) {
        persistenceBean.setProfilDsi(profilDsi);
    }

    public String getGroupesDsi() {
        return persistenceBean.getGroupesDsi();
    }

    public void setGroupesDsi(final String groupesDsi) {
        persistenceBean.setGroupesDsi(groupesDsi);
    }

    public String getCodeLdap() {
        return persistenceBean.getCodeLdap();
    }

    public void setCodeLdap(final String codeLdap) {
        persistenceBean.setCodeLdap(codeLdap);
    }

    public String getGroupesDsiImport() {
        return persistenceBean.getGroupesDsiImport();
    }

    public void setGroupesDsiImport(final String groupesDsiImport) {
        persistenceBean.setGroupesDsiImport(groupesDsiImport);
    }

    public String getRoles() {
        return persistenceBean.getRoles();
    }

    public void setRoles(final String roles) {
        persistenceBean.setRoles(roles);
    }

    public Date getDateDerniereSession() {
        return persistenceBean.getDateDerniereSession();
    }

    public void setDateDerniereSession(final Date dateDerniereSession) {
        persistenceBean.setDateDerniereSession(dateDerniereSession);
    }

    public String getProfilDefaut() {
        return persistenceBean.getProfilDefaut();
    }

    public void setProfilDefaut(final String profilDefaut) {
        persistenceBean.setProfilDefaut(profilDefaut);
    }

    public String getSourceImport() {
        return persistenceBean.getSourceImport();
    }

    public void setSourceImport(final String sourceImport) {
        persistenceBean.setSourceImport(sourceImport);
    }

    public String getFormatEnvoi() {
        return persistenceBean.getFormatEnvoi();
    }

    public void setFormatEnvoi(final String formatEnvoi) {
        persistenceBean.setFormatEnvoi(formatEnvoi);
    }

    public String getModeSaisieExpert() {
        return persistenceBean.getModeSaisieExpert();
    }

    public void setModeSaisieExpert(final String modeSaisieExpert) {
        persistenceBean.setModeSaisieExpert(modeSaisieExpert);
    }

    public Long getTsCacheGroupes() {
        return persistenceBean.getTsCacheGroupes();
    }

    public void setTsCacheGroupes(final Long tsCacheGroupes) {
        persistenceBean.setTsCacheGroupes(tsCacheGroupes);
    }
}
