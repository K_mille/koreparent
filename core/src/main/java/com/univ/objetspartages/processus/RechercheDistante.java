package com.univ.objetspartages.processus;

import java.util.StringTokenizer;

import com.jsbsoft.jtf.core.InfoBean;
import com.univ.utils.RechercheFiche;
// TODO: Auto-generated Javadoc

/**
 * The Class RechercheDistante.
 */
public class RechercheDistante extends RechercheFiche {

    /**
     * Commentaire relatif au constructeur RechercheGRS.
     *
     * @param ciu
     *            the ciu
     */
    public RechercheDistante(final InfoBean ciu) {
        super(ciu);
    }

    /* (non-Javadoc)
     * @see com.univ.utils.RechercheFiche#traiterRECHERCHE()
     */
    @Override
    protected void traiterRECHERCHE() throws Exception {
        if (action.equals(InfoBean.ACTION_VALIDER)) {
            String requete = "";
            final String listeParams = infoBean.getString("LISTE_PARAMS");
            final StringTokenizer st = new StringTokenizer(listeParams, ",");
            while (st.hasMoreTokens()) {
                final String nomParam = st.nextToken().trim();
                if (infoBean.getString(nomParam).length() > 0) {
                    if (requete.length() > 0) {
                        requete += "&";
                    }
                    requete += nomParam + "=" + infoBean.getString(nomParam);
                }
            }
            infoBean.set("REQUETE_DISTANTE", requete);
            preparerLISTE();
        }
    }
}
