package com.univ.objetspartages.processus;

import java.util.Collection;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.database.ProcessusBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.MessageHelper;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.DiffusionSelective;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.objetspartages.util.FichesAValiderUtil;

/**
 * processus de gestion de la validation des fiches.
 */
public class Validation extends ProcessusBean {

    /** The Constant ECRAN_SELECTION_NIVEAU_APPROBATION. */
    private static final String ECRAN_SELECTION_NIVEAU_APPROBATION = "APPROBATION";

    /** The Constant ECRAN_SELECTION_VALIDATEUR. */
    private static final String ECRAN_SELECTION_VALIDATEUR = "VALIDATEUR";

    /** The Constant ECRAN_CONFIRMATION. */
    private static final String ECRAN_CONFIRMATION = "CONFIRMATION";

    /** The Constant ECRAN_LISTE. */
    private static final String ECRAN_LISTE = "LISTE";

    private static final Logger LOG = LoggerFactory.getLogger(Validation.class);

    private final ServiceUser serviceUser;

    /** The autorisations. */
    AutorisationBean autorisations;

    /** The fiche univ. */
    FicheUniv ficheUniv;

    /**
     * processus saisie Structure.
     *
     * @param ciu
     *            com.jsbsoft.jtf.core.InfoBean
     */
    public Validation(final InfoBean ciu) {
        super(ciu);
        serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
    }

    /**
     * Point d'entrée du processus.
     *
     * @return true, if traiter action
     *
     * @throws Exception
     *             the exception
     */
    @Override
    public boolean traiterAction() throws Exception {
        final Object o = getGp().getSessionUtilisateur().getInfos().get(SessionUtilisateur.AUTORISATIONS);
        if (o == null) {
            infoBean.setEcranRedirection(WebAppUtil.CONNEXION_BO);
            infoBean.setEcranLogique("LOGIN");
        } else {
            try {
                autorisations = (AutorisationBean) o;
                ecranLogique = infoBean.getEcranLogique();
                action = infoBean.getActionUtilisateur();
                etat = EN_COURS;
                if (infoBean.get("CODE_OBJET") != null) {
                    ficheUniv = ReferentielObjets.instancierFiche(ReferentielObjets.getNomObjet(infoBean.getString("CODE_OBJET")));
                    ficheUniv.setCtx(this);
                    ficheUniv.init();
                    // on renseigne le périmètre saisi par l'utilisateur
                    if (infoBean.get("CODE_RATTACHEMENT") != null) {
                        ficheUniv.setCodeRattachement(infoBean.getString("CODE_RATTACHEMENT"));
                    }
                    // JSS 20051031 : structures secondaires
                    if (ficheUniv instanceof FicheRattachementsSecondaires && (infoBean.get("CODE_RATTACHEMENT_AUTRES") != null)) {
                        ((FicheRattachementsSecondaires) ficheUniv).setCodeRattachementAutres(infoBean.getString("CODE_RATTACHEMENT_AUTRES"));
                    }
                    if (infoBean.get("CODE_RUBRIQUE") != null) {
                        ficheUniv.setCodeRubrique(infoBean.getString("CODE_RUBRIQUE"));
                    }
                    if (ficheUniv instanceof DiffusionSelective && infoBean.get("PUBLIC_VISE_DSI") != null) {
                        ((DiffusionSelective) ficheUniv).setDiffusionPublicVise(infoBean.getString("PUBLIC_VISE_DSI"));
                    }
                }
                if (ecranLogique == null) {
                    if ("LISTE".equals(action)) {
                        preparerListeAValider(autorisations);
                    } else if ("APPROBATION".equals(action)) {
                        preparerListeApprobation(infoBean.getString("NIVEAU_APPROBATION"));
                    }
                } else {
                    if (action.equals(InfoBean.ACTION_ANNULER)) {
                        etat = FIN;
                    } else if (ecranLogique.equals(ECRAN_SELECTION_NIVEAU_APPROBATION)) {
                        preparerListeValidateur(infoBean.getString("NIVEAU_APPROBATION"));
                    } else if (ecranLogique.equals(ECRAN_SELECTION_VALIDATEUR)) {
                        traiterListeValidateur();
                    }
                }
                //placer l'état dans le composant d'infoBean
                infoBean.setEcranLogique(ecranLogique);
            } catch (final Exception e) {
                LOG.error("erreur de traitement sur le processus", e);
                infoBean.addMessageErreur(e.toString());
            }
        }
        infoBean.set("NOM_ONGLET", "valider");
        // On continue si on n'est pas à la FIN !!!
        return etat == FIN;
    }

    /**
     * Affichage de la liste des objets à valider.
     *
     * @param metatag
     *            the fiche univ
     * @param nbItems
     *            the nb items
     *
     * @return the int
     *
     */
    protected int insererElement(final MetatagBean metatag, int nbItems) {
        infoBean.set("NOM_PROCESSUS#" + nbItems, ReferentielObjets.getProcessus(metatag.getMetaCodeObjet()));
        infoBean.set("EXTENSION#" + nbItems, ReferentielObjets.getExtension(metatag.getMetaCodeObjet()));
        infoBean.set("ID_FICHE#" + nbItems, metatag.getMetaIdFiche().toString());
        infoBean.set("LIBELLE#" + nbItems, metatag.getMetaLibelleFiche());
        infoBean.set("NOM_OBJET#" + nbItems, ReferentielObjets.getLibelleObjet(metatag.getMetaCodeObjet()));
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(metatag.getMetaCodeRubrique());
        String label = MessageHelper.getCoreMessage("RUBRIQUE_INEXISTANTE");
        if (rubriqueBean != null) {
            label = rubriqueBean.getIntitule();
        }
        infoBean.set("CODE_RUBRIQUE#" + nbItems, label);
        infoBean.set("DATE_PROPOSITION#" + nbItems, metatag.getMetaDateProposition());
        infoBean.set("LIBELLE_REDACTEUR#" + nbItems, serviceUser.getLibelle(metatag.getMetaCodeRedacteur()));
        infoBean.set("LANGUE#" + nbItems, metatag.getMetaLangue());
        infoBean.set("ETAT_OBJET#" + nbItems, metatag.getMetaEtatObjet());
        nbItems++;
        return nbItems;
    }

    /**
     * Preparer liste approbation.
     *
     * @param _code
     *            the _code
     *
     * @throws Exception
     *             the exception
     */
    private void preparerListeApprobation(final String _code) throws Exception {
        final Hashtable<String, String> hPermissionsValidation = AutorisationBean.getListeNiveauxApprobation();
        final Iterator<String> it = AutorisationBean.getListePermissionsValidation().iterator();
        boolean possedePermissionApprobation = true;
        boolean possedeTouteLesPermissions = true;
        boolean debut = false;
        String codeApprobation = "";
        int i = 0;
        // on boucle sur les permissions d'approbation
        if (it.hasNext()) {
            while (it.hasNext()) {
                // si permission précedente ok on continue
                if (possedePermissionApprobation) {
                    codeApprobation = it.next();
                    // initialisation du code d'approbation demandé
                    if (_code.length() == 0) {
                        debut = true;
                    } else if (_code.equals(codeApprobation)) {
                        if (it.hasNext()) {
                            codeApprobation = it.next();
                            debut = true;
                        } else {
                            codeApprobation = "V";
                        }
                    }
                    if (debut) {
                        if (!autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_APPROBATION, codeApprobation)) {
                            possedePermissionApprobation = false;
                            possedeTouteLesPermissions = false;
                            infoBean.set("CODE_NIVEAU_AUTOMATIQUE", codeApprobation);
                            infoBean.set("LIBELLE_NIVEAU_AUTOMATIQUE", "A valider : " + hPermissionsValidation.get(codeApprobation));
                        } else {
                            infoBean.set("CODE_NIVEAU_APPROBATION#" + i, codeApprobation);
                            infoBean.set("LIBELLE_NIVEAU_APPROBATION#" + i, hPermissionsValidation.get(codeApprobation));
                            i++;
                        }
                    }
                } else {
                    break;
                }
            }
            // validation classique
        } else {
            codeApprobation = "V";
        }
        // si il les a toutes on regarde la permission de validation classique
        if (possedeTouteLesPermissions) {
            if (autorisations.getAutorisationParFiche(ficheUniv, AutorisationBean.INDICE_APPROBATION, "")) {
                infoBean.set("CODE_NIVEAU_APPROBATION#" + i, "V");
                infoBean.set("LIBELLE_NIVEAU_APPROBATION#" + i, "Mise en ligne");
                infoBean.set("CODE_NIVEAU_AUTOMATIQUE", "M");
                infoBean.set("LIBELLE_NIVEAU_AUTOMATIQUE", "En ligne");
                i++;
            } else {
                infoBean.set("CODE_NIVEAU_AUTOMATIQUE", "V");
                infoBean.set("LIBELLE_NIVEAU_AUTOMATIQUE", "A valider : Mise en ligne");
            }
        }
        // Si il y a plus d'une approbation possible renvoi sur l'écran de sélection d'une approbation
        if (i > 0) {
            infoBean.setInt("NB_ITEMS_NIVEAU_APPROBATION", i);
            ecranLogique = ECRAN_SELECTION_NIVEAU_APPROBATION;
        }
        // sinon renvoi directement sur la selection des validateurs
        else {
            preparerListeValidateur(codeApprobation);
        }
    }

    /**
     * Preparer liste validateur.
     *
     * @param codeApprobation
     *            the code approbation
     *
     * @throws Exception
     *             the exception
     */
    private void preparerListeValidateur(final String codeApprobation) throws Exception {
        if (!"M".equals(codeApprobation)) {
            String libelleApprobation = "";
            final Hashtable<String, String> hPermissionsValidation = AutorisationBean.getListeNiveauxApprobation();
            if ("V".equals(codeApprobation)) {
                libelleApprobation = "Mise en ligne";
            } else {
                libelleApprobation = hPermissionsValidation.get(codeApprobation);
            }
            String codeUtilisateur = "";
            final String codeRubrique = ficheUniv.getCodeRubrique();
            final String publicsVises = "";
            String espace = "";
            if (ficheUniv instanceof DiffusionSelective) {
                // JSS 20051201 : suppression périmètres groupes sur les fiches
                //publicsVises = ((DiffusionSelective) metatag).getDiffusionPublicVise();
                if ("4".equals(((DiffusionSelective) ficheUniv).getDiffusionModeRestriction())) {
                    espace = ((DiffusionSelective) ficheUniv).getDiffusionPublicViseRestriction();
                }
            }
            final Collection<String> mails = serviceUser.getListeUtilisateursPossedantPermission(new PermissionBean("FICHE", ReferentielObjets.getCodeObjet(ficheUniv), codeApprobation), AutorisationBean.getStructuresPerimetreFiche(ficheUniv), codeRubrique, publicsVises, espace, false);
            final Iterator<String> iter = mails.iterator();
            int i = 0;
            while (iter.hasNext()) {
                codeUtilisateur = iter.next();
                // on filtre le redacteur
                if (!codeUtilisateur.equals(autorisations.getCode())) {
                    infoBean.set("CODE_VALIDATEUR_" + i, codeUtilisateur);
                    infoBean.set("NOM_VALIDATEUR_" + i, serviceUser.getLibelle(codeUtilisateur));
                    i++;
                }
            }
            if (!infoBean.get("CODE_NIVEAU_AUTOMATIQUE").equals(codeApprobation)) {
                infoBean.set("NIVEAU_APPROBATION", codeApprobation);
            } else {
                infoBean.set("NIVEAU_APPROBATION", "");
            }
            infoBean.set("LIBELLE_APPROBATION", libelleApprobation);
            infoBean.setInt("NB_ITEMS_VALIDATEUR", i);
            ecranLogique = ECRAN_SELECTION_VALIDATEUR;
        } else {
            infoBean.set("NIVEAU_APPROBATION", "M");
            infoBean.set("LIBELLE_APPROBATION", "");
            infoBean.set("CHAINE_VALIDATEURS", "");
            infoBean.set("LISTE_VALIDATEURS", "");
            ecranLogique = ECRAN_CONFIRMATION;
        }
    }

    /**
     * Traiter liste validateur.
     *
     * @throws Exception
     *             the exception
     */
    private void traiterListeValidateur() throws Exception {
        String chaineValidateurs = "";
        String listeValidateurs = "";
        for (int i = 0; i < (Integer) infoBean.get("NB_ITEMS_VALIDATEUR"); i++) {
            if (infoBean.get("VALIDATEUR_" + i) != null) {
                if (chaineValidateurs.length() > 0) {
                    listeValidateurs += ";";
                    chaineValidateurs += ", ";
                }
                listeValidateurs += infoBean.getString("CODE_VALIDATEUR_" + i);
                chaineValidateurs += serviceUser.getLibelle(infoBean.getString("CODE_VALIDATEUR_" + i));
            }
        }
        if (listeValidateurs.length() == 0) {
            listeValidateurs = "[AUCUNS]";
            chaineValidateurs = "[aucuns]";
        }
        infoBean.set("CHAINE_VALIDATEURS", chaineValidateurs);
        infoBean.set("LISTE_VALIDATEURS", listeValidateurs);
        ecranLogique = ECRAN_CONFIRMATION;
    }

    /**
     * Affichage de la liste des objets à valider AM 02.2003 Modif sur les périmètres de validation
     *
     * @param autorisations
     *            the autorisations
     *
     */
    private void preparerListeAValider(final AutorisationBean autorisations) {
        int nbItems = 0;
        /************************************************************
         * Traitement générique : droit de validations sur les objets
         *
         * Il peut exister une structure de restriction
         *************************************************************/
        final List<MetatagBean> metaToValidate = FichesAValiderUtil.getObjetsAValider(autorisations);
        for (MetatagBean meta : metaToValidate) {
            nbItems = insererElement(meta, nbItems);
        }
        infoBean.set("LISTE_NB_ITEMS", nbItems);
        ecranLogique = ECRAN_LISTE;
    }

}
