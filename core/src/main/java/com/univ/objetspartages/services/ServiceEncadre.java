package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.AbstractServiceBean;
import com.univ.objetspartages.bean.EncadreBean;
import com.univ.objetspartages.dao.impl.EncadreDAO;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;


/**
 * Created on 21/04/15.
 */
public class ServiceEncadre extends AbstractServiceBean<EncadreBean, EncadreDAO> {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceEncadre.class);

    /**
     * Gets a the list of objects for the given {@link com.univ.objetspartages.bean.EncadreBean}.
     * @param encadre : the {@link com.univ.objetspartages.bean.EncadreBean} to retrieve the objects for.
     * @return a {@link java.util.List} of {@link String}
     */
    public List<String> getListObjets(final EncadreBean encadre) {
        if(encadre != null && StringUtils.isNotBlank(encadre.getObjets())) {
            return Arrays.asList(StringUtils.split(encadre.getObjets(), ";"));
        }
        return new ArrayList<>();
    }

    /**
     * Sets the list of objects of the {@link com.univ.objetspartages.bean.EncadreBean}.
     * @param encadre : the {@link com.univ.objetspartages.bean.EncadreBean} to update.
     * @param objets : a {@link java.util.List} of {@link String} of the objects to set.
     */
    public void setListObjet(final EncadreBean encadre, final List<String> objets) {
        encadre.setObjets(StringUtils.defaultString(StringUtils.join(objets, ";")));
    }

    /**
     * Delete all the {@link com.univ.objetspartages.bean.EncadreBean} in the datasource matching the section codes condition.
     * @param codes : a {@link java.util.List} of {@link String}.
     */
    public void deleteByCodesRubriques(final List<String> codes) {
        dao.deleteByCodesRubriques(codes);
    }

    /**
     * Retrieves a {@link com.univ.objetspartages.bean.EncadreBean} by its code.
     * @param code : the encadre code as a {@link String}.
     * @return the {@link com.univ.objetspartages.bean.EncadreBean} if found, null otherwise.
     */
    public EncadreBean getByCode(final String code) {
        return dao.getByCode(code);
    }

    /**
     * Retrieves all the {@link com.univ.objetspartages.bean.EncadreBean} in the datasource matching the section codes condition.
     * @param codes : a {@link java.util.List} of {@link String}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.EncadreBean}
     */
    public List<EncadreBean> getByCodesRubrique(final List<String> codes) {
        return dao.getByCodesRubrique(codes);
    }

    /**
     * Retrieves all the persisted {@link com.univ.objetspartages.bean.EncadreBean}.
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.EncadreBean}
     */
    public List<EncadreBean> getAllEncadres() {
        return dao.getAllEncadres();
    }

    /**
     * Retrieves the persisted {@link com.univ.objetspartages.bean.EncadreBean} for the given parameter
     * @param codeRubrique the section code
     * @param ficheUniv the {@link FicheUniv} is need to get the "code strucutre" and the content type code. If this parameter is null, an empty list is returned
     * @param langue the language of the {@link EncadreBean}
     * @return a {@link java.util.List} of {@link com.univ.objetspartages.bean.EncadreBean} or an empty list if there is no result
     */
    public List<EncadreBean> getEncadreObjectList(final String codeRubrique, final FicheUniv ficheUniv, final String langue) {
        if (ficheUniv != null) {
            final String codeObjet = ReferentielObjets.getCodeObjetParClasse(ficheUniv.getClass().getName());
            final String codeStructure = ficheUniv.getCodeRattachement();
            if (StringUtils.isEmpty(codeObjet)) {
                return new ArrayList<>();
            }
            return dao.getEncadreObjectList(codeRubrique, codeStructure, codeObjet, langue);
        }
        return new ArrayList<>();
    }

    /**
     * Retrieves all the contents of the matching {@link com.univ.objetspartages.bean.EncadreBean} as an {@link java.util.ArrayList} of {@link String}.
     * @param codeRubrique : the section code to match as a {@link String}.
     * @param ficheUniv : a {@link com.univ.objetspartages.om.FicheUniv} for which to retrieve the content.
     * @param langue : the language to look for as a {@link String}.
     * @return an {@link java.util.ArrayList} of {@link String}
     */
    public ArrayList<String> getEncadresList(final String codeRubrique, final FicheUniv ficheUniv, final String langue) {
        final ArrayList<String> contents = new ArrayList<>();
        for (final EncadreBean encadre : getEncadreObjectList(codeRubrique, ficheUniv, langue)) {
            contents.add(encadre.getContenu());
        }
        return contents;
    }

    /**
     * Retrieves the defined search encadre defined.
     * @return a {@link java.util.Map} of {@link String},{@link String}
     */
    public Map<String, String> getEncadreSearchList(){
        final Map<String, String> listeEncadresRecherche = new HashMap<>();
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            try {
                if (ReferentielObjets.gereEncadreRechercheEmbarquable(codeObjet)) {
                    listeEncadresRecherche.put(codeObjet, ReferentielObjets.getLibelleObjet(codeObjet));
                }
            } catch (final ReflectiveOperationException e) {
                LOG.error(String.format("Une erreur est survenue lors de l'ajout de l'encadré de recherche pour le code objet %s", codeObjet), e);
            }
        }
        return listeEncadresRecherche;
    }

    public void addWithForcedId(EncadreBean encadreBean) throws Exception {
        dao.addWithForcedId(encadreBean);
    }
}
