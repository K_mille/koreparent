package com.univ.objetspartages.sgbd;

/**
 * Interface décrivant l'objet DB d'une fiche K-Portal.
 */
public interface FicheUnivDB {

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    void add() throws Exception;

    /**
     * Adds the.
     *
     * @throws Exception
     *             the exception
     */
    void addWithForcedId() throws Exception;

    /**
     * Delete.
     *
     * @throws Exception
     *             the exception
     */
    void delete() throws Exception;

    /**
     * Update.
     *
     * @throws Exception
     *             the exception
     */
    void update() throws Exception;

    /**
     * Select.
     *
     * @param requete
     *            the requete
     *
     * @return the int
     *
     * @throws Exception
     *             the exception
     */
    int select(String requete) throws Exception;

    /**
     * Retrieve.
     *
     * @throws Exception
     *             the exception
     */
    void retrieve() throws Exception;

    /**
     * Next item.
     *
     * @return true, if successful
     *
     * @throws Exception
     *             the exception
     */
    boolean nextItem() throws Exception;
}
