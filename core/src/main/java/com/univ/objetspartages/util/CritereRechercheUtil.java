package com.univ.objetspartages.util;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.CodeLibelle;
import com.jsbsoft.jtf.core.Formateur;
import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.textsearch.RechercheFmt;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.DateUtil;

/**
 * Classe utilitaire permetant de construirez des {@link CritereRecherche} généralement utilisé
 *
 * @author olivier.camon
 *
 */
public class CritereRechercheUtil {

    /**
     * Construit un critère de recherche pour un champ de type texte si il est renseigné.
     *
     * @param infoBean la map contenant ttes les données de la requete
     * @param nomChamp
     * @return
     */
    public static CritereRecherche getCritereTexteNonVide(final InfoBean infoBean, final String nomChamp) {
        CritereRecherche critere = null;
        final String valeur = infoBean.getString(nomChamp);
        if (StringUtils.isNotBlank(valeur)) {
            critere = new CritereRecherche(nomChamp, valeur, valeur);
        }
        return critere;
    }

    /**
     * Retourne un critère de recherche pour un champ de type texte si il est renseigné. La valeur est formatée par la méthode {@link RechercheFmt#formaterTexteRecherche(String)}
     *
     * @param infoBean la map contenant ttes les données de la requete
     * @param nomChamp
     * @return
     */
    public static CritereRecherche getCritereTexteNonVideFormater(final InfoBean infoBean, final String nomChamp) {
        CritereRecherche critere = null;
        final String valeur = infoBean.getString(nomChamp);
        if (StringUtils.isNotBlank(valeur)) {
            critere = new CritereRecherche(nomChamp, RechercheFmt.formaterTexteRecherche(valeur), valeur);
        }
        return critere;
    }

    /**
     * Retourne un critère de recherche pour un champ de type texte si il est renseigné et si il ne vaut pas "0000". La valeur est formatée par la méthode
     * {@link RechercheFmt#formaterTexteRecherche(String)}
     *
     * @param infoBean la map contenant ttes les données de la requete
     * @param nomChamp
     * @return
     */
    public static CritereRecherche getCritereTexteNonVideSansValeurDefaut(final InfoBean infoBean, final String nomChamp) {
        CritereRecherche critere = null;
        final String valeur = infoBean.getString(nomChamp);
        if (StringUtils.isNotBlank(valeur) && !"0000".equals(valeur)) {
            critere = new CritereRecherche(nomChamp, RechercheFmt.formaterTexteRecherche(valeur), valeur);
        }
        return critere;
    }

    /**
     * Retourne un critère de recherche pour un champ de type liste si il est renseigné et si il ne vaut pas "0000" (valeur par défaut). La valeur à affiché à l'utilisateur est
     * récupéré à partir d'un objet {@link com.univ.objetspartages.bean.LabelBean}
     *
     * @param infoBean la map contenant ttes les données de la requete

     * @param nomChamp
     *            le nom du champ à récupérer
     * @param typeLibelle
     *            le type de {@link com.univ.objetspartages.bean.LabelBean}
     * @return
     */
    public static CritereRecherche getCritereChaineAvecLibelle(final InfoBean infoBean, final String nomChamp, final String typeLibelle) {
        CritereRecherche critere = null;
        final String valeur = infoBean.getString(nomChamp);
        if (StringUtils.isNotBlank(valeur) && !"0000".equals(valeur)) {
            final ContexteUniv ctx = ContexteUtil.getContexteUniv();
            final String libelleValeur = LabelUtils.getLibelle(typeLibelle, valeur, ctx.getLocale());
            critere = new CritereRecherche(nomChamp, valeur, libelleValeur);
        }
        return critere;
    }

    /**
     * Retourne un critère de recherche pour un champ de type liste si il est renseigné et si il ne vaut pas "0000" (valeur par défaut). La valeur à affiché à l'utilisateur est
     * récupéré à partir des vieux fichiers .dat de kportal
     *
     * @param infoBean la map contenant ttes les données de la requete
     * @param nomChamp
     * @param nomFichierDat
     * @param idExtension
     * @return
     */
    public static CritereRecherche getCritereChaineAvecFichierDat(final InfoBean infoBean, final String nomChamp, final String nomFichierDat, final String idExtension) {
        CritereRecherche critere = null;
        final String valeur = infoBean.getString(nomChamp);
        if (StringUtils.isNotBlank(valeur) && !"0000".equals(valeur)) {
            final String valeurFichierDat = CodeLibelle.lireLibelle(idExtension, nomFichierDat, valeur);
            critere = new CritereRecherche(nomChamp, valeur, valeurFichierDat);
        }
        return critere;
    }

    /**
     * Retourne un critère de recherche pour un champ de type date si il est renseigné. La valeur et la valeur à afficher est formater par {@link Formateur#formater(Object)}
     *
     * @param infoBean la map contenant ttes les données de la requete
     * @param nomChamp
     * @return
     */
    public static CritereRecherche getCritereDate(final InfoBean infoBean, final String nomChamp) {
        CritereRecherche critere = null;
        final Object infoBeanValue = infoBean.get(nomChamp);
        Date valeur = null;
        if (infoBeanValue != null && infoBeanValue instanceof Date) {
            valeur = (Date) infoBeanValue;
        } else if (infoBeanValue != null && infoBeanValue instanceof String) {
            valeur = DateUtil.parseDate((String) infoBeanValue);
        }
        if (Formateur.estSaisie(valeur)) {
            final String valeurDateFormat = Formateur.formater(valeur);
            critere = new CritereRecherche(nomChamp, valeurDateFormat, valeurDateFormat);
        }
        return critere;
    }

    /**
     * Retourne le critère pour le champ thématique qui est utilisé 15 fois dans l'appli...
     *
     * @param infoBean la map contenant ttes les données de la requete
     * @return le critère correspondant à la valeur du champ thématique saisie. Il contient le code et le libelle
     */
    public static CritereRecherche getCritereThematique(final InfoBean infoBean) {
        return getCritereChaineAvecLibelle(infoBean, "THEMATIQUE", "04");
    }
}
