package com.univ.objetspartages.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceMetatag;

/**
 * Classe utilitaire pour les fiches en état à valider
 */
public class FichesAValiderUtil {

    /**
     * Methode calculant les fiches à valider possible de l'utilisateur courant.
     * @param autorisations les autorisations de l'utilisateur courant.
     * @return La liste des méta à valider qui ne sont pas du collab et qui sont autorisées pour l'utilisateur courant.
     */
    public static List<MetatagBean> getObjetsAValider(final AutorisationBean autorisations) {
        final Collection<String> codeObjets = new ArrayList<>();
        final List<MetatagBean> result = new ArrayList<>();
        for (final String codeObjet : ReferentielObjets.getListeCodesObjet()) {
            if (autorisations.getAutorisation(codeObjet, AutorisationBean.INDICE_VALIDATION)) {
                codeObjets.add(codeObjet);
            }
        }
        final ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
        final List<MetatagBean> metaToValidate = serviceMetatag.getToValidateMetas(codeObjets);
        for (MetatagBean meta : metaToValidate) {
            boolean isFicheEspace = "4".equals(meta.getMetaDiffusionModeRestriction());
            if (!isFicheEspace) {
                final String codeApprobation = StringUtils.defaultString(meta.getMetaNiveauApprobation());
                if (autorisations.getAutorisationParMeta(meta, AutorisationBean.INDICE_APPROBATION, codeApprobation)) {
                    result.add(meta);
                }
            }
        }
        return result;
    }

}
