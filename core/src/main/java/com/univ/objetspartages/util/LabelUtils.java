package com.univ.objetspartages.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.utils.ContexteUtil;

/**
 * Created on 23/04/15.
 */
public class LabelUtils {

    private static ServiceLabel getServiceLabel() {
        return ServiceManager.getServiceForBean(LabelBean.class);
    }

    /**
     * Récupération des libellés à partir d'un type, de code (séparés par des ; si plusieurs) et de la locale
     * @param type : type du libellé
     * @param code : code (séparés par ; si plusieurs)
     * @param language : locale
     * @return
     */
    public static String getLibelle(final String type, final String code, final Locale language) {
        return getLibelle(type, code, String.valueOf(LangueUtil.getIndiceLocale(language)));
    }

    /**
     * Récupération des libellés à partir d'un type, de code (séparés par des ; si plusieurs) et de la langue
     * La chaîne de libellés est ensuite formatée (séparation des libellés par , ou ;)
     * @param type : type du libellé
     * @param code : code (séparés par ; si plusieurs)
     * @param language : langue
     * @return : libellés séparés par des , ou des ; (si présence d'un contexte)
     */
    public static String getLibelle(final String type, final String code, final String language) {
        List<String> codes = new ArrayList<>();
        if(StringUtils.isNotBlank(code) && !"0000".equals(code)) {
            codes = Arrays.asList(code.split(";"));
        }
        final List<String> libelles = getServiceLabel().getLabelsLibelles(type, codes, language);
        if(ContexteUtil.getContexteUniv() != null) {
            return StringUtils.join(libelles, ", ");
        } else {
            return StringUtils.join(libelles, ";");
        }
    }

    /**
     * Récupération d'une liste de libellés à partir d'un type, de codes (séparés par des ; si plusieurs) et de la locale
     * @param type : type du libellé
     * @param codes : code (séparés par ; si plusieurs)
     * @param locale : locale
     * @return : liste de libellés
     */
    public static List<String> getLibelleList(final String type, final String codes, final Locale locale) {
        List<String> codeList = new ArrayList<>();
        if(StringUtils.isNotBlank(codes) && !"0000".equals(codes)) {
            codeList = Arrays.asList(codes.split(";"));
        }
        return getServiceLabel().getLabelsLibelles(type, codeList, Integer.toString(LangueUtil.getIndiceLocale(locale)));
    }

    /**
     *{@link ServiceLabel#getLabelForCombo(String, Locale)}
     */
    public static Map<String, String> getLabelCombo(final String code, final Locale locale) {
        return getServiceLabel().getLabelForCombo(code, locale);
    }

    /**
     * Récupération d'un label bean à partir d'un type, d'un code et de la langue
     * @param type : type du libellé
     * @param code : code
     * @param language : langue
     * @return : {@link LabelBean}
     */
    public static LabelBean getLabel(final String type, final String code, final String language) {
        return getServiceLabel().getByTypeCodeLanguage(type, code, language);
    }
}
