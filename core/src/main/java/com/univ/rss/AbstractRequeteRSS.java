package com.univ.rss;

/**
 * Superclasse des classes chargées de récupérer les flux rss
 *
 * @author aga
 */
public abstract class AbstractRequeteRSS implements Runnable {

    /**
     * Référence vers le gestionnaire de flux rss
     */
    private RequeteMultiRSS requeteMulti;

    /**
     * nombre de réponse max à renvoyer
     */
    private int maxReponses;

    /**
     * Url du flux rss a récupérer
     */
    private String url;

    /**
     * Un constructeur
     *
     * @param requeteMulti
     *            Référence vers le gestionnaire de flux rss
     * @param maxReponses
     *            nombre de réponse max à renvoyer
     * @param url
     *            Url du flux rss a récupérer
     */
    public AbstractRequeteRSS(RequeteMultiRSS requeteMulti, int maxReponses, String url) {
        this.requeteMulti = requeteMulti;
        this.maxReponses = maxReponses;
        this.url = url;
    }

    /**
     * @return the requeteMulti
     */
    protected RequeteMultiRSS getRequeteMulti() {
        return requeteMulti;
    }

    /**
     * @param requeteMulti
     *            the requeteMulti to set
     */
    protected void setRequeteMulti(RequeteMultiRSS requeteMulti) {
        this.requeteMulti = requeteMulti;
    }

    /**
     * @return the maxReponses
     */
    protected int getMaxReponses() {
        return maxReponses;
    }

    /**
     * @param maxReponses
     *            the maxReponses to set
     */
    protected void setMaxReponses(int maxReponses) {
        this.maxReponses = maxReponses;
    }

    /**
     * @return the url
     */
    protected String getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    protected void setUrl(String url) {
        this.url = url;
    }
}
