package com.univ.rss;

import java.net.URL;

/**
 *
 * Représente une entrée de type media:content dans un flux RSS
 *
 * @author aga
 *
 */
public class RSSMediaContentBean {

    /**
     * La hauteur du média
     */
    private Integer height;

    /**
     * La largeur du média
     */
    private Integer width;

    /**
     * L'url poIntegerant sur le média
     */
    private URL url;

    /**
     * Un titre du média
     */
    private String titre;

    /**
     * Une description du contenu du média
     */
    private String description;

    /**
     * Le type mime du média parmi {@link RSSMediaContentBean#supportedMimeTypes}
     */
    private String mimeType;

    /**
     * Retourne vrai si le type mime en paramètre est supporté par cette classe, faux sinon
     *
     * @param sourceMimeType
     *            Le type mime à vérifier
     * @return vrai si le type mime en paramètre est supporté par cette classe, faux sinon
     */
    public static boolean isMimeTypeSupported(String sourceMimeType) {
        return true;
    }

    /**
     * @return the height
     */
    public Integer getHeight() {
        return height;
    }

    /**
     * @param height
     *            the height to set
     */
    public void setHeight(Integer height) {
        this.height = height;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     *            the description to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return le type mime du média parmi {@link RSSMediaContentBean#supportedMimeTypes}
     */
    public String getMimeType() {
        return mimeType;
    }

    /**
     * @param mimeType
     *            le type mime du média.Doit faire parti de {@link RSSMediaContentBean#supportedMimeTypes}
     */
    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    /**
     * @return the width
     */
    public Integer getWidth() {
        return width;
    }

    /**
     * @param width
     *            the width to set
     */
    public void setWidth(Integer width) {
        this.width = width;
    }

    /**
     * @return the url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * @param url
     *            the url to set
     */
    public void setUrl(URL url) {
        this.url = url;
    }

    /**
     * Retourne vrai si ce media:content représente une image (dont le type est supporté)
     *
     * @return vrai si ce media:content représente une image (dont le type est supporté), faux sinon
     */

    public boolean isImage() {
        boolean isImage = Boolean.FALSE;
        if (mimeType != null) {
            isImage = mimeType.toLowerCase().startsWith("image");
        }
        return isImage;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

}
