package com.univ.rss;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.config.PropertyHelper;
import com.univ.rss.impl.rome.RequeteRSSImplRome;
// TODO: Auto-generated Javadoc

/**
 * Lance les requêtes en simultané sur plusieurs sites. NOTE : multithread et multisite inutile pour cette fonctionnalité
 */
public class RequeteMultiRSS {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteMultiRSS.class);

    /** The resultats. */
    private List<RSSBean> resultats = null;

    private String titre = "";

    private String description = "";

    /**
     * Constructeur.
     */
    public RequeteMultiRSS() {
        this.resultats = new ArrayList<>();
    }

    /**
     * Méthode de lancement du requêteur multiple sur un flux rss.
     *
     * @param param
     *            Les paramètres de la requête
     *
     * @return the vector
     */
    public Vector<RSSBean> lancerRequetes(final String param) {
        // Analyse des paramètres
        final Vector<String> vUrls = new Vector<>();
        int maxResultats = 0;
        final int indicePlus = param.indexOf("+");
        // si pas de +
        if (indicePlus == -1) {
            vUrls.add(StringUtils.replace(param, "&amp;", "&"));
            maxResultats = 100;
        } else {
            //sinon, on prend le premier + et on coupe la chaine (permet d'avoir des urls rss qui contiennent un +
            try {
                //format NB+URL
                maxResultats = Integer.parseInt(param.substring(0, indicePlus));
                vUrls.add(StringUtils.replace(param.substring(indicePlus + 1), "&amp;", "&"));
            } catch (final NumberFormatException e) {
                LOG.debug("unable to parse the given value", e);
                //si le + s'avère être dans l'url et pas un séparateur, dans ce cas on met tout param dans l'url
                vUrls.add(StringUtils.replace(param, "&amp;", "&"));
                maxResultats = 100;
            } catch (final Exception e) {
                //autre type d'erreur, on ignore l'url
                LOG.error("mauvaise URL peut être?", e);
            }
        }
        final List<Thread> lstTread = new ArrayList<>();
        // Lancement des requêtes
        try {
            // Création des requêtes
            AbstractRequeteRSS requete = null;
            for (String vUrl : vUrls) {
                requete = new RequeteRSSImplRome(this, maxResultats, vUrl);
                final Thread threadRequete = new Thread(requete);
                threadRequete.start();
                lstTread.add(threadRequete);
            }
            // Pour chaque thread crée on spécifie un timeout 2s par defaut
            long to = PropertyHelper.getCorePropertyAsLong("import_rss.timeout", 2000);
            for (final Thread thread : lstTread) {
                thread.join(to);
            }
        } catch (final InterruptedException e) {
            LOG.error("erreur lors de l'appel du thread", e);
        }
        // Parcours des résultats pour stocker dans le vecteur
        final Vector<RSSBean> res = new Vector<>();
        for (RSSBean resultat : resultats) {
            res.add(resultat);
        }
        return res;
    }

    /**
     * Ajouter resultat.
     *
     * @param rss
     *            the rss
     */
    public synchronized void ajouterResultat(final RSSBean rss) {
        resultats.add(rss);
    }

    /**
     * Indique au gestionnaire qu'un thread a fini de s'exécuter.
     */
    public synchronized void decrementerCptRequetes() {}

    public String getTitre() {
        return titre;
    }

    public void setTitre(final String titre) {
        this.titre = titre;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }
}
