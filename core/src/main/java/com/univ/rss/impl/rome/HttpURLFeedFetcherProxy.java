package com.univ.rss.impl.rome;

import java.net.URLConnection;

import com.rometools.fetcher.impl.FeedFetcherCache;
import com.rometools.fetcher.impl.HttpURLFeedFetcher;
import com.rometools.fetcher.impl.SyndFeedInfo;

/**
 *
 * @author emmanuel.clisson
 *
 */
public class HttpURLFeedFetcherProxy extends HttpURLFeedFetcher {

    public HttpURLFeedFetcherProxy(FeedFetcherCache feedFetcherCache) {
        super(feedFetcherCache);
    }

}
