/*
 * Created on 14 sept. 2005
 *
 * Permet de stocker le résultat d'un thread
 */
package com.univ.services;
// TODO: Auto-generated Javadoc

/**
 * The Class ReponseService.
 *
 * @author jeanseb
 */
public class ReponseService {

    /** The terminated. */
    boolean terminated = false;

    /** The reponse. */
    String reponse = "";

    /**
     * Instantiates a new reponse service.
     */
    public ReponseService() {
        super();
    }

    /**
     * Sets the terminated.
     *
     * @param terminated
     *            the new terminated
     */
    public void setTerminated(boolean terminated) {
        this.terminated = terminated;
    }

    /**
     * Checks if is terminated.
     *
     * @return true, if is terminated
     */
    public boolean isTerminated() {
        return terminated;
    }

    /**
     * Gets the reponse.
     *
     * @return the reponse
     */
    public String getReponse() {
        return reponse;
    }

    /**
     * Sets the reponse.
     *
     * @param reponse
     *            the new reponse
     */
    public void setReponse(String reponse) {
        this.reponse = reponse;
    }
}
