/*
 * Created on 14 sept. 2005
 *
 */
package com.univ.services;

import com.univ.objetspartages.om.ServiceBean;

/**
 * The Class RequeteService.
 *
 * @author jeanseb
 *
 *         Permet de stocker la requête d'un thread
 */
public class RequeteService {

    /** The service. */
    ServiceBean service = null;

    /** The vue. */
    String vue = "";

    /** The id page. */
    String idPage = "";

    /**
     * The Constructor.
     *
     * @param service
     *            the service
     * @param vue
     *            the vue
     */
    public RequeteService(ServiceBean service, String vue) {
        this.service = service;
        this.vue = vue;
    }

    /**
     * Gets the service.
     *
     * @return the service
     */
    public ServiceBean getService() {
        return service;
    }

    /**
     * Gets the vue.
     *
     * @return the vue
     */
    public String getVue() {
        return vue;
    }

    /**
     * Gets the id page.
     *
     * @return the id page
     */
    public String getIdPage() {
        return idPage;
    }
}
