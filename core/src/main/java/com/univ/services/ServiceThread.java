/*
 * Created on 14 sept. 2005
 *
 *
 * Unité d'exécution permettant de lancer un service
 */
package com.univ.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.utils.ContexteUniv;
// TODO: Auto-generated Javadoc

/**
 * Chaque instance est créee à partir d'un ServiceInvoker qui sera notifié à la fin de l'exécution.
 *
 * @author jeanseb
 */
public class ServiceThread implements Runnable {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceThread.class);

    ContexteUniv ctx;

    RequeteService requete;

    /** The invoker. */
    ServicesInvoker invoker;

    /** The cle requete. */
    String cleRequete = "";

    /**
     * The Constructor.
     *
     * @param _invoker
     *            the _invoker
     * @param _ctx
     *            the _ctx
     * @param _cleRequete
     *            the _cle requete
     * @param _requete
     *            the _requete
     */
    public ServiceThread(final ServicesInvoker _invoker, final ContexteUniv _ctx, final String _cleRequete, final RequeteService _requete) {
        super();
        this.ctx = _ctx;
        this.requete = _requete;
        this.invoker = _invoker;
        this.cleRequete = _cleRequete;
    }

    /**
     * Détermine pour un service le contenu à afficher
     *
     * Pour l'instant, seule l'url est gérée.
     *
     * @return @throws Exception
     */
    @Override
    public void run() {
        LOG.debug("thread service " + cleRequete + "  -> début");
        String contenu = "";
        try {
            contenu = CacheServiceViewManager.getContenuService(ctx, requete);
        } catch (final Exception e) {
            LOG.error("erreur lors de la récupération du contenu du service", e);
            contenu = "Service indisponible";
        }
        invoker.finRequete(cleRequete, contenu);
        LOG.debug("thread service " + cleRequete + "  -> fin");
    }
}