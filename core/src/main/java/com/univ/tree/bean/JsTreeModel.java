package com.univ.tree.bean;

import java.util.ArrayList;
import java.util.List;

public class JsTreeModel {

    private List<JsTreeNodeModel> nodes;

    public JsTreeModel() {
        nodes = new ArrayList<>();
    }

    public List<JsTreeNodeModel> getNodes() {
        return nodes;
    }

    public void setNodes(List<JsTreeNodeModel> nodes) {
        this.nodes = nodes;
    }
}
