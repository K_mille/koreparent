package com.univ.tree.processus;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.tree.bean.JsTreeDataModel;
import com.univ.tree.bean.JsTreeModel;
import com.univ.tree.bean.JsTreeNodeModel;
import com.univ.tree.bean.JsTreePath;
import com.univ.tree.utils.JsTreeUtils;
import com.univ.utils.EscapeString;
import com.univ.utils.SessionUtil;

public class StructuresJsTree extends GestionJsTree<List<StructureModele>> {

    private ServiceStructure serviceStructure;

    public void setServiceStructure(final ServiceStructure serviceStructure) {
        this.serviceStructure = serviceStructure;
    }

    /** l'id Spring du bean. */
    public static final String ID_BEAN = "structuresJsTree";

    private static final Logger LOG = LoggerFactory.getLogger(StructuresJsTree.class);

    private static JsTreeNodeModel buildStructureNode(final String permissions, final AutorisationBean autorisations, final TypeStructureFilter filter, final StructureModele structure, final boolean front, int niveau, final Set<String> ids) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        JsTreeNodeModel node = null;
        // Ajoute les éléments à l'arbre
        final List<StructureModele> subStructures = structure == null ? serviceStructure.getSubStructures(StringUtils.EMPTY, LangueUtil.getIndiceLocaleDefaut(), true) : serviceStructure.getSubStructures(structure.getCode(), LangueUtil.getIndiceLocaleDefaut(), true);
        final boolean selectable = isStructureSelectable(permissions, autorisations, filter, structure, ids);
        if (isStructureVisible(permissions, autorisations, filter, structure, front, selectable, ids)) {
            node = new JsTreeNodeModel();
            if (!selectable) {
                node.getAttr().put("rel", "not_selectable");
            }
            // on ajoute l'élément
            final JsTreeDataModel datas = new JsTreeDataModel();
            if (structure == null) {
                datas.setTitle(" ");
                node.getAttr().put("rel", "root");
                node.getAttr().put("class", "structure_root");
                if(subStructures.size() > 0) {
                    node.setState("open");
                }
            } else {
                datas.setTitle(structure.getLibelleLong());
                node.getAttr().put("class", EscapeString.escapeAttributHtml("structure_" + ReferentielObjets.getCodeObjet(structure) + "_" + structure.getTypeStructure()));
                node.getAttr().put("title", structure.getLibelleLong());
                node.getAttr().put("id", structure.getTypeStructure() + "_" + Long.toString(structure.getIdFiche()));
                node.getMetadata().put("libelle", structure.getLibelleLong());
                node.getMetadata().put("sCode", structure.getCode());
                if (subStructures.size() > 0 && hasVisibleChildren(permissions, autorisations, structure, filter, front, ids)) {
                    node.setState("closed");
                }
            }
            node.getMetadata().put("numchildren", Integer.toString(subStructures.size()));
            node.setData(datas);
            if (niveau > 0 || niveau == -1) {
                for (final StructureModele currentStructure : subStructures) {
                    if (niveau != -1) {
                        niveau--;
                    }
                    final JsTreeNodeModel child = buildStructureNode(permissions, autorisations, filter, currentStructure, front, niveau, ids);
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                    if (niveau != -1) {
                        niveau++;
                    }
                }
            } else if (niveau == -2) {
                for (final StructureModele currentStructure : subStructures) {
                    final JsTreeNodeModel child;
                    if (!ids.contains(currentStructure.getCode())) {
                        child = buildStructureNode(permissions, autorisations, filter, currentStructure, front, 0, null);
                    } else {
                        child = buildStructureNode(permissions, autorisations, filter, currentStructure, front, niveau, ids);
                    }
                    if (child != null) {
                        node.getChildren().add(child);
                    }
                }
            }
        }
        return node;
    }

    /**
     * Teste si la structure est sélectionnable.
     *
     * @param structure
     *            La structure à tester
     *
     * @return true si la structure est sélectionnable
     *
     */
    private static boolean isStructureSelectable(final String permissions, final AutorisationBean autorisations, final TypeStructureFilter filter, final StructureModele structure, final Set<String> ids) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        boolean selectableItem = true;
        final boolean inIds = structure != null && (ids == null || ids.isEmpty() || ids.contains(structure.getCode()));
        // Controle le périmètre de l'utilisateur sur la structure
        if (permissions.length() > 0 && structure != null) {
            selectableItem = serviceStructure.checkPermission(autorisations, permissions, structure.getCode());
        }
        // Controle le filtre sur les types de structures
        if (filter != null && selectableItem && structure != null) {
            selectableItem = filter.accept(structure.getTypeStructure());
        }
        // si la fiche en cours de saisie est une fiche annuaire (ou étudiant ou ancien étudiant),
        // que l'utilisateur est redacteur de sa fiche,
        // et qu'il n'a pas de role concernant les fiches annuaire,
        // il peut choisir la structure
        final PermissionBean permissionCourante = new PermissionBean(permissions);
        if (!selectableItem && autorisations != null && autorisations.isRedacteurFicheCourante() && ("0006".equals(permissionCourante.getObjet()) || "0007".equals(permissionCourante.getObjet()) || "0013".equals(permissionCourante.getObjet())) && !autorisations.possedePermission(permissionCourante)) {
            selectableItem = true;
        }
        return selectableItem && inIds;
    }

    private static boolean hasVisibleChildren(final String permissions, final AutorisationBean autorisations, final StructureModele structure, final TypeStructureFilter filter, final boolean front, final Set<String> ids) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<StructureModele> subStructures = structure == null ? serviceStructure.getSubStructures(StringUtils.EMPTY, LangueUtil.getIndiceLocaleDefaut(), true) : serviceStructure.getSubStructures(structure.getCode(), structure.getLangue(), true);
        for (final StructureModele currentChild : subStructures) {
            final boolean selectable = isStructureSelectable(permissions, autorisations, filter, currentChild, ids);
            final boolean visible = isStructureVisible(permissions, autorisations, filter, structure, front, selectable, ids);
            if (visible) {
                return true;
            }
        }
        return false;
    }

    /**
     * Teste si la structure est visible dans l'arbre (structure sélectionnable ou ayant un fils visible.
     *
     * @param structure
     *            La structure à tester
     * @param selectable
     *            true si la structure est sélectionnable
     *
     * @return true si la structure est visible dans l'arbre
     *
     */
    private static boolean isStructureVisible(final String permissions, final AutorisationBean autorisations, final TypeStructureFilter filter, final StructureModele structure, final boolean front, final boolean selectable, final Set<String> ids) {
        if(structure == null) {
            return true;
        }
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        boolean visible = selectable;
        final boolean inIds = ids == null || ids.isEmpty() || ids.contains(structure.getCode());
        if (!selectable) // structure non sélectionnable
        {
            // on vérifie ses structures filles
            final Iterator<StructureModele> listSousStructuresIt = serviceStructure.getSubStructures(structure.getCode(), LangueUtil.getIndiceLocaleDefaut(), true).iterator();
            while (listSousStructuresIt.hasNext() && !visible) {
                final StructureModele sousStructure = listSousStructuresIt.next();
                // Si on est en front, vérifie que la structure est bien visible
                if (!front || serviceStructure.isVisibleInFront(sousStructure)) {
                    visible = isStructureVisible(permissions, autorisations, filter, sousStructure, front, isStructureSelectable(permissions, autorisations, filter, sousStructure, ids), ids);
                }
            }
        }
        return visible && inIds;
    }

    public static String getPath(String root, final String code, final String separator) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final List<String> path = new ArrayList<>();
        if (StringUtils.isBlank(root)) {
            root = JsTreeUtils.CODE_ROOT;
        }
        if (StringUtils.isBlank(code)) {
            return root;
        }
        StructureModele structure = serviceStructure.getByCodeLanguage(code, LangueUtil.getIndiceLocaleDefaut());
        while (structure != null && StringUtils.isNotBlank(structure.getCode())) {
            path.add(structure.getLibelleAffichable());
            structure = serviceStructure.getByCodeLanguage(structure.getCodeRattachement(), structure.getLangue());
        }
        Collections.reverse(path);
        return StringUtils.join(path, separator);
    }

    @Override
    public JsTreeModel traiterDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final HashMap<String, String> parameters = JsTreeUtils.getParameters(req);
        JsTreeModel jsTree = new JsTreeModel();
        assertParametersConsistency(parameters);
        final boolean front = parameters.get("FRONT") != null && ("true".equals(parameters.get("FRONT")));
        final String permissions = parameters.get("PERMISSION");
        TypeStructureFilter filter = null;
        if (!StringUtils.isEmpty(parameters.get("FILTRE"))) {
            filter = new TypeStructureFilter(parameters.get("FILTRE"));
        }
        final StructureModele structure = serviceStructure.getByCodeLanguage(parameters.get("RACINE"), LangueUtil.getIndiceLocaleDefaut());
        if(structure != null || StringUtils.isBlank(parameters.get("RACINE"))) {
            final int niveau = Integer.parseInt(parameters.get("NIVEAU"));
            final String[] selection = StringUtils.defaultString(parameters.get("SELECTED")).split(";");
            jsTree.getNodes().add(buildStructureNode(permissions, autorisations, filter, structure, front, niveau, null));
            if (selection.length > 0) {
                loadToPath(autorisations, permissions, filter, front, jsTree, selection);
            }
        }
        return jsTree;
    }

    private void loadToPath(final AutorisationBean autorisations, final String permissions, final TypeStructureFilter filter, final boolean front, final JsTreeModel jsTree, final String[] selection) {
        final List<String> loadedNodes = new ArrayList<>();
        for (final String code : selection) {
            final JsTreePath path = getPathToCode(code);
            final Iterator<String> codeIt = path.getChildPath().iterator();
            final Set<String> nodes = new HashSet<>();
            String topCode = codeIt.next();
            nodes.addAll(path.getChildPath());
            if (!loadedNodes.isEmpty()) {
                while (loadedNodes.contains(topCode) && codeIt.hasNext()) {
                    topCode = codeIt.next();
                }
            }
            if (StringUtils.isNotBlank(topCode)) {
                final JsTreeNodeModel parentNode = JsTreeUtils.getNodeWithCode(jsTree.getNodes().get(0), topCode);
                final StructureModele parent = serviceStructure.getByCodeLanguage(topCode, LangueUtil.getIndiceLocaleDefaut());
                final JsTreeNodeModel model = buildStructureNode(permissions, autorisations, filter, parent, front, -2, nodes);
                if (parentNode != null && model != null) {
                    parentNode.getChildren().addAll(model.getChildren());
                }
                loadedNodes.addAll(nodes);
            }
        }
    }

    private JsTreePath getPathToCode(final String code) {
        final JsTreePath path = new JsTreePath();
        path.addChild(code);
        final StructureModele child = serviceStructure.getByCodeLanguage(code, LangueUtil.getIndiceLocaleDefaut());
        if(child != null) {
            StructureModele parent = serviceStructure.getByCodeLanguage(child.getCodeRattachement(), LangueUtil.getIndiceLocaleDefaut());
            while (parent != null) {
                path.addChild(parent.getCode());
                parent = serviceStructure.getByCodeLanguage(parent.getCodeRattachement(), LangueUtil.getIndiceLocaleDefaut());
            }
        }
        return path;
    }

    @Override
    public void assertParametersConsistency(final Map<String, String> parameters) {
        if (parameters.get("CODE") == null || parameters.get("CODE").length() == 0 || "00".equals(parameters.get("CODE"))) {
            parameters.put("CODE", StringUtils.EMPTY);
        }
        if (parameters.get("RACINE") == null || parameters.get("RACINE").length() == 0 || "00".equals(parameters.get("RACINE"))) {
            parameters.put("RACINE", StringUtils.EMPTY);
        }
    }

    @Override
    public JsTreeModel traiterRechercheDepuisRequete(final HttpServletRequest req) {
        return new JsTreeModel();
    }

    @Override
    public JsTreeModel traiterFiltreDepuisRequete(final HttpServletRequest req) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        final HashMap<String, String> parameters = JsTreeUtils.getParameters(req);
        final String query = StringUtils.defaultString(req.getParameter("QUERY"), StringUtils.EMPTY);
        assertParametersConsistency(parameters);
        parameters.put("NIVEAU", "-1");
        final Map<String, String> structures = serviceStructure.getStructureListWithFullLabels();
        final List<StructureModele> listStructures = new ArrayList<>();
        for (final Entry<String, String> structure : structures.entrySet()) {
            if (StringUtils.containsIgnoreCase(structure.getValue(), query)) {
                final StructureModele currentStructure = serviceStructure.getByCodeLanguage(structure.getKey(), LangueUtil.getIndiceLocaleDefaut());
                if(currentStructure != null) {
                    listStructures.add(currentStructure);
                }
            }
        }
        final JsTreeModel jsTree = filterTree(autorisations, listStructures, parameters);
        openAllNodes(jsTree.getNodes());
        return jsTree;
    }

    @Override
    protected JsTreeModel filterTree(final AutorisationBean autorisations, final List<StructureModele> structures, final HashMap<String, String> parameters) {
        final JsTreeModel jsTree = new JsTreeModel();
        final String permissions = parameters.get("PERMISSION");
        final StructureModele structure = serviceStructure.getByCodeLanguage(parameters.get("RACINE"), LangueUtil.getIndiceLocaleDefaut());
        final int niveau = Integer.parseInt(parameters.get("NIVEAU"));
        final Set<String> ids = new HashSet<>();
        final boolean front = parameters.get("FRONT") != null && ("true".equals(parameters.get("FRONT")));
        TypeStructureFilter filter = null;
        if (!StringUtils.isEmpty(parameters.get("FILTRE"))) {
            filter = new TypeStructureFilter(parameters.get("FILTRE"));
        }
        for (final StructureModele currentStructure : structures) {
            ids.add(currentStructure.getCode());
            StructureModele structureMere = serviceStructure.getByCodeLanguage(currentStructure.getCodeRattachement(), LangueUtil.getIndiceLocaleDefaut());
            while (StringUtils.isNotBlank(structureMere.getCode())) {
                ids.add(structureMere.getCode());
                structureMere = serviceStructure.getByCodeLanguage(structureMere.getCodeRattachement(), LangueUtil.getIndiceLocaleDefaut());
            }
        }
        if (ids.isEmpty()) {
            return jsTree;
        }
        ids.add("00");
        jsTree.getNodes().add(buildStructureNode(permissions, autorisations, filter, structure, front, niveau, ids));
        return jsTree;
    }

    @Override
    public String getSelectedIds(final String string) {
        if (StringUtils.isEmpty(string)) {
            return StringUtils.EMPTY;
        }
        final List<String> selectedIds = new ArrayList<>();
        final String[] selections = string.split(";");
        for (final String currentSelection : selections) {
            final StructureModele structure = serviceStructure.getByCodeLanguage(currentSelection, LangueUtil.getIndiceLocaleDefaut());
            if (structure != null && StringUtils.isNotBlank(structure.getCode())) {
                selectedIds.add(structure.getTypeStructure() + "_" + Long.toString(structure.getIdFiche()));
            }
        }
        return StringUtils.join(selectedIds, ";");
    }
}

/**
 * Classe implémentant un filtre sur les types de structure.
 */
class TypeStructureFilter {

    /** Liste des types de structures autorisés. */
    private String authorizedTypeStructureList = null;

    /**
     * Constructeur.
     *
     * @typeStructureList Liste des types de structures autorisés (séparation des types de structure par des ';')
     */
    TypeStructureFilter(final String typeStructureList) {
        this.authorizedTypeStructureList = typeStructureList;
    }

    /**
     * Teste si le type de structure est accepté.
     *
     * @param typeStructure
     *            Le type de structure à tester
     * @return true si le type est accepté
     */
    boolean accept(final String typeStructure) {
        return typeStructure == null || typeStructure.length() == 0 || authorizedTypeStructureList.contains(typeStructure);
    }
}
