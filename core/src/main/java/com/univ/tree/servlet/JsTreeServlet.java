package com.univ.tree.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jsbsoft.jtf.exception.ErreurAsyncException;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.tree.processus.GestionJsTree;
import com.univ.tree.utils.JsTreeManager;
import com.univ.tree.utils.JsTreeUtils;
import com.univ.utils.SessionUtil;

public class JsTreeServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = -7982996758134619952L;

    private static final Logger LOG = LoggerFactory.getLogger(JsTreeServlet.class);

    @Override
    protected void doGet(final HttpServletRequest req, final HttpServletResponse resp) {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        if (SessionUtil.getInfosSession(req).get(SessionUtilisateur.CODE) == null || autorisations == null) {
            try {
                req.getRequestDispatcher("/adminsite/").forward(req, resp);
            } catch (final ServletException | IOException e) {
                LOG.error("unable to forward the user to the login page", e);
            }
        } else {
            final GestionJsTree<?> gestionJsTree = JsTreeManager.getGestionJsTree(req.getParameter(JsTreeUtils.PARAM_BEAN_JSTREE));
            final ObjectMapper mapper = new ObjectMapper();
            mapper.setSerializationInclusion(JsonInclude.Include.NON_DEFAULT);
            final String action = req.getParameter("ACTION");

            try {
                if (!StringUtils.isBlank(action)) {
                    if ("RECHERCHER".equals(action)) {
                        final String resultatFormatter = mapper.writeValueAsString(gestionJsTree.traiterRechercheDepuisRequete(req).getNodes());
                        resp.setContentType("application/json");
                        resp.getWriter().write(resultatFormatter);
                    } else if ("FILTRER".equals(action)) {
                        final String resultatFormatter = mapper.writeValueAsString(gestionJsTree.traiterFiltreDepuisRequete(req).getNodes());
                        resp.setContentType("application/json");
                        resp.getWriter().write(resultatFormatter);
                    } else {
                        final String resultatFormatter = mapper.writeValueAsString(gestionJsTree.traiterDepuisRequete(req).getNodes());
                        resp.setContentType("application/json");
                        resp.getWriter().write(resultatFormatter);
                    }
                } else {
                    final String resultatFormatter = mapper.writeValueAsString(gestionJsTree.traiterDepuisRequete(req).getNodes());
                    resp.setContentType("application/json");
                    resp.getWriter().write(resultatFormatter);
                }
            } catch (final ErreurAsyncException eA) {
                LOG.error("erreur de calcul de l'arbre", eA);
                try {
                    resp.getWriter().write(eA.getMessage());
                } catch (final IOException e) {
                    LOG.error("impossible d'écrire dans la réponse", e);
                }
                resp.setStatus(500);
            } catch (final IOException e) {
                LOG.error("Une erreur est survenue lors de la construction de l'arborescence", e);
            }
        }
    }

    @Override
    protected void doPost(final HttpServletRequest req, final HttpServletResponse resp)  {
        final AutorisationBean autorisations = (AutorisationBean) SessionUtil.getInfosSession(req).get(SessionUtilisateur.AUTORISATIONS);
        try {
            if (SessionUtil.getInfosSession(req).get(SessionUtilisateur.CODE) == null || autorisations == null) {
                req.getRequestDispatcher("/adminsite/").forward(req, resp);
            } else {
                final GestionJsTree<?> gestionJsTree = JsTreeManager.getGestionJsTree(req.getParameter(JsTreeUtils.PARAM_BEAN_JSTREE));
                resp.setCharacterEncoding(CharEncoding.DEFAULT);
                try {
                    final String result = gestionJsTree.traiterAction(autorisations, req.getParameterMap());
                    resp.getWriter().write(result);
                } catch (final ErreurAsyncException e) {
                    LOG.error("erreur de traitement de l'action", e);
                    resp.getWriter().write(e.getMessage());
                    resp.setStatus(500);
                }
            }
        } catch (final IOException | ServletException e) {
            LOG.error("Une erreur est survenue lors du traitement sur l'arborescence", e);
        }
    }
}
