package com.univ.tree.utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.autorisation.util.PermissionUtil;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.tree.bean.JsTreeNodeModel;
import com.univ.tree.processus.GestionJsTree;
import com.univ.utils.EscapeString;

public class JsTreeUtils {

    public static final String MODE_PARAM = "MODE";

    public static final String CODE_ROOT = "00";

    public static final String PARAM_BEAN_JSTREE = "JSTREEBEAN";

    public static final String BASE_URL_JSTREE = "/servlet/com.kportal.servlet.JsTreeServlet?JSTREEBEAN=%s";

    public static final String BASE_REQUEST_JSTREE = "/servlet/com.kportal.servlet.JsTreeServlet%s?JSTREEBEAN=%s";

    private static final Logger LOG = LoggerFactory.getLogger(JsTreeUtils.class);

    public static HashMap<String, String> getParameters(final HttpServletRequest req) {
        final HashMap<String, String> parameters = new HashMap<>();
        for (final String currentKey : req.getParameterMap().keySet()) {
            final String currentValue = req.getParameter(currentKey);
            if (StringUtils.isNotBlank(currentValue)) {
                parameters.put(currentKey, currentValue);
            }
        }
        // Récupération des infos pour tous les modes
        final String permission = req.getParameter("PERMISSION") == null ? StringUtils.defaultString(req.getPathInfo()) : StringUtils.defaultString(req.getParameter("PERMISSION"));
        parameters.put("PERMISSION", permission);
        parameters.put("NIVEAU", StringUtils.isBlank(req.getParameter("NIVEAU")) ? "1" : req.getParameter("NIVEAU"));
        return parameters;
    }

    public static String getAjaxUrl(final HttpServletRequest req) {
        final HashMap<String, String> params = getParameters(req);
        StringBuilder url = new StringBuilder(String.format(BASE_REQUEST_JSTREE, params.get("PERMISSION"), params.get(PARAM_BEAN_JSTREE)));
        final GestionJsTree<?> gestionJsTree = JsTreeManager.getGestionJsTree(params.get(PARAM_BEAN_JSTREE));
        try {
            if (gestionJsTree != null) {
                // Si aucune action n'est spécifiée, on corrige la requête
                if (StringUtils.isBlank(params.get("ACTION"))) {
                    gestionJsTree.assertParametersConsistency(params);
                }
                params.put("RACINE", "{0}");
                params.put("CODE", "{0}");
                params.remove("PERMISSION");
                params.remove(PARAM_BEAN_JSTREE);
                for (final String currentKey : params.keySet()) {
                    if (!StringUtils.isEmpty(params.get(currentKey))) {
                        url.append("&").append(EscapeString.escapeURL(currentKey)).append("=").append(params.get(currentKey));
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("les parametres passés dans la requête ne respectent pas les conditions pour generer un arbre", e);
            url = new StringBuilder("");
        }
        return url.toString();
    }

    public static String getFilterUrl(final HttpServletRequest req) {
        final HashMap<String, String> params = getParameters(req);
        return String.format("/servlet/com.kportal.servlet.JsTreeServlet/TECH/rub/M?JSTREEBEAN=%s&ACTION=FILTRER&QUERY={0}", params.get(PARAM_BEAN_JSTREE));
    }

    public static String getSelected(final HttpServletRequest req) {
        final HashMap<String, String> params = getParameters(req);
        final GestionJsTree<?> gestionJsTree = JsTreeManager.getGestionJsTree(params.get(PARAM_BEAN_JSTREE));
        return gestionJsTree.getSelectedIds(params.get("SELECTED"));
    }

    public static String getSearchAjaxUrl(final HttpServletRequest req) {
        final HashMap<String, String> params = getParameters(req);
        return getSearchAjaxUrl(params);
    }

    public static String getSearchAjaxUrl(final Map<String, String> params) {
        StringBuilder url = new StringBuilder(String.format(BASE_URL_JSTREE, params.get(PARAM_BEAN_JSTREE)));
        final GestionJsTree<?> gestionJsTree = JsTreeManager.getGestionJsTree(params.get(PARAM_BEAN_JSTREE));
        try {
            if (gestionJsTree != null) {
                params.put("ACTION", "RECHERCHER");
                gestionJsTree.assertParametersConsistency(params);
                params.remove(PARAM_BEAN_JSTREE);
                for (final String currentKey : params.keySet()) {
                    if (!StringUtils.isEmpty(params.get(currentKey))) {
                        url.append("&").append(currentKey).append("=").append(params.get(currentKey));
                    }
                }
            }
        } catch (final Exception e) {
            LOG.error("les parametres passés dans la requête ne respectent pas les conditions pour generer un arbre", e);
            url = new StringBuilder("");
        }
        return url.toString();
    }

    public static String getSearchAjaxUrl(final String params) {
        final HashMap<String, String> parameters = new HashMap<>();
        final String[] splitParams = params.split("&");
        for (final String currentParam : splitParams) {
            final String[] keyValue = currentParam.split("=");
            if (!StringUtils.isBlank(keyValue[0]) && !StringUtils.isBlank(keyValue[1])) {
                parameters.put(keyValue[0], keyValue[1]);
            }
        }
        return getSearchAjaxUrl(parameters);
    }

    /**
     * Teste si la rubrique est sélectionnable.
     *
     * @param rubrique
     *            La rubrique à tester
     *
     * @return true si la rubrique est sélectionnable
     *
     */
    public static boolean isRubriqueSelectable(final String permissions, final AutorisationBean autorisations, final RubriqueBean rubrique, final Set<String> ids) {
        // Controle le périmètre de l'utilisateur sur la rubrique
        boolean selectableItem = true;
        if (rubrique != null) {
            final boolean inIds = ids == null || ids.isEmpty() || ids.contains(rubrique.getCode());
            if (permissions.length() > 0 && inIds) {
                selectableItem = PermissionUtil.controlerPermissionRubrique(autorisations, permissions, rubrique.getCode());
            }
        }
        return selectableItem;
    }

    /**
     * Teste si la rubrique est visible dans l'arbre (rubrique sélectionnable ou ayant un fils visible.
     *
     * @param rubrique
     *            La rubrique à tester
     * @param selectable
     *            true si la rubrique est sélectionnable
     *
     * @return true si la rubrique est visible dans l'arbre
     *
     */
    public static boolean isRubriqueVisible(final String permissions, final AutorisationBean autorisations, final RubriqueBean rubrique, final boolean selectable, final Set<String> ids) {
        boolean visible = selectable;
        final boolean inIds = ids == null || ids.isEmpty() || rubrique == null || ids.contains(rubrique.getCode());
        // rubrique non sélectionnable
        if (!selectable)  {
            // on vérifie ses rubriques filles
            final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
            String codeRubrique = StringUtils.EMPTY;
            if (rubrique != null) {
                codeRubrique = rubrique.getCode();
            }
            final Iterator<RubriqueBean> listSousRubriquesIt = serviceRubrique.getRubriqueByCodeParent(codeRubrique).iterator();
            while (listSousRubriquesIt.hasNext() && !visible) {
                RubriqueBean sousRubrique = listSousRubriquesIt.next();
                visible = isRubriqueVisible(permissions, autorisations, sousRubrique, isRubriqueSelectable(permissions, autorisations, sousRubrique, ids), ids);
            }
        }
        return visible && inIds;
    }

    /**
     * Teste si le groupe DSI est sélectionnable.
     *
     * @param groupeDsi
     *            Le groupe DSI à tester
     *
     * @return true si le groupe DSI est sélectionnable
     *
     */
    public static boolean isGroupeSelectable(final String permissions, final AutorisationBean autorisations, final GroupeDsiBean groupeDsi, final Set<String> ids) {
        final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
        if(groupeDsi != null) {
            // Controle le périmètre de l'utilisateur sur le groupe
            boolean selectableItem = "1".equals(groupeDsi.getSelectionnable());
            final boolean inIds = ids == null || ids.isEmpty() || ids.contains(groupeDsi.getCode());
            if (selectableItem && permissions.length() > 0) {
                // patch filtre sur les groupes dynamiques
                if (permissions.startsWith("DYN")) {
                    selectableItem = !(groupeDsi.getRequeteGroupe().length() > 0);
                } else {
                    selectableItem = serviceGroupeDsi.controlerPermission(autorisations, permissions, groupeDsi.getCode());
                }
            }
            return selectableItem && inIds;
        }
        return false;
    }

    /**
     * Teste si le groupe est visible dans l'arbre (groupe sélectionnable ou ayant un fils visible).
     *
     * @param groupe
     *            Le groupe à tester
     * @param selectable
     *            true si le groupe est sélectionnable
     *
     * @return true si le groupe est visible dans l'arbre
     *
     */
    public static boolean isGroupeVisible(final String permissions, final AutorisationBean autorisations, final GroupeDsiBean groupe, final boolean selectable, final Set<String> ids) {
        if(groupe != null) {
            boolean visible = selectable;
            final boolean inIds = ids == null || ids.isEmpty() || ids.contains(groupe.getCode());
            if (!selectable) {
                // on vérifie ses groupes fils
                final ServiceGroupeDsi serviceGroupeDsi = ServiceManager.getServiceForBean(GroupeDsiBean.class);
                final Iterator<GroupeDsiBean> listSousGroupesIt = serviceGroupeDsi.getByParentGroup(groupe.getCode()).iterator();
                while (listSousGroupesIt.hasNext() && !visible) {
                    GroupeDsiBean currentGroup = listSousGroupesIt.next();
                    visible = isGroupeVisible(permissions, autorisations, currentGroup, isGroupeSelectable(permissions, autorisations, currentGroup, ids), ids);
                }
            }
            return visible && inIds;
        }
        return true;
    }

    public static JsTreeNodeModel getNodeWithCode(final JsTreeNodeModel tree, final String code) {
        JsTreeNodeModel node = null;
        for (final JsTreeNodeModel currentNode : tree.getChildren()) {
            if (currentNode.getMetadata().get("sCode").equals(code)) {
                node = currentNode;
            } else {
                if (currentNode.getChildren().size() > 0) {
                    node = getNodeWithCode(currentNode, code);
                }
            }
            if (node != null) {
                break;
            }
        }
        return node;
    }

    public enum Mode {
        STRUCTURE, RUBRIQUE, GROUPE
    }
}
