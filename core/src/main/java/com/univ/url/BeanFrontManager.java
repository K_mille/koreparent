package com.univ.url;

/**
 *
 * Classe servant à initialiser des Beans à utiliser dans les jsp via le tag <jsp:useBean id="t" class="<T>" scope="request" /> Il est utilisé par le produit pour initialiser un
 * maximum d'info sur le front gen.
 *
 * @author olivier.camon
 *
 * @param <T>
 *            le type de bean à ajouter à la request.
 */
public interface BeanFrontManager<T> {

    /**
     * Instancie un Bean de type T qui sera ajouter en attribut à la request avant de passer par les jsp.
     *
     * @return le bean qui sera utilisé en front
     */
    T initialiseBeanFront();
}
