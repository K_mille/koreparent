package com.univ.url;

import java.io.File;
import java.io.IOException;
import java.net.URLEncoder;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.identification.GestionnaireIdentification;
import com.jsbsoft.jtf.lang.CharEncoding;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.kportal.cms.objetspartages.om.ContentRedirect;
import com.kportal.core.webapp.WebAppUtil;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.SSOBean;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.ExceptionFicheArchivee;
import com.univ.utils.ExceptionFicheNonAccessible;
import com.univ.utils.ExceptionFicheNonTrouvee;
import com.univ.utils.ExceptionLogin;
import com.univ.utils.ExceptionSite;
import com.univ.utils.ServicesUtil;
// TODO: Auto-generated Javadoc

/**
 * The Class ContentServlet.
 */
public class ContentServlet extends HttpServlet {

    private static final Logger LOG = LoggerFactory.getLogger(ContentServlet.class);

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    /*
     * (non-Javadoc)
     *
     * @see javax.servlet.GenericServlet#getServletInfo()
     */
    @Override
    public String getServletInfo() {
        return "K-Portal - Content Servlet";
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * javax.servlet.http.HttpServlet#service(javax.servlet.http.HttpServletRequest
     * , javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void service(final HttpServletRequest request, final HttpServletResponse response) throws IOException, ServletException {
        try {
            // Contexte persistant pour les services cf FrontOfficeMgr
            ContexteUniv ctx = ContexteUtil.getContexteUniv();
            // accès en BO
            if (request.getRequestURI().startsWith("/adminsite")) {
                getServletContext().getNamedDispatcher("default").forward(request, response);
                return;
            }
            String forward = "";
            boolean kticketValide = false;
            try {
                final String parametreFWD = request.getParameter("FWD");
                final String parametreJSP = request.getParameter("JSP");
                // lecture d'un ticket
                if (request.getParameter("kticket") != null) {
                    kticketValide = gestionConnexionMultisite(request, response, ctx);
                    if ("".equals(parametreFWD)) {
                        LOG.debug("FWD = vide -> on sort de ContentServlet -->");
                        return;
                    }
                }
                final String dossierJspFo = ctx.getInfosSite().getJspFo();
                if (StringUtils.isNotEmpty(parametreFWD) && (kticketValide || isParametreValidePourForward(parametreFWD, dossierJspFo))) {
                    forward = parametreFWD;
                    LOG.debug("FWD = " + forward);
                } else {
                    LOG.debug("FWD = vide -> lecture fiche ou parametre JSP");
                    final FicheUniv fiche = FrontOfficeMgr.lireFiche(request);
                    if (fiche instanceof ContentRedirect && StringUtils.isNotBlank(((ContentRedirect) fiche).getUrlRedirect())) {
                        response.sendRedirect(((ContentRedirect) fiche).getUrlRedirect());
                        forward = StringUtils.EMPTY;
                        return;
                    }
                    if (isParametreValidePourForward(parametreJSP, dossierJspFo)) {
                        forward = parametreJSP;
                        LOG.debug("JSP = " + forward);
                    } else {
                        final FicheUniv ficheCourante = ctx.getFicheCourante();
                        // calcul de la jsp de forward
                        forward = dossierJspFo + "/fiche_" + ReferentielObjets.getNomObjet(ficheCourante) + ".jsp";
                        final String webAppPath = WebAppUtil.getAbsolutePath();
                        if (!new File(webAppPath + forward).exists()) {
                            forward = dossierJspFo + "/fiche_objet.jsp";
                        }
                    }
                }
            } catch (final ExceptionLogin | ExceptionSite | ExceptionFicheNonAccessible | ExceptionFicheArchivee e) {
                // Ce ne sont pas des erreurs, mais permet de gérer une redirection vers la page d'erreur correspondante
                LOG.debug(e.getMessage(), e);
                forward = ctx.getInfosSite().getJspFo() + "/jsb_exception.jsp";
                request.setAttribute("Exception", e);
            } catch (final ExceptionFicheNonTrouvee e) {
                // ExceptionFicheNonTrouvee n'est pas une erreur, mais permet d'indiquer que la fiche n'a pas été trouvée (404)
                LOG.debug(e.getMessage(), e);
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                forward = ctx.getInfosSite().getJspFo() + "/error/404.jsp?URL_DEMANDEE=" + URLEncoder.encode(request.getRequestURI(), CharEncoding.DEFAULT);
                final String ref = request.getHeader("referer");
                if (StringUtils.isNotEmpty(ref)) {
                    forward += "&REFERER=" + URLEncoder.encode(ref, CharEncoding.DEFAULT);
                }
                request.setAttribute("Exception", e);
            } catch (final Exception e) {
                LOG.error(e.getMessage(), e);
                forward = ctx.getInfosSite().getJspFo() + "/jsb_exception.jsp";
                request.setAttribute("Exception", e);
            } finally {
                if (StringUtils.isNotBlank(forward)) {
                    final javax.servlet.ServletContext context = getServletConfig().getServletContext();
                    final javax.servlet.RequestDispatcher rd = context.getRequestDispatcher(forward);
                    rd.forward(request, response);
                }
            }
        } catch (final Exception e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private boolean gestionConnexionMultisite(final HttpServletRequest request, final HttpServletResponse response, final ContexteUniv ctx) {
        // si non connecté
        boolean kticketValide = Boolean.FALSE;
        if (ctx.getKsession().length() == 0) {
            SSOBean ssoBean = null;
            try {
                // validation du ticket par un site distant
                ssoBean = ServicesUtil.getSSOBean(request);
                // transmission du ksession pour éviter la création d'un nouveau de fichier xml
                LOG.debug("enregistrement ksession dans la session http : " + ssoBean.getId());
                request.getSession(Boolean.FALSE).setAttribute(SessionUtilisateur.KSESSION, ssoBean.getId());
                String codeUtilisateur = ssoBean.getCodeKportal();
                if (GestionnaireIdentification.getInstance().getMappingLogin() && ssoBean.getCodeGestion().length() > 0) {
                    codeUtilisateur = ssoBean.getCodeGestion();
                }
                GestionnaireIdentification.getInstance().chargeInfoUser(codeUtilisateur, ctx, request.getSession(Boolean.FALSE), request.getHeader("user-agent"));
                ctx.initialiserInfosUtilisateur();
                LOG.debug("ksession dans le contexte :" + ctx.getKsession());
                LOG.debug("code utilisateur dans le contexte :" + ctx.getCode());
                kticketValide = Boolean.TRUE;
            } catch (final Exception e) {
                LOG.debug("erreur de traitement de la session utilisateur", e);
                if (ssoBean != null) {
                    if ("100".equals(ssoBean.getCodeRetour())) {
                        LOG.error("kticket non valide");
                    } else if ("200".equals(ssoBean.getCodeRetour())) {
                        LOG.error("session expirée pour le kticket");
                    } else if ("400".equals(ssoBean.getCodeRetour())) {
                        LOG.error("Erreur de validation du kticket");
                    } else if ("500".equals(ssoBean.getCodeRetour())) {
                        LOG.error("kreferer non valide");
                    } else if ("600".equals(ssoBean.getCodeRetour())) {
                        LOG.error("ksite non valide");
                    }
                } else {
                    LOG.error("kticket invalide ignoré");
                }
            }
        } else {
            // sinon suppression du kticket et du cookie sso si il existe
            LOG.debug("ksession existe -> suppression de kticket et maj du cookie 'sso' à false");
            ServicesUtil.supprimerTicket(request.getParameter("kticket"));
            final Cookie[] tCookies = request.getCookies();
            for (final Cookie cookie : tCookies) {
                if ("sso".equals(cookie.getName())) {
                    cookie.setValue("false");
                    cookie.setMaxAge(0);
                    response.addCookie(cookie);
                    break;
                }
            }
        }
        return kticketValide;
    }

    private boolean isParametreValidePourForward(final String urlAForwarder, final String dossierJspValide) {
        return StringUtils.isNotEmpty(urlAForwarder) && urlAForwarder.startsWith(dossierJspValide + "/");
    }
}
