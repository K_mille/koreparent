package com.univ.url;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.InfoBean;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.kportal.ihm.utils.EncadresFrontUtils;
import com.kportal.ihm.utils.FrontUtil;
import com.univ.collaboratif.om.Espacecollaboratif;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.SousParagrapheBean;
import com.univ.objetspartages.services.ServiceUser;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

public class FrontOfficeBeanUtil implements BeanFrontManager<FrontOfficeBean> {

    private static final Logger LOG = LoggerFactory.getLogger(FrontOfficeBeanUtil.class);

    private static final String SAISIE_FRONT_KEY = "SAISIE_FRONT";

    private static void initialiseEncadres(final ContexteUniv ctx, final FrontOfficeBean foBean) {
        if (!foBean.isCollaboratif() && !foBean.isSaisieFront()) {
            foBean.setEncadresFiche(EncadresFrontUtils.getEncadresParType());
            foBean.setEncadresAutoFiche(EncadresFrontUtils.getEncadresAutoFiche(ctx, foBean.getFicheUniv()));
            foBean.setEncadresRecherche(EncadresFrontUtils.getEncadresRechercheFiche(foBean.getFicheUniv()));
            foBean.setEncadrePresent(existeEncadrePourPage(foBean));
        } else if (StringUtils.isNotBlank(ctx.getTemplateExterne())) {
            final Map<String, List<SousParagrapheBean>> encadresExterne = new HashMap<>();
            encadresExterne.put(EncadresFrontUtils.EXTERNE, EncadresFrontUtils.getEncadresExterne(ctx));
            foBean.setEncadresFiche(encadresExterne);
        }
    }

    private static boolean existeEncadrePourPage(final FrontOfficeBean foBean) {
        boolean existeEncadreFiche = Boolean.FALSE;
        for (final List<SousParagrapheBean> sousParagrapheEncadre : foBean.getEncadresFiche().values()) {
            existeEncadreFiche = !sousParagrapheEncadre.isEmpty();
            if (existeEncadreFiche) {
                break;
            }
        }
        return existeEncadreFiche || StringUtils.isNotBlank(foBean.getEncadresAutoFiche()) || !foBean.getEncadresRecherche().isEmpty();
    }

    /**
     * A partir du calcul des rubriques on récupère les éléments graphiques de la rubrique de navigation de niveau 1
     */
    private static void initialiseRubrique(final ContexteUniv ctx, final FrontOfficeBean foBean) {
        foBean.setVisuelRubrique(FrontUtil.getUrlBandeauCourant(ctx));
        final List<RubriqueBean> listeRubriques = FrontUtil.calculerRubriquesPageCourante(ctx, 5);
        final RubriqueBean rubriqueNiveau1 = listeRubriques.get(2);
        if (rubriqueNiveau1 != null) {
            try {
                foBean.setAccrocheRubrique(UnivWebFmt.formaterEnHTML(ctx, rubriqueNiveau1.getAccroche()));
            } catch (final Exception e) {
                LOG.error("impossible de calculer l'accroche de la rubrique courante", e);
            }
            foBean.setContactRubrique(rubriqueNiveau1.getContact());
            if (StringUtils.isNotBlank(rubriqueNiveau1.getCouleurTitre())) {
                String couleurTitreRubrique = rubriqueNiveau1.getCouleurTitre();
                if (!couleurTitreRubrique.startsWith("#")) {
                    couleurTitreRubrique = "#" + couleurTitreRubrique;
                }
                foBean.setCouleurTitreRubrique(couleurTitreRubrique);
            }
            if (rubriqueNiveau1.getCouleurFond().length() > 0) {
                String couleurFondRubrique = rubriqueNiveau1.getCouleurFond();
                if (!couleurFondRubrique.startsWith("#")) {
                    couleurFondRubrique = "#" + couleurFondRubrique;
                }
                foBean.setCouleurFondRubrique(couleurFondRubrique);
            }
        }
    }

    private static void initialiseRedacteur(final FrontOfficeBean foBean) {
        final ServiceUser serviceUser = ServiceManager.getServiceForBean(UtilisateurBean.class);
        final FicheUniv ficheCourante = foBean.getFicheUniv();
        if (ficheCourante != null && isUtilisateurValide(ficheCourante.getCodeRedacteur())) {
            final UtilisateurBean utilisateur = serviceUser.getByCode(ficheCourante.getCodeRedacteur());
            if (utilisateur != null) {
                foBean.setRedacteur(String.format("%s %s", utilisateur.getPrenom(), utilisateur.getNom()));
            }
        }
    }

    private static boolean isUtilisateurValide(final String codeRedacteur) {
        return StringUtils.isNotBlank(codeRedacteur) && !ServiceUser.UTILISATEUR_ANONYME.equals(codeRedacteur);
    }

    @Override
    public FrontOfficeBean initialiseBeanFront() {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final FrontOfficeBean foBean = new FrontOfficeBean();
        foBean.setSaisieFront(isSaisieFront(ctx));
        foBean.setFicheUniv(ctx.getFicheCourante());
        foBean.setAccueilSite(FrontUtil.isAccueilSite(foBean.getFicheUniv()));
        foBean.setAccueilRubrique(FrontUtil.isFicheAccueilRubrique(foBean.getFicheUniv()));
        foBean.setApercu(ctx.isApercu());
        foBean.setMetatag(ctx.getMetaCourant());
        foBean.setCollaboratif(Espacecollaboratif.isExtensionActivated() && StringUtils.isNotBlank(ctx.getEspaceCourant()));
        initialiseEncadres(ctx, foBean);
        foBean.setActivationDSI("1".equals(PropertyHelper.getCoreProperty("dsi.activation")));
        foBean.setDsi(foBean.isActivationDSI() && StringUtils.isNotBlank(ctx.getCode()));
        foBean.setJspFo(ctx.getInfosSite().getJspFo());
        foBean.setRecherche(FrontUtil.isRechecheFullText(ctx.getUrlPageCourante()));
        initialiseRubrique(ctx, foBean);
        initialiseRedacteur(foBean);
        initialiseMenus(foBean);
        foBean.setNavigationSecondairePresente(!FrontUtil.isRechecheFullText(ctx.getUrlPageCourante()) && (foBean.isCollaboratif() || (foBean.getMenuSecondaire() != null && !foBean.getMenuSecondaire().isEmpty())));
        foBean.setIdCentre(getIdCentre(foBean));
        foBean.setClassBody(getClassBody(foBean));
        return foBean;
    }

    /**
     * Renvoie l'id de l'élément principal de la page.
     * NOTE : ceci est un report depuis la JSP header.jsp → améliorer plus tard en utilisant des classes CSS.
     * @param frontOfficeBean Bean Front
     * @return ID à utiliser
     */
    private static String getIdCentre(FrontOfficeBean frontOfficeBean) {
        String idCentre = "contenu_sans_nav_sans_encadres";
        if (frontOfficeBean.isEncadrePresent() && frontOfficeBean.isNavigationSecondairePresente()) {
            idCentre = "avec_nav_avec_encadres";
        } else if (frontOfficeBean.isEncadrePresent()) {
            idCentre = "sans_nav_avec_encadres";
        } else if (frontOfficeBean.isNavigationSecondairePresente()) {
            idCentre = "avec_nav_sans_encadres";
        }
        return idCentre;
    }

    /**
     * Calcul des classes à appliquer au body (body-switching).
     * Calcul des classes à appliquer au body (body-switching).
     * @param frontOfficeBean Bean Front
     * @return Classes CSS séparées par des espaces
     */
    private static String getClassBody(FrontOfficeBean frontOfficeBean) {
        List<String> classBody = new ArrayList<>();
        FicheUniv ficheUniv = frontOfficeBean.getFicheUniv();
        if (frontOfficeBean.isCollaboratif()) {
            classBody.add("collaboratif");
        } else if (ficheUniv != null) {
            classBody.add("fiche");
        } else if (frontOfficeBean.isRecherche()) {
            classBody.add("rechercheFullText");
        } else {
            classBody.add("recherche");
        }
        if (ficheUniv != null) {
            String nomClasseFiche = ficheUniv.getClass().getName();
            String codeObjet = ReferentielObjets.getCodeObjetParClasse(nomClasseFiche);
            classBody.add(ReferentielObjets.getNomObjet(codeObjet).toLowerCase());
        }
        if (frontOfficeBean.isAccueilSite()) {
            classBody.add("accueil");
        }
        if (frontOfficeBean.isAccueilRubrique()) {
            classBody.add("rubrique");
        }
        if (frontOfficeBean.isDsi()) {
            classBody.add("connecte");
        }
        if (frontOfficeBean.isApercu()) {
            classBody.add("apercu");
        }
        return StringUtils.join(classBody, StringUtils.SPACE);
    }

    private void initialiseMenus(final FrontOfficeBean foBean) {
        foBean.setMenuAccesDirect(FrontUtil.getMenuAccesDirectParCategorie());
        foBean.setMenuLangue(FrontUtil.getMenuLangueParCategorie());
        foBean.setMenuPrincipal(FrontUtil.getMenuPrincipalParCategorie());
        foBean.setMenuSecondaire(FrontUtil.getMenuSecondaireDepuisMenuPrincipal(foBean.getMenuPrincipal()));
        foBean.setMenuPiedDePage(FrontUtil.getMenuPiedDePageParCategorie());
        foBean.setMenuReseauxSociaux(FrontUtil.getMenuReseauxSociauxParCategorie());
    }

    private boolean isSaisieFront(final ContexteUniv ctx) {
        final InfoBean infoBean = (InfoBean) ctx.getRequeteHTTP().getAttribute("infoBean");
        if (infoBean != null && StringUtils.isNotBlank(infoBean.get(SAISIE_FRONT_KEY, String.class)) && StringUtils.isNotBlank(infoBean.getNomProcessus())) {
            return FrontUtil.isSaisieFront(infoBean.get(SAISIE_FRONT_KEY, String.class), infoBean.getNomProcessus());
        } else {
            return FrontUtil.isSaisieFront(ctx.getRequeteHTTP().getParameter(SAISIE_FRONT_KEY), ctx.getRequeteHTTP().getParameter("PROC"));
        }
    }
}
