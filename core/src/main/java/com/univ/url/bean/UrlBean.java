package com.univ.url.bean;

import java.util.List;

import com.univ.objetspartages.bean.AbstractPersistenceBean;

/**
 * Created by olivier.camon on 30/09/15.
 */
public class UrlBean extends AbstractPersistenceBean {

    private static final long serialVersionUID = -230702807102580281L;

    private String url;

    private String sectionCode;

    private String codeSite;

    private List<String> parentsSectionCodes;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSectionCode() {
        return sectionCode;
    }

    public void setSectionCode(String sectionCode) {
        this.sectionCode = sectionCode;
    }

    public String getCodeSite() {
        return codeSite;
    }

    public void setCodeSite(String codeSite) {
        this.codeSite = codeSite;
    }

    public List<String> getParentsSectionCodes() {
        return parentsSectionCodes;
    }

    public void setParentsSectionCodes(List<String> parentsSectionCodes) {
        this.parentsSectionCodes = parentsSectionCodes;
    }

}
