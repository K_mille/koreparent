package com.univ.url.service;

import org.quartz.JobBuilder;
import org.quartz.JobDataMap;
import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.extension.module.plugin.rubrique.PageAccueilRubriqueManager;
import com.kportal.scheduling.module.SchedulerManager;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Created by olivier.camon on 13/11/15.
 */
public class ServiceUrlObserver {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceUrlObserver.class);

    private ServiceRubrique serviceRubrique;

    private SchedulerManager schedulerManager;

    private Scheduler scheduler;

    public void setServiceRubrique(final ServiceRubrique serviceRubrique) {
        this.serviceRubrique = serviceRubrique;
    }

    public void init() throws SchedulerException {
        final JobDetail urlJob = JobBuilder.newJob(UrlUpdaterJob.class).withIdentity(UrlUpdaterJob.JOB_KEY).storeDurably().build();
        scheduler = schedulerManager.getScheduler();
        scheduler.getListenerManager().addSchedulerListener(new SchedulerListenerForUrl(scheduler, urlJob));
    }

    /**
     * Methode appelée lors de la création / modification / suppression d'un {@link InfosSite}
     * La methode est appelée via publish/subscribe
     * @param parameter : peut varier suivant l'appelant (String, InfosSite)
     */
    public void updateContext(final Object parameter) {
        LOG.debug("an update is schedule on the URL");
        JobDataMap dataMap = new JobDataMap();
        dataMap.put(UrlUpdaterJob.URL_PARAMETER, parameter);
        try {
            if (scheduler.isStarted()) {
                scheduler.triggerJob(UrlUpdaterJob.JOB_KEY, dataMap);
            } else {
                LOG.info("scheduler not started, the job will be triggered with a listener");
            }
        } catch (SchedulerException e) {
            LOG.error("unable to schedule url job", e);
        }
    }

    public void setSchedulerManager(final SchedulerManager schedulerManager) {
        this.schedulerManager = schedulerManager;
    }
}
