/*
 * Created on 16 janv. 2007
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.utils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
// TODO: Auto-generated Javadoc

/**
 * The Class ExceptionFicheArchivee.
 */
public class ExceptionFicheArchivee extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -5907466784685488513L;

    /**
     * Instantiates a new exception fiche archivee.
     *
     * @param _message
     *            the _message
     */
    public ExceptionFicheArchivee(final String _message) {
        super(_message);
    }
}
