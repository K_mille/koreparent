/*
 * Created on 14 janv. 2005
 *
 * To change the template for this generated file go to
 * Window - Preferences - Java - Code Generation - Code and Comments
 */
package com.univ.utils;

import com.jsbsoft.jtf.exception.ErreurApplicative;
// TODO: Auto-generated Javadoc

/**
 * The Class ExceptionLogin.
 *
 * @author romain
 *
 *         To change the template for this generated type comment go to Window - Preferences - Java - Code Generation - Code and Comments
 */
public class ExceptionLogin extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = -5231055049983686627L;

    /** The url redirect. */
    private String urlRedirect = "";

    /**
     * Instantiates a new exception login.
     *
     * @param _urlRedirect
     *            the _url redirect
     */
    public ExceptionLogin(final String _urlRedirect) {
        super("redirection login");
        urlRedirect = _urlRedirect;
    }

    /**
     * Gets the url redirect.
     *
     * @return Returns the urlRedirect.
     */
    public String getUrlRedirect() {
        return urlRedirect;
    }
}
