package com.univ.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
// TODO: Auto-generated Javadoc

/**
 * The Class ExceptionSite.
 *
 * @author romain
 */
public class ExceptionSite extends ErreurApplicative {

    /**
     *
     */
    private static final long serialVersionUID = 691069140571703797L;

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionSite.class);

    /** URL absolue de redirection. */
    private String urlRedirect = "";

    /**
     * Instantiates a new exception site.
     *
     * @param _urlRedirect
     *            the _url redirect
     */
    public ExceptionSite(final String _urlRedirect) {
        super("redirection site");
        LOG.debug("redirection vers " + _urlRedirect);
        urlRedirect = _urlRedirect;
    }

    /**
     * Gets the url redirect.
     *
     * @return Returns the urlRedirect.
     */
    public String getUrlRedirect() {
        return urlRedirect;
    }
}
