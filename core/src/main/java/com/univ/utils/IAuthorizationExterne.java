package com.univ.utils;

import com.jsbsoft.jtf.database.OMContext;
// TODO: Auto-generated Javadoc

/**
 * Cette interface est à utiliser pour déléguer à une source EXTERNE l'authorisation de l'utilisateur et pour synchroniser un utilisateur a une source externe.
 *
 * @author Boutin
 */
public interface IAuthorizationExterne {

    /** The JT f_ authorizatio n_ classe. */
    String JTF_AUTHORIZATION_CLASSE = "authorization.externe.classe";

    /**
     * Synchronise.
     *
     * @param _login
     *            the _login
     *
     * @return true, if successful
     */
    boolean synchronise(String _login);

    /**
     * Authorized.
     *
     * @param _login
     *            the _login
     * @param _ctx
     *            the _ctx
     *
     * @return true, if successful
     */
    boolean authorized(String _login, OMContext _ctx);
}
