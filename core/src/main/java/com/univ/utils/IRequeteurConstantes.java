package com.univ.utils;

/**
 * The Interface IRequeteurConstantes.
 */
public interface IRequeteurConstantes {

    /** The Constant MODE. */
    String MODE = "MODE";

    /** The Constant ACTIONS. */
    String ACTIONS = "ACTIONS";

    /** The Constant AVEC_DOSSIER. */
    String AVEC_DOSSIER = "DOSSIER";

    /** The Constant LISTE_OBJETS. */
    String LISTE_OBJETS = "LISTE";

    /** The Constant AVEC_PAGINATION. */
    String AVEC_PAGINATION = "PAGE";

    /** The Constant AVEC_AJOUT. */
    String AVEC_AJOUT = "AJOUT";

    /** The Constant REQUETE. */
    String REQUETE = "REQUETE";

    /** The Constant MODE_CONSULTATION. */
    String MODE_CONSULTATION = "CONSUL";

    /** The Constant MODE_MODIFICATION. */
    String MODE_MODIFICATION = "MODIF";

    /** The Constant MODE_VALIDATION. */
    String MODE_VALIDATION = "VALID";

    /** The Constant REQUETE_SANS_COUNT. */
    String REQUETE_SANS_COUNT = "1";

    /** The Constant REQUETE_AVEC_COUNT. */
    String REQUETE_AVEC_COUNT = "2";

    /** The Constant REQUETE_COUNT. */
    String REQUETE_COUNT = "3";

    /** The Constant CODE_DYNAMIQUE. */
    String CODE_DYNAMIQUE = "DYNAMIK";

    /** The Constant SANS_CODE_DYNAMIQUE. */
    String SANS_CODE_DYNAMIQUE = "SANSDYNAMIK";

    /** The Constant ACTION_VOIR. */
    String ACTION_VOIR = "V";

    /** The Constant ACTION_MODIFIER. */
    String ACTION_MODIFIER = "M";

    /** The Constant ACTION_SUPPRIMER. */
    String ACTION_SUPPRIMER = "S";

    /** The Constant TRI_LIBELLE. */
    String TRI_LIBELLE = "1";

    /** The Constant TRI_DATE. */
    String TRI_DATE_DESC = "2";

    /** The Constant TRI_AUTEUR. */
    String TRI_AUTEUR = "3";

    /** The Constant TRI_ETAT. */
    String TRI_ETAT = "4";

    /** The Constant TRI_OBJET. */
    String TRI_OBJET = "5";

    /** The Constant TRI_RUBRIQUE. */
    String TRI_RUBRIQUE = "6";

    /** The Constant TRI_STRUCTURE. */
    String TRI_STRUCTURE = "7";

    /** The Constant TRI_DSI. */
    String TRI_DSI = "8";

    /** The Constant TRI_LANGUE. */
    String TRI_LANGUE = "9";

    String TRI_DATE_ASC = "10";

    /** The Constant ORDRE_TRI_DESCENDANT. */
    String ORDRE_TRI_DESCENDANT = "desc";

    /** The Constant ATTRIBUT_ETAT_OBJET. */
    String ATTRIBUT_ETAT_OBJET = "ETAT_OBJET";

    /** The Constant ATTRIBUT_CODE_OBJET. */
    String ATTRIBUT_CODE_OBJET = "CODE_OBJET";

    /** The Constant ATTRIBUT_LIBELLE_FICHE. */
    String ATTRIBUT_LIBELLE_FICHE = "LIBELLE_FICHE";

    /** The Constant ATTRIBUT_CODE_REDACTEUR. */
    String ATTRIBUT_CODE_REDACTEUR = "CODE_REDACTEUR";

    /** The Constant ATTRIBUT_REDACTEUR. */
    String ATTRIBUT_REDACTEUR = "REDACTEUR";

    /** The Constant ATTRIBUT_CODE_RUBRIQUE. */
    String ATTRIBUT_CODE_RUBRIQUE = "CODE_RUBRIQUE";

    /** The Constant ATTRIBUT_CODE_RATTACHEMENT. */
    String ATTRIBUT_CODE_RATTACHEMENT = "CODE_RATTACHEMENT";

    /** The Constant ATTRIBUT_DIFFUSION_PUBLIC_VISE. */
    String ATTRIBUT_DIFFUSION_PUBLIC_VISE = "DIFFUSION_PUBLIC_VISE";

    /** The Constant ATTRIBUT_DIFFUSION_MODE_RESTRICTION. */
    String ATTRIBUT_DIFFUSION_MODE_RESTRICTION = "DIFFUSION_MODE_RESTRICTION";

    /** The Constant ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION. */
    String ATTRIBUT_DIFFUSION_PUBLIC_VISE_RESTRICTION = "DIFFUSION_PUBLIC_VISE_RESTRICTION";

    /** The Constant ATTRIBUT_LANGUE. */
    String ATTRIBUT_LANGUE = "LANGUE_FICHE";

    /** The Constant ATTRIBUT_DATE_DEBUT. */
    String ATTRIBUT_DATE_DEBUT = "DATE_DEBUT";

    /** The Constant ATTRIBUT_DATE_FIN. */
    String ATTRIBUT_DATE_FIN = "DATE_FIN";

    /** The Constant ATTRIBUT_TYPE_DATE. */
    String ATTRIBUT_TYPE_DATE = "TYPE_DATE";

    /** The Constant TYPE_DATE_MODIFICATION. */
    String TYPE_DATE_MODIFICATION = "DATE_MODIFICATION";

    /** The Constant TITRE. */
    String TITRE = "TITRE";

    /** The Constant TITRE_MES_FICHES_A_VALIDER. */
    String TITRE_MES_FICHES_A_VALIDER = "ST_REQUETEUR_TITRE_MES_FICHES_A_VALIDER";

    /** The Constant TITRE_MES_FICHES. */
    String TITRE_MES_FICHES = "ST_REQUETEUR_TITRE_MES_FICHES";

    /** The Constant NB_RESULTAT. */
    int NB_RESULTAT = 100000;
}
