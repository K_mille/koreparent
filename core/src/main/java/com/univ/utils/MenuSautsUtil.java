package com.univ.utils;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.module.composant.Menu;

public class MenuSautsUtil {

    public static final String INFOBEAN_FIELD = "MENU_SAUTS_ITEMS";

    public static Menu retournerMenuSauts(InfoBean infoBean) {
        final String sousOnglet = StringUtils.defaultString(infoBean.getString("SOUS_ONGLET"));
        final Menu onglets = (Menu) infoBean.get(INFOBEAN_FIELD, Menu.class);
        Menu menuSauts = new Menu();
        if (onglets != null && onglets.getSousMenu() != null) {
            for (final Menu onglet : onglets.getSousMenu()) {
                if (sousOnglet.equals(onglet.getCode())) {
                    menuSauts = onglet;
                }
            }
        }
        return menuSauts;
    }

    public static boolean hasMenuSauts(InfoBean infoBean) {
        final Menu menu = retournerMenuSauts(infoBean);
        return !(menu.getSousMenu().isEmpty());
    }
}
