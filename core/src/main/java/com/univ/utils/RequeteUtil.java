package com.univ.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.database.OMContext;
import com.jsbsoft.jtf.textsearch.ResultatRecherche;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.extension.module.plugin.objetspartages.IPluginRecherche;
import com.kportal.extension.module.plugin.objetspartages.PluginRechercheHelper;
import com.univ.objetspartages.bean.ProfildsiBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.services.ServiceProfildsi;

/**
 * Classe de parsing des requetes sur les objets
 */
public class RequeteUtil {

    private static final Logger LOG = LoggerFactory.getLogger(RequeteUtil.class);

    /**
     * Classe destinée à centraliser les requêtes sur les objets.
     */
    public RequeteUtil() {
        super();
    }

    /**
     * Lecture d'une fiche issue du résultat de la requête sous la forme (objet;code;langue).
     *
     * @param _ctx
     *            contexte pour accès à la base
     * @param resultat
     *            the resultat
     *
     * @return the fiche univ
     *
     * @throws Exception
     *             the exception
     * @deprecated la méthode utilise le ctx uniquement pour l'accès à la base. Utilisez la même méthode sans le contexte en paramètre
     */
    @Deprecated
    public static FicheUniv lireFiche(final OMContext _ctx, final ResultatRecherche resultat) throws Exception {
        return lireFiche(resultat);
    }

    /**
     * Lecture d'une fiche issue du résultat de la requête .
     *
     * @param resultat
     *            the resultat
     *
     * @return la ficheUniv correspondant à la description ou null si non trouvée
     *
     */
    public static FicheUniv lireFiche(final ResultatRecherche resultat) {
        final String objet = resultat.getObjet();
        final String idFiche = resultat.getIdFiche();
        if (StringUtils.isEmpty(objet) || StringUtils.isEmpty(idFiche)) {
            return null;
        }
        FicheUniv ficheUniv = ReferentielObjets.instancierFiche(objet);
        if (ficheUniv == null) {
            return null;
        }
        ficheUniv.init();
        // gestion des fiches indexées mais supprimées
        try (ContexteDao ctx = new ContexteDao()) {
            ficheUniv.setCtx(ctx);
            ficheUniv.setIdFiche(Long.valueOf(idFiche));
            ficheUniv.retrieve();
        } catch (final Exception e) {
            LOG.warn("Fiche inexistante id = " + idFiche, e);
            ficheUniv = null;
        }
        return ficheUniv;
    }

    /**
     * Lecture d'une fiche issue du résultat de la requête sous la forme (objet;id).
     *
     * @param ctx              contexte pour accès à la base
     * @param descriptionFiche the description fiche
     * @return the fiche univ
     * @throws Exception the exception
     * @deprecated la méthode utilise le ctx uniquement pour l'accès à la base. Utilisez la même méthode sans le contexte en paramètre
     */
    @Deprecated
    public static FicheUniv lireFiche(final OMContext ctx, final String descriptionFiche) throws Exception {
        return lireFiche(descriptionFiche);
    }

    /**
     * Lecture d'une fiche issue du résultat de la requête sous la forme (objet;id).
     *
     * @param descriptionFiche Une chaine contenant le code de l'objet et l'id de la fiche...
     * @return la ficheUniv correspondant à la description ou null si non trouvé
     * @deprecated Utiliser {@link #lireFiche(ResultatRecherche)}
     */
    @Deprecated
    public static FicheUniv lireFiche(final String descriptionFiche) {
        FicheUniv ficheUniv = null;
        String objet = "";
        String idFiche = "";
        int indiceToken = 0;
        final StringTokenizer st = new StringTokenizer(descriptionFiche, ";");
        while (st.hasMoreTokens()) {
            if (indiceToken == 0) {
                objet = st.nextToken();
            } else if (indiceToken == 1) {
                idFiche = st.nextToken();
            } else {
                st.nextToken();
            }
            indiceToken++;
        }
        if ((objet.length() == 0) || (idFiche.length() == 0)) {
            return ficheUniv;
        }
        ficheUniv = ReferentielObjets.instancierFiche(objet);
        ficheUniv.init();
        try (ContexteDao ctx = new ContexteDao()) {
            ficheUniv.setCtx(ctx);
            ficheUniv.setIdFiche(Long.valueOf(idFiche));
            ficheUniv.retrieve();
        } catch (final Exception e) {
            LOG.warn("Fiche inexistante id = " + idFiche, e);
            ficheUniv = null;
        }
        return ficheUniv;
    }

    /**
     * Renvoie la valeur d'un paramètre de la requête (ou "") en parsant la requête.
     *
     * @param requete      the requete
     * @param nomParametre the nom parametre
     * @return the string
     */
    public static String renvoyerParametre(final String requete, final String nomParametre) {
        // on ne gère plus l'uppercase par défaut, car certaines fonctions (type Rubrique.renvoyerItemRubrique) sont sensibles à la casse
        return renvoyerParametre(requete, nomParametre, false);
    }

    /**
     * Renvoie la valeur d'un paramètre de la requête (ou "") en parsant la requête.
     *
     * @param requete      the requete
     * @param nomParametre the nom parametre
     * @param toUpperCase   the to upper case
     * @return the string
     */
    public static String renvoyerParametre(final String requete, final String nomParametre, final boolean toUpperCase) {
        String res = "";
        if (requete != null) {
            final String chaineARechercher = nomParametre.toUpperCase() + "=";
            final StringTokenizer st = new StringTokenizer(requete, "&");
            while (st.hasMoreTokens()) {
                final String val = st.nextToken();
                if (val.toUpperCase().startsWith(chaineARechercher)) {
                    if (chaineARechercher.length() < val.length()) {
                        res = val.substring(chaineARechercher.length());
                        if (toUpperCase) {
                            res = res.toUpperCase();
                        }
                    } else {
                        res = "";
                    }
                }
            }
        }
        return res;
    }

    /**
     * Traitement générique d'une requête : une requête se présente sous la forme OBJET=xxxx&PARAM1=VALEUR1&PARAM2=VALEUR2 .... Pour l'instant, cette méthode ne fonctionne que pour
     * un objet donné (pas pour tous) (parametre objet de l'utl)
     * Renvoie un vecteur qui contient la liste des objets ( à exploiter par LireFiche) AM 02.2003 Modification : + appel rechercherPages avec critere FROM + correction sur le tri
     * des actus pour les fils.
     *
     * @param ctx              contexte pour accès à la base
     * @param requete          the requete
     * @param renvoyerResultat the renvoyer resultat
     * @return the vector
     * @throws Exception the exception
     * @deprecated Utiliser {@link #traiterRequete(OMContext, String)}
     */
    @Deprecated
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static Vector traiterRequete(final OMContext ctx, String requete, final boolean renvoyerResultat) throws Exception {
        Vector resultats = new Vector<>();
        List<ResultatRecherche> res = traiterRequete(ctx, requete);
        if (renvoyerResultat) {
            resultats = new Vector<>(res);
        } else {
            for (final ResultatRecherche resultatRecherche : res) {
                resultats.add(resultatRecherche.getObjet() + ";" + resultatRecherche.getIdFiche());
            }
        }
        return resultats;
    }

    /**
     * Traitement générique d'une requête : une requête se présente sous la forme OBJET=xxxx&PARAM1=VALEUR1&PARAM2=VALEUR2 ....
     * Cette méthode ne fonctionne que pour un objet donné (parametre OBJET de la requête).
     * Elle renvoie une liste d'objets ResultatRecherche à exploiter ensuite par la méthode RequeteUtil.lireFiche().
     *
     * @param ctx     the contexte
     * @param requete the requete
     * @return the vector
     * @throws Exception the exception
     */
    public static List<ResultatRecherche> traiterRequete(final OMContext ctx, String requete) throws Exception {
        requete = StringUtils.replace(requete, "+", "%2B");
        requete = EscapeString.unescapeURL(requete);
        List<ResultatRecherche> res = new ArrayList<>();
        LOG.debug("requete (traiterRequete) :" + requete);
        final String REQUETE = requete.toUpperCase();
        final String objet = renvoyerParametre(REQUETE, "OBJET");
        if (objet.length() > 0) {
            // Optimisation des requetes en front : ajout d'une variable dans le contexte
            ctx.getDatas().put("optimizedSelect", "1");
            // calcul du plugin de requete actif
            final IPluginRecherche pluginRequete = PluginRechercheHelper.getPluginActif(REQUETE);
            if (pluginRequete != null) {
                requete = pluginRequete.preparerRequete(ctx, requete);
            }
            if (StringUtils.isNotEmpty(requete)) {
                final boolean annuleCritereNombre = pluginRequete != null && pluginRequete.annuleCritereNombre(REQUETE);
                int limit = -1;
                final int idxDebNb = requete.indexOf("NOMBRE=");
                // on ne positionne pas le témoin optimizedLimit
                // car la limitation du nombre de résultats se fera APRES sur le plugin
                if (annuleCritereNombre && idxDebNb != -1) {
                    try {
                        limit = Integer.parseInt(RequeteUtil.renvoyerParametre(requete, "NOMBRE"));
                        int idxFinNb = requete.indexOf("&", idxDebNb + 1);
                        if (idxFinNb == -1) {
                            idxFinNb = requete.length() - 1;
                        }
                        requete = requete.substring(0, idxDebNb) + requete.substring(idxFinNb + 1);
                    } catch (final NumberFormatException e) {
                        LOG.debug("unable to parse the given string", e);
                    }
                }
                if (ctx instanceof ContexteUniv) {
                    String groupeDsi = renvoyerParametre(requete, "GROUPE_DSI").replaceAll("@", ",");
                    // migration du tag profil
                    final String profilDsi = renvoyerParametre(requete, "PROFIL_DSI");
                    if (profilDsi.equals(IRequeteurConstantes.CODE_DYNAMIQUE)) {
                        groupeDsi = profilDsi;
                    } else {
                        final ServiceProfildsi serviceProfildsi = ServiceManager.getServiceForBean(ProfildsiBean.class);
                        final ProfildsiBean profildsiBean = serviceProfildsi.getByCode(profilDsi);
                        if (profildsiBean != null && profildsiBean.getGroupes().size() > 0) {
                            for (final String codeGroupe : profildsiBean.getGroupes()) {
                                if (groupeDsi.length() > 0) {
                                    groupeDsi += ";";
                                }
                                groupeDsi += codeGroupe;
                            }
                        }
                    }
                    ((ContexteUniv) ctx).setGroupePersonnalisationCourant(groupeDsi);
                    final String espace = renvoyerParametre(requete, "ESPACE");
                    ((ContexteUniv) ctx).setEspacePersonnalisationCourant(espace);
                }
                // requête par défaut sur l'objet
                final long debutRequete = System.currentTimeMillis();
                if (pluginRequete == null || !pluginRequete.isExclusif()) {
                    FicheUniv ficheUniv = ReferentielObjets.instancierFiche(objet);
                    if (ficheUniv != null) {
                        ficheUniv.setCtx(ctx);
                        ficheUniv.init();
                        ficheUniv.traiterRequete(requete);
                        if (ctx.getDatas().get("optimizedLimit") != null) {
                            limit = Integer.parseInt((String) ctx.getDatas().get("optimizedLimit"));
                        }
                        final List<Long> tListeFiches = new ArrayList<>();
                        while (ficheUniv.nextItem() && (limit == -1 || res.size() < limit)) {
                            // gestion des doublons
                            if (!tListeFiches.contains(ficheUniv.getIdFiche())) {
                                tListeFiches.add(ficheUniv.getIdFiche());
                                final ResultatRecherche resultat = new ResultatRecherche();
                                resultat.setObjet(objet);
                                resultat.setIdFiche(String.valueOf(ficheUniv.getIdFiche()));
                                res.add(resultat);
                            }
                        }
                    }
                }
                if (pluginRequete != null) {
                    res = pluginRequete.traiterRequete(ctx, requete, res);
                }
                final long finRequete = System.currentTimeMillis();
                LOG.debug("Durée requête : " + (finRequete - debutRequete) + " ms.");
            }
            if (res.size() > 0) {
                res.get(0).setTotal(res.size());
            }
        }
        // Optimisation des requetes en front : réinitialisation du contexte
        ctx.getDatas().remove("optimizedSelect");
        ctx.getDatas().remove("optimizedLimit");
        ctx.getDatas().remove("optimizedObject");
        if (ctx instanceof ContexteUniv) {
            ((ContexteUniv) ctx).setGroupePersonnalisationCourant("");
            ((ContexteUniv) ctx).setEspacePersonnalisationCourant("");
        }
        return res;
    }

    /**
     * Initialiser requête.
     *
     * @param ctx the ctx
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static void initialiserRequete(final OMContext ctx) {
        // Optimisation des requetes en front : ajout d'une variable dans le contexte
        ctx.getDatas().put("optimizedSelect", "1");
    }

    /**
     * Finaliser requete.
     *
     * @param ctx the ctx
     * @param res the res
     * @return the vector
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static List<ResultatRecherche> finaliserRequete(final OMContext ctx, final List<ResultatRecherche> res) {
        if (res.size() > 0) {
            res.get(0).setTotal(res.size());
        }
        // Optimisation des requetes en front : ajout d'une variable dans le contexte
        ctx.getDatas().remove("optimizedSelect");
        ctx.getDatas().remove("optimizedLimit");
        ctx.getDatas().remove("optimizedObject");
        if (ctx instanceof ContexteUniv) {
            ((ContexteUniv) ctx).setGroupePersonnalisationCourant("");
            ((ContexteUniv) ctx).setEspacePersonnalisationCourant("");
        }
        return res;
    }

    /**
     * Renvoie la liste des paramètres renseignés passés à la requête.
     *
     * @param requete the requete
     * @return the string
     * @deprecated Méthode plus utilisée
     */
    @Deprecated
    public static String renvoyerListeParametresRenseignesRequete(final String requete) {
        String res = "";
        final StringTokenizer st = new StringTokenizer(requete.toUpperCase(), "&");
        while (st.hasMoreTokens()) {
            final String val = st.nextToken();
            final String partieGauche = val.substring(0, val.indexOf("="));
            String partieDroite = val.substring(val.indexOf("=") + 1);
            partieDroite = StringUtils.replace(partieDroite, "%20", " ");
            partieDroite = StringUtils.replace(partieDroite, "'", "");
            if (!"OBJET".equals(partieGauche)) {
                if (partieDroite.length() > 0) {
                    if (partieDroite.compareTo("A") > 0) {
                        if (res.length() > 0) {
                            res += ", ";
                        }
                        res += partieDroite;
                    }
                }
            }
        }
        return res;
    }
}
