/*
 * Created on 10 sept. 2005
 *
 *Pseudo-reponse http pour faire des appels de service
 */
package com.univ.utils;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
// TODO: Auto-generated Javadoc

/**
 * The Class ServiceWrappedResponse.
 *
 * @author jeanseb
 */
public class ServiceWrappedResponse extends HttpServletResponseWrapper {

    /** The writer. */
    private PrintWriter writer;

    /**
     * Instantiates a new service wrapped response.
     *
     * @param response
     *            the response
     * @param _writer
     *            the _writer
     */
    public ServiceWrappedResponse(HttpServletResponse response, PrintWriter _writer) {
        super(response);
        writer = _writer;
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletResponseWrapper#getWriter()
     */
    @Override
    public PrintWriter getWriter() throws IOException {
        return writer;
    }

    /* (non-Javadoc)
     * @see javax.servlet.ServletResponseWrapper#getContentType()
     */
    @Override
    public java.lang.String getContentType() {
        return "text/html";
    }
}
