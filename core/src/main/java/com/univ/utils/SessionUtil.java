package com.univ.utils;

import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.jsbsoft.jtf.session.SessionUtilisateur;
// TODO: Auto-generated Javadoc

/**
 * Insert the type's description here. Creation date: (10/11/2001 12:10:26)
 *
 * @author:
 */
public class SessionUtil {

    /**
     * Récupération des informations de session.
     *
     * @param _request
     *            the _request
     *
     * @return the infos session
     */
    public static Map<String, Object> getInfosSession(final HttpServletRequest _request) {
        final HttpSession _session = _request.getSession(false);
        return getInfosSession(_session);
    }

    /**
     * Gets the infos session.
     *
     * @param _session
     *            the _session
     *
     * @return the infos session
     */
    public static Map<String, Object> getInfosSession(final HttpSession _session) {
        final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) _session.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
        Map<String, Object> infos;
        if (sessionUtilisateur != null) {
            infos = sessionUtilisateur.getInfos();
        } else {
            infos = new HashMap<>();
        }
        return infos;
    }

    /**
     * Détermine si l'utilisateur est connecté.
     *
     * @param sessionUtilisateur
     *            the session utilisateur
     *
     * @return true, if est connecte
     */
    public static boolean estConnecte(final SessionUtilisateur sessionUtilisateur) {
        boolean res = false;
        if (sessionUtilisateur != null) {
            final Object o = sessionUtilisateur.getInfos().get(SessionUtilisateur.AUTORISATIONS);
            if (o != null) {
                res = true;
            }
        }
        return res;
    }
}
