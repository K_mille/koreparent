package com.univ.utils;

public enum TableauBordAttribute {

    LIBELLE_FICHE(IRequeteurConstantes.ATTRIBUT_LIBELLE_FICHE),
    CODE_OBJET(IRequeteurConstantes.ATTRIBUT_CODE_OBJET),
    DATE_FIN(IRequeteurConstantes.ATTRIBUT_DATE_FIN),
    CODE_REDACTEUR(IRequeteurConstantes.ATTRIBUT_CODE_REDACTEUR),
    TYPE_DATE(IRequeteurConstantes.ATTRIBUT_TYPE_DATE),
    LANGUE_FICHE(IRequeteurConstantes.ATTRIBUT_LANGUE),
    DIFFUSION_PUBLIC_VISE(IRequeteurConstantes.ATTRIBUT_DIFFUSION_PUBLIC_VISE),
    CODE_RUBRIQUE(IRequeteurConstantes.ATTRIBUT_CODE_RUBRIQUE),
    DATE_DEBUT(IRequeteurConstantes.ATTRIBUT_DATE_DEBUT),
    CODE_RATTACHEMENT(IRequeteurConstantes.ATTRIBUT_CODE_RATTACHEMENT),
    ETAT_OBJET(IRequeteurConstantes.ATTRIBUT_ETAT_OBJET);

    private String value;

    TableauBordAttribute(String value){
        this.value = value;
    }

    public String getRequeteParam(){
        return this.value + "_REQ";
    }

    public String getTriParam(){
        return this.value + "_TRI";
    }

    public String getValue() {
        return value;
    }
}
