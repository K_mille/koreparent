package com.univ.utils.cache;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.univ.utils.ContexteUtil;

public class CacheRequestManager {

    public static final String ID_BEAN = "cacheRequestManager";

    private ICacheRequest getCacheRequest(String idBean) {
        return (ICacheRequest) ApplicationContextManager.getCoreContextBean(idBean);
    }

    // TODO : renvoyer plutôt un List<ResultatRecherche>
    public Object call(String idBean, Object... objects) throws Exception {
        ICacheRequest cacheRequest = getCacheRequest(idBean);
        String key = cacheRequest.getKey(objects);
        // pas de cache en aperçu
        if (ContexteUtil.getContexteUniv().isApercu()) {
            return cacheRequest.perform(objects);
        } else {
            return cacheRequest.call(ContexteUtil.getContexteUniv().getKsession(), key, objects);
        }
    }
}
