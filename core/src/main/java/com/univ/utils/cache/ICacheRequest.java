package com.univ.utils.cache;

public interface ICacheRequest {

    Object call(String kSession, String key, Object... objects) throws Exception;

    Object perform(Object... objects) throws Exception;

    void flush() throws Exception;

    String getKey(Object... objects);
}
