package com.univ.utils.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.univ.utils.ContexteUtil;

/**
 *
 * Ce filtre sert uniquement à initialiser un Contexte pouvant être récupérer facilement dans le thread courant. Il se charge de l'initialisation et du release du contexte.
 *
 * @author olivier.camon
 *
 */
public class ContexteFilter implements Filter {

    private FilterConfig config;

    @Override
    public void destroy() {}

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        ContexteUtil.setContexteUniv((HttpServletRequest) request, (HttpServletResponse) response, config.getServletContext());
        try {
            chain.doFilter(request, response);
        } finally {
            ContexteUtil.releaseContexteUniv();
        }
    }

    @Override
    public void init(final FilterConfig config) throws ServletException {
        this.config = config;
    }
}
