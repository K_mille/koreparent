package com.univ.utils.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.jsbsoft.jtf.session.SessionUtilisateur;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;

/**
 * Filtre permettant de garder la requête apres un forward
 */
public class ForwardContexteFilter implements Filter {

    @Override
    public void destroy() {
        //nothing to unload
    }

    /**
     * Dans le contexte on stocke 40K truc, or sur les forward, il faut mettre à jour des paramètres qui changent comme la requête http, le code de rubrique etc etc.
     */
    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain) throws IOException, ServletException {
        final ContexteUniv ctx = ContexteUtil.getContexteUniv();
        final HttpServletRequest requeteHttp = (HttpServletRequest) request;
        if (ctx != null && !requeteHttp.equals(ctx.getRequeteHTTP())) {
            ctx.setRequeteHTTP(requeteHttp);
            gestionParametreRH(ctx);
            rechargeInfosSessionsUtilisateurs(ctx);
        }
        chain.doFilter(request, response);
    }

    /**
     * Si l'utilisateur est connecté mais qu'on a pas encore ses autorisations (après une connexion par exemple) on recharge ses infos dans le contexte. Si il est connecté, on
     * récupère les infos sur l'espace collab
     *
     * @param ctx
     */
    private void rechargeInfosSessionsUtilisateurs(final ContexteUniv ctx) {
        final HttpServletRequest requeteHttp = ctx.getRequeteHTTP();
        final HttpSession sessionHTTP = requeteHttp.getSession(Boolean.FALSE);
        if (sessionHTTP != null) {
            final SessionUtilisateur sessionUtilisateur = (SessionUtilisateur) sessionHTTP.getAttribute(SessionUtilisateur.CLE_SESSION_UTILISATEUR_DANS_SESSION_HTTP);
            if (sessionUtilisateur != null) {
                if (ctx.getAutorisation() == null) {
                    ctx.initialiserInfosUtilisateur();
                } else {
                    ctx.initialiserEspace();
                }
            }
        }
    }

    /**
     * On récupère le parametre RH depuis la requete ou depuis l'infobean si il existe.
     *
     * @param ctx
     */
    private static void gestionParametreRH(final ContexteUniv ctx) {
        final HttpServletRequest requeteHttp = ctx.getRequeteHTTP();
        String codeRH = requeteHttp.getParameter("RH");
        if (StringUtils.isBlank(codeRH)) {
            final InfoBean infoBean = (InfoBean) requeteHttp.getAttribute("infoBean");
            if (infoBean != null) {
                codeRH = infoBean.getString("RH");
            }
        }
        if (StringUtils.isNotBlank(codeRH)) {
            ctx.setCodeRubriqueHistorique(codeRH);
        }
    }

    @Override
    public void init(final FilterConfig arg0) throws ServletException {
        //nothing
    }
}
