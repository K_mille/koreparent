package com.univ.utils.json;

import org.apache.commons.lang3.StringUtils;

public class NamingStrategyHelper {

    public static String translateUppercaseToUnderscore(String defaultName) {
        char[] nameChars = StringUtils.uncapitalize(defaultName).toCharArray();
        StringBuilder nameTranslated = new StringBuilder(nameChars.length * 2);
        for (char c : nameChars) {
            if (Character.isUpperCase(c)) {
                nameTranslated.append("_");
                c = Character.toLowerCase(c);
            }
            nameTranslated.append(c);
        }
        return nameTranslated.toString();
    }

    public static String translateUnderscoreToUppercase(String defaultName) {
        final char[] nameChars = defaultName.toCharArray();
        final StringBuilder nameTranslated = new StringBuilder(nameChars.length * 2);
        boolean wasUnderscore = false;
        for (char c : nameChars) {
            if (c == '_') {
                wasUnderscore = true;
            } else if (wasUnderscore) {
                nameTranslated.append(Character.toUpperCase(c));
                wasUnderscore = false;
            } else {
                nameTranslated.append(c);
            }
        }
        return nameTranslated.toString();
    }
}
