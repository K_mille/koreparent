package com.univ.utils.media;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe servant à aggreger / désagreger un code de type de ressource et un code de type média d'un media pour les afficher ensemble dans l'ihm
 *
 * @author aga
 *
 */
public class CodeTypeRessourceCodeTypeMedia {

    /**
     * Séparateur utilisé pour représenter cet agrégat sous forme de string
     */
    public static final String SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA = "|";

    /**
     * Ancien séparateur posant des problèmes d'encodage car si codeTypeMedia commence par 000 alors \000 est un début d'entité
     */
    public static final String OLD_SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA = "\\";

    /**
     * Le code du type ressource du media (photo, video, audio ...)
     */
    private String codeTypeRessource;

    /**
     * Le code du type media du media (issu des libelles personnalisables de chaque typeRessource)
     */
    private String codeTypeMedia;

    /**
     * Constructeur
     */
    public CodeTypeRessourceCodeTypeMedia() {
        super();
    }

    /**
     * Constructeur
     *
     * @param codeTypeRessource
     *            Le code du type ressource du media (photo, video, audio ...)
     * @param codeTypeMedia
     *            Le code du type media du media (issu des libelles personnalisables de chaque typeRessource)
     */
    public CodeTypeRessourceCodeTypeMedia(String codeTypeRessource, String codeTypeMedia) {
        super();
        this.codeTypeRessource = codeTypeRessource;
        this.codeTypeMedia = codeTypeMedia;
    }

    /**
     * Construit un objet de type CodeTypeRessourceCodeTypeMedia à partir d'une chaine de la forme codeTypeRessource +SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA+codeTypeMedia
     *
     * @param codeTypeRessourceCodeTypeMedia
     *            chaine de la forme codeTypeRessource +SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA+codeTypeMedia
     * @return un objet CodeTypeRessourceCodeTypeMedia
     */
    public static CodeTypeRessourceCodeTypeMedia fromString(String codeTypeRessourceCodeTypeMedia) {
        if (StringUtils.isBlank(codeTypeRessourceCodeTypeMedia)) {
            throw new IllegalArgumentException("codeTypeRessourceCodeTypeMedia cannot be blank.");
        }
        CodeTypeRessourceCodeTypeMedia result = new CodeTypeRessourceCodeTypeMedia();
        String[] codetypeRessourceCodeTypeMediaArray;
        if (codeTypeRessourceCodeTypeMedia.contains(SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA)) {
            codetypeRessourceCodeTypeMediaArray = StringUtils.splitPreserveAllTokens(codeTypeRessourceCodeTypeMedia, SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA);
        } else {
            codetypeRessourceCodeTypeMediaArray = StringUtils.splitPreserveAllTokens(codeTypeRessourceCodeTypeMedia, OLD_SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA);
        }
        result.setCodeTypeRessource(codetypeRessourceCodeTypeMediaArray[0]);
        if (codetypeRessourceCodeTypeMediaArray.length > 1) {
            result.setCodeTypeMedia(codetypeRessourceCodeTypeMediaArray[1]);
        }
        return result;
    }

    /**
     * @return le code du type ressource du media (photo, video, audio ...)
     */
    public String getCodeTypeRessource() {
        return codeTypeRessource;
    }

    /**
     * @param codeTypeRessource
     *            the codeTypeRessource to set
     */
    public void setCodeTypeRessource(String codeTypeRessource) {
        this.codeTypeRessource = codeTypeRessource;
    }

    /**
     * @return le code du type media du media (issu des libelles personnalisables de chaque typeRessource)
     */
    public String getCodeTypeMedia() {
        return codeTypeMedia;
    }

    /**
     * @param codeTypeMedia
     *            the codeTypeMedia to set
     */
    public void setCodeTypeMedia(String codeTypeMedia) {
        this.codeTypeMedia = codeTypeMedia;
    }

    /**
     * Renvoie une représentation string de cet objet sous la forme codeTypeRessource +SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA+codeTypeMedia (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     **/
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (StringUtils.isNotBlank(getCodeTypeRessource())) {
            buf.append(getCodeTypeRessource());
        }
        if (StringUtils.isNotBlank(getCodeTypeMedia())) {
            buf.append(SEPARATEUR_CODE_TYPE_RESSOURCE_CODE_TYPE_MEDIA);
            buf.append(getCodeTypeMedia());
        }
        return buf.toString();
    }
}
