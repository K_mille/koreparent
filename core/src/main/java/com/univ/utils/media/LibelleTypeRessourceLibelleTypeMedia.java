package com.univ.utils.media;

import org.apache.commons.lang3.StringUtils;

/**
 * Classe servant à aggreger / désagreger un libellé de type de ressource et un libellé de type média d'un media pour les afficher ensemble dans l'ihm
 *
 * @author aga
 *
 */
public class LibelleTypeRessourceLibelleTypeMedia {

    /**
     * Séparateur utilisé pour représenter cet agrégat sous forme de string
     */
    public static final String SEPARATEUR_LIBELLE_TYPE_RESSOURCE_LIBELLE_TYPE_MEDIA = "/";

    /**
     * Libelle indiquant qu'une instance représente tous les types de media pour un type de ressource donné
     */
    public static final String LIBELLE_TOUS_TYPES_MEDIA = "Tous";

    /**
     * Le libelle du type ressource du media (photo, video, audio ...)
     */
    private String libelleTypeRessource;

    /**
     * Le libelle du type media du media (issu des libelles personnalisables de chaque typeRessource)
     */
    private String libelleTypeMedia;

    /**
     * Constructeur
     */
    public LibelleTypeRessourceLibelleTypeMedia() {
        super();
    }

    /**
     * Constructeur
     *
     * @param libelleTypeRessource
     *            Le libelle du type ressource du media (photo, video, audio ...)
     * @param libelleTypeMedia
     *            Le libelle du type media du media (issu des libelles personnalisables de chaque typeRessource)
     */
    public LibelleTypeRessourceLibelleTypeMedia(String libelleTypeRessource, String libelleTypeMedia) {
        super();
        this.libelleTypeRessource = libelleTypeRessource;
        this.libelleTypeMedia = libelleTypeMedia;
    }

    /**
     * Construit un objet de type LibelleTypeRessourceLibelleTypeMedia à partir d'une chaine de la forme libelleTypeRessource
     * +SEPARATEUR_LIBELLE_TYPE_RESSOURCE_LIBELLE_TYPE_MEDIA+libelleTypeMedia
     *
     * @param libelleTypeRessourceLibelleTypeMedia
     *            chaine de la forme libelleTypeRessource +SEPARATEUR_LIBELLE_TYPE_RESSOURCE_LIBELLE_TYPE_MEDIA +libelleTypeMedia
     * @return un objet LibelleTypeRessourceLibelleTypeMedia
     */
    public static LibelleTypeRessourceLibelleTypeMedia fromString(String libelleTypeRessourceLibelleTypeMedia) {
        if (StringUtils.isBlank(libelleTypeRessourceLibelleTypeMedia)) {
            throw new IllegalArgumentException("libelleTypeRessourceLibelleTypeMedia cannot be blank.");
        }
        LibelleTypeRessourceLibelleTypeMedia result = new LibelleTypeRessourceLibelleTypeMedia();
        String[] libelleTypeRessourceLibelleTypeMediaArray = StringUtils.splitPreserveAllTokens(libelleTypeRessourceLibelleTypeMedia, SEPARATEUR_LIBELLE_TYPE_RESSOURCE_LIBELLE_TYPE_MEDIA);
        result.setLibelleTypeRessource(libelleTypeRessourceLibelleTypeMediaArray[0]);
        result.setLibelleTypeMedia(libelleTypeRessourceLibelleTypeMediaArray[1]);
        return result;
    }

    /**
     * @return le libelle du type ressource du media (photo, video, audio ...)
     */
    public String getLibelleTypeRessource() {
        return libelleTypeRessource;
    }

    /**
     * @param libelleTypeRessource
     *            the libelleTypeRessource to set
     */
    public void setLibelleTypeRessource(String libelleTypeRessource) {
        this.libelleTypeRessource = libelleTypeRessource;
    }

    /**
     * @return le libelle du type media du media (issu des libelles personnalisables de chaque typeRessource)
     */
    public String getLibelleTypeMedia() {
        return StringUtils.defaultIfEmpty(libelleTypeMedia, LIBELLE_TOUS_TYPES_MEDIA);
    }

    /**
     * @param libelleTypeMedia
     *            the libelleTypeMedia to set
     */
    public void setLibelleTypeMedia(String libelleTypeMedia) {
        this.libelleTypeMedia = libelleTypeMedia;
    }

    /**
     * Renvoie une représentation string de cet objet sous la forme libelleTypeRessource +SEPARATEUR_LIBELLE_TYPE_RESSOURCE_LIBELLE_TYPE_MEDIA+libelleTypeMedia (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     **/
    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (StringUtils.isNotBlank(getLibelleTypeRessource())) {
            buf.append(getLibelleTypeRessource());
        }
        buf.append(SEPARATEUR_LIBELLE_TYPE_RESSOURCE_LIBELLE_TYPE_MEDIA);
        if (StringUtils.isNotBlank(getLibelleTypeMedia())) {
            buf.append(getLibelleTypeMedia());
        }
        return buf.toString();
    }
}
