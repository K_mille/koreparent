package com.univ.utils.recherche;

import java.util.Date;

public class RequeteMultifiche {

    public String sLibelle = "";

    public String sCodeObjet = "";

    public String sCodeFiche = "";

    public String sCodeRubrique = "";

    public String sCodeRattachement = "";

    public String sCodeRedacteur = "";

    public String sIdMeta = "";

    public String urlFiche = "";

    public Date dateDebutCreation = new Date(0);

    public Date dateFinCreation = new Date(0);

    public Date dateDebutModification = new Date(0);

    public Date dateFinModification = new Date(0);

    public Date dateDebutMiseEnLigne = new Date(0);

    public Date dateFinMiseEnLigne = new Date(0);

    public RequeteMultifiche(String sLibelle, String sCodeObjet, String sCodeFiche, String sCodeRubrique, String sCodeRattachement, String sCodeRedacteur, String sIdMeta, String urlFiche, Date dateDebutCreation, Date dateFinCreation, Date dateDebutModification, Date dateFinModification, Date dateDebutMiseEnLigne, Date dateFinMiseEnLigne) {
        super();
        this.sLibelle = sLibelle;
        this.sCodeObjet = sCodeObjet;
        this.sCodeFiche = sCodeFiche;
        this.sCodeRubrique = sCodeRubrique;
        this.sCodeRattachement = sCodeRattachement;
        this.sCodeRedacteur = sCodeRedacteur;
        this.sIdMeta = sIdMeta;
        this.urlFiche = urlFiche;
        this.dateDebutCreation = dateDebutCreation;
        this.dateFinCreation = dateFinCreation;
        this.dateDebutModification = dateDebutModification;
        this.dateFinModification = dateFinModification;
        this.dateDebutMiseEnLigne = dateDebutMiseEnLigne;
        this.dateFinMiseEnLigne = dateFinMiseEnLigne;
    }

    public String getsLibelle() {
        return sLibelle;
    }

    public void setsLibelle(String sLibelle) {
        this.sLibelle = sLibelle;
    }

    public String getsCodeObjet() {
        return sCodeObjet;
    }

    public void setsCodeObjet(String sCodeObjet) {
        this.sCodeObjet = sCodeObjet;
    }

    public String getsCodeFiche() {
        return sCodeFiche;
    }

    public void setsCodeFiche(String sCodeFiche) {
        this.sCodeFiche = sCodeFiche;
    }

    public String getsCodeRubrique() {
        return sCodeRubrique;
    }

    public void setsCodeRubrique(String sCodeRubrique) {
        this.sCodeRubrique = sCodeRubrique;
    }

    public String getsCodeRattachement() {
        return sCodeRattachement;
    }

    public void setsCodeRattachement(String sCodeRattachement) {
        this.sCodeRattachement = sCodeRattachement;
    }

    public String getsCodeRedacteur() {
        return sCodeRedacteur;
    }

    public void setsCodeRedacteur(String sCodeRedacteur) {
        this.sCodeRedacteur = sCodeRedacteur;
    }

    public String getsIdMeta() {
        return sIdMeta;
    }

    public void setsIdMeta(String sIdMeta) {
        this.sIdMeta = sIdMeta;
    }

    public String getUrlFiche() {
        return urlFiche;
    }

    public void setUrlFiche(String urlFiche) {
        this.urlFiche = urlFiche;
    }

    public Date getDateDebutCreation() {
        return dateDebutCreation;
    }

    public void setDateDebutCreation(Date dateDebutCreation) {
        this.dateDebutCreation = dateDebutCreation;
    }

    public Date getDateFinCreation() {
        return dateFinCreation;
    }

    public void setDateFinCreation(Date dateFinCreation) {
        this.dateFinCreation = dateFinCreation;
    }

    public Date getDateDebutModification() {
        return dateDebutModification;
    }

    public void setDateDebutModification(Date dateDebutModification) {
        this.dateDebutModification = dateDebutModification;
    }

    public Date getDateFinModification() {
        return dateFinModification;
    }

    public void setDateFinModification(Date dateFinModification) {
        this.dateFinModification = dateFinModification;
    }

    public Date getDateDebutMiseEnLigne() {
        return dateDebutMiseEnLigne;
    }

    public void setDateDebutMiseEnLigne(Date dateDebutMiseEnLigne) {
        this.dateDebutMiseEnLigne = dateDebutMiseEnLigne;
    }

    public Date getDateFinMiseEnLigne() {
        return dateFinMiseEnLigne;
    }

    public void setDateFinMiseEnLigne(Date dateFinMiseEnLigne) {
        this.dateFinMiseEnLigne = dateFinMiseEnLigne;
    }
}
