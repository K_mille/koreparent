package com.univ.utils.recherche;

import java.util.ArrayList;
import java.util.Collection;

import com.univ.objetspartages.bean.MetatagBean;

/**
 * Encapsule les résultats d'une recherche multifiche
 *
 */
public class ResultatRechercheMultifiche {

    /**
     * Le code objet partagé par tous les résultats
     */
    private String codeObjet;

    /**
     * La liste de Metatag décrivant les fiches faisant parti des résultats
     */
    private Collection<MetatagBean> resultats = new ArrayList<>();

    /**
     * @return the codeObjet
     */
    public String getCodeObjet() {
        return codeObjet;
    }

    /**
     * @param codeObjet
     *            the codeObjet to set
     */
    public void setCodeObjet(String codeObjet) {
        this.codeObjet = codeObjet;
    }

    /**
     * @return the resultats
     */
    public Collection<MetatagBean> getResultats() {
        return resultats;
    }

    /**
     * @param resultats
     *            the resultats to set
     */
    public void setResultats(Collection<MetatagBean> resultats) {
        this.resultats = resultats;
    }
}
