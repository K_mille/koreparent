package com.univ.utils.sql;

/**
 * Enumeration contenant les opérations possible sur une condition.
 *
 * @author olivier.camon
 *
 */
public enum Operateur {
    EQUALS(" = "),
    NOT_EQUALS(" != "),
    GREATER_THAN(" > "),
    LESS_THAN(" < "),
    GREATER_EQUALS(" >= "),
    LESS_EQUALS(" <= "),
    NOT_GREATER_THAN(" !> "),
    NOT_LESS_THAN(" !< "),
    NOT(" NOT "),
    IS_NOT(" IS NOT "),
    ALL(" ALL "),
    ANY(" ANY "),
    IS(" IS "),
    IN(" IN "),
    NOT_IN(" NOT IN "),
    ON(" ON "),
    BETWEEN(" BETWEEN "),
    UNIQUE(" UNIQUE "),
    UNION(" UNION "),
    LIKE(" LIKE "),
    NOT_LIKE(" NOT LIKE "),
    EXISTS(" EXIST "),
    REGEXP(" REGEXP ");

    private String operateur;

    private Operateur(final String valeur) {
        operateur = valeur;
    }

    public String getOperateur() {
        return operateur;
    }
}
