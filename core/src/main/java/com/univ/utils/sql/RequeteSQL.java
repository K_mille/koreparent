package com.univ.utils.sql;

import java.util.ArrayList;
import java.util.Collection;

import com.univ.utils.sql.clause.ClauseGroupBy;
import com.univ.utils.sql.clause.ClauseHaving;
import com.univ.utils.sql.clause.ClauseJoin;
import com.univ.utils.sql.clause.ClauseLimit;
import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseSQL;
import com.univ.utils.sql.clause.ClauseWhere;

import static org.apache.commons.lang3.StringUtils.isEmpty;

/**
 * Permet de modéliser toute les clauses d'une requête SQL excepter la clause SELECT.
 *
 * @author olivier.camon
 *
 */
public class RequeteSQL implements Cloneable {

    private ClauseWhere where;

    private Collection<ClauseJoin> jointures;

    private Collection<ClauseOrderBy> ordersBy;

    private ClauseGroupBy groupBy;

    private ClauseHaving having;

    private ClauseLimit limit;

    public RequeteSQL() {
        jointures = new ArrayList<>();
        ordersBy = new ArrayList<>();
    }

    public ClauseWhere getWhere() {
        return where;
    }

    public Collection<ClauseJoin> getJointures() {
        return jointures;
    }

    public Collection<ClauseOrderBy> getOrdersBy() {
        return ordersBy;
    }

    public void setJointures(final Collection<ClauseJoin> jointures) {
        this.jointures = jointures;
    }

    public void setOrdersBy(final Collection<ClauseOrderBy> ordersBy) {
        this.ordersBy = ordersBy;
    }

    public ClauseGroupBy getGroupBy() {
        return groupBy;
    }

    public ClauseLimit getLimit() {
        return limit;
    }

    public RequeteSQL where(final ClauseWhere where) {
        this.where = where;
        return this;
    }

    public RequeteSQL having(final ClauseHaving having) {
        this.having = having;
        return this;
    }

    public RequeteSQL orderBy(final ClauseOrderBy orderBy) {
        this.ordersBy.add(orderBy);
        return this;
    }

    public RequeteSQL groupBy(final ClauseGroupBy groupBy) {
        this.groupBy = groupBy;
        return this;
    }

    public RequeteSQL join(final ClauseJoin join) {
        if (join == null || isEmpty(join.getNomTable())) {
            return this;
        }
        final String tableAAJouter = join.getNomTable();
        for (final ClauseJoin joinDejaPresent : this.jointures) {
            if (tableAAJouter.equals(joinDejaPresent.getNomTable())) {
                return this;
            }
        }
        this.jointures.add(join);
        return this;
    }

    public RequeteSQL limit(final ClauseLimit limit) {
        this.limit = limit;
        return this;
    }

    /**
     * formatte la requete pour retourner une requête SQL. Le select n'est pas compris dans la requête. La requête est construite à partir des jointures.
     *
     * @return la requête SQL formater.
     */
    public String formaterRequete() {
        final StringBuilder requete = new StringBuilder();
        if (!jointures.isEmpty()) {
            for (final ClauseSQL join : jointures) {
                if (join != null) {
                    requete.append(join.formaterSQL());
                }
            }
        }
        if (where != null) {
            requete.append(where.formaterSQL());
        }
        if (groupBy != null) {
            requete.append(groupBy.formaterSQL());
        }
        if (having != null) {
            requete.append(having.formaterSQL());
        }
        if (!ordersBy.isEmpty()) {
            for (final ClauseSQL orderBy : ordersBy) {
                if (orderBy != null) {
                    requete.append(orderBy.formaterSQL());
                }
            }
        }
        if (limit != null) {
            requete.append(limit.formaterSQL());
        }
        return requete.toString();
    }

    /*
     * methode clone
     * @return la requête SQL clonee.
     */
    @Override
    public RequeteSQL clone() throws CloneNotSupportedException {
        return (RequeteSQL) super.clone();
    }
}
