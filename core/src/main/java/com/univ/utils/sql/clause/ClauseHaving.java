package com.univ.utils.sql.clause;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.OperateurConditionnel;
import com.univ.utils.sql.condition.Condition;

/**
 * Représente une clause HAVING d'une requête SQL. une clause Having contient un ensemble de condition et d'opérateur (OR ou AND) Exemple : HAVING FOO = 'BAR'
 *
 * @author olivier.camon
 *
 */
public class ClauseHaving implements ClauseSQL {

    private Condition premierCritere;

    private List<Condition> critereRequete;

    private List<OperateurConditionnel> operateurDeCondition;

    /**
     * Constructeur par défaut
     */
    public ClauseHaving() {
        critereRequete = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
    }

    /**
     * Instancie un objet ClauseHaving et initialise la première condition
     *
     * @param premiereCondition
     */
    public ClauseHaving(Condition premiereCondition) {
        critereRequete = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
        premierCritere = premiereCondition;
    }

    /**
     * initialise la première condition. La première condition est à traité différement car elle n'a pas besoin de condition OR ou AND pour être ajouter
     *
     * @param condition
     *            La première condition de la clause having
     */
    public void setPremiereCondition(Condition condition) {
        premierCritere = condition;
    }

    /**
     * Rajoute une condition OR à la clause having courante
     *
     * @param condition
     *            La condition à ajouter
     * @return la référence à clause having courante
     */
    public ClauseHaving or(Condition condition) {
        if (premierCritere == null || premierCritere.isEmpty()) {
            premierCritere = condition;
        } else {
            operateurDeCondition.add(OperateurConditionnel.OR);
            critereRequete.add(condition);
        }
        return this;
    }

    /**
     * Rajoute une condition AND à la clause having courante
     *
     * @param condition
     *            La condition à ajouter
     * @return la référence à la clause having courante
     */
    public ClauseHaving and(Condition condition) {
        if (premierCritere == null || premierCritere.isEmpty()) {
            premierCritere = condition;
        } else {
            operateurDeCondition.add(OperateurConditionnel.AND);
            critereRequete.add(condition);
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.clause.ClauseSQL#formaterSQL()
     */
    @Override
    public String formaterSQL() {
        StringBuilder requeteHaving = new StringBuilder();
        if (premierCritere == null) {
            return requeteHaving.toString();
        }
        requeteHaving.append(premierCritere.formaterCondition());
        for (int i = 0; i < operateurDeCondition.size(); ++i) {
            String sousCondition = critereRequete.get(i).formaterCondition();
            if (StringUtils.isNotEmpty(sousCondition)) {
                requeteHaving.append(operateurDeCondition.get(i).getOperateur()).append(sousCondition);
            }
        }
        requeteHaving = ajouterClauseHavingSiNonVide(requeteHaving);
        return requeteHaving.toString();
    }

    private StringBuilder ajouterClauseHavingSiNonVide(StringBuilder requeteSQLFormater) {
        if (StringUtils.isNotEmpty(requeteSQLFormater.toString())) {
            requeteSQLFormater.insert(0, ConstanteSQL.HAVING);
        }
        return requeteSQLFormater;
    }
}
