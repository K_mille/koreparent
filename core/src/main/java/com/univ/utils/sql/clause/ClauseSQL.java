package com.univ.utils.sql.clause;

/**
 * Interface regroupant l'ensemble des objet représentant des clauses SQL. Ces clauses peuvent être des clauses Join, Where, Group By, Order by et Limit
 *
 * @author olivier.camon
 *
 */
public interface ClauseSQL {

    /**
     * Formate la ClauseSQL pour fournir une chaine de caractère correspondant à la requête SQL des données qu'elle posséde.
     *
     * @return
     */
    String formaterSQL();
}
