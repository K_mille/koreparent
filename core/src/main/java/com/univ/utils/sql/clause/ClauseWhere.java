package com.univ.utils.sql.clause;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.OperateurConditionnel;
import com.univ.utils.sql.condition.Condition;

/**
 * Représente une clause WHERE d'une requête SQL. Une clause where est représenté par un ensemble de Condition et d'opérateur. exemple : WHERE FOO = 'BAR' OR X = 'Y'
 *
 * @author olivier.camon
 *
 */
public class ClauseWhere implements ClauseSQL {

    private Condition premierCritere;

    private List<Condition> critereRequete;

    private List<OperateurConditionnel> operateurDeCondition;

    /**
     * Constructeur par défaut
     */
    public ClauseWhere() {
        critereRequete = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
    }

    /**
     * Instancie un objet ClauseWhere et initialise la première condition
     *
     * @param premiereCondition
     */
    public ClauseWhere(Condition premiereCondition) {
        critereRequete = new ArrayList<>();
        operateurDeCondition = new ArrayList<>();
        premierCritere = premiereCondition;
    }

    /**
     * initialise la première condition. La première condition est à traité différement car elle n'a pas besoin de condition OR ou AND pour être ajouter
     *
     * @param condition
     *            La première condition de la clause where
     */
    public void setPremiereCondition(Condition condition) {
        premierCritere = condition;
    }

    /**
     * Rajoute une condition OR à la clause where courante
     *
     * @param condition
     *            La condition à ajouter
     * @return la référence à clause where courante
     */
    public ClauseWhere or(Condition condition) {
        if (premierCritere == null || premierCritere.isEmpty()) {
            premierCritere = condition;
        } else {
            operateurDeCondition.add(OperateurConditionnel.OR);
            critereRequete.add(condition);
        }
        return this;
    }

    /**
     * Rajoute une condition AND à la clause where courante
     *
     * @param condition
     *            La condition à ajouter
     * @return la référence à la clause where courante
     */
    public ClauseWhere and(Condition condition) {
        if (premierCritere == null || premierCritere.isEmpty()) {
            premierCritere = condition;
        } else {
            operateurDeCondition.add(OperateurConditionnel.AND);
            critereRequete.add(condition);
        }
        return this;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.clause.ClauseSQL#formaterSQL()
     */
    @Override
    public String formaterSQL() {
        StringBuilder requeteWhere = new StringBuilder();
        if (premierCritere == null) {
            return requeteWhere.toString();
        }
        requeteWhere.append(premierCritere.formaterCondition());
        for (int i = 0; i < operateurDeCondition.size(); ++i) {
            if (critereRequete.get(i) != null) {
                String sousCondition = critereRequete.get(i).formaterCondition();
                if (StringUtils.isNotEmpty(sousCondition)) {
                    requeteWhere.append(operateurDeCondition.get(i).getOperateur()).append(sousCondition);
                }
            }
        }
        requeteWhere = ajouterClauseWhereSiNonVide(requeteWhere);
        return requeteWhere.toString();
    }

    private StringBuilder ajouterClauseWhereSiNonVide(StringBuilder requeteSQLFormater) {
        if (StringUtils.isNotEmpty(requeteSQLFormater.toString())) {
            requeteSQLFormater.insert(0, ConstanteSQL.WHERE);
        }
        return requeteSQLFormater;
    }
}
