/** Package contenant Les outils pour construire des conditions en SQL.
 */
package com.univ.utils.sql.condition;

/**
 * Répresente une condition SQL. Elle peut être simple : X = Y Ou Multiple (A = B AND C = D )
 *
 * @author olivier.camon
 *
 */
public interface Condition {

    /**
     * Formatte les valeurs de la condition pour sortir une requête SQL.
     *
     * @return la condition formatter au language SQL
     */
    String formaterCondition();

    /**
     * Vérifie que la condition n'est pas vide ou nulle.
     *
     * @return vrai si la condition est vide ou nulle
     */
    boolean isEmpty();
}
