package com.univ.utils.sql.criterespecifique;

import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.LangueUtil;
import com.jsbsoft.jtf.database.OMContext;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.EscapeString;
import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.Operateur;
import com.univ.utils.sql.clause.ClauseWhere;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;
import com.univ.utils.sql.condition.ConditionSimple;
import com.univ.utils.sql.operande.Operande;
import com.univ.utils.sql.operande.OperandeFactory;
import com.univ.utils.sql.operande.TypeOperande;

/**
 * Classe utilitaire permettant de construire des conditions. Cela va des donctions X = Y ou des conditions plus complexe spécifique pour K-Portal
 *
 * @author olivier.camon
 *
 */
public final class ConditionHelper {

    /**
     * Valeur par défaut des codes multi valués dans la BDD K-Portal.
     */
    public static final String CODE_DEFAUT = "0000";

    /**
     * Retourne une condition COLONNE = VALEUR. Avec valeur étant un champ de type texte.
     *
     * @param nomColonne
     *            Le nom de la colonne de la bdd
     * @param valeur
     *            la valeur du champ
     * @return l'objet Condition respectif
     */
    public static Condition egalVarchar(final String nomColonne, final String valeur) {
        return egal(nomColonne, valeur, TypeOperande.VARCHAR);
    }

    /**
     * Retourne une condition COLONNE = VALEUR. La valeurs est échappé en fonction du {@link TypeOperande}.
     *
     * @param nomColonne
     *            Le nom de la colonne de la bdd
     * @param valeur
     *            la valeur du champ
     * @param typeValeur
     *            varchar, Number ...
     * @return l'objet Condition respectif
     */
    public static Condition egal(final String nomColonne, final Object valeur, final TypeOperande typeValeur) {
        return genericConditionSurColonne(nomColonne, valeur, typeValeur, Operateur.EQUALS);
    }

    /**
     * Condition de type : COLONNE != VALEUR La valeurs est échappé en fonction du {@link TypeOperande}.
     *
     * @param nomColonne
     * @param valeur
     * @param typeValeur
     * @return
     */
    public static Condition notEgal(final String nomColonne, final Object valeur, final TypeOperande typeValeur) {
        return genericConditionSurColonne(nomColonne, valeur, typeValeur, Operateur.NOT_EQUALS);
    }

    /**
     * Condition de type : COLONNE >= VALEUR La valeurs est échappé en fonction du {@link TypeOperande}.
     *
     * @param nomColonne
     * @param valeur
     * @param typeValeur
     * @return
     */
    public static Condition greaterEquals(final String nomColonne, final Object valeur, final TypeOperande typeValeur) {
        return genericConditionSurColonne(nomColonne, valeur, typeValeur, Operateur.GREATER_EQUALS);
    }

    /**
     * Condition de type : COLONNE <= VALEUR La valeurs est échappé en fonction du {@link TypeOperande}.
     *
     * @param nomColonne
     * @param valeur
     * @param typeValeur
     * @return
     */
    public static Condition lessEquals(final String nomColonne, final Object valeur, final TypeOperande typeValeur) {
        return genericConditionSurColonne(nomColonne, valeur, typeValeur, Operateur.LESS_EQUALS);
    }

    /**
     * Condition de type : COLONNE > VALEUR La valeurs est échappé en fonction du {@link TypeOperande}.
     *
     * @param nomColonne
     * @param valeur
     * @param typeValeur
     * @return
     */
    public static Condition greaterThan(final String nomColonne, final Object valeur, final TypeOperande typeValeur) {
        return genericConditionSurColonne(nomColonne, valeur, typeValeur, Operateur.GREATER_THAN);
    }

    /**
     * Condition de type : COLONNE < VALEUR La valeurs est échappé en fonction du {@link TypeOperande}.
     *
     * @param nomColonne
     * @param valeur
     * @param typeValeur
     * @return
     */
    public static Condition lessThan(final String nomColonne, final Object valeur, final TypeOperande typeValeur) {
        return genericConditionSurColonne(nomColonne, valeur, typeValeur, Operateur.LESS_THAN);
    }

    /**
     * Condition de type : COLONNE like tokenBefore VALEUR tokenAfter Valeur est échappé mais pas les tokens.
     *
     * @param nomColonne
     * @param valeur
     * @param tokenBefore
     * @param tokenAfter
     * @return
     */
    public static Condition like(final String nomColonne, final String valeur, final String tokenBefore, final String tokenAfter) {
        return genericConditionSurColonne(nomColonne, "'" + tokenBefore +  EscapeString.escapeSql(valeur) + tokenAfter + "'", TypeOperande.NON_ECHAPABLE, Operateur.LIKE);
    }

    /**
     * Condition de type : COLONNE like tokenBefore VALEUR tokenAfter Valeur est échappé mais pas les tokens.
     *
     * @param nomColonne
     * @param valeur
     * @param tokenBefore
     * @param tokenAfter
     * @return
     */
    public static Condition notlike(final String nomColonne, final String valeur, final String tokenBefore, final String tokenAfter) {
        return genericConditionSurColonne(nomColonne, "'" + tokenBefore +  EscapeString.escapeSql(valeur) + tokenAfter + "'", TypeOperande.NON_ECHAPABLE, Operateur.NOT_LIKE);
    }

    /**
     * Condition de type : COLONNE IS NULL.
     *
     * @param nomColonne
     * @return
     */
    public static Condition isNull(final String nomColonne) {
        return genericConditionSurColonne(nomColonne, ConstanteSQL.NULL, TypeOperande.NON_ECHAPABLE, Operateur.IS);
    }

    /**
     * Condition de type : COLONNE1 = COLONNE2.
     *
     * @param nomPremiereColonne
     * @param nomDeuxiemeColonne
     * @return
     */
    public static Condition critereJointureSimple(final String nomPremiereColonne, final String nomDeuxiemeColonne) {
        return genericConditionSurColonne(nomPremiereColonne, nomDeuxiemeColonne, TypeOperande.NOM_CHAMP, Operateur.EQUALS);
    }

    /**
     * Condition de type : COLONNE operateur valeur La valeur est échappé en fonction du type fourni en paramètre.
     *
     * @param nomColonne
     * @param valeur
     * @param typeValeur
     * @param operateur
     * @return
     */
    public static Condition genericConditionSurColonne(final String nomColonne, final Object valeur, final TypeOperande typeValeur, final Operateur operateur) {
        return genericCondition(nomColonne, TypeOperande.NOM_CHAMP, valeur, typeValeur, operateur);
    }

    /**
     * Condition générique permettant de construire tt type de condition : VALEURA operateur VALEURB ou VALEURA et VALEURB sont échappé en fonction de leur type.
     *
     * @param valeurA
     * @param typeValeurA
     * @param valeurB
     * @param typeValeurB
     * @param operateur
     * @return
     */
    public static Condition genericCondition(final Object valeurA, final TypeOperande typeValeurA, final Object valeurB, final TypeOperande typeValeurB, final Operateur operateur) {
        final Operande colonne = OperandeFactory.creerOperande(valeurA, typeValeurA);
        final Operande valeurColonne = OperandeFactory.creerOperande(valeurB, typeValeurB);
        return new ConditionSimple(colonne, operateur, valeurColonne);
    }

    /**
     * Condition de type : COLONNE IN ( ... ) La liste de valeur est de type varchar.
     * Cette méthode ne vérifie pas l'unicité des valeurs fourni en entrée. Utiliser {@link #in(String, Set)} plutôt.
     * @param nomColonne
     * @param valeurs
     * @return
     */
    public static Condition in(final String nomColonne, final Collection<String> valeurs) {
        return in(nomColonne, new HashSet<>(valeurs));
    }

    /**
     * Condition de type : COLONNE IN ( ... ) La liste de valeur est de type varchar.
     *
     * @param nomColonne
     * @param valeurs
     * @return
     */
    public static Condition in(final String nomColonne, final Set<String> valeurs) {
        if (valeurs != null && !valeurs.isEmpty()) {
            return genericConditionSurColonne(nomColonne, valeurs, TypeOperande.LISTE_VARCHAR, Operateur.IN);
        } else {
            return new ConditionSimple();
        }
    }

    /**
     * Condition de type : COLONNE IN ( ... ) La liste de valeur est de type varchar.
     * Cette méthode ne vérifie pas l'unicité des valeurs fourni en entrée. Utiliser {@link #notIn(String, Set)} plutôt.
     * @param nomColonne
     * @param valeurs
     * @return
     */
    public static Condition notIn(final String nomColonne, final Collection<String> valeurs) {
        return notIn(nomColonne, new HashSet<>(valeurs));
    }

    /**
     * Condition de type : COLONNE IN ( ... ) La liste de valeur est de type varchar.
     *
     * @param nomColonne
     * @param valeurs
     * @return
     */
    public static Condition notIn(final String nomColonne, final Set<String> valeurs) {
        if (valeurs != null && !valeurs.isEmpty()) {
            return genericConditionSurColonne(nomColonne, valeurs, TypeOperande.LISTE_VARCHAR, Operateur.NOT_IN);
        } else {
            return new ConditionSimple();
        }
    }

    /**
     * Construit une condition de même type que SQLUtil.formaterRechercheMots(String, String). C'est à dire : splitter la valeur de motsCles sur +'\", parcours la liste en rajoutant
     * des conditions like % valeur %
     *
     * @param colonne
     * @param motsCles
     * @return
     */
    public static Condition rechercheMots(final String colonne, final String motsCles) {
        final ConditionList conditionsPourRechMots = new ConditionList();
        if (StringUtils.isNotEmpty(motsCles) && StringUtils.isNotBlank(motsCles)) {
            final String[] motsClesSpliter = StringUtils.split(motsCles.toUpperCase(), " +'\"");
            for (final String motCle : motsClesSpliter) {
                if (StringUtils.isNotEmpty(motCle) && StringUtils.isNotBlank(motCle)) {
                    conditionsPourRechMots.and(like(colonne, motCle, "%", "%"));
                }
            }
        }
        return conditionsPourRechMots;
    }

    /**
     * Si la date est supérieure à l'année 1970 on ajoute une condition de type : COLONNE >= DATE.
     *
     * @param colonne
     * @param valeur
     * @return
     */
    public static Condition critereDateDebut(final String colonne, final Date valeur) {
        return greaterEquals(colonne, valeur, TypeOperande.DATE);
    }

    /**
     * Si la date est supérieure à l'année 1970 on ajoute une condition.
     *
     * @param colonne
     * @param valeur
     * @return
     */
    public static Condition critereDateFin(final String colonne, final Date valeur) {
        return lessEquals(colonne, valeur, TypeOperande.DATE);
    }

    /**
     * Construit une condition de même type que SQLUtil.formaterRechercheParCritereMutliple(String, String) C'est à dire : si la valeur est différentes de CODE_DEFAUT, on splitte
     * valeur sur des "+" puis on construit des requêtes de type : COLONNE LIKE "" valeur "" OR COLONNE LIKE "" valeur ";%" ... etc Ceci à cause des champs de la base contenant
     * plusieurs valeurs séparés par des ";"
     *
     * @param colonne
     * @param valeur
     * @return
     */
    public static Condition likePourValeursMultiple(final String colonne, final String valeur) {
        final ConditionList criterevaleurMultiple = new ConditionList();
        if (isValeurValidePourChampMultiple(valeur)) {
            final String[] valeursMultiples = StringUtils.split(valeur, "+");
            for (final String valeurPossible : valeursMultiples) {
                criterevaleurMultiple.or(like(colonne, valeurPossible, "", ""));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "", ";%"));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%;", ""));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%;", ";%"));
            }
        }
        return criterevaleurMultiple;
    }

    /**
     * Construit une condition afin de récupérer des valeurs séparées par des ";" mais qui peuvent aussi avoir un séparateur entre les ";".
     *
     * @param colonne
     * @param valeur
     * @return
     */
    public static Condition likePourValeursMultipleAvecSeparateurInterne(final String colonne, final String valeur, final String separateurInterne) {
        final ConditionList criterevaleurMultiple = new ConditionList();
        if (isValeurValidePourChampMultiple(valeur)) {
            final String[] valeursMultiples = StringUtils.split(valeur, "+");
            final String separateur = EscapeString.escapeSql(StringUtils.defaultString(separateurInterne));
            for (final String valeurPossible : valeursMultiples) {
                criterevaleurMultiple.or(like(colonne, valeurPossible, "", ""));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "", separateur + "%"));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%" + separateur, ""));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%" + separateur, separateur + "%"));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%;", separateur +"%"));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%;", ""));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%" + separateur, ";%"));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "", ";%"));
                criterevaleurMultiple.or(like(colonne, valeurPossible, "%;", ";%"));
            }
        }
        return criterevaleurMultiple;
    }

    /**
     * Construit une condition de même type que SQLUtil#formaterRechercheParCritereMutliple(String, String) C'est à dire : si la valeur est différentes de CODE_DEFAUT, on splitte
     * valeur sur des "+" puis on construit des requêtes de type : COLONNE LIKE "" valeur "" OR COLONNE LIKE "" valeur ";%" ... etc Ceci à cause des champs de la base contenant
     * plusieurs valeurs séparés par des ";"
     *
     * @param colonne
     * @param valeur
     * @param separateur
     * @return
     */
    public static Condition notlikePourValeursMultiple(final String colonne, final String valeur, final String separateur) {
        final ConditionList criterevaleurMultiple = new ConditionList();
        if (isValeurValidePourChampMultiple(valeur)) {
            final String[] valeursMultiples = StringUtils.split(valeur, separateur);
            for (final String valeurPossible : valeursMultiples) {
                criterevaleurMultiple.and(notlike(colonne, valeurPossible, "", ""));
                criterevaleurMultiple.and(notlike(colonne, valeurPossible, "", ";%"));
                criterevaleurMultiple.and(notlike(colonne, valeurPossible, "%;", ""));
                criterevaleurMultiple.and(notlike(colonne, valeurPossible, "%;", ";%"));
            }
        }
        return criterevaleurMultiple;
    }

    private static boolean isValeurValidePourChampMultiple(final String valeur) {
        return StringUtils.isNotEmpty(valeur) && !CODE_DEFAUT.equals(valeur);
    }

    /**
     * Migration de SQLUtil.ajouterCriteresDsi(String, OMContext) Calcule l'ensemble des conditions sur les critères dsi de la fiche courante. Si la diffusion est limitée
     * (diffusion selective), on ajoute des critères sur les ip et les hosts si renseigné dans le JTF (ancien comportement) les groupes et profils les espaces collab Les groupes
     * perso Les espaces perso
     *
     * @param ctx
     *            Le contexte si c'est selectif le contexte est de type ContexteUniv
     * @param fiche
     *            la fiche courante
     * @return les conditions DSI, null si il n'y en a pas.
     */
    public static Condition getConditionDSI(final OMContext ctx, final FicheUniv fiche) {
        return CritereDSI.traiterCritereDSI(ctx, fiche);
    }

    /**
     * Migration de SQLUtil.ajouterCriteresBO(String, OMContext, FicheUniv).
     *
     * @param ctx
     * @param fiche
     * @return
     */
    public static Condition getConditionBO(final OMContext ctx, final FicheUniv fiche) {
        return CritereBO.traiterCritereBO(ctx, fiche);
    }

    /**
     *
     * @param ctx
     * @return
     */
    public static Condition getConditionRubPubDSIFiche(final OMContext ctx, final String codeRubrique) {
        return CritereRubriquePublicationHelper.traiterCritereRubPubDSIFiche(ctx, codeRubrique);
    }

    /**
     *
     * @param ficheUniv
     * @return
     */
    public static Condition getConditionRubPubFiche(final FicheUniv ficheUniv) {
        return CritereRubriquePublicationHelper.traiterConditionRubPubIdRubriqueAndFicheOrig(ficheUniv);
    }

    /**
     *
     * @param ctx
     * @return
     */
    public static Condition getConditionRubPubSuivantAction(final OMContext ctx, final String action, final String nomColonne, final String codeRubrique) {
        return CritereRubriquePublicationHelper.traiterCritereRubriquePublicationSuivantAction(ctx, action, nomColonne, codeRubrique);
    }

    /**
     * Recherche d'une fiche par rapport à sa rubrique. Migration de SQLUtil.formaterRechercheParRubrique(String, String).
     *
     * @param codeRubrique
     * @param nomColonne
     * @return
     */
    public static Condition getConditionRubrique(final String nomColonne, String codeRubrique) {
        if (isRubriqueNoArbo(codeRubrique)) {
            codeRubrique = StringUtils.substringBefore(codeRubrique, ConstanteSQL.NO_ARBO);
            return ConditionHelper.egalVarchar(nomColonne, codeRubrique);
        }
        final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        final RubriqueBean infosRub = serviceRubrique.getRubriqueByCode(codeRubrique);
        final Set<String> codeRubriquesTousNiveaux = new HashSet<>();
        if (infosRub != null) {
            final Collection<RubriqueBean> infosRubriquesToutNiveau = serviceRubrique.getAllChilds(infosRub.getCode());
            infosRubriquesToutNiveau.add(infosRub);
            for (final RubriqueBean rub : infosRubriquesToutNiveau) {
                codeRubriquesTousNiveaux.add(rub.getCode());
            }
        }
        return ConditionHelper.in(nomColonne, codeRubriquesTousNiveaux);
    }

    private static boolean isRubriqueNoArbo(final String codeRubrique) {
        return StringUtils.isNotEmpty(codeRubrique) && !codeRubrique.contains(";") && codeRubrique.endsWith("_NOARBO");
    }

    /**
     *
     * @param codeStructure
     * @param nomColonne
     * @return
     */
    public static Condition getConditionStructure(final String nomColonne, String codeStructure) {
        codeStructure = codeStructure.replaceAll("\\+", ";");
        if (isStructureNoArbo(codeStructure)) {
            codeStructure = StringUtils.substringBefore(codeStructure, ConstanteSQL.NO_ARBO);
            return ConditionHelper.egalVarchar(nomColonne, codeStructure);
        }
        final Set<String> listeDesCodesStructures = getListeDesCodesStructures(codeStructure);
        return ConditionHelper.in(nomColonne, listeDesCodesStructures);
    }

    public static boolean isStructureNoArbo(final String codeStructure) {
        return StringUtils.isNotEmpty(codeStructure) && !codeStructure.contains(";") && codeStructure.endsWith("_NOARBO");
    }

    public static Set<String> getListeDesCodesStructures(final String codesStructures) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        final String[] codesStructuresSplite = codesStructures.split(";");
        final Set<String> listeDesCodesStructures = new HashSet<>();
        for (final String codeCourant : codesStructuresSplite) {
            if (StringUtils.isNotEmpty(codeCourant)) {
                listeDesCodesStructures.add(codeCourant);
                final Collection<StructureModele> allInfosStructures = serviceStructure.getAllSubStructures(codeCourant, LangueUtil.getIndiceLocaleDefaut(), true);
                for (final StructureModele structureCourante : allInfosStructures) {
                    listeDesCodesStructures.add(structureCourante.getCode());
                }
            }
        }
        return listeDesCodesStructures;
    }

    /**
     * Migration de la méthode SQLUtil.formaterRechercheParStructureMultiple(String, String).
     *
     * @param nomColonne
     *            le nomm de la colonne à requeter
     * @param codeStructure
     *            le code de(s) structure(s) (peut être séparer par des ";"
     * @return une conditionde type nomColonne REGEXP '(^|.*;|.*\\\\[)( codesStructures )($|;.*|\\\\].*)'
     */
    public static Condition getConditionStructureMultiple(final String nomColonne, String codeStructure) {
        final ServiceStructure serviceStructure = ServiceManager.getServiceForBean(StructureModele.class);
        codeStructure = codeStructure.replaceAll("\\+", ";");
        final String[] codeStructureSplite = codeStructure.split(";");
        final StringBuilder regexpAExecuter = new StringBuilder();
        for (final String code : codeStructureSplite) {
            if (StringUtils.isNotEmpty(code)) {
                if (regexpAExecuter.length() != 0) {
                    regexpAExecuter.append("|");
                }
                regexpAExecuter.append(EscapeString.escapeSql(code));
                final Collection<StructureModele> allStructure = serviceStructure.getAllSubStructures(code, LangueUtil.getIndiceLocaleDefaut(), true);
                for (final StructureModele structure : allStructure) {
                    regexpAExecuter.append("|");
                    regexpAExecuter.append(structure.getCode());
                }
            }
        }
        if (regexpAExecuter.length() == 0) {
            return null;
        } else {
            regexpAExecuter.insert(0, "'(^|.*;|.*\\\\[)(");
            regexpAExecuter.append(")($|;.*|\\\\].*)'");
        }
        return ConditionHelper.genericConditionSurColonne(nomColonne, regexpAExecuter.toString(), TypeOperande.NON_ECHAPABLE, Operateur.REGEXP);
    }

    /**
     * Construit une clause where de type where T1.CODE = "toto" and T1.LANGUE = "toto" and T1.ETAT_OBJET = "toto".
     *
     * @param code
     *            si le code n'est pas vide; on le rajoute à la requête
     * @param langue
     *            si la langue n'est pas vide, on la rajoute à la requête
     * @param etat
     *            si l'état n'est pas vide et n'est pas égal à 0000, on le rajoute à la requête
     * @return
     */
    public static ClauseWhere whereCodeLangueEtat(final String code, final String langue, final String etat) {
        final ClauseWhere codeLangueEtat = new ClauseWhere();
        if (StringUtils.isNotEmpty(code)) {
            codeLangueEtat.setPremiereCondition(ConditionHelper.egalVarchar("T1.CODE", code));
        }
        if (StringUtils.isNotEmpty(langue)) {
            codeLangueEtat.and(ConditionHelper.egalVarchar("T1.LANGUE", langue));
        }
        if (StringUtils.isNotEmpty(etat) && !CODE_DEFAUT.equals(etat)) {
            codeLangueEtat.and(ConditionHelper.egalVarchar("T1.ETAT_OBJET", etat));
        }
        return codeLangueEtat;
    }
}
