package com.univ.utils.sql.criterespecifique;

import java.util.Collection;
import java.util.Map;
import java.util.Vector;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.database.OMContext;
import com.univ.objetspartages.om.AutorisationBean;
import com.univ.objetspartages.om.FicheRattachementsSecondaires;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.Perimetre;
import com.univ.objetspartages.om.PermissionBean;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.om.StructureModele;
import com.univ.utils.sql.ConstanteSQL;
import com.univ.utils.sql.condition.Condition;
import com.univ.utils.sql.condition.ConditionList;

/**
 * Classe utilitaire permettant de calculer les conditions pour le BO lors d'une requête SQL. C'est une migration de SQLUtil.ajouterCriteresBO(String, OMContext, FicheUniv)
 *
 * @author olivier.camon
 */
public final class CritereBO {

    /**
     *
     * @param ctx
     * @param fiche
     * @return
     */
    public static Condition traiterCritereBO(final OMContext ctx, final FicheUniv fiche) {
        // la montagne russe commence!
        final ConditionList critereBO = new ConditionList();
        Boolean aucuneRestriction = Boolean.FALSE;
        final boolean isATraiterBO = isATraiterBO(ctx);
        if (!isATraiterBO) {
            return critereBO;
        }
        final AutorisationBean autorisations = (AutorisationBean) ctx.getDatas().get("AUTORISATIONS");
        if (autorisations != null) {
            final Map<String, Vector<Perimetre>> listePermissions = autorisations.getListePermissions();
            final String codeObjet = ReferentielObjets.getCodeObjet(fiche);
            for (Map.Entry<String,Vector<Perimetre>> permissionPerimetre : listePermissions.entrySet()) {
                final PermissionBean permission = new PermissionBean(permissionPerimetre.getKey());
                // on traite les permissions sur l'objet courant
                if (isPermissionATraiter(permission, codeObjet)) {
                    final Collection<Perimetre> perimetres = permissionPerimetre.getValue();
                    // on parcourt l'ensemble des périmètres assocciés à la permission
                    for (Perimetre perimetreCourant : perimetres) {
                        if (isPerimetreSansRestriction(perimetreCourant)) {
                            aucuneRestriction = true;
                        } else {
                            final ConditionList conditionsSelectionPerimetre = traiterCriterePerimetre(fiche, perimetreCourant);
                            if (!conditionsSelectionPerimetre.isEmpty()) {
                                critereBO.or(conditionsSelectionPerimetre);
                            }
                        }
                    }
                }
            }
            if (!aucuneRestriction) {
                critereBO.or(ConditionHelper.egalVarchar("T1.CODE_REDACTEUR", autorisations.getCode()));
            }
        }
        if (critereBO.isEmpty() && !aucuneRestriction) {
            critereBO.and(ConstanteSQL.CONDITION_IMPOSSIBLE);
        }
        return critereBO;
    }

    private static ConditionList traiterCriterePerimetre(final FicheUniv fiche, final Perimetre perimetre) {
        final ConditionList conditionsSelectionPerimetre = new ConditionList();
        boolean criteresPertinents = Boolean.FALSE;
        final String codeRubrique = perimetre.getCodeRubrique();
        if (StringUtils.isNotEmpty(codeRubrique)) {
            criteresPertinents = Boolean.TRUE;
            final Condition conditionRubrique = traiterConditionRubrique(codeRubrique);
            conditionsSelectionPerimetre.setPremiereCondtion(conditionRubrique);
        }
        final String codeStructure = perimetre.getCodeStructure();
        if (StringUtils.isNotEmpty(codeStructure)) {
            criteresPertinents = Boolean.TRUE;
            final ConditionList conditionStructure = traiterConditionStructure(codeStructure, fiche);
            if (!conditionStructure.isEmpty()) {
                conditionsSelectionPerimetre.and(conditionStructure);
            }
        }
        final String codeEspaceCollaboratif = perimetre.getCodeEspaceCollaboratif();
        if (StringUtils.isNotEmpty(codeEspaceCollaboratif)) {
            final ConditionList conditionsEspaceCollab = new ConditionList(ConditionHelper.egalVarchar("T1.DIFFUSION_MODE_RESTRICTION", "4"));
            conditionsEspaceCollab.and(ConditionHelper.egalVarchar("T1.DIFFUSION_PUBLIC_VISE_RESTRICTION", codeEspaceCollaboratif));
            conditionsSelectionPerimetre.and(conditionsEspaceCollab);
            criteresPertinents = Boolean.TRUE;
        }
        if (!criteresPertinents) {
            return new ConditionList();
        }
        return conditionsSelectionPerimetre;
    }

    private static Condition traiterConditionRubrique(final String codeRubrique) {
        Condition conditionRubrique;
        final String nomCodeRubrique = "T1.CODE_RUBRIQUE";
        if (ConstanteSQL.JOCKER_CODE.equals(codeRubrique)) {
            conditionRubrique = ConditionHelper.egalVarchar(nomCodeRubrique, "");
        } else {
            conditionRubrique = ConditionHelper.getConditionRubrique(nomCodeRubrique.toUpperCase(), codeRubrique);
        }
        return conditionRubrique;
    }

    private static boolean isATraiterBO(final OMContext ctx) {
        Boolean isATraiterBO = Boolean.TRUE;
        if (ctx.getDatas() != null) {
            final String controle = (String) ctx.getDatas().get("CONTROLE_PERIMETRE_BO");
            isATraiterBO = "1".equals(controle);
        }
        return isATraiterBO;
    }

    private static boolean isPermissionATraiter(final PermissionBean permission, final String codeObjet) {
        return "FICHE".equals(permission.getType()) && permission.getObjet().equals(codeObjet) && "M".equals(permission.getAction());
    }

    private static boolean isPerimetreSansRestriction(final Perimetre perimetre) {
        return StringUtils.isEmpty(perimetre.getCodeRubrique()) && StringUtils.isEmpty(perimetre.getCodeStructure()) && StringUtils.isEmpty(perimetre.getCodeProfil()) && StringUtils.isEmpty(perimetre.getCodeGroupe());
    }

    private static ConditionList traiterConditionStructure(final String codeStructure, final FicheUniv fiche) {
        final ConditionList conditionStructure = new ConditionList();
        String nomCodeStructure = "T1.CODE_RATTACHEMENT";
        if (ConstanteSQL.JOCKER_CODE.equals(codeStructure)) {
            conditionStructure.setPremiereCondtion(ConditionHelper.egalVarchar(nomCodeStructure, ""));
            if (fiche instanceof FicheRattachementsSecondaires) {
                nomCodeStructure = "T1.CODE_RATTACHEMENT_AUTRES";
                conditionStructure.and(ConditionHelper.egalVarchar(nomCodeStructure, ""));
            }
        } else {
            conditionStructure.setPremiereCondtion(ConditionHelper.getConditionStructure(nomCodeStructure.toUpperCase(), codeStructure));
            if (fiche instanceof StructureModele) {
                conditionStructure.or(ConditionHelper.egalVarchar("CODE", codeStructure));
            }
            if (fiche instanceof FicheRattachementsSecondaires) {
                nomCodeStructure = "T1.CODE_RATTACHEMENT_AUTRES";
                conditionStructure.or(ConditionHelper.getConditionStructureMultiple(nomCodeStructure.toUpperCase(), codeStructure));
            }
        }
        return conditionStructure;
    }
}
