package com.univ.utils.sql.criterespecifique;

import org.apache.commons.lang3.StringUtils;

import com.univ.utils.sql.clause.ClauseOrderBy;
import com.univ.utils.sql.clause.ClauseOrderBy.SensDeTri;

/**
 * Classe utilitaire permettant de creer une clause OrderBy depuis les méthodes select des FicheUniv.
 *
 * @author olivier.camon
 *
 */
public class OrderByHelper {

    /**
     * Lorsque valeurOrdre est renseigné, on essaye de déconstruire la valeur afin de pouvoir retrouver les clauses OrderBy fourni en paramètre. Si la clause n'est pas valide, on
     * n'ajoute pas de tri.
     *
     * @param valeurOrdre
     * @return
     */
    public static ClauseOrderBy reconstruireClauseOrderBy(final String valeurOrdre) {
        ClauseOrderBy orderBy = new ClauseOrderBy();
        if (StringUtils.isNotEmpty(valeurOrdre)) {
            String[] toutlesOrdres = valeurOrdre.split(",");
            for (String ordre : toutlesOrdres) {
                orderBy = traiterChaqueClauseOrderBy(orderBy, ordre);
            }
        }
        return orderBy;
    }

    private static ClauseOrderBy traiterChaqueClauseOrderBy(ClauseOrderBy orderBy, String valeurOrdre) {
        if (StringUtils.isNotEmpty(valeurOrdre)) {
            valeurOrdre = valeurOrdre.toUpperCase();
            if (valeurOrdre.trim().endsWith(SensDeTri.ASC.getSens().trim()) || valeurOrdre.trim().endsWith(SensDeTri.DESC.getSens().trim())) {
                if (valeurOrdre.trim().endsWith(SensDeTri.DESC.getSens().trim())) {
                    valeurOrdre = valeurOrdre.substring(0, valeurOrdre.indexOf(SensDeTri.DESC.getSens().trim()));
                    orderBy.orderBy(valeurOrdre, SensDeTri.DESC);
                } else {
                    valeurOrdre = valeurOrdre.substring(0, valeurOrdre.indexOf(SensDeTri.ASC.getSens().trim()));
                    orderBy.orderBy(valeurOrdre, SensDeTri.ASC);
                }
            } else {
                orderBy.orderBy(valeurOrdre, SensDeTri.ASC);
            }
        }
        return orderBy;
    }
}
