package com.univ.utils.sql.operande;

public class LongOperande implements Operande {

    private Object valeurDuLong;

    public LongOperande(Long valeur) {
        this.valeurDuLong = valeur;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeurDuLong == null) {
            return "";
        }
        return String.valueOf(valeurDuLong);
    }
}
