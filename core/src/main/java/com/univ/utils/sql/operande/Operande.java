package com.univ.utils.sql.operande;

/**
 * Interface représentant la plus petite valeur d'une requête SQL. Cela peut être un nom de champ, une variable, un appel de fonction, ...
 *
 * @author olivier.camon
 *
 */
public interface Operande {

    /**
     * Formate l'opérande pour retourner une valeur possible en SQL
     *
     * @return
     */
    String formaterOperande();
}
