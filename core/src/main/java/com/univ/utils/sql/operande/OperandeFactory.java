package com.univ.utils.sql.operande;

import java.util.Collection;
import java.util.Date;

/**
 * Fabrique permettant de construire les opérandes en fonction de leur type.
 *
 * @author olivier.camon
 *
 */
public class OperandeFactory {

    /**
     * Retourne le bon type d'opérande en fonction du type choisie. Si le type n'est pas encore prévue, l'opérande par défaut est de type VARCHAR. Si le type choisi ne correspond
     * pas au type d'objet de valeurOperande, une Exception de type {@link IllegalArgumentException } est lancée.
     *
     * @param valeurOperande
     *            La valeur de l'opérande
     * @param typeValeur
     *            le type d'opérande VARCHAR...
     * @return L'instance d'opérande correspondant à ce que l'on souhaite.
     */
    @SuppressWarnings("unchecked")
    public static Operande creerOperande(final Object valeurOperande, final TypeOperande typeValeur) {
        Operande operandeACreer = null;
        if (typeValeur == null) {
            gererErreur(valeurOperande, typeValeur);
        }
        switch (typeValeur) {
            case VARCHAR:
                if (valeurOperande instanceof String) {
                    operandeACreer = new VarcharOperande((String) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case NOM_CHAMP:
                if (valeurOperande instanceof String) {
                    operandeACreer = new NomChamp((String) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case LISTE_VARCHAR:
                if (valeurOperande instanceof Collection<?>) {
                    operandeACreer = new ListeVarchar((Collection<String>) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case DATE:
                if (valeurOperande instanceof Date) {
                    operandeACreer = new OperandeDate((Date) valeurOperande, Boolean.FALSE);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case DATE_ET_HEURE:
                if (valeurOperande instanceof Date) {
                    operandeACreer = new OperandeDate((Date) valeurOperande, Boolean.TRUE);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case LONG:
                if (valeurOperande instanceof Long) {
                    operandeACreer = new LongOperande((Long) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case INTEGER:
                if (valeurOperande instanceof Integer) {
                    operandeACreer = new IntegerOperande((Integer) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            case NON_ECHAPABLE:
                if (valeurOperande instanceof String) {
                    operandeACreer = new NonEchapable((String) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
            default:
                if (valeurOperande instanceof String) {
                    operandeACreer = new VarcharOperande((String) valeurOperande);
                } else {
                    gererErreur(valeurOperande, typeValeur);
                }
                break;
        }
        return operandeACreer;
    }

    private static void gererErreur(final Object valeurOperande, final TypeOperande typeValeur) {
        if (typeValeur == null) {
            throw new IllegalArgumentException("Impossible de creer une Operande de type null");
        } else if (valeurOperande == null) {
            throw new IllegalArgumentException("Impossible de creer une Operande de type : " + typeValeur.toString() + " avec une valeur null");
        } else {
            throw new IllegalArgumentException("Impossible de creer une Operande de type : " + typeValeur.toString() + " avec une valeur d'instance : " + valeurOperande.getClass());
        }
    }
}
