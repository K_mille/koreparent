package com.univ.utils.sql.operande;

import com.univ.utils.EscapeString;

public class VarcharOperande implements Operande {

    private String valeurOperande;

    public VarcharOperande(String valeurOperande) {
        this.valeurOperande = valeurOperande;
    }

    /*
     * (non-Javadoc)
     * @see com.univ.utils.sql.operande.Operande#formaterOperande()
     */
    @Override
    public String formaterOperande() {
        if (valeurOperande == null) {
            return "";
        }
        return "'" + EscapeString.escapeSql(valeurOperande) + "'";
    }
}
