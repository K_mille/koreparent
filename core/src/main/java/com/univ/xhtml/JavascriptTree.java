package com.univ.xhtml;

import java.util.ArrayList;
// TODO: Auto-generated Javadoc

/**
 * Classe représentant un arbre Javascript.
 *
 * @author FBI
 */
public class JavascriptTree {

    /** Liste des éléments de l'arbre. */
    private ArrayList<JavascriptTreeItem> listItem = null;

    /**
     * Constructeur.
     */
    public JavascriptTree() {
        this.listItem = new ArrayList<>();
    }

    /**
     * Ajoute un élément à la liste.
     *
     * @param item
     *            L'élément à ajouter
     */
    public void addItem(JavascriptTreeItem item) {
        listItem.add(item);
    }

    /**
     * Affiche l'arbre Javascript.
     *
     * @return Le code Javascript d'affichage de l'arbre
     */
    public String print() {
        StringBuilder sb = new StringBuilder();
        for (JavascriptTreeItem aListItem : listItem) {
            sb.append(aListItem.print());
        }
        return sb.toString();
    }
}
