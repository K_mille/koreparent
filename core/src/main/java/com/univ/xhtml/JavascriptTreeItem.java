package com.univ.xhtml;

import org.apache.commons.lang3.StringUtils;
// TODO: Auto-generated Javadoc

/**
 * Classe correspondant à un élément d'un menu Javascript.
 *
 * @author FBI
 */
public class JavascriptTreeItem {

    /** Code de l'élément parent. */
    private String codeParent = null;

    /** Code de l'élément. */
    private String code = null;

    /** Libellé de l'élément. */
    private String text = null;

    /** Etat ouvert(1) ou fermé (0) de l'élément. */
    private boolean opened = true;

    /** Elément sélectionnable (1) ou non (0). */
    private boolean selectable = true;

    /**
     * Constructeur.
     *
     * @param codeParent
     *            the code parent
     * @param code
     *            the code
     * @param text
     *            the text
     * @param opened
     *            the opened
     * @param selectable
     *            the selectable
     */
    public JavascriptTreeItem(final String codeParent, final String code, final String text, final boolean opened, final boolean selectable) {
        this.codeParent = codeParent;
        this.code = code;
        this.text = text;
        this.opened = opened;
        this.selectable = selectable;
    }

    /**
     * Affiche l'élément.
     *
     * @return the string
     */
    public String print() {
        return "oTree.AddNode('" + StringUtils.replace(codeParent, "'", "\\'") + "', '" + StringUtils.replace(code, "'", "\\'") + "', '" + StringUtils.replace(text, "'", "\\'") + "', " + (opened ? "true" : "false") + ", " + (selectable ? "true" : "false") + ");\r\n";
    }
}
