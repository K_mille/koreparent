package com.univ.xhtml.pdf;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import com.jsbsoft.jtf.exception.ErreurApplicative;
// TODO: Auto-generated Javadoc

/**
 * The Interface IGenerateurPDF.
 */
public interface IGenerateurPDF {

    /**
     * Creates the pdf.
     *
     * @param is
     *            the is
     * @param os
     *            the os
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     */
    void createPDF(InputStream is, OutputStream os) throws ErreurApplicative;

    /**
     * Creates the pdf.
     *
     * @param fXml
     *            the f xml
     * @param fXslt
     *            the f xslt
     * @param os
     *            the os
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     */
    void createPDF(File fXml, File fXslt, OutputStream os) throws ErreurApplicative;
}
