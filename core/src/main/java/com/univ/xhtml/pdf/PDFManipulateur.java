package com.univ.xhtml.pdf;

import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kportal.core.config.PropertyHelper;
// TODO: Auto-generated Javadoc

/**
 * Prend en charge la manipulation de PDF. Créée pour gérer les différentes versions de librairies permettant de générer des PDF.
 *
 * @author jbiard
 */
public class PDFManipulateur {

    /** The Constant PARAM_JTF_PDF_GENERATION_CLASS. */
    public final static String PARAM_JTF_PDF_GENERATION_CLASS = "pdf.generation_impl";

    /** The _log. */
    private static final Logger LOG = LoggerFactory.getLogger(PDFManipulateur.class);

    /** The _instance. */
    private static PDFManipulateur _instance;

    /** The pdf generation impl. */
    private IGenerateurPDF pdfGenerationImpl;

    /**
     * Constructeur privé pour prevenir de l'instanciation (cf methodes statiques).
     */
    private PDFManipulateur() {
        loadImpl();
    }

    /**
     * Lit un flux d'entrée pour écrire en sortie le flux correspondant au PDF.
     *
     * @param is
     *            flux d'entree
     * @param os
     *            flux PDF de sortie
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     */
    public static void createPDF(final InputStream is, final OutputStream os) throws ErreurApplicative {
        createInstance();
        _instance.pdfGenerationImpl.createPDF(is, os);
    }

    /**
     * Cree un PDF en appliquant une transformation XSLT.
     *
     * @param xmlFile
     *            fichier source
     * @param xsltFile
     *            fichier XSL
     * @param os
     *            flux de sortie
     *
     * @throws ErreurApplicative
     *             the erreur applicative
     */
    public static void createPDF(final File xmlFile, final File xsltFile, final OutputStream os) throws ErreurApplicative {
        createInstance();
        _instance.pdfGenerationImpl.createPDF(xmlFile, xsltFile, os);
    }

    /**
     * Cree une instance.
     */
    private static void createInstance() {
        if (_instance == null) {
            synchronized (PDFManipulateur.class) {
                // 2eme thread
                if (_instance == null) {
                    _instance = new PDFManipulateur();
                }
            }
        }
    }

    /**
     * Charge l'implementation permettant la generation de PDF.
     */
    private void loadImpl() {
        final String nomClasse = PropertyHelper.getCoreProperty(PARAM_JTF_PDF_GENERATION_CLASS);
        if (nomClasse != null) {
            Object o = null;
            try {
                o = Class.forName(nomClasse).newInstance();
            } catch (final InstantiationException | IllegalAccessException | ClassNotFoundException e) {
                LOG.error("Echec chargement generateur PDF", e);
            }
            if ((o != null) && (o instanceof IGenerateurPDF)) {
                pdfGenerationImpl = (IGenerateurPDF) o;
                LOG.debug("Generateur PDF " + pdfGenerationImpl);
            } else {
                pdfGenerationImpl = new DefautGenerateurPDF();
            }
        } else {
            pdfGenerationImpl = new DefautGenerateurPDF();
        }
    }
}
