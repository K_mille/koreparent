package com.univ.xml;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

import com.jsbsoft.jtf.core.HTMLUtil;
// TODO: Auto-generated Javadoc

/**
 * Insérez la description du type à cet endroit. Date de création : (26/04/2002 09:30:40)
 *
 * @author :
 */
public class NodeUtil {

    /**
     * Ajout d'un noeud.
     *
     * @param pere
     *            the pere
     * @param nomDonnee
     *            the nom donnee
     * @param valeur
     *            the valeur
     */
    public static void addNode(final Node pere, final String nomDonnee, final String valeur) {
        addNode(pere, nomDonnee, valeur, true);
    }

    /**
     * Adds the node.
     *
     * @param pere
     *            the pere
     * @param nomNoeud
     *            the nom noeud
     * @param majus
     *            the majus
     *
     * @return the node
     */
    public static Node addNode(final Node pere, String nomNoeud, final boolean majus) {
        if (majus) {
            nomNoeud = nomNoeud.toUpperCase();
        }
        final Document document = pere.getOwnerDocument();
        final Node noeud = document.createElement(nomNoeud);
        pere.appendChild(noeud);
        return noeud;
    }

    /**
     * Ajout d'un noeud.
     *
     * @param pere
     *            the pere
     * @param nomDonnee
     *            the nom donnee
     * @param valeur
     *            the valeur
     * @param majus
     *            the majus
     */
    public static void addNode(final Node pere, final String nomDonnee, String valeur, final boolean majus) {
        valeur = StringUtils.defaultString(valeur);
        final Document document = pere.getOwnerDocument();
        /*********************************/
        /* Suppression des requetes      */
        /*********************************/
        int indexTexte = 0;
        int indexDebutMotCle = -1;
        int indexFinMotCle = 0;
        final StringBuilder newTexte = new StringBuilder(10000);
        /* Boucle sur chaque mot clé */
        while ((indexDebutMotCle = valeur.indexOf("[traitement", indexTexte)) != -1) {
            // Recopie portion avant le mot- clé
            newTexte.append(valeur.substring(indexTexte, indexDebutMotCle));
            // Extraction de la chaine
            indexFinMotCle = valeur.indexOf("]", indexDebutMotCle);
            indexTexte = indexFinMotCle + 1;
        }
        if (indexTexte < valeur.length()) {
            newTexte.append(valeur.substring(indexTexte));
        }
        valeur = newTexte.toString();
        String chaineASCII = HTMLUtil.convertirEnASCII(valeur, true);
        /**********************************************************/
        /* IL EST NECESSAIRE D'EFFECTUER D'AUTRES TRANSFORMATIONS */
        /* PROPRES AU XML                                         */
        /**********************************************************/
        /* ATTENTION : la chaine peut contenir des Codes > 255 (coller Word)
          -> TRANSCRIPTION
         */
        chaineASCII = StringUtils.replace(chaineASCII, "&#8217;", "'");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8230;", "...");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8211;", "-");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8221;", "'");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8364;", " Eur ");
        // JSS 20040226 : ajout carac euro    (demande Axonie)
        chaineASCII = StringUtils.replace(chaineASCII, "&euro;", " Eur ");
        chaineASCII = StringUtils.replace(chaineASCII, "&#339;", "oe");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8212;", "-");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8226;", "-");
        chaineASCII = StringUtils.replace(chaineASCII, "&#8220;", " ");
        // La double-quote fait planter l'import dans word
        chaineASCII = StringUtils.replace(chaineASCII, "\"", "'");
        // Suppression des tildes (126) et des pipes (124) |
        // Ces caractères sont utilisées dans le publipostage
        //chaineASCII = StringUtils.replace(chaineASCII,"~"," ");
        //chaineASCII = StringUtils.replace(chaineASCII,"|"," ");
        String name = nomDonnee;
        if (majus) {
            name = name.toUpperCase();
        }
        final Node attribut = document.createElement(name);
        attribut.appendChild(document.createTextNode(chaineASCII));
        pere.appendChild(attribut);
    }

    /**
     * Renvoie la valeur d'un noeud sous forme de chaine.
     *
     * @param node
     *            the node
     *
     * @return the string
     *
     * @throws Exception
     *             the exception
     */
    public static String extraireValeurNode(final Node node) throws Exception {
        String res = "";
        if (node.getFirstChild() != null) {
            res = node.getFirstChild().getNodeValue();
        }
        return res;
    }
}
