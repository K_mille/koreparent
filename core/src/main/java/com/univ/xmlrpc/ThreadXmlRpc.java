/*
 * Created on 10 févr. 2004
 *
 * To change the template for this generated file go to
 * Window>Preferences>Java>Code Generation>Code and Comments
 */
package com.univ.xmlrpc;

import java.util.Vector;

import org.apache.xmlrpc.XmlRpcClient;
import org.slf4j.LoggerFactory;
// TODO: Auto-generated Javadoc

/**
 * The Class ThreadXmlRpc.
 *
 * @author jean-sébastien steux
 *
 *
 *
 *         Modif le 10 févr. 2004
 */
public class ThreadXmlRpc implements Runnable {

    private static org.slf4j.Logger LOG = LoggerFactory.getLogger(ThreadXmlRpc.class);

    /** The co. */
    RequeteXmlRpc co;

    /** The methode. */
    private String url, user, passwor, methode;

    /** The param. */
    private Vector param;

    /**
     * The Constructor.
     *
     * @param _co
     *            the _co
     * @param _url
     *            the _url
     * @param _user
     *            the _user
     * @param _password
     *            the _password
     * @param _methode
     *            the _methode
     * @param _param
     *            the _param
     */
    public ThreadXmlRpc(final RequeteXmlRpc _co, final String _url, final String _user, final String _password, final String _methode, final Vector _param) {
        super();
        setCo(_co);
        setUrl(_url);
        setUser(_user);
        setPasswor(_password);
        setMethode(_methode);
        setParam(_param);
    }

    /* (non-Javadoc)
     * @see java.lang.Runnable#run()
     */
    @Override
    public void run() {
        try {
            final XmlRpcClient client = new XmlRpcClient(getUrl());
            // JB20060228 : conserve en deprecated pour le moment : le passage en 3.0. necessitera encore une autre modif
            // (utilisation d'une factory de config differente de celle de la 2.0.1)
            client.setBasicAuthentication(getUser(), getPasswor());
            final Object o = client.execute(getMethode(), getParam());
            getCo().terminer(o);
        } catch (final Exception e) {
            LOG.error("erreur lors de l'execution de la requete xml rpc", e);
        }
    }

    /**
     * Sets the url.
     *
     * @param newUrl
     *            the new url
     */
    private void setUrl(final java.lang.String newUrl) {
        url = newUrl;
    }

    /**
     * Gets the co.
     *
     * @return the co
     */
    public RequeteXmlRpc getCo() {
        return co;
    }

    /**
     * Gets the methode.
     *
     * @return the methode
     */
    public String getMethode() {
        return methode;
    }

    /**
     * Gets the param.
     *
     * @return the param
     */
    public Vector getParam() {
        return param;
    }

    /**
     * Gets the passwor.
     *
     * @return the passwor
     */
    public String getPasswor() {
        return passwor;
    }

    /**
     * Gets the url.
     *
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * Gets the user.
     *
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * Sets the co.
     *
     * @param office
     *            the office
     */
    public void setCo(final RequeteXmlRpc office) {
        co = office;
    }

    /**
     * Sets the methode.
     *
     * @param string
     *            the string
     */
    public void setMethode(final String string) {
        methode = string;
    }

    /**
     * Sets the param.
     *
     * @param vector
     *            the vector
     */
    public void setParam(final Vector vector) {
        param = vector;
    }

    /**
     * Sets the passwor.
     *
     * @param string
     *            the string
     */
    public void setPasswor(final String string) {
        passwor = string;
    }

    /**
     * Sets the user.
     *
     * @param string
     *            the string
     */
    public void setUser(final String string) {
        user = string;
    }
}
