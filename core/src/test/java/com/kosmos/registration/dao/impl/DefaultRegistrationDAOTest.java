package com.kosmos.registration.dao.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.lang3.tuple.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.registration.action.history.ActionHistory;
import com.kosmos.registration.bean.Registration;
import com.kosmos.registration.bean.RegistrationData;
import com.kosmos.registration.bean.RegistrationState;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;

/**
 * Created on 02/06/2015.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/registration/dao/impl/DefaultRegistrationDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class DefaultRegistrationDAOTest extends AbstractDbUnitTestngTests {

    @Autowired DefaultRegistrationDAO defaultRegistrationDAO;

    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.add.xml")
    @ExpectedDatabase("DefaultRegistrationDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        final Registration registration = new Registration();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        registration.setModelId(2L);
        registration.setDataByAction(new HashMap<String, RegistrationData>());
        registration.setActionHistory(new ArrayList<Pair<String, ActionHistory>>());
        registration.setState(RegistrationState.FINISHED);
        registration.setCreationIdentity("CREATION_IDENTITY");
        registration.setCreationDate(format.parse("1970-01-01 00:00:00"));
        registration.setLastUpdateIdentity("LAST_UPDATE_IDENTITY");
        registration.setLastUpdateDate(format.parse("1970-01-01 00:00:00"));
        defaultRegistrationDAO.add(registration);
    }

    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.update.xml")
    @ExpectedDatabase("DefaultRegistrationDAOTest.update-expected.xml")
    public void testUpdate() {
        final Registration registration = defaultRegistrationDAO.getById(2L);
        registration.setCreationIdentity("CREATION_IDENTITY2");
        defaultRegistrationDAO.update(registration);
    }

    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.add-expected.xml")
    @ExpectedDatabase("DefaultRegistrationDAOTest.add.xml")
    public void testDelete() {
        defaultRegistrationDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.add-expected.xml")
    @ExpectedDatabase("DefaultRegistrationDAOTest.add.xml")
    public void testDeleteByModelId() {
        defaultRegistrationDAO.deleteByModelId(2L);
    }


    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.update.xml")
    public void testById() {
        final Registration registration = defaultRegistrationDAO.getById(2L);
        Assert.assertNotNull(registration);
        Assert.assertEquals(registration.getId(), Long.valueOf(2), "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.update.xml")
    public void testByModelId() {
        final List<Registration> results = defaultRegistrationDAO.getByModel(2L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("DefaultRegistrationDAOTest.update.xml")
    public void testByModelAndState() {
        final List<Registration> results = defaultRegistrationDAO.getByModelAndState(2L, RegistrationState.FINISHED);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un résultat");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "le code doit correspondre à la deuxième entrée");
    }

}
