package com.kosmos.tests.testng;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.AbstractRestrictionFicheBean;

/**
 * Created on 28/05/15.
 */
public class TestFicheUtils {

    public final static DateFormat hourFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public final static DateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");

    public static AbstractRestrictionFicheBean fillCommonFields(AbstractRestrictionFicheBean ficheBean) throws ParseException {
        fillCommonFields((AbstractFicheBean) ficheBean);
        ficheBean.setDiffusionPublicVise("DIFFUSION_PUBLIC_VISE");
        ficheBean.setDiffusionModeRestriction("0");
        ficheBean.setDiffusionPublicViseRestriction("DIFFUSION_PUBLIC_VISE_RESTRICTION");
        return ficheBean;
    }

    public static AbstractFicheBean fillCommonFields(AbstractFicheBean ficheBean) throws ParseException {
        ficheBean.setCode("CODE");
        ficheBean.setCodeRubrique("CODE_RUBRIQUE");
        ficheBean.setCodeRattachement("CODE_RATTACHEMENT");
        ficheBean.setMetaKeywords("META_KEYWORDS");
        ficheBean.setMetaDescription("META_DESCRIPTION");
        ficheBean.setTitreEncadre("TITRE_ENCADRE");
        ficheBean.setContenuEncadre("CONTENU_ENCADRE");
        ficheBean.setEncadreRecherche("E");
        ficheBean.setEncadreRechercheBis("EBIS");
        ficheBean.setDateAlerte(simpleFormat.parse("1970-01-01"));
        ficheBean.setMessageAlerte("MESSAGE_ALERTE");
        ficheBean.setDateCreation(hourFormat.parse("2007-01-04 00:00:00"));
        ficheBean.setDateProposition(hourFormat.parse("2007-01-04 00:00:00"));
        ficheBean.setDateValidation(hourFormat.parse("2010-05-24 17:02:59"));
        ficheBean.setDateModification(hourFormat.parse("2010-05-24 17:02:59"));
        ficheBean.setCodeRedacteur("CODE_REDACTEUR");
        ficheBean.setCodeValidation("CODE_VALIDATION");
        ficheBean.setLangue("0");
        ficheBean.setEtatObjet("0003");
        ficheBean.setNbHits(0L);
        return ficheBean;
    }
}
