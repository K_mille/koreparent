package com.kosmos.usinesite.utils;

import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class InfosSiteHelperTest {

    @Test
    public void testIsHostValid(){
        Assert.assertFalse(InfosSiteHelper.isHostValid(null));
        Assert.assertFalse(InfosSiteHelper.isHostValid(StringUtils.EMPTY));
        Assert.assertFalse(InfosSiteHelper.isHostValid("http://www.google.fr"));
        Assert.assertFalse(InfosSiteHelper.isHostValid("https://www.google.fr"));
        Assert.assertFalse(InfosSiteHelper.isHostValid("ftp://www.google.fr"));
        Assert.assertFalse(InfosSiteHelper.isHostValid("test/test"));
        Assert.assertFalse(InfosSiteHelper.isHostValid("#"));
        Assert.assertFalse(InfosSiteHelper.isHostValid("?"));
        Assert.assertFalse(InfosSiteHelper.isHostValid(" test.fr"));
        Assert.assertFalse(InfosSiteHelper.isHostValid("a.b-.co"));

        Assert.assertTrue(InfosSiteHelper.isHostValid("127.0.0.1"));
        Assert.assertTrue(InfosSiteHelper.isHostValid("test.fr"));
        Assert.assertTrue(InfosSiteHelper.isHostValid("sous-domaine.domaine.fr"));
        Assert.assertTrue(InfosSiteHelper.isHostValid("domaine"));
    }
}
