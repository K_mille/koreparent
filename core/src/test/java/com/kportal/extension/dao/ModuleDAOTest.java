package com.kportal.extension.dao;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.kportal.extension.bean.ModuleBean;

/**
 * Created by olivier.camon on 02/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kportal/extension/dao/ModuleDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class ModuleDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    ModuleDAO moduleDAO;

    @Test
    @DatabaseSetup("ModuleDAOTest.add.xml")
    @ExpectedDatabase("ModuleDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final ModuleBean moduleBean = new ModuleBean();
        moduleBean.setIdBean("ID_BEAN2");
        moduleBean.setLibelle("LIBELLE");
        moduleBean.setIdExtension(0L);
        moduleBean.setDateCreation(format.parse("1970-01-01 00:00:00"));
        moduleBean.setDateModification(format.parse("1970-01-01 00:00:00"));
        moduleBean.setEtat(0);
        moduleBean.setType(0);
        moduleDAO.add(moduleBean);
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.add.xml")
    @ExpectedDatabase("ModuleDAOTest.add-expected.xml")
    public void testAddWithForcedId() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final ModuleBean moduleBean = new ModuleBean();
        moduleBean.setId(2L);
        moduleBean.setIdBean("ID_BEAN2");
        moduleBean.setLibelle("LIBELLE");
        moduleBean.setIdExtension(0L);
        moduleBean.setDateCreation(format.parse("1970-01-01 00:00:00"));
        moduleBean.setDateModification(format.parse("1970-01-01 00:00:00"));
        moduleBean.setEtat(0);
        moduleBean.setType(0);
        moduleDAO.addWithForcedId(moduleBean);
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.update.xml")
    @ExpectedDatabase("ModuleDAOTest.update-expected.xml")
    public void testUpdate() {
        final ModuleBean moduleBean = moduleDAO.getById(2L);
        moduleBean.setLibelle("LIBELLE2");
        moduleDAO.update(moduleBean);
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.add-expected.xml")
    @ExpectedDatabase("ModuleDAOTest.add.xml")
    public void testDelete() {
        moduleDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.update.xml")
    public void testById() {
        final ModuleBean moduleBean = moduleDAO.getById(2L);
        Assert.assertNotNull(moduleBean);
        Assert.assertEquals(moduleBean.getIdBean(), "ID_BEAN2", "le bean doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.add-expected.xml")
    @ExpectedDatabase("ModuleDAOTest.deleteAll.xml")
    public void testDeleteByState() {
        moduleDAO.deleteByState(0);
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.add-expected.xml")
    @ExpectedDatabase("ModuleDAOTest.deleteAll.xml")
    public void testDeleteByExtension() {
        moduleDAO.deleteByExtension(0);
    }

    @Test
    @DatabaseSetup("ModuleDAOTest.update.xml")
    public void testByExtensionIdAndType() {
        final List<ModuleBean> moduleBean = moduleDAO.getByExtensionIdAndType(0L, Collections.singletonList(0));
        Assert.assertNotNull(moduleBean);
        Assert.assertEquals(moduleBean.size(), 2, "il doit y avoir deux résultats");
        Assert.assertEquals(moduleBean.get(0).getIdBean(), "ID_BEAN", "le beandoit correspondre à la premère entrée");
    }

}
