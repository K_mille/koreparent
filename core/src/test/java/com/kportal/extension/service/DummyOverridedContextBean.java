package com.kportal.extension.service;

import java.util.List;
import java.util.Locale;

import com.kportal.core.autorisation.Permission;
import com.kportal.core.context.OverridedContextBean;
import com.kportal.extension.IExtension;
import com.kportal.extension.module.IModule;
import com.kportal.util.StatutsMessage;

/**
 * Created by fabien.leconte on 04/01/16.
 */
public class DummyOverridedContextBean implements OverridedContextBean, IModule {

    @Override
    public IExtension getExtension() {
        return null;
    }

    @Override
    public String getLibelleExtension() {
        return null;
    }

    @Override
    public void setLibelleExtension(final String libelle) {
    }

    @Override
    public List<Permission> getPermissions() {
        return null;
    }

    @Override
    public String getId() {
        return null;
    }

    @Override
    public void setId(final String id) {
    }

    @Override
    public void setLibelle(final String Libelle) {
    }

    @Override
    public String getDescription() {
        return "Plop";
    }

    @Override
    public String getLibelle() {
        return null;
    }

    @Override
    public String getLibelleAffichable() {
        return null;
    }

    @Override
    public int getEtat() {
        return 0;
    }

    @Override
    public void setEtat(final int etat) {
    }

    @Override
    public int getType() {
        return 0;
    }

    @Override
    public void setType(final int type) {
    }

    @Override
    public String getMessage(final Locale locale, final String key) {
        return null;
    }

    @Override
    public String getMessage(final String key) {
        return null;
    }

    @Override
    public String getProperty(final String key) {
        return null;
    }

    @Override
    public List<StatutsMessage> getStatuts() {
        return null;
    }

    @Override
    public String getUrl() {
        return null;
    }

    @Override
    public String getIdBean() {
        return null;
    }

    @Override
    public void setIdBean(final String idbean) {
    }

    @Override
    public String getIdExtension() {
        return null;
    }

    @Override
    public void setIdExtension(final String idExtension) {
    }

    @Override
    public String getIdBeanToOverride() {
        return null;
    }

    @Override
    public void setIdBeanToOverride(final String idBean) {
    }

    @Override
    public String getIdExtensionToOverride() {
        return null;
    }

    @Override
    public void setIdExtensionToOverride(final String idExtension) {
    }
}
