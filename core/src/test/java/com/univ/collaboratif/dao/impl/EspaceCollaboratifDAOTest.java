package com.univ.collaboratif.dao.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.collaboratif.bean.EspaceCollaboratifBean;

/**
 * Created by olivier.camon on 02/06/15.
 */

@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/collaboratif/dao/impl/EspaceCollaboratifDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class EspaceCollaboratifDAOTest extends AbstractDbUnitTestngTests {
    
    @Autowired EspaceCollaboratifDAO espaceCollaboratifDAO;


    @Test
    @DatabaseSetup("EspaceCollaboratifDAOTest.add.xml")
    @ExpectedDatabase("EspaceCollaboratifDAOTest.add-expected.xml")
    public void testAdd() throws ParseException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        final EspaceCollaboratifBean espaceCollaboratifBean = new EspaceCollaboratifBean();
        espaceCollaboratifBean.setCode("CODE2");
        espaceCollaboratifBean.setActif("A");
        espaceCollaboratifBean.setTheme("THEME");
        espaceCollaboratifBean.setInscriptionFront("I");
        espaceCollaboratifBean.setIntitule("INTITULE");
        espaceCollaboratifBean.setCodeStructure("CODE_STRUCTURE");
        espaceCollaboratifBean.setGroupesMembres("GROUPES_MEMBRES");
        espaceCollaboratifBean.setRolesMembre("ROLES_MEMBRE");
        espaceCollaboratifBean.setDescription("DESCRIPTION");
        espaceCollaboratifBean.setInscriptionsEnCours("INSCRIPTIONS_EN_COURS");
        espaceCollaboratifBean.setServices("SERVICES");
        espaceCollaboratifBean.setDateCreation(format.parse("1970-01-01 00:00:00"));
        espaceCollaboratifBean.setPeriodiciteNewsletter("P");
        espaceCollaboratifBean.setContenuNewsletter("CONTENU_NEWSLETTER");
        espaceCollaboratifBean.setDateDernierEnvoiNewsletter(format.parse("1970-01-01 00:00:00"));
        espaceCollaboratifBean.setDateEnvoiNewsletter(format.parse("1970-01-01 00:00:00"));
        espaceCollaboratifBean.setGroupesInscription("GROUPES_INSCRIPTION");
        espaceCollaboratifBean.setGroupesConsultation("GROUPES_CONSULTATION");
        espaceCollaboratifBean.setCodeRubrique("CODE_RUBRIQUE");
        espaceCollaboratifBean.setLangue("0");
        espaceCollaboratifBean.setEnteteEspace("ENTETE_ESPACE");
        espaceCollaboratifBean.setPiedEspace("PIED_ESPACE");
        espaceCollaboratifDAO.add(espaceCollaboratifBean);
    }

    @Test
    @DatabaseSetup("EspaceCollaboratifDAOTest.update.xml")
    @ExpectedDatabase("EspaceCollaboratifDAOTest.update-expected.xml")
    public void testUpdate() {
        final EspaceCollaboratifBean espaceCollaboratifBean = espaceCollaboratifDAO.getById(2L);
        Assert.assertNotNull(espaceCollaboratifBean);
        espaceCollaboratifBean.setIntitule("INTITULE2");
        espaceCollaboratifDAO.update(espaceCollaboratifBean);
    }

    @Test
    @DatabaseSetup("EspaceCollaboratifDAOTest.add-expected.xml")
    @ExpectedDatabase("EspaceCollaboratifDAOTest.add.xml")
    public void testDelete() {
        espaceCollaboratifDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("EspaceCollaboratifDAOTest.update.xml")
    public void testById() {
        final EspaceCollaboratifBean espaceCollaboratifBean = espaceCollaboratifDAO.getById(2L);
        Assert.assertNotNull(espaceCollaboratifBean);
        Assert.assertEquals(espaceCollaboratifBean.getCode(), "CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("EspaceCollaboratifDAOTest.update.xml")
    public void testByCode() {
        EspaceCollaboratifBean espaceCollaboratifBean = espaceCollaboratifDAO.getByCode("CODE2");
        Assert.assertNotNull(espaceCollaboratifBean);
        Assert.assertEquals(espaceCollaboratifBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("EspaceCollaboratifDAOTest.update.xml")
    public void testAll() {
        List<EspaceCollaboratifBean> results = espaceCollaboratifDAO.getAll();
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getCode(), "CODE", "le code doit correspondre à la première entrée");
    }

}
