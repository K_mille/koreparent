package com.univ.collaboratif.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.collaboratif.bean.UserRolesCollaboratifBean;

/**
 * Created by olivier.camon on 02/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/collaboratif/dao/impl/UserRolesCollaboratifDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class UserRolesCollaboratifDAOTest extends AbstractDbUnitTestngTests {

    @Autowired UserRolesCollaboratifDAO userRolesCollaboratifDAO;


    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.add.xml")
    @ExpectedDatabase("UserRolesCollaboratifDAOTest.add-expected.xml")
    public void testAdd() {
        final UserRolesCollaboratifBean userRolesCollaboratifBean = new UserRolesCollaboratifBean();
        userRolesCollaboratifBean.setCodeUser("CODE_USER2");
        userRolesCollaboratifBean.setCodeRole("CODE_ROLE");
        userRolesCollaboratifBean.setIdCollaboratif(0L);
        userRolesCollaboratifDAO.add(userRolesCollaboratifBean);
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    @ExpectedDatabase("UserRolesCollaboratifDAOTest.update-expected.xml")
    public void testUpdate() {
        final UserRolesCollaboratifBean userRolesCollaboratifBean = userRolesCollaboratifDAO.getById(2L);
        Assert.assertNotNull(userRolesCollaboratifBean);
        userRolesCollaboratifBean.setCodeRole("CODE_ROLE2");
        userRolesCollaboratifDAO.update(userRolesCollaboratifBean);
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.add-expected.xml")
    @ExpectedDatabase("UserRolesCollaboratifDAOTest.add.xml")
    public void testDelete() {
        userRolesCollaboratifDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    public void testById() {
        final UserRolesCollaboratifBean userRolesCollaboratifBean = userRolesCollaboratifDAO.getById(2L);
        Assert.assertNotNull(userRolesCollaboratifBean);
        Assert.assertEquals(userRolesCollaboratifBean.getCodeUser(), "CODE_USER2", "le code user doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    public void testByIdCollaboratif() {
        final List<UserRolesCollaboratifBean> results = userRolesCollaboratifDAO.getByIdCollaboratif(0L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir tout les résultats");
        Assert.assertEquals(results.get(0).getCodeUser(), "CODE_USER", "le code user doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    public void testByCodeUser() {
        final List<UserRolesCollaboratifBean> results = userRolesCollaboratifDAO.getByCodeUser("CODE_USER2");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir un seul résultat");
        Assert.assertEquals(results.get(0).getCodeUser(), "CODE_USER2", "le code user doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    public void testByCodeRoleAndIdCollab() {
        final List<UserRolesCollaboratifBean> results = userRolesCollaboratifDAO.getByCodeRoleAndIdCollab("CODE_ROLE", 0L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir tout les résultats");
        Assert.assertEquals(results.get(0).getCodeUser(), "CODE_USER", "le code user doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    @ExpectedDatabase("UserRolesCollaboratifDAOTest.deleteAll-expected.xml")
    public void testDeleteByIdCollaboratif() {
        userRolesCollaboratifDAO.deleteByIdCollaboratif(0L);
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.update.xml")
    public void testByCodeUserAndIdCollab() {
        final UserRolesCollaboratifBean userRolesCollaboratifBean = userRolesCollaboratifDAO.getByCodeUserAndIdCollab("CODE_USER2", 0L);
        Assert.assertNotNull(userRolesCollaboratifBean);
        Assert.assertEquals(userRolesCollaboratifBean.getId(), Long.valueOf(2), "le code user doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.add-expected.xml")
    @ExpectedDatabase("UserRolesCollaboratifDAOTest.add.xml")
    public void testDeleteByCodeUser() {
        userRolesCollaboratifDAO.deleteByCodeUser("CODE_USER2");
    }

    @Test
    @DatabaseSetup("UserRolesCollaboratifDAOTest.add-expected.xml")
    @ExpectedDatabase("UserRolesCollaboratifDAOTest.add.xml")
    public void testDeleteByCodeUserAndIdCollab() {
        userRolesCollaboratifDAO.deleteByCodeUserAndIdCollab("CODE_USER2", 0L);
    }
}
