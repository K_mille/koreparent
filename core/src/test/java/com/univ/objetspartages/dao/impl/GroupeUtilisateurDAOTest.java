package com.univ.objetspartages.dao.impl;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.objetspartages.bean.GroupeUtilisateurBean;

/**
 * Created by olivier.camon on 01/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/objetspartages/dao/impl/GroupeUtilisateurDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class GroupeUtilisateurDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    GroupeUtilisateurDAO groupeUtilisateurDAO;

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testAdd() {
        final GroupeUtilisateurBean groupeUtilisateurBean = new GroupeUtilisateurBean();
        groupeUtilisateurBean.setCodeGroupe("CODE_GROUPE2");
        groupeUtilisateurBean.setCodeUtilisateur("CODE_UTILISATEUR");
        groupeUtilisateurBean.setSourceImport("SOURCE_IMPORT");
        groupeUtilisateurDAO.add(groupeUtilisateurBean);
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.update.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.update-expected.xml")
    public void testUpdate() {
        final GroupeUtilisateurBean groupeUtilisateurBean = groupeUtilisateurDAO.getById(2L);
        groupeUtilisateurBean.setCodeUtilisateur("CODE_UTILISATEUR2");
        groupeUtilisateurDAO.update(groupeUtilisateurBean);
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add.xml")
    public void testDelete() {
        groupeUtilisateurDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add.xml")
    public void testDeleteByGroupCode() {
        groupeUtilisateurDAO.deleteByGroupCode("CODE_GROUPE2");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.update-expected.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add.xml")
    public void testDeleteByUserCode() {
        groupeUtilisateurDAO.deleteByUserCode("CODE_UTILISATEUR2");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add.xml")
    public void testDeleteByUserCodeAndGroupCode() {
        groupeUtilisateurDAO.deleteByUserCodeAndGroupCode("CODE_UTILISATEUR", "CODE_GROUPE2");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add.xml")
    public void testDeleteByGroupCodeAndImportSource() {
        groupeUtilisateurDAO.deleteByGroupCodeAndImportSource("CODE_GROUPE2", "SOURCE_IMPORT");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.update-expected.xml")
    @ExpectedDatabase("GroupeUtilisateurDAOTest.add.xml")
    public void testDeleteByUserCodeAndImportSource() {
        groupeUtilisateurDAO.deleteByUserCodeAndImportSource("CODE_UTILISATEUR2", "SOURCE_IMPORT");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByUserCode() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByUserCode("CODE_UTILISATEUR");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByImportSource() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByImportSource("SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByUserCodeAndImportSource() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByUserCodeAndImportSource("CODE_UTILISATEUR", "SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir 2 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByGroupCodeAndImportSource() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByGroupCodeAndImportSource("CODE_GROUPE2", "SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByUserAndGroup() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByUserAndGroup("CODE_UTILISATEUR", "CODE_GROUPE2");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByUserGroupAndImportSource() {
        GroupeUtilisateurBean groupeUtilisateurBean = groupeUtilisateurDAO.selectByUserGroupAndImportSource("CODE_UTILISATEUR", "CODE_GROUPE2", "SOURCE_IMPORT");
        Assert.assertNotNull(groupeUtilisateurBean);
        Assert.assertEquals(groupeUtilisateurBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByGroupCode() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByGroupCode("CODE_GROUPE2");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testInGroupCode() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectInGroupCode(Collections.singletonList("CODE_GROUPE2"));
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("GroupeUtilisateurDAOTest.add-expected.xml")
    public void testByUserNotInGroupCodeAndImportSource() {
        List<GroupeUtilisateurBean> results = groupeUtilisateurDAO.selectByUserNotInGroupCodeAndImportSource("CODE_UTILISATEUR", Collections.singletonList("CODE_GROUPE2"), "SOURCE_IMPORT");
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 1, "il doit y avoir 1 résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

}
