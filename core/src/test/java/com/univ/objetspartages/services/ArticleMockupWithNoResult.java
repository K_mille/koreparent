package com.univ.objetspartages.services;

import mockit.Mock;

/**
 * Created by olivier.camon on 24/12/15.
 */
public class ArticleMockupWithNoResult extends EmptyArticleMockup {

    @Override
    public String getLibelleAffichable() {
        return "LIBELLE";
    }
    @Override
    public String getCode() {
        return "CODE";
    }
    @Override
    public String getLangue() {
        return "0";
    }
    @Mock
    public int selectCodeLangueEtat(String code, String langue, String etat) throws Exception {
        return 0;
    }
}
