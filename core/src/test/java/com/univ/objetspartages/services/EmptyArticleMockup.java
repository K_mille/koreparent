package com.univ.objetspartages.services;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.univ.objetspartages.om.Article;

import mockit.Mock;
import mockit.MockUp;

/**
 * Created on 10/11/2015.
 */
public class EmptyArticleMockup extends MockUp<Article> {

    @Mock
    public Long getIdFiche() {
        return 1L;
    }

    @Mock
    public Long getIdVignette() {
        return 0L;
    }

    @Mock
    public String getCorps() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getReferences() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getContenuEncadre() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getLibelleAffichable() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getMetaDescription() {
        return StringUtils.EMPTY;
    }

    @Mock
    public Long getNbHits() {
        return 0L;
    }

    @Mock
    public String getMetaKeywords() {
        return StringUtils.EMPTY;
    }

    @Mock
    public Date getDateCreation() {
        return null;
    }

    @Mock
    public Date getDateModification() {
        return null;
    }

    @Mock
    public Date getDateProposition() {
        return null;
    }

    @Mock
    public Date getDateValidation() {
        return null;
    }

    @Mock
    public String getCode() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getCodeRattachement() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getCodeRubrique() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getCodeRedacteur() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getCodeValidation() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getLangue() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getEtatObjet() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getDiffusionModeRestriction() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getDiffusionPublicVise() {
        return StringUtils.EMPTY;
    }

    @Mock
    public String getDiffusionPublicViseRestriction() {
        return StringUtils.EMPTY;
    }

}
