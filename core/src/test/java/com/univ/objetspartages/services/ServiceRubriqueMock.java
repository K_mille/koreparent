package com.univ.objetspartages.services;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.kportal.ihm.utils.FrontUtil;
import com.univ.objetspartages.bean.RubriqueBean;

import mockit.Mock;
import mockit.MockUp;

/**
 * Created by olivier.camon on 12/10/15.
 */
public final class ServiceRubriqueMock extends MockUp<ServiceRubrique> {

    public static final String CODE_SITE = "SITE";

    public static final String CODE_LANGUE = "LANGUE";

    public static final String CODE_NAVIGATION = "NAV";

    public static final String CODE_NIVEAU1 = "NIVEAU1";

    public static final String LANGUE_DEFAUT = "0";

    public static RubriqueBean infosrubriqueSitelangueNavNiveau1 = null;

    public static RubriqueBean infosrubriquelangueNavNiveau1 = null;

    public static RubriqueBean infosrubriqueNavNiveau1 = null;

    public static RubriqueBean infosRubriqueNiveau1 = null;

    static {
        infosRubriqueNiveau1 = new RubriqueBean();
        infosRubriqueNiveau1.setCategorie(StringUtils.EMPTY);
        infosRubriqueNiveau1.setCode(CODE_NIVEAU1);
        infosRubriqueNiveau1.setIntitule(CODE_NIVEAU1);
        infosRubriqueNiveau1.setCodeRubriqueMere(CODE_NAVIGATION);
        infosRubriqueNiveau1.setLangue(LANGUE_DEFAUT);
        infosrubriqueNavNiveau1 = new RubriqueBean();
        infosrubriqueNavNiveau1.setCategorie(FrontUtil.CATEGORIE_NAVIGATION);
        infosrubriqueNavNiveau1.setCode(CODE_NAVIGATION);
        infosrubriqueNavNiveau1.setIntitule(CODE_NAVIGATION);
        infosrubriqueNavNiveau1.setCodeRubriqueMere(CODE_LANGUE);
        infosrubriqueNavNiveau1.setLangue(LANGUE_DEFAUT);
        infosrubriquelangueNavNiveau1 = new RubriqueBean();
        infosrubriquelangueNavNiveau1.setCategorie(CODE_LANGUE);
        infosrubriquelangueNavNiveau1.setCode(CODE_LANGUE);
        infosrubriquelangueNavNiveau1.setIntitule(FrontUtil.CATEGORIE_LANGUE);
        infosrubriquelangueNavNiveau1.setCodeRubriqueMere(CODE_SITE);
        infosrubriquelangueNavNiveau1.setLangue(LANGUE_DEFAUT);
        infosrubriqueSitelangueNavNiveau1 = new RubriqueBean();
        infosrubriqueSitelangueNavNiveau1.setCode(CODE_SITE);
        infosrubriqueSitelangueNavNiveau1.setIntitule(CODE_SITE);
        infosrubriqueSitelangueNavNiveau1.setCategorie(StringUtils.EMPTY);
        infosrubriqueSitelangueNavNiveau1.setCodeRubriqueMere(StringUtils.EMPTY);
        infosrubriqueSitelangueNavNiveau1.setLangue(LANGUE_DEFAUT);
    }

    @Mock
    public RubriqueBean getRubriqueByCode(final String code) {
        RubriqueBean result = null;
        if (CODE_SITE.equals(code)) {
            result = infosrubriqueSitelangueNavNiveau1;
        } else if (CODE_LANGUE.equals(code)) {
            result = infosrubriquelangueNavNiveau1;
        } else if (CODE_NAVIGATION.equals(code)) {
            result = infosrubriqueNavNiveau1;
        } else if (CODE_NIVEAU1.equals(code)) {
            result = infosRubriqueNiveau1;
        }
        return result;
    }

    @Mock
    public List<RubriqueBean> getRubriqueByCodeParent(final String codeParent) {
        List<RubriqueBean> result = new ArrayList<>();
        if (CODE_SITE.equals(codeParent)) {
            result = Collections.singletonList(infosrubriquelangueNavNiveau1);
        } else if (CODE_LANGUE.equals(codeParent)) {
            result = Collections.singletonList(infosrubriqueNavNiveau1);
        } else if (CODE_NAVIGATION.equals(codeParent)) {
            result = Collections.singletonList(infosRubriqueNiveau1);
        }
        return result;
    }

    @Mock
    public RubriqueBean getFirstOfCategoryInSubTree(final String rootCode, final String category) {
        if (CODE_LANGUE.equals(category)) {
            return infosrubriquelangueNavNiveau1;
        } else if (CODE_NAVIGATION.equals(category)) {
            return infosrubriqueNavNiveau1;
        }
        return infosrubriqueSitelangueNavNiveau1;
    }
}
