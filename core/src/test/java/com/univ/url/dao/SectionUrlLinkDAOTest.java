package com.univ.url.dao;

import java.util.Collections;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;
import com.univ.url.bean.SectionUrlLinkBean;

/**
 * Created by olivier.camon on 29/10/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/univ/url/dao/SectionUrlLinkDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class SectionUrlLinkDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    SectionUrlLinkDAO sectionUrlLinkDAO;

    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.add.xml")
    @ExpectedDatabase("SectionUrlLinkDAOTest.add-expected.xml")
    public void testAdd() {
        final SectionUrlLinkBean bean = new SectionUrlLinkBean();
        bean.setSectionCode("SECTION_CODE2");
        bean.setIdUrl(1L);
        sectionUrlLinkDAO.add(bean);
    }

    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.add.xml")
    @ExpectedDatabase("SectionUrlLinkDAOTest.add.xml")
    public void testAddAllEmpty() {
        sectionUrlLinkDAO.addAll(Collections.<SectionUrlLinkBean>emptyList());
        sectionUrlLinkDAO.addAll(null);
    }


    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.add-expected.xml")
    @ExpectedDatabase("SectionUrlLinkDAOTest.add.xml")
    public void testDelete() {
        sectionUrlLinkDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.update-expected.xml")
    @ExpectedDatabase("SectionUrlLinkDAOTest.add.xml")
    public void testDeleteAllForSection() {
        sectionUrlLinkDAO.deleteAllForSection("SECTION_CODE2");
    }

    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.deleteAllForSite.xml")
    @ExpectedDatabase("SectionUrlLinkDAOTest.deleteAllForSite-expected.xml")
    public void testDeleteAllForSite() {
        sectionUrlLinkDAO.deleteAllForSite("CODE_SITE");
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.update.xml")
    public void testFill() {
        final SectionUrlLinkBean bean = sectionUrlLinkDAO.getById(2L);
        Assert.assertNotNull(bean);
        Assert.assertEquals(bean.getSectionCode(), "SECTION_CODE2", "le code doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("SectionUrlLinkDAOTest.update.xml")
    @ExpectedDatabase("SectionUrlLinkDAOTest.update-expected.xml")
    public void testUpdate() {
        final SectionUrlLinkBean bean = sectionUrlLinkDAO.getById(2L);
        Assert.assertNotNull(bean);
        bean.setIdUrl(2L);
        sectionUrlLinkDAO.update(bean);
    }


}
