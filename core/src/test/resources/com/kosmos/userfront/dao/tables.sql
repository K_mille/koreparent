CREATE TABLE USER_MODERATION (
  ID_USER_MODERATION     BIGINT (20) NOT NULL AUTO_INCREMENT,
  EMAIL                  VARCHAR(255)     DEFAULT NULL,
  LOGIN                  VARCHAR(64)      DEFAULT NULL,
  FIRST_NAME             VARCHAR(255)     DEFAULT NULL,
  LAST_NAME              VARCHAR(255)     DEFAULT NULL,
  ID_LOCALE_KPORTAL      CHAR(1) NOT NULL DEFAULT '0',
  ALIAS_SITE             VARCHAR(255)     DEFAULT NULL,
  MODERATION_MESSAGE     TEXT,
  MODERATION_STATE       VARCHAR(64)      DEFAULT NULL,
  CREATION_DATE          DATETIME         DEFAULT '1970-01-01 00:00:00',
  PARENT_ID_REGISTRATION BIGINT (20) NOT NULL,
  PRIMARY KEY (ID_USER_MODERATION)
);