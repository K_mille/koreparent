package com.kosmos.externalsite.dao;

import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.kosmos.externalsite.bean.SiteExterneBean;

/**
 * DAO gérant la liste des site externe à indexer
 */
public class SiteExterneDAO extends AbstractCommonDAO<SiteExterneBean> {

    public SiteExterneDAO() {
        tableName = "SITE";
    }

    public List<SiteExterneBean> getAllSite() {
        final List<SiteExterneBean> result;
        try {
            result = namedParameterJdbcTemplate.query("SELECT * FROM SITE ORDER BY CODE", rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query table \"SITE\"", dae);
        }
        return result;
    }

    /**
     * Récupère le site externe à partir de son code.
     * @param code
     * @return
     */
    public SiteExterneBean getByCode(String code) {
        final SiteExterneBean result;
        try {
            final SqlParameterSource namedParameters = new MapSqlParameterSource("code", code);
            result = namedParameterJdbcTemplate.queryForObject(String.format("select * from `%1$s` WHERE CODE = :code", tableName), namedParameters, rowMapper);
        } catch (final DataAccessException dae) {
            throw new DataSourceException("Unable to query table \"SITE\"", dae);
        }
        return result;
    }
}
