package com.kosmos.externalsite.index;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.univ.objetspartages.om.EtatFiche;

/**
 * Created by camille.lebugle on 18/04/17.
 */
public class ExternalSiteSerializer extends StdSerializer<IndexableSiteBean> {

    private static final long serialVersionUID = -7117742488293865049L;

    public ExternalSiteSerializer() {
        super(IndexableSiteBean.class);
    }

    private static String FICHE_NODE = "fiche";

    @Override
    public void serialize(final IndexableSiteBean indexableBean, final JsonGenerator jgen, final SerializerProvider provider) throws IOException {
        if (indexableBean == null) {
            jgen.writeNull();
            return;
        }
        jgen.writeStartObject();
        jgen.writeObjectFieldStart(FICHE_NODE);
        jgen.writeStringField("site_label", indexableBean.getSite());
        jgen.writeStringField("site_code", indexableBean.getCode());
        jgen.writeStringField("titre", indexableBean.getTitre());
        jgen.writeStringField("url", indexableBean.getUrl());
        jgen.writeStringField("site_contenu", indexableBean.getContenu());
        jgen.writeStringField("site_description", indexableBean.getDescription());
        jgen.writeStringField("site_keywords", indexableBean.getKeywords());
        jgen.writeStringField("etatObjet", EtatFiche.EN_LIGNE.getEtat());
        jgen.writeEndObject();
        jgen.writeEndObject();
    }
}
