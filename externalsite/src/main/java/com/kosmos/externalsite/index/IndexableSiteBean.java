package com.kosmos.externalsite.index;

import java.io.Serializable;
import java.util.Date;

/**
 * Created by camille.lebugle on 14/04/17.
 */
public class IndexableSiteBean implements Serializable {

    private static final long serialVersionUID = 800558449621275384L;

    private String site;

    private String code;

    private String titre;

    private String contenu;

    private String description;

    private String rubrique;

    private String url;

    private String etatFiche;

    private Date dateCreation;

    private String keywords;

    public IndexableSiteBean() {
        //Default Constructor
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(final String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(final String contenu) {
        this.contenu = contenu;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getRubrique() {
        return rubrique;
    }

    public void setRubrique(final String rubrique) {
        this.rubrique = rubrique;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public String getSite() {
        return site;
    }

    public void setSite(final String site) {
        this.site = site;
    }

    public String getCode() {
        return code;
    }

    public void setCode(final String code) {
        this.code = code;
    }

    public String getEtatFiche() {
        return etatFiche;
    }

    public void setEtatFiche(final String etatFiche) {
        this.etatFiche = etatFiche;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(final Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public String getKeywords() {
        return keywords;
    }

    public void setKeywords(final String keywords) {
        this.keywords = keywords;
    }
}
