package com.kosmos.externalsite.index;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.lang.CharEncoding;
import com.jsbsoft.jtf.textsearch.RechercheFmt;
import com.kosmos.externalsite.bean.SiteExterneBean;
import com.kosmos.externalsite.service.ServiceIndexationSiteExterne;
import com.kosmos.externalsite.service.ServiceSiteExterne;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.core.config.PropertyHelper;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.utils.Chaine;
import com.univ.xhtml.HTMLParser;

/**
 * Singleton prennant en charge l'aspiration de sites distants, et l'indexation des pages. Code inspire de http://moguntia.ucd.ie/programming/webcrawler/ On a un index par site et
 * par langue. Pour eviter de referencer des pages qui n'existent plus, on repart de zéro.
 *
 * @author jbiard
 */
public class IndexeurSitesDistants {

    private static Logger logger = LoggerFactory.getLogger(IndexeurSitesDistants.class);

    /** The PARA m_ jt f_ nbthreads. */
    public static final String PARAM_JTF_NBTHREADS = "lucene.nb_threads_aspiration";

    /** The PARA m_ jt f_ timeout. */
    public static final String PARAM_JTF_TIMEOUT = "lucene.timeout_threads";

    /** */
    public static final String PARAM_JTF_MAXSIZE_HTMLQUEUE = "lucene.max_size_queue";

    /** */
    public static final String PARAM_JTF_TIME_SLEEP = "lucene.time_sleep";

    /** The N b_ ma x_ thread s_ defaut. */
    public static final int NB_MAX_THREADS_DEFAUT = 4;

    /** The TIMEOU t_ defaut. */
    public static final int TIMEOUT_DEFAUT = 10; // en s

    /** The _instance. */
    private static IndexeurSitesDistants _instance;

    /** The queue sites. */
    private final QueueSiteAIndexer queueSites;


    /** The n niveau courant profondeur. */
    private int nNiveauCourantProfondeur;

    /** The n niveau max profondeur. */
    private int nNiveauMaxProfondeur;

    /** The n nb threads courants. */
    private int nNbThreadsCourants;

    /** The n nb max threads. */
    private int nNbMaxThreads;

    /** The n timeout. */
    private int nTimeout;

    /** Le nombre max de page html contenu dans la queue de page html */
    private int maxSizeQueue;

    /** Le temps en millisecond d'attente du thread lorsque la queue de page est pleinne */
    private long timeToSleep;

    /** The b fin indexation. */
    private boolean bFinAspiration, bFinIndexation;

    /** The queue url. */
    private URLQueue queueURL;

    /** The queue html. */
    private QueueFluxHTML queueHTML;

    /** The pattern url refus. */
    private Pattern patternURLAcceptation, patternURLRefus;

    /** The sz url site. */
    private String szUrlSite;

    /** The asz url disallows. */
    private String[] aszUrlDisallows;

    private ServiceIndexationSiteExterne serviceIndexation;

    /**
     * Constructeur prive. La classe est en fait un singleton.
     */
    private IndexeurSitesDistants() {
        bFinIndexation = true;
        queueSites = new QueueSiteAIndexer();
        serviceIndexation = new ServiceIndexationSiteExterne();
        // lecture du nombre de threads
        final String szNbThreadJTF = PropertyHelper.getCoreProperty(PARAM_JTF_NBTHREADS);
        if (szNbThreadJTF != null) {
            try {
                nNbMaxThreads = Integer.parseInt(szNbThreadJTF);
            } catch (final NumberFormatException e) {
                logger.debug("unable to parse the given value", e);
                nNbMaxThreads = NB_MAX_THREADS_DEFAUT;
            }
        } else {
            nNbMaxThreads = NB_MAX_THREADS_DEFAUT;
        }
        final String szTimeout = PropertyHelper.getCoreProperty(PARAM_JTF_TIMEOUT);
        if (szTimeout != null) {
            try {
                nTimeout = Integer.parseInt(szTimeout);
            } catch (final NumberFormatException e) {
                logger.debug("unable to parse the given value", e);
                nTimeout = TIMEOUT_DEFAUT;
            }
        } else {
            nTimeout = TIMEOUT_DEFAUT;
        }
        final String sMaxSizeQueue = PropertyHelper.getCoreProperty(PARAM_JTF_MAXSIZE_HTMLQUEUE);
        if (sMaxSizeQueue != null) {
            maxSizeQueue = Integer.parseInt(sMaxSizeQueue);
        } else {
            maxSizeQueue = 1000;
        }
        final String sTimeToSleep = PropertyHelper.getCoreProperty(PARAM_JTF_TIME_SLEEP);
        if (sTimeToSleep != null) {
            timeToSleep = Long.parseLong(sTimeToSleep);
        } else {
            timeToSleep = 30000L;
        }
        logger.debug("%%%% Nombre de threads affectes a l'aspiration: " + nNbMaxThreads + " - Timeout: " + nTimeout + "s");
    }

    /**
     * Renvoie l'unique instance (singleton).
     *
     * @return l'instance
     */
    public synchronized static IndexeurSitesDistants getInstance() {
        if (_instance == null) {
            _instance = new IndexeurSitesDistants();
        }
        return _instance;
    }

    public void setLogger(final Logger logger) {
        this.logger = logger;
    }

    /**
     * Lance l'indexation d'un site.
     *
     * @param site
     *            site a indexer
     *
     */
    public void indexeSite(final SiteExterneBean site, final boolean join) {
        try {
            queueSites.push(site);
            lanceIndexations(join);
        } catch (final InterruptedException e) {
            logger.error(e.getMessage(), e);
        }
    }

    /**
     * Retourne pour chacun des sites son etat.
     *
     * @return map dont la clef est le libelle du site
     *
     * @see SiteIndexStates
     */
    public Map<Long, SiteIndexStates> getEtatsSites() {
        return queueSites.getEtats();
    }

    /**
     * Indexe l'ensemble des sites en base.
     *
     */
    public void indexeSites() {
        final ServiceSiteExterne serviceSiteExterne = ServiceManager.getServiceForBean(SiteExterneBean.class);
        List<SiteExterneBean> allSite = serviceSiteExterne.getAllExernalWebSite();
        for (SiteExterneBean site : allSite) {
            indexeSite(site, true);
        }
    }

    /**
     * Indexation en cours.
     *
     * @return true, if successful
     */
    public boolean indexationEnCours() {
        return !bFinIndexation;
    }

    /**
     * Gets the nb threads courants.
     *
     * @return the nb threads courants
     */
    public int getNbThreadsCourants() {
        return nNbThreadsCourants;
    }

    /**
     * Gets the nb max threads.
     *
     * @return the nb max threads
     */
    public int getNbMaxThreads() {
        return nNbMaxThreads;
    }

    /**
     * Gets the niveau courant profondeur.
     *
     * @return the niveau courant profondeur
     */
    public int getNiveauCourantProfondeur() {
        return nNiveauCourantProfondeur;
    }

    /**
     * Gets the niveau max profondeur.
     *
     * @return the niveau max profondeur
     */
    public int getNiveauMaxProfondeur() {
        return nNiveauMaxProfondeur;
    }

    /**
     * Indique si une url doit etre aspiree ou non (suivant le robots.txt)
     *
     * @param szUrl
     *            url a valider ou non
     *
     * @return vrai si l'url est acceptable
     */
    public boolean accepteUrlRobots(final String szUrl) {
        if (aszUrlDisallows == null) {
            return true;
        }
        for (final String aszUrlDisallow : aszUrlDisallows) {
            if (szUrl.contains(aszUrlDisallow)) {
                logger.debug("\t!!URL refusee (robots.txt) : " + szUrl);
                return false;
            }
        }
        return true;
    }

    /**
     * Renvoie le pattern correspondant a l'expression reguliere validant les url.
     *
     * @return pattern, null si non definie
     */
    public Pattern getPatternURLAcceptation() {
        return patternURLAcceptation;
    }

    /**
     * Renvoie le pattern correspondant a l'expression reguliere refusant les url.
     *
     * @return pattern, null si non definie
     */
    public Pattern getPatternURLRefus() {
        return patternURLRefus;
    }

    /**
     * Lance le thread dedie a l'indexation. Permet un traitement en tâche de fond.
     *
     * @throws InterruptedException
     */
    protected void lanceIndexations(final boolean join) throws InterruptedException {
        if (bFinIndexation) {
            bFinIndexation = false;
            final Thread threadIndexation = new Thread() {

                @Override
                public void run() {
                    int nbPages = 0;
                    for (SiteExterneBean site = queueSites.pop(); site != null; site = queueSites.pop()) {
                        Date dateDeb = new Date();
                        logger.info("%%%% Début de l'indexation de " + site.getUrl() + " à " + dateDeb);
                        nbPages = indexe(site);
                        queueSites.finIndexation(site);
                        Date dateFin = new Date();
                        logger.info("%%%% Fin de l'indexation de " + site.getUrl() + " en " + ((dateFin.getTime() - dateDeb.getTime()) / 1000) + "s - " + nbPages + " pages indexées.");
                        RechercheSitesDistants.init();
                    }
                    bFinIndexation = true;
                }
            };
            threadIndexation.start();
            if (join) {
                threadIndexation.join();
            }
        }
    }

    /**
     * Indexe le site : lance les threads prennant en charge l'aspiration et indexe en parallele les pages remontees.
     *
     * @param siteDistant
     *            site a indexer
     *
     * @return nombre de pages indexees
     *
     * @throws PatternSyntaxException
     *             si le pattern n'est pas correct
     * @throws IOException
     *             Signals that an I/O exception has occurred.
     */
    protected int indexe(final SiteExterneBean siteDistant) {
        prepareIndexation(siteDistant);
         // ajout du point d'entree sur le site
        try {
            queueURL.push(new URL(szUrlSite), 0);
            lanceThreadsAspiration();
        } catch (MalformedURLException e) {
            logger.warn("Erreur lors de l'indexation du site avec l'url {}.", szUrlSite, e);
        }
        return indexePagesHTML(siteDistant);
    }

    /**
     * Prepare les elements necessaires a l'indexation.
     *
     * @param siteDistant
     *            site a indexer
     *
     * @return index
     *
     *             levee si la construction du pattern de l'expression reguliere a echoue
     * @throws IOException
     *             levee si probleme d'acces a l'index
     */
    protected void prepareIndexation(final SiteExterneBean siteDistant) {
        this.nNiveauCourantProfondeur = 0;
        this.nNiveauMaxProfondeur = siteDistant.getNiveauProfondeur();
        this.szUrlSite = siteDistant.getUrl();
        this.bFinAspiration = false;
        this.queueHTML = new QueueFluxHTML();
        this.queueURL = new URLQueue();
        queueURL.setMaxElements(-1);
        litRobotsTxt();
        // patterns (threadsafe) pour les expressions regulieres
        // celles-ci ont ete verifiees a la saisie
        String szRegExpUrl = siteDistant.getRegExpAccepte();
        if ((szRegExpUrl != null) && !"".equals(szRegExpUrl)) {
            patternURLAcceptation = Pattern.compile(szRegExpUrl);
        } else {
            patternURLAcceptation = null;
        }
        szRegExpUrl = siteDistant.getRegExpRefuse();
        if ((szRegExpUrl != null) && !"".equals(szRegExpUrl)) {
            patternURLRefus = Pattern.compile(szRegExpUrl);
        } else {
            patternURLRefus = null;
        }
        //Suppression des documents de l'index pour le site.
        serviceIndexation.deleteAllIndexBySite(siteDistant.getCode());
    }

    /**
     * Supprime l'ensemble des index correspondant au site distant passé en paramètre.
     * @param siteDistant le site distant
     */
    public void deleteAllIndexBySite(final SiteExterneBean siteDistant){
        //Suppression des documents de l'index pour le site.
        serviceIndexation.deleteAllIndexBySite(siteDistant.getCode());
    }

    /**
     * Lance les thread sur le site courant. Appelee par les threads lorsqu'ils constatent que le nombre de threads courants est inferieur au nombre maximal.
     */
    protected synchronized void lanceThreadsAspiration() {
        // Lance les threads suivant le maximum autorise, et s'il reste des url
        // pour le niveau courant
        int nNbThreads = nNbMaxThreads - nNbThreadsCourants;
        final int nTailleQueueURL = queueURL.getQueueSize(nNiveauCourantProfondeur);
        if (nTailleQueueURL < nNbThreads || nNbMaxThreads == -1) {
            nNbThreads = nTailleQueueURL;
        }
        // lance les threads nécessaires
        ThreadAspirateur threadAspi;
        for (int n = 1; n <= nNbThreads; ++n) {
            threadAspi = new ThreadAspirateur(this, szUrlSite, nNbThreadsCourants++, nNiveauCourantProfondeur, queueURL, queueHTML, maxSizeQueue, timeToSleep);
            threadAspi.start();
        }
    }

    /**
     * Appelee par les threads pour indiquer qu'ils sont arrives en fin de traitement.
     *
     * @param threadId
     *            identifiant du thread
     */
    protected synchronized void finTraitementThreadAspiration(final int threadId) {
        --nNbThreadsCourants;
        if (nNbThreadsCourants == 0) {
            ++nNiveauCourantProfondeur;
            // si le niveau courant est superieur au niveau maximal
            // ou s'il n'y a pas d'url pour le niveau courant, il est inutile
            // de relancer d'autres threads
            if (nNiveauCourantProfondeur > nNiveauMaxProfondeur) {
                return;
            }
            if (queueURL.getQueueSize(nNiveauCourantProfondeur) == 0) {
                // BUG jamais atteint voir gestion timeout dans indexePagesHTML
                bFinAspiration = true;
                return;
            }
            lanceThreadsAspiration();
        }
    }

    /**
     * Lance l'indexation des pages HTML aspirees. On recree complement l'index a chaque lancement.
     *
     * @param siteDistant
     *            site a indexer
     *
     * @return the int
     *
     * @throws IOException
     *             levee si probleme lors de l'indexation d'un document
     */
    protected int indexePagesHTML(final SiteExterneBean siteDistant) {
        QueueFluxHTML.FluxHTML fluxHTML;
        int nIterationVide = 0;
        int nbPage = 0;
        // on effectue l'indexation jusqu'a ce que la fin du traitement soit
        // effectuee
        while (!bFinAspiration) {
            // recuperation du flux aspire
            fluxHTML = queueHTML.pop();
            if (fluxHTML != null) {
                IndexableSiteBean indexableBean = this.buildIndexableBean(fluxHTML, siteDistant);
                serviceIndexation.saveIndex(indexableBean);
                ++nbPage;
            } else {
                try {
                    // attente qu'une url soit pushee
                    Thread.sleep(1000);
                    // gestion du timeout : pas de timeout sur les connexions +
                    // pb détection fin aspiration
                    if ((queueHTML.getTaille() != 0) || (nNbThreadsCourants != 0)) {
                        nIterationVide = 0;
                    } else {
                        ++nIterationVide;
                    }
                    // si au bout de n iterations on a rien dans la queue : on
                    // s'arrete.
                    if (nIterationVide == nTimeout) {
                        bFinAspiration = true;
                    }
                } catch (final InterruptedException e) {
                    logger.error("Erreur los de l'indexation des pages HTML", e);
                }
            }
        }
        return nbPage;
    }

     /**
     * Cree index.
     *
     * @param fluxHTML
      *            flux html de la page parcourue
      * @param siteExterne
      *            siteExterne à indexer
     *
      * @return {@link IndexableSiteBean} bean à indexer
      **/
     private IndexableSiteBean buildIndexableBean(final QueueFluxHTML.FluxHTML fluxHTML, final SiteExterneBean siteExterne) {
    final HTMLParser htmlParser = new HTMLParser();
    htmlParser.setInputHtml(fluxHTML.getPage());
    String chaine = htmlParser.extractString(false);
    String title = htmlParser.getTitle();
    if (title.trim().length() == 0) {
        title = siteExterne.getLibelle();
    }
    String keywords = htmlParser.getMetaTag("keywords");
    String description = htmlParser.getMetaTag("description");
    chaine = Chaine.encode(chaine, CharEncoding.DEFAULT);
    title = Chaine.encode(title, CharEncoding.DEFAULT);
    keywords = Chaine.encode(keywords, CharEncoding.DEFAULT);
    description = Chaine.encode(description, CharEncoding.DEFAULT);
         final IndexableSiteBean indexableBean = new IndexableSiteBean();
    final String url = fluxHTML.getUrl();
         indexableBean.setSite(siteExterne.getLibelle());
         indexableBean.setCode(siteExterne.getCode());
         indexableBean.setTitre(title);
         indexableBean.setUrl(url);
         indexableBean.setDescription(description);
         indexableBean.setContenu(RechercheFmt.formaterTexteRecherche(chaine, Boolean.FALSE, Boolean.FALSE));
         indexableBean.setEtatFiche(EtatFiche.EN_LIGNE.getEtat());
         indexableBean.setDateCreation(new Date());
         indexableBean.setKeywords(keywords);
         return indexableBean;
    }

    /**
     * Lit le fichier robots.txt.
     */
    protected void litRobotsTxt() {
        try {
            final URL urlRobotsTxt = new URL(szUrlSite.substring(0, szUrlSite.lastIndexOf('/') + 1) + "robots.txt");
            final BufferedReader bReader = new BufferedReader(new InputStreamReader(urlRobotsTxt.openConnection().getInputStream()));
            aszUrlDisallows = HTMLParser.parseRobots(bReader);
        } catch (final IOException e) {
            logger.debug("unable to parse the robots.txt", e);
            aszUrlDisallows = null;
        }
    }



}
