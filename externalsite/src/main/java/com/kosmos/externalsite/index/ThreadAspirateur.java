package com.kosmos.externalsite.index;

import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.univ.xhtml.HTMLParser;

/**
 * Thread prennant en charge l'aspiration d'une page HTML.
 *
 * @author jbiard
 */
public class ThreadAspirateur extends Thread {

    private static final Logger LOG = LoggerFactory.getLogger(ThreadAspirateur.class);

    /** The n niveau courant profondeur. */
    private final int nNiveauCourantProfondeur;

    /** The n id thread. */
    private final int nIdThread;

    /** Le nombre max de page html contenu dans la queue de page html */
    private final int maxSizeQueue;

    /** The queue url. */
    private final URLQueue queueURL;

    /** The queue html. */
    private final QueueFluxHTML queueHTML;

    /** The g threads aspi. */
    private final IndexeurSitesDistants gThreadsAspi;

    /**
     * Le temps en millisecond d'attente du thread lorsque la queue de page est pleinne
     */
    private final long timeToSleep;

    /**
     * Constructeur.
     *
     * @param gThreadsAspi
     * @param szUrlBase
     * @param nIdThread
     * @param nNiveauCourantProfondeur
     * @param queueURL
     * @param queueHTML
     * @param maxSizeQueue
     * @param timeToSleep
     */
    public ThreadAspirateur(final IndexeurSitesDistants gThreadsAspi, final String szUrlBase, final int nIdThread, final int nNiveauCourantProfondeur, final URLQueue queueURL, final QueueFluxHTML queueHTML, final int maxSizeQueue, final long timeToSleep) {
        this.gThreadsAspi = gThreadsAspi;
        this.nIdThread = nIdThread;
        this.nNiveauCourantProfondeur = nNiveauCourantProfondeur;
        this.queueURL = queueURL;
        this.queueHTML = queueHTML;
        this.maxSizeQueue = maxSizeQueue;
        this.timeToSleep = timeToSleep;
    }

    /**
     * Lance l'analyse de l'url.
     */
    @Override
    public void run() {
        // on recupere les url a aspirer
        for (URL url = queueURL.pop(nNiveauCourantProfondeur); url != null; url = queueURL.pop(nNiveauCourantProfondeur)) {
            while (this.queueHTML.getTaille() >= maxSizeQueue) {
                try {
                    Thread.sleep(timeToSleep);
                } catch (final InterruptedException e) {
                    LOG.debug("unable to pause the current thread", e);
                }
            }
            analysePage(url);
            // peut-etre faut-il lancer d'autres threads ?
            if (gThreadsAspi.getNbMaxThreads() > gThreadsAspi.getNbThreadsCourants()) {
                try {
                    gThreadsAspi.lanceThreadsAspiration();
                } catch (final Exception e) {
                    LOG.error("[" + nIdThread + "] ", e);
                }
            }
        }
        // on indique au gestionnaire la fin de l'aspiration
        gThreadsAspi.finTraitementThreadAspiration(nIdThread);
    }

    /**
     * Analyse l'URL : recuperation du flux, extraction des liens hypertextes et ajout de celles-ci dans la queue.
     *
     * @param pageURL
     *            url a analyser
     */
    protected void analysePage(final URL pageURL) {
        // Log.debug( "### [" + nIdThread + "] Début analysePage - URL =" + pageURL );
        try {
            final String szUrlPage = pageURL.toString();
            // on ne parse que du texte
            final URLConnection urlConnection = pageURL.openConnection();
            final int responseCode = ((HttpURLConnection) urlConnection).getResponseCode();
            if (responseCode != 200 && responseCode != 301 && responseCode != 302) {
                LOG.error("Code " + responseCode + " (" + HttpStatus.getStatusText(responseCode) + ") : " + szUrlPage);
                return;
            }
            final String szMimeType = urlConnection.getContentType();
            if (!szMimeType.startsWith("text")) {
                return;
            }
            final HTMLParser htmlParser = new HTMLParser(urlConnection);
            htmlParser.parse();
            final String szPage = htmlParser.getInputHtml();
            // ajout dans la queue des fichiers html a indexer
            queueHTML.push(szPage, StringUtils.defaultIfEmpty(htmlParser.getCanonicalLink(szUrlPage), szUrlPage));
            String szUrlBase = szUrlPage;
            final int nIndex = szUrlPage.lastIndexOf('/');
            if (nIndex > 0) {
                try {
                    szUrlBase = new URL(szUrlPage.substring(0, nIndex + 1)).toString();
                } catch (final MalformedURLException e) {
                    LOG.error("mauvaise URL", e);
                }
            }
            final int nNiveauMaxProfondeur = gThreadsAspi.getNiveauMaxProfondeur();
            if (nNiveauCourantProfondeur <= nNiveauMaxProfondeur || nNiveauMaxProfondeur == -1) {
                final Pattern patternAcceptation = gThreadsAspi.getPatternURLAcceptation();
                final Pattern patternRefus = gThreadsAspi.getPatternURLRefus();
                final List<String> lLiens = htmlParser.extractLinks(szUrlBase);
                // cree l'url pour chaque lien et verifie si elle est conforme
                for (final String szLien : lLiens) {
                    try {
                        ajouteUrl(szLien, pageURL, patternAcceptation, patternRefus);
                    } catch (final MalformedURLException e) {
                        LOG.error("URL invalide : " + szLien, e);
                    }
                }
            }
        } catch (final Exception e) {
            // levee si l'url est invalide
            LOG.warn("url invalide", e);
        }
    }

    /**
     * Ajoute l'url si celle-ci est valide.
     *
     * @param szLien
     *            url du lien
     * @param pageURL
     *            url de la page courante
     * @param patternAcceptation
     *            pattern pour valider
     * @param patternRefus
     *            pattern pour refuser
     *
     * @throws MalformedURLException
     *             levee si l'url est invalide
     */
    protected void ajouteUrl(final String szLien, final URL pageURL, final Pattern patternAcceptation, final Pattern patternRefus) throws MalformedURLException {
        boolean bAjouteLien = false;
        // url peut etre relative
        final URL urlLien = new URL(pageURL, szLien);
        final String szUrlAbsolue = urlLien.toString();
        //Log.debug("### URL absolue: " + urlLien );
        // validation par rapport au robots.txt
        if (!gThreadsAspi.accepteUrlRobots(urlLien.toString())) {
            return;
        }
        if (patternAcceptation != null || patternRefus != null) {
            // on verifie que l'url est acceptable suivant le masque donne (regexp)
            if (patternAcceptation != null) {
                Matcher matcher = patternAcceptation.matcher(szUrlAbsolue);
                bAjouteLien = matcher.matches();
            }
            if (bAjouteLien && patternRefus != null) {
                Matcher matcher = patternRefus.matcher(szUrlAbsolue);
                bAjouteLien = !matcher.matches();
            }
        } else {
            bAjouteLien = true;
        }
        if (bAjouteLien) {
            if (gThreadsAspi.getNiveauMaxProfondeur() == -1) {
                queueURL.push(urlLien, nNiveauCourantProfondeur);
            } else {
                queueURL.push(urlLien, nNiveauCourantProfondeur + 1);
            }
        } else {
            LOG.debug("URL refusee : " + urlLien);
        }
    }
}