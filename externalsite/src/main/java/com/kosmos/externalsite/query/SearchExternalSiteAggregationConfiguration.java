package com.kosmos.externalsite.query;

import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.externalsite.bean.SiteExterneBean;
import com.kosmos.externalsite.service.ServiceSiteExterne;
import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;

/**
 * Created by camille.lebugle on 18/04/17.
 */
public class SearchExternalSiteAggregationConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = 2817517333731952242L;

    @Autowired
    transient private ServiceSiteExterne serviceSiteExterne;

    @Override
    public String getBucketLabelValue(String rawValue) {
        SiteExterneBean siteExterneBean = serviceSiteExterne.getSiteByCode(rawValue);
        if (siteExterneBean != null) {
            return siteExterneBean.getLibelle();
        } else {
            return rawValue;
        }
    }

    ;
}
