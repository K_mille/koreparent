package com.kosmos.externalsite.service;

import org.elasticsearch.index.IndexNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.externalsite.index.IndexableSiteBean;
import com.kosmos.search.index.bean.IndexingCommand;
import com.kosmos.search.index.service.ServiceIndexer;
import com.kosmos.search.index.service.impl.ServiceIndexerIndexerMetatag;
import com.kosmos.search.index.utils.IndexingAction;

/**
 * Created by camille.lebugle on 14/04/17.
 */
public class ServiceIndexationSiteExterne {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceIndexerIndexerMetatag.class);

    private static final String INDEX_NAME = "fiche_externalsite_fr";

    private static final String INDEX_TYPE = "fiche";

    private ServiceIndexer serviceIndexer;

    public ServiceIndexationSiteExterne() {
        this.serviceIndexer = (ServiceIndexer) ApplicationContextManager.getCoreContextBean("serviceIndexer");
    }

    public void saveIndex(final IndexableSiteBean bean) {
        LOG.debug("Indexation de l'url {}", bean.getUrl());
        IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setAction(IndexingAction.CREATE_UPDATE);
        indexingCommand.setIndex(INDEX_NAME);
        indexingCommand.setType(INDEX_TYPE);
        indexingCommand.setDocument(bean);
        this.serviceIndexer.processIndexCommand(indexingCommand);
    }

    /**
     * Suppression des documents de l'index pour le site externe passé en paramètre
     * @param siteCode : code du site à puger de l'index.
     */
    public void deleteAllIndexBySite(final String siteCode) {
        LOG.debug("Suppression de l'index du site ayant le code{}", siteCode);
        IndexingCommand indexingCommand = new IndexingCommand();
        indexingCommand.setAction(IndexingAction.DELETE);
        indexingCommand.setIndex(INDEX_NAME);
        indexingCommand.setType(INDEX_TYPE);
        final String byQuery = String.format("{\"query\": { \"term\": {\"fiche.site_code\":\"%1$s\"}}}", siteCode);
        indexingCommand.setByQuery(byQuery);
        try {
            this.serviceIndexer.processIndexCommand(indexingCommand);
        } catch (IndexNotFoundException e) {
            LOG.warn("Index {} n'existe pas", INDEX_NAME);
        }
    }
}
