package com.kosmos.externalsite.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.externalsite.bean.SiteExterneBean;
import com.kosmos.externalsite.dao.SiteExterneDAO;
import com.kosmos.service.impl.AbstractServiceBean;

/**
 * Service permettant de gerer les {@link SiteExterneBean}
 */
public class ServiceSiteExterne extends AbstractServiceBean<SiteExterneBean, SiteExterneDAO> {

    /**
     * Récupère l'ensemble des sites enregistrer en base de données
     * @return l'ensemble des sites ou une liste vide si rien n'est trouvé.
     */
    public List<SiteExterneBean> getAllExernalWebSite() {
        return dao.getAllSite();
    }

    /**
     * Récupère le site externe à partir de son code
     * @param code
     * @return
     */
    public SiteExterneBean getSiteByCode(String code) {
        if (StringUtils.isNotEmpty(code)) {
            return dao.getByCode(code);
        } else {
            return null;
        }
    }

}
