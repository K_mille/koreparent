package com.kosmos.externalsite.util;

import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

import com.kosmos.externalsite.index.CibleRecherche;
import com.kosmos.externalsite.index.RechercheSitesDistants;
import com.kosmos.service.impl.ServiceManager;
import com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus;
import com.univ.multisites.InfosSite;
import com.univ.multisites.bean.impl.InfosSiteImpl;
import com.univ.utils.ContexteUniv;

/**
 * Created by camille.lebugle on 10/03/17.
 */
public class SiteDistantUtil {

    /**
     * Inserer combo sites.
     *
     * @param ctx
     *            the ctx
     * @param out
     *            the out
     * @param sitesLocaux
     *            the sites locaux
     * @param sitesDistants
     *            the sites distants
     * @param selectedOption
     *            the selected option
     *
     * @throws IOException
     *             the exception
     */
    public static void insererComboSites(final ContexteUniv ctx, final Writer out, final boolean sitesLocaux, final boolean sitesDistants, String selectedOption) throws IOException {
        if (sitesDistants || sitesLocaux) {
            if ("".equals(selectedOption) && ctx.getInfosSite() != null) {
                selectedOption = ctx.getInfosSite().getCodeRubrique();
            }
            out.write("<option value=\"TOUS\">Tous</option>");
            if (sitesLocaux) {
                out.write("<option value=\"\">--------------------</option>");
                final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
                for (InfosSite infosSite : serviceInfosSite.getSitesList().values()) {
                    String selection = "";
                    if (infosSite.getCodeRubrique().equals(selectedOption)) {
                        selection = "selected=\"selected\"";
                    }
                    out.write("<option value=\"" + infosSite.getAlias() + "\" " + selection + ">" + infosSite.getIntitule() + "</option>");
                }
            }
            if (sitesDistants) {
                final RechercheSitesDistants recherche = new RechercheSitesDistants();
                final Collection<CibleRecherche> lstCible = recherche.getLstCible();
                out.write("<option value=\"\">--------------------</option>");
                for (CibleRecherche cible : lstCible) {
                    String selection = "";
                    if (cible.getCode().equals(selectedOption)) {
                        selection = "selected=\"selected\"";
                    }
                    out.write("<option value=\"" + cible.getCode() + "\" " + selection + ">" + cible.getLibelle() + "</option>\n");
                }
            }
        }
    }
}
