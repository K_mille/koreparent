package com.kosmos.externalsite;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ContextConfiguration;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.kosmos.externalsite.bean.SiteExterneBean;
import com.kosmos.externalsite.dao.SiteExterneDAO;
import com.kosmos.externalsite.service.ServiceSiteExterne;
import com.kosmos.tests.testng.AbstractCacheTestngTests;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by camille.lebugle on 03/11/15.
 */
@Test
@ContextConfiguration(locations = {"classpath:/com/kosmos/cache-config/test-context-ehcache.xml", "classpath:ServiceSiteExterneTest.test-context.xml"})
@DirtiesContext
public class ServiceSiteExterneTest extends AbstractCacheTestngTests {

    @Autowired
    ServiceSiteExterne serviceSiteExterne;

    @Test
    public void testGetAllExernalWebSite() {
        final SiteExterneDAO dao = new SiteExterneDAO();
        serviceSiteExterne.setDao(dao);
        new Expectations(SiteExterneDAO.class) {{
            dao.getAllSite();
            result = Arrays.asList(new SiteExterneBean(), new SiteExterneBean());
        }};
        final List<SiteExterneBean> listeSiteExterne = serviceSiteExterne.getAllExernalWebSite();
        Assert.assertTrue(CollectionUtils.isNotEmpty(listeSiteExterne), "La liste retournée ne doit pas être vide");
        Assert.assertEquals(listeSiteExterne.size(), 2, "La liste retournée doit contenir 2 éléments.");
    }
}
