package com.kosmos.layout.card.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonView;
import com.kosmos.components.media.bean.ComponentMediaFile;
import com.kosmos.layout.annotations.MediaContent;
import com.univ.utils.json.Views;

/**
 * Created on 12/12/14.
 */
public class PictureCardBean extends SimpleCardBean {

    @JsonIgnore
    private static final long serialVersionUID = -6292727807736158426L;

    @JsonView({Views.DaoView.class})
    protected String style;

    @JsonView({Views.DaoView.class})
    @MediaContent
    protected ComponentMediaFile picture;

    public PictureCardBean() {
        this.view = "/WEB-INF/jsp/layout/card/fo/view/pictureCardView.jsp";
        this.viewBo = "/adminsite/layout/card/bo/view/pictureCardView.jsp";
        this.editFragment = "/adminsite/layout/card/bo/edit/pictureCardEdit.jsp";
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    public ComponentMediaFile getPicture() {
        return picture;
    }

    public void setPicture(ComponentMediaFile picture) {
        this.picture = picture;
    }
}
