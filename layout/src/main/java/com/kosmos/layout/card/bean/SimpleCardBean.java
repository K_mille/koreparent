package com.kosmos.layout.card.bean;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.univ.utils.json.Views;

@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class SimpleCardBean extends CardBean {

    @JsonIgnore
    private static final long serialVersionUID = -7349440682972704459L;

    protected static final String DEFAULT_COLOR = "#ffffff";

    @JsonView({Views.DaoView.class})
    protected String title;

    @JsonView({Views.DaoView.class})
    protected String backgroundColor = DEFAULT_COLOR;

    @JsonView({Views.DaoView.class})
    protected String foregroundColor;

    @JsonView({Views.DaoView.class})
    protected String link;

    public SimpleCardBean() {
        this.view = "/WEB-INF/jsp/layout/card/fo/view/simpleCardView.jsp";
        this.viewBo = "/adminsite/layout/card/bo/view/simpleCardView.jsp";
        this.editFragment = "/adminsite/layout/card/bo/edit/simpleCardEdit.jsp";
    }

    public String getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(String backGroundColor) {
        this.backgroundColor = backGroundColor;
    }

    public String getForegroundColor() {
        return foregroundColor;
    }

    public void setForegroundColor(String foregroundColor) {
        this.foregroundColor = foregroundColor;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getView() {
        return this.view;
    }

    public void setView(String tileView) {
        this.view = tileView;
    }
}
