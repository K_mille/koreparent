package com.kosmos.layout.card.util;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.utils.LayoutJacksonMapper;
import com.univ.utils.ContexteUtil;
import com.univ.utils.UnivWebFmt;

public class CardUtil {

    /**
     * Méthode utilisée pour obtenir la chaîne JSON correspondant à la CardBean passée en paramètre.
     *
     * @param card the {@link com.kosmos.layout.card.bean.CardBean} to serialize.
     * @return the JSON serialization of the {@link com.kosmos.layout.card.bean.CardBean} "card".
     * @see #getCardFromJson(String)
     */
    public static String getJsonModel(CardBean card) throws IOException {
        if (card != null) {
            return LayoutJacksonMapper.getMapper().writeValueAsString(card);
        }
        return null;
    }

    /**
     * Méthode utilisé pour désérialiser une {@link com.kosmos.layout.card.bean.CardBean} à partir d'une chaîne JSON.
     *
     * @param jsonCard la chaîne à parser
     * @return la {@link com.kosmos.layout.card.bean.CardBean} décrite par la chaîne JSON.
     * @see #getJsonModel(com.kosmos.layout.card.bean.CardBean)
     */
    public static CardBean getCardFromJson(String jsonCard) throws IOException {
        return LayoutJacksonMapper.getMapper().readValue(jsonCard, CardBean.class);
    }

    /**
     * Méthode utilisée pour le calcul de liens issus de composants KmonoSelect.
     *
     * @param link la chaine "code, langue, type | libellé"
     * @return le lien http calculé ou vide si l'url n'a pas pu être calculée.
     */
    public static String getLink(String link) {
        if(StringUtils.isNotBlank(link)) {
            final String[] datas = StringUtils.split(StringUtils.substringBefore(link, "|"), ",");
            if (datas.length == 3) {
                final String code = datas[0];
                final String langue = datas[1].replace("LANGUE=", StringUtils.EMPTY);
                final String objet = datas[2].replace("TYPE=", StringUtils.EMPTY);
                return UnivWebFmt.determinerUrlFiche(ContexteUtil.getContexteUniv(), objet, code, langue, true, ContexteUtil.getContexteUniv().getCodeRubriqueFicheCourante());
            }
        }
        return StringUtils.EMPTY;
    }
}
