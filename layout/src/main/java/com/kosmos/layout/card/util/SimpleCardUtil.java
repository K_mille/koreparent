package com.kosmos.layout.card.util;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.card.bean.SimpleCardBean;

/**
 * Created on 30/12/14.
 */
public class SimpleCardUtil {

    public static String computeDynamicStyle(SimpleCardBean card) {
        final String foregroundColor = StringUtils.isNotBlank(card.getForegroundColor()) ? String.format("color: %s;", card.getForegroundColor()) : StringUtils.EMPTY;
        final String backgroundColor = StringUtils.isNotBlank(card.getBackgroundColor()) ? String.format("background-color: %s;", card.getBackgroundColor()) : StringUtils.EMPTY;
        String style = StringUtils.EMPTY;
        if (StringUtils.isNotBlank(foregroundColor) || StringUtils.isNotBlank(backgroundColor)) {
            style = String.format("style=\"%s%s\"", foregroundColor, backgroundColor);
        }
        return style;
    }
}
