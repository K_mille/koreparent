package com.kosmos.layout.card.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.layout.card.bean.ToolboxCardBean;
import com.kosmos.toolbox.utils.ToolboxHelper;

public class ToolboxCardUtil {

    private static final Logger LOG = LoggerFactory.getLogger(ToolboxCardUtil.class);

    /**
     * Compute content of a toolbox according to the {@link com.kosmos.layout.card.bean.ToolboxCardBean} passed.
     *
     * @param card
     *         as a {@link com.kosmos.layout.card.bean.ToolboxCardBean}
     * @return the content formated as valid HTML or the original value if any error occured.
     */
    public static String getFormatedContent(ToolboxCardBean card) {
        return ToolboxHelper.getFormatedContent(card.getDynamic());
    }
}
