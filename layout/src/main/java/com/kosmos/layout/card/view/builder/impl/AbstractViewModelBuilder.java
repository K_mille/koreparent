package com.kosmos.layout.card.view.builder.impl;

import java.io.IOException;
import java.lang.reflect.ParameterizedType;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.service.ServiceCard;
import com.kosmos.layout.card.util.CardUtil;
import com.kosmos.layout.card.view.builder.ViewModelBuilder;
import com.kosmos.layout.card.view.model.CardViewModel;
import com.kosmos.layout.exception.ViewModelBuilderException;
import com.kosmos.layout.service.ServiceLayout;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 14/05/2015.
 */
public abstract class AbstractViewModelBuilder<C extends CardBean, V extends CardViewModel> implements ViewModelBuilder<C, V> {

    private Class<C> cardClass;

    private Class<V> viewModelClass;

    protected ServiceLayout serviceLayout;

    protected ServiceCard serviceCard;

    public AbstractViewModelBuilder(){
        this.cardClass = (Class<C>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        this.viewModelClass = (Class<V>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[1];
    }

    public void setServiceLayout(ServiceLayout serviceLayout) {
        this.serviceLayout = serviceLayout;
    }

    public void setServiceCard(ServiceCard serviceCard) {
        this.serviceCard = serviceCard;
    }

    @Override
    public Class<C> appliesTo() {
        return cardClass;
    }

    @Override
    public V getViewModel(final C card, final Slot slot, final Layout layout) throws ViewModelBuilderException {
        try {
            V viewModel = viewModelClass.newInstance();
            viewModel = buildViewModel(viewModel, card, slot, layout);
            String jsonCardModel = StringUtils.defaultIfBlank(CardUtil.getJsonModel(card), StringUtils.EMPTY);
            viewModel.setJsonCard(jsonCardModel);
            viewModel.setCardBean(card);
            return viewModel;
        } catch (InstantiationException | IllegalAccessException | IOException e) {
            throw new ViewModelBuilderException("La création du viewModel pour le type " + card.getClass().getName() + " a échoué", e);
        }
    }

    protected abstract V buildViewModel(V viewModel, C card, Slot slot, Layout layout);
}
