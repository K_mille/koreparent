package com.kosmos.layout.card.view.builder.impl;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.SimpleCardBean;
import com.kosmos.layout.card.util.CardUtil;
import com.kosmos.layout.card.util.SimpleCardUtil;
import com.kosmos.layout.card.view.model.SimpleCardViewModel;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 14/05/2015.
 */
public class SimpleCardViewModelBuilder extends AbstractViewModelBuilder<SimpleCardBean, SimpleCardViewModel>{

    @Override
    protected SimpleCardViewModel buildViewModel(SimpleCardViewModel viewModel, SimpleCardBean card, Slot slot, Layout layout) {
        viewModel.setUrl(CardUtil.getLink(card.getLink()));
        viewModel.setDynamicStyle(SimpleCardUtil.computeDynamicStyle(card));
        viewModel.setLink(StringUtils.isNotBlank(viewModel.getUrl()));
        viewModel.setTitle(card.getTitle());
        return viewModel;
    }
}
