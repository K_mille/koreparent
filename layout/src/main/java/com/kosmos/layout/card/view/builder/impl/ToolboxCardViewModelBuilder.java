package com.kosmos.layout.card.view.builder.impl;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.ToolboxCardBean;
import com.kosmos.layout.card.util.ToolboxCardUtil;
import com.kosmos.layout.card.view.model.ToolboxCardViewModel;
import com.kosmos.layout.slot.Slot;

/**
 * Created on 14/05/2015.
 */
public class ToolboxCardViewModelBuilder extends AbstractViewModelBuilder<ToolboxCardBean, ToolboxCardViewModel>{

    @Override
    protected ToolboxCardViewModel buildViewModel(ToolboxCardViewModel viewModel, ToolboxCardBean card, Slot slot, Layout layout) {
        viewModel.setTitle(card.getTitle());
        viewModel.setStyle(card.getStyle());
        viewModel.setContent(ToolboxCardUtil.getFormatedContent(card));
        return viewModel;
    }
}
