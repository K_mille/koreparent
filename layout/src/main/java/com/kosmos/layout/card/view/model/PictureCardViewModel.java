package com.kosmos.layout.card.view.model;

import com.kosmos.layout.card.bean.PictureCardBean;

/**
 * Created on 14/05/2015.
 */
public class PictureCardViewModel extends CardViewModel<PictureCardBean> {

    private static final long serialVersionUID = 4748861892065024692L;

    private String url;

    private String title;

    private String style;

    private String dynamicStyle;

    private String imgSrc;

    private boolean link;

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(final String style) {
        this.style = style;
    }

    public String getDynamicStyle() {
        return dynamicStyle;
    }

    public void setDynamicStyle(String dynamicStyle) {
        this.dynamicStyle = dynamicStyle;
    }

    public String getImgSrc() {
        return imgSrc;
    }

    public void setImgSrc(String imgSrc) {
        this.imgSrc = imgSrc;
    }

    public boolean isLink() {
        return link;
    }

    public void setLink(boolean link) {
        this.link = link;
    }
}
