package com.kosmos.layout.card.view.model;

import com.kosmos.layout.card.bean.ToolboxCardBean;
import com.kportal.cms.objetspartages.annotation.GetterAnnotation;
import com.kportal.cms.objetspartages.annotation.SetterAnnotation;

/**
 * Created on 14/05/2015.
 */
public class ToolboxCardViewModel extends CardViewModel<ToolboxCardBean> {

    private static final long serialVersionUID = 3135335954507487785L;

    private String title;

    private String style;

    private String content;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStyle() {
        return style;
    }

    public void setStyle(String style) {
        this.style = style;
    }

    @GetterAnnotation(isToolbox = true)
    public String getContent() {
        return content;
    }

    @SetterAnnotation(isToolbox = true)
    public void setContent(String content) {
        this.content = content;
    }
}
