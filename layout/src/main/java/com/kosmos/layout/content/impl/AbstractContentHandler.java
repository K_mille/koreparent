package com.kosmos.layout.content.impl;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.content.ContentHandler;
import com.univ.objetspartages.om.FicheUniv;

/**
 * Created by Fabien on 11/11/2014.
 */
public abstract class AbstractContentHandler<T extends Annotation> implements ContentHandler {

    private Class<T> actualClass;

    public AbstractContentHandler() {
        actualClass = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    @Override
    public void prepareContent(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas) {
        prepareModelContent(inputedModel, ficheUniv, datas);
        handleSwitch(inputedModel, ficheUniv, datas);
    }

    @Override
    public int handleContent(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas, final int startIndex) {
        return handleModel(inputedModel, ficheUniv, datas, startIndex);
    }

    protected Collection<Field> getAnnotatedFields(CardBean card) {
        final Collection<Field> annotatedFields = new ArrayList<>();
        final Field[] fields = card.getClass().getDeclaredFields();
        for (Field currentField : fields) {
            if (currentField.isAnnotationPresent(actualClass)) {
                annotatedFields.add(currentField);
            }
        }
        return annotatedFields;
    }

    protected void prepareModelContent(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas) {
    }

    protected int handleModel(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas, final int startIndex) {
        return 0;
    }

    protected void handleSwitch(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas) {
    }
}
