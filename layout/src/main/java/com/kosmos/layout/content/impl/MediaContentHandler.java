package com.kosmos.layout.content.impl;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Map;
import java.util.Objects;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.components.media.bean.ComponentMediaFile;
import com.kosmos.components.media.bean.ComponentMediaFileList;
import com.kosmos.layout.annotations.ContentTypes;
import com.kosmos.layout.annotations.MediaContent;
import com.kosmos.layout.card.bean.CardBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.services.ServiceRessource;
import com.univ.objetspartages.util.RessourceUtils;

/**
 * Created on 11/11/2014.
 */
public class MediaContentHandler extends AbstractContentHandler<MediaContent> {

    private static final Logger LOG = LoggerFactory.getLogger(MediaContentHandler.class);

    private ServiceRessource serviceRessource;

    public void setServiceRessource(final ServiceRessource serviceRessource) {
        this.serviceRessource = serviceRessource;
    }

    @Override
    protected int handleModel(final Map<String, CardBean> inputedModel, final FicheUniv ficheUniv, final Map<String, Object> datas, final int startIndex) {
        final ComponentMediaFileList mediaFiles = getMediaFiles(inputedModel,startIndex);
        int index = startIndex;
        for (ComponentMediaFile currentComponentMediaFile : mediaFiles) {
            synchronizeMedia(currentComponentMediaFile, ficheUniv);
            index++;
        }
        return index;
    }

    @Override
    protected void handleSwitch(final Map<String, CardBean> inputedModel, FicheUniv ficheUniv, final Map<String, Object> datas) {
        final ComponentMediaFileList mediaFiles = getMediaFiles(inputedModel,0); // J'avoue nombre magique parce que je pense que ça va passer
        for (ComponentMediaFile currentComponentMediaFile : mediaFiles) {
            switchRessource(currentComponentMediaFile, ficheUniv);
        }
    }

    private ComponentMediaFileList getMediaFiles(final Map<String, CardBean> inputedModel, int startIndex) {
        final ComponentMediaFileList mediaFiles = new ComponentMediaFileList();
        for (CardBean currentCard : inputedModel.values()) {
            final Collection<Field> fields = getAnnotatedFields(currentCard);
            for (Field currentField : fields) {
                mediaFiles.addAll(addMediaFiles(currentCard, currentField));
            }
        }
        sortMediaFiles(mediaFiles,startIndex);
        return mediaFiles;
    }

    private ComponentMediaFileList addMediaFiles(final CardBean currentCard, final Field currentField) {
        final ComponentMediaFileList mediaFiles = new ComponentMediaFileList();
        final MediaContent contentType = currentField.getAnnotation(MediaContent.class);
        if (contentType.type() == ContentTypes.MEDIA) {
            try {
                currentField.setAccessible(true);
                final Object fieldValue = currentField.get(currentCard);
                if (fieldValue instanceof ComponentMediaFile) {
                    mediaFiles.add((ComponentMediaFile) fieldValue);
                } else if (fieldValue instanceof ComponentMediaFileList) {
                    mediaFiles.addAll((ComponentMediaFileList) fieldValue);
                }
            } catch (IllegalAccessException e) {
                LOG.error(String.format("An error occured trying to handle card %s", currentCard.toString()), e);
            }
        }
        return mediaFiles;
    }

    private void sortMediaFiles(final ComponentMediaFileList mediaFiles, int startIndex) {
        int index = Math.max(getMaxIndex(mediaFiles),startIndex); //La première fois, startIndex sert de point de départ en valeur
        Collections.sort(mediaFiles, new Comparator<ComponentMediaFile>() {
            @Override
            public int compare(ComponentMediaFile o1, ComponentMediaFile o2) {
                return o2.getFileNumber() - o1.getFileNumber();
            }
        });
        for (ComponentMediaFile currentComponentMediaFile : mediaFiles) {
            if (currentComponentMediaFile.getFileNumber() == -1) {
                currentComponentMediaFile.setFileNumber(++index);
            }
        }
    }

    private static int getMaxIndex(ComponentMediaFileList componentMediaFileList) {
        int index = 0;
        for (ComponentMediaFile currentComponentMediaFile : componentMediaFileList) {
            if (index < currentComponentMediaFile.getFileNumber()) {
                index = currentComponentMediaFile.getFileNumber();
            }
        }
        return index;
    }

    private void synchronizeMedia(ComponentMediaFile componentMediaFile, FicheUniv ficheUniv) {
        serviceRessource.syncFile(ficheUniv, componentMediaFile.toString(), componentMediaFile.getFileNumber());
    }

    private void switchRessource(final ComponentMediaFile componentMediaFile, final FicheUniv ficheUniv) {
        final RessourceBean newRessource = serviceRessource.getFile(ficheUniv, Integer.toString(componentMediaFile.getFileNumber()));
        if (newRessource != null && !Objects.equals(newRessource.getId(), componentMediaFile.getId())) {
            componentMediaFile.setId(newRessource.getId());
            componentMediaFile.setFormat(RessourceUtils.getFormat(newRessource));
            componentMediaFile.setLegende(RessourceUtils.getLegende(newRessource));
            componentMediaFile.setTitre(RessourceUtils.getLibelle(newRessource));
        }
    }
}
