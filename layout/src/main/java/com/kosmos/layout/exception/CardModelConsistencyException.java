package com.kosmos.layout.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

/**
 * Created by Fabien on 09/11/2014.
 */
public class CardModelConsistencyException extends ErreurApplicative {

    private static final long serialVersionUID = -2773526251216698341L;

    public CardModelConsistencyException(String mes) {
        super(mes);
    }

    public CardModelConsistencyException(String mes, Throwable t) {
        super(mes, t);
    }
}

