package com.kosmos.layout.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

public class CardNotFoundException extends ErreurApplicative {

    private static final long serialVersionUID = -2773526251216698341L;

    public CardNotFoundException(String mes) {
        super(mes);
    }

    public CardNotFoundException(String mes, Throwable t) {
        super(mes, t);
    }
}
