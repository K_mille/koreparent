package com.kosmos.layout.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

public class DefaultLayoutNotFoundException extends ErreurApplicative {

    private static final long serialVersionUID = -2773526251216698341L;

    public DefaultLayoutNotFoundException(String mes) {
        super(mes);
    }

    public DefaultLayoutNotFoundException(String mes, Throwable t) {
        super(mes, t);
    }
}
