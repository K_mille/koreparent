package com.kosmos.layout.exception;

public class GridNotFoundException extends LayoutNotFoundException {

    private static final long serialVersionUID = -2773526251216698341L;

    public GridNotFoundException(String mes) {
        super(mes);
    }

    public GridNotFoundException(String mes, Throwable t) {
        super(mes, t);
    }
}
