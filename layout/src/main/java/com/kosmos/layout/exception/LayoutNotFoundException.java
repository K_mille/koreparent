package com.kosmos.layout.exception;

import com.jsbsoft.jtf.exception.ErreurApplicative;

public class LayoutNotFoundException extends ErreurApplicative {

    private static final long serialVersionUID = -2773526251216698341L;

    public LayoutNotFoundException(String mes) {
        super(mes);
    }

    public LayoutNotFoundException(String mes, Throwable t) {
        super(mes, t);
    }
}
