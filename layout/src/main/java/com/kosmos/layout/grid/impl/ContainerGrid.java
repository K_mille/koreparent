package com.kosmos.layout.grid.impl;

public class ContainerGrid extends AbstractGrid {

    private static final long serialVersionUID = 197534101759313746L;

    public ContainerGrid() {
        this.viewBo = "/adminsite/layout/grid/bo/containerGrid.jsp";
        this.view = "/WEB-INF/jsp/layout/grid/fo/containerGrid.jsp";
    }


}
