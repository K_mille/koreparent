package com.kosmos.layout.grid.impl;

public class SimpleGrid extends AbstractGrid {

    private static final long serialVersionUID = 197534101759313746L;

    public SimpleGrid() {
        this.viewBo = "/adminsite/layout/grid/bo/simpleGrid.jsp";
        this.view = "/WEB-INF/jsp/layout/grid/fo/simpleGrid.jsp";
    }

}
