package com.kosmos.layout.grid.managers.impl;

import java.util.Map;

import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.grid.managers.GridManager;
import com.kosmos.layout.manager.impl.AbstractLayoutManager;

public abstract class AbstractGridManager<T extends GridLayout> extends AbstractLayoutManager<T> implements GridManager<T> {

    public Map<String, T> getAvailableGrids() {
        return this.getLayouts();
    }

    public T getGridByName(String name) {
        return this.getLayouts().get(name);
    }
}
