package com.kosmos.layout.grid.managers.impl;

import org.springframework.core.annotation.Order;
import org.springframework.integration.annotation.ServiceActivator;

import com.kosmos.layout.grid.impl.ContainerGrid;

public class ContainerGridManager extends AbstractGridManager<ContainerGrid> {

    /**
     * {@inheritDoc}
     */
    @Override
    @ServiceActivator(inputChannel = CHANNEL_MODULE)
    @Order(ORDER_CORE -1)
    public void update() {
        super.update();
    }
}
