package com.kosmos.layout.index;

import java.util.Collection;

import com.kosmos.layout.slot.view.model.SlotViewModel;
import com.kportal.extension.module.bean.PluginIndexableBean;

/**
 * Created by camille.lebugle on 27/03/17.
 */
public class LayoutIndexableBean extends PluginIndexableBean {

    private Collection<SlotViewModel> slotView;

    public Collection<SlotViewModel> getSlotView() {
        return slotView;
    }

    public void setSlotView(final Collection<SlotViewModel> slotView) {
        this.slotView = slotView;
    }
}
