package com.kosmos.layout.manager;

import java.util.Map;

import com.kosmos.layout.Layout;

public interface LayoutManager<T extends Layout> {

    String getId();

    String getName();

    Map<String, T> getLayouts();
}
