package com.kosmos.layout.meta.dao;

import java.sql.Types;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;

import com.jsbsoft.jtf.datasource.dao.impl.mysql.AbstractCommonDAO;
import com.jsbsoft.jtf.datasource.exceptions.DataSourceException;
import com.jsbsoft.jtf.datasource.exceptions.DeleteFromDataSourceException;
import com.kosmos.layout.meta.bean.CardMetaBean;

public class CardMetaDAO extends AbstractCommonDAO<CardMetaBean> {

    private static final Logger LOG = LoggerFactory.getLogger(CardMetaDAO.class);

    public CardMetaDAO() {
        this.tableName = "CARD_META";
    }

    public List<CardMetaBean> getByMeta(Long metaId) throws DataSourceException {
        final List<CardMetaBean> results;
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("idMeta", metaId, Types.BIGINT);
            results = namedParameterJdbcTemplate.query("select * from CARD_META T1 WHERE T1.ID_META = :idMeta", parameters, rowMapper);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return results;
    }

    public CardMetaBean getCardMeta(Long cardId, Long metaId) throws DataSourceException {
        CardMetaBean cardMetaBean = null;
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("idMeta", metaId, Types.BIGINT);
            parameters.addValue("idCard", cardId, Types.BIGINT);
            cardMetaBean = namedParameterJdbcTemplate.queryForObject("select * from CARD_META T1 WHERE T1.ID_CARD = :idCard AND T1.ID_META = :idMeta", parameters, rowMapper);
        } catch (EmptyResultDataAccessException e) {
            LOG.debug(String.format("empty resultset size for cardId %s and metaId %s on table %s", cardId, metaId, tableName), e);
        } catch (IncorrectResultSizeDataAccessException e) {
            LOG.debug("incorrect result size for query", e);
            LOG.info(String.format("incorrect result size  for cardId %s and metaId %s ", cardId, metaId));
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
        return cardMetaBean;
    }

    public void deleteCardMeta(Long cardId, Long metaId) throws DeleteFromDataSourceException {
        try {
            MapSqlParameterSource parameters = new MapSqlParameterSource();
            parameters.addValue("idMeta", metaId, Types.BIGINT);
            parameters.addValue("idCard", cardId, Types.BIGINT);
            namedParameterJdbcTemplate.update("delete from CARD_META WHERE ID_CARD = :idCard AND ID_META = :idMeta", parameters);
        } catch (DataAccessException dae) {
            throw new DataSourceException(String.format("An error occured during selection of rows from table \"%s\"", tableName), dae);
        }
    }
}
