package com.kosmos.layout.plugin;

import java.util.List;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.service.ServiceCard;
import com.kosmos.layout.content.ContentHandler;
import com.kosmos.layout.content.manager.ContentHandlerManager;
import com.kosmos.layout.exception.DefaultLayoutNotFoundException;
import com.kosmos.layout.exception.LayoutNotFoundException;
import com.kosmos.layout.index.LayoutIndexableBean;
import com.kosmos.layout.meta.bean.CardMetaBean;
import com.kosmos.layout.meta.bean.LayoutMetaBean;
import com.kosmos.layout.service.ServiceLayout;
import com.kosmos.layout.view.model.LayoutViewModel;
import com.kportal.core.context.BeanUtil;
import com.kportal.extension.module.bean.PluginIndexableBean;
import com.kportal.extension.module.plugin.objetspartages.DefaultPluginFiche;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivHelper;

public class LayoutFichePlugin extends DefaultPluginFiche {

    private static final Logger LOG = LoggerFactory.getLogger(LayoutFichePlugin.class);

    private ContentHandlerManager contentHandlerManager;

    private ServiceCard serviceCard;

    private ServiceLayout serviceLayout;

    public void setContentHandlerManager(ContentHandlerManager contentHandlerManager) {
        this.contentHandlerManager = contentHandlerManager;
    }

    public void setServiceCard(ServiceCard serviceCard) {
        this.serviceCard = serviceCard;
    }

    public void setServiceLayout(ServiceLayout serviceLayout) {
        this.serviceLayout = serviceLayout;
    }

    @Override
    public void supprimerObjets(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjetCible) throws Exception {
        deleteLayoutDataFromMeta(ficheUniv, meta);
    }

    private void deleteLayoutDataFromMeta(final FicheUniv ficheUniv, final MetatagBean meta) {
        final Map<String, CardBean> cardModel = serviceCard.getCards(meta.getId());
        for (CardBean currentCard : cardModel.values()) {
            serviceCard.deleteCardForMeta(currentCard, meta);
        }
        serviceLayout.deleteLayoutForMeta(meta.getId());
    }

    @Override
    public void preparerPrincipal(final Map<String, Object> datas, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        LayoutFichePluginHelper.prepareBoDatas(datas, ficheUniv);
    }

    @Override
    public void traiterPrincipal(final Map<String, Object> datas, final FicheUniv ficheUniv, final MetatagBean meta) throws Exception {
        final Long idFiche = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("ID_FICHE"), "0"));
        final Long layoutId = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("ID_LAYOUT"), "0"));
        final Long customLayoutId = Long.valueOf(StringUtils.defaultIfBlank((String) datas.get("CUSTOM_LAYOUT"), "0"));
        final int filesStartIndex = Integer.valueOf(StringUtils.defaultIfBlank((String) datas.get("FILES_START_INDEX"),"0"));
        if (!layoutId.equals(0L) && customLayoutId.equals(0L)) {
            traiterCasStandard(layoutId,idFiche,ficheUniv,datas,meta,filesStartIndex);
        } else if (customLayoutId != 0L) {
            traiterCasCustom(customLayoutId,datas,layoutId,idFiche,ficheUniv,meta,filesStartIndex);
        } else {
            // *****Cas pas de layout dans les données******
            // on récupère les informations de la fiche d'origine
            final MetatagBean originalMeta = serviceMetatag.getByCodeAndIdFiche(ReferentielObjets.getCodeObjet(ficheUniv), idFiche);
            if (originalMeta != null) {
                traiterCasModifierFicheGarderLayout(originalMeta,meta,ficheUniv,datas);
            } else {
                traiterCasNouvelleFicheSansLayoutDansDonnees(ficheUniv,meta);
            }
        }
    }

    /**
     *
     * Cas layout standard dans les données. On persiste le model.
     *
     * @param layoutId
     *  l'id du layout tel que  présent dans l'infoBean
     * @param idFiche
     *  l'id de la fiche qui porte le layout tel que présent dans l'infoBean
     * @param ficheUniv
     *  La fiche qui porte le layout
     * @param datas
     *  aka L'infobean
     * @param meta
     *  Le metatagBean associé à la fiche (celui initié par {@link com.univ.objetspartages.processus.ControleurUniv#traiterPRINCIPAL(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.FicheUniv, com.univ.objetspartages.processus.SaisieFiche)}))
     * @param filesStartIndex
     *  Index de début des fichiers (pour lien avec ControleurFichierGW). Nécessaire quand la fiche a déjà des fichiers (pour éviter d'entrer en collision avec les fichiers du layout. tel que  présent dans l'infoBean
     * @throws LayoutNotFoundException
     */
    private void traiterCasStandard(Long layoutId, Long idFiche, FicheUniv ficheUniv, Map<String,Object> datas, MetatagBean meta, int filesStartIndex) throws LayoutNotFoundException {
        // Association du layout à la fiche
        final Layout layout = serviceLayout.getLayout(layoutId);
        // Récupération du modèle saisi
        final boolean resetModelIds = !Objects.equals(ficheUniv.getIdFiche(), idFiche);
        final Map<String, CardBean> inputedModel = LayoutFichePluginHelper.retrieveModelFromDatas(datas, layout, resetModelIds);
        persistModel(layout,meta,ficheUniv,inputedModel,datas,filesStartIndex);
    }

    /**
     * Cas présence d'un layout custom dans les données. On crée le nouveau layout, on l'associe à la fiche et on persiste le modèle.
     * @param customLayoutId
     * L'id Custom qui est égale à l'idLayout mais n'est renseigné que quand le layout a "bougé" tel que  présent dans l'infoBean
     * @param datas
     *  aka L'infobean
     * @param layoutId
     *  l'id du layout tel que  présent dans l'infoBean
     * @param idFiche
     *  l'id de la fiche qui porte le layout tel que  présent dans l'infoBean
     * @param ficheUniv
     *  La fiche qui porte le layout
     * @param meta
     *  Le metatagBean associé à la fiche (celui initié par {@link com.univ.objetspartages.processus.ControleurUniv#traiterPRINCIPAL(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.FicheUniv, com.univ.objetspartages.processus.SaisieFiche)}))
     * @param filesStartIndex
     *  Index de début des fichiers (pour lien avec ControleurFichierGW). Nécessaire quand la fiche a déjà des fichiers (pour éviter d'entrer en collision avec les fichiers du layout. tel que  présent dans l'infoBean
     * @throws LayoutNotFoundException
     */
    private void traiterCasCustom(Long customLayoutId,  Map<String,Object> datas, Long layoutId, Long idFiche, FicheUniv ficheUniv,  MetatagBean meta, int filesStartIndex) throws LayoutNotFoundException {
        // *****Cas Layout custom dans les données******
        final Layout oldLayout = serviceLayout.getLayout(customLayoutId);
        //Recuperation d'une instance similaire
        final Layout newLayout = serviceLayout.getLayout(customLayoutId);
        //Initialisation du nouveau model
        newLayout.setId(0L);
        //Mise à jour du model avec les données de formulaires
        serviceLayout.updateLayoutModel(oldLayout,datas,newLayout);
        serviceLayout.addLayout(newLayout);
        serviceLayout.deleteLayoutWithNoLayoutMeta(layoutId);
        // Récupération du modèle saisi
        final boolean resetModelIds = !Objects.equals(ficheUniv.getIdFiche(), idFiche);
        final Map<String, CardBean> inputedModel = LayoutFichePluginHelper.retrieveModelFromDatas(datas, newLayout, resetModelIds);
        persistModel(newLayout,meta,ficheUniv,inputedModel,datas,filesStartIndex);
    }

    /**
     * Cas la fiche d'origine a un layout et on le conserve mais on n'a pas d'idLayout dans les données de l'infoBean. Cela arrive principalement quand on a le layout sur un autre onglet que le principal
     * et quand on modifie la fiche en ne touchant pas à l'onglet qui à le layout. Il faut donc aller chercher en base le layout associé à la fiche.
     * @param originalMeta
     *  Le metatagBean associé à la fiche (celui récupéré en base)
     * @param meta
     *  Le metatagBean associé à la fiche (celui initié par {@link com.univ.objetspartages.processus.ControleurUniv#traiterPRINCIPAL(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.FicheUniv, com.univ.objetspartages.processus.SaisieFiche)}))
     * @param ficheUniv
     *  La fiche qui a le layout
     * @param datas
     *  aka l'infobean
     * @throws Exception
     */
    private void traiterCasModifierFicheGarderLayout(MetatagBean originalMeta, MetatagBean meta, FicheUniv ficheUniv, Map<String,Object> datas) throws Exception {
        // *****Cas la fiche d'origine a un layout et on le conserve******
        final FicheUniv originalFiche = FicheUnivHelper.getFicheParIdMeta(originalMeta.getId());
        final Layout originalLayout = serviceLayout.getLayout(originalFiche);
        final Map<String, CardBean> originalCards = serviceCard.getCards(originalFiche);
        // On reset les ids pour que ça duplique les cards plutôt que de les modifier... c'est moche
        for (CardBean cardBean : originalCards.values()) {
            cardBean.setId(null);
        }
        persistModel(originalLayout, meta, ficheUniv, originalCards, datas, 0);
    }

    /**
     * Cas la fiche est nouvelle et on ne passe pas sur l'onglet avec le layout, du coup pas de layout dans les données. On affecte donc le layout pas défaut de la fiche. Layout récupéré par la méthode {@link ServiceLayout#getLayout(FicheUniv)}
     * @param ficheUniv
     * La fiche qui a le layout
     * @param meta
     *  Le metatagBean associé à la fiche (celui initié par {@link com.univ.objetspartages.processus.ControleurUniv#traiterPRINCIPAL(com.jsbsoft.jtf.core.InfoBean, com.univ.objetspartages.om.FicheUniv, com.univ.objetspartages.processus.SaisieFiche)}))
     * @throws LayoutNotFoundException
     * @throws DefaultLayoutNotFoundException
     */
    private void traiterCasNouvelleFicheSansLayoutDansDonnees(FicheUniv ficheUniv, MetatagBean meta) throws LayoutNotFoundException, DefaultLayoutNotFoundException {
        //Récupérationd du layout défaut de la fiche
        Layout layout =  serviceLayout.getLayout(ficheUniv);
        if(layout != null) {
            //Si on trouve un layout, on crée un méta
            //Pas besoin de persister un model vu qu'il n'y a aucune donnée saisie
            serviceLayout.addLayoutForMeta(layout, meta.getId());
        }
    }


    private void persistModel(Layout layout, MetatagBean meta, FicheUniv ficheUniv, Map<String, CardBean> inputedModel , Map<String, Object> datas, int filesStartIndex){
        serviceLayout.addLayoutForMeta(layout, meta.getId());
        // Nettoyage du modèle eventuellement présent par rapport au modèle saisi
        serviceCard.cleanUpModel(ficheUniv, inputedModel);
        // Sauvegarde du modèle et des références vers la fiche
        handleContent(inputedModel, ficheUniv, datas, filesStartIndex);
        for (Map.Entry<String, CardBean> currentEntry : inputedModel.entrySet()) {
            // Prise en charge du contenu à partir des "ContentHandler" définis
            serviceCard.addCardForMeta(currentEntry.getValue(), meta);
        }
    }

    private void handleContent(Map<String, CardBean> inputedModel, FicheUniv ficheUniv, Map<String, Object> datas, int filesStartIndex) {
        int startIndex = filesStartIndex;
        for (ContentHandler currentHandler : contentHandlerManager.getContentHandlers()) {
            startIndex += currentHandler.handleContent(inputedModel, ficheUniv, datas, startIndex);
        }
    }

    @Override
    public void setDataContexteUniv(final FicheUniv ficheUniv, final MetatagBean meta, final String classeObjet) throws Exception {
        LayoutFichePluginHelper.prepareFoDatas(ContexteUtil.getContexteUniv().getDatas(), ficheUniv);
    }

    @Override
    public Object getDataContexteUniv() {
        return ContexteUtil.getContexteUniv().getData(BeanUtil.getBeanKey(getId(), getIdExtension()));
    }

    @Override
    public void apresMiseEnLigne(final FicheUniv ficheUniv, final Long idMetaLigne, final Long oldIdMeta, final String classeObjet) {
        //On bascule les cartes du meta de la fiche sur les metas de la fiche en ligne
        if (!idMetaLigne.equals(0L)) {
            //Il existe déjà une fiche en ligne
            MetatagBean oldMeta = serviceMetatag.getById(oldIdMeta);
            MetatagBean metaLigne = serviceMetatag.getById(idMetaLigne);
            //On supprime les anciennes données liées à la fiche en ligne
            deleteLayoutDataFromMeta(ficheUniv, metaLigne);
            //On associe ensuite les données du layout à la fiche en ligne
            try {
                Layout layout = serviceLayout.getLayoutForMeta(oldIdMeta);
                if (layout != null) {
                    LayoutMetaBean layoutMeta = serviceLayout.getLayoutMetaBean(layout, oldMeta);
                    if (layoutMeta != null) {
                        layoutMeta.setIdMeta(idMetaLigne);
                        serviceLayout.updateLayoutMeta(layoutMeta);
                    }
                    final List<CardMetaBean> cardMetaList = serviceCard.getCardMetaBeanFromIdMeta(oldIdMeta);
                    for (CardMetaBean cardMeta : cardMetaList) {
                        cardMeta.setIdMeta(idMetaLigne);
                        serviceCard.updateCardMetaBean(cardMeta);
                    }
                }
            } catch (LayoutNotFoundException lnfe) {
                LOG.warn("layout not found for idMeta {}", oldIdMeta, lnfe);
            }
        }
    }

    @Override
    public PluginIndexableBean generatePluginIndex(MetatagBean meta) {
        LayoutIndexableBean indexableBean = null;
        try {
            LayoutViewModel layoutViewModel = LayoutFichePluginHelper.prepareViewModelForIndex(meta);
            if (layoutViewModel != null) {
                indexableBean = new LayoutIndexableBean();
                indexableBean.setType(this.id);
                indexableBean.setSlotView(layoutViewModel.getSlotViewModels().values());
            }
        } catch (JsonProcessingException | LayoutNotFoundException e) {
            LOG.warn("error during layout indexation for meta with id {}", meta.getId(), e);
        }
        return indexableBean;
    }
}
