package com.kosmos.layout.plugin;

import java.io.IOException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.exception.ErreurApplicative;
import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.service.ServiceCard;
import com.kosmos.layout.card.util.CardList;
import com.kosmos.layout.card.util.CardUtil;
import com.kosmos.layout.card.view.builder.ViewModelBuilder;
import com.kosmos.layout.card.view.builder.manager.ViewModelBuilderManager;
import com.kosmos.layout.card.view.model.CardViewModel;
import com.kosmos.layout.content.ContentHandler;
import com.kosmos.layout.content.manager.ContentHandlerManager;
import com.kosmos.layout.exception.LayoutNotFoundException;
import com.kosmos.layout.exception.ViewModelBuilderException;
import com.kosmos.layout.service.ServiceLayout;
import com.kosmos.layout.slot.Slot;
import com.kosmos.layout.slot.util.SlotState;
import com.kosmos.layout.slot.view.model.SlotViewModel;
import com.kosmos.layout.utils.LayoutJacksonMapper;
import com.kosmos.layout.view.model.LayoutManagerViewModel;
import com.kosmos.layout.view.model.LayoutViewModel;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.utils.EscapeString;

/**
 * Created on 13/11/14.
 */
public class LayoutFichePluginHelper {

    public static final String LAYOUT_VIEW_MODEL = "LAYOUT_VIEW_MODEL";

    public static final String LAYOUT_MANAGER_VIEW_MODEL = "LAYOUT_MANAGER_VIEW_MODEL";

    public static final String CARD_VIEW_MODEL = "CARD_VIEW_MODEL";

    public static final String SLOT_VIEW_MODEL = "SLOT_VIEW_MODEL";

    private static final Logger LOG = LoggerFactory.getLogger(LayoutFichePluginHelper.class);

    private static ContentHandlerManager getContentHandlerManager() {
        return ApplicationContextManager.getBean("core", ContentHandlerManager.ID_BEAN, ContentHandlerManager.class);
    }

    private static ViewModelBuilderManager getViewModelBuilderManager(){
        return ApplicationContextManager.getBean("core", ViewModelBuilderManager.ID_BEAN, ViewModelBuilderManager.class);
    }

    private static ServiceCard getServiceCard(){
        return ServiceManager.getServiceForBean(CardBean.class);
    }

    private static ServiceLayout getServiceLayout(){
        return ServiceManager.getServiceForBean(Layout.class);
    }

    public static void prepareBoDatas(final Map<String, Object> datas, final FicheUniv ficheUniv) throws JsonProcessingException, ErreurApplicative {
        final Layout layout = getServiceLayout().getLayout(ficheUniv);
        prepareBoDatas(datas, ficheUniv, layout);
    }

    public static void prepareBoDatas(final Map<String, Object> datas, final FicheUniv ficheUniv, final Long layoutId) throws JsonProcessingException, LayoutNotFoundException {
        final Layout layout = getServiceLayout().getLayout(layoutId);
        prepareBoDatas(datas, ficheUniv, layout);
    }

    public static void prepareBoDatas(final Map<String, Object> data, final FicheUniv ficheUniv, final Layout layout) throws JsonProcessingException {
        if (layout != null) {
            final Map<String, CardBean> cardModel = getServiceCard().getCards(ficheUniv);
            prepareBoDatas(data, ficheUniv, layout, cardModel);
        }
    }

    public static void prepareBoDatas(final Map<String, Object> datas, final FicheUniv ficheUniv, final Layout layout, Map<String, CardBean> cardModel) throws JsonProcessingException {
        if (layout != null) {
            prepareContent(cardModel, ficheUniv, datas);
            getServiceCard().updateCardModel(cardModel);
            assertModelConsistency(layout, cardModel);
            final LayoutViewModel layoutViewModel = prepareLayoutViewModel(layout, cardModel);
            final LayoutManagerViewModel layoutManagerViewModel = prepareLayoutManagerViewModel(layout,ficheUniv.getClass());
            layoutManagerViewModel.setLayoutViewModel(layoutViewModel);
            datas.put(LAYOUT_MANAGER_VIEW_MODEL, layoutManagerViewModel);
        }
    }

    protected static void prepareFoDatas(final Map<String, Object> datas, final FicheUniv ficheUniv) throws ErreurApplicative, JsonProcessingException {
        final Layout layout = getServiceLayout().getLayout(ficheUniv);
        if (layout != null) {
            final Map<String, CardBean> cardModel = getServiceCard().getCards(ficheUniv);
            prepareContent(cardModel, ficheUniv, datas);
            getServiceCard().updateCardModel(cardModel);
            assertModelConsistency(layout, cardModel);
            final LayoutViewModel layoutViewModel = prepareLayoutViewModel(layout, cardModel);
            datas.put(LAYOUT_VIEW_MODEL, layoutViewModel);
        }
    }

    private static LayoutManagerViewModel prepareLayoutManagerViewModel(final Layout layout, final Class<?> clazz) throws JsonProcessingException {
        final LayoutManagerViewModel layoutManagerViewModel = new LayoutManagerViewModel();
        final Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes = getServiceCard().getAllowedCardTypes(layout);
        final CardList cardPool = getServiceCard().getAllowedCardsPool(allowedCardTypes);
        final String jsonCardPool = LayoutJacksonMapper.getMapper().writeValueAsString(cardPool);
        layoutManagerViewModel.setCardBoViewPool(getServiceCard().getAllowedCardBoView(allowedCardTypes));
        layoutManagerViewModel.setAvailableLayouts(getServiceLayout().getAvailableLayouts(clazz));
        layoutManagerViewModel.setCardsPool(EscapeString.escapeAttributHtml(jsonCardPool));
        layoutManagerViewModel.setEditFragments(getServiceCard().getAllowedEditFragments(cardPool));
        layoutManagerViewModel.setCardsDescriptions(getServiceCard().getAllowedCardsDescriptions(layout));
        layoutManagerViewModel.setAllowedCardTypes(allowedCardTypes);
        return layoutManagerViewModel;
    }

    private static LayoutViewModel prepareLayoutViewModel(final Layout layout, Map<String, CardBean> cardModel) throws JsonProcessingException {
        final LayoutViewModel layoutViewModel = new LayoutViewModel();
        layoutViewModel.setLayout(layout);
        layoutViewModel.setSlotViewModels(prepareSlotViewModels(layout, cardModel));
        return layoutViewModel;
    }

    private static Map<String, SlotViewModel> prepareSlotViewModels(final Layout layout, final Map<String, CardBean> cards) throws JsonProcessingException {
        final Map<String, SlotViewModel> slotViewModels = new HashMap<>();
        final Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes = getServiceCard().getAllowedCardTypes(layout);
        for (Slot currentSlot : layout.getSlots()) {
            final String currentKey = currentSlot.getKey().toString();
            final SlotViewModel currentSlotViewModel = new SlotViewModel();
            final String jsonAllowedCardTypes = LayoutJacksonMapper.getMapper().writeValueAsString(allowedCardTypes.get(currentKey));
            final CardViewModel cardViewModel = prepareCardViewModel(cards.get(currentKey), currentSlot, layout);
            currentSlot.setState(cardViewModel == null || cardViewModel.getCardBean() == null ? SlotState.EMPTY : SlotState.FILLED);
            currentSlotViewModel.setSlot(currentSlot);
            currentSlotViewModel.setAllowedCardTypes(EscapeString.escapeAttributHtml(jsonAllowedCardTypes));
            currentSlotViewModel.setCardViewModel(cardViewModel);
            slotViewModels.put(currentKey, currentSlotViewModel);
        }
        return slotViewModels;
    }

    private static CardViewModel prepareCardViewModel(final CardBean card, final Slot slot, final Layout layout) {
        final ViewModelBuilder builder = getViewModelBuilderManager().getBuilderForCard(card);
        try {
            return builder.getViewModel(card, slot, layout);
        } catch (ViewModelBuilderException e) {
            LOG.error("Une erreur est survenue lors de la préparation du modèle de vue", e);
        }
        return null;
    }

    private static void assertModelConsistency(Layout layout, Map<String, CardBean> cardModel) {
        for (Slot currentSlot : layout.getSlots()) {
            final String currentKey = currentSlot.getKey().toString();
            if (cardModel.containsKey(currentKey)) {
                final CardBean currentCard = cardModel.get(currentKey);
                if (CollectionUtils.isNotEmpty(currentSlot.getAllowedCardTypes()) && !currentSlot.getAllowedCardTypes().contains(currentCard.getClass())) {
                    cardModel.remove(currentKey);
                }
            }
        }
    }

    // Permet de gérér les changements de ressources qui interviennent lors des changements d'états de fiche
    // ex : media de toolbox et media
    private static void prepareContent(Map<String, CardBean> cards, FicheUniv ficheUniv, Map<String, Object> datas) {
        for (ContentHandler currentHandler : getContentHandlerManager().getContentHandlers()) {
            currentHandler.prepareContent(cards, ficheUniv, datas);
        }
    }

    public static Map<String, CardBean> retrieveModelFromDatas(Map<String, Object> datas, Layout layout, boolean resetIds) {
        final Map<String, CardBean> inputedModel = new HashMap<>();
        for (Slot currentSlot : layout.getSlots()) {
            final String currentModel = (String) datas.get(String.format("jsonModel-%s", currentSlot.getKey().toString()));
            if (StringUtils.isNotBlank(currentModel)) {
                final CardBean currentInputedCard;
                try {
                    currentInputedCard = CardUtil.getCardFromJson(currentModel);
                    if (resetIds) {
                        currentInputedCard.setId(null);
                    }
                    inputedModel.put(currentInputedCard.getKey().toString(), currentInputedCard);
                } catch (IOException e) {
                    LOG.error(String.format("Une erreur est survenue lors de la récupération de la carte depuis le json \"%s\"", currentModel), e);
                }
            }
        }
        return inputedModel;
    }

    public static void propagateInputedModel(Map<String, Object> datas, Long layoutId, FicheUniv ficheUniv) throws JsonProcessingException, LayoutNotFoundException {
        final Layout layout = getServiceLayout().getLayout(layoutId);
        final Map<String, CardBean> cardModel = retrieveModelFromDatas(datas, layout, false);
        final LayoutViewModel layoutViewModel = prepareLayoutViewModel(layout, cardModel);
        final LayoutManagerViewModel layoutManagerViewModel = prepareLayoutManagerViewModel(layout,ficheUniv.getClass());
        layoutManagerViewModel.setLayoutViewModel(layoutViewModel);
        //Traitemement des cas où le layout est custom (ordre et position des cartes)
        if(StringUtils.isNotBlank("CUSTOM_LAYOUT") && StringUtils.isNumeric((String) datas.get("CUSTOM_LAYOUT"))){
            final Long customLayoutId = Long.parseLong((String) datas.get("CUSTOM_LAYOUT"));
            if (customLayoutId != 0L) {
                ServiceLayout serviceLayout = (ServiceLayout) ApplicationContextManager.getCoreContextBean("serviceLayout");
                //Case custom layout
                final Layout oldLayout = serviceLayout.getLayout(customLayoutId);
                //Recuperation d'une instance similaire
                final Layout newLayout = serviceLayout.getLayout(customLayoutId);
                //Mise à jour du model avec les données de formulaires
                serviceLayout.updateLayoutModel(oldLayout,datas,newLayout);
                newLayout.setCustomId(customLayoutId);
                //On remet à jour le layout view model
                layoutManagerViewModel.getLayoutViewModel().setLayout(newLayout);
                layoutManagerViewModel.getLayoutViewModel().setSlotViewModels(prepareSlotViewModels(newLayout, cardModel));
            }
        }

        datas.put(LAYOUT_MANAGER_VIEW_MODEL, layoutManagerViewModel);
    }

    /**
     * Préparation du modèle pour indexation à partir du metatag
     * @param meta
     * @return
     * @throws JsonProcessingException
     * @throws LayoutNotFoundException
     */
    public static LayoutViewModel prepareViewModelForIndex(MetatagBean meta) throws JsonProcessingException, LayoutNotFoundException {
        Layout layout = getServiceLayout().getLayoutForMeta(meta.getId());
        final Map<String, CardBean> mapCard = getServiceCard().getCards(meta.getId());
        return prepareLayoutViewModel(layout, mapCard);
    }
}
