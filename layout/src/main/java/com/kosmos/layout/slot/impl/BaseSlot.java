package com.kosmos.layout.slot.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonView;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.slot.Slot;
import com.kosmos.layout.slot.util.SlotState;
import com.univ.utils.json.Views;

/**
 * Created by fabien.leconte on 08/12/14.
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS, include = JsonTypeInfo.As.PROPERTY, property = "class")
public class BaseSlot implements Slot {

    private static final long serialVersionUID = 6322019311465965263L;

    @JsonIgnore
    protected String view;

    @JsonIgnore
    protected String viewBo;

    @JsonView({Views.DaoView.class})
    private Collection<Class<? extends CardBean>> allowedCardTypes;

    @JsonView({Views.DaoView.class})
    private UUID key;

    @JsonView({Views.DaoView.class})
    private SlotState state = SlotState.EMPTY;

    @JsonView(Views.DaoView.class)
    private String description;

    public BaseSlot() {
        allowedCardTypes = new ArrayList<>();
        viewBo = "/adminsite/layout/slot/bo/view/slotView.jsp";
        view = "/WEB-INF/jsp/layout/slot/fo/view/slotView.jsp";
    }

    @Override
    public Collection<Class<? extends CardBean>> getAllowedCardTypes() {
        return allowedCardTypes;
    }

    public void setAllowedCardTypes(Collection<Class<? extends CardBean>> allowedCardTypes) {
        this.allowedCardTypes = allowedCardTypes;
    }

    @Override
    public UUID getKey() {
        return key;
    }

    public void setKey(UUID key) {
        this.key = key;
    }

    @Override
    public SlotState getState() {
        return state;
    }

    @Override
    public void setState(SlotState state) {
        this.state = state;
    }

    @Override
    public String getView() {
        return view;
    }

    @Override
    public String getViewBo() {
        return viewBo;
    }

    @Override
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
