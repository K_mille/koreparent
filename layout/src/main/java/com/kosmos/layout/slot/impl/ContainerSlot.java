package com.kosmos.layout.slot.impl;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public class ContainerSlot extends GridSlot {

    private static final long serialVersionUID = 8649906437392779317L;

    public ContainerSlot() {
        this.viewBo = "/adminsite/layout/slot/bo/view/containerSlotView.jsp";
        this.view = "/WEB-INF/jsp/layout/slot/fo/view/containerSlotView.jsp";
    }

}
