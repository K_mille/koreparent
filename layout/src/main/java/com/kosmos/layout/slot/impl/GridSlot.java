package com.kosmos.layout.slot.impl;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public class GridSlot extends BaseSlot {

    private static final long serialVersionUID = 8649906437392779317L;

    private int row;

    private int rowSpan;

    private int column = 1;

    private int colSpan = 1;

    public GridSlot() {
        this.viewBo = "/adminsite/layout/slot/bo/view/gridSlotView.jsp";
        this.view = "/WEB-INF/jsp/layout/slot/fo/view/gridSlotView.jsp";
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public int getRowSpan() {
        return rowSpan;
    }

    public void setRowSpan(int rowSpan) {
        this.rowSpan = rowSpan;
    }

    public int getColumn() {
        return column;
    }

    public void setColumn(int column) {
        this.column = column;
    }

    public int getColSpan() {
        return colSpan;
    }

    public void setColSpan(int colSpan) {
        this.colSpan = colSpan;
    }
}
