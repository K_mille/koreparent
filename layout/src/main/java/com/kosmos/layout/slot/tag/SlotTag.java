package com.kosmos.layout.slot.tag;

import javax.servlet.jsp.JspException;

import com.kosmos.layout.plugin.LayoutFichePluginHelper;
import com.kosmos.layout.slot.view.model.SlotViewModel;
import com.kosmos.layout.tag.AbstractLayoutTag;

public class SlotTag extends AbstractLayoutTag<SlotViewModel> {

    private static final long serialVersionUID = -2967702308590896379L;

    public SlotTag() {
        retrieveKey = LayoutFichePluginHelper.SLOT_VIEW_MODEL;
    }

    @Override
    public int doStartTag() throws JspException {
        super.doStartTag();
        includeJSP(front ? viewModel.getSlot().getView() : viewModel.getSlot().getViewBo());
        return 0;
    }
}
