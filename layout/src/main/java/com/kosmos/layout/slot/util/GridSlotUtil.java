package com.kosmos.layout.slot.util;

import org.apache.commons.lang3.StringUtils;

import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.slot.Slot;
import com.kosmos.layout.slot.impl.GridSlot;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public class GridSlotUtil {

    public static String getSlotKey(GridLayout grid, int row, int column) {
        for (Slot currentSlot : grid.getSlots()) {
            final GridSlot gridSlot = (GridSlot) currentSlot;
            if (gridSlot.getRow() == row && gridSlot.getColumn() == column) {
                return gridSlot.getKey().toString();
            }
        }
        return null;
    }

    public static String getClassName(GridSlot gridSlot) {
        if (gridSlot != null) {
            final String classColSpan = gridSlot.getColSpan() > 1 ? String.format("layout__slot--columnspan%d", gridSlot.getColSpan()) : StringUtils.EMPTY;
            final String classRowSpan = gridSlot.getRowSpan() > 1 ? String.format("layout__slot--rowspan%d", gridSlot.getRowSpan()) : StringUtils.EMPTY;
            final String position = String.format("layout__slot--%dx%d", gridSlot.getRow(), gridSlot.getColumn());
            return String.format("%s %s %s", classColSpan, classRowSpan, position);
        }
        return StringUtils.EMPTY;
    }
}
