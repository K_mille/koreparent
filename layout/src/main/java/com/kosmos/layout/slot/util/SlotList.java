package com.kosmos.layout.slot.util;

import java.util.ArrayList;

import com.kosmos.layout.slot.impl.BaseSlot;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public class SlotList extends ArrayList<BaseSlot> {

    private static final long serialVersionUID = 3304597142920922395L;
}
