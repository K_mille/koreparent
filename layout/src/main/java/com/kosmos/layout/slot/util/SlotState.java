package com.kosmos.layout.slot.util;

/**
 * Created by fabien.leconte on 08/12/14.
 */
public enum SlotState {
    EMPTY, FILLED
}
