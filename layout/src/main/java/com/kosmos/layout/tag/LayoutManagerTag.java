package com.kosmos.layout.tag;

import javax.servlet.jsp.JspException;

import com.kosmos.layout.plugin.LayoutFichePluginHelper;
import com.kosmos.layout.view.model.LayoutManagerViewModel;

public class LayoutManagerTag extends AbstractLayoutTag<LayoutManagerViewModel> {

    private static final String LAYOUT_MANAGER_PATH = "/adminsite/layout/manager/bo/layout_manager.jsp";

    private static final long serialVersionUID = 1534473124641597886L;

    public LayoutManagerTag() {
        retrieveKey = LayoutFichePluginHelper.LAYOUT_MANAGER_VIEW_MODEL;
    }

    protected int filesStartIndex = 0;

    public static final String FILE_START_INDEX = "filesStartIndex";

    public void setFilesStartIndex(int filesStartIndex) {
        this.filesStartIndex = filesStartIndex;
    }

    @Override
    public int doStartTag() throws JspException {
        super.doStartTag();
        pageContext.getRequest().setAttribute(FILE_START_INDEX, filesStartIndex);
        includeJSP(LAYOUT_MANAGER_PATH);
        return SKIP_BODY;
    }
}
