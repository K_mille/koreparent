package com.kosmos.layout.view.model;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import com.kosmos.layout.Layout;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.util.CardDescription;

/**
 * Created on 10/12/14.
 */
public class LayoutManagerViewModel implements Serializable {

    private static final long serialVersionUID = 2798893863905519919L;

    private Collection<Layout> availableLayouts;

    private Map<String, String> editFragments;

    private Collection<CardDescription> cardsDescriptions;

    private String cardsPool;

    private Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes;

    private Map<String, String> cardBoViewPool;

    private LayoutViewModel layoutViewModel;

    public Collection<Layout> getAvailableLayouts() {
        return availableLayouts;
    }

    public void setAvailableLayouts(Collection<Layout> availableLayouts) {
        this.availableLayouts = availableLayouts;
    }

    public Map<String, String> getEditFragments() {
        return editFragments;
    }

    public void setEditFragments(Map<String, String> editFragments) {
        this.editFragments = editFragments;
    }

    public Collection<CardDescription> getCardsDescriptions() {
        return cardsDescriptions;
    }

    public void setCardsDescriptions(Collection<CardDescription> cardsDescriptions) {
        this.cardsDescriptions = cardsDescriptions;
    }

    public String getCardsPool() {
        return cardsPool;
    }

    public void setCardsPool(String cardsPool) {
        this.cardsPool = cardsPool;
    }

    public Map<String, Collection<Class<? extends CardBean>>> getAllowedCardTypes() {
        return allowedCardTypes;
    }

    public void setAllowedCardTypes(Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes) {
        this.allowedCardTypes = allowedCardTypes;
    }

    public Map<String, String> getCardBoViewPool() {
        return cardBoViewPool;
    }

    public void setCardBoViewPool(Map<String, String> cardBoViewPool) {
        this.cardBoViewPool = cardBoViewPool;
    }

    public LayoutViewModel getLayoutViewModel() {
        return layoutViewModel;
    }

    public void setLayoutViewModel(LayoutViewModel layoutViewModel) {
        this.layoutViewModel = layoutViewModel;
    }
}
