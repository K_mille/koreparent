<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="viewModel" class="com.kosmos.layout.card.view.model.SimpleCardViewModel" scope="request" />
<<%= viewModel.isLink() ? "a" : "div" %>
    class="card simpleCard"
    <%= viewModel.isLink() ? String.format("href=\"%s\"", viewModel.getUrl()) : StringUtils.EMPTY %>
    <%= viewModel.getDynamicStyle() %>><%
    if (StringUtils.isNotBlank(viewModel.getTitle())) {
    %><h2 class="simpleCard__title"><%= viewModel.getTitle() %></h2><%
    }
%></<%= viewModel.isLink() ? "a" : "div" %>>