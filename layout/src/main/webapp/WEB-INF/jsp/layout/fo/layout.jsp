<%@page import="com.kosmos.layout.Layout" %>
<%@ page import="com.kosmos.layout.slot.view.model.SlotViewModel" %>
<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.view.model.LayoutViewModel" scope="request" /><%

    final Layout layout = viewModel.getLayout();

%><div class="layout layout--<%= layout.getName() %> js-layout"><%
    for(SlotViewModel slotViewModel : viewModel.getSlotViewModels().values()) {
        %><layout:slot viewModel="<%= slotViewModel %>" front="true" extension="<%= idExtension %>"/><%
    }
%></div>
