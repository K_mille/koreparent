<%@ page import="com.kportal.core.config.MessageHelper" %>
<p>
    <label for="title-simplecardbean" class="colonne"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.TITLE") %>
    </label>
    <input type="text" name="title" id="title-simplecardbean">
</p>
<p>
    <label for="background_color-simplecardbean" class="colonne"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.BACKGROUND_COLOR") %>
    </label>
    <input type="color" name="background_color" id="background_color-simplecardbean">
</p>
<p>
    <label for="foreground_color-simplecardbean" class="colonne"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.FOREGROUND_COLOR") %>
    </label>
    <input type="color" name="foreground_color" id="foreground_color-simplecardbean">
</p>
<div>
    <span class="label colonne"><%= MessageHelper.getCoreMessage("BO.CARD.EDIT_FRAGMENT.LINK") %></span>
    <div id="kMonoSelectlink" class="kmonoselect" data-value data-placeholder data-editAction="&#x2f;adminsite&#x2f;toolbox&#x2f;choix_objet.jsp&#x3f;TOOLBOX&#x3d;PAGE_TETE" data-popintitle="LOCALE_BO.popin.title.objet" data-popinwidth="700" data-popinvalidate="false" data-title="">
        <input type="hidden" id="link" name="link">
        <input type="hidden" name="LIBELLE_link"/>
    </div>
</div>
