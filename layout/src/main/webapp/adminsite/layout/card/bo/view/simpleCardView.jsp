<%@ page import="com.kportal.core.config.MessageHelper" %>

<div class="card simpleCard">
    <span class="simpleCard__title"><%= MessageHelper.getCoreMessage("BO.CARD.SIMPLE_CARD.NAME") %></span>
</div>