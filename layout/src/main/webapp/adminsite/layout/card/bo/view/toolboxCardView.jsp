<%@ page import="com.kportal.core.config.MessageHelper" %>

<div class="card toolboxCard">
    <span class="toolboxCard__title"><%= MessageHelper.getCoreMessage("BO.CARD.TOOLBOX_CARD.NAME") %></span>
</div>