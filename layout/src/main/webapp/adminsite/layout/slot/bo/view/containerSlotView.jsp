<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kosmos.layout.card.view.model.CardViewModel" %>
<%@ page import="com.kosmos.layout.slot.util.GridSlotUtil" %>
<%@ page import="com.kosmos.layout.slot.util.SlotUtil" %>
<%@ page import="com.univ.utils.EscapeString" %>
<%@ page import="com.kosmos.layout.slot.impl.ContainerSlot" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kosmos.layout.slot.util.SlotState" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.slot.view.model.SlotViewModel" scope="request" />
<%
    final ContainerSlot slot = (ContainerSlot) viewModel.getSlot();
    final CardViewModel cardViewModel = viewModel.getCardViewModel();
    final String description = StringUtils.defaultIfBlank(MessageHelper.getMessage(idExtension,slot.getDescription()), StringUtils.EMPTY);
    final String classDescription = StringUtils.isNotBlank(description) ? "slot-description" : StringUtils.EMPTY;

%>
<div class="<%= String.format("%s %s", SlotUtil.getClassName(slot), GridSlotUtil.getClassName(slot)) %> js-layout__slot
col--lg-<%=slot.getColSpan()%>   row--lg-<%=slot.getRowSpan()%> "
     data-allowed-card-types="<%= viewModel.getAllowedCardTypes() %>"
     data-slot-key="<%=slot.getKey()%>"
     data-slot-width="<%=slot.getColSpan()%>">
    <input type="hidden" value="<%=slot.getRow()%>" class="js-slot-row" name ="slot-row-<%=slot.getKey()%>" data-slot-key="<%=slot.getKey()%>"/>
    <input type="hidden" value="<%=slot.getColumn()%>" class="js-slot-column"   name ="slot-column-<%=slot.getKey()%>" data-slot-key="<%=slot.getKey()%>"/>
    <textarea class="layout__card-model js-layout__card-model" name="jsonModel-<%= slot.getKey() %>"
              data-key="<%= slot.getKey() %>"><%= EscapeString.escapeHtml(StringUtils.defaultIfBlank(cardViewModel.getJsonCard(), StringUtils.EMPTY)) %></textarea>
    <div class="layout__slot-status layout__slot-status--empty js-layout__slot-status">
        <layout:card viewModel="<%= viewModel.getCardViewModel() %>" extension="<%= idExtension %>"/>
        <span class="icon icon-slot-empty <%=classDescription%>"><span><%=description%></span></span>
    </div>
</div>