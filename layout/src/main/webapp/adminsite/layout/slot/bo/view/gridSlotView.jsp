<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kosmos.layout.card.view.model.CardViewModel" %>
<%@ page import="com.kosmos.layout.slot.impl.GridSlot" %>
<%@ page import="com.kosmos.layout.slot.util.GridSlotUtil" %>
<%@ page import="com.kosmos.layout.slot.util.SlotUtil" %>
<%@ page import="com.univ.utils.EscapeString" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="layout" uri="http://kportal.kosmos.fr/tags/layout" %>
<jsp:useBean id="idExtension" class="java.lang.String" scope="request" />
<jsp:useBean id="viewModel" class="com.kosmos.layout.slot.view.model.SlotViewModel" scope="request" />
<%
    final GridSlot slot = (GridSlot) viewModel.getSlot();
    final CardViewModel cardViewModel = viewModel.getCardViewModel();
    final String description = StringUtils.defaultIfBlank(MessageHelper.getMessage(idExtension,slot.getDescription()), StringUtils.EMPTY);
    final String classDescription = StringUtils.isNotBlank(description) ? "slot-description" : StringUtils.EMPTY;

%>
<div class="<%= String.format("%s %s", SlotUtil.getClassName(slot), GridSlotUtil.getClassName(slot)) %> js-layout__slot"
     data-allowed-card-types="<%= viewModel.getAllowedCardTypes() %>">
    <textarea class="layout__card-model js-layout__card-model" name="jsonModel-<%= slot.getKey() %>"
              data-key="<%= slot.getKey() %>"><%= EscapeString.escapeHtml(StringUtils.defaultIfBlank(cardViewModel.getJsonCard(), StringUtils.EMPTY)) %></textarea>
    <div class="layout__slot-status layout__slot-status--empty js-layout__slot-status">
        <layout:card viewModel="<%= viewModel.getCardViewModel() %>" extension="<%= idExtension %>"/>
        <span class="icon icon-slot-empty <%=classDescription%>"><span><%=description%></span></span>
    </div>
</div>