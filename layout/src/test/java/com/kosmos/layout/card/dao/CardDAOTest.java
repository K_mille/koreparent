package com.kosmos.layout.card.dao;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;

/**
 * CardDAO Tester.
 *
 * @author <Authors name>
 * @version 1.0
 * @since <pre>déc. 2, 2014</pre>
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/layout/card/dao/test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class CardDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    CardDAO cardDAO;

    /**
     * Method: add(CardBean card)
     */
    @Test
    @DatabaseSetup("add.xml")
    @ExpectedDatabase("add-expected.xml")
    public void testAdd() {
        final CardBean cardBean = new CardBean();
        cardBean.setKey(UUID.fromString("28665f22-763a-4d93-bf60-abed97e59af6"));
        cardDAO.add(cardBean);
    }

    /**
     * Method: fill(ResultSet rs)
     */
    @Test
    @DatabaseSetup("update.xml")
    public void testFill() {
        final CardBean cardBean = cardDAO.getById(2L);
        Assert.assertEquals("28665f22-763a-4d93-bf60-abed97e59af6", cardBean.getKey().toString());
    }

    /**
     * Method: update(CardBean card)
     */
    @Test
    @DatabaseSetup("update.xml")
    @ExpectedDatabase("update-expected.xml")
    public void testUpdate() {
        final CardBean cardBean = cardDAO.getById(2L);
        cardBean.setKey(UUID.fromString("28665f22-763a-4d93-bf60-abed97e59ae5"));
        cardDAO.update(cardBean);
    }
}
