package com.kosmos.layout.card.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.bean.PictureCardBean;
import com.kosmos.layout.card.bean.SimpleCardBean;
import com.kosmos.layout.card.bean.ToolboxCardBean;
import com.kosmos.layout.card.util.CardDescription;

public class MockedCardUtil {

    private static CardDescription getCardDescription(Class<? extends CardBean> type, String name, String description) {
        final CardDescription cardDescription = new CardDescription();
        cardDescription.setType(type);
        cardDescription.setName(name);
        cardDescription.setDescription(description);
        return cardDescription;
    }

    public static Map<String, Collection<CardDescription>> getCardsDescriptionFromContext() {
        final Map<String, Collection<CardDescription>> descriptions = new HashMap<>();
        descriptions.put("extension1", Arrays.asList(getCardDescription(SimpleCardBean.class, "SimpleCard", "SimpleCard description")));
        descriptions.put("extension2", Arrays.asList(getCardDescription(ToolboxCardBean.class, "ToolboxCard", "ToolboxCard description")));
        descriptions.put("extension3", Arrays.asList(getCardDescription(PictureCardBean.class, "PictureCard", "PictureCard description")));
        return descriptions;
    }

    public static Map<String, Collection<Class<? extends CardBean>>> getPartialAllowedCardTypes() {
        final Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes = new HashMap<>();
        allowedCardTypes.put("plop1", Arrays.asList(SimpleCardBean.class, ToolboxCardBean.class));
        final Collection<Class<? extends CardBean>> list = new ArrayList<>();
        list.add(SimpleCardBean.class);
        allowedCardTypes.put("plop2", list);
        final Collection<Class<? extends CardBean>> list2 = new ArrayList<>();
        list2.add(ToolboxCardBean.class);
        allowedCardTypes.put("plop3", list2);
        return allowedCardTypes;
    }

    public static Map<String, Collection<Class<? extends CardBean>>> getAllowedCardTypes() {
        final Map<String, Collection<Class<? extends CardBean>>> allowedCardTypes = new HashMap<>();
        allowedCardTypes.put("plop1", Arrays.asList(SimpleCardBean.class, ToolboxCardBean.class, PictureCardBean.class));
        allowedCardTypes.put("plop2", Arrays.asList(SimpleCardBean.class, ToolboxCardBean.class));
        allowedCardTypes.put("plop3", Arrays.asList(ToolboxCardBean.class, PictureCardBean.class));
        return allowedCardTypes;
    }

    public static Collection<CardBean> getPartialCardPool() {
        final Collection<CardBean> cardPool = new ArrayList<>();
        cardPool.add(new SimpleCardBean());
        cardPool.add(new ToolboxCardBean());
        return cardPool;
    }

    public static Collection<CardBean> getCardPool() {
        final Collection<CardBean> cardPool = new ArrayList<>();
        cardPool.add(new SimpleCardBean());
        cardPool.add(new ToolboxCardBean());
        cardPool.add(new PictureCardBean());
        return cardPool;
    }
}
