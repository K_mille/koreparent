package com.kosmos.layout.dao;

import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

import org.testng.annotations.DataProvider;

import com.kosmos.layout.card.bean.CardBean;
import com.kosmos.layout.card.bean.PictureCardBean;
import com.kosmos.layout.card.bean.ToolboxCardBean;
import com.kosmos.layout.slot.impl.GridSlot;
import com.kosmos.layout.slot.util.SlotList;

public class CardModelProvider {

    public static final String ADD_CARD_MODEL_PROVIDER = "AddCardModelProvider";
    public static final String UPDATE_CARD_MODEL_PROVIDER = "UpdateCardModelProvider";

    @DataProvider(name = ADD_CARD_MODEL_PROVIDER)
    public static Object[][] addModel() {
        return new Object[][]{
            {getAddModel()}
        };
    }

    private static SlotList getAddModel() {
        final SlotList model = new SlotList();
        final GridSlot slot1 = new GridSlot();
        final Collection<Class<? extends CardBean>> allowedCard = new ArrayList<>();
        allowedCard.add(ToolboxCardBean.class);
        slot1.setKey(UUID.fromString("49dc54de-3539-47df-90b5-3255eebb3f19"));
        slot1.setAllowedCardTypes(allowedCard);
        slot1.setRow(0);
        slot1.setColumn(0);
        slot1.setColSpan(3);
        slot1.setRowSpan(3);
        final GridSlot slot2 = new GridSlot();
        final Collection<Class<? extends CardBean>> allowedCard2 = new ArrayList<>();
        allowedCard2.add(PictureCardBean.class);
        slot2.setAllowedCardTypes(allowedCard2);
        slot2.setKey(UUID.fromString("cab461ce-e820-4b60-b954-13e9d6e0f384"));
        slot2.setRow(0);
        slot2.setColumn(3);
        model.add(slot1);
        model.add(slot2);
        return model;
    }

    @DataProvider(name = UPDATE_CARD_MODEL_PROVIDER)
    public static Object[][] updateModel() {
        return new Object[][]{
            {getUpdateModel()}
        };
    }

    private static SlotList getUpdateModel() {
        final SlotList model = new SlotList();
        final GridSlot slot1 = new GridSlot();
        final Collection<Class<? extends CardBean>> allowedCard = new ArrayList<>();
        allowedCard.add(ToolboxCardBean.class);
        slot1.setKey(UUID.fromString("49dc54de-3539-47df-90b5-3255eebb3f20"));
        slot1.setAllowedCardTypes(allowedCard);
        slot1.setRow(1);
        slot1.setColumn(3);
        slot1.setColSpan(4);
        slot1.setRowSpan(2);
        final GridSlot slot2 = new GridSlot();
        final Collection<Class<? extends CardBean>> allowedCard2 = new ArrayList<>();
        allowedCard2.add(PictureCardBean.class);
        slot2.setKey(UUID.fromString("cab461ce-e820-4b60-b954-13e9d6e0f395"));
        slot2.setAllowedCardTypes(allowedCard2);
        slot2.setRow(2);
        slot2.setColumn(3);
        slot2.setRowSpan(2);
        slot2.setColSpan(4);
        model.add(slot1);
        model.add(slot2);
        return model;
    }
}
