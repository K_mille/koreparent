package com.kosmos.layout.dao;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.layout.Layout;
import com.kosmos.layout.grid.GridLayout;
import com.kosmos.layout.grid.impl.SimpleGrid;
import com.kosmos.layout.slot.util.SlotList;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;

@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/layout/dao/test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class LayoutDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    LayoutDAO layoutDAO;

    @Test(dataProviderClass = CardModelProvider.class, dataProvider = CardModelProvider.ADD_CARD_MODEL_PROVIDER)
    @DatabaseSetup("add.xml")
    @ExpectedDatabase("add-expected.xml")
    public void testAdd(SlotList model) {
        final SimpleGrid grid = new SimpleGrid();
        grid.setRows(3);
        grid.setColumns(7);
        grid.setSlots(model);
        grid.setName("internetTemplate");
        grid.setDescription("BO.LAYOUT.INTERNET_TEMPLATE");
        grid.setContext(true);
        layoutDAO.add(grid);
    }

    @Test(dataProviderClass = CardModelProvider.class, dataProvider = CardModelProvider.UPDATE_CARD_MODEL_PROVIDER)
    @DatabaseSetup("update.xml")
    @ExpectedDatabase("update-expected.xml")
    public void testUpdate(SlotList model) {
        final SimpleGrid grid = (SimpleGrid) layoutDAO.getById(2L);
        grid.setSlots(model);
        layoutDAO.update(grid);
    }

    @Test
    @DatabaseSetup("update.xml")
    public void testFill() {
        final Layout layout = layoutDAO.getById(2L);
        Assert.assertEquals(layout.getName(), "internetTemplate");
        Assert.assertEquals(layout.getDescription(), "BO.LAYOUT.INTERNET_TEMPLATE");
        Assert.assertEquals(layout.hasContext(), true);
        Assert.assertTrue(layout instanceof GridLayout);
        final GridLayout gridLayout = (GridLayout) layout;
        Assert.assertEquals(gridLayout.getSlots().size(), 2);
        Assert.assertEquals(gridLayout.getRows(), 3);
        Assert.assertEquals(gridLayout.getColumns(), 7);
    }

    @Test
    @DatabaseSetup("update.xml")
    public void testGetByName() {
        final Layout layout = layoutDAO.getByName("internetTemplate");
        Assert.assertEquals((long) layout.getId(), 2L);
        Assert.assertEquals(layout.hasContext(), true);
        Assert.assertTrue(layout instanceof GridLayout);
        final GridLayout gridLayout = (GridLayout) layout;
        Assert.assertEquals(gridLayout.getSlots().size(), 2);
        Assert.assertEquals(gridLayout.getRows(), 3);
        Assert.assertEquals(gridLayout.getColumns(), 7);
    }

    @Test
    @DatabaseSetup("update.xml")
    public void testGetAllLayouts() {
        final Collection<Layout> layouts = layoutDAO.getAllLayouts();
        Assert.assertEquals(layouts.size(), 2);
    }
}