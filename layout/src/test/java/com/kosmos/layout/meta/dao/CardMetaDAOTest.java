package com.kosmos.layout.meta.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.layout.meta.bean.CardMetaBean;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;

/**
 * Created by olivier.camon on 05/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/layout/meta/dao/CardMetaDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class CardMetaDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    CardMetaDAO cardMetaDAO;

    @Test
    @DatabaseSetup("CardMetaDAOTest.add.xml")
    @ExpectedDatabase("CardMetaDAOTest.add-expected.xml")
    public void testAdd() {
        final CardMetaBean cardMetaBean = new CardMetaBean();
        cardMetaBean.setIdCard(2L);
        cardMetaBean.setIdMeta(1L);
        cardMetaDAO.add(cardMetaBean);
    }

    @Test
    @DatabaseSetup("CardMetaDAOTest.update.xml")
    @ExpectedDatabase("CardMetaDAOTest.update-expected.xml")
    public void testUpdate() {
        final CardMetaBean cardMetaBean = cardMetaDAO.getById(2L);
        Assert.assertNotNull(cardMetaBean);
        cardMetaBean.setIdMeta(2L);
        cardMetaDAO.update(cardMetaBean);
    }

    @Test
    @DatabaseSetup("CardMetaDAOTest.add-expected.xml")
    @ExpectedDatabase("CardMetaDAOTest.add.xml")
    public void testDelete() {
        cardMetaDAO.delete(2L);
    }

    @Test
    @DatabaseSetup("CardMetaDAOTest.add-expected.xml")
    @ExpectedDatabase("CardMetaDAOTest.add.xml")
    public void testDeleteCardMeta() {
        cardMetaDAO.deleteCardMeta(2L, 1L);
    }


    @Test @ExpectedDatabase("CardMetaDAOTest.update.xml")
    public void testByCardMeta() {
        CardMetaBean cardMetaBean = cardMetaDAO.getCardMeta(2L, 1L);
        Assert.assertNotNull(cardMetaBean);
        Assert.assertEquals(cardMetaBean.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test @ExpectedDatabase("CardMetaDAOTest.update.xml")
    public void testByMeta() {
        List<CardMetaBean> results = cardMetaDAO.getByMeta(1L);
        Assert.assertNotNull(results);
        Assert.assertEquals(results.size(), 2, "il doit y avoir deux résultats");
        Assert.assertEquals(results.get(0).getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }


}
