package com.kosmos.layout.meta.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;
import org.springframework.test.context.TestExecutionListeners;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.kosmos.layout.meta.bean.LayoutMetaBean;
import com.kosmos.tests.testng.AbstractDbUnitTestngTests;

import java.util.List;

/**
 * Created by olivier.camon on 05/06/15.
 */
@Test
@ContextHierarchy(@ContextConfiguration(locations = {"classpath:/com/kosmos/layout/meta/dao/LayoutMetaDAOTest.test-context.xml"}))
@TestExecutionListeners({DbUnitTestExecutionListener.class})
public class LayoutMetaDAOTest extends AbstractDbUnitTestngTests {

    @Autowired
    LayoutMetaDAO layoutMetaDAO;

    @Test
    @DatabaseSetup("LayoutMetaDAOTest.add.xml")
    @ExpectedDatabase("LayoutMetaDAOTest.add-expected.xml")
    public void testAdd() {
        final LayoutMetaBean layoutMetaBean = new LayoutMetaBean();
        layoutMetaBean.setIdLayout(2L);
        layoutMetaBean.setIdMeta(1L);
        layoutMetaDAO.add(layoutMetaBean);
    }

    @Test
    @DatabaseSetup("LayoutMetaDAOTest.update.xml")
    @ExpectedDatabase("LayoutMetaDAOTest.update-expected.xml")
    public void testUpdate() {
        final LayoutMetaBean layoutMetaBean = layoutMetaDAO.getById(2L);
        Assert.assertNotNull(layoutMetaBean);
        layoutMetaBean.setIdMeta(2L);
        layoutMetaDAO.update(layoutMetaBean);
    }

    @Test
    @DatabaseSetup("LayoutMetaDAOTest.add-expected.xml")
    @ExpectedDatabase("LayoutMetaDAOTest.add.xml")
    public void testDelete() {
        layoutMetaDAO.delete(2L);
    }


    @Test
    @DatabaseSetup("LayoutMetaDAOTest.update.xml")
    public void testByLayoutMeta() {
        LayoutMetaBean layoutMeta = layoutMetaDAO.getLayoutMeta(2L, 1L);
        Assert.assertNotNull(layoutMeta);
        Assert.assertEquals(layoutMeta.getId(), Long.valueOf(2), "l'id doit correspondre à la deuxième entrée");
    }

    @Test
    @DatabaseSetup("LayoutMetaDAOTest.update-expected.xml")
    public void testByMeta() {
        LayoutMetaBean layoutMeta = layoutMetaDAO.getByMetaId(1L);
        Assert.assertNotNull(layoutMeta);
        Assert.assertEquals(layoutMeta.getId(), Long.valueOf(1), "l'id doit correspondre à la première entrée");
    }

    @Test
    @DatabaseSetup("LayoutMetaDAOTest.update-expected.xml")
    public void testLayoutMetasByLayoutId() {
        List<LayoutMetaBean> layoutMeta = layoutMetaDAO.getLayoutMetasByLayoutId(2L);
        Assert.assertNotNull(layoutMeta);
        Assert.assertEquals(layoutMeta.size(), 2, "l'id doit correspondre à la première entrée");
    }

}
