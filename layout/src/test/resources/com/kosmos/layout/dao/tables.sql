CREATE TABLE LAYOUT (
  ID_LAYOUT          BIGINT(20) NOT NULL AUTO_INCREMENT,
  NAME_LAYOUT        VARCHAR(64)         DEFAULT '',
  DESCRIPTION_LAYOUT VARCHAR(64),
  HAS_CONTEXT_LAYOUT BOOL                DEFAULT 1,
  SLOTS_LAYOUT  TEXT,
  DATAS_LAYOUT       TEXT,
  CUSTOM             TINYINT(1) NULL,
  PRIMARY KEY (ID_LAYOUT),
  UNIQUE KEY NAME_LAYOUT_UNIQUE (NAME_LAYOUT)
);


CREATE TABLE `LAYOUT_CARRIER` (
  `ID_LAYOUT_CARRIER` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `ID_LAYOUT`      BIGINT(20) NOT NULL,
  `CLASS`        VARCHAR(255) NOT NULL,
  PRIMARY KEY (`ID_LAYOUT_CARRIER`)
);
