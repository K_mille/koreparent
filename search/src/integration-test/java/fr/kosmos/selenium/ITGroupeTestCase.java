package fr.kosmos.selenium;

import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.GroupeBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceGroupe;
import fr.kosmos.selenium.service.ServiceLibelle;

/**
 * Suite de test liés aux groupes
 * Created by mathis.abdelaziz on 22/05/17.
 */
public class ITGroupeTestCase extends KSupTestCase {

    private ServiceGroupe serviceGroupe;

    private ServiceLibelle serviceLibelle;

    public ITGroupeTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     * De base, l'url est le site de test en localhost, en back-office
     */
    public void initScreen() {
        serviceLibelle = new ServiceLibelle(this);
        serviceGroupe = new ServiceGroupe(this);
    }

    /***
     * Méthode permettant de créer des libellés pour un groupe, un groupe, le rechercher, et le supprimer.
     */
    @Test
    public void testCreationGroupe() throws InterruptedException {
        serviceMenuBO.navigateToLibelle();
        serviceLibelle.createLibelle("11", "codeGr1","Libelle Groupe", "Libelle Groupe En");
        serviceMenuBO.navigateInMenuBO("menuAdministration", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_GROUPEDSI&ACTION=ACCUEIL");
        GroupeBean groupeBean = new GroupeBean();
        groupeBean.setCode("codeGroupe");
        groupeBean.setLibelle("Libelle Groupe Test");
        groupeBean.setType("codeGr1");
        groupeBean.setStructure("Structures pour l'intégration et jeux de données");
        groupeBean.setGroupeParent("Interne Kosmos");
        serviceGroupe.createGroupe(groupeBean);
        serviceGroupe.switchToTableView();
        serviceGroupe.deleteContentDatagrid("Libelle Groupe Test", "td:nth-child(1)");
    }
}
