package fr.kosmos.selenium;

import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.SiteExterneBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceIndexationExterne;

/**
 * Suite de tests liés à l'indexation de sites externes
 * Created by mathis.abdelaziz on 17/05/17.
 */
public class ITIndexationExterneTestCase extends KSupTestCase {

    private ServiceIndexationExterne serviceIndexationExterne;

    public ITIndexationExterneTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     * De base, l'url est le site de test en localhost, en back-office
     */
    public void initScreen() {
        serviceIndexationExterne = new ServiceIndexationExterne(this);
    }

    /***
     * Méthode permettant de lancer l'indexation d'un site externe, puis d'attendre que l'indexation soit terminée.
     * Une fois l'indexation terminée, supprime le site.
     */
    @Test
    public void testIndexationExterne() {
        serviceMenuBO.navigateInMenuBO("menuModule", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_SITE&ACTION=MODIFIER");
        SiteExterneBean siteExterneBean = new SiteExterneBean();
        siteExterneBean.setCode("CodeSiteExterne");
        siteExterneBean.setLibelle("Libelle Site Externe");
        siteExterneBean.setNiveauProfondeur("-1");
        siteExterneBean.setUrl("http://localhost:8080");
        siteExterneBean.setRegExp("");
        serviceIndexationExterne.createSiteExterne(siteExterneBean);
        serviceIndexationExterne.indexationSite(siteExterneBean.getLibelle());
    }
}
