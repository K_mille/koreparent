package fr.kosmos.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.LaboratoireBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceLibelle;
import fr.kosmos.selenium.service.ServiceRecherche;
import fr.kosmos.selenium.service.fiche.ServiceLaboratoire;

/**
 * Suite de tests liés aux laboratoires
 * Created by mathis.abdelaziz on 19/05/17.
 */
public class ITLaboratoireTestCase extends KSupTestCase {

    private ServiceLibelle serviceLibelle;

    private ServiceRecherche serviceRecherche;

    private ServiceLaboratoire serviceLaboratoire;

    public ITLaboratoireTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     * De base, l'url est le site de test en localhost, en back-office
     */
    public void initScreen() {
        serviceLibelle = new ServiceLibelle(this);
        serviceRecherche = new ServiceRecherche(this);
        serviceLaboratoire = new ServiceLaboratoire(this);
    }

    /***
     * Méthode permettant de créer des libellés pour laboratoire, un laboratoire et de l'afficher en front-office
     */
    @Test
    public void testCreationLaboratoire() {
        serviceMenuBO.navigateToLibelle();
        serviceLibelle.createLibelle("63", "codeLab1", "TypeLaboratoire Test", "TypeLaboratoire Test En");
        serviceLibelle.createLibelle("22", "codeLab2", "Ville Test", "Ville Test En");
        serviceMenuBO.navigateToContenu("rechercheetlabo","SAISIE_LABORATOIRE");
        LaboratoireBean laboratoireBean = new LaboratoireBean();
        laboratoireBean.setCode("CodeLaboratoire");
        laboratoireBean.setLibelleCourt("Libelle Laboratoire");
        laboratoireBean.setLibelleLong("Libelle Laboratoire Long");
        laboratoireBean.setTypeLaboratoire("codeLab1");
        laboratoireBean.setReference("Reference Laboratoire");
        laboratoireBean.setTutelle("Tutelle Laboratoire");
        laboratoireBean.setDepartementScientifique("DS10");
        laboratoireBean.setStructure("Structures pour l'intégration et jeux de données");
        laboratoireBean.setRattachementsExternes("Rattachements Externes Laboratoire");
        laboratoireBean.setAdresse("Adresse Laboratoire");
        laboratoireBean.setCodePostal("44000");
        laboratoireBean.setVille("Ville Laboratoire");
        laboratoireBean.setVilleMultiSelect("codeLab2");
        laboratoireBean.setTelephone("01 02 03 04 05");
        serviceLaboratoire.getScreenBO().createContent();
        serviceLaboratoire.fillContent(laboratoireBean);
        serviceLaboratoire.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.rechercheItem(laboratoireBean.getLibelleLong());
    }

    /***
     * Méthode permettant de rechercher en front-office un laboratoire et de vérifier que les champs saisis sont présents
     */
    @Test(dependsOnMethods = {"testCreationLaboratoire"})
    public void testRechercheLaboratoire() {
        LaboratoireBean searchLaboratoireBean = new LaboratoireBean();
        searchLaboratoireBean.setCode("CodeLaboratoire");
        searchLaboratoireBean.setLibelleCourt("Libelle Laboratoire");
        searchLaboratoireBean.setLibelleLong("Libelle Laboratoire Long");
        searchLaboratoireBean.setTypeLaboratoire("TypeLaboratoire Test");
        searchLaboratoireBean.setReference("Reference Laboratoire");
        searchLaboratoireBean.setTutelle("Tutelle Laboratoire");
        searchLaboratoireBean.setDepartementScientifique("Agronomie, productions animale et végétale et agro-alimentaire");
        searchLaboratoireBean.setStructure("Structures pour l'intégration et jeux de données");
        searchLaboratoireBean.setRattachementsExternes("Rattachements Externes Laboratoire");
        searchLaboratoireBean.setAdresse("Adresse Laboratoire");
        searchLaboratoireBean.setCodePostal("44000");
        searchLaboratoireBean.setVille("Ville Laboratoire");
        searchLaboratoireBean.setVilleMultiSelect("Ville Test");
        searchLaboratoireBean.setTelephone("01 02 03 04 05");
        serviceRecherche.getURL(urlFO);
        WebElement element = serviceRecherche.rechercheItem(searchLaboratoireBean.getLibelleLong());
        element.findElement(new By.ByCssSelector("a.item-title__element_title")).click();
        serviceLaboratoire.controlFront(searchLaboratoireBean);
    }
}
