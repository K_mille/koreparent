package fr.kosmos.selenium;

import org.testng.annotations.Test;

import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceLibelle;

/**
 * Suite de tests liés à la création de libellés
 * Created by mathis.abdelaziz on 17/05/17.
 */
public class ITLibelleTestCase extends KSupTestCase {

    private ServiceLibelle serviceLibelle;

    public ITLibelleTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     * De base, l'url est le site de test en localhost, en back-office
     */
    public void initScreen() {
        serviceLibelle = new ServiceLibelle(this);
    }

    /***
     * Méthode permettant de naviguer jusqu'à l'onglet libellés, créer un libelle et le rechercher.
     */
    @Test
    public void testCreationLibelle() {
        serviceMenuBO.navigateToLibelle();
        serviceLibelle.createLibelle("08", "codeAct1","ActualiteFrancais", "ActualiteAnglais");
        serviceLibelle.isPresentResult("codeAct1");
    }
}
