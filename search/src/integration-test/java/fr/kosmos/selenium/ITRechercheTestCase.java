package fr.kosmos.selenium;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;

import fr.kosmos.selenium.annotation.Exigence;
import fr.kosmos.selenium.bean.ActualiteBean;
import fr.kosmos.selenium.bean.AgendaBean;
import fr.kosmos.selenium.bean.LibelleBean;
import fr.kosmos.selenium.bean.PageLibreBean;
import fr.kosmos.selenium.bean.RubriqueBean;
import fr.kosmos.selenium.bean.UtilisateurBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceLibelle;
import fr.kosmos.selenium.service.ServiceRecherche;
import fr.kosmos.selenium.service.ServiceRubrique;
import fr.kosmos.selenium.service.ServiceUtilisateur;
import fr.kosmos.selenium.service.fiche.ServiceActualite;
import fr.kosmos.selenium.service.fiche.ServicePageLibre;
import fr.kosmos.selenium.utils.RoleTest;

/**
 * Suite de tests liés à la recherche
 * Created by mathis.abdelaziz on 03/05/17.
 */
public class ITRechercheTestCase extends KSupTestCase {

    private ServiceRecherche serviceRecherche;

    private ServiceLibelle serviceLibelle;

    private ServiceActualite serviceActualite;

    private ServiceRubrique serviceRubrique;

    private ServiceUtilisateur serviceUtilisateur;

    private ServicePageLibre servicePageLibre;

    private static final Logger LOGGER = LoggerFactory.getLogger(ITRechercheTestCase.class);

    public ITRechercheTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     * De base, l'url est le site de test en localhost, en back-office
     */
    public void initScreen() {
        serviceLibelle = new ServiceLibelle(this);
        serviceActualite = new ServiceActualite(this);
        serviceRecherche = new ServiceRecherche(this);
        serviceRubrique = new ServiceRubrique(this);
        serviceUtilisateur = new ServiceUtilisateur(this);
        servicePageLibre = new ServicePageLibre(this);
    }

    /***
     * Test permettant de créer une fiche actualité et de la rechercher en front-office.
     * Si aucun résultat n'est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test
    public void testRechercheFicheCreated() {
        serviceMenuBO.navigateToContenu("actualite", "SAISIE_ACTUALITE");
        ActualiteBean actualiteBean = new ActualiteBean();
        actualiteBean.setTitre("Titre Actualite 1");
        LocalDateTime localTime = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        actualiteBean.setDateDebut(localTime.format(dateFormatter));
        actualiteBean.setDateFin(localTime.plusDays(10).format(dateFormatter));
        actualiteBean.setResume("Resume actualite 1");
        serviceActualite.getScreenBO().createContent();
        serviceActualite.fillContent(actualiteBean);
        serviceActualite.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.rechercheItem(actualiteBean.getTitre());
    }

    /***
     * Test permettant de se connecter, de supprimer une fiche actualité (par défaut celle créée par le test précédent) et de tenter une recherche de celle-ci en front-office.
     * Si un résultat est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test(dependsOnMethods = {"testRechercheFicheCreated"})
    public void testRechercheFicheDeleted() {
        String libelleActualite = "Titre Actualite 1";
        ActualiteBean actualiteBean = new ActualiteBean();
        actualiteBean.setTitre(libelleActualite);
        serviceMenuBO.navigateToContenu("actualite", "SAISIE_ACTUALITE");
        serviceActualite.deleteContentDatagrid(libelleActualite,"td:nth-CreateLayoutContentScreenBO(2)");
        serviceRecherche.getURL(urlFO);
        serviceRecherche.checkResultNotExist(libelleActualite);
    }

    /***
     * Test permettant de se connecter, de créer une rubrique avec des droits limités et de se déconnecter.
     * Puis, de se connecter en front-office avec un compte aux droits restreints et de tenter un accès à cette rubrique.
     * Si un résultat est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test
    public void testAccesRubrique() {
        serviceMenuBO.navigateInMenuBO("menuAdministration", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_UTILISATEUR&ACTION=ACCUEIL");
        UtilisateurBean utilisateur1 = new UtilisateurBean();
        utilisateur1.setCivilite("M.");
        utilisateur1.setNom("Nom Utilisateur 1");
        utilisateur1.setPrenom("Prenom Utilisateur 1");
        utilisateur1.setMail("Mail Utilisateur 1");
        utilisateur1.setLogin("user1");
        utilisateur1.setPassword("user1");
        utilisateur1.setConfirmPassowrd("user1");
        utilisateur1.setRole("0007");
        serviceUtilisateur.createUtilisateur(utilisateur1);
        serviceMenuBO.navigateInMenuBO("menuGestionSite", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_RUBRIQUE&ACTION=ACCUEIL");
        RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setLangue("0");
        rubriqueBean.setIntitule("Rubrique Test Recherche");
        rubriqueBean.setIntituleAriane("Rubrique Test Recherche");
        rubriqueBean.setGroupeRestriction("Personnel");
        rubriqueBean.setRubrique("Premiers pas");
        serviceRubrique.createRubrique(rubriqueBean);
        serviceRecherche.getURL(urlFO);
        serviceRecherche.getServiceConnexionFO().deconnexion();
        serviceRecherche.getServiceConnexionFO().identification("user1", "user1");
        Assert.assertFalse(serviceRecherche.isPresentRubriqueTest(), "La rubrique est présente après restriction");
    }

    /***
     * Test permettant de se connecter, de créer une fiche actualité avec des droits limités aux personnels et étudiants et de se déconnecter.
     * Puis, de se connecter en front-office avec un compte étudiant et de tenter une recherche de cette fiche.
     * De retourner en back-office, se reconnecter en admin, modifier la fiche pour l'affilier à une rubrique restreinte aux étudiants.
     * Retourner en front-office avec un compte personnel et de retenter une recherche de cette fiche
     * Si un résultat est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test(dependsOnMethods = {"testAccesRubrique"})
    public void testDroitsRecherche() {
        serviceMenuBO.navigateInMenuBO("menuAdministration", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_UTILISATEUR&ACTION=ACCUEIL");
        UtilisateurBean utilisateur2 = new UtilisateurBean();
        utilisateur2.setCivilite("M.");
        utilisateur2.setNom("Nom Utilisateur 2");
        utilisateur2.setPrenom("Prenom Utilisateur 2");
        utilisateur2.setMail("Mail Utilisateur 2");
        utilisateur2.setLogin("user2");
        utilisateur2.setPassword("user2");
        utilisateur2.setConfirmPassowrd("user2");
        utilisateur2.setRole("0007");
        utilisateur2.setGroupe("Etudiant");
        serviceUtilisateur.createUtilisateur(utilisateur2);
        serviceMenuBO.navigateToContenu("actualite", "SAISIE_ACTUALITE");
        ActualiteBean actualiteBean = new ActualiteBean();
        actualiteBean.setTitre("Titre Actualite 3");
        LocalDateTime localTime = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        actualiteBean.setDateDebut(localTime.format(dateFormatter));
        actualiteBean.setDateFin(localTime.plusDays(10).format(dateFormatter));
        actualiteBean.setResume("Résumé actualité 3");
        actualiteBean.setPlublicVises("Etudiant");
        actualiteBean.setModeRestriction("2");
        serviceActualite.getScreenBO().createContent();
        serviceActualite.fillContent(actualiteBean);
        serviceActualite.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.getServiceConnexionFO().deconnexion();
        serviceRecherche.getServiceConnexionFO().identification("user2", "user2");
        serviceRecherche.rechercheItem(actualiteBean.getTitre());
        driver.get(urlBO);
        serviceMenuBO.deconnexion();
        connexionBOUtils.connexionBO("admin", "admin");
        serviceMenuBO.navigateToContenu("actualite", "SAISIE_ACTUALITE");
        serviceActualite.searchContent(actualiteBean);
        serviceActualite.updateContentDatagrid(actualiteBean.getTitre());
        actualiteBean.setRubrique("Rubrique Test Recherche");
        serviceActualite.getScreenBO().createContent();
        serviceActualite.fillContent(actualiteBean);
        serviceActualite.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.getServiceConnexionFO().deconnexion();
        serviceRecherche.getServiceConnexionFO().identification("user2", "user2");
        serviceRecherche.checkResultNotExist(actualiteBean.getTitre());
    }
    /***
     * Méthode permettant de vérifier si le rôle webmaster possède les droits d'édition d'agendas,
     * de créer une actualité, de lui ajouter un agenda et de vérifier la présence de celui-ci en front-office
     * Si la date n'est pas identique à celle renseignée lors de la création, renseigne un message d'erreur et arrête le test.
     */
    @Test
    public void testRechercheActualiteAgenda() {
        serviceMenuBO.navigateToLibelle();
        serviceLibelle.createLibelle("08", "codeAg1", "Lieu Agenda Test","Lieu Agenda Test En" );
        serviceLibelle.createLibelle("0200", "codeAg2", "Categorie Agenda Test","Categorie Agenda Test En" );
        serviceLibelle.createLibelle("04", "codeAg3", "Thematique Agenda Test","Thematique Agenda Test En" );
        serviceMenuBO.navigateToContenu("actualite", "SAISIE_ACTUALITE");
        ActualiteBean actualiteBean = new ActualiteBean();
        actualiteBean.setTitre("Actualite Agenda 1");
        actualiteBean.setResume("Resume actualite agenda");
        LocalDateTime localTime = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        actualiteBean.setDateDebut(localTime.format(dateFormatter));
        actualiteBean.setDateFin(localTime.plusDays(10).format(dateFormatter));
        AgendaBean agendaBean = new AgendaBean();
        agendaBean.setCategorie(new LibelleBean("codeAg2", "libelleFR", "libelleEN"));
        agendaBean.setThematique(new LibelleBean("codeAg3", "libelleFR", "libelleEN"));
        agendaBean.setLieu(new LibelleBean("codeAg1", "libelleFR", "libelleEN"));
        agendaBean.setEvenementDebut(localTime.format(dateFormatter));
        agendaBean.setEvenementFin(localTime.plusDays(10).format(dateFormatter));
        agendaBean.setComplement("Complement Actualite Agenda Test");
        actualiteBean.setListAgenda(Arrays.asList(agendaBean));
        serviceActualite.getScreenBO().createContent();
        serviceActualite.fillContent(actualiteBean);
        serviceActualite.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.recherche(actualiteBean.getTitre());
    }
    /***
     * Test permettant de se connecter, de créer une fiche actualité, et de la rechercher en front-office.
     * Une fois recherchée, des facettes sont appliquées et vérifie si l'actualité est encore présente.
     * Si aucun résultat n'est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test
    public void testFacette() {
        serviceMenuBO.navigateToContenu("actualite", "SAISIE_ACTUALITE");
        ActualiteBean actualiteBean = new ActualiteBean();
        actualiteBean.setTitre("Titre Actualite Test Facette");
        LocalDateTime localTime = LocalDateTime.now();
        DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        actualiteBean.setDateDebut(localTime.format(dateFormatter));
        actualiteBean.setDateFin(localTime.plusDays(10).format(dateFormatter));
        actualiteBean.setResume("Resume Test Facette");
        serviceActualite.getScreenBO().createContent();
        serviceActualite.fillContent(actualiteBean);
        serviceActualite.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.recherche(actualiteBean.getTitre());
        serviceRecherche.selectFacette("types-actualite");
        serviceRecherche.checkResultExist(actualiteBean.getTitre());
        serviceRecherche.selectFacette("modificationDateRange-SEARCH_RANGE_LAST_WEEK");
        serviceRecherche.removeFilter("types","actualite");
        serviceRecherche.checkResultExist(actualiteBean.getTitre());
        serviceRecherche.removeFilter("modificationDateRange","SEARCH_RANGE_LAST_WEEK");
        serviceRecherche.checkResultExist(actualiteBean.getTitre());
    }

    @Test
    @Exigence(ticketExigence = "KRTO-560", ticketTest = "KRTO-1146", enTantQue = RoleTest.ADMIN, jeVeux = "Publier une fiche avec des mots-clés renseignés", afinDe = "Vérifier que la recherche sur ce champ fonctionne")
    public void testRechercheParMotCle(){
        servicePageLibre = new ServicePageLibre(this);

        String keywords[] = {"keywords1", "monKeywordsUnPeuPlusCompliquéEtLong", "UnDernierPourLaFin"};

        PageLibreBean pageLibreBean = new PageLibreBean();
        pageLibreBean.setRubrique("Site DATA");
        pageLibreBean.setTitre("Page libre pour recherche " + System.currentTimeMillis());
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");
        servicePageLibre.getScreenBO().createContent();
        servicePageLibre.fillContent(pageLibreBean);
        servicePageLibre.fillKeywords(String.join(",", keywords));

        servicePageLibre.publishContent();

        // On doit attendre que l'indexation se fasse
        try {
            Thread.sleep(2000L);
        } catch (InterruptedException e) {
            LOGGER.error("impossible de faire un sleep à l'arrêt du driver", e);
            Thread.currentThread().interrupt();
        }

        serviceRecherche.getURL(urlFO);
        for(String keyword : keywords) {
            serviceRecherche.recherche(keyword);
            serviceRecherche.checkResultExist(pageLibreBean.getTitre());
        }
    }
}
