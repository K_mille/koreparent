package fr.kosmos.selenium;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.RubriqueBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceRubrique;

/**
 * Suite de tests sur les rubriques
 * Created by mathis.abdelaziz on 22/05/17.
 */
public class ITRubriqueTestCase extends KSupTestCase {

    private ServiceRubrique serviceRubrique;

    public ITRubriqueTestCase() {
    }

    /**
     * {@inheritDoc}
     */
    public void initScreen() {
        serviceRubrique = new ServiceRubrique(this);
    }

    /***
     * Méthode permettant de créer une rubrique, la rechercher, la déplacer, puis la supprimer
     */
    @Test
    public void testCreationRubrique() {
        serviceMenuBO.navigateInMenuBO("menuGestionSite", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_RUBRIQUE&ACTION=ACCUEIL");
        String rubriqueCode = "CodeRubrique";
        RubriqueBean rubriqueBean = new RubriqueBean();
        rubriqueBean.setCode(rubriqueCode);
        rubriqueBean.setIntitule("Rubrique Modif");
        rubriqueBean.setIntituleAriane("Rubrique Modif");
        rubriqueBean.setLangue("0");
        rubriqueBean.setRubrique("");
        serviceRubrique.createRubrique(rubriqueBean);
        serviceRubrique.switchToTableView();
        WebElement row = serviceRubrique.getContentDatagrid(rubriqueCode, "td:nth-child(3)");
        serviceRubrique.modifierRubrique(row);
        serviceRubrique.switchToTableView();
        serviceRubrique.deleteContentDatagrid(rubriqueCode, "td:nth-child(3)");
    }
}
