package fr.kosmos.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import fr.kosmos.selenium.annotation.Exigence;
import fr.kosmos.selenium.bean.ArticleBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceRecherche;
import fr.kosmos.selenium.service.fiche.ServiceArticle;
import fr.kosmos.selenium.utils.RoleTest;

/**
 * Suite de test liés à la création d'articles
 * Created by mathis.abdelaziz on 18/05/17.
 */
public class ITSearchArticleTestCase extends KSupTestCase {

    private ServiceRecherche serviceRecherche;

    private ServiceArticle serviceArticle;

    public ITSearchArticleTestCase() {
    }

    /***
     * {@inheritDoc}
     */
    public void initScreen() {
        serviceRecherche = new ServiceRecherche(this);
        serviceArticle = new ServiceArticle(this);
    }

    /***
     * Méthode permettant de créer un libellé thématique, un article et de le rechercher en front-office
     */
    @Test
    @Exigence(ticketExigence = "KRTO-977", ticketTest = "KRTO-978", enTantQue = RoleTest.ADMIN, jeVeux= "publier une fiche", afinDe = "Vérifier que la fiche apparaît dans les résultats de recherche en front-office")
    public void testRechercheArticle() {
        serviceMenuBO.navigateToContenu("core", "SAISIE_ARTICLE");
        ArticleBean articleBean = new ArticleBean();
        articleBean.setTitre("Titre Article Test");
        articleBean.setSousTitre("Sous Titre Article");
        articleBean.setRubrique("Les fiches et leurs listes");
        articleBean.setStructure("Structures pour l'intégration et jeux de données");
        articleBean.setDateArticle("01/01/2017");
        articleBean.setChapeau("Résumé article");
        articleBean.setCorps("Description article");
        serviceArticle.getScreenBO().createContent();
        serviceArticle.fillContent(articleBean);
        serviceArticle.publishContent();
        serviceRecherche.getURL(urlFO);
        WebElement element = serviceRecherche.rechercheItem(articleBean.getTitre());
        element.findElement(new By.ByCssSelector("a.item-title__element_title")).click();
    }
}
