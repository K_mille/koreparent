package fr.kosmos.selenium;

import org.testng.annotations.Test;

import fr.kosmos.selenium.annotation.Exigence;
import fr.kosmos.selenium.bean.PageAccueilBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceRecherche;
import fr.kosmos.selenium.service.fiche.ServicePageAccueil;
import fr.kosmos.selenium.utils.PageAccueilType;
import fr.kosmos.selenium.utils.RoleTest;

/**
 * Suite de tests liés à la page d'accueil
 * Created by mathis.abdelaziz on 15/05/17.
 */
public class ITSearchFicheAccueilTestCase extends KSupTestCase {

    private ServiceRecherche serviceRecherche;

    private ServicePageAccueil servicePageAccueil;

    public ITSearchFicheAccueilTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     * De base, l'url est le site de test en localhost, en back-office
     */
    public void initScreen() {
        servicePageAccueil = new ServicePageAccueil(this);
        serviceRecherche = new ServiceRecherche(this);
    }

    /***
     * Méthode permettant de créer une page d'accueil, de renseigner toutes les cartes et de la rechercher en front-office (Style1)
     */
    @Test
    @Exigence(ticketExigence = "KRTO-XXX", ticketTest = "KRTO-XXX", enTantQue = RoleTest.ADMIN, jeVeux= "publier une fiche accueil", afinDe = "Vérifier que la fiche apparaît dans les résultats de recherche en front-office")
    public void testCreationPageAcceuil1() {
        serviceMenuBO.navigateToContenu("accueil", "SAISIE_FICHEACCUEIL");

        PageAccueilBean pageAccueilBean = new PageAccueilBean();
        pageAccueilBean.setTitre("Titre Page d'Accueil Recherche");
        pageAccueilBean.setRubrique("Les fiches et leurs listes");
        pageAccueilBean.setStructure("Structures pour l'intégration et jeux de données");
        pageAccueilBean.setTypeAffichage(PageAccueilType.SITE_COM);
        //Grande Carte Photo
        pageAccueilBean.setTitreGrandeCartePhoto("Titre Grande Carte Photo");
        pageAccueilBean.setPhotoGrandeCartePhoto("");
        //Carte Simple 1
        pageAccueilBean.setTitreCarteSimple1("Titre Carte Simple 1");
        //Carte Simple 2
        pageAccueilBean.setTitreCarteSimple2("Titre Carte Simple 2");
        //Carte Toolbox 1
        pageAccueilBean.setTitreCarteToolbox1("Titre Carte Toolbox 1");
        pageAccueilBean.setStyleCarteToolbox1("style1");
        pageAccueilBean.setContenuCarteToolbox1("Contenu Carte Toolbox 1");
        //Carte Toolbox 2
        pageAccueilBean.setTitreCarteToolbox2("Titre Carte Toolbox 2");
        pageAccueilBean.setStyleCarteToolbox2("style2");
        pageAccueilBean.setContenuCarteToolbox2("Contenu Carte Toolbox 2");
        //Carte Toolbox 3
        pageAccueilBean.setTitreCarteToolbox3("Titre Carte Toolbox 3");
        pageAccueilBean.setStyleCarteToolbox3("style3");
        pageAccueilBean.setContenuCarteToolbox3("Contenu Carte Toolbox 3");
        //Carte Photo 1
        pageAccueilBean.setTitreCartePhoto("Titre Carte Photo");
        pageAccueilBean.setPhotoCartePhoto("");
        servicePageAccueil.getScreenBO().createContent();
        servicePageAccueil.fillContent(pageAccueilBean);
        servicePageAccueil.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.recherche(pageAccueilBean.getTitre());
    }

}