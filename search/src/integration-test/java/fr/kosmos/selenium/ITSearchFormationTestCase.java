package fr.kosmos.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.FormationBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceFormation;
import fr.kosmos.selenium.service.ServiceLibelle;
import fr.kosmos.selenium.service.ServiceRecherche;

/**
 * Suite de tests sur les formations
 * Created by mathis.abdelaziz on 19/05/17.
 */
public class ITSearchFormationTestCase extends KSupTestCase {

    private ServiceLibelle serviceLibelle;

    private ServiceRecherche serviceRecherche;

    private ServiceFormation serviceFormation;

    public ITSearchFormationTestCase() {
    }

    /***
     * Méthode Initialisant le navigateur ainsi que tous les écrans et outils necessaires.
     */
    public void initScreen() {
        serviceLibelle = new ServiceLibelle(this);
        serviceRecherche = new ServiceRecherche(this);
        serviceFormation = new ServiceFormation(this);
    }

    /***
     * Méthode permettant de créer des libellés pour la formation, une formation et l'afficher en front-office
     */
    @Test
    public void testCreationFormation() {
        serviceMenuBO.navigateToLibelle();
        /**serviceLibelle.createLibelle("2005", "codeFo1", "Mention", "Mention En");
        serviceLibelle.createLibelle("2006", "codeFo2", "Parcours", "Parcours En");
        serviceLibelle.createLibelle("2007", "codeFo3", "Niveau Recrutement", "Niveau Recrutement En");
        serviceLibelle.createLibelle("2008", "codeFo4", "Année Post Bac", "Année Post Bac En");
        serviceLibelle.createLibelle("2009", "codeFo5", "Niveau Sortie", "Niveau Sortie En");
        serviceLibelle.createLibelle("2010", "codeFo6", "Lieu", "Lieu En");
        serviceLibelle.createLibelle("2011", "codeFo7", "Langue", "Langue En");
        serviceLibelle.createLibelle("2012", "codeFo8", "Durée Formation", "Durée Formation En");
        serviceLibelle.createLibelle("65", "codeFo9", "Secteur Activite", "Secteur Activité En");
        serviceLibelle.createLibelle("2014", "codeFo10", "Matière principale", "Matière principale En");
        serviceLibelle.createLibelle("2015", "codeFo11", "Centre d'intérêt", "Centre d'intérêt En");
        serviceLibelle.createLibelle("2016", "codeFo12", "Série de bac", "Série de bac En");
        serviceLibelle.createLibelle("2017", "codeFo13", "Domaine Erasmus", "Domaine Erasmus En");
        serviceLibelle.createLibelle("2001", "codeFo14", "Catalogue", "Catalogue En");
        serviceLibelle.createLibelle("2002", "codeFo15", "Nature Formation", "Nature Formation En");
        serviceLibelle.createLibelle("2003", "codeFo16", "Grade Formation", "Grade Formation En");
        serviceLibelle.createLibelle("2004", "codeFo17", "Domaine LMD", "Domaine LMD En");
        serviceLibelle.createLibelle("55", "codeFo18", "Discipline", "Discipline En");**/

        serviceMenuBO.navigateToContenu("ofin", "SAISIE_FORMATION");
        FormationBean formationBean = new FormationBean();
        //Onglet Caractéristiques
        formationBean.setCode(formationBean.getCode());
        formationBean.setLangue("0");
        formationBean.setCodeSI("codeSI");
        formationBean.setIntituleCourt("Intitule Formation");
        formationBean.setIntituleLong("Intitule Formation Long");
        formationBean.setRubrique("Premiers pas");
        formationBean.setStructure("Structures pour l'intégration et jeux de données");
        formationBean.setCatalogueFormation("codeFo14");
        formationBean.setNatureFormation("codeFo15");
        formationBean.setTypeDiplome("13000");
        formationBean.setGrade("codeFo16");
        formationBean.setDomaineLMD("codeFo17");
        formationBean.setDiscipline("codeFo18");
        formationBean.setMention("Mention");
        formationBean.setParcours("Parcours");
        formationBean.setNiveauRecrutement("codeFo3");
        formationBean.setAnneePostBacSortie("codeFo4");
        formationBean.setNiveauSortie("codeFo5");
        formationBean.setLieu("codeFo6");
        formationBean.setLanguesEnseignements("codeFo7");
        formationBean.setDureeFormation("codeFo8");
        formationBean.setAutreDuree("Autre durée formation");
        formationBean.setPrincipalesMatieres("codeFo10");
        formationBean.setSecteursActivites("Secteur Activite");
        //formationBean.setMetiers("Métier");
        formationBean.setCentresInterets("codeFo11");
        formationBean.setSerieBacPreconisee("codeFo12");
        formationBean.setDomaineErasmus("codeFo13");

        //Onglet Affichage
        formationBean.setObjectifs("Objectifs formation");
        //Carte Infos Générales
        formationBean.setTitreCarteInfosGenerales("Titre Carte Générale");
        formationBean.setLieuCommentaire("Lieu Commentaire");
        formationBean.setStageCommentaire("Stage Commentaire");
        formationBean.setLanguesEnseignementsCommentaire("Langues Enseignements Commentaire");
        //Carte Bloc Infos 1ère année
        formationBean.setTitreCarteInfos("Titre Carte Infos Première Année");
        formationBean.setChargeTravailHebdo("Charge Travail Hebdomadaire");
        formationBean.setEffectifAttenduLibelle("Effectif Attendu Libelle");
        formationBean.setEffectifAttenduChiffre("10");
        formationBean.setEffectifAttenduCommentaire("Effectif Attendu Commentaire");
        formationBean.setTauxReussiteLibelle("Taux Reussite Libelle");
        formationBean.setTauxReussitePourcentage("10%");
        formationBean.setTauxReussiteCommentaire("Taux Reussite Commentaire");
        //Carte Bloc Publics
        formationBean.setTitreCartePublics("Titre Carte Publics");
        formationBean.setSerieBacPreconiseeCommentaire("Serie Bac Preconisee");
        formationBean.setPublicCible("Publics Ciblés");
        //Carte Bloc Rythme
        formationBean.setTitreCarteRythme("Titre Carte Rythme");
        formationBean.setCommentaireTempsPlein("Commentaire Temps Plein");
        formationBean.setCommentaireCompatible("Commentaire Compatible");
        formationBean.setCommentaireTempsAmenage("Commentaire Temps Amenagé");
        formationBean.setCommentaireAlternance("Commentaire Alternance");
        formationBean.setCommentaireHorsContrat("Commentaire Hors Contrat");
        formationBean.setCommentaireContratApprentissage("Commentaire Contrat Apprentissage");
        formationBean.setCommentaireContratProfessionnalisation("Commentaire Contrat Professionnalisation");
        formationBean.setCommentairePresentiel("Commentaire Présentiel");
        formationBean.setCommentairePartielDistance("Commentaire Partiellement à Distance");
        formationBean.setCommentaireTotalDistance("Commentaire Totalement à Distance");
        //Carte Bloc Admission
        formationBean.setTitreCarteAdmission("Titre Carte Admission");
        formationBean.setModalitesSelection("Modalités Selection");
        formationBean.setCapaciteAccueilLibelle("Capacité Accueil Libellé");
        formationBean.setCapaciteAccueilChiffre("15");
        formationBean.setCapaciteAccueilCommentaire("Capacite Accueil Commentaire");
        //Carte Bloc Temoignage
        formationBean.setTitreCarteTemoignage("Titre Carte Temoignage");
        //Carte Bloc International
        formationBean.setTitreCarteInternational("Titre Carte International");
        formationBean.setCoursLangueEtrangereLibelle("Cours Langue Etrangere Libelle");
        formationBean.setCoursLangueEtrangerePourcentage("15%");
        formationBean.setCoursLangueEtrangereCommentaire("Cours Langue Etrangere Commentaires");
        formationBean.setSejourEtranger("Sejour Etranger");
        //Carte Bloc Inscription
        formationBean.setTitreCarteInscription("Titre Carte Inscription");
        formationBean.setCoutFormation("Cout Formation");
        formationBean.setModalitesInscription("Modalites d'inscriptions");
        //Carte Bloc Debouches
        formationBean.setTitreCarteDebouches("Titre Carte Debouchés");
        formationBean.setPoursuiteEtudes("Poursuites d'études");
        //Carte Bloc Video
        formationBean.setTitreCarteVideo("Titre Carte Vidéo");
        //Carte Bloc Specificités
        formationBean.setTitreCarteSpecificites("Titre Carte Spécificités");
        formationBean.setSpecificites("Specificités");
        //Carte Bloc Professionnalisation
        formationBean.setTitreCartePro("Titre Carte Professionnalisation");
        formationBean.setProjetTutore("Projet Tutoré");
        formationBean.setFormationPro("Formation Professionnelle");
        formationBean.setTauxInsertionProChiffre("20");
        formationBean.setTauxInsertionProCommentaire("Taux Insertion Commentaire");
        //Carte Bloc Libre 1
        formationBean.setTitreCarteLibre1("Titre Carte Libre 1");
        formationBean.setLibelleLien1("Libelle Lien 1");
        //Carte Bloc Libre 2
        formationBean.setTitreCarteLibre2("Titre Carte Libre 2");
        formationBean.setLibelleLien2("Libelle Lien 2");
        //Carte Bloc Libre 3
        formationBean.setTitreCarteLibre3("Titre Carte Libre 3");
        formationBean.setLibelleLien3("Libelle Lien 3");

        //Remplissage des CKEditors
        formationBean.setResume("Resume");
        formationBean.setEnjeux("Enjeux");
        formationBean.setSpecificitesComplement("Specificités");
        formationBean.setLieuComplement("Lieu complément");
        formationBean.setResponsableFormation("Responsable Formation");
        formationBean.setLaboratoires("Laboratoires");
        formationBean.setEtablissements("Etablissements");
        formationBean.setEntreprises("Entreprises");
        formationBean.setConvention("Convention");
        formationBean.setFormationRequise("Formation Requise");
        formationBean.setPublicCible("Public Ciblé");
        formationBean.setModalitesCandidature("Modalités Candidature");
        formationBean.setModalitesCandidatureSpecifique("Modalités Candidature Spécifiques");
        formationBean.setConditionAdmission("Condition d'Admission");
        formationBean.setIntroductionProgramme("Introduction Programme");
        formationBean.setContenuFormation("Contenu Formation");
        formationBean.setRythmePremiereAnnee("Rythme Première Année");
        formationBean.setDescriptionEquipePedagogique("Equipe Pédagogique");
        formationBean.setDescriptionRythmeTempsPlein("Description Rythme Temps Plein");
        formationBean.setDescriptionRythmeTempsAmenage("Description Rythme Temps Amenage");
        formationBean.setDescriptionRythmeCompatibleProfessionnelle("Description Rythme Compatible Professionnelle");
        formationBean.setDescriptionRythmeAlternance("Description Rythme Alternance");
        formationBean.setDescriptionPresentiel("Description Rythme Presentiel");
        formationBean.setDescriptionPartielDistance("Description Rythme Partiellement à distance");
        formationBean.setDescriptionTotalDistance("Description Rythme Totalement à distance");
        formationBean.setDescriptionStage("Informations Complémentaires Stage");
        formationBean.setDescriptionSejour("Informations Complémentaires Stage Etrangers");
        formationBean.setDescriptionProjet("Informations Complémentaires Projets Tutorés");
        formationBean.setModalitesEvaluation("Modalités d'évaluation");
        formationBean.setUrlFicheRNCP("Fiche RNCP URL");
        formationBean.setActivitesVisees("Activités Visées");
        formationBean.setPoursuiteEtudesComplement("Poursuite d'études Complément");
        formationBean.setSecteurActivitesAccessibles("Secteurs Activités Accessibles");
        formationBean.setInsertionPro("Insertion Professionnelle");
        formationBean.setCoutFormationComplement("Cout Formation Complément");
        formationBean.setModalitesInscription("Modalités Inscription");

        serviceFormation.getScreenBO().createContent();
        serviceFormation.fillContent(formationBean);
        serviceFormation.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.recherche(formationBean.getIntituleLong());
    }

    /***
     * Méthode permettant de rechercher en front-office un article et de vérifier que les champs saisis sont présents
     */
    @Test(dependsOnMethods = {"testCreationFormation"})
    public void testRechercheFormation() {
        FormationBean searchFormationBean = new FormationBean();
        //Onglet Caractéristiques
        searchFormationBean.setCode("0.81871970");
        searchFormationBean.setLangue("0");
        searchFormationBean.setCodeSI("codeSI");
        searchFormationBean.setIntituleCourt("Intitule Formation");
        searchFormationBean.setIntituleLong("Intitule Formation Long");
        searchFormationBean.setRubrique("Premiers pas");
        searchFormationBean.setStructure("Structures pour l'intégration et jeux de données");
        searchFormationBean.setCatalogueFormation("Catalogue");
        searchFormationBean.setNatureFormation("Nature Formation");
        searchFormationBean.setTypeDiplome("Licence");
        searchFormationBean.setGrade("Grade Formation");
        searchFormationBean.setDomaineLMD("Formation LMD");
        searchFormationBean.setDiscipline("Discipline");
        searchFormationBean.setMention("Mention");
        searchFormationBean.setParcours("Parcours");
        searchFormationBean.setNiveauRecrutement("Niveau Recrutement");
        searchFormationBean.setAnneePostBacSortie("Annee Post Bac");
        searchFormationBean.setNiveauSortie("Niveau Sortie");
        searchFormationBean.setLieu("Lieu");
        searchFormationBean.setLanguesEnseignements("Langue");
        searchFormationBean.setDureeFormation("Duree Formation");
        searchFormationBean.setAutreDuree("Autre durée formation");
        searchFormationBean.setPrincipalesMatieres("Matiere principale");
        searchFormationBean.setSecteursActivites("Secteur Activite");
        //formationBean.setMetiers("Métier");
        searchFormationBean.setCentresInterets("Centre d'interet");
        searchFormationBean.setSerieBacPreconisee("Serie de bac");
        searchFormationBean.setDomaineErasmus("Domaine Erasmus");
        //Onglet Affichage
        searchFormationBean.setObjectifs("Objectifs formation");
        //Carte Infos Générales
        searchFormationBean.setTitreCarteInfosGenerales("Titre Carte Générale");
        searchFormationBean.setLieuCommentaire("Lieu Commentaire");
        searchFormationBean.setStageCommentaire("Stage Commentaire");
        searchFormationBean.setLanguesEnseignementsCommentaire("Langues Enseignements Commentaire");
        //Carte Bloc Infos 1ère année
        searchFormationBean.setTitreCarteInfos("Titre Carte Infos Première Année");
        searchFormationBean.setChargeTravailHebdo("Charge Travail Hebdomadaire");
        searchFormationBean.setEffectifAttenduLibelle("Effectif Attendu Libelle");
        searchFormationBean.setEffectifAttenduChiffre("10");
        searchFormationBean.setEffectifAttenduCommentaire("Effectif Attendu Commentaire");
        searchFormationBean.setTauxReussiteLibelle("Taux Reussite Libelle");
        searchFormationBean.setTauxReussitePourcentage("10%");
        searchFormationBean.setTauxReussiteCommentaire("Taux Reussite Commentaire");
        //Carte Bloc Publics
        searchFormationBean.setTitreCartePublics("Titre Carte Publics");
        searchFormationBean.setSerieBacPreconiseeCommentaire("Serie Bac Preconisee");
        searchFormationBean.setPublicCible("Publics Ciblés");
        //Carte Bloc Rythme
        searchFormationBean.setTitreCarteRythme("Titre Carte Rythme");
        searchFormationBean.setCommentaireTempsPlein("Commentaire Temps Plein");
        searchFormationBean.setCommentaireCompatible("Commentaire Compatible");
        searchFormationBean.setCommentaireTempsAmenage("Commentaire Temps Amenagé");
        searchFormationBean.setCommentaireAlternance("Commentaire Alternance");
        searchFormationBean.setCommentaireHorsContrat("Commentaire Hors Contrat");
        searchFormationBean.setCommentaireContratApprentissage("Commentaire Contrat Apprentissage");
        searchFormationBean.setCommentaireContratProfessionnalisation("Commentaire Contrat Professionnalisation");
        searchFormationBean.setCommentairePresentiel("Commentaire Présentiel");
        searchFormationBean.setCommentairePartielDistance("Commentaire Partiellement à Distance");
        searchFormationBean.setCommentaireTotalDistance("Commentaire Totalement à Distance");
        //Carte Bloc Admission
        searchFormationBean.setTitreCarteAdmission("Titre Carte Admission");
        searchFormationBean.setModalitesSelection("Modalités Selection");
        searchFormationBean.setCapaciteAccueilLibelle("Capacité Accueil Libellé");
        searchFormationBean.setCapaciteAccueilChiffre("15");
        searchFormationBean.setCapaciteAccueilCommentaire("Capacite Accueil Commentaire");
        //Carte Bloc Temoignage
        searchFormationBean.setTitreCarteTemoignage("Titre Carte Temoignage");
        //Carte Bloc International
        searchFormationBean.setTitreCarteInternational("Titre Carte International");
        searchFormationBean.setCoursLangueEtrangereLibelle("Cours Langue Etrangere Libelle");
        searchFormationBean.setCoursLangueEtrangerePourcentage("15%");
        searchFormationBean.setCoursLangueEtrangereCommentaire("Cours Langue Etrangere Commentaires");
        searchFormationBean.setSejourEtranger("Sejour Etranger");
        //Carte Bloc Inscription
        searchFormationBean.setTitreCarteInscription("Titre Carte Inscription");
        searchFormationBean.setCoutFormation("Cout Formation");
        searchFormationBean.setModalitesInscription("Modalites d'inscriptions");
        //Carte Bloc Debouches
        searchFormationBean.setTitreCarteDebouches("Titre Carte Debouchés");
        searchFormationBean.setPoursuiteEtudes("Poursuites d'études");
        //Carte Bloc Video
        searchFormationBean.setTitreCarteVideo("Titre Carte Vidéo");
        //Carte Bloc Specificités
        searchFormationBean.setTitreCarteSpecificites("Titre Carte Spécificités");
        searchFormationBean.setSpecificites("Specificités");
        //Carte Bloc Professionnalisation
        searchFormationBean.setTitreCartePro("Titre Carte Professionnalisation");
        searchFormationBean.setProjetTutore("Projet Tutoré");
        searchFormationBean.setFormationPro("Formation Professionnelle");
        searchFormationBean.setTauxInsertionProChiffre("20");
        searchFormationBean.setTauxInsertionProCommentaire("Taux Insertion Commentaire");
        //Carte Bloc Libre 1
        searchFormationBean.setTitreCarteLibre1("Titre Carte Libre 1");
        searchFormationBean.setLibelleLien1("Libelle Lien 1");
        //Carte Bloc Libre 2
        searchFormationBean.setTitreCarteLibre2("Titre Carte Libre 2");
        searchFormationBean.setLibelleLien2("Libelle Lien 2");
        //Carte Bloc Libre 3
        searchFormationBean.setTitreCarteLibre3("Titre Carte Libre 3");
        searchFormationBean.setLibelleLien3("Libelle Lien 3");

        //Remplissage des CKEditors
        searchFormationBean.setResume("Resume");
        searchFormationBean.setEnjeux("Enjeux");
        searchFormationBean.setSpecificitesComplement("Specificités");
        searchFormationBean.setLieuComplement("Lieu complément");
        searchFormationBean.setResponsableFormation("Responsable Formation");
        searchFormationBean.setLaboratoires("Laboratoires");
        searchFormationBean.setEtablissements("Etablissements");
        searchFormationBean.setEntreprises("Entreprises");
        searchFormationBean.setConvention("Convention");
        searchFormationBean.setFormationRequise("Formation Requise");
        searchFormationBean.setPublicCible("Public Ciblé");
        searchFormationBean.setModalitesCandidature("Modalités Candidature");
        searchFormationBean.setModalitesCandidatureSpecifique("Modalités Candidature Spécifiques");
        searchFormationBean.setConditionAdmission("Condition d'Admission");
        searchFormationBean.setIntroductionProgramme("Introduction Programme");
        searchFormationBean.setContenuFormation("Contenu Formation");
        searchFormationBean.setRythmePremiereAnnee("Rythme Première Année");
        searchFormationBean.setDescriptionEquipePedagogique("Equipe Pédagogique");
        searchFormationBean.setDescriptionRythmeTempsPlein("Description Rythme Temps Plein");
        searchFormationBean.setDescriptionRythmeTempsAmenage("Description Rythme Temps Amenage");
        searchFormationBean.setDescriptionRythmeCompatibleProfessionnelle("Description Rythme Compatible Professionnelle");
        searchFormationBean.setDescriptionRythmeAlternance("Description Rythme Alternance");
        searchFormationBean.setDescriptionPresentiel("Description Rythme Presentiel");
        searchFormationBean.setDescriptionPartielDistance("Description Rythme Partiellement à distance");
        searchFormationBean.setDescriptionTotalDistance("Description Rythme Totalement à distance");
        searchFormationBean.setDescriptionStage("Informations Complémentaires Stage");
        searchFormationBean.setDescriptionSejour("Informations Complémentaires Stage Etrangers");
        searchFormationBean.setDescriptionProjet("Informations Complémentaires Projets Tutorés");
        searchFormationBean.setModalitesEvaluation("Modalités d'évaluation");
        searchFormationBean.setUrlFicheRNCP("Fiche RNCP URL");
        searchFormationBean.setActivitesVisees("Activités Visées");
        searchFormationBean.setPoursuiteEtudesComplement("Poursuite d'études Complément");
        searchFormationBean.setSecteurActivitesAccessibles("Secteurs Activités Accessibles");
        searchFormationBean.setInsertionPro("Insertion Professionnelle");
        searchFormationBean.setCoutFormationComplement("Cout Formation Complément");
        searchFormationBean.setModalitesInscription("Modalités Inscription");
        serviceRecherche.getURL(urlFO);
        WebElement element = serviceRecherche.rechercheItem(searchFormationBean.getIntituleLong());
        element.findElement(new By.ByCssSelector("a.item-title__element_title")).click();
        serviceFormation.controlFront(searchFormationBean);
    }
}

