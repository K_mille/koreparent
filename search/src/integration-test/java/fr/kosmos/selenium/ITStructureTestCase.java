package fr.kosmos.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.StructureBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceLibelle;
import fr.kosmos.selenium.service.ServiceRecherche;
import fr.kosmos.selenium.service.fiche.ServiceStructure;

/**
 * Suite de tests liés aux structures
 * Created by mathis.abdelaziz on 19/05/17.
 */
public class ITStructureTestCase extends KSupTestCase {

    private ServiceLibelle serviceLibelle;

    private ServiceRecherche serviceRecherche;

    private ServiceStructure serviceStructure;

    public ITStructureTestCase() {
    }

    /**
     * {@inheritDoc}
     */
    public void initScreen() {
        serviceLibelle = new ServiceLibelle(this);
        serviceRecherche = new ServiceRecherche(this);
        serviceStructure = new ServiceStructure(this);
    }

    /***
     * Méthode permettant de créer des libellés de structure, une structure, puis de la rechercher et de l'afficher en front-office.
     * Si aucun résultat n'est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test
    public void testCreationStructure() {
        serviceMenuBO.navigateToLibelle();
        serviceLibelle.createLibelle("60", "codeSt1", "LibelleStructure","LibelleStructure En");
        serviceLibelle.createLibelle("22","codeSt2", "LibelleVille","LibelleVille En");
        serviceMenuBO.navigateToContenu("annuairesup", "SAISIE_DEFAULTSTRUCTUREKSUP");
        StructureBean structureBean = new StructureBean();
        structureBean.setLibelleCourt("Libelle Structure Test");
        structureBean.setLibelleLong("Libelle Structure Test");
        structureBean.setRubrique("Les fiches et leurs listes");
        structureBean.setStructure("Structures pour l'intégration et jeux de données");
        structureBean.setTypeStructure("codeSt1");
        structureBean.setAdresse("Adresse Structure");
        structureBean.setCodePostal("44000");
        structureBean.setVille("Ville Structure");
        structureBean.setVilleMultiSelect("codeSt2");
        structureBean.setTelephone("01 02 03 04 05");
        structureBean.setMail("Structure mail");
        serviceStructure.getScreenBO().createContent();
        serviceStructure.fillContent(structureBean);
        serviceStructure.publishContent();
        serviceRecherche.getURL(urlFO);
        serviceRecherche.rechercheItem(structureBean.getLibelleLong());
    }

    /***
     * Méthode permettant de rechercher en front-office une structure et de vérifier que les champs saisis sont présents
     */
    @Test(dependsOnMethods = {"testCreationStructure"})
    public void testRechercheStructure() {
        StructureBean searchStructureBean = new StructureBean();
        searchStructureBean.setLibelleCourt("Libelle Structure Test");
        searchStructureBean.setLibelleLong("Libelle Structure Test");
        searchStructureBean.setRubrique("Les fiches et leurs listes");
        searchStructureBean.setStructure("Structures pour l'intégration et jeux de données");
        searchStructureBean.setTypeStructure("LibelleStructure");
        searchStructureBean.setAdresse("Adresse Structure");
        searchStructureBean.setCodePostal("44000");
        searchStructureBean.setVille("Ville Structure");
        searchStructureBean.setVilleMultiSelect("LibelleVille");
        searchStructureBean.setTelephone("01 02 03 04 05");
        searchStructureBean.setMail("Structure mail");
        serviceRecherche.getURL(urlFO);
        WebElement element = serviceRecherche.rechercheItem(searchStructureBean.getLibelleLong());
        element.findElement(new By.ByCssSelector("a.item-title__element_title")).click();
        serviceStructure.controlFront(searchStructureBean);
    }
}
