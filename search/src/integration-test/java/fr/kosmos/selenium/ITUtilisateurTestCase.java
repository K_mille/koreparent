package fr.kosmos.selenium;

import org.testng.annotations.Test;

import fr.kosmos.selenium.bean.UtilisateurBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceUtilisateur;

/**
 * Suite de tests liés aux comptes utilisateurs
 * Created by mathis.abdelaziz on 22/05/17.
 */
public class ITUtilisateurTestCase extends KSupTestCase {

    private ServiceUtilisateur serviceUtilisateur;

    public ITUtilisateurTestCase() {
    }

    /**
     * {@inheritDoc}
     */
    public void initScreen() {
        serviceUtilisateur = new ServiceUtilisateur(this);
    }

    /***
     * Méthode permettant de créer un utilisateur, de le rechercher, puis de le supprimer.
     * Si aucun résultat n'est trouvé, renseigne un message d'erreur et arrête le test.
     */
    @Test
    public void testCreationUtilisateur() {
        serviceMenuBO.navigateInMenuBO("menuAdministration", "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_UTILISATEUR&ACTION=ACCUEIL");
        UtilisateurBean utilisateur1 = new UtilisateurBean();
        utilisateur1.setCivilite("M.");
        utilisateur1.setNom("Nom Utilisateur 1");
        utilisateur1.setPrenom("Prénom Utilisateur 1");
        utilisateur1.setMail("Mail Utilisateur 1");
        utilisateur1.setLogin("login");
        utilisateur1.setPassword("pass");
        utilisateur1.setConfirmPassowrd("pass");
        serviceUtilisateur.createUtilisateur(utilisateur1);
        serviceUtilisateur.deleteContentDatagrid(utilisateur1.getLogin(), "td:nth-child(3)");
    }
}
