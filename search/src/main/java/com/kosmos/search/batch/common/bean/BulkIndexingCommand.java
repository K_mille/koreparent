package com.kosmos.search.batch.common.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.kosmos.search.index.bean.IndexingCommand;

/**
 * Wrapper de multiples commandes d'indexations (plusieurs commandes par Metatag / Fiche).
 *
 */
public class BulkIndexingCommand implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -9114858296650538451L;

    /**
     * Liste de commandes d'indexation.
     */
    private final List<IndexingCommand> indexingCommandList;

    /**
     * Constructeur.
     * @param indexingCommandList
     * Liste de commandes d'indexation
     */
    public BulkIndexingCommand(final List<IndexingCommand> indexingCommandList) {
        super();
        this.indexingCommandList = indexingCommandList;
    }

    /**
     * Constructeur par défaut.
     */
    public BulkIndexingCommand() {
        super();
        this.indexingCommandList = new ArrayList<>();
    }

    /**
     * Accesseur des commandes d'indexations.
     * @return la liste de commandes d'indexation gérée
     */
    public final List<IndexingCommand> getIndexingCommandList() {
        return indexingCommandList;
    }

    /**
     * Ajout d'une commande d'indexation. La nullité est vérifiée avant l'ajout dans la liste.
     * @param indexingCommand
     * Commande d'indexation
     */
    public final void addIndexingCommand(final IndexingCommand indexingCommand) {
        if (indexingCommand != null) {
            this.indexingCommandList.add(indexingCommand);
        }
    }

}
