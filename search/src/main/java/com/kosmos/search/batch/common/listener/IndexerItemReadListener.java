package com.kosmos.search.batch.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.ItemReadListener;

/**
 * Listener, utilisé pour les traces du traitement.
 *
 * @author cpoisnel
 *
 * @param <T>
 *            Type d'entrée en lecture
 */
public class IndexerItemReadListener<T> implements ItemReadListener<T> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexerItemReadListener.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beforeRead() {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("[Read] Lecture des entités");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void afterRead(final T item) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace(String.format("[Read] Lecture de l'entite '%s'. Retour %s", item == null ? "N/A" : item.getClass(), item));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onReadError(final Exception exception) {
        if (LOGGER.isTraceEnabled()) {
            LOGGER.trace("[Read] Exception à la lecture d'entités", exception);
        }
    }
}
