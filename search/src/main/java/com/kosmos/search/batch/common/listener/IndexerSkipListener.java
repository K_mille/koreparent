package com.kosmos.search.batch.common.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.SkipListener;

/**
 * Listener sur les éléments skippés (éléments exclus car une exception est survenue).
 *
 * @author cpoisnel
 *
 * @param <E>
 *            Type d'entrée
 * @param <S>
 *            Type de sortie
 */
public class IndexerSkipListener<E, S> implements SkipListener<E, S> {

    private static final Logger LOGGER = LoggerFactory.getLogger(IndexerSkipListener.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSkipInRead(final Throwable paramThrowable) {
        LOGGER.error("[Skip] Elément ignoré à la lecture des données", paramThrowable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSkipInWrite(final S item, final Throwable paramThrowable) {
        LOGGER.error("[Skip] Erreur à l'écriture de la donnée {}", item, paramThrowable);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onSkipInProcess(final E item, final Throwable paramThrowable) {
        LOGGER.error("[Skip] Elément ignoré au traitement (process) de la donnée {}", item, paramThrowable);
    }
}
