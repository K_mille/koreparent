package com.kosmos.search.index.bean;

import java.io.Serializable;
import java.util.List;

/**
 * Classe de définition des accès. Ces accès sont ensuite vérifiés par les filtres de requêtes Elasticsearch.
 * @author cpoisnel
 *
 */
public class AccessControl implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -2068603640494827689L;

    /**
     * Type de restriction pour les fiches (0,1,2).
     */
    private String groupesFicheRestriction;

    /**
     * Liste groupes de fiches (ID).
     */
    private List<String> groupesFiche;

    /**
     * Liste de regex par rubrique (rubrique principale + rubrique de publication).
     */
    private List<String> groupesRubrique;

    public final List<String> getGroupesFiche() {
        return groupesFiche;
    }

    public final void setGroupesFiche(final List<String> groupesFiche) {
        this.groupesFiche = groupesFiche;
    }

    public final List<String> getGroupesRubrique() {
        return groupesRubrique;
    }

    public final void setGroupesRubrique(final List<String> groupesRubrique) {
        this.groupesRubrique = groupesRubrique;
    }

    /**
     * @return the groupesFicheRestriction
     */
    public String getGroupesFicheRestriction() {
        return groupesFicheRestriction;
    }

    /**
     * @param groupesFicheRestriction the groupesFicheRestriction to set
     */
    public void setGroupesFicheRestriction(final String groupesFicheRestriction) {
        this.groupesFicheRestriction = groupesFicheRestriction;
    }

    // TODO Produit : Définir les structures ?

}
