package com.kosmos.search.index.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by camille.lebugle on 10/02/17.
 */
public class CompletionBean implements Serializable {

    private static final long serialVersionUID = -717439472453475245L;

    @JsonProperty("input")
    private Map<String, String> input;

    public CompletionBean() {
        this.input = new HashMap<>();
    }

    public Map<String, String> getInput() {
        return input;
    }

    public void setInput(final Map<String, String> input) {
        this.input = input;
    }
}
