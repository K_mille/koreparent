package com.kosmos.search.index.bean;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.constraints.NotNull;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.util.Assert;

/**
 * Bean de contexte de fiches sérialisées. Permet de sauvegarder les informations de sérialisation des metatags déjà traités (nécessaire pour éviter les sérialisations de cycles)
 *
 * @author cpoisnel
 */
public class FicheTreeNode {

    @NotNull
    private final String name;

    /**
     * Noeud de fiche parent.
     */
    private FicheTreeNode parent;

    /**
     * Enfants, pour navigation bi-directionnelle
     */
    private Set<FicheTreeNode> children = new HashSet<>();

    private Set<Long> idMetatags = new HashSet<>();

    /**
     * Ajout d'un noeud. Synchronise parent/enfant
     * @param node
     * Noeud à ajouter
     * @return <i>true</i> si le noeud est nouveau dans l'arborescence, <i>false</i> sinon
     */
    public boolean addChild(FicheTreeNode node) {
        boolean newNode = this.children.add(node);
        if (newNode) {
            node.setParent(this);
        }
        return newNode;
    }

    /**
     * Récupère le noeud nommé dans les fils.
     * @param name
     * Nom du noeud
     * @return le noeud dans les enfants, null si non trouvé
     */
    public FicheTreeNode getChild(String name) {
        for (FicheTreeNode node : children) {
            if (node.getName().equals(name)) {
                return node;
            }
        }
        return null;
    }

    /**
     * Calcule le niveau du noeud.
     * @return le niveau par rapport à la racine (=0)
     */
    public int getLevel() {
        if (isRoot()) {
            return 0;
        } else {
            return 1 + parent.getLevel();
        }
    }

    /**
     * Constructeur d'un noeud dans l'arborescence.
     * @param name
     * Nom du noeud
     */
    public FicheTreeNode(String name) {
        Assert.notNull(name,"Le nom du noeud ne doit pas être null");
        this.name = name;
    }

    public String getName() {
        return name;
    }

    /**
     * Accesseur des IDs metatags traités dans cette branche.
     * @return les metatags
     */
    public Set<Long> getIdMetatags() {
        return idMetatags;
    }

    /**
     * Ajoute un ou plusieurs IDs de metatags traités dans cette branche.
     * @param idMetatags
     * IDs metatags
     */
    public void addIdMetatags(Long... idMetatags) {
        for (Long idMetatag : idMetatags) {
            this.idMetatags.add(idMetatag);
        }
    }

    public FicheTreeNode getParent() {
        return parent;
    }

    public void setParent(FicheTreeNode parent) {
        this.parent = parent;
    }

    /**
     * Renvoie vrai si noeud est la racine de l'arbre.
     * @return <i>true</i> si le noeud est la racine (pas de parent)
     */
    public boolean isRoot() {
        return parent == null;
    }

    public Set<FicheTreeNode> getChildren() {
        return children;
    }

    /**
     * Vérifie l'égalité du noeud. Un noeud est égal si son nom et son parent sont égaux.
     * @param o
     * Noeud à vérifier
     * @return <i>true</i> si égal, <i>false</i> sinon
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FicheTreeNode that = (FicheTreeNode) o;
        return new EqualsBuilder().append(name, that.name).append(parent, that.parent).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).append(parent).toHashCode();
    }

    /**
     * Vérifie que l'ID metatag n'est pas présent dans les parents.
     * @return <i>true</i> si le metatag est présent dans la branche de l'arbre, <i>false</i> sinon
     */
    public boolean hasMetatagInParents(Long idMetatag){
        if (!isRoot()) {
            // Evaluation paresseuse (si l'ID metatag est contenu dans la liste du parent, pas d'appel récursif)
            return parent.getIdMetatags().contains(idMetatag) || parent.hasMetatagInParents(idMetatag);
        }
        return false;
    }


    /**
     * Construction du chemin du noeud dans l'arbre.
     * @return le chemin du noeud dans l'arbre (les enfants ne sont pas concernés)
     */
    @Override
    public String toString() {
        return StringUtils.join(getPath(),"/");
    }

    /**
     * Construction du chemin du noeud dans l'arbre.
     * @return la liste composée des noms des noeud de l'arbre
     */
    public List<String> getPath() {
        List<String> pathList;
        if (isRoot()){
            pathList= Arrays.asList("/");
        }
        else {
            pathList= parent.getPath();
            pathList.add(this.getName());
        }
        return  pathList;
    }
}
