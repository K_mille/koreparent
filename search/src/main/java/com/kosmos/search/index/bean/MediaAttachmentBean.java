package com.kosmos.search.index.bean;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Media bean, analysé par ES. Il s'agit de la partie spécifique gérée par le plugin mapper-attachments
 * <p>Plugin mapper-attachments. Voir <a href="https://github.com/elastic/elasticsearch-mapper-attachments">la documentation</a>.</p>
 * @author cpoisnel
 *
 */
public class MediaAttachmentBean implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 5900961244294276126L;


    /**
     * Nom du document.
     */
    @JsonProperty("_name")
    private String name;

    /**
     * Type du document, si non renseigné, il sera déterminé à partir du document.
     */
    @JsonProperty("_content_type")
    private String contentType;

    /**
     * Langue du document (fr, en, ...).
     */
    @JsonProperty("_language")
    private String language;

    /**
     * Contenu encodé en base 64.
     * <p>Attention, la taille du fichier peut-être importante et le ficher est chargé en mémoire ! (fonctionnement d'ES, pas de streaming de fichier à ce jour).</p>
     */
    @JsonProperty("_content")
    private byte[] content;

    @JsonProperty("_content_length")
    private Long contentLength;

    public final String getName() {
        return name;
    }

    public final void setName(final String name) {
        this.name = name;
    }

    public final String getContentType() {
        return contentType;
    }

    public final void setContentType(final String contentType) {
        this.contentType = contentType;
    }

    public final String getLanguage() {
        return language;
    }

    public final void setLanguage(final String language) {
        this.language = language;
    }

    public final byte[] getContent() {
        return content;
    }

    public final void setContent(final byte[] content) {
        this.content = content;
    }

    public final Long getContentLength() {
        return contentLength;
    }

    public final void setContentLength(final Long contentLength) {
        this.contentLength = contentLength;
    }



    /**
     * Affichage du bean (au format texte). Le contenu du fichier n'est pas affiché ! (base64).
     */
    @Override
    public String toString() {
        return "MediaAttachmentBean [name=" + name + ", contentType=" + contentType + ", language=" + language + ", content=NON_AFFICHE]";
    }

}
