package com.kosmos.search.index.mapper;

/**
 * Clés du contexte de sérialisation (Jackson).
 * @author cpoisnel
 *
 */
public final class ContextAttributeSerializerUtils {

    /**
     * Clé de la donnée de langue stockée dans le contexte de sérialisation Jackson.
     */
    public static final String CONTEXT_LANGUAGE = "CONTEXT_LANGUAGE";

    /**
     * Constructeur privé, classe non instanciable.
     */
    private ContextAttributeSerializerUtils() {}
}
