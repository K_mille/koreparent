package com.kosmos.search.index.mapper;

import java.util.Collection;

import javax.annotation.PostConstruct;

import org.apache.commons.collections4.CollectionUtils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.DefaultDeserializationContext;
import com.fasterxml.jackson.databind.ser.DefaultSerializerProvider;

/**
 * CoreSearchMapper, Implémentation du mapper Core pour l'enrichissement du flux JSON (conçu initialisement pour Elasticsearch).
 * <p>
 *     Les modules sont initialisés après la création de la factory Spring.
 * </p>
 *
 * @author cpoisnel
 *
 */
public class CoreSearchMapper extends ObjectMapper {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -3531363030279891630L;

    /**
     * Modules Jackson pour l'indexation, utilisés pour la sérialisation des beans.
     */
    private transient Collection<Module> modules;

    /**
     * Constructeur par défaut (appelle le constructeur de la classe parente).
     */
    public CoreSearchMapper() {
        super();
    }

    /**
     * Constructeur (appelle le constructeur de la classe parente).
     * @param jsonFactory
     * Factory Json, qui sert à définir le parser et le writer Json
     * @param serializerProvider
     * Serializer par défaut (en fonction du type d'objet)
     * @param deserializationContext
     * Contexte de désérialisation
     */
    public CoreSearchMapper(final JsonFactory jsonFactory, final DefaultSerializerProvider serializerProvider, final DefaultDeserializationContext deserializationContext) {
        super(jsonFactory, serializerProvider, deserializationContext);
    }

    /**
     * Constructeur (appelle le constructeur de la classe parente).
     * @param jsonFactory
     * Factory Json, qui sert à définir le parser et le writer Json
     */
    public CoreSearchMapper(final JsonFactory jsonFactory) {
        super(jsonFactory);
    }

    /**
     * Constructeur (appelle le constructeur de la classe parente).
     * @param objectMapper
     * Mapper d'origine.
     */
    public CoreSearchMapper(final ObjectMapper objectMapper) {
        super(objectMapper);
    }

    public Collection<Module> getModules() {
        return modules;
    }

    public void setModules(final Collection<Module> modules) {
        this.modules = modules;
    }

    /**
     * Enregistrement des modules de traitement (plugins avec serializers / deserializers) après initialisation de la factory Spring.
     */
    @PostConstruct
    protected void registerModules() {
        if (CollectionUtils.isNotEmpty(modules)) {
            this.registerModules(modules);
        }
    }
}
