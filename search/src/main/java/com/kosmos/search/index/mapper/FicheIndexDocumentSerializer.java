package com.kosmos.search.index.mapper;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kosmos.search.index.bean.FicheIndexDocument;
import com.kosmos.search.index.bean.FicheTreeNode;

/**
 * Sérialisation des FicheIndexDocument. Permet de définir la langue de la fiche et la sérialisation éventuelle de libellés (selon la bonne langue).
 * <p>
 * Toutes les sérialisations (pour l'indexation) spécifiques des objets metiers *Bean <b>doivent</b> hériter de cette classe, pour pouvoir gérer le contexte de langue.
 * </p>
 *
 * @author cpoisnel
 *
 */
public class FicheIndexDocumentSerializer extends JsonSerializer<FicheIndexDocument> implements ContextualSerializer {

    private static final Logger LOG = LoggerFactory.getLogger(FicheIndexDocumentSerializer.class);

    private final JsonSerializer<FicheIndexDocument> defaultSerializer;

    /**
     * Construit le serializer spécifique avec un sérialiseur délégué.
     * @param defaultSerializer
     * Serializer délégué
     *
     */
    public FicheIndexDocumentSerializer(final JsonSerializer<FicheIndexDocument> defaultSerializer) {
        this.defaultSerializer = defaultSerializer;
    }

    /**
     * <p>Pas d'action par défaut sur la création du contexte, on renvoie uniquement l'instance (sans action particulière)</p>
     * {@inheritDoc}
     */
    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider prov, final BeanProperty property) throws JsonMappingException {
        LOG.debug("Initialisation du contexte de sérialisation '{}'", FicheIndexDocumentSerializer.class);
        return this;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    public void serialize(final FicheIndexDocument value, final JsonGenerator gen, final SerializerProvider serializers) throws IOException, JsonProcessingException {
        // Au moment de la sérialisation, on ajoute la langue dans le contexte (utile pour les libellés par exemple).
        initContext(value, gen, serializers);
        defaultSerializer.serialize(value, gen, serializers);
        destroyContext(value, gen, serializers);
    }

    /**
     * Initialisation du contexte de sérialisation du bean.
     * <p>Au moment de la sérialisation, s'il y a une langue définie sur la fiche, on va l'ajouter dans le contexte du {@link SerializerProvider}</p>
     * @param value
     *          Bean métier.
     * @param gen
     *          Générateur de flux Json
     * @param serializers
     *          Serializer à utiliser
     */
    protected void initContext(final FicheIndexDocument value, final JsonGenerator gen, final SerializerProvider serializers) {
        FicheIndexContext.context.set(new FicheTreeNode("/"));
        FicheIndexContext.ficheIndexDocument.set(value);
        FicheIndexContext.typeFiche.set(value.getTypeFiche());
    }

    protected void destroyContext(final FicheIndexDocument value, final JsonGenerator gen, final SerializerProvider serializers) {
        FicheIndexContext.context.remove();
        FicheIndexContext.ficheIndexDocument.remove();
        FicheIndexContext.typeFiche.remove();
    }

    /**
     * Contexte d'indexation de fiche (dans le ThreadLocal).
     * <p>
     *     Solution par défaut : c'est à défaut de pouvoir utiliser le contexte de sérialisation qui n'est pas partagé.
     *     Et c'est pour éviter de le mettre dans le contexte de mapper (qui lui est partagé, mais commun à l'ensemble des appels).
     * </p>
     */
    public static final class FicheIndexContext {

        /**
         * Variable stockant le contexte de sérialisation.
         */
        public static final ThreadLocal<FicheTreeNode> context = new ThreadLocal<>();

        /**
         * Variable stockant la liste des aggrégations transverses.
         */
        public static final ThreadLocal<FicheIndexDocument> ficheIndexDocument = new ThreadLocal<>();

        /**
         * Variable stockant le type de la fiche
         */
        public static final ThreadLocal<String> typeFiche = new ThreadLocal<>();
    }
}
