package com.kosmos.search.index.mapper.context;

import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.kosmos.search.index.mapper.serializer.AggregationSerializer;
import com.kportal.cms.objetspartages.annotation.Aggregation;

/**
 * Created by camille.lebugle on 05/04/17.
 */
public class AggregationContextSerializer implements ContextualSerializer {

    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider prov, final BeanProperty paramBeanProperty) throws JsonMappingException {
        //Recherche de l'annotation @Aggregation sur les attributs
        if (null != paramBeanProperty) {
            Aggregation aggregation = paramBeanProperty.getAnnotation(Aggregation.class);
            if (aggregation != null) {
                return new AggregationSerializer(aggregation.name());
            }
        }
        return null;
    }
}
