package com.kosmos.search.index.mapper.context;

import java.io.IOException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.ContextualSerializer;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kosmos.search.index.mapper.serializer.CoreDateSerializer;
import com.kportal.cms.objetspartages.annotation.Aggregation;

/**
 * Contexte de serialisation des dates afin de gérer les annotations Aggregation.
 */
public class CoreDateContextSerializer extends StdSerializer<Date> implements ContextualSerializer {

    public CoreDateContextSerializer() {
        super(Date.class);
    }

    @Override
    public JsonSerializer<?> createContextual(final SerializerProvider prov, final BeanProperty property) throws JsonMappingException {
        if (null != property) {
            Aggregation aggregation = property.getAnnotation(Aggregation.class);
            if (null != aggregation) {
                return new CoreDateSerializer(aggregation.name());
            }

        }
        return DateSerializer.instance;
    }

    @Override
    public void serialize(final Date value, final JsonGenerator gen, final SerializerProvider provider) throws IOException {
        DateSerializer.instance.serialize(value, gen, provider);
    }
}
