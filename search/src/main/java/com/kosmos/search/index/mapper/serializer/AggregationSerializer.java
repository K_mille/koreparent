package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.kosmos.search.index.bean.FicheIndexDocument;
import com.kosmos.search.index.mapper.FicheIndexDocumentSerializer;

/**
 * Serializer permettant de pousser dans le thread local les listes des valeurs utilisés pour les différentes aggrégations
 */
public class AggregationSerializer extends DelegateStdSerializer<String> {

    private String typeAggregation;

    public AggregationSerializer(String typeAggregation) {
        super(String.class);
        this.typeAggregation = typeAggregation;
    }

    @Override
    public void serialize(final String value, final JsonGenerator gen, final SerializerProvider provider) throws IOException {
        if (StringUtils.isNotEmpty(value)) {
            FicheIndexDocument ficheIndexDocument = FicheIndexDocumentSerializer.FicheIndexContext.ficheIndexDocument.get();
            Map<String, Set<Object>> aggregationMap = ficheIndexDocument.getAggregation();
            Set<Object> valueList = aggregationMap.get(this.typeAggregation);
            if (valueList == null) {
                valueList = new HashSet<>();
                aggregationMap.put(typeAggregation, valueList);
            }
            final String[] codes = value.split(";");
            for (final String code : codes) {
                if (StringUtils.isNotBlank(code) && !"0000".equals(code)) {
                    valueList.add(code);
                }
            }
        }
        super.serialize(value, gen, provider);
    }
}
