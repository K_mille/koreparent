package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.DateSerializer;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kosmos.search.index.bean.FicheIndexDocument;
import com.kosmos.search.index.mapper.FicheIndexDocumentSerializer;

/**
 * Serializer spécifique pour les dates afin de gérer l'annotation Aggregation
 */
public class CoreDateSerializer extends StdSerializer<Date> {

    private String typeAggregation;

    public CoreDateSerializer(String typeAggregation) {
        super(Date.class);
        this.typeAggregation = typeAggregation;
    }

    @Override
    public void serialize(final Date value, final JsonGenerator gen, final SerializerProvider provider) throws IOException {
        if (StringUtils.isNotBlank(this.typeAggregation)) {
            FicheIndexDocument ficheIndexDocument = FicheIndexDocumentSerializer.FicheIndexContext.ficheIndexDocument.get();
            Map<String, Set<Object>> aggregationMap = ficheIndexDocument.getAggregation();
            Set<Object> valueList = aggregationMap.get(this.typeAggregation);
            if (valueList == null) {
                valueList = new HashSet<>();
                aggregationMap.put(typeAggregation, valueList);
            }
            valueList.add(value);
        }
        DateSerializer.instance.serialize(value, gen, provider);
    }
}
