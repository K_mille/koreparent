package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

/**
 * Serializer permettant de chainer un autre serializer.
 */
public class DelegateStdSerializer<T> extends StdSerializer<T> {

    /**
     * Serializer appelé après l'appel de la sérialisatin du serializer courant
     */
    private JsonSerializer<T> delegate;

    protected DelegateStdSerializer(final Class<T> clazz) {
        super(clazz);
    }

    @Override
    public void serialize(final T value, final JsonGenerator gen, final SerializerProvider provider) throws IOException {
        if (delegate != null) {
            delegate.serialize(value, gen, provider);
        }
    }

    public JsonSerializer<T> getDelegate() {
        return delegate;
    }

    public void addDelegate(final JsonSerializer<T> serializer) {
        if (serializer != null) {
            if (this.delegate == null) {
                this.delegate = serializer;
            } else {
                if (DelegateStdSerializer.class.isAssignableFrom(this.delegate.getClass())) {
                    ((DelegateStdSerializer) this.delegate).addDelegate(serializer);
                }
            }
        }
    }
}
