package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.Triple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.kosmos.search.exception.IndexationException;
import com.kosmos.search.index.bean.FicheTreeNode;
import com.kosmos.search.index.mapper.FicheIndexDocumentSerializer;
import com.kosmos.search.utils.IndexerUtil;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.AbstractFiche;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.objetspartages.util.MetatagUtils;
import com.univ.utils.FicheUnivHelper;

/**
 * Sérialisation de fiches. Gère la sérialisation récursive (jusqu'au niveau getMaxLevel).
 * Lors d'un cycle rencontré dans la sérialisation, les fiches enfants sont sérialisées pour conserver uniquement les références (code / langue / idMetatag).
 */
public class FicheSerializer extends AbstractLanguageSerializer<String> {



    /**
     * Niveau maximun de sérialisation JSON (multiple de 2, car il y a item -> fiche_value -> item2 -> fiche_value).
     * Une sérialisation demandée au maxLevel peut dépasser ce maxLevel. Néanmoins, les sérialisations de sous-objets ne seront pas traitées ensuite (uniquement le code/langue de la fiche).
     */
    private int maxLevel;

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -6590254568954269002L;

    /**
     * The Constant FICHE_ID_METATAG.
     */
    private static final String FICHE_ID_METATAG = "fiche_id_metatag";

    /**
     * The Constant FICHE_VALUE.
     */
    private static final String FICHE_VALUE = "fiche_value";

    private static final String FICHE_CODE = "code";

    private static final String FICHE_LANGUE = "langue";

    /**
     * The Constant FICHE_TYPE.
     */
    private static final String FICHE_TYPE = "fiche_type";

    /**
     * The Constant LOG.
     */
    private static final Logger LOG = LoggerFactory.getLogger(FicheSerializer.class);

    /**
     * The type.
     */
    private final String type;

    /**
     * Constructeur.
     *
     * @param type
     *            String - Le type.
     * @param maxLevel
     * Niveau maximum de sérialisation autorisé sur une sérialisation de fiche
     */
    public FicheSerializer(final String type, final int maxLevel) {
        super(String.class);
        this.type = type;
        this.maxLevel = maxLevel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void serialize(final String codes, final JsonGenerator jsonGen, final SerializerProvider provider) throws IOException {
        // Lecture dans le contexte de sérialisation de la fiche les données sur les parents / enfants
        if (StringUtils.isBlank(codes)) {
            jsonGen.writeNull();
            return;
        }
        // On récupère le chemin du champ (JSON)
        List<String> pathAttribute = getPathAttributeJson(jsonGen.getOutputContext());
        // On récupèle contexte de fiche, qui sert à savoir quel metatag a été traité dans l'arborescence
        FicheTreeNode rootNode = FicheIndexDocumentSerializer.FicheIndexContext.context.get();
        if (null == rootNode) {
            LOG.warn("Impossible de récuperer le contexte d'indexation pour le chemin {}", pathAttribute);
            throw new IndexationException("Impossible de récupérer le contexte d'indexation des fiches");
        }
        // Création du noeud correspondant au chemin
        FicheTreeNode currentNode = this.getOrCreateNode(rootNode, pathAttribute);
        jsonGen.writeStartArray();
        for (final String frg : StringUtils.split(codes, ';')) {
            // extraction des données contenues dans le fragment de codes courant
            final Triple<String, String, String> datas = IndexerUtil.getRessourceCodeLangueType(frg);
            final String type = this.getType(datas);
            // récupération de la fiche associée
            final FicheUniv fiche = this.getFiche(datas, type, jsonGen, provider);
            if (null == fiche) {
                LOG.warn("La fiche recherchée est introuvable avec les données suivantes (code='{}',type='{}')", frg, type);
                continue;
            }
            jsonGen.writeStartObject();
            // extraction des données metatag
            final MetatagBean metatag = MetatagUtils.lireMeta(fiche);
            if (null == metatag) {
                LOG.warn("Le metatag recherché est introuvable avec les données suivantes (code='{}',type='{}')", fiche.getCode(), type);
                continue;
            }
            // extraction des données de fiche
            final String nomObjet = ReferentielObjets.getNomObjet(metatag.getMetaCodeObjet());
            final AbstractFicheBean bean = ((AbstractFiche<?>) fiche).getPersistenceBean();
            Long idMetatag = metatag.getId();
            jsonGen.writeNumberField(FICHE_ID_METATAG, idMetatag);
            jsonGen.writeStringField(FICHE_TYPE, nomObjet);
            // Ajout du métatag dans le noeud courant de sérialisation
            currentNode.addIdMetatags(idMetatag);
            if (!currentNode.hasMetatagInParents(idMetatag)) {
                if (currentNode.getLevel() <= getMaxLevel()) {
                    LOG.debug("Sérialisation de la fiche  {} (chemin : {})", bean, currentNode);
                    jsonGen.writeObjectField(FICHE_VALUE, bean);
                } else {
                    LOG.debug("Niveau de profondeur sérialisation maximum atteint. Niveau : {} (niveau max : {}). Fiche {}", Arrays.asList(currentNode.getLevel(), maxLevel, bean));
                    jsonGen.writeObjectFieldStart(FICHE_VALUE);
                    jsonGen.writeStringField(FICHE_CODE, bean.getCode());
                    jsonGen.writeStringField(FICHE_LANGUE, bean.getLangue());
                    jsonGen.writeEndObject();
                }
            } else {
                LOG.debug("Elément déjà traité dans l'arborescence. Metatag : {}. Niveau : {}. Fiche {}", Arrays.asList(idMetatag, currentNode.getLevel(), maxLevel, bean));
                jsonGen.writeObjectFieldStart(FICHE_VALUE);
                jsonGen.writeStringField(FICHE_CODE, bean.getCode());
                jsonGen.writeStringField(FICHE_LANGUE, bean.getLangue());
                jsonGen.writeEndObject();
            }
            jsonGen.writeEndObject();
        }
        jsonGen.writeEndArray();
    }

    /**
     * @param datas : Triple : code | langue | type
     * {@link FicheSerializer#serialize(String, JsonGenerator, SerializerProvider)}.
     */
    private FicheUniv getFiche(final Triple<String, String, String> datas, final String type, final JsonGenerator jsonGen, final SerializerProvider provider) {
        if (null == datas || StringUtils.isBlank(type)) {
            return null;
        }
        try {
            // récupération de la langue contextuelle si vide
            String langue = datas.getMiddle();
            if (StringUtils.isBlank(langue)) {
                langue = super.processLanguage(jsonGen, provider);
            }
            // récupération du code (non nul)
            final String code = datas.getLeft();
            return FicheUnivHelper.getFiche(type, code, langue);
        } catch (final Exception e) {
            LOG.error("Exception lors de la lecture de la fiche '{}'", datas, e);
        }
        return null;
    }

    /**
     * @param datas : Triple : code | langue | type
     * {@link FicheSerializer#serialize(String, JsonGenerator, SerializerProvider)}.
     */
    private String getType(final Triple<String, String, String> datas) {
        if (null == datas) {
            return StringUtils.EMPTY;
        }
        String type = datas.getRight();
        if (StringUtils.isBlank(type)) {
            type = this.type;
        }
        return type;
    }



    /**
     * Méthode récursive permettant d'ajouter le chemin dans une liste.
     * @param context
     * Contexte de sérialisation
     * @return une liste du chemin de sérialisation de type [att1, att1_fils]
     */
    protected List<String> getPathAttributeJson(JsonStreamContext context) {
        final List<String> path;
        if (context.inRoot()) {
            path = new ArrayList<>();
        } else {
            path = getPathAttributeJson(context.getParent());
            if (null != context.getCurrentName()) {
                path.add(context.getCurrentName());
            }
        }
        return path;
    }

    /**
     * Créée ou récupère le noeud à partir du contexte. Créée les noeuds intermédiaires (liés à une sérialisation).
     * @param rootNode
     * Noeud racine
     * @param path
     * Chemin relatif
     * @return le noeud correspondant au chemin en paramètre (parcours du chemin du premier au dernier élement.
     */
    protected FicheTreeNode getOrCreateNode(FicheTreeNode rootNode, List<String> path) {
        FicheTreeNode itemNode = rootNode;
        for (String itemPath : path) {
            FicheTreeNode child = itemNode.getChild(itemPath);
            if (null == child) {
                child = new FicheTreeNode(itemPath);
                itemNode.addChild(child);
            }
            itemNode = child;
        }
        // On retourne le dernier item initialisé, donc le dernier noeud connu
        return itemNode;
    }

    protected int getMaxLevel() {
        return maxLevel;
    }

    protected void setMaxLevel(int maxLevel) {
        this.maxLevel = maxLevel;
    }
}
