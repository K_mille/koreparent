package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Sérialisation des rubriques.
 *
 * @author cpoisnel
 *
 */
public class RubriqueSerializer extends StdSerializer<String> {

    /**
     * Singleton instance to use.
     */
    public static final  RubriqueSerializer instance = new RubriqueSerializer();

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 654777125748674811L;

    private static final Logger LOG = LoggerFactory.getLogger(RubriqueSerializer.class);

    public static final String RUBRIQUE_CODE = "rubrique_code";

    public static final String RUBRIQUE_VALUE = "rubrique_value";

    public static final String RUBRIQUE_CATEGORY = "rubrique_category";

    public static final String RUBRIQUE_ID = "rubrique_id";

    private final transient ServiceRubrique serviceRubrique;

    /**
     * Constructeur, initialisation des services.
     */
    public RubriqueSerializer() {
        super(String.class);
        this.serviceRubrique = ServiceManager.getService(ServiceRubrique.class);
    }

    /**
     * Sérialise le code rubrique avec le code, le libellé et la catégorie.
     * <p>{@inheritDoc}</p>
     */
    @Override
    public void serialize(final String codesSerialise, final JsonGenerator jgen, final SerializerProvider provider) throws IOException {
        if (StringUtils.isNotEmpty(codesSerialise)) {
            jgen.writeStartArray();
            final String[] codes = codesSerialise.split(";");
            for (final String code : codes) {
                final String codeLabel;
                if ("".equals(code) || "0000".equals(code)) {
                    LOG.debug("Code rubrique par défaut '{}', le code 'null' est conservé", code);
                    codeLabel = null;
                } else {
                    codeLabel = code;
                }
                jgen.writeStartObject();
                jgen.writeStringField(RUBRIQUE_CODE, codeLabel);
                if (codeLabel != null) {
                    final RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(code);
                    if (rubriqueBean != null) {
                        jgen.writeStringField(RUBRIQUE_VALUE, rubriqueBean.getIntitule());
                        jgen.writeStringField(RUBRIQUE_CATEGORY,rubriqueBean.getCategorie());
                        jgen.writeNumberField(RUBRIQUE_ID, rubriqueBean.getId());
                    } else {
                        LOG.warn("Rubrique non trouvé pour le code {} . Le code est conservé.", code);
                    }
                }
                jgen.writeEndObject();
            }
            jgen.writeEndArray();
        } else {
            jgen.writeNull();
        }
    }
}
