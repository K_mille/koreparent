package com.kosmos.search.index.mapper.serializer;

import java.io.IOException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.jsbsoft.jtf.textsearch.RechercheFmt;
import com.univ.utils.ContexteUniv;
import com.univ.utils.ContexteUtil;
import com.univ.xhtml.HTMLParser;

/**
 * Sérialisation des toolbox.
 * @author cpoisnel
 *
 */
public class ToolboxSerializer extends AbstractLanguageSerializer<String> {

    /**
     * Singleton instance to use.
     */
    public final static ToolboxSerializer instance = new ToolboxSerializer();

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -6306628163276126066L;

    private static final Logger LOG = LoggerFactory.getLogger(ToolboxSerializer.class);

    /**
     * Toolbox (String).
     */
    public ToolboxSerializer() {
        super(String.class);
    }

    /**
     * Interprète le contenu de la toolbox (en mode non connecté).
     * <p>
     * {@inheritDoc}
     * </p>
     */
    @Override
    public void serialize(final String content, final JsonGenerator jgen, final SerializerProvider provider) throws IOException {
        LOG.trace("Demande de sérialisation du contenu d'une toolbox : '{}'", content);
        String contenuHtml = StringUtils.EMPTY;
        if (StringUtils.isNotEmpty(content)) {
            ContexteUniv ctx = new ContexteUniv(StringUtils.EMPTY);
            if(ContexteUtil.getContexteUniv() != null) {
                ctx.setInfosSite(ContexteUtil.getContexteUniv().getInfosSite());
            }else{
                LOG.debug("Initialisation d'un contexte univ");
                ctx = ContexteUtil.setContexteSansRequete();
                ctx.setConnectionLongue(true);
            }
            try {
                final HTMLParser parser = new HTMLParser();
                parser.setInputHtml(RechercheFmt.formaterEnHTML(ctx, content));
                contenuHtml = RechercheFmt.formaterTexteRecherche(parser.extractString(false), false, false);
                LOG.debug(String.format("Contenu de toolbox formaté. Résultat : %s", contenuHtml));
            } catch (final Exception e) {
                LOG.warn(String.format("Impossible de formater en HTML le contenu '%s'. Le contenu de la toolbox est conservé sans traitement.", content), e);
                contenuHtml = content;
            } finally {
                ctx.release();
            }
        }
        jgen.writeString(contenuHtml);
    }
}
