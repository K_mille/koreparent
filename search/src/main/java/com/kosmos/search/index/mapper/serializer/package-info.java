/**
 * Instances des Serializers.
 *
 * <h2>Fonctionnement</h2>
 * Chaque classe de ce package implémente {@link com.fasterxml.jackson.databind.ser.std.StdSerializer} et possède la méthode
 * <pre>
 *     public void serialize(final String codesSerialise, final JsonGenerator jgen, final SerializerProvider provider) throws IOException
 * </pre>
 *  L'implémentation de cette méthode doit utiliser le générateur jgen pour ajouter des balises JSON (array / value), basées sur les attributs de la classe initialisée depuis une classe étendant
 *  {@link com.fasterxml.jackson.databind.ser.ContextualSerializer}. Les classes sont orchestrées par Spring et peuvent donc l'injection @{@link org.springframework.beans.factory.annotation.Autowired}
 *
 *
 * Voir le package "com.kosmos.search.index.mapper.context" pour plus d'explications sur le fonctionnement du contexte à initialiser
 *
 * ![Serializer](serializer.png)
 *
 * @author cpoisnel
 *
 * @startuml serializer.png
 * participant CoreObjectMapper as Mapper
 * participant ContextSerializer as Context
 * participant StdSerializer as Serial
 * participant "Générateur JSON" as G
 * database "Elasticsearch" as ES
 * title Fonctionnement de l'enrichissement d'un bean avec Jackson
 *
 * Mapper -> Context : utilise
 * note right
 *      Utilisation de contextes
 *      de sérialisation pour un
 *      type donné (String, Long,...)
 * end note
 * Context ->  Serial : initialise et utilise
 * note right
 *      Initialisation de la classe
 *      de sérialisation pour un type
 *      et une annotation (par exemple)
 * end note
 * Serial -> G : écrit
 * note left : Ecrit du flux JSON
 * G -> ES : indexe
 * note left
 *          Persistence du flux JSON
 *          dans Elasticsearch
 * end note
 * @enduml
 *
 */
package com.kosmos.search.index.mapper.serializer;