/**
 * Fournit l'ensemble des composants pour <b>indexer</b> des fiches et d'autres entités.
 *
 *
 * ![Index](index.png)
 *
 * @author cpoisnel
 *
 * @startuml index.png
 * participant "Souscription au messages" as S
 * entity "Filtrage" as R
 * participant "Service Indexer" as SA
 * participant "Service d'indexation \nde fiche" as CI
 * participant Jackson
 * database "Elasticsearch" as ES
 *
 * title Fonctionnement de l'indexation générique
 * S ->  R : route
 * note right
 *  Spring Intégration
 *  - reçoit un message de modification de Metatag (fiche), Rubrique, Utilisateur,...
 *  - fait le routage et filtrage
 *  **package com.kosmos.search.subscribe(.filter)**
 * end note
 * R ->  SA : transfère
 * note right
 *     Service Activator
 *     - lance une indexation unitaire (Metatag)
 *     - et/ou lance un batch de réindexation des données
 *     liées (Metatag, Rubrique, Ressource, Media, Utilisateur)
 *     **package package com.kosmos.search.index.service**
 * end note
 * SA -> CI : génère / batch
 * note right
 *  - Sélection de fiche
 *  - Construction d'une commande d'indexation
 *      - Fiche
 *      - Plugins
 *      - Accès autorisés
 *  **package package com.kosmos.search.index.service**
 * end note
 * CI -> Jackson : enrichit avant indexation
 * note left
 *   - Sérialise via l'ObjectMapper
 *   - Enrichit le flux JSON (@User, @Fiche,...)
 *   **package com.kosmos.search.index.mapper**
 * end note
 * Jackson -> ES : alimente l'index
 * note left
 *   - Indexe la donnée
 *   **classe com.kosmos.search.index.service.impl.ServiceIndexerImpl**
 *   - Applique les templates de mapping
 *   **package com.kosmos.search.index.mapping**
 * end note
 *
 * @enduml
 */
package com.kosmos.search.index;
