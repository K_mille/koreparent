package com.kosmos.search.index.service.fiche;

import java.util.Map;

import com.kosmos.search.index.bean.CompletionBean;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.bean.RessourceBean;
import com.kosmos.search.index.bean.IndexingCommand;

/**
 * Interface commune à l'ensemble des indexations de fiches.
 * @author cpoisnel
 *
 * @param <T>
 * Type de bean manipulé par les fiches.
 */
public interface ServiceIndexerFiche<T> {

    /**
     * Renvoie la classe applicable.
     * @return la type de l'objet métier manipulé
     */
    Class<T> getType();

    /**
     * Sauvegarde de l'index (création ou mise à jour et suppression de l'index).
     * @param metatagBean
     * Bean metatag dont le bean de fiche doit être indexée
     * @param type
     * Type de fiche, correspond à l'index Elasticsearch
     * @param bean
     * Bean de fiche
     * @param completions
     * Map des valuers utilisées pour les différentes auto-complétions
     */
    void saveIndex(MetatagBean metatagBean, String type, T bean, Map<String, CompletionBean> completions);

    /**
     * Sauvegarde de l'index (création ou mise à jour et suppression de l'index).
     * @param ressource
     * Ressource
     * @param type
     * Type de la fiche, correspondant à l'index Elasticsearch
     * @param bean
     * Bean de fiche
     * @param parent
     * Identifiant parent
     */
    void saveIndexRessource(RessourceBean ressource, String type, T bean, final Long parent);

    /**
     * Suppression de l'index à partir du metatag.
     * @param idMetatag
     * ID du métatag
     */
    void deleteIndex(Long idMetatag);

    /**
     * Suppression de l'index de la ressource.
     * @param idResource
     * ID de la ressource
     */
    void deleteIndexRessource(Long idResource);

    /**
     * Créer une commande d'indexation de suppression de metatag (à utiliser par les traitements batchs par exemple).
     * On utilise cette méthode car seul l'ID est connu lors d'une suppression
     * @param idMetatag
     * ID du métatag
     * @return une commande de suppression de l'index
     */
    IndexingCommand createDeleteIndexCommand(Long idMetatag);

    /**
     * Créer une commande de sauvegarde d'index.
     * @param metatag
     * Metatag (porte les informations de la fiche)
     * @param type
     * Type de fiche (correspondant à l'index elasticsearch)
     * @param bean
     * Bean métier associé au métatag
     * @param completions
     * Map des valuers utilisées pour les différentes auto-complétions
     * @return une commande d'indexation
     */
    IndexingCommand createSaveIndexCommand(MetatagBean metatag, String type, T bean, Map<String, CompletionBean> completions);

    /**
     * Générer une commande d'indexation pour une ressource (à utiliser par les traitements batchs par exemple).
     *
     * @param ressourceBean
     * Ressource à indexer
     * @param type
     * Type de fiche (correspondant à l'index elasticsearch)
     * @param bean
     * Bean métier lié à la ressource
     * @param parent
     * ID du metatag parent (car relation Elasticsearch parent-child)
     * @param completions
     * Map des valuers utilisées pour les différentes auto-complétions
     * @return une commande d'indexation
     */
    IndexingCommand createSaveIndexCommand(RessourceBean ressourceBean, String type, T bean, final Long parent, Map<String, CompletionBean> completions);

    /**
     * Générer une commande d'indexation pour une suppression d'index d'une ressource (à utiliser par les traitements batchs par exemple).
     * @param idResource
     * ID de la ressource supprimée
     * @return une commende de suppression de l'index
     */
    IndexingCommand createDeleteIndexRessource(Long idResource);

    /**
     * Création d'une demande de suppression de l'index.
     * @param idMeta
     * ID Meta de la fiche modifiée ou supprimée
     * @return une commande de suppression de l'index
     */
    IndexingCommand createDeleteIndexRessourceByIdMeta(Long idMeta);
}
