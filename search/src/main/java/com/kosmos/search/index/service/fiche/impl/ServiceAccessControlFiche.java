package com.kosmos.search.index.service.fiche.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.index.bean.AccessControl;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.AbstractRestrictionFicheBean;
import com.univ.objetspartages.bean.GroupeDsiBean;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.bean.RubriquepublicationBean;
import com.univ.objetspartages.services.ServiceGroupeDsi;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.objetspartages.services.ServiceRubriquePublication;
import com.univ.utils.Chaine;

/**
 * Service de calcul des restriction d'accès pour les fiches.
 * <ul>
 * <li>Restriction par fiche</li>
 * <li>Restriction par rubriques / rubriques publication</li>
 * </ul>
 * @author cpoisnel
 *
 */
public class ServiceAccessControlFiche {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceAccessControlFiche.class);

    private static final String CROCHET_OUVRANT = "[/";

    private static final String CROCHET_FERMANT = "]";

    private static final String CROCHET_FERMANT_OUVRANT_POUR_SPLIT = "\\];\\[/";

    @Autowired
    private ServiceRubrique serviceRubrique;

    @Autowired
    private ServiceGroupeDsi serviceGroupeDsi;

    @Autowired
    private ServiceRubriquePublication serviceRubriquePublication;

    /**
     * Constructeur par défaut du service
     */
    public ServiceAccessControlFiche() {
        super();
    }

    /**
     * Générer un bean de contrôle d'accès permettant de valider les droits d'accès à l'interrogation. Utilise les expressions régulières pour dénormaliser un arbre.
     * @param bean
     * Bean métier. Si le bean hérite de {@link AbstractRestrictionFicheBean}, alors les restrictions de fiches sont ajoutées.
     * @param typeObjet Code type de l'objet
     * @return un bean de contrôle d'accès, qui sera sérialisé au moment de l'écriture.
     */
    public AccessControl genererAccessControl(final AbstractFicheBean bean, final String typeObjet) {
        final AccessControl accessControl = new AccessControl();
        accessControl.setGroupesRubrique(genererRestrictionRubriques(bean, typeObjet));
        if (AbstractRestrictionFicheBean.class.isAssignableFrom(bean.getClass())) {
            final AbstractRestrictionFicheBean beanRestriction = (AbstractRestrictionFicheBean) bean;
            accessControl.setGroupesFicheRestriction(beanRestriction.getDiffusionModeRestriction());
            accessControl.setGroupesFiche(genererRestrictionFiche(beanRestriction));
        }
        return accessControl;
    }

    protected List<String> genererRestrictionRubriques(final AbstractFicheBean bean, final String typeObjet) {
        final List<String> regexAllRubriques = new ArrayList<>();
        // Regex de restriction de la rubrique principale
        String restrictionRubrique = genererRegexRestrictionRubrique(bean.getCodeRubrique());
        if (StringUtils.isNoneEmpty(restrictionRubrique)) {
            regexAllRubriques.add(restrictionRubrique);
        }else{
            return Collections.emptyList();
        }

        // Parcourir les rubriques publication... et générer la regex correspondante.
        final List<RubriquepublicationBean> rubriquesPublication = serviceRubriquePublication.getByTypeCodeLanguage(typeObjet, bean.getCode(), bean.getLangue());
        for (final RubriquepublicationBean rubriquepublicationBean : rubriquesPublication) {
            String restrictionRubriquePublication = genererRegexRestrictionRubrique(rubriquepublicationBean.getRubriqueDest());
            if (StringUtils.isNotEmpty(restrictionRubriquePublication)) {
                regexAllRubriques.add(restrictionRubriquePublication);
            }else{
                return Collections.emptyList();
            }
        }
        return regexAllRubriques;
    }

    protected String genererRegexRestrictionRubrique(final String codeRubrique) {
        final Collection<AccessControlRubrique> acrListe = genererRestrictionRubrique(codeRubrique);
        final String regexRubrique = buidRegex(acrListe);
        LOG.debug("Expression régulère de droits sur la rubrique '{}' = {}", codeRubrique, regexRubrique);
        return regexRubrique;
    }

    protected Collection<AccessControlRubrique> genererRestrictionRubrique(final String codeRubrique) {
        final Collection<AccessControlRubrique> acrListe = new ArrayList<>();
        if (StringUtils.isNotBlank(codeRubrique) && !"0000".equals(codeRubrique)) {
            final List<RubriqueBean> rubriques = serviceRubrique.getAllAscendant(codeRubrique);
            final RubriqueBean rubriqueFicheBean = serviceRubrique.getRubriqueByCode(codeRubrique);
            if (null != rubriqueFicheBean) {
                rubriques.add(rubriqueFicheBean);
            } else {
                LOG.warn("Rubrique de code {} non trouvée", codeRubrique);
            }
            for (final RubriqueBean rubriqueBean : rubriques) {
                final String chaineGroupesDsi = rubriqueBean.getGroupesDsi();
                final Set<String> groupesDsi = Chaine.getHashSetAccolades(chaineGroupesDsi);
                if (CollectionUtils.isNotEmpty(groupesDsi)) {
                    final AccessControlRubrique acr = new AccessControlRubrique();
                    for (final String codeGroupeDsi : groupesDsi) {
                        acr.add(genererIdGroupeDsiES(codeGroupeDsi));
                    }
                    acrListe.add(acr);
                }
            }
        }
        return acrListe;
    }

    protected List<String> genererRestrictionFiche(final AbstractRestrictionFicheBean beanRestriction) {
        final List<String> restrictionFiche;
        final String mode = beanRestriction.getDiffusionModeRestriction();
        if ("2".equals(mode)) {
            restrictionFiche = new ArrayList<>();
            final List<String> groupesDsi = unserialize(beanRestriction.getDiffusionPublicVise());
            for (final String codeGroupeDsi : groupesDsi) {
                restrictionFiche.add(genererIdGroupeDsiES(codeGroupeDsi));
            }
        } else {
            restrictionFiche = Collections.emptyList();
        }
        return restrictionFiche;
    }

    /**
     * Désérialiser une donnée.
     * @param donnee la donnée de la forme [A][B]
     * @return une liste de valeurs
     */
    public static List<String> unserialize(final String donnee) {
        if (!isDonneeFormatValide(donnee)) {
            return new ArrayList<>(0);
        }
        final String forSplit = StringUtils.removeEnd(StringUtils.removeStart(donnee, CROCHET_OUVRANT), CROCHET_FERMANT);
        final String[] split = forSplit.split(CROCHET_FERMANT_OUVRANT_POUR_SPLIT);
        return new ArrayList<>(Arrays.asList(split));
    }

    private static boolean isDonneeFormatValide(final String donnee) {
        return StringUtils.isNotBlank(donnee) && StringUtils.startsWith(donnee, CROCHET_OUVRANT) && StringUtils.endsWith(donnee, CROCHET_FERMANT);
    }

    /**
     * Génération de l'ID correspondant à un code de groupe DSI. La raison de l'utilisation d'un ID est de simplifier la grammaire de regex.
     * @param codeGroupeDsi
     * Code Groupe de DSI
     * @return l'ID du groupe s'il est retrouvé depuis le {@link ServiceGroupeDsi#getByCode(String)}, le MD5 du code de groupe sinon (exemple : code groupe LDAP).
     */
    public String genererIdGroupeDsiES(final String codeGroupeDsi) {
        final GroupeDsiBean groupeDsi = serviceGroupeDsi.getByCode(codeGroupeDsi);
        final String idGroupeDsi;
        if (null != groupeDsi) {
            idGroupeDsi = String.valueOf(groupeDsi.getId());
        } else {
            idGroupeDsi = DigestUtils.md5Hex(codeGroupeDsi);
            LOG.debug("Code de groupe dynamique : {} -->  {} (md5)", codeGroupeDsi, idGroupeDsi);
        }
        return idGroupeDsi;
    }

    /**
     * Renvoie la chaine regex qu'il faudra valider lors de la recherche.
     * <pre>
     *     ([1][2])([3])
     * </pre>
     * @param accesControl
     * Collection de beans de contrôle d'accès
     * @return la regex construite à partir des beans de contrôle d'accès
     */
    public String buidRegex(final Collection<AccessControlRubrique> accesControl) {
        String regex = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(accesControl)) {
            final StringBuilder sb = new StringBuilder();
            for (final AccessControlRubrique accessControlRubrique : accesControl) {
                if (CollectionUtils.isNotEmpty(accessControlRubrique.items)) {
                    sb.append("(");
                    for (final String acces : accessControlRubrique.items) {
                        sb.append("[").append(acces).append("]");
                    }
                    sb.append(")");
                }
            }
            regex = sb.toString();
        }
        return regex;
    }

    /**
     * Classe interne pour gérer les contrôles d'accès par rubrique ({@link Set})
     */
    private static final class AccessControlRubrique {

        private final Set<String> items = new HashSet<>();

        /**
         * Ajout d'une rubrique
         * @param valeur
         * Code rubrique
         */
        public void add(final String valeur) {
            this.items.add(valeur);
        }
    }
}
