package com.kosmos.search.index.service.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.beanutils.PropertyUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.util.ReflectionUtils;

import com.kosmos.search.exception.IndexationException;
import com.kosmos.search.index.bean.CompletionBean;
import com.kosmos.search.index.bean.IndexableFiche;
import com.kosmos.search.utils.IndexerUtil;
import com.kosmos.service.ServiceBean;
import com.kosmos.service.impl.ServiceManager;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.cms.objetspartages.annotation.Autocompletion;
import com.kportal.cms.objetspartages.annotation.FicheAnnotation;
import com.univ.objetspartages.bean.AbstractFicheBean;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
;

/**
 * Classe abstraite d'indexation, à utiliser par les services d'indexation manipulant des fiches.
 * @param <T>
 *     Bean, avec des attributs de fiche
 */
public abstract class AbstractServiceIndexerCore<T extends PersistenceBean> {

    private static final Logger LOG = LoggerFactory.getLogger(AbstractServiceIndexerCore.class);

    /**
     * Recherche du type correspondant à la classe du bean. Est utilisé pour déterminer le nom de l'index Elasticsearch.
     * <p>Renvoie le nom de la classe de bean en minuscule</p>
     * @param beanClass
     * Classe du bean métier
     * @param <F>
     *     Type du bean métier (normalement, étend {@link AbstractOm})
     * @return le type de fiche
     */
    protected <F> String getTypeFiche(final Class<F> beanClass) {
        List<Objetpartage> listeObjetPartage = ReferentielObjets.getObjetsPartagesTries();
        String typeFiche = beanClass.getSimpleName().toLowerCase();
        for(Objetpartage objetpartage:listeObjetPartage){
            if(objetpartage.getNomClasse().equals(beanClass)){
                typeFiche = objetpartage.getNomObjet();
            }
        }
        return typeFiche;
    }

    /**
     * Récupérer le code de l'objet cible.
     * @param bean
     * PersistenceBean
     * @return le code objet du bean manipulé
     */
    protected abstract String getCodeObjet(T bean);

    /**
     * Récupérer l'ID de la fiche.
     * @param bean
     * PersistenceBean
     * @return l'ID de la fiche
     */
    protected abstract Long getIdFiche(T bean);

    /**
     * Récupérer la fiche indexable à partir du bean.
     * @param bean
     * Bean qui contient les informations de fiche
     *
     * @return la fiche à indexer
     */
    @SuppressWarnings("rawtypes")
    protected IndexableFiche fetchIndexableFiche(final T bean) {
        IndexableFiche indexableFiche = null;
        PersistenceBean ficheBean;
        String typeFiche;
        final String codeObjet = getCodeObjet(bean);
        final Long idFiche = getIdFiche(bean);
        if (codeObjet != null && idFiche != null) {

            // On récupère la classe de la fiche univ
            final Class<?> classeFiche = IndexerUtil.getClasseFiche(codeObjet);
            typeFiche = getTypeFiche(classeFiche);

            if (typeFiche != null && AbstractOm.class.isAssignableFrom(classeFiche)) {
                final FicheAnnotation ficheAnnotation = AnnotationUtils.findAnnotation(classeFiche, FicheAnnotation.class);
                if (ficheAnnotation != null && ficheAnnotation.isIndexable()) {
                    @SuppressWarnings({"unchecked"})
                    final Class<?> classBean = IndexerUtil.resolveClassPersistenceBean((Class<AbstractOm>) classeFiche);
                    // Avec persistence bean !
                    // Charger le service correspondant au document indexé
                    final ServiceBean<?> serviceBean = ServiceManager.getServiceForBean(classBean);
                    if (serviceBean != null) {
                        Method methodGet;
                        try {
                            methodGet = serviceBean.getClass().getMethod("getById", Long.class);
                        } catch (NoSuchMethodException | SecurityException e) {
                            LOG.warn("Impossible de déterminer la méthode getById pour le service '{}' et l'ID '{}'", serviceBean, idFiche);
                            throw new IndexationException("Impossible de déterminer la methode getById", e);
                        }
                        ficheBean = (AbstractFicheBean) ReflectionUtils.invokeMethod(methodGet, serviceBean, idFiche);
                    } else {
                        // Il se peut qu'il n'y ait pas de service pour l'objet, ex : Actualite -> Avoir un comportement dégradé avec les fiches non migrées sur les services.
                        LOG.debug("Service de la classe '{}' non trouvé, utilisation de l'objet FicheUniv", classeFiche);
                        final FicheUniv ficheUniv = ReferentielObjets.instancierFiche(codeObjet);
                        if (ficheUniv ==null) {
                            throw new IndexationException("Impossible d'instancier la fiche de code objet : "+codeObjet);
                        }
                        ficheUniv.setIdFiche(idFiche);
                        try {
                            ficheUniv.retrieve();
                        } catch (final Exception e) {
                            LOG.debug("Impossible de récupérer la fiche", e);
                        }
                        ficheBean = ((AbstractOm) ficheUniv).getPersistenceBean();

                    }
                    if (ficheBean != null) {
                        indexableFiche = new IndexableFiche(ficheBean, typeFiche, codeObjet);
                        indexableFiche.setCompletions(buildCompletion((AbstractFicheBean) ficheBean, classBean));
                    }
                } else {
                    LOG.debug("Indexation désactivé pour le type {} (FicheAnnotation)", typeFiche);
                }
            }
        }

        return indexableFiche;
    }

    /**
     * Construction de la map des champs à indexer pour l'autocomplétion.
     * On retourne une map avec :
     * clé = nom de l'autocomplétion (ex:titre_completion)
     * valeur = liste des champs
     * @param fiche
     * @param clazz
     * @return
     */
    private Map<String, CompletionBean> buildCompletion(AbstractFicheBean fiche, Class<?> clazz){
        Map<String, CompletionBean> mapCompletion = new HashMap<>();
        ;
        final List<Field> completionFieldList = FieldUtils.getFieldsListWithAnnotation(clazz, Autocompletion.class);
        for(Field completionField:completionFieldList){
            Autocompletion completionAnnotation = completionField.getAnnotation(Autocompletion.class);
            CompletionBean completionBean = mapCompletion.get(completionAnnotation.name());
            if(completionBean ==null){
                completionBean = new CompletionBean();
                mapCompletion.put(completionAnnotation.name(), completionBean);
            }
            try{
                Object propertyValue = PropertyUtils.getProperty(fiche, completionField.getName());
                if(propertyValue instanceof String){
                    completionBean.getInput().put(completionField.getName() + IndexerUtil.INDEX_SEPARATEUR + IndexerUtil.INDEX_COMPLETION_SUFFIX, StringUtils.lowerCase((String) propertyValue));
                }
            } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException | IllegalArgumentException e) {
                LOG.warn("Impossible de retrouver la propriété {} du bean {}", completionField.getName(), clazz.getClass());
            }
        }
        return mapCompletion;
    }

}
