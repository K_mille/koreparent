package com.kosmos.search.index.utils;

/**
 * Type de demande d'indexation.
 */
public enum IndexingAction {
    /**
     * Création ou mise à jour (Elasticsearch ne fait pas de différence entre une création d'un index ou sa mise à jour).
     */
    CREATE_UPDATE, /**
     * Suppression d'un document (ou plusieurs si combiné avec l'utilisation du plugin delete-by-query).
     */
    DELETE;
}
