/**
 * Module de recherche et d'indexation reposant sur Elasticsearch.
 *
 * @author cpoisnel
 */
package com.kosmos.search;