package com.kosmos.search.query.bean;

import java.util.List;

/**
 * Created by charlie.camus on 23/06/17.
 */
public class AggregationFusion {

    private String codeFusion;

    private String propertyKeyLibelle;

    private List<String> codesFusionnes;

    private String idCtx;

    public String getIdCtx() {
        return idCtx;
    }

    public void setIdCtx(final String idCtx) {
        this.idCtx = idCtx;
    }

    public String getCodeFusion() {
        return codeFusion;
    }

    public void setCodeFusion(final String codeFusion) {
        this.codeFusion = codeFusion;
    }

    public String getPropertyKeyLibelle() {
        return propertyKeyLibelle;
    }

    public void setPropertyKeyLibelle(final String propertyKeyLibelle) {
        this.propertyKeyLibelle = propertyKeyLibelle;
    }

    public List<String> getCodesFusionnes() {
        return codesFusionnes;
    }

    public void setCodesFusionnes(final List<String> codesFusionnes) {
        this.codesFusionnes = codesFusionnes;
    }
}
