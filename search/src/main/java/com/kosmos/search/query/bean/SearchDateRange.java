package com.kosmos.search.query.bean;

/**
 * Created by camille.lebugle on 07/04/17.
 */
public class SearchDateRange {

    private String from;

    private String to;

    public SearchDateRange() {
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(final String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(final String to) {
        this.to = to;
    }
}
