package com.kosmos.search.query.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Preconditions;
import com.univ.objetspartages.om.EtatFiche;

/**
 * Options de recherche, simplification et valeurs par défaut ElasticSearch.
 * Ces Options de recherche peuvent être utilisés dans les recherches simples (issues de la servlet proposée). Si la recherche est spécifique à un projet, l'API Java Elasticsearch pourrait directement être utilisée.
 * @author cpoisnel
 *
 */
public class SearchOptions implements Serializable {

    private static final int DEFAULT_OFFSET = 0;

    private static final int DEFAULT_PAGE = 1;

    private static final int DEFAULT_LIMIT = 10;

    private static final String DEFAULT_LANGUAGE = "fr";

    private static final int MAX_LIMIT = 500;

    private static final long serialVersionUID = 6714525796288072130L;

    private String searchParameterBeanKey;

    private int offset = DEFAULT_OFFSET;

    private int limit = DEFAULT_LIMIT;

    private int page = DEFAULT_PAGE;

    private String query;

    private String language = DEFAULT_LANGUAGE;

    private boolean autocomplete = false;

    private String autoCompleteName;

    private Set<String> facets = new LinkedHashSet<>();

    private Set<String> excludedFacets = new LinkedHashSet<>();

    private Map<String, String[]> filters = new HashMap<>();

    private Set<SortOption> sort = new HashSet<>();

    private Set<String> authorizedGroups = new HashSet<>();

    private Set<EtatFiche> etatFicheList = new HashSet<>();

    // les types qui proviennent de l'url
    private List<String> typesFiches;

    /**
     * Attribut permettant de définir une recherche via une flux JSON (écrase les autres paramètres utiles, hors index / type.
     */
    private String jsonSource;

    public final String getQuery() {
        return query;
    }

    public SearchOptions() {
        initSearchOptions();
    }

    public final void setQuery(final String query) {
        this.query = query;
    }

    private void initSearchOptions() {
        this.etatFicheList.add(EtatFiche.EN_LIGNE);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(final String language) {
        this.language = language;
    }

    /**
     * Offset of the first result to return. Defaults to {@link #DEFAULT_OFFSET}
     */
    public int getOffset() {
        return offset;
    }

    /**
     * Sets the offset of the first result to return (zero-based).
     */
    public void setOffset(final int offset) {
        Preconditions.checkArgument(offset >= 0, "Offset must be positive");
        this.offset = offset;
    }

    /**
     * Set offset and limit according to page approach. If pageSize is negative, then
     * {@link #MAX_LIMIT} is used.
     */
    public SearchOptions setPage(int page, final int pageSize) {
        if(page > 0){
            this.page = page;
        }
        setLimit(pageSize);
        setOffset((this.page * this.limit) - this.limit);
        return this;
    }

    public int getPage() {
        return limit > 0 ? (int) Math.ceil((double) (offset + 1) / (double) limit) : 0;
    }

    /**
     * @param sortOption the sort to add
     */
    public void addSort(final SortOption sortOption) {
        this.sort.add(sortOption);
    }

    /**
     * Ajout d'un filtre dans la configuration de recherche.
     * @param parameter
     * Paramètre HTTP
     * @param values
     * Valeurs attendues (en String)
     */
    public void addFilter(final String parameter, final String[] values) {
        if(StringUtils.isNoneBlank(values)) {
            this.filters.put(parameter, values);
        }
    }

    public void setSort(final Set<SortOption> sort) {
        this.sort = sort;
    }

    /**
     * Accesseur.
     * @return the sort
     */
    public Set<SortOption> getSort() {
        return sort;
    }

    public Map<String, String[]> getFilters() {
        return filters;
    }

    public void setFilters(Map<String, String[]> filters) {
        this.filters = filters;
    }



    public boolean isAutocomplete() {
        return autocomplete;
    }

    public void setAutocomplete(final boolean autocomplete) {
        this.autocomplete = autocomplete;
    }

    /**
     * @param authorizedGroup
     * Groupe autorisé à accéder aux documents
     */
    public void addAuthorizedGroup(final String authorizedGroup) {
        this.authorizedGroups.add(authorizedGroup);
    }

    /**
     * @param authorizedGroups the authorizedGroups to set
     */
    public void setAuthorizedGroups(final Set<String> authorizedGroups) {
        this.authorizedGroups = authorizedGroups;
    }

    public Set<String> getAuthorizedGroups() {
        return authorizedGroups;
    }

    /**
     * Limit on the number of results to return. Defaults to {@link #DEFAULT_LIMIT}.
     */
    public int getLimit() {
        return limit;
    }

    /**
     * Définit le nombre max des résultats
     * @param limit limite du nombre de résultats
     * @return les options de recherche
     */
    public void setLimit(final int limit) {
        if (limit < 0) {
            this.limit = MAX_LIMIT;
        } else if(limit == 0) {
            this.limit = DEFAULT_LIMIT;
        } else {
            this.limit = Math.min(limit, MAX_LIMIT);
        }
    }

    /**
     * Lists selected facets.
     */
    public Collection<String> getFacets() {
        return facets;
    }

    /**
     * Selects facets to return for the domain.
     * @param f Aggrégation
     */
    public SearchOptions addFacets(final Collection<String> f) {
        if (f != null) {
            this.facets.addAll(f);
        }
        return this;
    }

    public Set<String> getExcludedFacets() {
        return excludedFacets;
    }

    public SearchOptions addExcludedFacets(final Collection<String> f) {
        if (f != null) {
            this.excludedFacets.addAll(f);
        }
        return this;
    }

    /**
     * Source JSON de la recherche. Si elle est non vide, elle prévaut sur toutes les autres options de recherche.
     * @return the jsonSource
     */
    public String getJsonSource() {
        return jsonSource;
    }

    /**
     * @param jsonSource the rawSearch to set
     */
    public void setJsonSource(final String jsonSource) {
        this.jsonSource = jsonSource;
    }

    public String getAutoCompleteName() {
        return autoCompleteName;
    }

    public void setAutoCompleteName(final String autoCompleteName) {
        this.autoCompleteName = autoCompleteName;
    }

    public Set<EtatFiche> getEtatFicheList() {
        return etatFicheList;
    }

    public void setEtatFicheList(final Set<EtatFiche> etatFicheList) {
        this.etatFicheList = etatFicheList;
    }

    public String getSearchParameterBeanKey() {
        return searchParameterBeanKey;
    }

    public void setSearchParameterBeanKey(String searchParameterBeanKey) {
        this.searchParameterBeanKey = searchParameterBeanKey;
    }

    public List<String> getTypesFiches() {
        return typesFiches;
    }

    public void setTypesFiches(List<String> typesFiches) {
        this.typesFiches = typesFiches;
    }

}