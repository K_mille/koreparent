package com.kosmos.search.query.bean;

import org.elasticsearch.common.unit.TimeValue;

/**
 * Bean de
 */
public class SearchParameter {

    /**
     * Début du tag encadrant le highlight
     */
    private String hightLightPreTag;

    /**
     * Fin du tag encadrant le highlight
     */
    private String hightLightPostTag;

    /**
     * Econdage du highlight
     */
    private String hightLightEncoder;

    /**
     * Tri du highlight
     */
    private String hightLightOrder;

    /**
     * Taille du fragment affiché pour le highlight
     */
    private int hightLightFragmentSize;

    /**
     * Nombre de highlight affiché sur un résultat de recherche
     */
    private int hightLightFragmentNumber;

    /**
     * Nombre d'agrégation visible
     */
    private int maxAggregationSize;

    /**
     * Timeout de la recherche
     */
    private TimeValue timeOut;

    /**
     * Boost des child query pour les ressources
     */
    private Float childRessourceBoost;

    /**
     * Slop (espace entre token) pour les query de type phrase
     */
    private int slop;

    /**
     * Boost pour la query Exact + Phrase
     */
    private float exactPhraseMatchBoost;

    /**
     * Boost pour la query Exact Match
     */
    private float exactMatchBoost;

    /**
     * Boost pour la query Phrase
     */
    private float phraseMatchBoost;

    /**
     * Boost pour la query MultiMatch
     */
    private float multiMatchBoost;

    /**
     * Minimum_should_match pour les query should
     */
    private String minimumShouldMatch;

    /**
     * Score minimum pour la query
     */
    private float minScore;

    /**
     * Score mode pour les requêtes de type child
     */
    private String scoreMode;

    public SearchParameter() {
    }

    public String getHightLightPreTag() {
        return hightLightPreTag;
    }

    public void setHightLightPreTag(final String hightLightPreTag) {
        this.hightLightPreTag = hightLightPreTag;
    }

    public String getHightLightPostTag() {
        return hightLightPostTag;
    }

    public void setHightLightPostTag(final String hightLightPostTag) {
        this.hightLightPostTag = hightLightPostTag;
    }

    public String getHightLightEncoder() {
        return hightLightEncoder;
    }

    public void setHightLightEncoder(final String hightLightEncoder) {
        this.hightLightEncoder = hightLightEncoder;
    }

    public String getHightLightOrder() {
        return hightLightOrder;
    }

    public void setHightLightOrder(final String hightLightOrder) {
        this.hightLightOrder = hightLightOrder;
    }

    public int getMaxAggregationSize() {
        return maxAggregationSize;
    }

    public void setMaxAggregationSize(final int maxAggregationSize) {
        this.maxAggregationSize = maxAggregationSize;
    }

    public TimeValue getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(final TimeValue timeOut) {
        this.timeOut = timeOut;
    }

    public Float getChildRessourceBoost() {
        return childRessourceBoost;
    }

    public void setChildRessourceBoost(final Float childRessourceBoost) {
        this.childRessourceBoost = childRessourceBoost;
    }

    public int getSlop() {
        return slop;
    }

    public void setSlop(final int slop) {
        this.slop = slop;
    }

    public int getHightLightFragmentSize() {
        return hightLightFragmentSize;
    }

    public void setHightLightFragmentSize(final int hightLightFragmentSize) {
        this.hightLightFragmentSize = hightLightFragmentSize;
    }

    public int getHightLightFragmentNumber() {
        return hightLightFragmentNumber;
    }

    public void setHightLightFragmentNumber(final int hightLightFragmentNumber) {
        this.hightLightFragmentNumber = hightLightFragmentNumber;
    }

    public float getExactMatchBoost() {
        return exactMatchBoost;
    }

    public void setExactMatchBoost(final float exactMatchBoost) {
        this.exactMatchBoost = exactMatchBoost;
    }

    public float getPhraseMatchBoost() {
        return phraseMatchBoost;
    }

    public void setPhraseMatchBoost(final float phraseMatchBoost) {
        this.phraseMatchBoost = phraseMatchBoost;
    }

    public float getMultiMatchBoost() {
        return multiMatchBoost;
    }

    public void setMultiMatchBoost(final float multiMatchBoost) {
        this.multiMatchBoost = multiMatchBoost;
    }

    public String getMinimumShouldMatch() {
        return minimumShouldMatch;
    }

    public void setMinimumShouldMatch(final String minimumShouldMatch) {
        this.minimumShouldMatch = minimumShouldMatch;
    }

    public float getMinScore() {
        return minScore;
    }

    public void setMinScore(final float minScore) {
        this.minScore = minScore;
    }

    public float getExactPhraseMatchBoost() {
        return exactPhraseMatchBoost;
    }

    public void setExactPhraseMatchBoost(final float exactPhraseMatchBoost) {
        this.exactPhraseMatchBoost = exactPhraseMatchBoost;
    }

    public String getScoreMode() {
        return scoreMode;
    }

    public void setScoreMode(final String scoreMode) {
        this.scoreMode = scoreMode;
    }
}
