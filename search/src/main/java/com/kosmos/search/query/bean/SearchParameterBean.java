package com.kosmos.search.query.bean;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Bean de paramétrage de la recherche.
 * Created by charlie.camus on 07/04/17.
 */
public class SearchParameterBean {

    public final static String DEFAULT_BEAN_KEY = "150bfcee-1f87-11e7-a0e0-b753bedcad22";

    private String key;

    //Aggregation à inclure : nom de l'aggregation
    private List<String> aggregations;

    //Agrégation à exclure : nom de l'aggregation
    private List<String> excludedAggregation;

    //Type de fiche à prendre en compte : nom de l'objet
    private List<String> typesFiche;

    //Type de fiche à exclure : nom de l'objet
    private List<String> excludedTypesFiche;

    private boolean restreintSiteCourant;



    public void setAggregationString(String agreggations){
        if(StringUtils.isNotBlank(agreggations)){
            aggregations = Arrays.asList(StringUtils.split(agreggations, ","));
        }
    }

    public void setTypeString(String types){
        if(StringUtils.isNotBlank(types)){
            typesFiche = Arrays.asList(StringUtils.split(types, ","));
        }
    }

    public List<String> getAggregations() {
        return aggregations;
    }

    public void setAggregations(List<String> aggregations) {
        this.aggregations = aggregations;
    }

    public List<String> getTypesFiche() {
        return typesFiche;
    }

    public void setTypesFiche(List<String> typesFiche) {
        this.typesFiche = typesFiche;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public void setRestreintSiteCourant(boolean restreintSiteCourant) {
        this.restreintSiteCourant = restreintSiteCourant;
    }

    public List<String> getExcludedAggregation() {
        return excludedAggregation;
    }

    public void setExcludedAggregation(final List<String> excludedAggregation) {
        this.excludedAggregation = excludedAggregation;
    }

    public List<String> getExcludedTypesFiche() {
        return excludedTypesFiche;
    }

    public void setExcludedTypesFiche(final List<String> excludedTypesFiche) {
        this.excludedTypesFiche = excludedTypesFiche;
    }

    public boolean isRestreintSiteCourant() {
        return restreintSiteCourant;
    }
}
