package com.kosmos.search.query.bean;

import java.util.Collection;

import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import org.apache.commons.lang3.builder.ReflectionToStringBuilder;
import org.elasticsearch.action.search.SearchResponse;

import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.search.query.view.bean.SearchMetadataModel;

/**
 * Classe de résultats de recherche.
 * @author cpoisnel
 */
public class SearchResultBean {

    /**
     * Les documents Elasticsearch retournés.
     */
    private Collection<SearchResultDoc> docs;

    /**
     * Résultats des aggrégations (facettes).
     */
    private Collection<AggregationModel> aggregations;

    /**
     * Options de recherche utilisées (si défini).
     */
    private SearchMetadataModel searchMetadataModel;

    public SearchResultBean() {
    }

    public Collection<SearchResultDoc> getDocs() {
        return docs;
    }

    public SearchMetadataModel getSearchMetadataModel() {
        return searchMetadataModel;
    }

    public void setDocs(Collection<SearchResultDoc> docs) {
        this.docs = docs;
    }

    public void setSearchMetadataModel(SearchMetadataModel searchMetadataModel) {
        this.searchMetadataModel = searchMetadataModel;
    }

    public Collection<AggregationModel> getAggregations() {
        return aggregations;
    }

    public void setAggregations(Collection<AggregationModel> aggregations) {
        this.aggregations = aggregations;
    }

    /**
     * Methode d'affichage de l'ensemble des attributs.
     * @return la chaine avec l'ensemble des attributs
     */
    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

}
