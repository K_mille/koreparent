package com.kosmos.search.query.bean;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Résultat de recherche.
 * @author cpoisnel
 *
 */
public class SearchResultDoc implements Serializable {

    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = -7537989401719334180L;

    private float score;

    private String id;

    private String codeObjet;

    private String typeFiche;

    private String libelleObjet;

    private String titre;

    private String url;

    private Date dateModification;

    private Map<String, HighlightedResult> highlightedFields;

    private transient Map<String, Object> source;

    private Map<String, List<SearchResultDoc>> innerDocs;

    public Map<String, Object> getSource() {
        return source;
    }

    public void setSource(final Map<String, Object> source) {
        this.source = source;
    }

    public float getScore() {
        return score;
    }

    public void setScore(final float score) {
        this.score = score;
    }

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getTypeFiche() {
        return typeFiche;
    }

    public void setTypeFiche(final String typeFiche) {
        this.typeFiche = typeFiche;
    }

    public Map<String, HighlightedResult> getHighlightedFields() {
        return highlightedFields;
    }

    public void setHighlightedFields(final Map<String, HighlightedResult> highlightedFields) {
        this.highlightedFields = highlightedFields;
    }

    public String getCodeObjet() {
        return codeObjet;
    }

    public void setCodeObjet(final String codeObjet) {
        this.codeObjet = codeObjet;
    }

    public String getLibelleObjet() {
        return libelleObjet;
    }

    public void setLibelleObjet(final String libelleObjet) {
        this.libelleObjet = libelleObjet;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(final String titre) {
        this.titre = titre;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public Date getDateModification() {
        return dateModification;
    }

    public void setDateModification(final Date dateModification) {
        this.dateModification = dateModification;
    }

    public Map<String, List<SearchResultDoc>> getInnerDocs() {
        return innerDocs;
    }

    public void setInnerDocs(final Map<String, List<SearchResultDoc>> innerDocs) {
        this.innerDocs = innerDocs;
    }

    public static class HighlightedResult implements Serializable {

        /**
         * Serial Version UID.
         */
        private static final long serialVersionUID = 625720628524667483L;

        private String name;

        private Collection<String> fragments;

        HighlightedResult() {}

        public HighlightedResult(final String name, final Collection<String> fragments) {
            this.name = name;
            this.fragments = fragments;
        }

        public String getName() {
            return name;
        }

        public void setName(final String name) {
            this.name = name;
        }

        public Collection<String> getFragments() {
            return fragments;
        }

        public void setFragments(final Collection<String> fragments) {
            this.fragments = fragments;
        }



    }

    @Override
    public String toString() {
        return "SearchResultDoc{" + "score=" + score + ", id='" + id + '\'' + ", codeObjet='" + codeObjet + '\'' + ", typeFiche='" + typeFiche + '\'' + ", highlightedFields=" + highlightedFields + ", source=" + source + ", innerDocs=" + innerDocs + '}';
    }
}
