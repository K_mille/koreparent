package com.kosmos.search.query.bean;

/**
 * Created on 21/09/17.
 */

import java.io.Serializable;

/**
 * Option d'ordonnancement des résultats de recherche.
 * @author cpoisnel
 *
 */
public class SortOption implements Serializable {

    private static final long serialVersionUID = 6488572212206347588L;

    //Champ sur lequel effectué le tri
    private String field;

    private SortOrderEnum order = SortOrderEnum.ASC;

    //Clé du tri à passer à la requête
    private String key;

    public SortOption() {
    }

    /**
     * Option d'ordonnancement des résultats de résultats.
     * @param field
     * Champ de recherche, peut être un chemin complet du champ dans l'index
     * @param order
     * Valeur de l'ordre
     */
    public SortOption(final String field, final SortOrderEnum order) {
        super();
        this.field = field;
        this.order = order;
    }

    /**
     * Option d'ordonnancement des résultats de résultats. Par défaut, ordonné de manière ascendante.
     * @param field
     * Champ de recherche, peut être un chemin complet du champ dans l'index
     */
    public SortOption(final String field) {
        super();
        this.field = field;
    }

    /**
     * @return the field
     */
    public String getField() {
        return field;
    }

    /**
     * @param field the field to set
     */
    public void setField(final String field) {
        this.field = field;
    }

    /**
     *
     * @return the order
     */
    public SortOrderEnum getOrder() {
        return order;
    }

    /**
     * @param order the order to set
     */
    public void setOrder(final SortOrderEnum order) {
        this.order = order;
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Ordre de tri des résultats de recherche.
     * @author cpoisnel
     *
     */
    public enum SortOrderEnum {
        ASC, DESC;
    }
}
