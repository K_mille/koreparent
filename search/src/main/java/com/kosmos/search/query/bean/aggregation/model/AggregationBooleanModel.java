package com.kosmos.search.query.bean.aggregation.model;

import com.kosmos.search.query.bean.aggregation.model.AggregationTermModel;

/**
 * Created by camille.lebugle on 21/02/17.
 */
public class AggregationBooleanModel extends AggregationTermModel {

    private String defaultValue;

    private String defaultLabel;

    /**
     * Constructeur par défaut.

     * @param name Nom de l'aggrégation (identifiant)
     */
    public AggregationBooleanModel(final String name) {
        super(name);
    }

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultLabel() {
        return defaultLabel;
    }

    public void setDefaultLabel(final String defaultLabel) {
        this.defaultLabel = defaultLabel;
    }
}
