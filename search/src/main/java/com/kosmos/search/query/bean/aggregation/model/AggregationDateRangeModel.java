package com.kosmos.search.query.bean.aggregation.model;

import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.bean.aggregation.model.BucketModel;

import java.util.Collection;

/**
 * Created by camille.lebugle on 07/04/17.
 */
public class AggregationDateRangeModel extends AggregationModel {

    private transient Collection<BucketModel> buckets;

    /**
     * Constructeur par défaut (utilisée pour une écriture simplifiée dans Spring).
     * @param name
     */
    public AggregationDateRangeModel(final String name) {
        super(name);
    }

    @Override
    public String getType() {
        return "dateRange";
    }

    public Collection<BucketModel> getBuckets() {
        return buckets;
    }

    public void setBuckets(final Collection<BucketModel> buckets) {
        this.buckets = buckets;
    }
}
