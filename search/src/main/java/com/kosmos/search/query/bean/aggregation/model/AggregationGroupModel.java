package com.kosmos.search.query.bean.aggregation.model;

import com.kosmos.search.query.bean.aggregation.model.AggregationModel;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * Modèle de vue pour les groupement d'agrégations
 */
public class AggregationGroupModel extends AggregationModel implements Iterable<AggregationModel> {

    public AggregationGroupModel(final String name) {
        super(name);
        this.childAggregation = new ArrayList<>();
    }

    private Collection<AggregationModel> childAggregation;

    @Override
    public String getType() {
        return "groupe";
    }

    @Override
    public Iterator<AggregationModel> iterator() {
        return childAggregation.iterator();
    }

    public void addChild(AggregationModel childModel) {
        if (childModel != null) {
            this.childAggregation.add(childModel);
        }
    }
}
