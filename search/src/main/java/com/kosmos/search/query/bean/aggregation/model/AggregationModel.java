package com.kosmos.search.query.bean.aggregation.model;

import java.io.Serializable;

import com.drew.lang.annotations.NotNull;
/**
 * Modèle d'aggregation générique pour le module de recherche.
 * Created by cpoisnel on 30/12/16.
 */
public abstract class AggregationModel implements Serializable {

    private static final long serialVersionUID = 8193386274728899951L;

    /**
     * Nom de l'aggrégation.
     */
    @NotNull
    private String name;

    private String label;

    /**
     * Constructeur par défaut (utilisée pour une écriture simplifiée dans Spring).
     */
    public AggregationModel(String name) {
        super();
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Accesseur du type de l'aggrégation (unique par classe).
     * @return le type de l'aggrégation
     */
    public abstract String getType();
}
