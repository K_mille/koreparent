package com.kosmos.search.query.bean.aggregation.model;

/**
 * Modèle d'aggrégation pour les sommes.
 * <p>https://www.elastic.co/guide/en/elasticsearch/reference/2.4/search-aggregations-metrics-sum-aggregation.html</p>
 * @author cpoisnel
 */
public class AggregationSumModel extends AggregationModel {

    private static final long serialVersionUID = -1077195375921564515L;

    private double value;

    /**
     * Constructeur par défaut pour une aggrégation de type Somme.
     */
    public AggregationSumModel(String name) {
        super(name);
    }

    public double getValue() {
        return value;
    }

    public void setValue(final double value) {
        this.value = value;
    }

    @Override
    public String getType() {
        return "sum";
    }
}
