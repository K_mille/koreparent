package com.kosmos.search.query.bean.aggregation.model;

import java.util.Collection;

/**
 * Retour d'aggrégation de type term.
 * Created by cpoisnel on 30/12/16.
 */
public class AggregationTermModel extends AggregationModel {

    private static final long serialVersionUID = 5953751079430229870L;

    private long otherDocCount;

    private transient Collection<BucketModel> buckets;

    /**
     * Constructeur par défaut.
     * @param name Nom de l'aggrégation (identifiant)
     */
    public AggregationTermModel(final String name) {
        super(name);
    }

    @Override
    public String getType() {
        return "term";
    }

    public long getOtherDocCount() {
        return otherDocCount;
    }

    public void setOtherDocCount(final long otherDocCount) {
        this.otherDocCount = otherDocCount;
    }

    public Collection<BucketModel> getBuckets() {
        return buckets;
    }

    public void setBuckets(final Collection<BucketModel> buckets) {
        this.buckets = buckets;
    }
}
