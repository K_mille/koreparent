package com.kosmos.search.query.bean.aggregation.model;

import java.io.Serializable;
import java.util.Collection;

/**
 * Modèle de bucket (panier) permettant d'avoir les informations de retour des aggrégations paramétrées.
 * Created by cpoisnel on 30/12/16.
 */
public class BucketModel implements HasAggregationsModel, Serializable {

    private static final long serialVersionUID = -8382171757643421478L;

    private String key;

    private Long docCount;

    private Collection<AggregationModel> aggregations;

    private String label;

    private boolean selected;

    /**
     * Constructeur.
     */
    public BucketModel() {
        super();
    }

    public String getKey() {
        return key;
    }

    public void setKey(final String key) {
        this.key = key;
    }

    public Long getDocCount() {
        return docCount;
    }

    public void setDocCount(final Long docCount) {
        this.docCount = docCount;
    }

    @Override
    public Collection<AggregationModel> getAggregations() {
        return aggregations;
    }


    public void setAggregations(final Collection<AggregationModel> aggregations) {
        this.aggregations = aggregations;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(final boolean selected) {
        this.selected = selected;
    }
}
