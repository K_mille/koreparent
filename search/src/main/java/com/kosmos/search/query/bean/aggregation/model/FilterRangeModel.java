package com.kosmos.search.query.bean.aggregation.model;

/**
 * Created by camille.lebugle on 20/01/17.
 */
public class FilterRangeModel extends FilterValueModel {

    private String fromName;

    private String toName;

    private String from;

    private String to;

    public FilterRangeModel(final String name) {
        super(name);
    }

    @Override
    public String getType() {
        return "range";
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(final String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(final String to) {
        this.to = to;
    }

    public String getFromName() {
        return fromName;
    }

    public void setFromName(final String fromName) {
        this.fromName = fromName;
    }

    public String getToName() {
        return toName;
    }

    public void setToName(final String toName) {
        this.toName = toName;
    }
}
