package com.kosmos.search.query.bean.aggregation.model;

/**
 * Created by camille.lebugle on 20/01/17.
 */
public class FilterValueModel extends AggregationModel{

    private String value;

    public FilterValueModel(final String name) {
        super(name);
    }

    @Override
    public String getType() {
        return "filter";
    }

    public String getValue() {
        return value;
    }

    public void setValue(final String value) {
        this.value = value;
    }
}
