package com.kosmos.search.query.builder.aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.AggregationBooleanModel;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.bean.aggregation.model.BucketModel;
import com.kosmos.search.query.configuration.SearchAggregationBooleanConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kportal.core.config.MessageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import java.util.ArrayList;
import java.util.Collection;

public class AggregationBooleanModelBuilder extends AggregationTermModelBuilder {

    @Override
    public AggregationModel buildModel(SearchAggregationConfiguration aggregationConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        if (!Terms.class.isAssignableFrom(aggregation.getClass()) || !SearchAggregationBooleanConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())) {
            return null;
        }
        SearchAggregationBooleanConfiguration booleanConfiguration = (SearchAggregationBooleanConfiguration) aggregationConfiguration;
        Terms termAggregation = (Terms) aggregation;
        AggregationBooleanModel aggregationBooleanModel = null;
        if (CollectionUtils.isNotEmpty(termAggregation.getBuckets()) || termAggregation.getSumOfOtherDocCounts() > 0) {
            aggregationBooleanModel = new AggregationBooleanModel(aggregation.getName());
            aggregationBooleanModel.setLabel(MessageHelper.getMessage(booleanConfiguration.getIdExtension(), booleanConfiguration.getLabel()));
            Collection<BucketModel> bucketModels = new ArrayList<>(termAggregation.getBuckets().size());
            aggregationBooleanModel.setBuckets(bucketModels);
            for (final Terms.Bucket bucket : termAggregation.getBuckets()) {
                if (booleanConfiguration.getDefaultValue().equals(bucket.getKeyAsString())) {
                    BucketModel bucketModel = new BucketModel();
                    bucketModel.setKey(bucket.getKeyAsString());
                    bucketModel.setDocCount(bucket.getDocCount());
                    bucketModel.setLabel(MessageHelper.getMessage(booleanConfiguration.getIdExtension(), booleanConfiguration.getDefaultLabel()));
                    bucketModel.setSelected(isBucketSelected(aggregation.getName(), bucket.getKeyAsString(), searchOptions));
                    bucketModels.add(bucketModel);
                }
            }
            if (CollectionUtils.isEmpty(bucketModels)) {
                return null;
            }
        }

        return aggregationBooleanModel;
    }

}
