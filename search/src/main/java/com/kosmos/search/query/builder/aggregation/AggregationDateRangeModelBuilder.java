package com.kosmos.search.query.builder.aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.bean.aggregation.model.AggregationTermModel;
import com.kosmos.search.query.bean.aggregation.model.BucketModel;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationDateRangeConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kportal.core.config.MessageHelper;
import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.range.InternalRange;
import org.elasticsearch.search.aggregations.bucket.range.date.InternalDateRange;

import java.util.ArrayList;
import java.util.Collection;

public class AggregationDateRangeModelBuilder extends AggregationTermModelBuilder {

    @Override
    public AggregationModel buildModel(SearchAggregationConfiguration aggregationConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        if (!InternalDateRange.class.isAssignableFrom(aggregation.getClass()) || !SearchAggregationDateRangeConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())) {
            return null;
        }
        SearchAggregationDateRangeConfiguration dateRangeConfigurationConfiguration = (SearchAggregationDateRangeConfiguration) aggregationConfiguration;
        InternalDateRange dateRangeAggregation = (InternalDateRange) aggregation;
        AggregationTermModel aggregationDateRangeModel = null;
        if (CollectionUtils.isNotEmpty(dateRangeAggregation.getBuckets())) {
            aggregationDateRangeModel = new AggregationTermModel(aggregation.getName());
            aggregationDateRangeModel.setLabel(MessageHelper.getMessage(dateRangeConfigurationConfiguration.getIdExtension(), dateRangeConfigurationConfiguration.getLabel()));
            Collection<BucketModel> bucketModels = new ArrayList<>(dateRangeAggregation.getBuckets().size());
            aggregationDateRangeModel.setBuckets(bucketModels);
            for (final InternalRange.Bucket bucket : dateRangeAggregation.getBuckets()) {
                if (bucket.getDocCount() > 0) {
                    BucketModel bucketModel = new BucketModel();
                    bucketModel.setKey(bucket.getKeyAsString());
                    bucketModel.setDocCount(bucket.getDocCount());
                    bucketModel.setLabel(dateRangeConfigurationConfiguration.getBucketLabelValue(bucket.getKeyAsString()));
                    bucketModel.setSelected(isBucketSelected(dateRangeAggregation.getName(), bucket.getKeyAsString(), searchOptions));
                    bucketModels.add(bucketModel);
                }
            }
            if (CollectionUtils.isEmpty(bucketModels)) {
                return null;
            }
        }
        return aggregationDateRangeModel;
    }

}
