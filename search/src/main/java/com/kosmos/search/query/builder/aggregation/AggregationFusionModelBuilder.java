package com.kosmos.search.query.builder.aggregation;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import com.kosmos.search.query.bean.AggregationFusion;
import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.BucketModel;
import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kportal.core.config.MessageHelper;

public class AggregationFusionModelBuilder extends AggregationTermModelBuilder {

    private List<AggregationFusion> aggregationFusionItems;

    @Override
    protected List<BucketModel> getBucketModels(Terms termAggregation, SearchAggregationTermConfiguration termConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        List<BucketModel> bucketModels = new ArrayList<>(termAggregation.getBuckets().size());
        for (final Terms.Bucket bucket : termAggregation.getBuckets()) {
            if (bucket.getDocCount() > 0) {
                String key = bucket.getKeyAsString();
                BucketModel bucketModel = null;
                AggregationFusion aggregationFusion = getAggregationFusionItem(key);
                if (aggregationFusion == null) {
                    // Pas de fusion d'aggrégation pour cet item
                    bucketModel = new BucketModel();
                    bucketModel.setDocCount(bucket.getDocCount());
                    bucketModel.setLabel(termConfiguration.getBucketLabelValue(bucket.getKeyAsString()));
                } else {
                    key = aggregationFusion.getCodeFusion();
                    // On a une fusion pour celui-ci
                    for (BucketModel existingBucketModel : bucketModels) {
                        if (aggregationFusion.getCodeFusion().equals(existingBucketModel.getKey())) {
                            bucketModel = existingBucketModel;
                        }
                    }
                    if (bucketModel != null) {
                        bucketModels.remove(bucketModel);
                    } else {
                        bucketModel = new BucketModel();
                        bucketModel.setLabel(MessageHelper.getMessage(aggregationFusion.getIdCtx(), aggregationFusion.getPropertyKeyLibelle()));
                    }
                    bucketModel.setDocCount(bucketModel.getDocCount() != null ? bucketModel.getDocCount() + bucket.getDocCount() : bucket.getDocCount());
                }
                bucketModel.setKey(key);
                bucketModel.setSelected(isBucketSelected(aggregation.getName(), key, searchOptions));
                processSubAggregation(bucketModel, bucket.getAggregations(), searchFicheConfiguration, searchOptions);
                bucketModels.add(bucketModel);
            }
        }
        return bucketModels;
    }

    /**
     * Retourne l'aggrégation qui contient la clé passée en paramètre.
     * @param key la clé d'aggregation
     * @return la fusion d'aggrégation désirée, ou null si pas de fusion pour cette aggrégation
     */
    private AggregationFusion getAggregationFusionItem(String key) {
        if (StringUtils.isNotEmpty(key)) {
            for (AggregationFusion fusionItem : aggregationFusionItems) {
                if (fusionItem.getCodesFusionnes().contains(key)) {
                    return fusionItem;
                }
            }
        }
        return null;
    }

    public List<AggregationFusion> getAggregationFusionItems() {
        return aggregationFusionItems;
    }

    public void setAggregationFusionItems(final List<AggregationFusion> aggregationFusionItems) {
        this.aggregationFusionItems = aggregationFusionItems;
    }
}
