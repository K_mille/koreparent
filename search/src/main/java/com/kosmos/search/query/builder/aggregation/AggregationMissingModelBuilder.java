package com.kosmos.search.query.builder.aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.AggregationMissingModel;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.bucket.missing.Missing;

public class AggregationMissingModelBuilder extends AggregationModelBuilder {
    @Override
    public AggregationModel buildModel(SearchAggregationConfiguration aggregationConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        if(Missing.class.isAssignableFrom(aggregation.getClass())) {
            Missing missingAggregation = (Missing) aggregation;
            final long docCount = missingAggregation.getDocCount();
            AggregationMissingModel aggregationModel = null;
            if (docCount > 0L) {
                aggregationModel = new AggregationMissingModel(aggregation.getName());
                aggregationModel.setDocCount(docCount);
            }
            return aggregationModel;
        }else{
            return null;
        }
    }
}
