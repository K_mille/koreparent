package com.kosmos.search.query.builder.aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import org.elasticsearch.search.aggregations.Aggregation;

public abstract class AggregationModelBuilder {

    /**
     * Builder du modele de l'aggregation à partir du retour de la recherche
     * @param aggregationConfiguration : Configuration de l'agregation
     * @param aggregation : agregation retournée par la recherche
     * @param searchOptions : paramètre de la recherche
     * @param searchFicheConfiguration : ensemble des configurations de fiches (utilisée dans le cas d'agregations imbriquee)
     * @return
     */
    public abstract AggregationModel buildModel(SearchAggregationConfiguration aggregationConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration);

    /**
     * Vérifie si le bucket est sélectionné par l'utilisateur.
      * @param aggregationName : nom de l'aggregation
     * @param bucketKey : valeur de l'aggregation
     * @param searchOptions : paramètre de la recherche
     * @return
     */
    protected boolean isBucketSelected(String aggregationName, String bucketKey, SearchOptions searchOptions) {
        boolean filterSelected = false;
        String[] listeFilters = searchOptions.getFilters().get(aggregationName);
        if (listeFilters != null && listeFilters.length > 0) {
            for (String filter : listeFilters) {
                if (bucketKey.equals(filter)) {
                    filterSelected = true;
                    break;
                }
            }
        }
        return filterSelected;
    }

}
