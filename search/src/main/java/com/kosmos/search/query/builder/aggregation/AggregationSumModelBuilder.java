package com.kosmos.search.query.builder.aggregation;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.bean.aggregation.model.AggregationSumModel;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.metrics.sum.Sum;

public class AggregationSumModelBuilder extends AggregationModelBuilder {

    @Override
    public AggregationModel buildModel(SearchAggregationConfiguration aggregationConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        if (Sum.class.isAssignableFrom(aggregation.getClass())) {
            Sum sumAggregation = (Sum) aggregation;
            AggregationSumModel aggregationSumModel = null;
            if (sumAggregation.getValue() > 0) {
                aggregationSumModel = new AggregationSumModel(aggregation.getName());
                aggregationSumModel.setValue(sumAggregation.getValue());
            }
            return aggregationSumModel;
        } else {
            return null;
        }
    }
}
