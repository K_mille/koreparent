package com.kosmos.search.query.builder.aggregation;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.bean.aggregation.model.AggregationTermModel;
import com.kosmos.search.query.bean.aggregation.model.BucketModel;
import com.kosmos.search.query.bean.aggregation.model.HasAggregationsModel;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.utils.QueryUtil;
import com.kportal.core.config.MessageHelper;

public class AggregationTermModelBuilder extends AggregationModelBuilder {

    @Override
    public AggregationModel buildModel(SearchAggregationConfiguration aggregationConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        if (!Terms.class.isAssignableFrom(aggregation.getClass()) || !SearchAggregationTermConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())) {
            return null;
        }
        Terms termAggregation = (Terms) aggregation;
        AggregationTermModel aggregationTermModel = null;
        //Récupération de la configuration de l'aggrégation.
        SearchAggregationTermConfiguration termConfiguration;
        termConfiguration = (SearchAggregationTermConfiguration) aggregationConfiguration;
        if (CollectionUtils.isNotEmpty(termAggregation.getBuckets()) || termAggregation.getSumOfOtherDocCounts() > 0) {
            aggregationTermModel = new AggregationTermModel(aggregation.getName());
            aggregationTermModel.setLabel(MessageHelper.getMessage(termConfiguration.getIdExtension(), termConfiguration.getLabel()));
            aggregationTermModel.setOtherDocCount(termAggregation.getSumOfOtherDocCounts());
            List<BucketModel> bucketModels = getBucketModels(termAggregation, termConfiguration, aggregation, searchOptions, searchFicheConfiguration);
            //Tri des bucket par nombre de hits
            Collections.sort(bucketModels, new Comparator<BucketModel>() {

                @Override
                public int compare(final BucketModel bucketModel, final BucketModel bM2) {
                    return bM2.getDocCount().compareTo(bucketModel.getDocCount());
                }
            });
            aggregationTermModel.setBuckets(bucketModels);
            if (CollectionUtils.isEmpty(bucketModels)) {
                return null;
            }
        }
        return aggregationTermModel;
    }

    /**
     * Construction de la liste des buckets.
     * @param termAggregation  le term aggregation
     * @param termConfiguration la configuration
     * @param aggregation l'aggrégation
     * @param searchOptions les options de recherche
     * @param searchFicheConfiguration la configuration de la recherche
     * @return la liste des buckets correspondant à la recherche
     */
    protected List<BucketModel> getBucketModels(Terms termAggregation, SearchAggregationTermConfiguration termConfiguration, Aggregation aggregation, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration){
        List<BucketModel> bucketModels = new ArrayList<>(termAggregation.getBuckets().size());
        for (final Terms.Bucket bucket : termAggregation.getBuckets()) {
            if (bucket.getDocCount() > 0) {
                BucketModel bucketModel = new BucketModel();
                bucketModel.setKey(bucket.getKeyAsString());
                bucketModel.setDocCount(bucket.getDocCount());
                bucketModel.setLabel(termConfiguration.getBucketLabelValue(bucket.getKeyAsString()));
                bucketModel.setSelected(isBucketSelected(aggregation.getName(), bucket.getKeyAsString(), searchOptions));
                processSubAggregation(bucketModel, bucket.getAggregations(), searchFicheConfiguration, searchOptions);
                bucketModels.add(bucketModel);
            }
        }
        return bucketModels;
    }

    protected static void processSubAggregation(HasAggregationsModel hasAggregationsModel, Aggregations aggregations, SearchFicheConfiguration searchFicheConfiguration, SearchOptions searchOptions) {
        if (null != aggregations && CollectionUtils.isNotEmpty(aggregations.asList())) {
            Collection<AggregationModel> subAggregationsModels = new ArrayList<>(aggregations.asList().size());
            for (Aggregation subAggregation : aggregations) {
                SearchAggregationConfiguration searchAggregationConfiguration = QueryUtil.getAggregationConfiguration(searchFicheConfiguration, subAggregation.getName());
                AggregationModel subAggregationModel = searchAggregationConfiguration.getAggregationModelBuilder().buildModel(searchAggregationConfiguration, subAggregation, searchOptions, searchFicheConfiguration);
                if (subAggregationModel != null) {
                    subAggregationsModels.add(subAggregationModel);
                }
            }
            hasAggregationsModel.setAggregations(subAggregationsModels);
        }
    }

}
