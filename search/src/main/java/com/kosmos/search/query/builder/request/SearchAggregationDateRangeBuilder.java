package com.kosmos.search.query.builder.request;

import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.range.date.DateRangeBuilder;

import com.kosmos.search.query.bean.SearchDateRange;
import com.kosmos.search.query.configuration.SearchAggregationDateRangeConfiguration;

/**
 * Created by camille.lebugle on 07/04/17.
 */
public class SearchAggregationDateRangeBuilder implements SearchAggregationBuilder<SearchAggregationDateRangeConfiguration> {

    @Override
    public AbstractAggregationBuilder build(final SearchAggregationDateRangeConfiguration configuration) {
        DateRangeBuilder dtBuilder = new DateRangeBuilder(configuration.getName());
        dtBuilder.field(configuration.getFormat());
        dtBuilder.field(configuration.getField());
        if (MapUtils.isNotEmpty(configuration.getRangeMap())) {
            for (Map.Entry<String, SearchDateRange> dateRangeEntry : configuration.getRangeMap().entrySet()) {
                String to = StringUtils.isNotBlank(dateRangeEntry.getValue().getTo()) ? dateRangeEntry.getValue().getTo() : "now";
                dtBuilder.addRange(dateRangeEntry.getKey(), dateRangeEntry.getValue().getFrom(), to);
            }
        }
        return dtBuilder;
    }
}
