package com.kosmos.search.query.builder.request;

import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.bucket.terms.TermsBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;
import com.kportal.ihm.utils.FrontUtil;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;
import com.univ.utils.ContexteUtil;

/**
 * Builder pour effectuer une recherche sur les rubriques de types navigation
 */
public class SearchAggregationRubriqueNavigationBuilder extends SearchAggregationTermBuilder {

    @Autowired
    private ServiceRubrique serviceRubrique;

    /**
     * Constructeur par défaut.
     */
    public SearchAggregationRubriqueNavigationBuilder() {
        super();
    }

    /**
     * Builder d'aggrégation pour filtrer la recherche sur les rubriques de types Navigation
     * @param configuration
     *  Configuration de recherche.
     * @return {@link AbstractAggregationBuilder}
     */
    @Override
    public AbstractAggregationBuilder build(final SearchAggregationTermConfiguration configuration) {
        TermsBuilder termsBuilder = (TermsBuilder) super.build(configuration);
        Set<String> codesRubriques = new HashSet<>();
        String rubriqueSite = ContexteUtil.getContexteUniv().getInfosSite().getCodeRubrique();
        final List<RubriqueBean> listeRubriques = serviceRubrique.getRubriquesByCategorieInSubTree(rubriqueSite, Collections.singleton(FrontUtil.CATEGORIE_NAVIGATION));
        for(RubriqueBean rubrique:listeRubriques){
            List<RubriqueBean> listeRubrique = serviceRubrique.getRubriqueByCodeParent(rubrique.getCode());
            if(CollectionUtils.isNotEmpty(listeRubrique)){
                for(RubriqueBean childRubrique:listeRubrique){
                    if (!FrontUtil.CATEGORIE_HIDDEN.equals(childRubrique.getCategorie())) {
                        codesRubriques.add(childRubrique.getCode());
                    }
                }
            }
        }
        termsBuilder.include(codesRubriques.toArray(new String[codesRubriques.size()]));
        return termsBuilder;
    }
}
