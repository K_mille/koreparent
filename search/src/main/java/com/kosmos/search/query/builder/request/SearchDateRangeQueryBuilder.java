package com.kosmos.search.query.builder.request;

import java.util.Map;

import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;

import com.kosmos.search.query.bean.SearchDateRange;
import com.kosmos.search.query.configuration.SearchAggregationDateRangeConfiguration;
import com.univ.utils.DateUtil;

/**
 * Created by camille.lebugle on 07/04/17.
 */
public class SearchDateRangeQueryBuilder implements SearchQueryBuilder<SearchAggregationDateRangeConfiguration> {

    @Override
    public QueryBuilder build(final SearchAggregationDateRangeConfiguration configuration, final String[] values) {
        if (values != null && values.length > 0) {
            BoolQueryBuilder shouldBuilder = new BoolQueryBuilder();
            for (String rangeValue : values) {
                final RangeQueryBuilder queryBuilder = new RangeQueryBuilder(configuration.getField());
                queryBuilder.format(DateUtil.getSimpleDateFormat().toPattern());
                for (Map.Entry<String, SearchDateRange> dateRangeEntry : configuration.getRangeMap().entrySet()) {
                    if (values[0].equals(dateRangeEntry.getKey())) {
                        queryBuilder.gt(dateRangeEntry.getValue().getFrom());
                        queryBuilder.lte(dateRangeEntry.getValue().getTo());
                        shouldBuilder.should(queryBuilder);
                    }
                }
            }
            return shouldBuilder;
        } else {
            return null;
        }
    }
}
