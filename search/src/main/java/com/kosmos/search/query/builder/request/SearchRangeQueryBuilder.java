package com.kosmos.search.query.builder.request;

import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.RangeQueryBuilder;

import com.kosmos.search.query.configuration.SearchFilterRangeConfiguration;
import com.kosmos.search.query.configuration.SearchFilterValueConfiguration;
import com.univ.utils.DateUtil;

/**
 * Created by camille.lebugle on 19/01/17.
 * Builder pour les filtres de type Range Query.
 * Ces filtres permettent d'effectuer une sélection sur une plage de valeur (numérique, date).
 */
public class SearchRangeQueryBuilder implements SearchQueryBuilder<SearchFilterValueConfiguration> {

    /**
     * Builder d'une requête à partir d'une configuration
     * @param configuration
     *      Configuration d'un filtre
     * @param values
     *      Valeurs correspondant au filtre
     * @return un fragment de requête Elasticsearch (ajout en "must" sur la requête)
     */
    @Override
    public QueryBuilder build(SearchFilterValueConfiguration configuration, final String[] values) {
        SearchFilterRangeConfiguration filterRangeConfiguration = (SearchFilterRangeConfiguration) configuration;
        if (values != null && values.length > 0 && StringUtils.isNotBlank(values[0])) {
            final RangeQueryBuilder queryBuilder = new RangeQueryBuilder(configuration.getField());
            queryBuilder.format(DateUtil.getSimpleDateFormat().toPattern());
            switch (filterRangeConfiguration.getRangeFilterOperator()) {
                case GT:
                    queryBuilder.gt(values[0]);
                    break;
                case GTE:
                    queryBuilder.gte(values[0]);
                    break;
                case LT:
                    queryBuilder.lt(values[0]);
                    break;
                case LTE:
                    queryBuilder.lte(values[0]);
                    break;
            }
            return queryBuilder;
        } else {
            return null;
        }
    }
}
