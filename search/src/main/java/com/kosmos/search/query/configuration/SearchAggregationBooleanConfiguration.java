package com.kosmos.search.query.configuration;

import com.kportal.core.config.MessageHelper;

/**
 * Created by camille.lebugle on 21/02/17.
 */
public class SearchAggregationBooleanConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = 996165961305134567L;

    private String defaultValue;

    private String defaultLabel;

    public String getDefaultValue() {
        return defaultValue;
    }

    public void setDefaultValue(final String defaultValue) {
        this.defaultValue = defaultValue;
    }

    public String getDefaultLabel() {
        return defaultLabel;
    }

    public void setDefaultLabel(final String defaultLabel) {
        this.defaultLabel = defaultLabel;
    }

    @Override
    public String getBucketLabelValue(String rawValue) {
        return MessageHelper.getMessage(this.getIdExtension(), this.getDefaultLabel());
    }

}
