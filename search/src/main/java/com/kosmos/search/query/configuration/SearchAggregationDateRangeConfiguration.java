package com.kosmos.search.query.configuration;

import java.util.Map;

import com.kosmos.search.query.bean.SearchDateRange;
import com.kportal.core.config.MessageHelper;

/**
 * Created by camille.lebugle on 07/04/17.
 */
public class SearchAggregationDateRangeConfiguration extends SearchAggregationValueConfiguration {

    private static final long serialVersionUID = -8893910054256794319L;

    private transient Map<String, SearchDateRange> rangeMap;

    private String format;

    public SearchAggregationDateRangeConfiguration() {
        //Default Constructor
    }

    public Map<String, SearchDateRange> getRangeMap() {
        return rangeMap;
    }

    public void setRangeMap(final Map<String, SearchDateRange> rangeMap) {
        this.rangeMap = rangeMap;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(final String format) {
        this.format = format;
    }

    public String getBucketLabelValue(String rawValue) {
        return MessageHelper.getMessage(this.getIdExtension(), rawValue);
    }

}
