package com.kosmos.search.query.configuration;

import java.util.Set;

/**
 * Configuration de recherche pour les groupement d'agrégations
 */
public class SearchAggregationGroupeConfiguration extends SearchAggregationConfiguration{

    //Liste des agrégations à grouper
    private Set<SearchFilterConfiguration> childAggregation;

    public Set<SearchFilterConfiguration> getChildAggregation() {
        return childAggregation;
    }

    public void setChildAggregation(final Set<SearchFilterConfiguration> childAggregation) {
        this.childAggregation = childAggregation;
    }

}
