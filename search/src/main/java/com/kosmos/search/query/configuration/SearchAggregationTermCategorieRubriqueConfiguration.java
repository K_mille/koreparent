package com.kosmos.search.query.configuration;

import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.util.LabelUtils;

/**
 * Created by camille.lebugle on 12/01/17.
 */
public class  SearchAggregationTermCategorieRubriqueConfiguration extends SearchAggregationTermConfiguration {

    private static final String TYPE_THEMATIQUE_LIBELLE = "80";

    private static final long serialVersionUID = -3851877024642720390L;

    @Override
    public String getBucketLabelValue(final String rawValue) {
        LabelBean labelBean = LabelUtils.getLabel(TYPE_THEMATIQUE_LIBELLE, rawValue, "0");
        return labelBean != null ? labelBean.getLibelle() : rawValue;
    }
}