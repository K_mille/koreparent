package com.kosmos.search.query.configuration;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.CodeLibelle;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.LabelBean;
import com.univ.objetspartages.services.ServiceLabel;
import com.univ.utils.ContexteUtil;

/**
 * Configuration d'une aggregation par Terme, avec les labels du CMS.
 * Created by cpoisnel on 19/12/16.
 */
public class SearchAggregationTermLabelConfiguration extends SearchAggregationTermConfiguration {

    private final static String SERVICE_LABEL_STRATEGY = "SERVICE_LABEL_STRATEGY";
    private final static String DAT_LABEL_STRATEGY = "DAT_LABEL_STRATEGY";

    private static final long serialVersionUID = 5148844867614908320L;

    private String labelType;

    private String labelStrategy = SERVICE_LABEL_STRATEGY;

    /**
     * Constructeur.
     */
    public SearchAggregationTermLabelConfiguration() {
    }

    @Override
    public String getBucketLabelValue(String rawValue) {
        String value = StringUtils.EMPTY;

        if(SERVICE_LABEL_STRATEGY.equals(labelStrategy)) {
            ServiceLabel serviceLabel = ServiceManager.getServiceForBean(LabelBean.class);
            LabelBean labelBean = serviceLabel.getByTypeCodeLanguage(this.labelType, rawValue, ContexteUtil.getContexteUniv().getLangue());
            if(labelBean != null) {
                value = labelBean.getLibelle();
            }
        }else if (DAT_LABEL_STRATEGY.equals(labelStrategy)){
            value = CodeLibelle.lireLibelle(getIdExtension(), labelType, ContexteUtil.getContexteUniv().getLocale(), rawValue);
        }

        return value;
    }

    public String getLabelType() {
        return labelType;
    }

    public void setLabelType(final String labelType) {
        this.labelType = labelType;
    }

    public String getLabelStrategy() {
        return labelStrategy;
    }

    public void setLabelStrategy(String labelStrategy) {
        this.labelStrategy = labelStrategy;
    }

}
