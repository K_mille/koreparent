package com.kosmos.search.query.configuration;

import org.apache.commons.lang3.StringUtils;

import com.kportal.core.config.MessageHelper;

/**
 * Created by charlie.camus on 09/06/17.
 */
public class SearchAggregationTermPropertiesConfiguration extends SearchAggregationTermConfiguration {

    private static final String SEPARATOR = ".";

    private String idContexte;

    private String prefix;

    @Override
    public String getBucketLabelValue(String rawValue) {
        StringBuilder key = new StringBuilder();
        if(StringUtils.isNotEmpty(prefix)){
            key.append(prefix);
            key.append(SEPARATOR);
        }
        key.append(rawValue);
        String retour = MessageHelper.getMessage(idContexte, key.toString());
        return StringUtils.isNotEmpty(retour) ? retour : rawValue;
    }

    public String getIdContexte() {
        return idContexte;
    }

    public void setIdContexte(final String idContexte) {
        this.idContexte = idContexte;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(final String prefix) {
        this.prefix = prefix;
    }
}
