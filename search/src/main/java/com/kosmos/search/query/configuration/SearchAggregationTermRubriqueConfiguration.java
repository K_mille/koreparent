package com.kosmos.search.query.configuration;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.RubriqueBean;
import com.univ.objetspartages.services.ServiceRubrique;

/**
 * Created by camille.lebugle on 05/01/17.
 */
public class SearchAggregationTermRubriqueConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = 6763998084160679367L;

    @Override
    public String getBucketLabelValue(final String rawValue) {
        ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
        RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(rawValue);
        return rubriqueBean!= null ? rubriqueBean.getIntitule() : rawValue;
    }
}
