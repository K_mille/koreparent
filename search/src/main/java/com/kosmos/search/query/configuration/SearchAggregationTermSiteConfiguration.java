package com.kosmos.search.query.configuration;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.exception.SearchException;
import com.univ.multisites.InfosSite;
import com.univ.multisites.service.ServiceInfosSite;

/**
 * Created by camille.lebugle on 23/02/17.
 */
public class SearchAggregationTermSiteConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = 1619133384701653593L;

    @Autowired
    private transient ServiceInfosSite serviceInfosSite;

    public SearchAggregationTermSiteConfiguration() {
        //Default Contructor
    }

    @Override
    public String getBucketLabelValue(final String rawValue) {
        try {
            final Collection<InfosSite> listeSites = serviceInfosSite.getListeTousInfosSites();
            for (InfosSite site : listeSites) {
                if (site.getCodeRubrique().equals(rawValue)) {
                    return site.getIntitule();
                }
            }
        } catch (Exception e) {
            throw new SearchException("Erreur lors de la recherche de sites", e);
        }
        return rawValue;
    }
}
