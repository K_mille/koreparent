package com.kosmos.search.query.configuration;

import com.univ.objetspartages.om.StructureModele;
import com.univ.objetspartages.services.ServiceStructure;
import com.univ.utils.ContexteUtil;

/**
 * Created by camille.lebugle on 22/02/17.
 */
public class SearchAggregationTermStructureConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = 7367609499918640716L;

    private ServiceStructure serviceStructure;

    @Override
    public String getBucketLabelValue(final String rawValue) {
        StructureModele structureModele = serviceStructure.getByCodeLanguage(rawValue, ContexteUtil.getContexteUniv().getLangue());
        return structureModele != null ? structureModele.getLibelleLong() : rawValue;
    }

    public void setServiceStructure(final ServiceStructure serviceStructure) {
        this.serviceStructure = serviceStructure;
    }
}