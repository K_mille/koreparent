package com.kosmos.search.query.configuration;

import org.apache.commons.lang3.StringUtils;

import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.om.ReferentielObjets;

/**
 * Created by camille.lebugle on 06/01/17.
 */
public class SearchAggregationTermTypeFicheConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = -1368404595028471697L;

    private static final String PREFIX_PROPRIETE = "SEARCH_LIBELLE";

    private static final String AUTRE_CONTENU_KEY = "autreContenu";

    @Override
    public String getBucketLabelValue(final String rawValue) {
        if (rawValue.equals(AUTRE_CONTENU_KEY)) {
            return MessageHelper.getCoreMessage("SEARCH_AGGREGATION_TYPE_FICHE.OTHER_CONTENTS");
        }
        String retour = StringUtils.EMPTY;
        String codeObjet = ReferentielObjets.getCodeObjet(rawValue);
        if (StringUtils.isNotEmpty(codeObjet)) {
            String idExtension = ReferentielObjets.getExtension(codeObjet);
            retour = MessageHelper.getMessage(idExtension, PREFIX_PROPRIETE + "." + rawValue);
        }
        // Si pas de surcharge on prend le libellé par défaut de l'objet
        if (StringUtils.isEmpty(retour)) {
            Objetpartage objetpartage = ReferentielObjets.getObjetByNom(rawValue);
            retour = objetpartage != null ? objetpartage.getLibelleAffichable() : StringUtils.EMPTY;
        }
        return retour;
    }
}