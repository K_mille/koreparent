package com.kosmos.search.query.configuration;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.UtilisateurBean;
import com.univ.objetspartages.services.ServiceUser;

/**
 * Created by camille.lebugle on 12/01/17.
 */
public class SearchAggregationTermUserConfiguration extends SearchAggregationTermConfiguration {

    private static final long serialVersionUID = -1533163747146178131L;

    @Override
    public String getBucketLabelValue(final String rawValue) {
        ServiceUser serviceUser = (ServiceUser) ServiceManager.getServiceForBean(UtilisateurBean.class);
        UtilisateurBean utilisateurBean = serviceUser.getByCode(rawValue);
        return utilisateurBean != null ? utilisateurBean.getNom() : rawValue;
    }
}
