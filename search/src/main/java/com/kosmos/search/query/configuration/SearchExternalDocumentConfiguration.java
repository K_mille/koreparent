package com.kosmos.search.query.configuration;

import java.util.LinkedHashSet;
import java.util.Set;

/**
 * Created by camille.lebugle on 14/04/17.
 */
public class SearchExternalDocumentConfiguration extends SearchCommonConfiguration {

    private Set<SearchIndexConfiguration> index = new LinkedHashSet<>();

    private String className;

    public Set<SearchIndexConfiguration> getIndex() {
        return index;
    }

    public void setIndex(final Set<SearchIndexConfiguration> index) {
        this.index = index;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(final String className) {
        this.className = className;
    }
}
