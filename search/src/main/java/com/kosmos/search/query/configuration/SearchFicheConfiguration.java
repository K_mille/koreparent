package com.kosmos.search.query.configuration;

import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.util.Assert;

import com.kosmos.search.query.utils.QueryUtil;

/**
 * Configuration de recherche par fiche (ou de plusieurs fiches).
 */
public class SearchFicheConfiguration extends SearchCommonConfiguration {

    private static final long serialVersionUID = -7273693594032435986L;

    private Set<SearchIndexConfiguration> index = new LinkedHashSet<>();

    /**
     * Constructeur par défaut, sans définition d'un nom d'objet.
     */
    public SearchFicheConfiguration() {
        super();
    }

    /**
     * Constructeur, avec un seul index défini par défaut.
     * @param objectName
     * Nom de l'objet, correspondant à un seul index
     */
    public SearchFicheConfiguration(String objectName) {
        super();
        this.setIndex(new SearchIndexConfiguration(objectName));
    }

    public SearchFicheConfiguration(SearchIndexConfiguration indexConfiguration) {
        super();
        this.setIndex(indexConfiguration);
    }

    public Set<SearchIndexConfiguration> getIndex() {
        return index;
    }

    public void setIndex(final Set<SearchIndexConfiguration> index) {
        this.index = index;
    }

    public void setIndex(SearchIndexConfiguration index) {
        this.index.add(index);
    }

    /**
     * Surcharge de la méthode parente pour résoudre les noms de champs générique (avec un joker)
     * @param fieldsSet
     */
    @Override
    public void setFieldsSet(final Set<SearchFieldConfiguration> fieldsSet) {
        Assert.notNull(fieldsSet, "Le fieldSet ne doit pas être null");
        this.fields = new HashMap<>(fieldsSet.size());
        String indexName = StringUtils.EMPTY;
        if (CollectionUtils.isNotEmpty(this.getIndex()) && this.getIndex().iterator().hasNext()) {
            indexName = ((SearchIndexConfiguration) this.getIndex().iterator().next()).getObjectName();
        }
        for (SearchFieldConfiguration searchFieldConfiguration : fieldsSet) {
            if (StringUtils.isNotBlank(indexName)) {
                searchFieldConfiguration.setFieldName(QueryUtil.translateSearchName(searchFieldConfiguration.getFieldName(), indexName));
            }
            this.fields.put(searchFieldConfiguration.getFieldName(), searchFieldConfiguration);
        }
    }

}
