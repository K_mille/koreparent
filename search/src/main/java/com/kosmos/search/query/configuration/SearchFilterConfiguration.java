package com.kosmos.search.query.configuration;

import java.io.Serializable;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.kosmos.search.query.builder.request.SearchQueryBuilder;
import com.kosmos.search.query.view.bean.SearchFilterModel;

/**
 * Configuration d'un filtre de recherche.
 *
 * Created by cpoisnel on 19/12/16.
 * @param <V>
 *     Type de filtre
 */
public class SearchFilterConfiguration<V extends SearchFilterModel> implements Serializable {

    private static final long serialVersionUID = 7319905491164262421L;

    /**
     * Nom du paramètre dans la requête HTTP et de l'identifiant du filtre.
     * Un filtre est unique que par le nom !
     */
    private String name;

    /**
     * Libellé du iltre à afficher
     */
    private String label;

    private String idExtension;

    /**
     * Requête de filtre Elasticsearch.
     */
    private transient SearchQueryBuilder queryBuilder;

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public SearchQueryBuilder getQueryBuilder() {
        return queryBuilder;
    }

    public void setQueryBuilder(final SearchQueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(final String label) {
        this.label = label;
    }

    public String getIdExtension() {
        return idExtension;
    }

    public void setIdExtension(final String idExtension) {
        this.idExtension = idExtension;
    }

    /**
     * Vérifie l'égalité de deux filtres. Deux filtres sont égaux s'ils ont le même nom
     * @param o
     * Filtre
     * @return
     * Vrai si le filtre est égal, faux sinon
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        final SearchFilterConfiguration<?> that = (SearchFilterConfiguration<?>) o;
        return new EqualsBuilder().append(name, that.name).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).toHashCode();
    }
}
