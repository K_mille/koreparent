package com.kosmos.search.query.configuration;

import com.kosmos.search.query.utils.RangeFilterOperator;

/**
 * Created by camille.lebugle on 19/01/17.
 */
public class SearchFilterRangeConfiguration extends SearchFilterConfiguration  implements SearchFilterValueConfiguration{

    private static final long serialVersionUID = 7997167230133452498L;

    private String field;

    private RangeFilterOperator rangeFilterOperator;

    private String group;

    public RangeFilterOperator getRangeFilterOperator() {
        return rangeFilterOperator;
    }

    public void setRangeFilterOperator(final RangeFilterOperator rangeFilterOperator) {
        this.rangeFilterOperator = rangeFilterOperator;
    }

    @Override
    public String getField() {
        return field;
    }

    @Override
    public void setField(final String field) {
        this.field = field;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(final String group) {
        this.group = group;
    }
}
