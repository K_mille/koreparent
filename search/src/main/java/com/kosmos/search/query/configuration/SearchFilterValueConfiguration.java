package com.kosmos.search.query.configuration;

/**
 * Created by cpoisnel on 28/12/16.
 */
public interface SearchFilterValueConfiguration {

    String getField();

    void setField(String field);

}
