package com.kosmos.search.query.configuration;

/**
 * Created by cpoisnel on 29/12/16.
 */
public class SearchPluginConfiguration extends SearchObjectConfiguration {

    private static final long serialVersionUID = 4911340437486920932L;

    private String id;

    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }
}
