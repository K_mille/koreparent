package com.kosmos.search.query.service;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.SearchResultBean;

/**
 * Service de recherche Elasticsearch.
 * @author cpoisnel
 *
 */
public interface ServiceSearcher {

    /**
     * Recherche Elasticsearch.
     * <p>
     *     Effectue une recherche sur le ou les index correspondant aux types demandés dans les options de recherche {@link SearchOptions}
     * </p>
     * @param searchOptions
     * Options de recherche
     * @return le bean de résultat de recherche
     */
    SearchResultBean search(SearchOptions searchOptions);
}
