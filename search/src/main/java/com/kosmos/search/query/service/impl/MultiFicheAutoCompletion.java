package com.kosmos.search.query.service.impl;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.bean.SearchResultDoc;
import com.kosmos.search.query.service.ServiceSearcher;
import com.kportal.core.config.PropertyHelper;
import com.univ.autocomplete.bean.AutoCompleteSuggestion;
import com.univ.autocomplete.processus.GestionAutoCompletion;
import com.univ.autocomplete.utils.AutoCompletionUtils;
import com.univ.utils.ContexteUtil;

public class MultiFicheAutoCompletion extends GestionAutoCompletion {

    public static final String ID_BEAN = "multiFicheAutoComplete";

    private static final int DEFAULT_AUTOCOMPLETE_LIMIT = 7;

    private static final Logger LOG = LoggerFactory.getLogger(MultiFicheAutoCompletion.class);

    @Autowired
    private ServiceSearcher serviceSearcher;

    /**
     * A partir de l'objet FicheUniv, on retrouve l'ensemble des fiches et on les map dans une liste de résultat exploitable
     *
     * @param resultBean
     * @return
     */
    private static List<AutoCompleteSuggestion> getResults(final SearchResultBean resultBean) {
        final List<AutoCompleteSuggestion> resultats = new ArrayList<>();
        if(CollectionUtils.isNotEmpty(resultBean.getDocs())){
            for (final SearchResultDoc resultDoc : resultBean.getDocs()) {
                final AutoCompleteSuggestion auto = new AutoCompleteSuggestion();
                auto.setValue(resultDoc.getTitre());
                resultats.add(auto);
            }
        }
        return resultats;
    }

    @Override
    public List<AutoCompleteSuggestion> traiterRechercheDepuisRequete(final HttpServletRequest req) {
        SearchOptions searchOptions = new SearchOptions();
        searchOptions.setAutocomplete(true);
        searchOptions.setAuthorizedGroups(ContexteUtil.getContexteUniv().getGroupesDsiAvecAscendants());
        searchOptions.setQuery(req.getParameter("query"));
        searchOptions.setSearchParameterBeanKey(req.getParameter(AutoCompletionUtils.PARAM_BEANKEY_SEARCHPARAMETER));
        searchOptions.setAutoCompleteName("titre_completion");
        // Récupération du maxsize
        int limit = PropertyHelper.getCorePropertyAsInt("autocomplete.maxsize", DEFAULT_AUTOCOMPLETE_LIMIT);
        searchOptions.setLimit(limit);
        List<AutoCompleteSuggestion> resultats = getResults(serviceSearcher.search(searchOptions));
        return resultats;
    }

    @Override
    public String getKeyForCache() {
        String code = "anonyme";
        if (ContexteUtil.getContexteUniv() != null) {
            if (StringUtils.isNotEmpty(ContexteUtil.getContexteUniv().getCode())) {
                code = ContexteUtil.getContexteUniv().getCode();
            }
        }
        return String.format("%s_%s", ID_BEAN, code);
    }
}
