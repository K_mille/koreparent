package com.kosmos.search.query.service.impl;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.Cacheable;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kosmos.search.exception.SearchException;
import com.kosmos.search.query.bean.AggregationFusion;
import com.kosmos.search.query.configuration.SearchAggregationGroupeConfiguration;
import com.kosmos.search.query.configuration.SearchCommonConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.configuration.SearchFieldConfiguration;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.kosmos.search.query.configuration.SearchIndexConfiguration;
import com.kosmos.search.query.configuration.SearchPluginConfiguration;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.search.utils.IndexerUtil;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.cms.objetspartages.annotation.Autocompletion;
import com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper;
import com.kportal.cms.objetspartages.annotation.Label;
import com.kportal.cms.objetspartages.annotation.Rubrique;
import com.kportal.cms.objetspartages.annotation.User;
import com.kportal.extension.module.plugin.objetspartages.IPluginFiche;
import com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper;
import com.univ.objetspartages.bean.PersistenceBean;
import com.univ.objetspartages.om.AbstractOm;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.FicheUnivHelper;

/**
 * Service permettant de constituer et d'utiliser les configurations de recherche (paramètres de fiche recherchés, boost, aggrégations,...).
 *
 * Created by cpoisnel on 28/12/16.
 */
public class ServiceSearchConfiguration {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceSearchConfiguration.class);

    protected static final Collection<String> IGNORED_FIELDS = Arrays.asList("serialVersionUID");

    public static final String PREFIX_FICHE = "fiche";

    public static final String PREFIX_PLUGIN = "plugins";

    public static final String PREFIX_PLUGIN_ITEMS = "items";

    public static final String FIELD_SEPARATOR = ".";

    private static final String DEFAULT_SEARCH_CONFIGURATION = "searchFicheConfiguration";

    /**
     * Récupération de la configuration mergée de fiches filtrées par nom d'objet.
     * @param defaultAggregation : On n'affiche que les facettes génériques (communes à chaque objet)
     * @param objectNames
     * Noms des objets (FicheUniv)
     * @return une configuration de fiche, fusion de toutes les configurations de fiche applicables
     */
    public SearchFicheConfiguration fetchMergedFilteredConfiguration(final boolean defaultAggregation, final String... objectNames) {
        // Récupérer l'ensemble des configurations de fiche
        Collection<SearchFicheConfiguration> searchFicheConfigurationList = this.fetchFilteredConfigurations(objectNames);
        SearchFicheConfiguration defaultFicheConfiguration = (SearchFicheConfiguration) ApplicationContextManager.getCoreContextBean(DEFAULT_SEARCH_CONFIGURATION);
        if (defaultFicheConfiguration == null) {
            LOG.error(String.format("Aucune configuration de recherche par défaut trouvée %s", DEFAULT_SEARCH_CONFIGURATION));
            throw new SearchException("Impossible d'effectuer la recherche, aucune configuration de recherche par défaut");
        }
        // Si pas de résultat, on soulève une exception
        if (CollectionUtils.isEmpty(searchFicheConfigurationList)) {
            LOG.error("Aucune configuration de recherche avec les noms d'objets {}", Arrays.asList(objectNames));
            throw new SearchException("Impossible d'effectuer la recherche, aucune configuration de recherche");
        }
        SearchFicheConfiguration searchFicheConfiguration = new SearchFicheConfiguration();
        Map<String, SearchFieldConfiguration> ficheFields = new HashMap<>();
        Map<String, Map<String, SearchFieldConfiguration>> ficheAutoCompleteFields = new HashMap<>();
        addChildFilter(defaultFicheConfiguration);
        searchFicheConfiguration.getFilters().addAll(defaultFicheConfiguration.getFilters());
        for (SearchFicheConfiguration searchFicheItemConfiguration : searchFicheConfigurationList) {
            searchFicheConfiguration.getIndex().addAll(searchFicheItemConfiguration.getIndex());
            if (!defaultAggregation) {
                searchFicheConfiguration.getFilters().addAll(searchFicheItemConfiguration.getFilters());
            }
            this.fetchMergedFilteredField(searchFicheItemConfiguration.getFields().values(), ficheFields);
            this.fetchMergedFilteredCompletionField(searchFicheItemConfiguration.getAutoCompleteFields(), ficheAutoCompleteFields);
        }
        searchFicheConfiguration.setFields(ficheFields);
        searchFicheConfiguration.setAutoCompleteFields(ficheAutoCompleteFields);
        return searchFicheConfiguration;
    }

    /**
     * Merge de toutes les configurations de fiches.
     * On parcourt les configurations de champs pour ne les ajouter qu'une et une seule fois (il n'est pas possible de faire une différenciation).
     * Si plusieurs sont trouvés, on garde celle ayant le boost le plus élevé.
     * @param fieldList
     * @param mergedFieldMap
     */
    private void fetchMergedFilteredField(Collection<SearchFieldConfiguration> fieldList, Map<String, SearchFieldConfiguration> mergedFieldMap) {
        //
        for (SearchFieldConfiguration itemField : fieldList) {
            if (mergedFieldMap.containsKey(itemField.getFieldName())) {
                SearchFieldConfiguration oldField = mergedFieldMap.get(itemField.getFieldName());
                // On prend celui qui a le boost le plus élevé.
                if (oldField.compareTo(itemField) < 0) {
                    mergedFieldMap.put(itemField.getFieldName(), itemField);
                }
            } else {
                mergedFieldMap.put(itemField.getFieldName(), itemField);
            }
        }
    }

    /**
     * Merge de toutes les configurations de fiches.
     * On parcourt les configurations de champs pour ne les ajouter qu'une et une seule fois (il n'est pas possible de faire une différenciation).
     * Si plusieurs sont trouvés, on garde celle ayant le boost le plus élevé.
     * @param mergedFieldMap
     */
    private void fetchMergedFilteredCompletionField(Map<String, Map<String, SearchFieldConfiguration>> wildcardMap, Map<String, Map<String, SearchFieldConfiguration>> mergedFieldMap) {
        //
        for (Map.Entry<String, Map<String, SearchFieldConfiguration>> entryMap : wildcardMap.entrySet()) {
            if (mergedFieldMap.containsKey(entryMap.getKey())) {
                Map<String, SearchFieldConfiguration> oldMap = mergedFieldMap.get(entryMap.getKey());
                for (Map.Entry<String, SearchFieldConfiguration> childEntry : entryMap.getValue().entrySet()) {
                    if (oldMap.containsKey(childEntry.getKey())) {
                        // On prend celui qui a le boost le plus élevé.
                        if (childEntry.getValue().compareTo(oldMap.get(childEntry.getKey())) > 0) {
                            oldMap.put(childEntry.getKey(), childEntry.getValue());
                        }
                    } else {
                        oldMap.put(childEntry.getKey(), childEntry.getValue());
                    }
                }
            } else {
                mergedFieldMap.put(entryMap.getKey(), entryMap.getValue());
            }
        }
    }

    /**
     * Récupération des configurations de fiches filtrées par nom d'objet.
     * @param objectNames
     * Noms des objets (FicheUniv). Si vide on retourne toutes les configurations de fiche
     * @return les configurations de recherche de fiche
     */
    public Collection<SearchFicheConfiguration> fetchFilteredConfigurations(final String... objectNames) {
        Collection<SearchFicheConfiguration> searchFicheConfigurationList = fetchAllConfigurationsBean();
        if (objectNames.length == 0) {
            return searchFicheConfigurationList;
        }
        AggregationFusion aggregationFusion = (AggregationFusion) ApplicationContextManager.getBean("core", "typeFicheFusionAggregation");
        Collection<String> objetNamesList = Arrays.asList(objectNames);
        Collection<SearchFicheConfiguration> filteredConfigurations = new ArrayList<>(objectNames.length);
        for (SearchFicheConfiguration searchFicheConfiguration : searchFicheConfigurationList) {
            for (SearchIndexConfiguration searchIndexConfiguration : searchFicheConfiguration.getIndex()) {
                if (objetNamesList.contains(searchIndexConfiguration.getObjectName())) {
                    filteredConfigurations.add(searchFicheConfiguration);
                } else if (aggregationFusion != null && objetNamesList.contains(aggregationFusion.getCodeFusion()) && aggregationFusion.getCodesFusionnes().contains(searchIndexConfiguration.getObjectName())) {
                    filteredConfigurations.add(searchFicheConfiguration);
                }
            }
        }
        return filteredConfigurations;
    }

    /**
     * Methode pour charger toutes les configurations pour chaque index.
     * On itère sur toutes les fiches indexables.
     * Cette méthode inititalise les beans avec des valeurs par défaut puis merge les surcharges si il y en a.
     * @return configurationEsBeanList : liste de configuration
     */
    @Cacheable(value = "ServiceSearcher.fetchAllConfigurationsBean")
    public List<SearchFicheConfiguration> fetchAllConfigurationsBean() {
        LOG.debug("Calcul des configurations de recherche");
        List<SearchFicheConfiguration> searchFicheConfigurationList = new ArrayList<>();
        //Récupération de toutes les configurations de fiche.
        for (Objetpartage objetpartage : ReferentielObjets.getObjetsPartagesTries()) {
            // On va d'abord chercher la configuration de recherche, si elle existe (valeur de l'autoscan)
            SearchFicheConfiguration searchFicheConfiguration = fetchSearchConfiguration(objetpartage.getNomObjet());
            SearchFicheConfiguration defaultSearchFicheConfiguration = null;
            if (null == searchFicheConfiguration) {
                searchFicheConfiguration = fetchDefaultSearchConfiguration();
                searchFicheConfiguration.setIndex(new SearchIndexConfiguration(objetpartage.getNomObjet(), 1.0F));
            } else {
                //Il existe une configuration déclarée en XML. On ajoute les filtres enfants.
                addChildFilter(searchFicheConfiguration);
            }
            // La configuration de recherche ne peut pas être nulle
            // On vérifie que la configuration est paramétrée pour chercher les attributs par introspection
            if (searchFicheConfiguration.isAutoscan()) {
                defaultSearchFicheConfiguration = new SearchFicheConfiguration(objetpartage.getNomObjet());
                final FicheUniv fiche = FicheUnivHelper.instancierFiche(objetpartage.getNomObjet());
                PersistenceBean ficheBean = ((AbstractOm) fiche).getPersistenceBean();
                scanDefaultConfiguration(defaultSearchFicheConfiguration, ficheBean.getClass(), objetpartage.getNomObjet());
            } else {
                LOG.debug("Autoscan désactivé pour la configuration de recherche de l'objet {}", objetpartage.getNomObjet());
            }
            // Ajout de la configuration des plugins
            mergePluginsConfiguration(searchFicheConfiguration, objetpartage.getNomObjet());
            // Merge de la configuration du bean avec celui déclaré en Spring
            mergeOverridedConfiguration(searchFicheConfiguration, defaultSearchFicheConfiguration, objetpartage.getNomObjet());
            LOG.debug("Configuration de recherche initialisée {}", searchFicheConfiguration);
            searchFicheConfigurationList.add(searchFicheConfiguration);
        }
        //Recupération des configurations d'objets autres que des fiches (sites externes par exemple)
        searchFicheConfigurationList.addAll(QueryUtil.fetchExternalDocumentConfiguration());
        return searchFicheConfigurationList;
    }

    private void addChildFilter(SearchFicheConfiguration searchFicheConfiguration) {
        List<SearchFilterConfiguration> childFilter = new ArrayList<>();
        for (SearchFilterConfiguration filterConfiguration : searchFicheConfiguration.getFilters()) {
            if (SearchAggregationGroupeConfiguration.class.isAssignableFrom(filterConfiguration.getClass())) {
                SearchAggregationGroupeConfiguration groupeConfiguration = (SearchAggregationGroupeConfiguration) filterConfiguration;
                if (CollectionUtils.isNotEmpty(groupeConfiguration.getChildAggregation())) {
                    childFilter.addAll(groupeConfiguration.getChildAggregation());
                }
            }
        }
        if (CollectionUtils.isNotEmpty(childFilter)) {
            searchFicheConfiguration.getFilters().addAll(childFilter);
        }
    }

    protected SearchFicheConfiguration initDefaultConfiguration(Objetpartage objetpartage) {
        SearchFicheConfiguration searchFicheConfiguration = null;
        // On manipule les ficheuniv pour l'instanciation, mais on ne veut plus les utiliser à terme (on utilise bien le bean)
        final FicheUniv fiche = FicheUnivHelper.instancierFiche(objetpartage.getNomObjet());
        if (FicheAnnotationHelper.isIndexable(fiche)) {
            searchFicheConfiguration = new SearchFicheConfiguration(objetpartage.getNomObjet());
            PersistenceBean ficheBean = ((AbstractOm) fiche).getPersistenceBean();
            scanDefaultConfiguration(searchFicheConfiguration, ficheBean.getClass(), objetpartage.getNomObjet());
        }
        return searchFicheConfiguration;
    }

    /**
     * Merge la configuration de plugin avec celle de la recherche.
     * @param searchFicheConfiguration
     * Configuration de recherche à fusionner
     */
    protected void mergePluginsConfiguration(final SearchFicheConfiguration searchFicheConfiguration, String objectName) {
        // Plugins activés par objet
        for (SearchIndexConfiguration indexConfiguration : searchFicheConfiguration.getIndex()) {
            String objet = indexConfiguration.getObjectName();
            Collection<IPluginFiche> plugins = PluginFicheHelper.getPluginsFromClass(ReferentielObjets.getClasseObjet(objet));
            for (IPluginFiche plugin : plugins) {
                // Vérification qu'il n'existe pas une configuration de plugin associée à ce plugin ?
                // Configuration du plugin
                // On prend l'ID comme nom de d'objet
                SearchPluginConfiguration searchObjectConfiguration = this.fetchPluginSearchConfiguration(plugin.getId());
                if (null != searchObjectConfiguration) {
                    SearchPluginConfiguration defaultPluginConfiguration = null;
                    if (searchObjectConfiguration.isAutoscan()) {
                        defaultPluginConfiguration = scanDefaultPluginConfiguration(searchObjectConfiguration.getType(), plugin.getId());
                    }
                    //Merge de la configuration spring avec la configuration par défaut
                    this.mergeOverridedConfiguration(searchObjectConfiguration, defaultPluginConfiguration, objectName);
                    //Merge de la configuration du plugin avec la configuration de la fiche
                    this.mergeOverridedConfiguration(searchFicheConfiguration, searchObjectConfiguration, objectName);
                }
            }
        }
    }

    /**
     * Constitution de la configuration par défaut des fiches.
     * On fonctionne par introspection.
     * Par défaut la configuration suivante est valable pour tous les champs :
     * - indexable
     * - boost = 1
     * - highlight = true
     *<p>
     *     Gère les champs @User, @Label, @Rubrique (ajoute les préfixes de chemins attendus).
     *     Si un projet désire gérer de manière générique les recherches par introspection, il peut surcharger cette méthode.
     *</p>
     *
     * @param searchFicheConfiguration
     * Configuration de fiche (initialisée)
     * @param type
     * Bean serializable
     */
    protected void scanDefaultConfiguration(SearchFicheConfiguration searchFicheConfiguration, Class<?> type, String objectName) {
        Map<String, SearchFieldConfiguration> fieldEsBeanMap = new HashMap<>();
        Map<String, Map<String, SearchFieldConfiguration>> fieldAutoCompleteMap = new HashMap<>();
        // On ne gère pas l'ensemble des éléments d'une fiche
        for (Field field : FieldUtils.getAllFieldsList(type)) {
            // Tous les champs ne peuvent pas être automatiquement gérés (on ne traite que les String)
            if (!String.class.isAssignableFrom(field.getType()) || IGNORED_FIELDS.contains(field.getName())) {
                continue;
            }
            String fieldName;
            if (field.isAnnotationPresent(Label.class)) {
                fieldName = PREFIX_FICHE + FIELD_SEPARATOR + objectName + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "label_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(Rubrique.class)) {
                fieldName = PREFIX_FICHE + FIELD_SEPARATOR + objectName + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "rubrique_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(User.class)) {
                fieldName = PREFIX_FICHE + FIELD_SEPARATOR + objectName + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_name";
                String fieldName2 = PREFIX_FICHE + FIELD_SEPARATOR + objectName + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_first_name";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
                fieldEsBeanMap.put(fieldName2, new SearchFieldConfiguration(fieldName2));
            } else {
                fieldName = PREFIX_FICHE + FIELD_SEPARATOR + objectName + FIELD_SEPARATOR + field.getName();
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            }
            if (field.isAnnotationPresent(Autocompletion.class) && StringUtils.isNotBlank(fieldName)) {
                Autocompletion completionAnnotation = field.getAnnotation(Autocompletion.class);
                String completionFieldAddress = "completions." + completionAnnotation.name() + ".input." + field.getName() + IndexerUtil.INDEX_SEPARATEUR + IndexerUtil.INDEX_COMPLETION_SUFFIX;
                Map<String, SearchFieldConfiguration> autoCompleteMap = fieldAutoCompleteMap.get(completionAnnotation.name());
                if (autoCompleteMap == null) {
                    autoCompleteMap = new HashMap<>();
                    fieldAutoCompleteMap.put(completionAnnotation.name(), autoCompleteMap);
                }
                autoCompleteMap.put(completionFieldAddress, new SearchFieldConfiguration(completionFieldAddress));
            }
        }
        searchFicheConfiguration.setFields(fieldEsBeanMap);
        searchFicheConfiguration.setAutoCompleteFields(fieldAutoCompleteMap);
    }

    /**
     * Constitution de la configuration par défaut d'un plugin.
     * On fonctionne par introspection.
     * Par défaut la configuration suivante est valable pour tous les champs :
     * - indexable
     * - highlight = false
     * <p>
     *     Si un projet désire gérer de manière générique les recherches par introspection, il peut surcharger cette méthode.
     *</p>
     * @param type Classe à scanner
     * @param pluginName Nom du plugin (habituellement en minuscule)
     * @return la configuration de recherche de plugin de fiche
     */
    protected SearchPluginConfiguration scanDefaultPluginConfiguration(Class<?> type, String pluginName) {
        SearchPluginConfiguration searchConfiguration = new SearchPluginConfiguration();
        Map<String, SearchFieldConfiguration> fieldEsBeanMap = new HashMap<>();
        Map<String, Map<String, SearchFieldConfiguration>> fieldAutoCompleteMap = new HashMap<>();
        // On ne gère pas l'ensemble des éléments d'une fiche
        for (Field field : FieldUtils.getAllFieldsList(type)) {
            // Tous les champs ne peuvent pas être automatiquement gérés (on ne traite que les String)
            if (!String.class.isAssignableFrom(field.getType()) || IGNORED_FIELDS.contains(field.getName())) {
                continue;
            }
            String fieldName;
            if (field.isAnnotationPresent(Label.class)) {
                fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "label_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(Rubrique.class)) {
                fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "rubrique_value";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            } else if (field.isAnnotationPresent(User.class)) {
                fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_name";
                String fieldName2 = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName() + FIELD_SEPARATOR + "user_first_name";
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
                fieldEsBeanMap.put(fieldName2, new SearchFieldConfiguration(fieldName2));
            } else {
                fieldName = PREFIX_PLUGIN + FIELD_SEPARATOR + pluginName + FIELD_SEPARATOR + PREFIX_PLUGIN_ITEMS + FIELD_SEPARATOR + field.getName();
                fieldEsBeanMap.put(fieldName, new SearchFieldConfiguration(fieldName));
            }
            if (field.isAnnotationPresent(Autocompletion.class) && StringUtils.isNotBlank(fieldName)) {
                Autocompletion completionAnnotation = field.getAnnotation(Autocompletion.class);
                String completionFieldAddress = "completions." + completionAnnotation.name() + ".input." + field.getName() + IndexerUtil.INDEX_SEPARATEUR + IndexerUtil.INDEX_COMPLETION_SUFFIX;
                Map<String, SearchFieldConfiguration> autoCompleteMap = fieldAutoCompleteMap.get(completionAnnotation.name());
                if (autoCompleteMap == null) {
                    autoCompleteMap = new HashMap<>();
                    fieldAutoCompleteMap.put(completionAnnotation.name(), autoCompleteMap);
                }
                autoCompleteMap.put(completionFieldAddress, new SearchFieldConfiguration(completionFieldAddress));
            }
            // L'annotation @Fiche n'est pas gérée automatiquement
        }
        searchConfiguration.setFields(fieldEsBeanMap);
        searchConfiguration.setAutoCompleteFields(fieldAutoCompleteMap);
        return searchConfiguration;
    }

    /**
     * Récupérer la configuration de recherche d'une fiche à partir un nom d'objet.
     * @param objectName
     * Nom d'objet
     * @return la configuration de recherche correspondant au nom d'objet
     */
    protected SearchFicheConfiguration fetchSearchConfiguration(String objectName) {
        Map<String, SearchFicheConfiguration> contextSearchConfigurationMap = ApplicationContextManager.getAllBeansOfType(SearchFicheConfiguration.class);
        for (SearchFicheConfiguration searchFicheConfiguration : contextSearchConfigurationMap.values()) {
            for (SearchIndexConfiguration searchIndexConfiguration : searchFicheConfiguration.getIndex()) {
                if (objectName.equals(searchIndexConfiguration.getObjectName())) {
                    return searchFicheConfiguration;
                }
            }
        }
        return null;
    }

    protected SearchFicheConfiguration fetchDefaultSearchConfiguration() {
        SearchFicheConfiguration contextSearchConfigurationMap = ApplicationContextManager.getBean("core", "searchDefaultFicheConfiguration", SearchFicheConfiguration.class);
        if (null == contextSearchConfigurationMap) {
            throw new SearchException("La configuration de recherche par défaut n'est pas définie");
        }
        return contextSearchConfigurationMap;
    }

    /**
     * Récupérer la configuration de recherche de plugin à partir d'un nom de plugin.
     * @param pluginId
     *  ID du plugin
     * @return la configuration de plugin
     */
    protected SearchPluginConfiguration fetchPluginSearchConfiguration(String pluginId) {
        Map<String, SearchPluginConfiguration> contextSearchConfigurationMap = ApplicationContextManager.getAllBeansOfType(SearchPluginConfiguration.class);
        for (SearchPluginConfiguration searchConfiguration : contextSearchConfigurationMap.values()) {
            if (pluginId.equals(searchConfiguration.getId())) {
                return searchConfiguration;
            }
        }
        return null;
    }

    /**
     * Merge des configurations cible et défaut. La configuration cible est étendue de celle par défaut s'il n'y a pas de valeur définie.
     * @param searchConfiguration
     * Configuration de recherche cible (qui aura le merge de la configuration par défaut)
     * @param defaultSearchConfiguration
     * Configuration par défaut utilisée
     */
    protected void mergeOverridedConfiguration(SearchCommonConfiguration searchConfiguration, SearchCommonConfiguration defaultSearchConfiguration, String objectName) {
        // S'il n'y a pas de configuration par défaut, ce n'est pas nécessaire de merger
        if (null != defaultSearchConfiguration) {
            // Pas de recopie sur le boost, par défaut, il est à 1
            // Suppression des champs que l'on ne souhaite pas rechercher
            for (String excludedField : defaultSearchConfiguration.getExcludedFields()) {
                if (!searchConfiguration.getExcludedFields().contains(excludedField)) {
                    searchConfiguration.getExcludedFields().add(excludedField);
                }
            }
            //Merge des données propres à chaque field
            for (Map.Entry<String, SearchFieldConfiguration> entryField : defaultSearchConfiguration.getFields().entrySet()) {
                // Si la clé n'existe pas dans la configuration cible, alors on prend celle par défaut
                if (!searchConfiguration.getFields().containsKey(entryField.getKey())) {
                    searchConfiguration.getFields().put(entryField.getKey(), entryField.getValue());
                }
            }
            for (Map.Entry<String, Map<String, SearchFieldConfiguration>> completionEntryField : defaultSearchConfiguration.getAutoCompleteFields().entrySet()) {
                if (!searchConfiguration.getAutoCompleteFields().containsKey(completionEntryField.getKey())) {
                    searchConfiguration.getAutoCompleteFields().put(completionEntryField.getKey(), completionEntryField.getValue());
                } else {
                    Map<String, SearchFieldConfiguration> mapWildCard = searchConfiguration.getAutoCompleteFields().get(completionEntryField.getKey());
                    for (Map.Entry<String, SearchFieldConfiguration> entryField : completionEntryField.getValue().entrySet()) {
                        // Si la clé n'existe pas dans la configuration cible, alors on prend celle par défaut
                        if (!mapWildCard.containsKey(entryField.getKey())) {
                            mapWildCard.put(entryField.getKey(), entryField.getValue());
                        }
                    }
                }
            }
            //Ajout des filtres
            searchConfiguration.getFilters().addAll(defaultSearchConfiguration.getFilters());
        }
        // Suppression des fields exclus
        for (String excludedField : searchConfiguration.getExcludedFields()) {
            searchConfiguration.getFields().remove(QueryUtil.translateSearchName(excludedField, objectName));
            }
        }

    /**
     * Merge des configurations cible et défaut. La configuration cible est étendue de celle par défaut s'il n'y a pas de valeur définie.
     * @param searchFicheConfiguration
     * Configuration de recherche cible (qui aura le merge de la configuration par défaut)
     * @param defaultSearchFicheConfiguration
     * Configuration par défaut utilisée
     */
    protected void mergeOverridedConfiguration(SearchFicheConfiguration searchFicheConfiguration, SearchFicheConfiguration defaultSearchFicheConfiguration, String objectName) {
        this.mergeOverridedConfiguration((SearchCommonConfiguration) searchFicheConfiguration, (SearchCommonConfiguration) defaultSearchFicheConfiguration, objectName);
        // S'il n'y a pas de configuration par défaut, ce n'est pas nécessaire de merger
        if (null != defaultSearchFicheConfiguration) {
            for (SearchFilterConfiguration entryFilter : defaultSearchFicheConfiguration.getFilters()) {
                // Si la clé n'existe pas dans la configuration cible, alors on prend celle par défaut
                if (!searchFicheConfiguration.getFilters().contains(entryFilter)) {
                    searchFicheConfiguration.getFilters().add(entryFilter);
                }
            } 
        }
    }
}
