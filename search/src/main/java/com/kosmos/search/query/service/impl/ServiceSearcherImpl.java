package com.kosmos.search.query.service.impl;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.ElasticsearchException;
import org.elasticsearch.action.search.SearchAction;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.IndicesOptions;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.HasChildQueryBuilder;
import org.elasticsearch.index.query.MultiMatchQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.RegexpQueryBuilder;
import org.elasticsearch.index.query.support.QueryInnerHitBuilder;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.HasAggregations;
import org.elasticsearch.search.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.search.exception.SearchException;
import com.kosmos.search.index.service.fiche.impl.ServiceAccessControlFiche;
import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.SearchParameter;
import com.kosmos.search.query.bean.SearchParameterBean;
import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.bean.SortOption;
import com.kosmos.search.query.bean.aggregation.model.AggregationGroupModel;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.configuration.SearchAggregationChildrenConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationDateRangeConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationGroupeConfiguration;
import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.configuration.SearchFieldConfiguration;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.kosmos.search.query.configuration.SearchIndexConfiguration;
import com.kosmos.search.query.configuration.SearchObjectConfiguration;
import com.kosmos.search.query.service.ServiceSearcher;
import com.kosmos.search.query.utils.QueryParam;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.search.query.view.bean.SearchFilterModel;
import com.kosmos.search.query.view.bean.SearchMetadataModel;
import com.kosmos.search.utils.IndexerUtil;
import com.kportal.cms.objetspartages.Objetpartage;
import com.kportal.core.config.MessageHelper;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUtil;

/**
 * Service de recherche (Elasticsearch).
 * @author cpoisnel
 *
 */
public class ServiceSearcherImpl implements ServiceSearcher {

    private static final Logger LOG = LoggerFactory.getLogger(ServiceSearcherImpl.class);

    @Autowired
    private ServiceAccessControlFiche serviceAccessControlFiche;

    @Autowired
    private ServiceSearchConfiguration serviceSearchConfiguration;

    private Client client;

    private SearchParameter searchParameter;

    private final String QUERY_TERM_LIBELLE = "libelle";

    /**
     * Configuration de recherche des ressources (global pour l'ensemble des fiches).
     */
    private SearchObjectConfiguration ressourceConfiguration;

    /**
     * Constructeur par défaut.
     */
    public ServiceSearcherImpl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchResultBean search(final SearchOptions searchOptions) {
        if (null == client) {
            throw new SearchException("Client Elasticsearch indisponible");
        }
        // Création d'un bean local pour la recherche
        SearchOptions localSearchOptions = defineLocalSearchOptions(searchOptions);
        // Définition des types
        String[] types = new String[0];
        if (localSearchOptions.getTypesFiches() != null) {
            types = localSearchOptions.getTypesFiches().toArray(new String[localSearchOptions.getTypesFiches().size()]);
        }
        boolean defaultAggregation = CollectionUtils.isEmpty(searchOptions.getTypesFiches());
        SearchFicheConfiguration searchFicheConfiguration = this.serviceSearchConfiguration.fetchMergedFilteredConfiguration(defaultAggregation, types);
        // Builder de requête Elasticsearch
        final SearchRequestBuilder searchRequestBuilder = this.initSearchRequestBuilder();
        searchRequestBuilder.setSize(localSearchOptions.getLimit()).setFrom(localSearchOptions.getOffset());
        addBooleanQueryBuilder(searchRequestBuilder, searchFicheConfiguration, localSearchOptions);
        addSort(searchRequestBuilder, localSearchOptions);
        // Recherche sur les indices
        if (LOG.isDebugEnabled()) {
            LOG.debug("Requête ElasticSearch :\n{}", searchRequestBuilder);
        }
        return executeQuery(searchRequestBuilder, searchOptions, searchFicheConfiguration);
    }

    /**
     * Définition de paramètres supplémentaire pour les options de recherche.
     * Un nouveau bean de paramètre est créé afin que les modifications apportées par le {@SearchParameterBean} ne soit pas affichées en front.
     * @param searchOptions les search options de base
     * @return les search options avec les paramètres définis dans le bean de paramétrage
     */
    private SearchOptions defineLocalSearchOptions(final SearchOptions searchOptions) {
        // Duplication pour surcharge spécifique à la recherche
        SearchOptions localSearchOptions;
        try {
            localSearchOptions = (SearchOptions) BeanUtils.cloneBean(searchOptions);
        } catch (InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            throw new SearchException("Erreur durant la copie du bean d'option de la recherche", e);
        }
        // On prend le bean correspondant à la clé passée en paramètre
        SearchParameterBean searchBean = getSearchParameterBean(localSearchOptions.getSearchParameterBeanKey());
        if (searchBean == null) {
            // S'il n'existe pas, on prend celui par défaut
            searchBean = getSearchParameterBean(SearchParameterBean.DEFAULT_BEAN_KEY);
        }
        // Restriction au site local si option activé
        if (searchBean != null && searchBean.isRestreintSiteCourant()) {
            String[] rubriqueParam = {ContexteUtil.getContexteUniv().getInfosSite().getCodeRubrique()};
            localSearchOptions.addFilter(QueryParam.SITE.getParam(), rubriqueParam);
        }
        // Définition des aggrégations
        if (searchBean != null) {
            localSearchOptions.addFacets(searchBean.getAggregations());
            localSearchOptions.addExcludedFacets(searchBean.getExcludedAggregation());
        }
        // Définition des types
        List<String> typesFiches = localSearchOptions.getTypesFiches();
        // Récupération des types de fiche à requêter depuis le bean seulement si aucun type n'a été appelé dans les facettes
        if (searchBean != null && typesFiches == null) {
            typesFiches = new ArrayList<>();
            if (CollectionUtils.isNotEmpty(searchBean.getTypesFiche())) {
                typesFiches.addAll(searchBean.getTypesFiche());
            } else if (CollectionUtils.isNotEmpty(searchBean.getExcludedTypesFiche())) {
                //Récupération des types de fiches qui ne sont pas exclus de la recherche
                for (Objetpartage objetpartage : ReferentielObjets.getObjetsPartagesTries()) {
                    if (!searchBean.getExcludedTypesFiche().contains(objetpartage.getNomObjet())) {
                        typesFiches.add(objetpartage.getNomObjet());
                    }
                }
                //Récupération des indexes d'objets autres que des fiches
                List<SearchFicheConfiguration> searchObjectConfigurationList = QueryUtil.fetchExternalDocumentConfiguration();
                for(SearchFicheConfiguration searchObjectConfiguration:searchObjectConfigurationList){
                    for(SearchIndexConfiguration searchObjectIndexConfiguration:searchObjectConfiguration.getIndex()){
                        typesFiches.add(searchObjectIndexConfiguration.getObjectName());
                    }
                }
            }
            if (CollectionUtils.isNotEmpty(typesFiches)) {
                localSearchOptions.setTypesFiches(typesFiches);
            }
        }
        return localSearchOptions;
    }

    /**
     * Initialisation de la requête générale (query)
     * <p>
     *     Parcourt la liste des configurations de fiche et effectue :
     * </p>
     *     <ul>
     *         <li>Définition des index</li>
     *         <li>Ajout des champs recherchés (avec pondération</li>
     *         <li>Ajout des filtres de recherche (obligatoire - DSI / Etat fiche - ou non)</li>
     *     </ul>
     *
     * @param searchRequestBuilder
     * Requête Elasticsearch
     * @param searchFicheConfiguration
     * Configuration de fiche
     * @param searchOptions
     * Options de recherche
     */
    protected void addBooleanQueryBuilder(SearchRequestBuilder searchRequestBuilder, SearchFicheConfiguration searchFicheConfiguration, SearchOptions searchOptions) {
        final Set<String> indices = new HashSet<>();
        final Set<SearchFieldConfiguration> searchFieldConfigurations = new HashSet<>();
        // Parcours des configurations de fiche
        // Gestion de l'index
        for (SearchIndexConfiguration indexConfiguration : searchFicheConfiguration.getIndex()) {
            String indexName = this.buildIndexName(indexConfiguration, searchOptions);
            indices.add(indexName);
            if (Float.compare(1.0F, indexConfiguration.getBoost()) != 0) {
                searchRequestBuilder.addIndexBoost(indexName, indexConfiguration.getBoost());
            }
        }
        // Gestion de la recherche
        // Traitement requête
        final BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        if (!searchOptions.isAutocomplete()) {
            for (Map.Entry<String, SearchFieldConfiguration> field : searchFicheConfiguration.getFields().entrySet()) {
                // Gérer le cas même chemin, boost différent
                searchFieldConfigurations.add(field.getValue());
            }
            addAggregation(searchRequestBuilder, searchFicheConfiguration, searchOptions);
            for (SearchFieldConfiguration searchFieldConfiguration : searchFieldConfigurations) {
                if (searchFieldConfiguration.isHighLight()) {
                    searchRequestBuilder.addHighlightedField(searchFieldConfiguration.getFieldName());
                    searchRequestBuilder.addHighlightedField(searchFieldConfiguration.getFieldName() + ".raw");
                }
            }
            if (StringUtils.isNotEmpty(searchOptions.getQuery())) {
                boolQueryBuilder.should(constructQueryBuilderWithQuery(searchOptions, searchFieldConfigurations));
            } else {
                boolQueryBuilder.must(QueryBuilders.matchAllQuery());
                //Si pas de query alors on ordonne les résultats par date de modification décroissante
                if(searchOptions.getSort().size() == 0){
                    SortOption dateSort = (SortOption) ApplicationContextManager.getCoreContextBean("sortDateMajDesc");
                    searchOptions.addSort(dateSort);
                }
            }
            addChildQuery(boolQueryBuilder, searchFicheConfiguration, searchOptions);
        } else {
            if (StringUtils.isNotBlank(searchOptions.getAutoCompleteName())) {
                final Set<Map.Entry<String, SearchFieldConfiguration>> fieldsAutoComplete = searchFicheConfiguration.getAutoCompleteFields().get(searchOptions.getAutoCompleteName()).entrySet();
                Set<String> queryTerms = new HashSet<>();
                queryTerms.add(QUERY_TERM_LIBELLE);
                for (Map.Entry<String, SearchFieldConfiguration> field : fieldsAutoComplete) {
                    queryTerms.add(field.getValue().getFieldName());
                }
                MultiMatchQueryBuilder phraseQueryBuilder = new MultiMatchQueryBuilder(StringUtils.lowerCase(searchOptions.getQuery()),queryTerms.toArray(new String[queryTerms.size()]));
                phraseQueryBuilder.analyzer(QueryUtil.DEFAULT_SEARCH_ANALYZER + searchOptions.getLanguage() );
                phraseQueryBuilder.type(MultiMatchQueryBuilder.Type.PHRASE_PREFIX);
                phraseQueryBuilder.slop(this.searchParameter.getSlop());
                phraseQueryBuilder.minimumShouldMatch(this.searchParameter.getMinimumShouldMatch());
                phraseQueryBuilder.boost(this.searchParameter.getPhraseMatchBoost());
                boolQueryBuilder.should(phraseQueryBuilder);
            }
        }
        // Ajout des filtres
        boolQueryBuilder.filter(buildFilterQuery(searchOptions, searchFicheConfiguration));
        boolQueryBuilder.minimumShouldMatch("1");
        boolQueryBuilder.disableCoord(true);
        searchRequestBuilder.setIndices(indices.toArray(new String[indices.size()]));
        if (LOG.isDebugEnabled()) {
            LOG.debug("Execution de la requête sur l'index et type : /{}/{}/", StringUtils.join(indices.toArray(new String[indices.size()]), ","), IndexerUtil.TYPE_FICHE);
        }
        // Ajout de la query
        searchRequestBuilder.setQuery(boolQueryBuilder);
    }

    /**
     * Calcul du nom d'indice à partir de la configuration et des paramètres d'appels.
     * @param searchIndexConfiguration
     * Configuration de fiche
     * @param searchOptions
     * Options de recherche
     * @return le nom de l'index utilisé.
     */
    protected String buildIndexName(SearchIndexConfiguration searchIndexConfiguration, SearchOptions searchOptions) {
        return IndexerUtil.INDEX_FICHE + IndexerUtil.INDEX_SEPARATEUR + searchIndexConfiguration.getObjectName().toLowerCase() + IndexerUtil.INDEX_SEPARATEUR + searchOptions.getLanguage();
    }

    /**
     * Construction de la requête ElasticSeach lorsque qu'une query est passée en paramètre de recherche.
     * @param searchOptions : Options de recherche
     * @param  searchFieldConfigurations : Liste des configurations de champs
     * @return
     */
    protected QueryBuilder constructQueryBuilderWithQuery(SearchOptions searchOptions, Set<SearchFieldConfiguration> searchFieldConfigurations){
        final Set<String> queryTerms = new HashSet<>();
        final Set<String> ngramTerms = new HashSet<>();
        final Set<String> rawTerms = new HashSet<>();
        for (SearchFieldConfiguration searchFieldConfiguration : searchFieldConfigurations) {
            String fieldName = searchFieldConfiguration.getFieldName();
            String boost = StringUtils.EMPTY;
            if (Float.compare(1.0F, searchFieldConfiguration.getBoost()) != 0) {
                boost = "^" + searchFieldConfiguration.getBoost();
            }
            queryTerms.add(fieldName + boost);
            rawTerms.add(fieldName +".raw" + boost);
            ngramTerms.add(fieldName + ".ngram" + boost);
        }
        //Exact Query + Phrase Query
        MultiMatchQueryBuilder exactPhraseQueryBuilder = new MultiMatchQueryBuilder(searchOptions.getQuery(), rawTerms.toArray(new String[rawTerms.size()]));
        exactPhraseQueryBuilder.analyzer(QueryUtil.RAW_ANALYZER + searchOptions.getLanguage());
        exactPhraseQueryBuilder.type(MultiMatchQueryBuilder.Type.PHRASE);
        exactPhraseQueryBuilder.slop(this.searchParameter.getSlop());
        exactPhraseQueryBuilder.minimumShouldMatch(this.searchParameter.getMinimumShouldMatch());
        exactPhraseQueryBuilder.boost(this.searchParameter.getExactPhraseMatchBoost());
        //Exact Query
        MultiMatchQueryBuilder exactQueryBuilder = new MultiMatchQueryBuilder(searchOptions.getQuery(), rawTerms.toArray(new String[rawTerms.size()]));
        exactQueryBuilder.analyzer(QueryUtil.RAW_ANALYZER + searchOptions.getLanguage());
        exactQueryBuilder.boost(this.searchParameter.getExactMatchBoost());
        //Prefix-Phrase Query
        MultiMatchQueryBuilder phraseQueryBuilder = new MultiMatchQueryBuilder(searchOptions.getQuery(), queryTerms.toArray(new String[queryTerms.size()]));
        phraseQueryBuilder.analyzer(QueryUtil.DEFAULT_SEARCH_ANALYZER + searchOptions.getLanguage());
        phraseQueryBuilder.type(MultiMatchQueryBuilder.Type.PHRASE_PREFIX);
        phraseQueryBuilder.slop(this.searchParameter.getSlop());
        phraseQueryBuilder.minimumShouldMatch(this.searchParameter.getMinimumShouldMatch());
        phraseQueryBuilder.boost(this.searchParameter.getPhraseMatchBoost());
        //MultiMatch Query
        MultiMatchQueryBuilder multiMatchQueryBuilder = new MultiMatchQueryBuilder(searchOptions.getQuery(), ngramTerms.toArray(new String[ngramTerms.size()]));
        multiMatchQueryBuilder.analyzer(QueryUtil.NGRAM_ANALYZER + searchOptions.getLanguage());
        phraseQueryBuilder.minimumShouldMatch(this.searchParameter.getMinimumShouldMatch());
        multiMatchQueryBuilder.boost(this.searchParameter.getMultiMatchBoost());
        //Construction de la requête
        BoolQueryBuilder builder = new BoolQueryBuilder();
        builder.should(exactPhraseQueryBuilder);
        builder.should(exactQueryBuilder);
        builder.should(phraseQueryBuilder);
        builder.should(multiMatchQueryBuilder);
        return builder;
    }

    /**
     * Ajout de la requête interne pour les resssources.
     * @param boolQueryBuilder
     * Builder de requête (global)
     * @param searchFicheConfiguration
     * Configuration de recherche de fiche
     * @param searchOptions
     * Options de recherche
     */
    protected void addChildQuery(final BoolQueryBuilder boolQueryBuilder, SearchFicheConfiguration searchFicheConfiguration, SearchOptions searchOptions) {
        // Traitement des ressources
        if (null != ressourceConfiguration && StringUtils.isNotEmpty(searchOptions.getQuery())) {
            final Set<String> childFieldNames = new HashSet<>();
            for (Map.Entry<String, SearchFieldConfiguration> searchFieldBeanEntry : ressourceConfiguration.getFields().entrySet()) {
                SearchFieldConfiguration searchFieldConfiguration = searchFieldBeanEntry.getValue();
                String fieldName = searchFieldBeanEntry.getKey();
                final float boost = searchFieldConfiguration.getBoost();
                if (boost > 1.0F) {
                    fieldName += "^" + boost;
                }
                childFieldNames.add(fieldName);
            }
            final MultiMatchQueryBuilder childQueryBuilder = QueryBuilders.multiMatchQuery(searchOptions.getQuery(), childFieldNames.toArray(new String[childFieldNames.size()]));
            childQueryBuilder.analyzer(QueryUtil.DEFAULT_SEARCH_ANALYZER + searchOptions.getLanguage());
            final HasChildQueryBuilder childQuery = new HasChildQueryBuilder(IndexerUtil.TYPE_RESSOURCE, childQueryBuilder);
            childQuery.scoreMode(this.searchParameter.getScoreMode());
            childQuery.boost(this.searchParameter.getChildRessourceBoost());
            final QueryInnerHitBuilder innerHit = new QueryInnerHitBuilder();
            innerHit.setNoFields();
            final HighlightBuilder chb = innerHit.highlightBuilder();
            for (String field : ressourceConfiguration.getFields().keySet()) {
                chb.field(field, 100, 1);
                chb.order("score");
                chb.preTags("<mark>");
                chb.postTags("</mark>");
                chb.encoder("html");
            }
            childQuery.innerHit(innerHit);
            boolQueryBuilder.should(childQuery);
        }
    }

    /**
     * Ajout des aggrégations à la requête.
     * Si des aggregations sont
     * @param searchRequestBuilder
     * Builder de requête Elasticsearch
     * @param searchFicheConfiguration
     * Configuration
     */
    protected void addAggregation(final SearchRequestBuilder searchRequestBuilder, final SearchFicheConfiguration searchFicheConfiguration, final SearchOptions searchOptions) {
        for (SearchFilterConfiguration filterConfiguration : searchFicheConfiguration.getFilters()) {
            if (SearchAggregationConfiguration.class.isAssignableFrom(filterConfiguration.getClass())) {
                SearchAggregationConfiguration aggregationConfiguration = (SearchAggregationConfiguration) filterConfiguration;
                if (null != aggregationConfiguration.getAggregationBuilder()) {
                    if ((CollectionUtils.isEmpty(searchOptions.getFacets()) || searchOptions.getFacets().contains(filterConfiguration.getName())) && (CollectionUtils.isEmpty(searchOptions.getExcludedFacets()) || !searchOptions.getExcludedFacets().contains(filterConfiguration.getName()))) {
                        searchRequestBuilder.addAggregation(aggregationConfiguration.getAggregationBuilder().build(aggregationConfiguration));
                    }
                }
            }
        }
    }

    /**
     * Exécution de la requête Elasticsearch sur le client.
     * @param searchRequestBuilder
     *   Builder de requête Elasticsearch
     * @param searchOptions
     *   Options de recherche
     * @return Un bean de resultat constitué.
     */
    private SearchResultBean executeQuery(SearchRequestBuilder searchRequestBuilder, SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        SearchResultBean searchResultBean = null;
        try {
            final SearchResponse response = searchRequestBuilder.get();
            LOG.debug("La recherche a retourné {} résultats en {} ms", response.getHits().getTotalHits(), response.getTookInMillis());
            searchResultBean = new SearchResultBean();
            searchResultBean.setSearchMetadataModel(buildSearchMetadataModel(response, searchOptions, searchFicheConfiguration));
            searchResultBean.setAggregations(buildAggregationModel(response.getAggregations(), searchFicheConfiguration, searchOptions));
            searchResultBean.setDocs(QueryUtil.convertToDocs(response.getHits()));
        } catch (ElasticsearchException ee) {
            LOG.warn("Exception lors de la recherche ElasticSearch", ee);
        } catch (final Exception e) {
            throw new SearchException("Exception lors de la requête Elasticsearch avec les options", e);
        }
        return searchResultBean;
    }

    /**
     * Construction de la listes des filtres et aggrégations à afficher.
     * @param responseAggregations : aggregation retournée par la recherche
     */
    private Collection<AggregationModel> buildAggregationModel(Aggregations responseAggregations, SearchFicheConfiguration searchFicheConfiguration, SearchOptions searchOptions) {
        Collection<AggregationModel> aggregationModels = new ArrayList<>();
        if (responseAggregations == null || CollectionUtils.isEmpty(responseAggregations.asList())) {
            return aggregationModels;
        }
        Map<String, AggregationModel> responseAggregationModelMap = processAllAggregation(searchFicheConfiguration, responseAggregations, searchOptions);
        for (SearchFilterConfiguration filterConfiguration : searchFicheConfiguration.getFilters()) {
            if (!SearchAggregationConfiguration.class.isAssignableFrom(filterConfiguration.getClass())) {
                break;
            }
            SearchAggregationConfiguration aggregationConfiguration = (SearchAggregationConfiguration) filterConfiguration;
            if (responseAggregations.asMap().containsKey(filterConfiguration.getName())) {
                //Une agrégation retournée par elasticsearch existe pour ce filtre. On la construit.
                if (!aggregationInGroup(aggregationConfiguration, searchFicheConfiguration)) {
                    Aggregation aggregation = responseAggregations.get(aggregationConfiguration.getName());
                    CollectionUtils.addIgnoreNull(aggregationModels, responseAggregationModelMap.get(aggregation.getName()));
                }
            } else if (SearchAggregationGroupeConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())) {
                AggregationGroupModel groupModel = buildGroupAggregation(aggregationConfiguration, responseAggregationModelMap);
                if (groupModel.iterator().hasNext()) {
                    CollectionUtils.addIgnoreNull(aggregationModels, groupModel);
                }
            }
        }
        return aggregationModels;
    }

    /**
     * Retourne le bean correspondant à la clé si aucun bean, renvoie null.
     * @param key la clé du bean de recherche
     * @return le bean correspondant
     */
    private SearchParameterBean getSearchParameterBean(String key) {
        if (StringUtils.isEmpty(key)) {
            return null;
        }
        Map<String, SearchParameterBean> beans = ApplicationContextManager.getAllBeansOfType(SearchParameterBean.class);
        SearchParameterBean bean = null;
        for (Map.Entry<String, SearchParameterBean> searchBeanEntry : beans.entrySet()) {
            if (key.equals(searchBeanEntry.getValue().getKey())) {
                bean = searchBeanEntry.getValue();
            }
        }
        return bean;
    }

    /**
     * Constitution du bean de metadata de la recherche à retourner à la vue
     * @param response
     * @param searchOptions
     * @param searchFicheConfiguration
     * @return
     */
    private SearchMetadataModel buildSearchMetadataModel(final SearchResponse response, final SearchOptions searchOptions, final SearchFicheConfiguration searchFicheConfiguration) {
        SearchMetadataModel searchMetadataModel = new SearchMetadataModel();
        searchMetadataModel.setQuery(searchOptions.getQuery());
        searchMetadataModel.setStatus(response.status().getStatus());
        searchMetadataModel.setTookInMillis(response.getTookInMillis());
        searchMetadataModel.setTotal(response.getHits().getTotalHits());
        searchMetadataModel.setFiltersMap(buildFiltersModelMap(searchOptions, searchFicheConfiguration));
        searchMetadataModel.setPage(searchOptions.getPage());
        searchMetadataModel.setLimit(searchOptions.getLimit());
        searchMetadataModel.setUrl(QueryUtil.buildQueryUrlFromSearchOption(searchOptions));
        searchMetadataModel.setMaxAggregationSize(this.searchParameter.getMaxAggregationSize());
        searchMetadataModel.setLangue(String.valueOf(LangueUtil.getIndiceFromCodeISO(searchOptions.getLanguage())));
        searchMetadataModel.setSearchParameterBeanKey(searchOptions.getSearchParameterBeanKey());
        searchMetadataModel.setHightLightFragmentNumber(searchParameter.getHightLightFragmentNumber());
        return searchMetadataModel;
    }

    private Map<String, List<SearchFilterModel>> buildFiltersModelMap(final SearchOptions searchOptions, final SearchFicheConfiguration searchFicheConfiguration) {
        Map<String, List<SearchFilterModel>> filtersMap = new HashMap<>();

        for (String filterName : searchOptions.getFilters().keySet()) {
            SearchFilterConfiguration filter = getFilterByName(searchFicheConfiguration.getFilters(), filterName);
            if(filter != null){
                SearchAggregationConfiguration aggregationConfiguration = QueryUtil.getAggregationConfiguration(searchFicheConfiguration, filter.getName());
                List<SearchFilterModel> filtersList = new ArrayList<>();
                if (null != aggregationConfiguration) {
                    for (String filterSelected : searchOptions.getFilters().get(filterName)) {
                        filtersList.add(new SearchFilterModel(filterName, filterSelected, getFilterLibelleFromAggregation(aggregationConfiguration, filterSelected)));
                    }
                }
                filtersMap.put(filter.getName(), filtersList);
            }
        }

        return filtersMap;
    }

    /**
     * Récupération du libellé du filtre depuis l'agrégation passée en paramètre.
     * @param aggregationConfiguration l'aggrégation
     * @param filterSelected le filtre sélectionné
     * @return le nom du filtre
     */
    private String getFilterLibelleFromAggregation(final SearchAggregationConfiguration aggregationConfiguration, final String filterSelected){
        String filterLibelle = StringUtils.EMPTY;

        // Si c'est une agrégation de type children, on passe en mode récursif sur les enfants pour avoir le libellé
        if(SearchAggregationChildrenConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())){
            SearchAggregationChildrenConfiguration searchAggregationChildrenConfiguration = (SearchAggregationChildrenConfiguration)aggregationConfiguration;
            Set<SearchAggregationConfiguration> aggregationConfigurations = searchAggregationChildrenConfiguration.getSubAggregations();
            for(SearchAggregationConfiguration searchAggregationConfiguration : aggregationConfigurations){
                filterLibelle = getFilterLibelleFromAggregation(searchAggregationConfiguration, filterSelected);
            }

        }

        // Gestion des différents cas de type d'agrégation simple pour récupérer le libellé
        if (StringUtils.isEmpty(filterLibelle) && SearchAggregationTermConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())) {
            SearchAggregationTermConfiguration termConfiguration = (SearchAggregationTermConfiguration) aggregationConfiguration;
            filterLibelle = termConfiguration.getBucketLabelValue(filterSelected);
        } else if (StringUtils.isEmpty(filterLibelle) && SearchAggregationDateRangeConfiguration.class.isAssignableFrom(aggregationConfiguration.getClass())) {
            SearchAggregationDateRangeConfiguration dateRangeConfiguration = (SearchAggregationDateRangeConfiguration) aggregationConfiguration;
            filterLibelle = dateRangeConfiguration.getBucketLabelValue(filterSelected);
        }
        if (StringUtils.isBlank(filterLibelle)) {
            filterLibelle = filterSelected;
        }

        return filterLibelle;
    }

    /**
     * Initialise la requête Elasticsearch.
     * @return le builder de requête initialisée
     * On valorise les paramètres spécifiques à l'API
     */
    protected SearchRequestBuilder initSearchRequestBuilder() {
        SearchRequestBuilder searchRequestBuilder = new SearchRequestBuilder(client, SearchAction.INSTANCE);
        searchRequestBuilder.setHighlighterPreTags(this.searchParameter.getHightLightPreTag());
        searchRequestBuilder.setHighlighterPostTags(this.searchParameter.getHightLightPostTag());
        searchRequestBuilder.setHighlighterOrder(this.searchParameter.getHightLightOrder());
        searchRequestBuilder.setHighlighterEncoder(this.searchParameter.getHightLightEncoder());
        searchRequestBuilder.setHighlighterFragmentSize(this.searchParameter.getHightLightFragmentSize());
        searchRequestBuilder.setTimeout(this.searchParameter.getTimeOut());
        searchRequestBuilder.setTypes(IndexerUtil.TYPE_FICHE);
        searchRequestBuilder.setIndicesOptions(IndicesOptions.fromOptions(true, true, true, false));
        searchRequestBuilder.setMinScore(this.searchParameter.getMinScore());
        return searchRequestBuilder;
    }

    /**
     * Initialisation de la requête de filtres sur la requête.
     * @param searchOptions
     * Options de recherche
     * @param searchFicheConfiguration
     * Configuration de fiche
     * @return la requête de filtres (les résultats filtrés seront non affichées, et ne rentreront pas en compte dans la définition des scores)
     */
    protected BoolQueryBuilder buildFilterQuery(SearchOptions searchOptions, SearchFicheConfiguration searchFicheConfiguration) {
        final BoolQueryBuilder filterQueryBuilder = new BoolQueryBuilder();
        addFilterAccessControl(filterQueryBuilder, searchOptions);
        addFilterConfiguration(filterQueryBuilder, searchOptions, searchFicheConfiguration);
        addFilterState(filterQueryBuilder, searchOptions, searchFicheConfiguration);
        return filterQueryBuilder;
    }

    /**
     * Filtrer sur les états des objets en ligne.
     * @param filterQueryBuilder
     *  Builder de requête de filtre
     * @param searchOptions
     * Options de recherche
     */
    protected void addFilterState(final BoolQueryBuilder filterQueryBuilder, final SearchOptions searchOptions, final SearchFicheConfiguration searchFicheConfiguration) {
        final BoolQueryBuilder filterEtatObjet = new BoolQueryBuilder();
        List<String> etatObjetFiche = new ArrayList<>();
        for (SearchIndexConfiguration indexConfiguration : searchFicheConfiguration.getIndex()) {
            etatObjetFiche.add(String.format("fiche.%s.etatObjet", indexConfiguration.getObjectName()));
        }
        if (CollectionUtils.isNotEmpty(etatObjetFiche)) {
            // On parcourt tous les états fiche pour les ajouter à la query - impact 2.4.5
            for (EtatFiche etatFiche : searchOptions.getEtatFicheList()) {
                filterEtatObjet.should(QueryBuilders.multiMatchQuery(etatFiche, etatObjetFiche.toArray(new String[etatObjetFiche.size()])));
            }
            BoolQueryBuilder missingEtat = new BoolQueryBuilder();
            for (String etatObjet : etatObjetFiche) {
                missingEtat.mustNot(QueryBuilders.existsQuery(etatObjet));
            }
            filterEtatObjet.should(missingEtat);
            filterEtatObjet.minimumShouldMatch("1");
            filterQueryBuilder.must(filterEtatObjet);
        }
    }

    /**
     * Ajout des filtres pour chaque configuration (voir {@link SearchFicheConfiguration#getFilters()}).
     * @param filterQueryBuilder
     * Builder de requête de filtre
     * @param searchOptions
     * Options de recherche
     * @param searchFicheConfiguration
     * Configuration des fiches
     */
    protected void addFilterConfiguration(BoolQueryBuilder filterQueryBuilder, final SearchOptions searchOptions, final SearchFicheConfiguration searchFicheConfiguration) {
        // Gestion des filtres
        for (String filterName : searchOptions.getFilters().keySet()) {
            SearchFilterConfiguration filter = getFilterByName(searchFicheConfiguration.getFilters(), filterName);
            if (null != filter && null != filter.getQueryBuilder()) {
                QueryBuilder queryBuilder = filter.getQueryBuilder().build(filter, searchOptions.getFilters().get(filter.getName()));
                if (null != queryBuilder) {
                    filterQueryBuilder.must(queryBuilder);
                }
            } else if (null != filter && filter instanceof SearchAggregationChildrenConfiguration) {
                SearchAggregationChildrenConfiguration parentFilter = (SearchAggregationChildrenConfiguration) filter;
                Set<SearchAggregationConfiguration> subAggregation = parentFilter.getSubAggregations();
                for (SearchAggregationConfiguration childrenFilter : subAggregation) {
                    if (childrenFilter.getQueryBuilder() != null) {
                        QueryBuilder queryBuilder = childrenFilter.getQueryBuilder().build(childrenFilter, searchOptions.getFilters().get(childrenFilter.getName()));
                        HasChildQueryBuilder hasChildQueryBuilder = new HasChildQueryBuilder(parentFilter.getChildType(), queryBuilder);
                        filterQueryBuilder.must(hasChildQueryBuilder);
                    }
                }
            }
        }
    }

    /**
     * Recherche dans une liste le filtre correspondant à un nom. Recherche aussi dans les enfants des filtres de type {@link SearchAggregationChildrenConfiguration}
     * @param filters un set de filtres
     * @param filterName le nom du filtre à trouver
     * @return le filtre trouvé ou null
     */
    private SearchFilterConfiguration getFilterByName(Set<SearchFilterConfiguration> filters, String filterName) {
        if (StringUtils.isEmpty(filterName) && filters != null) {
            return null;
        }
        // Initialisation du retour
        SearchFilterConfiguration outFilter = null;
        // Boucle sur tous les fitres
        for (SearchFilterConfiguration filter : filters) {
            // Si pas de filtre trouvé et que le nom correspond, on définit le filtre de sortie
            if (outFilter == null && filter.getName().equals(filterName)) {
                outFilter = filter;
            }
            // Si pas de filtre trouvé et qu'il est de type SearchAggregationChildrenConfiguration, on regarde ses enfants
            if (outFilter == null && filter instanceof SearchAggregationChildrenConfiguration) {
                outFilter = getFilterByName(((SearchAggregationChildrenConfiguration) filter).getSubAggregations(), filterName);
                // Si le nom d'un des enfants correspond on renvoie le parent
                if (outFilter != null) {
                    outFilter = filter;
                }
            }
        }
        return outFilter;
    }

    /**
     * Méthode d'ajout des contrôles d'accès à la requête de recherche.
     * <p>
     *     Attention, il s'agit d'une méthode permettant de gérer la DSI. La surcharge de cette méthode peut empêcher d'avoir accès aux résultats ou au contraire rendre visible des résultats à l'ensemble des utilisateurs.
     * </p>
     * @param filterQueryBuilder
     * Builder de requête de filtre
     * @param searchOptions
     * Options de recherche
     */
    protected void addFilterAccessControl(final BoolQueryBuilder filterQueryBuilder, final SearchOptions searchOptions) {
        // Ajout du filtre sur les droits
        final Set<String> groupesAutorises = searchOptions.getAuthorizedGroups();
        final Collection<String> idGroupesAutorises = new ArrayList<>(groupesAutorises.size());
        for (final String groupe : groupesAutorises) {
            idGroupesAutorises.add(this.serviceAccessControlFiche.genererIdGroupeDsiES(groupe));
        }
        //Gestion de la restriction sur les rubriques.
        // Si l'utilisateur est dans un groupe il doit pouvoir voir les fiches sans restriction de rubrique + les fiches visibles par le groupe.
        final BoolQueryBuilder accessControlRubriqueQuery = new BoolQueryBuilder();
        accessControlRubriqueQuery.should(QueryBuilders.boolQuery().mustNot(QueryBuilders.existsQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesRubrique")));
        final String[] idGroupesAutorisesArray = idGroupesAutorises.toArray(new String[idGroupesAutorises.size()]);
        if (idGroupesAutorisesArray.length > 0) {
            final RegexpQueryBuilder accessControlRegexQueryBuilder = new RegexpQueryBuilder(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesRubrique", processAccessControlRegexGroups(idGroupesAutorisesArray));
            accessControlRubriqueQuery.should(accessControlRegexQueryBuilder);
        }
        accessControlRubriqueQuery.minimumShouldMatch("1");
        filterQueryBuilder.must(accessControlRubriqueQuery);
        // Filtrage sur les fiches
        final BoolQueryBuilder accessControlQuery = new BoolQueryBuilder();
        accessControlQuery.should(QueryBuilders.boolQuery().mustNot(QueryBuilders.termQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesFicheRestriction", "2")));
        accessControlQuery.should(QueryBuilders.boolQuery().must(QueryBuilders.termQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesFicheRestriction", "2")).must(QueryBuilders.termsQuery(IndexerUtil.FIELD_ACCESSCONTROL + ".groupesFiche", idGroupesAutorisesArray)));
        accessControlQuery.minimumShouldMatch("1");
        filterQueryBuilder.must(accessControlQuery);
    }

    /**
     *
     * Récupération de l'expression régulière correspondant aux groupes autorisés à afficher les documents de la requête.
     *
     * <pre>
     *
     * La grammaire :
     *     - les codes de groupes ne peuvent avoir que des caractères hexadécimaux compris entre a et f 0 et 9 (donc à adapter au vrai cas). La grammaires est réduite à a-f0-9[](), ceci permettant d'accélérer les recherches.
     *     - les codes de groupes sont délimités par un crochet ouvrant et un crochet fermant
     *     - les parenthèses sont utilisés pour délimiter les unions de groupes (dans notre cas les groupes contenus dans la diffusion d'une rubrique)
     *     - les expressions parenthésées qui se suivent forment des intersections d'expression soit dans notre cas des intersections d'unions de groupes
     *     - exemple : ([A])([A][AB][C][D])([AB][D])([ABC])
     *     -- traduction : A et (A ou AB ou C ou D) et (AB ou D) et ABC
     *     -- traduction fonctionnelle : (Codes groupes de diffusion rubrique 1)(Codes groupes de diffusion rubrique 2 mère de rubrique 1)(Codes groupes de diffusion rubrique 3 mère de rubrique 2)(Codes groupes de diffusion rubrique 4 mère de rubrique 3)
     *     La régex de validation :
     *     ^(\((\[[A-Z]+\])*(\[A\]|\[AB\])+(\[[A-Z]+\])*\))+$
     * </pre>
     *
     * @param authorizedGroups
     * IDs des Groupes autorisés (ID techique ou md5 si le groupe n'est pas présent)
     * @return expression régulière à valider
     */
    protected String processAccessControlRegexGroups(final String[] authorizedGroups) {
        final StringBuilder regexBuilder = new StringBuilder();
        //^ Ex : (\((\[[a-f0-9]+\])*(\[1\]|\[2\])+(\[[a-f0-9]+\])*\))+$
        regexBuilder.append("(\\((\\[[a-f0-9]+\\])*");
        int length = authorizedGroups.length;
        if (length > 0) {
            regexBuilder.append("(");
            for (int i = 0; i < length; i++) {
                regexBuilder.append(String.format("\\[%s\\]", authorizedGroups[i]));
                if (i != (length - 1)) {
                    regexBuilder.append("|");
                }
            }
            regexBuilder.append(")+");
        }
        regexBuilder.append("(\\[[a-f0-9]+\\])*\\))+");
        return regexBuilder.toString();
    }

    /**
     * Ajout du tri sur la requête Elasticsearch.
     * @param searchRequestBuilder
     * Requête Elasticsearch
     * @param searchOptions
     * Options de recherche
     */
    protected void addSort(final SearchRequestBuilder searchRequestBuilder, final SearchOptions searchOptions) {
        // Ajout de l'ordre
        Set<SortOption> sortOptions = searchOptions.getSort();
        if (CollectionUtils.isNotEmpty(sortOptions)) {
            for (final SortOption sortOption : sortOptions) {
                final SortBuilder sortBuilder = new FieldSortBuilder(sortOption.getField());
                SortOrder sortOrder = SortOrder.ASC;
                if (sortOption.getOrder() == SortOption.SortOrderEnum.DESC) {
                    sortOrder = SortOrder.DESC;
                }
                sortBuilder.order(sortOrder);
                searchRequestBuilder.addSort(sortBuilder);
            }
        }
    }

    /**
     * Vérification de la présence de l'agrégation dans un groupe.
     * @param aggregationConfiguration : configuration de l'agrégation
     * @param searchFicheConfiguration : configuration globale de la recherche
     * @return
     */
    private static boolean aggregationInGroup(SearchAggregationConfiguration aggregationConfiguration, SearchFicheConfiguration searchFicheConfiguration) {
        for (SearchFilterConfiguration filterConfiguration : searchFicheConfiguration.getFilters()) {
            if (SearchAggregationGroupeConfiguration.class.isAssignableFrom(filterConfiguration.getClass())) {
                SearchAggregationGroupeConfiguration aggregationGroupConfiguration = (SearchAggregationGroupeConfiguration) filterConfiguration;
                Set<SearchFilterConfiguration> childAggregationSet = aggregationGroupConfiguration.getChildAggregation();
                for (SearchFilterConfiguration childAggregation : childAggregationSet) {
                    if (childAggregation.getName().equals(aggregationConfiguration.getName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Construction d'un groupement d'agrégation
     * On récupère toutes les agrégations participant à ce groupe
     * @param aggregationConfiguration : configuration du groupement
     * @param mapAggregation : map complète des agrégation retournées par la recherche
     * @return
     */
    private static AggregationGroupModel buildGroupAggregation(SearchAggregationConfiguration aggregationConfiguration, Map<String, AggregationModel> mapAggregation) {
        SearchAggregationGroupeConfiguration groupeConfiguration = (SearchAggregationGroupeConfiguration) aggregationConfiguration;
        AggregationGroupModel groupeModel = new AggregationGroupModel(groupeConfiguration.getName());
        groupeModel.setLabel(MessageHelper.getMessage(groupeConfiguration.getIdExtension(), groupeConfiguration.getLabel()));
        for (SearchFilterConfiguration childConfiguration : (Set<SearchFilterConfiguration>) groupeConfiguration.getChildAggregation()) {
            AggregationModel childModel = mapAggregation.get(childConfiguration.getName());
            if (childModel != null) {
                groupeModel.addChild(childModel);
            }
        }
        return groupeModel;
    }

    /**
     * Construction du model de chaque agrégation
     * @param searchFicheConfiguration : configuration globale de la recherche
     * @param aggregations : agrégations retournées par la recherche
     * @param searchOptions : options de recherche
     * @return
     */
    private static Map<String, AggregationModel> processAllAggregation(SearchFicheConfiguration searchFicheConfiguration, Aggregations aggregations, SearchOptions searchOptions) {
        Map<String, AggregationModel> mapAggregationModel = new HashedMap<>();
        for (Aggregation aggregation : aggregations.asList()) {
            if (HasAggregations.class.isAssignableFrom(aggregation.getClass())) {
                for (Aggregation itemAggregation : ((HasAggregations) aggregation).getAggregations()) {
                    SearchAggregationConfiguration searchAggregationConfiguration = QueryUtil.getAggregationConfiguration(searchFicheConfiguration, itemAggregation.getName());
                    if (searchAggregationConfiguration != null) {
                        AggregationModel aggregationModel = searchAggregationConfiguration.getAggregationModelBuilder().buildModel(searchAggregationConfiguration, itemAggregation, searchOptions, searchFicheConfiguration);
                        if (aggregationModel != null) {
                            mapAggregationModel.put(aggregation.getName(), aggregationModel);
                        }
                    }
                }
            } else {
                SearchAggregationConfiguration searchAggregationConfiguration = QueryUtil.getAggregationConfiguration(searchFicheConfiguration, aggregation.getName());
                if (searchAggregationConfiguration != null) {
                    AggregationModel aggregationModel = searchAggregationConfiguration.getAggregationModelBuilder().buildModel(searchAggregationConfiguration, aggregation, searchOptions, searchFicheConfiguration);
                    if (aggregationModel != null) {
                        mapAggregationModel.put(aggregation.getName(), aggregationModel);
                    }
                }
            }
        }
        return mapAggregationModel;
    }

    public final Client getClient() {
        return client;
    }

    public final void setClient(final Client client) {
        this.client = client;
    }

    public SearchParameter getSearchParameter() {
        return searchParameter;
    }

    public void setSearchParameter(final SearchParameter searchParameter) {
        this.searchParameter = searchParameter;
    }

    public SearchObjectConfiguration getRessourceConfiguration() {
        return ressourceConfiguration;
    }

    public void setRessourceConfiguration(final SearchObjectConfiguration ressourceConfiguration) {
        this.ressourceConfiguration = ressourceConfiguration;
    }
}
