package com.kosmos.search.query.servlet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.TilesContainer;
import org.apache.tiles.access.TilesAccess;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.Request;
import org.apache.tiles.request.servlet.ServletRequest;
import org.apache.tiles.request.servlet.ServletUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.bean.SortOption;
import com.kosmos.search.query.service.ServiceSearcher;
import com.kosmos.search.query.utils.QueryParam;
import com.kportal.servlet.ExtensionServlet;
import com.univ.utils.ContexteUtil;

/**
 * Search servlet.
 * <p>
 *     La servlet de recherche effectue la recherche Elasticsearch et prépare la vue d'affichage.
 * </p>
 */
@WebServlet(name = "search", urlPatterns = "/servlet/search", loadOnStartup = 0)
public class SearchServlet extends ExtensionServlet {

    private static final Logger LOG = LoggerFactory.getLogger(SearchServlet.class);

    @Autowired
    private ServiceSearcher serviceSearcher;

    /**
     *
     * {@inheritDoc}
     * <p>
     *     Bean's autowiring.
     * </p>
     */
    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());
    }

    protected SearchOptions processRequest(final HttpServletRequest request) {
        SearchOptions searchOptions = new SearchOptions();
        int page = 0;
        int pageLimit = 0;
        // Parcours des propriétés
        Map<String, String[]> parameterMap = request.getParameterMap();
        for (Map.Entry<String, String[]> parameterEntry : request.getParameterMap().entrySet()) {
            String parameter = parameterEntry.getKey();
            String[] values = parameterEntry.getValue();
            if (QueryParam.QUERY.getParam().equals(parameter)) {
                searchOptions.setQuery(getFirst(values));
            } else if (QueryParam.BEAN_KEY.getParam().equals(parameter)) {
                searchOptions.setSearchParameterBeanKey(getFirst(values));
            } else if (QueryParam.SORT.getParam().equals(parameter)) {
                fillSortOptions(searchOptions, Arrays.asList(values));
            } else if (QueryParam.LANGUAGE.getParam().equals(parameter)) {
                searchOptions.setLanguage(LangueUtil.getLocale(getFirst(values)).getLanguage());
            } else if (QueryParam.TYPES.getParam().equals(parameter)) {
                List<String> types = new ArrayList<>(Arrays.asList(values));
                searchOptions.setTypesFiches(types);
                searchOptions.addFilter(parameter, values);
            } else if (QueryParam.PAGE.getParam().equals(parameter)) {
                try {
                    page = StringUtils.isNotBlank(getFirst(values)) ? Integer.valueOf(getFirst(values)) : 0;
                } catch (NumberFormatException nfe) {
                    LOG.warn("Paramètre {} non numérique", "page", nfe);
                }
            } else if (QueryParam.LIMIT.getParam().equals(parameter)) {
                try {
                    pageLimit = Integer.valueOf(getFirst(values));
                } catch (NumberFormatException nfe) {
                    LOG.warn("Paramètre {} non numérique", "limit", nfe);
                }
            } else {
                // Ajout d'un filtre
                searchOptions.addFilter(parameter, values);
            }
        }
        // Langue (fr par défaut)
        if (!parameterMap.containsKey("l")) {
            searchOptions.setLanguage(LangueUtil.getLocale(ContexteUtil.getContexteUniv().getLangue()).getLanguage());
        }
        // Ajout des groupes autorisés pour effectuer la recherche
        searchOptions.setAuthorizedGroups(ContexteUtil.getContexteUniv().getGroupesDsiAvecAscendants());
        searchOptions.setPage(page, pageLimit);
        return searchOptions;
    }

    /**
     *
     * {@inheritDoc}
     */
    @Override
    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        long debut = System.currentTimeMillis();
        SearchOptions searchOptions = this.processRequest(request);
        LOG.debug("Search options : {} ", searchOptions);
        // Appel du traitement de la recherche
        SearchResultBean result = null;
        try {
            result = serviceSearcher.search(searchOptions);
        } catch (Exception e) {
            LOG.error("Exception lors de la recherche", e);
            throw e;
        }
        response.setStatus(HttpServletResponse.SC_OK);
        response.setCharacterEncoding("UTF-8");
        //On passe au contexte de la servlet le bean de resultat pour pouvoir l'utiliser dans les différentes vues.
        if (result != null) {
            ApplicationContext tilesContext = ServletUtil.getApplicationContext(request.getSession().getServletContext());
            Request tilesRequest = new ServletRequest(tilesContext, (HttpServletRequest) request, (HttpServletResponse) response);
            TilesContainer container = TilesAccess.getContainer(tilesContext);
            AttributeContext attributeContext = container.getAttributeContext(tilesRequest);
            attributeContext.putAttribute("search_result", new Attribute(result), true);
        }
        getServletContext().getRequestDispatcher("/jsp/liste_resultats_fullText.jsp").forward(request, response);
        LOG.debug("Fin du traitement de la recherche en {} ms", System.currentTimeMillis() - debut);
    }

    /**
     * Méthode permettant de récupérer la première valeur des paramètres.
     * @param stringValues
     * @return
     */
    protected static String getFirst(String[] stringValues) {
        String result = null;
        if (ArrayUtils.isNotEmpty(stringValues)) {
            result = stringValues[0];
        }
        return result;
    }

    /**
     * Ajout du critère de tri de la recherche dans le bean d'option
     * @param searchOptions : bean d'option
     * @param sortValues : liste des options de tris.
     */
    protected static void fillSortOptions(SearchOptions searchOptions, List<String> sortValues) {
        Map<String, SortOption> sortBeanMap = ApplicationContextManager.getAllBeansOfType(SortOption.class);
        for (Map.Entry<String, SortOption> sortEntry : sortBeanMap.entrySet()) {
            if (sortValues.contains(sortEntry.getValue().getKey())) {
                searchOptions.addSort(sortEntry.getValue());
            }
        }
    }

    protected ServiceSearcher getServiceSearcher() {
        return serviceSearcher;
    }

    public void setServiceSearcher(final ServiceSearcher serviceSearcher) {
        this.serviceSearcher = serviceSearcher;
    }
}

