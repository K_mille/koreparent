package com.kosmos.search.query.tag;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import javax.servlet.jsp.tagext.TryCatchFinally;

import org.apache.commons.lang3.StringUtils;

import com.jsbsoft.jtf.core.InfoBean;
import com.kportal.extension.ExtensionHelper;
import com.kportal.frontoffice.util.JSPIncludeHelper;
import com.univ.utils.ContexteUtil;

/**
 *
 * Created on 12/11/14.
 */
public abstract class AbstractSearchTag<T extends Serializable> extends TagSupport implements TryCatchFinally {

    public static final String ID_EXTENSION = "idExtension";

    public static final String VIEW_MODEL = "viewModel";

    private static final long serialVersionUID = 8099001541848967543L;

    protected T viewModel;

    protected boolean front = false;

    protected String extension;

    public void setViewModel(T viewModel) {
        this.viewModel = viewModel;
    }

    public void setFront(boolean front) {
        this.front = front;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    @Override
    public int doStartTag() throws JspException {
        pageContext.getRequest().setAttribute(VIEW_MODEL, viewModel);
        pageContext.getRequest().setAttribute(ID_EXTENSION, extension);
        return 0;
    }

    protected void includeJSP(String path) {
        String templatedPath = path;
        if (StringUtils.isNotBlank(templatedPath)) {
            if (StringUtils.isNotBlank(extension)) {
                templatedPath = ExtensionHelper.getTemplateExtension(extension, path, front);
            }
            JSPIncludeHelper.includeJsp(pageContext.getOut(), pageContext.getServletContext(), (HttpServletRequest) pageContext.getRequest(), (HttpServletResponse) pageContext.getResponse(), templatedPath);
        }
    }

    @Override
    public void doCatch(Throwable throwable) throws Throwable {
        throw throwable;
    }

    @Override
    public void doFinally() {
        viewModel = null;
    }
}
