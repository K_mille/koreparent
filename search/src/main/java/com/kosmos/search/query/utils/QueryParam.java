package com.kosmos.search.query.utils;

/**
 * Created by camille.lebugle on 12/01/17.
 */
public enum QueryParam {
    QUERY("q"),
    SORT("s"),
    PAGE("page"),
    BEAN_KEY("beanKey"),
    TYPES("types"),
    LANGUAGE("l"),
    SITE("site"),
    LIMIT("limit");

    private String param = "";

    QueryParam(String param) {
        this.param = param;
    }

    public String getParam() {
        return param;
    }
}
