package com.kosmos.search.query.utils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.highlight.HighlightField;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.jsbsoft.jtf.core.LangueUtil;
import com.kosmos.search.query.bean.SearchOptions;
import com.kosmos.search.query.bean.SearchResultDoc;
import com.kosmos.search.query.bean.SearchResultDoc.HighlightedResult;
import com.kosmos.search.query.configuration.SearchAggregationConfiguration;
import com.kosmos.search.query.configuration.SearchExternalDocumentConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.univ.utils.EscapeString;

/**
 * Utilitaire pour la recherche.
 * @author cpoisnel
 *
 */
public class QueryUtil {

    public static final String SEARCH_SERVLET = "/servlet/search";

    public static final String SEARCH_RESULT = "search_result";

    public static final String DEFAULT_AGGREGATION_DEFINITION = "aggregation";

    public static final String DEFAULT_SEARCH_ANALYZER = "analyzer_core_";

    public static final String RAW_ANALYZER = "analyzer_core_raw_";

    public static final String NGRAM_ANALYZER = "analyzer_core_ngram_";

    private static final String SEARCH_JOKER_FICHE = "*";

    /**
     * Pour la gestion de multiple highlight, item ajouté à la fin de la clé.
     */
    private static final String HIGHLIGHT_KEY_RAW = ".raw";

    /**
     * Conversion de résultats de recherche Elasticsearch.
     * @param hits
     *   Résultats de type Elasticsearch
     * @return La liste des résultats Elasticsearch
     */
    public static List<SearchResultDoc> convertToDocs(final SearchHits hits) throws Exception {
        final List<SearchResultDoc> docs = new ArrayList<>();
        for (final SearchHit hit : hits.getHits()) {
            final SearchResultDoc resultat = new SearchResultDoc();
            resultat.setScore(hit.getScore());
            resultat.setId(hit.getId());
            final Map<String, Object> source = hit.getSource();
            if (source != null) {
                Map<String, Object> fiche = (HashMap<String, Object>) source.get("fiche");
                resultat.setSource(source);
                // Spécifique Fiche Core
                resultat.setTypeFiche(String.valueOf(source.get("type_fiche")));
                resultat.setCodeObjet(String.valueOf(source.get("code_objet")));
                resultat.setTitre(String.valueOf(source.get("libelle")));
                Map<String, Object> dataFiche = (Map<String, Object>) fiche.get(String.valueOf(source.get("type_fiche")));
                if (dataFiche != null) {
                    String datets = String.valueOf(dataFiche.get("dateModification"));
                    if (StringUtils.isNotBlank(datets) && StringUtils.isNumeric(datets)) {
                        resultat.setDateModification(new Date(Long.parseLong(datets)));
                    }
                }
            }
            resultat.setHighlightedFields(convertToHighlightedResults(hit.getHighlightFields()));
            final Map<String, SearchHits> innerHits = hit.getInnerHits();
            if (MapUtils.isNotEmpty(innerHits)) {
                final Map<String, List<SearchResultDoc>> innerDocs = new HashMap<>();
                for (final Map.Entry<String, SearchHits> innerHit : innerHits.entrySet()) {
                    innerDocs.put(innerHit.getKey(), convertToDocs(innerHit.getValue()));
                }
                resultat.setInnerDocs(innerDocs);
            }
            docs.add(resultat);
        }
        return docs;
    }

    /**
     * Convertisseurs de highlights de résultats
     * @param highlights
     * Highlights Elasticsearch
     * @return
     * Modèle d'highlights
     */
    public static Map<String, HighlightedResult> convertToHighlightedResults(final Map<String, HighlightField> highlights) {
        final Map<String, HighlightedResult> highlightsModels = new HashMap<>();
        for (final Map.Entry<String, HighlightField> entry : highlights.entrySet()) {
            // Ajout des highlights dans la map
            final HighlightField hField = entry.getValue();
            final Collection<String> fragments = new ArrayList<>();
            if (ArrayUtils.isNotEmpty(hField.getFragments())) {
                for (final Text fragment : hField.getFragments()) {
                    fragments.add(fragment.string());
                }
            }

            // On insère dans tous les cas une clé sans ".raw"
            if(entry.getKey().endsWith(HIGHLIGHT_KEY_RAW)) {
                highlightsModels.put(StringUtils.substringBefore(entry.getKey(), HIGHLIGHT_KEY_RAW), new HighlightedResult(hField.getName(), fragments));
            } else if(!highlightsModels.containsKey(entry.getKey())){
                highlightsModels.put(entry.getKey(), new HighlightedResult(hField.getName(), fragments));
            }
        }
        return highlightsModels;
    }

    /**
     * Récupération de la configuration de l'aggrégation à partir du nom
     * @param searchFicheConfiguration : Configuration globale de la recherche
     * @param aggregationName : Nom de l'aggrégation
     * @return
     */
    public static SearchAggregationConfiguration getAggregationConfiguration(SearchFicheConfiguration searchFicheConfiguration, String aggregationName) {
        for (SearchFilterConfiguration filterConfiguration : searchFicheConfiguration.getFilters()) {
            if (SearchAggregationConfiguration.class.isAssignableFrom(filterConfiguration.getClass())) {
                SearchAggregationConfiguration searchAggregation = (SearchAggregationConfiguration) filterConfiguration;
                SearchAggregationConfiguration aggregationConfiguration = getAggregationConfiguration(searchAggregation, aggregationName);
                if (null != aggregationConfiguration) {
                    return aggregationConfiguration;
                }
            }
        }
        return null;
    }

    /**
     * Recherche récursive des configuration d'aggrégations.
     * @param aggregationConfiguration
     * Configuration de l'aggrégation
     * @param aggregationName
     * Nom de l'aggrégation
     * @return la configuration d'aggrégation trouvée, null sinon.
     */
    protected static SearchAggregationConfiguration getAggregationConfiguration(SearchAggregationConfiguration aggregationConfiguration, String aggregationName) {
        if (aggregationConfiguration.getName().equals(aggregationName)) {
            return aggregationConfiguration;
        } else if (CollectionUtils.isNotEmpty(aggregationConfiguration.getSubAggregations())) {
            Set<SearchAggregationConfiguration> childAggregationConfigurationList = aggregationConfiguration.getSubAggregations();
            for (SearchAggregationConfiguration childAggregation : childAggregationConfigurationList) {
                SearchAggregationConfiguration foundAggregationConfiguration = getAggregationConfiguration(childAggregation, aggregationName);
                if (null != foundAggregationConfiguration) {
                    return foundAggregationConfiguration;
                }
            }
        }
        return null;
    }

    /**
     * Création de l'URL de recherche à partir d'un SearchOptions
     * @param searchOptions
     * @return
     */
    public static String buildQueryUrlFromSearchOption(SearchOptions searchOptions) {
        String queryUrl = StringUtils.EMPTY;
        if (searchOptions != null) {
            if (StringUtils.isNotBlank(searchOptions.getQuery())) {
                queryUrl += QueryParam.QUERY.getParam() + "=" + EscapeString.escapeURL(searchOptions.getQuery());
            }
            if (StringUtils.isNotBlank(searchOptions.getLanguage())) {
                if (StringUtils.isNotBlank(queryUrl)) {
                    queryUrl += "&";
                }
                queryUrl += QueryParam.LANGUAGE.getParam() + "=" + String.valueOf(LangueUtil.getIndiceFromCodeISO(searchOptions.getLanguage()));
            }
            if (StringUtils.isNotBlank(searchOptions.getSearchParameterBeanKey())) {
                if (StringUtils.isNotBlank(queryUrl)) {
                    queryUrl += "&";
                }
                queryUrl += QueryParam.BEAN_KEY.getParam() + "=" + EscapeString.escapeURL(searchOptions.getSearchParameterBeanKey());
            }
            if (StringUtils.isNotBlank(queryUrl)) {
                queryUrl += "&";
            }
            if (MapUtils.isNotEmpty(searchOptions.getFilters())) {
                for (Map.Entry<String, String[]> filterEntry : searchOptions.getFilters().entrySet()) {
                    for (String filter : filterEntry.getValue()) {
                        if (StringUtils.isNotBlank(queryUrl)) {
                            queryUrl += "&";
                        }
                        queryUrl += EscapeString.escapeHtml(filterEntry.getKey()) + "=" + EscapeString.escapeURL(filter);
                    }
                }
            }
            if (StringUtils.isNotBlank(queryUrl)) {
                queryUrl += "&";
            }
            queryUrl += QueryParam.LIMIT.getParam() + "=" + searchOptions.getLimit();
        }
        queryUrl = QueryUtil.SEARCH_SERVLET + (StringUtils.isNotBlank(queryUrl) ? ("?" + queryUrl) : StringUtils.EMPTY);
        return queryUrl;
    }

    /**
     * Méthode permettant de remplacer le joker dans le nom des champs.
     * @param rawName : nom du champ d'origine
     * @param objectName : nom de l'objet
     * @return
     */
    public static String translateSearchName(String rawName, String objectName) {
        return StringUtils.replace(rawName, SEARCH_JOKER_FICHE, objectName);
    }

    /**
     * Constitution de la liste des configurations des objets autres que des fiches.
     * @return
     */
    public static List<SearchFicheConfiguration> fetchExternalDocumentConfiguration() {
        List<SearchFicheConfiguration> listeConfiguration = new ArrayList<>();
        Map<String, SearchExternalDocumentConfiguration> listeBean = ApplicationContextManager.getAllBeansOfType(SearchExternalDocumentConfiguration.class);
        for (Map.Entry<String, SearchExternalDocumentConfiguration> confEntry : listeBean.entrySet()) {
            listeConfiguration.add(convertExternalDocToFicheConfiguration(confEntry.getValue()));
        }
        return listeConfiguration;
    }

    private static SearchFicheConfiguration convertExternalDocToFicheConfiguration(SearchExternalDocumentConfiguration documentConfiguration) {
        SearchFicheConfiguration searchFicheConfiguration = null;
        if (documentConfiguration != null) {
            searchFicheConfiguration = new SearchFicheConfiguration();
            searchFicheConfiguration.setIndex(documentConfiguration.getIndex());
            searchFicheConfiguration.setFields(documentConfiguration.getFields());
            searchFicheConfiguration.setFilters(documentConfiguration.getFilters());
            searchFicheConfiguration.setExcludedFields(documentConfiguration.getExcludedFields());
        }
        return searchFicheConfiguration;
    }
}
