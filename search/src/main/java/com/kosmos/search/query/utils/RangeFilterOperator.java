package com.kosmos.search.query.utils;

/**
 * Enumerator pour les différents opérateurs des filtres de type Range Query
 */
public enum RangeFilterOperator {

    LTE,
    LT,
    GTE,
    GT;

}
