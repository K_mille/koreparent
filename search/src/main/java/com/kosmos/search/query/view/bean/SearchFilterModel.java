package com.kosmos.search.query.view.bean;


public class SearchFilterModel {

    private String aggregationName;

    private String filterKey;

    private String filterName;

    private String filterUrl;

    public SearchFilterModel(final String aggregationName, final String filterKey, final String filterName) {
        this.aggregationName = aggregationName;
        this.filterKey = filterKey;
        this.filterName = filterName;
    }

    public String getAggregationName() {
        return aggregationName;
    }

    public void setAggregationName(final String aggregationName) {
        this.aggregationName = aggregationName;
    }

    public String getFilterKey() {
        return filterKey;
    }

    public void setFilterKey(final String filterKey) {
        this.filterKey = filterKey;
    }

    public String getFilterName() {
        return filterName;
    }

    public void setFilterName(final String filterName) {
        this.filterName = filterName;
    }

    public String getFilterUrl() {
        return filterUrl;
    }

    public void setFilterUrl(final String filterUrl) {
        this.filterUrl = filterUrl;
    }
}
