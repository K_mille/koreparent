package com.kosmos.search.query.view.bean;

import java.util.List;
import java.util.Map;

/**
 * Métadonnées de la recherche à afficher
 */
public class SearchMetadataModel {

    /**
     * Map des filtres sélectionnés par l'utilisateur.
     */
    private Map<String, List<SearchFilterModel>> filtersMap;

    /**
     * Requête renseignée par l'utilisateur
     */
    private String query;

    /**
     * Nombre total de résultats (attention, ne pas confondre avec le nombre de résultats demandés (pagination)).
     */
    private long total;

    /**
     * Temps pris en ms pour effectuer la requête Elasticsearch.
     */
    private long tookInMillis;

    /**
     * Code HTTP du résultat de l'appel à Elasticsearch.
     */
    private int status;

    private int page;

    private int limit;

    private String url;

    private int maxAggregationSize;

    private String langue;

    private String searchParameterBeanKey;

    private int hightLightFragmentNumber;

    public SearchMetadataModel() {
    }

    public Map<String, List<SearchFilterModel>> getFiltersMap() {
        return filtersMap;
    }

    public void setFiltersMap(final Map<String, List<SearchFilterModel>> filtersMap) {
        this.filtersMap = filtersMap;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(final String query) {
        this.query = query;
    }

    public long getTotal() {
        return total;
    }

    public void setTotal(final long total) {
        this.total = total;
    }

    public long getTookInMillis() {
        return tookInMillis;
    }

    public void setTookInMillis(final long tookInMillis) {
        this.tookInMillis = tookInMillis;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(final int status) {
        this.status = status;
    }

    public int getPage() {
        return page;
    }

    public void setPage(final int page) {
        this.page = page;
    }

    public int getLimit() {
        return limit;
    }

    public void setLimit(final int limit) {
        this.limit = limit;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(final String url) {
        this.url = url;
    }

    public int getMaxAggregationSize() {
        return maxAggregationSize;
    }

    public void setMaxAggregationSize(final int maxAggregationSize) {
        this.maxAggregationSize = maxAggregationSize;
    }

    public String getLangue() {
        return langue;
    }

    public void setLangue(final String langue) {
        this.langue = langue;
    }

    public String getSearchParameterBeanKey() {
        return searchParameterBeanKey;
    }

    public void setSearchParameterBeanKey(String searchParameterBeanKey) {
        this.searchParameterBeanKey = searchParameterBeanKey;
    }

    public int getHightLightFragmentNumber() {
        return hightLightFragmentNumber;
    }

    public void setHightLightFragmentNumber(final int hightLightFragmentNumber) {
        this.hightLightFragmentNumber = hightLightFragmentNumber;
    }
}
