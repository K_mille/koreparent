package com.kosmos.search.query.view.preparer;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.Definition;
import org.apache.tiles.TilesContainer;
import org.apache.tiles.access.TilesAccess;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.search.query.bean.aggregation.model.AggregationGroupModel;
import com.kosmos.tiles.DefinitionModel;
import com.kosmos.tiles.TilesUtil;

import static com.kosmos.search.query.utils.QueryUtil.DEFAULT_AGGREGATION_DEFINITION;

/**
 * Created by camille.lebugle on 17/02/17.
 */
public class AggregationGroupViewPreparer implements ViewPreparer {

    private static final Logger LOG = LoggerFactory.getLogger(AggregationGroupViewPreparer.class);

    @Override
    public void execute(final Request request, final AttributeContext attributeContext) {
        TilesContainer tilesContainer = TilesAccess.getContainer(request.getApplicationContext());
        final String defaultTilesDefinition = TilesUtil.DEFAULT_TILES_SEARCH_DEFINITION + TilesUtil.TILES_DEFINITION_SEPARATOR + DEFAULT_AGGREGATION_DEFINITION + TilesUtil.TILES_DEFINITION_SEPARATOR;
        if (attributeContext.getAttribute("aggregationModel") != null) {
            AggregationGroupModel groupModel = (AggregationGroupModel) attributeContext.getAttribute("aggregationModel").getValue();
            Iterator<AggregationModel> it = groupModel.iterator();
            //Constitution des definition et vérification qu'il en existe une dans les configurations de tiles.
            List<DefinitionModel> listeDefinition = new ArrayList<>();
            Definition aggregationDefinition;
            while (it.hasNext()) {
                AggregationModel aggregationModel = it.next();
                //Existe-t-il une définition spécifique (par le nom)
                String aggregationDefinitionName = defaultTilesDefinition + aggregationModel.getName();
                aggregationDefinition = tilesContainer.getDefinition(aggregationDefinitionName, request);
                if (aggregationDefinition == null) {
                    //Sinon existe-t-il une définition propre au type de l'aggrégation
                    aggregationDefinitionName = defaultTilesDefinition + aggregationModel.getType();
                    aggregationDefinition = tilesContainer.getDefinition(aggregationDefinitionName, request);
                }
                if (aggregationDefinition != null) {
                    aggregationDefinition.putAttribute("aggregationModel", new Attribute(aggregationModel), true);
                    listeDefinition.add(new DefinitionModel(aggregationDefinitionName, aggregationModel));
                } else {
                    LOG.debug("La définition tiles {} est introuvable", aggregationDefinitionName);
                }
            }
            attributeContext.putAttribute("aggregationChildList", new Attribute(listeDefinition));
        }
    }
}
