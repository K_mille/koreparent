package com.kosmos.search.query.view.preparer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.Definition;
import org.apache.tiles.TilesContainer;
import org.apache.tiles.access.TilesAccess;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.bean.aggregation.model.AggregationModel;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.tiles.DefinitionModel;
import com.kosmos.tiles.TilesUtil;
import com.univ.utils.ContexteUtil;

import static com.kosmos.search.query.utils.QueryUtil.DEFAULT_AGGREGATION_DEFINITION;

/**
 * Created by camille.lebugle on 04/01/17.
 */
public class AggregationViewPreparer implements ViewPreparer {

    private static final Logger LOG = LoggerFactory.getLogger(AggregationViewPreparer.class);

    @Override
    public void execute(final Request request, final AttributeContext attributeContext) {
        ApplicationContext tilesContext = request.getApplicationContext();
        TilesContainer tilesContainer = TilesAccess.getContainer(tilesContext);
        boolean isResultPresent = false;
        final String defaultTilesDefinition = TilesUtil.DEFAULT_TILES_SEARCH_DEFINITION + TilesUtil.TILES_DEFINITION_SEPARATOR + DEFAULT_AGGREGATION_DEFINITION + TilesUtil.TILES_DEFINITION_SEPARATOR;
        if(attributeContext.getAttribute(QueryUtil.SEARCH_RESULT) != null) {
            SearchResultBean result = (SearchResultBean) attributeContext.getAttribute(QueryUtil.SEARCH_RESULT).getValue();
            //Itération sur les différentes aggregations
            if (CollectionUtils.isNotEmpty(result.getDocs())) {
                isResultPresent = true;
            }
            final Collection<AggregationModel> aggregations = result.getAggregations();
            Iterator<AggregationModel> it = aggregations.iterator();
            //Constitution des definition et vérification qu'il en existe une dans les configurations de tiles.
            List<DefinitionModel> listeDefinition = new ArrayList<>();
            Definition aggregationDefinition;
            while (it.hasNext()) {
                AggregationModel aggregationModel = it.next();
                //Existe-t-il une définition spécifique (par le nom)
                String aggregationDefinitionName = defaultTilesDefinition + aggregationModel.getName();
                aggregationDefinition = tilesContainer.getDefinition(aggregationDefinitionName, request);
                if (aggregationDefinition == null) {
                    //Sinon existe-t-il une définition propre au type de l'aggrégation
                    aggregationDefinitionName = defaultTilesDefinition + aggregationModel.getType();
                    aggregationDefinition = tilesContainer.getDefinition(aggregationDefinitionName, request);
                }
                if (aggregationDefinition != null) {
                    aggregationDefinition.putAttribute("aggregationModel", new Attribute(aggregationModel), true);
                    listeDefinition.add(new DefinitionModel(aggregationDefinitionName, aggregationModel));
                } else {
                    LOG.debug("La définition tiles {} est introuvable", aggregationDefinitionName);
                }
            }
            attributeContext.putAttribute("aggregationList", new Attribute(listeDefinition));
            attributeContext.putAttribute("aggregationMaxSize", new Attribute(result.getSearchMetadataModel().getMaxAggregationSize()), true);
            attributeContext.putAttribute("langue", new Attribute(result.getSearchMetadataModel().getLangue()), true);
            attributeContext.putAttribute("query", new Attribute(result.getSearchMetadataModel().getQuery()), true);
            attributeContext.putAttribute("searchParameterBeanKey", new Attribute(result.getSearchMetadataModel().getSearchParameterBeanKey()), true);
            attributeContext.putAttribute("url", new Attribute(result.getSearchMetadataModel().getUrl()), true);
            attributeContext.putAttribute("rh", new Attribute(ContexteUtil.getContexteUniv().getCodeRubriquePageCourante()), true);
        }else{
            LOG.warn("Bean de résultat de recherche non trouvé");
        }
        attributeContext.putAttribute("isResultPresent", new Attribute(isResultPresent));
    }
}