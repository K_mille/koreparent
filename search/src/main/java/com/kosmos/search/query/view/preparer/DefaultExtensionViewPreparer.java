package com.kosmos.search.query.view.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kportal.core.webapp.WebAppUtil;

/**
 * ViewPreparer parent pour toutes les extensions.
 * La méthode execute permet de calculer l'emplacement du répertoire des extensions dans la webapp.
 */
public abstract class DefaultExtensionViewPreparer extends DefaultResultViewPreparer implements ViewPreparer {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultExtensionViewPreparer.class);

    @Override
    public void execute(final Request request, final AttributeContext attributeContext) {
        super.execute(request,attributeContext);
        LOG.debug("Calcul du chemin du template pour l'extension {}", getIdExtension());
        final Attribute templateAttribute = attributeContext.getTemplateAttribute();
        templateAttribute.setValue(WebAppUtil.getRelativeExtensionPath(getIdExtension()) + templateAttribute.getValue());
    }

    /**
     * Retourne l'ID de l'extension.
     */
    public abstract String getIdExtension();
}
