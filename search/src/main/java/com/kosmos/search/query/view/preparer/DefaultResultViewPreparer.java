package com.kosmos.search.query.view.preparer;

import java.text.DateFormat;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.collections4.MapUtils;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.search.query.bean.SearchResultDoc;
import com.kportal.cms.objetspartages.Objetpartage;
import com.univ.objetspartages.om.FicheUniv;
import com.univ.objetspartages.om.ReferentielObjets;
import com.univ.utils.ContexteUtil;
import com.univ.utils.FicheUnivHelper;
import com.univ.utils.UnivWebFmt;

/**
 * Created by camille.lebugle on 09/02/17.
 */
public class DefaultResultViewPreparer implements ViewPreparer {

    private static final Logger LOG = LoggerFactory.getLogger(DefaultResultViewPreparer.class);

    protected FicheUniv localFiche;

    @Override
    public void execute(final Request tilesContext, final AttributeContext attributeContext) {
        if (attributeContext.getAttribute("resultModel") != null) {
            SearchResultDoc searchResultDoc = (SearchResultDoc) attributeContext.getAttribute("resultModel").getValue();
            if (searchResultDoc.getDateModification() != null) {
                DateFormat dateFormatLong = DateFormat.getDateInstance(DateFormat.SHORT, ContexteUtil.getContexteUniv().getLocale());
                attributeContext.putAttribute("date_modification", new Attribute(dateFormatLong.format(searchResultDoc.getDateModification())));
            }
            String score = String.valueOf((int) (searchResultDoc.getScore() * 100D));
            attributeContext.putAttribute("score", new Attribute(score));
            final Map<String, Object> source = searchResultDoc.getSource();
            if (source != null) {
                Map<String, Object> fiche = (HashMap<String, Object>) source.get("fiche");
                if (MapUtils.isNotEmpty(fiche)) {
                    Objetpartage objetpartage = ReferentielObjets.getObjetByCode(String.valueOf(source.get("code_objet")));
                    Map<String, Object> rawFiche = null;
                    rawFiche = (Map<String, Object>) fiche.get(source.get("type_fiche"));
                    if (rawFiche != null && objetpartage != null) {
                        searchResultDoc.setLibelleObjet(objetpartage.getLibelleAffichable());
                        FicheUniv ficheUniv = null;
                        try {
                            ficheUniv = FicheUnivHelper.getFiche(String.valueOf(source.get("code_objet")), String.valueOf(rawFiche.get("code")), String.valueOf(rawFiche.get("langue")));
                        } catch (Exception e) {
                            LOG.error("Erreur lors de la récupération de la ficheUniv", e);
                        }
                        if (ficheUniv != null) {
                            localFiche = ficheUniv;
                            searchResultDoc.setTitre(ficheUniv.getLibelleAffichable());
                            searchResultDoc.setUrl(UnivWebFmt.determinerUrlFiche(ContexteUtil.getContexteUniv(), ficheUniv));
                        }
                    } else {
                        //Affichage par défaut. On récupère le titre et l'url du document qui sont les champs génériques.
                        searchResultDoc.setTitre(String.valueOf(fiche.get("titre")));
                        searchResultDoc.setUrl(String.valueOf(fiche.get("url")));
                    }
                }
            }
        }
    }
}

