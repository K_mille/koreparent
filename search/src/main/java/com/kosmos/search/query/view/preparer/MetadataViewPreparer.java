package com.kosmos.search.query.view.preparer;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;

import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.search.query.view.bean.SearchFilterModel;
import com.kosmos.search.query.view.bean.SearchMetadataModel;
import com.univ.utils.ContexteUtil;

/**
 * ViewPreparer du bandeau de metadonnées de la recherche.
 */
public class MetadataViewPreparer implements ViewPreparer {

    @Override
    public void execute(final Request request, final AttributeContext attributeContext) {
        if(attributeContext.getAttribute(QueryUtil.SEARCH_RESULT) != null) {
            SearchResultBean result = (SearchResultBean) attributeContext.getAttribute(QueryUtil.SEARCH_RESULT).getValue();
            List<SearchFilterModel> filterList = new ArrayList();
            SearchMetadataModel searchMetadataModel = result.getSearchMetadataModel();
            Map<String, List<SearchFilterModel>> filters = searchMetadataModel.getFiltersMap();
            List<Map.Entry<String, String>> pairFilter = new ArrayList<>();
            if (filters != null && filters.size() > 0) {
                for (Map.Entry<String, List<SearchFilterModel>> entry : filters.entrySet()) {
                    for(SearchFilterModel filter:entry.getValue()){
                        filter.setFilterUrl(StringUtils.removePattern(searchMetadataModel.getUrl(),"&?"+filter.getAggregationName() + "=" + filter.getFilterKey()));
                        filterList.add(filter);
                        pairFilter.add(new AbstractMap.SimpleEntry<String, String>(filter.getAggregationName(), filter.getFilterKey()));
                    }
                }
            }
            attributeContext.putAttribute("filterList", new Attribute(filterList));
            if(StringUtils.isNotBlank(searchMetadataModel.getQuery())){
                attributeContext.putAttribute("query", new Attribute(searchMetadataModel.getQuery()));
            }
            if(searchMetadataModel.getTotal()>0){
                attributeContext.putAttribute("total", new Attribute(searchMetadataModel.getTotal()));
            }
            attributeContext.putAttribute("langue", new Attribute(searchMetadataModel.getLangue()));
            attributeContext.putAttribute("searchParameterBeanKey", new Attribute(searchMetadataModel.getSearchParameterBeanKey()));
            attributeContext.putAttribute("rh", new Attribute(ContexteUtil.getContexteUniv().getCodeRubriquePageCourante()));
            attributeContext.putAttribute("pairFilter", new Attribute(pairFilter));
        }
    }
}
