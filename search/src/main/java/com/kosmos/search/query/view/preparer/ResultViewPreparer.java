package com.kosmos.search.query.view.preparer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.Definition;
import org.apache.tiles.TilesContainer;
import org.apache.tiles.access.TilesAccess;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.bean.SearchResultDoc;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.search.query.view.bean.SearchMetadataModel;
import com.kosmos.tiles.DefinitionModel;
import com.kosmos.tiles.TilesUtil;

/**
 * Created by camille.lebugle on 06/01/17.
 */
public class ResultViewPreparer implements ViewPreparer {

    private static final String RESULT_DEFINITION = "result";

    private static final String DEFAULT_SEARCH_RESULT = "default";

    private static final Logger LOG = LoggerFactory.getLogger(ResultViewPreparer.class);

    @Override
    public void execute(final Request request, final AttributeContext attributeContext) {
        ApplicationContext tilesContext = request.getApplicationContext();
        TilesContainer tilesContainer = TilesAccess.getContainer(tilesContext);
        final String defaultTilesDefinition = TilesUtil.DEFAULT_TILES_SEARCH_DEFINITION + TilesUtil.TILES_DEFINITION_SEPARATOR + RESULT_DEFINITION + TilesUtil.TILES_DEFINITION_SEPARATOR;
        if (attributeContext.getAttribute(QueryUtil.SEARCH_RESULT) != null) {
            SearchResultBean result = (SearchResultBean) attributeContext.getAttribute(QueryUtil.SEARCH_RESULT).getValue();
            List<DefinitionModel> listeDefinition = new ArrayList<>();
            final Collection<SearchResultDoc> docs = result.getDocs();
            for (SearchResultDoc searchResultDoc : docs) {
                //Existe-t-il une définition spécifique (par le nom)
                String resultDefinitionName = defaultTilesDefinition + searchResultDoc.getTypeFiche();
                Definition resultDefinition = tilesContainer.getDefinition(resultDefinitionName, request);
                if (resultDefinition == null) {
                    //Sinon existe-t-il une définition propre au type de l'aggrégation
                    resultDefinitionName = defaultTilesDefinition + DEFAULT_SEARCH_RESULT;
                    resultDefinition = tilesContainer.getDefinition(resultDefinitionName, request);
                }
                if (resultDefinition != null) {
                    listeDefinition.add(new DefinitionModel(resultDefinitionName, searchResultDoc));
                } else {
                    LOG.debug("La définition tiles pour l'affichage du résultat de recherche {} est introuvable", resultDefinitionName);
                }
            }
            attributeContext.putAttribute("resultList", new Attribute(listeDefinition));
            SearchMetadataModel searchMetadataModel = result.getSearchMetadataModel();
            attributeContext.putAttribute("hightLightFragmentNumber", new Attribute(searchMetadataModel.getHightLightFragmentNumber()), true);
        }
    }
}

