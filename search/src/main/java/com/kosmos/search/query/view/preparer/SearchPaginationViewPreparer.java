package com.kosmos.search.query.view.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.ApplicationContext;
import org.apache.tiles.request.Request;

import com.kosmos.search.query.bean.SearchResultBean;
import com.kosmos.search.query.utils.QueryUtil;
import com.kosmos.search.query.view.bean.SearchMetadataModel;

/**
 * Created by camille.lebugle on 12/01/17.
 */
public class SearchPaginationViewPreparer implements ViewPreparer {

    @Override
    public void execute(final Request request, final AttributeContext attributeContext) {
        ApplicationContext tilesContext = request.getApplicationContext();
        if(attributeContext.getAttribute(QueryUtil.SEARCH_RESULT) != null) {
            SearchResultBean result = (SearchResultBean) attributeContext.getAttribute(QueryUtil.SEARCH_RESULT).getValue();
            SearchMetadataModel searchMetadataModel = result.getSearchMetadataModel();
            int nbPage = (int) Math.floor(searchMetadataModel.getTotal() / searchMetadataModel.getLimit());
            int lastPage = (searchMetadataModel.getTotal() % searchMetadataModel.getLimit() != 0) ? (nbPage + 1) : nbPage;
            if (lastPage > searchMetadataModel.getPage()) {
                attributeContext.putAttribute("lastPage", new Attribute(lastPage));
                attributeContext.putAttribute("nextPage", new Attribute(searchMetadataModel.getPage() + 1));
            }
            if (searchMetadataModel.getPage() > 1) {
                attributeContext.putAttribute("firstPage", new Attribute(1));
                attributeContext.putAttribute("previousPage", new Attribute(searchMetadataModel.getPage() - 1));
            }
            if (searchMetadataModel.getTotal() > 1) {
                attributeContext.putAttribute("currentPage", new Attribute(searchMetadataModel.getPage()));
            }
            attributeContext.putAttribute("url", new Attribute(searchMetadataModel.getUrl()));
        }
    }

}
