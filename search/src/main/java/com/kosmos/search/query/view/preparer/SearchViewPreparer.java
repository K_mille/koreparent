package com.kosmos.search.query.view.preparer;

import org.apache.tiles.Attribute;
import org.apache.tiles.AttributeContext;
import org.apache.tiles.preparer.ViewPreparer;
import org.apache.tiles.request.Request;

import com.univ.utils.ContexteUtil;
import com.univ.utils.URLResolver;

/**
 * Created by charlie.camus on 07/04/17.
 */
public class SearchViewPreparer implements ViewPreparer {

    public final static String TILES_SEARCH_URL = "TILES_SEARCH_URL";
    public final static String WEB_SITE_URL = "WEB_SITE_URL";
    public final static String TILES_LANGUE = "LANGUE";
    public final static String TILES_RH = "rh";

    @Override
    public void execute(Request request, AttributeContext attributeContext) {
        attributeContext.putAttribute(WEB_SITE_URL, new Attribute(URLResolver.getAbsoluteUrl("/", ContexteUtil.getContexteUniv())));
        attributeContext.putAttribute(TILES_SEARCH_URL, new Attribute(URLResolver.getAbsoluteUrl("/servlet/search", ContexteUtil.getContexteUniv())));
        attributeContext.putAttribute(TILES_LANGUE, new Attribute(ContexteUtil.getContexteUniv().getLangue()));
        attributeContext.putAttribute(TILES_RH, new Attribute(ContexteUtil.getContexteUniv().getCodeRubriquePageCourante()));
    }

}
