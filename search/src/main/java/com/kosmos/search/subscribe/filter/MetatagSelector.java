package com.kosmos.search.subscribe.filter;

import java.util.Collection;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.Message;

import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.services.ServiceMetatag;

/**
 * Filtre sur les metatags. N'accepte que les metatags avec un état validé et dont l'objet peut être indexé.
 * @author cpoisnel
 *
 */
public class MetatagSelector extends FicheSelector {

    private static final Logger LOG = LoggerFactory.getLogger(MetatagSelector.class);

    /**
     * Constructeur par défaut.
     */
    public MetatagSelector() {
        super();
    }

    /**
     * Etat(s) de fiche non pris en compte. Par défaut, pas de sélection, ce qui signifie que tous les états sont autorisés.
     */
    private Collection<String> etatsExclus;

    /**
     * {@inheritDoc}
     * Filtrage sur l'état et le code objet du metatag bean.
     */
    @Override
    public boolean accept(final Message<?> message) {
        boolean isAccepted = false;
        try {
            if (!"metatag".equals(message.getHeaders().get("target"))) {
                return isAccepted;
            }
            final Object payload = message.getPayload();
            if (!(payload instanceof Long)) {
                return isAccepted;
            }
            ServiceMetatag serviceMetatag = ServiceManager.getServiceForBean(MetatagBean.class);
            MetatagBean meta = serviceMetatag.getById((Long) payload);
            isAccepted = meta != null && isEtatAutorise(meta) && isCodeObjetAutorise(meta.getMetaCodeObjet());
            return isAccepted;
        } finally {
            LOG.debug("Message accepté ? => '{}'. (Message : '{}').", isAccepted, message);
        }
    }

    /**
     * Est-ce que l'état est filtré?
     * @param metatagBean
     *      Bean de Metatag
     * @return vrai si l'état du Metatag est autorisé, faux sinon (élément rejeté)
     * @see #etatsExclus
     */
    protected boolean isEtatAutorise(final MetatagBean metatagBean) {
        if (CollectionUtils.isNotEmpty(etatsExclus)) {
            final String etatFicheCode = metatagBean.getMetaEtatObjet();
            return !etatsExclus.contains(etatFicheCode);
        }
        return true;
    }

    public Collection<String> getEtatsExclus() {
        return etatsExclus;
    }

    public void setEtatsExclus(final Collection<String> etatsExclus) {
        this.etatsExclus = etatsExclus;
    }
}
