<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>

<tilesx:useAttribute id="aggregationList" name="aggregationList" classname="java.util.List" ignore="true"/>
<tilesx:useAttribute id="isResultPresent" name="isResultPresent" classname="java.lang.Boolean"/>
<tilesx:useAttribute name="aggregationMaxSize" id="aggregationMaxSize"/>
<tilesx:useAttribute id="langue" name="langue" ignore="true"/>
<tilesx:useAttribute id="searchParameterBeanKey" name="searchParameterBeanKey" ignore="true"/>
<tilesx:useAttribute id="query" name="query" ignore="true"/>
<tilesx:useAttribute id="rh" name="rh" ignore="true"/>

<c:if test="${isResultPresent}">
    <c:if test="${not empty aggregationList}">
        <button type="button" class="aggregation-toggle">${utils:getCoreMessage("SEARCH_AGGREGATION_FILTERS")}</button>
        <form id="aggregation_search" name="aggregation_search" action="/servlet/search" method="get">
            <input type="hidden" name="q" value="<c:out value="${query}"/>"/>
            <input type="hidden" name="l" value="<c:out value="${langue}"/>"/>
            <input type="hidden" name="beanKey" value="<c:out value="${searchParameterBeanKey}"/>">
            <input type="hidden" name="RH" value="<c:out value="${rh}"/>">
            <ul class="search-aggregation__aggregation-list">
                <c:forEach var="item" items="${aggregationList}">
                    <c:set var="nbItem" value="${0}" scope="request"/>
                    <li>
                        <span class="aggregation-item__aggregation-name"><c:out value="${item.model.label}"/></span>
                        <tiles:insertDefinition name="${item.name}">
                            <tiles:putAttribute name="aggregationModel" value="${item.model}" cascade="true"/>
                        </tiles:insertDefinition>
                    </li>
                </c:forEach>
            </ul>
            <button class="submit" type="submit">
                    ${utils:getCoreMessage('JTF_BOUTON_VALIDER')}
            </button>
        </form>
    </c:if>
</c:if>

