<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>
<tilesx:useAttribute id="aggregationChildList" name="aggregationChildList" classname="java.util.List" ignore="true"/>

<c:if test="${not empty aggregationChildList}">
    <ul class="term-aggregation__item-list">
    <c:forEach var="item" items="${aggregationChildList}">
        <tiles:insertDefinition name="${item.name}">
            <tiles:putAttribute name="aggregationModel" value="${item.model}" cascade="true"/>
            <tiles:putAttribute name="mergedList" value="true" cascade="true" type="java.lang.Boolean"/>
            <tiles:putAttribute name="nbItem" value="${nbItem}" cascade="true" type="java.lang.Integer"/>
        </tiles:insertDefinition>
    </c:forEach>
    </ul>
</c:if>