<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>
<tilesx:useAttribute name="aggregationModel" id="aggregationModel" classname="com.kosmos.search.query.bean.aggregation.model.FilterRangeModel"/>

<div class="aggregation-item__range-filter">

    <c:if test="${not empty aggregationModel.fromName}">
        <p class="range-filter__from champ">
            <label for="${aggregationModel.fromName}">${utils:getCoreMessage("SEARCH_FILTER_UPDATE_DATE_FROM")}</label>
            <input type="text" name="${aggregationModel.fromName}" id="${aggregationModel.fromName}" value="${aggregationModel.from}" class="type_date"/>
        </p>
    </c:if>


    <c:if test="${not empty aggregationModel.toName}">
        <p class="range-filter__to champ">
            <label for="${aggregationModel.toName}">${utils:getCoreMessage("SEARCH_FILTER_UPDATE_DATE_TO")}</label>
            <input type="text" name="${aggregationModel.toName}" id="${aggregationModel.toName}" value="${aggregationModel.to}" class="type_date"/>
        </p>
    </c:if>
</div>
