<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>
<tilesx:useAttribute name="aggregationModel" id="aggregation" classname="com.kosmos.search.query.bean.aggregation.model.AggregationTermModel"/>
<tilesx:useAttribute name="mergedList" id="mergedList" ignore="true"/>
<tilesx:useAttribute name="nbItem" id="nbItem" ignore="true" scope="request"/>
<tilesx:useAttribute name="aggregationMaxSize" id="aggregationMaxSize"/>

<c:if test="${empty nbItem}">
    <c:set var="nbItem" value="${0}"/>
</c:if>

<c:if test="${not mergedList}">
<ul class="term-aggregation__item-list">
</c:if>
    <c:forEach var="bucket" items="${aggregation.buckets}" varStatus="loop">
        <li class="js-search-aggregation-select <c:if test="${nbItem gt (aggregationMaxSize-1)}">item-list__hidden</c:if>">
            <label for="${aggregation.name}-${bucket.key}">
                <input type="checkbox" name="${aggregation.name}" id="${aggregation.name}-${bucket.key}" value="${bucket.key}" <c:if test="${bucket.selected}">checked</c:if>>
                <c:out value="${bucket.label} (${bucket.docCount})"/>
            </label>
        </li>
        <c:set var="nbItem" value="${nbItem + 1}" scope="request"/>
    </c:forEach>
    <c:if test="${nbItem gt aggregationMaxSize}">
        <a href="#" class="item-list__show-more">${utils:getCoreMessage("SEARCH_AGGREGATION_MORE")}</a>
        <a href="#" class="item-list__show-less">${utils:getCoreMessage("SEARCH_AGGREGATION_LESS")}</a>
    </c:if>

<c:if test="${not mergedList}">
</ul>
</c:if>