<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>

<tilesx:useAttribute id="filterList" name="filterList" classname="java.util.List" ignore="true"/>
<tilesx:useAttribute id="pairFilter" name="pairFilter" classname="java.util.List" ignore="true"/>
<tilesx:useAttribute id="query" name="query" ignore="true"/>
<tilesx:useAttribute id="total" name="total" ignore="true"/>
<tilesx:useAttribute id="langue" name="langue" ignore="true"/>
<tilesx:useAttribute id="searchParameterBeanKey" name="searchParameterBeanKey" ignore="true"/>
<tilesx:useAttribute id="rh" name="rh" ignore="true"/>

<div class="search-metadata__search-filter">
    <c:forEach var="filter" items ="${filterList}">
        <span class="search-filter__filter-selected">
            <c:out value="${filter.filterName}"/>
            <a href="${filter.filterUrl}" filter-value="${filter.filterKey}" agg-name="${filter.aggregationName}">&times;</a>
        </span>
    </c:forEach>
</div>
<div class="search-metadata__search-input">
    <form id="search_query_input" name="search_query_input" action="<c:out value="${formUrl}"/>" method="get">
        <input type="text" name="q" id="q" autocomplete="off" maxlength="255" data-bean="multiFicheAutoComplete" data-beankey="<c:out value="${searchParameterBeanKey}"/>"
               data-autocompleteurl="/servlet/com.kportal.servlet.autoCompletionServlet" data-no-tooltip placeholder="${utils:getCoreMessage("MOT_CLE")}"
               <c:if test="${not empty query}">value="<c:out value="${query}"/>"
        </c:if>/>
        <input type="hidden" name="l" value="<c:out value="${langue}"/>"/>
        <input type="hidden" name="beanKey" value="<c:out value="${searchParameterBeanKey}"/>">
        <input type="hidden" name="RH" value="<c:out value="${rh}"/>">
        <c:forEach var="filter" items="${pairFilter}">
            <input type="hidden" name="${filter.key}" value="${filter.value}"/>
        </c:forEach>
    </form>
</div>
<div class="search-metadata__search-title">
<span>${utils:getCoreMessage("ST_RESULTATS_LA_RECHERCHE")}
<c:if test="${not empty query}">
    ${utils:getCoreMessage("ST_RESULTATS_PORTANT_SUR")} <strong><c:out value="${query}"/></strong>
</c:if>
<c:choose>
    <c:when test="${empty total || total eq '0'}">
        ${utils:getCoreMessage("ST_RESULTATS_NA_DONNE")} ${utils:getCoreMessage("ST_RESULTATS_AUCUN_RESULTAT")}. <a href="/servlet/search?l="<c:out value="${langue}"/>"&beanKey="<c:out value="${searchParameterBeanKey}"/>">${utils:getCoreMessage("ST_RESULTATS_EFFECTUER_NOUVELLE_RECHERCHE")}</a>
    </c:when>
    <c:when test="${total eq '1'}">
        ${utils:getCoreMessage("ST_RESULTATS_A_DONNE")} ${total} ${utils:getCoreMessage("ST_RESULTATS_RESULTAT")}
    </c:when>
    <c:otherwise>
        ${utils:getCoreMessage("ST_RESULTATS_A_DONNE")} ${total} ${utils:getCoreMessage("ST_RESULTATS_RESULTATS")}
    </c:otherwise>
</c:choose>
</span>
</div>
