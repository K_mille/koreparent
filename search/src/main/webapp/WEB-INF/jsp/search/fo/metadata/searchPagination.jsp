<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>

<tilesx:useAttribute name="firstPage" id="firstPage" ignore="true"/>
<tilesx:useAttribute name="lastPage" id="lastPage" ignore="true"/>
<tilesx:useAttribute name="previousPage" id="previousPage" ignore="true"/>
<tilesx:useAttribute name="nextPage" id="nextPage" ignore="true"/>
<tilesx:useAttribute name="currentPage" id="currentPage" ignore="true"/>
<tilesx:useAttribute name="url" id="url" ignore="true"/>

<c:if test="${not empty firstPage}">
    <a href="<c:out value="${url}&page=${firstPage}"/>" class="search-pagination__first-page">${utils:getCoreMessage("ST_RESULTATS_PREMIERE_PAGE")}</a>
</c:if>
<c:if test="${not empty previousPage}">
    <a href="<c:out value="${url}&page=${previousPage}"/>" class="search-pagination__previous-page">${utils:getCoreMessage("ST_RESULTATS_PAGE_PRECEDENTE")}</a>
</c:if>
<c:if test="${not empty currentPage}">
    <span class="search-pagination__current-page"> ${utils:getCoreMessage("ST_RESULTATS_PAGE")} <c:out value="${currentPage}"/> /
        <c:choose>
            <c:when test="${not empty lastPage}">
                <c:out value="${lastPage}"/>
            </c:when>
            <c:otherwise>
                <c:out value="${currentPage}"/>
            </c:otherwise>
        </c:choose>
    </span>
</c:if>
<c:if test="${not empty nextPage}">
    <a href="<c:out value="${url}&page=${nextPage}"/>" class="search-pagination__next-page">${utils:getCoreMessage("ST_RESULTATS_PAGE_SUIVANTE")}</a>
</c:if>
<c:if test="${not empty lastPage}">
    <a href="<c:out value="${url}&page=${lastPage}"/>" class="search-pagination__next-page">${utils:getCoreMessage("ST_RESULTATS_DERNIERE_PAGE")}</a>
</c:if>

