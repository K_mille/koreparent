<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<tilesx:useAttribute id="resultList" name="resultList" classname="java.util.List" ignore="true"/>

<c:if test="${not empty resultList}">
    <ul class="search-result__result-list">
        <c:forEach var="item" items="${resultList}">
            <li>
                <div class="search-result__result-item">
                    <tiles:insertDefinition name="${item.name}">
                        <tiles:putAttribute name="resultModel" value="${item.model}" cascade="true"></tiles:putAttribute>
                    </tiles:insertDefinition>
                </div>
            </li>
        </c:forEach>
    </ul>
</c:if>