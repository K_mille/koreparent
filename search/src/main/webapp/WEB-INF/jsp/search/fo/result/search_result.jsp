<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles-extras" prefix="tilesx" %>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://kportal.kosmos.fr/tags/utils" prefix="utils" %>
<tilesx:useAttribute name="resultModel" id="resultModel" classname="com.kosmos.search.query.bean.SearchResultDoc"/>
<tilesx:useAttribute name="date_modification" id="dateModif" ignore="true"/>
<tilesx:useAttribute name="hightLightFragmentNumber" id="hightLightFragmentNumber" ignore="true"/>


<c:set var="hightlightCount" value="0"/>


<div class="search-result__item-header">
    <a class="item-title__element_title" href="${resultModel.url}"><c:out value="${resultModel.titre}"/></a>
    <input type="hidden" name="score" value="<c:out value="${resultModel.score}"/>"/>
</div>
<div class="search-result__item-content">
    <c:forEach var="hightlight" items="${resultModel.highlightedFields}">
        <c:forEach var="fragment" items="${hightlight.value.fragments}">
            <c:if test="${hightlightCount lt hightLightFragmentNumber}">
                <c:out value="${fragment}" escapeXml="false"/>
                <c:set var="hightlightCount" value="${hightlightCount + 1}"/>
            </c:if>
        </c:forEach>
    </c:forEach>
</div>
<div class="search-result__item-footer">
    <span class="item-footer__element_type"><c:out value="${resultModel.libelleObjet}"/></span>
    <c:if test="${not empty dateModif}">
        <span class="item-footer__element_type">- <c:out value="${dateModif}"/></span>
    </c:if>
</div>