<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<div class="search-aggregation">
    <tiles:insertAttribute name="aggregation"/>
</div>

<div class="search-result">
    <div class="search-result__metadata">
        <tiles:insertAttribute name="metadata"/>
    </div>

    <div class="search-result__result">
        <tiles:insertAttribute name="result"/>
    </div>

    <div class="search-result__pagination">
        <tiles:insertAttribute name="pagination"/>
    </div>
</div>