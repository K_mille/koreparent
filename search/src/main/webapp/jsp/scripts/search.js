(function ($) {
    'use strict';
    $('.aggregation-toggle').on('click', function() {
        $('#aggregation_search').toggleClass('aggregation_search--visible');
    });

    $('.item-list__show-more, .item-list__show-less').on('click', function() {
        $(this.parentElement).children('.item-list__show-more, .item-list__show-less').toggle();
        $(this.parentElement).children('.item-list__hidden').toggle();
    });

    $('.aggregation-item__aggregation-name').on('click', function() {
        $(this).parent('li').toggleClass('search-aggregation__aggregation-list__item--visible');
    });

    $('.js-search-aggregation-select').on('change', function () {
        var $form = $(this).closest('form');
        $form.submit();
    });

    $(document).on('click', '.autocomplete-suggestions', function() {
        $('#search_query_input').submit();
    });

})(jQuery.noConflict());
