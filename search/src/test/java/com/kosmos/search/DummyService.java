package com.kosmos.search;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Created by cpoisnel on 15/12/16.
 */
public class DummyService {

    private static final Logger LOG = LoggerFactory.getLogger(DummyService.class);

    public <T> void activate(T bean) {
        LOG.info(String.valueOf(bean));
    }
}
