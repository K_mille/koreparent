package com.kosmos.search.query.util;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.kosmos.search.query.configuration.SearchAggregationTermConfiguration;
import com.kosmos.search.query.configuration.SearchFicheConfiguration;
import com.kosmos.search.query.configuration.SearchFilterConfiguration;
import com.kosmos.search.query.utils.QueryUtil;

/**
 * Tests des utilitaires de recherche.
 * @author cpoisnel
 */
public class QueryUtilTest {

    /**
     * Validation de la recherche récursive d'aggrégations.
     * @throws Exception
     */
    @Test
    public void getAggregationConfiguration() throws Exception {


        SearchFicheConfiguration searchFicheConfiguration = new SearchFicheConfiguration("document");

        SearchAggregationTermConfiguration aggregation1 = new SearchAggregationTermConfiguration();
        aggregation1.setName("1");

        SearchAggregationTermConfiguration aggregation2 = new SearchAggregationTermConfiguration();
        aggregation2.setName("2");


        SearchAggregationTermConfiguration aggregation3 = new SearchAggregationTermConfiguration();
        aggregation3.setName("3");

        SearchAggregationTermConfiguration aggregation31 = new SearchAggregationTermConfiguration();
        aggregation31.setName("31");
        Set<SearchFilterConfiguration> filters3 = new HashSet<>();
        filters3.add(aggregation31);
        aggregation3.setSubAggregations(filters3);

        SearchAggregationTermConfiguration aggregation311 = new SearchAggregationTermConfiguration();
        aggregation311.setName("311");

        Set<SearchFilterConfiguration> filters31 = new HashSet<>();
        filters31.add(aggregation311);
        aggregation31.setSubAggregations(filters31);


        Set<SearchFilterConfiguration> filters = new HashSet<>();
        filters.add(aggregation1);
        filters.add(aggregation2);
        filters.add(aggregation3);

        searchFicheConfiguration.setFilters(filters);
        Assert.assertNull("L'aggrégation 4 n'existe pas", QueryUtil.getAggregationConfiguration(searchFicheConfiguration, "4"));
        Assert.assertEquals("L'aggrégation imbriquée doit être trouvée",aggregation31,QueryUtil.getAggregationConfiguration(searchFicheConfiguration,"31"));
        Assert.assertEquals("L'aggrégation imbriquée doit être trouvée",aggregation311,QueryUtil.getAggregationConfiguration(searchFicheConfiguration,"311"));
    }
}