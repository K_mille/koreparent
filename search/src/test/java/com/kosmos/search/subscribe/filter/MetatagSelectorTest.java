package com.kosmos.search.subscribe.filter;

import java.util.Arrays;
import java.util.Map;

import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.support.MessageBuilder;
import org.testng.Assert;
import org.testng.annotations.Test;

import com.google.common.collect.ImmutableMap;
import com.kosmos.service.impl.ServiceManager;
import com.univ.objetspartages.bean.MetatagBean;
import com.univ.objetspartages.om.EtatFiche;
import com.univ.objetspartages.services.ServiceMetatag;

import mockit.Expectations;
import mockit.Mocked;

/**
 * Created by cpoisnel on 15/12/16.
 */
@Test
public class MetatagSelectorTest {


    @Test
    public void testAccept(@Mocked final ServiceMetatag serviceMetatag) {
        MetatagSelector metatagSelector = new MetatagSelector();
        MetatagBean metatagBean = new MetatagBean();
        metatagBean.setId(1L);
        metatagBean.setMetaCodeObjet("9998");
        new Expectations(ServiceManager.class){{
            ServiceManager.getServiceForBean(MetatagBean.class);
            result=serviceMetatag;
        }};
        new Expectations() {{
            serviceMetatag.getById(1L);
            result=metatagBean;
        }};

        Map<String, Object> mapHeaders = ImmutableMap.of("target", (Object) "metatag", "action", (Object) "save");
        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(1L, new MessageHeaders(mapHeaders))));

        metatagSelector = new MetatagSelector();
        metatagSelector.setObjetsInclus(Arrays.asList("9997"));

        Assert.assertFalse(metatagSelector.accept(MessageBuilder.createMessage(1L, new MessageHeaders(mapHeaders))));


        metatagBean.setMetaCodeObjet("9999");
        metatagSelector = new MetatagSelector();
        metatagSelector.setObjetsExclus(Arrays.asList("9998"));
        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(1L, new MessageHeaders(mapHeaders))));

        metatagBean.setMetaCodeObjet("9999");
        metatagSelector = new MetatagSelector();
        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(1L, new MessageHeaders(mapHeaders))));


        metatagBean.setMetaCodeObjet("9996");
        metatagBean.setMetaEtatObjet(EtatFiche.EN_LIGNE.getEtat());


        metatagSelector = new MetatagSelector();
        metatagSelector.setEtatsExclus(Arrays.asList(EtatFiche.APERCU.getEtat()));
        metatagSelector.setObjetsExclus(Arrays.asList("9997"));
        metatagSelector.setObjetsInclus(Arrays.asList("9996"));

        Assert.assertTrue(metatagSelector.accept(MessageBuilder.createMessage(1L, new MessageHeaders(mapHeaders))));
        metatagBean.setMetaEtatObjet(EtatFiche.APERCU.getEtat());
        metatagSelector.setEtatsExclus(Arrays.asList(EtatFiche.APERCU.getEtat()));
        Assert.assertFalse(metatagSelector.accept(MessageBuilder.createMessage(1L, new MessageHeaders(mapHeaders))));

    }
}