package com.univ.objetspartages;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;

import fr.kosmos.selenium.annotation.Exigence;
import fr.kosmos.selenium.bean.ArticleBean;
import fr.kosmos.selenium.bean.LibelleBean;
import fr.kosmos.selenium.bean.MediaBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.fiche.ServiceArticle;
import fr.kosmos.selenium.utils.RoleTest;
import fr.kosmos.selenium.utils.TestUtils;

/**
 * Created on 19/12/17.
 */
public class ITArticleTestCase extends KSupTestCase {

    private ServiceArticle serviceArticle;

    public ITArticleTestCase() {
    }

    /***
     * {@inheritDoc}
     */
    public void initScreen() {
        serviceArticle = new ServiceArticle(this);
    }

    @Test
    @Exigence(ticketExigence = "KRTO-89", ticketTest = "KRTO-954", enTantQue = RoleTest.ADMIN, jeVeux = "Publier une fiche ayant tous ses champs renseignés", afinDe = "Vérifier que tous les champs de la fiche apparaîssent en front office")
    public void testCreationArticle() {
        serviceMenuBO.navigateToContenu("core", "SAISIE_ARTICLE");
        LibelleBean libelleBean = new LibelleBean("01","Administratif", StringUtils.EMPTY);
        ArticleBean articleBean = new ArticleBean();
        articleBean.setTitre("Fiche article");
        articleBean.setSousTitre("Sous-titre fiche article");
        articleBean.setRubrique("Les fiches et leurs listes");
        articleBean.setStructure("Structures pour l'intégration et jeux de données");
        articleBean.setDateArticle("01/01/2017");
        articleBean.setChapeau("Résumé article");
        articleBean.setCorps("Description article");
        articleBean.setThematique(libelleBean);
        articleBean.setPhoto(new MediaBean("Sac de glands", "Des glands posés sur un sac"));
        articleBean.setEncadre("Encadré article");
        serviceArticle.getScreenBO().createContent();
        serviceArticle.fillContent(articleBean);
        serviceArticle.previewContent();
        TestUtils.focusLastOpenedTab(this.getDriver());
        serviceArticle.controlFront(articleBean);
        TestUtils.closeCurrentTab(this.getDriver());
        TestUtils.focusLastOpenedTab(this.getDriver());
        serviceArticle.publishContent();
    }



}
