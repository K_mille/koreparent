package com.univ.objetspartages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;
import com.kdecole.core.selenium.helper.SeleniumHelper;

import fr.kosmos.selenium.annotation.Exigence;
import fr.kosmos.selenium.bean.PageLibreBean;
import fr.kosmos.selenium.bean.PageLibreBean.BlocContenu;
import fr.kosmos.selenium.bean.PageLibreBean.StyleBlocContenu;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.fiche.ServicePageLibre;
import fr.kosmos.selenium.utils.RoleTest;
import fr.kosmos.selenium.utils.TestUtils;

public class ITPagelibreTestCase extends KSupTestCase {

    private ServicePageLibre servicePageLibre;

    private PageLibreBean pageLibreBean;

    @Override
    public void initScreen() {
        servicePageLibre = new ServicePageLibre(this);

        pageLibreBean = new PageLibreBean();
        pageLibreBean.setTitre("Page libre avec contenu");
        List<BlocContenu> blocsContenu = new ArrayList<>();
        blocsContenu.add(new BlocContenu("1", "1", "100", "Première ligne"));
        blocsContenu.add(new BlocContenu("2", "1", "33", "deuxième ligne / première colonne"));
        blocsContenu.add(new BlocContenu("2", "2", "33", "deuxième ligne / deuxième colonne"));
        blocsContenu.add(new BlocContenu("2", "3", "33", "deuxième ligne / troisième colonne"));
        blocsContenu.add(new BlocContenu("3", "1", "100", "troisième ligne"));
        pageLibreBean.setBlocsContenu(blocsContenu);
    }

    @Test
    @Exigence(ticketExigence = "KRTO-121", ticketTest = "KRTO-940", enTantQue = RoleTest.ADMIN, jeVeux = "Publier une page libre ayant tous ses champs renseignés", afinDe = "Vérifier que tous les champs de la page libre apparaîssent en front office")
    public void testCreationPageLibre() {
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");
        creerPageLibre();
        servicePageLibre.viewContentDatagrid(pageLibreBean.getTitre());
        servicePageLibre.controlFront(pageLibreBean);
    }

    @Test
    @Exigence(ticketExigence = "KRTO-113", ticketTest = "KRTO-942", enTantQue = RoleTest.ADMIN, jeVeux = "Modifier une page libre existante", afinDe = "Vérifier que la modification apparaît en front office")
    public void testModificationPageLibre() {
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");

        creerPageLibre();

        servicePageLibre.searchContent(pageLibreBean);
        servicePageLibre.updateContentDatagrid(pageLibreBean.getTitre());

        pageLibreBean.getBlocsContenu().add(new BlocContenu("4", "1", "100", "Ajout ligne par modification"));

        servicePageLibre.editContent(pageLibreBean.getBlocsContenu().get(5),5);
        servicePageLibre.viewContentDatagrid(pageLibreBean.getTitre());

        servicePageLibre.controlFront(pageLibreBean);
    }

    @Test
    @Exigence(ticketExigence = "KRTO-113", ticketTest = "KRTO-939", enTantQue = RoleTest.ADMIN, jeVeux = "Consulter une page libre existante", afinDe = "Vérifier que le lien voir en ligne fonctionne")
    public void testConsultationEnLignePageLibre() {
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");

        creerPageLibre();

        servicePageLibre.searchContent(pageLibreBean);
        servicePageLibre.viewContentDatagrid(pageLibreBean.getTitre());
        servicePageLibre.controlFront(pageLibreBean);
    }

    @Test
    @Exigence(ticketExigence = "KRTO-113", ticketTest = "KRTO-939", enTantQue = RoleTest.ADMIN, jeVeux = "Consulter une page libre existante", afinDe = "Vérifier que la prévisualisation fonctionne")
    public void testConsultationPageLibre() {
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");

        creerPageLibre();

        servicePageLibre.searchContent(pageLibreBean);
        servicePageLibre.updateContentDatagrid(pageLibreBean.getTitre());
        servicePageLibre.previewContent();
        TestUtils.focusLastOpenedTab(this.getDriver());
        servicePageLibre.controlFront(pageLibreBean);
        TestUtils.closeCurrentTab(this.getDriver());
    }

    @Test
    @Exigence(ticketExigence = "KRTO-113", ticketTest = "KRTO-1113", enTantQue = RoleTest.ADMIN, jeVeux = "Consulter une page libre existante", afinDe = "Contrôler l'affichage des styles de paragraphe d'une page libre")
    public void testStyleParagraphePageLibre() {
        // Création du jeu de données
        PageLibreBean pageLibreBeanAvecStyle = new PageLibreBean();
        pageLibreBeanAvecStyle.setTitre("Page libre avec style de paragraphe");
        List<BlocContenu> blocsContenu = new ArrayList<>();
        StyleBlocContenu styleBlocContenu1 = new PageLibreBean.StyleBlocContenu("1","Lorem ipsum dolor sit amet, consectetur adipiscing elit", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis molestie nisi felis, sit amet viverra libero pellentesque vitae. Curabitur porta tellus eros. Quisque commodo turpis diam, faucibus accumsan felis vulputate eget. Vivamus fringilla pretium sem vitae sollicitudin. Fusce tellus arcu, posuere et varius ac, vestibulum non magna. Phasellus congue arcu nec lectus cursus aliquet. Nunc sodales elementum enim. Sed tincidunt leo vitae libero condimentum, ac rutrum dolor aliquet. Sed sit amet nisi non turpis tempus mollis ut nec ex.");
        StyleBlocContenu styleBlocContenu2 = new PageLibreBean.StyleBlocContenu("2","Lorem ipsum dolor sit amet, consectetur adipiscing elit", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis molestie nisi felis, sit amet viverra libero pellentesque vitae. Curabitur porta tellus eros. Quisque commodo turpis diam, faucibus accumsan felis vulputate eget. Vivamus fringilla pretium sem vitae sollicitudin. Fusce tellus arcu, posuere et varius ac, vestibulum non magna. Phasellus congue arcu nec lectus cursus aliquet. Nunc sodales elementum enim. Sed tincidunt leo vitae libero condimentum, ac rutrum dolor aliquet. Sed sit amet nisi non turpis tempus mollis ut nec ex.");
        StyleBlocContenu styleBlocContenu3 = new PageLibreBean.StyleBlocContenu("3","Lorem ipsum dolor sit amet, consectetur adipiscing elit", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis molestie nisi felis, sit amet viverra libero pellentesque vitae. Curabitur porta tellus eros. Quisque commodo turpis diam, faucibus accumsan felis vulputate eget. Vivamus fringilla pretium sem vitae sollicitudin. Fusce tellus arcu, posuere et varius ac, vestibulum non magna. Phasellus congue arcu nec lectus cursus aliquet. Nunc sodales elementum enim. Sed tincidunt leo vitae libero condimentum, ac rutrum dolor aliquet. Sed sit amet nisi non turpis tempus mollis ut nec ex.");
        blocsContenu.add(new BlocContenu("1", "1", "100", styleBlocContenu1.getTextForToolbox()));
        blocsContenu.add(new BlocContenu("2", "1", "100", styleBlocContenu2.getTextForToolbox()));
        blocsContenu.add(new BlocContenu("3", "1", "100", styleBlocContenu3.getTextForToolbox()));
        pageLibreBeanAvecStyle.setBlocsContenu(blocsContenu);

        // Lancement du test pour la prévisualisation
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");
        servicePageLibre.getScreenBO().createContent();
        servicePageLibre.fillContent(pageLibreBeanAvecStyle);
        servicePageLibre.previewContent();
        TestUtils.focusLastOpenedTab(this.getDriver());
        servicePageLibre.testStylesPresence(Arrays.asList(new StyleBlocContenu[]{styleBlocContenu1, styleBlocContenu2, styleBlocContenu3}));
        TestUtils.closeCurrentTab(this.getDriver());

        // Lancement du test pour l'enregistrement en ligne
        WebDriverWait wait = new WebDriverWait(getDriver(), SeleniumHelper.DEFAULT_WAIT_TIME_SECONDS, 1000);
        wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#actions li button.publier")));
        servicePageLibre.publishContent();
        servicePageLibre.viewContentDatagrid(pageLibreBeanAvecStyle.getTitre());
        servicePageLibre.testStylesPresence(Arrays.asList(new StyleBlocContenu[]{styleBlocContenu1, styleBlocContenu2, styleBlocContenu3}));
    }

    /**
     * Crée une page libre pour les tests.
     */
    private void creerPageLibre(){
        servicePageLibre.getScreenBO().createContent();
        servicePageLibre.fillContent(pageLibreBean);
        servicePageLibre.publishContent();
    }
}
