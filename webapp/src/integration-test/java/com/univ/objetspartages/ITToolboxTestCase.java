package com.univ.objetspartages;

import java.util.ArrayList;
import java.util.List;

import org.testng.annotations.Test;

import com.jsbsoft.jtf.core.ApplicationContextManager;

import fr.kosmos.selenium.annotation.Exigence;
import fr.kosmos.selenium.bean.PageLibreBean;
import fr.kosmos.selenium.bean.ToolboxLienRubriqueBean;
import fr.kosmos.selenium.core.KSupTestCase;
import fr.kosmos.selenium.service.ServiceToolbox;
import fr.kosmos.selenium.service.fiche.ServicePageLibre;
import fr.kosmos.selenium.utils.RoleTest;
import fr.kosmos.selenium.utils.TestUtils;

public class ITToolboxTestCase extends KSupTestCase {

    private ServicePageLibre servicePageLibre;
    private ServiceToolbox serviceToolbox;

    @Override
    public void initScreen() {
        servicePageLibre = new ServicePageLibre(this);
        serviceToolbox = new ServiceToolbox(this);
    }

    @Test
    @Exigence(ticketExigence = "KRTO-851", ticketTest = "KRTO-991", enTantQue = RoleTest.ADMIN, jeVeux = "Insérer un lien de rubrique", afinDe = "Vérifier que le lien fonctionne et qu'il est possible de le modifier")
    public void testInsererLienRubrique() {
        // Initialisation bean de page libre
        PageLibreBean pageLibreBean = new PageLibreBean();
        String titrePageLibre = "QA : AF - Insérer un lien Rubrique";
        pageLibreBean.setTitre(titrePageLibre);
        List<PageLibreBean.BlocContenu> blocsContenu = new ArrayList<>();
        blocsContenu.add(new PageLibreBean.BlocContenu("1", "1", "100", ""));
        pageLibreBean.setBlocsContenu(blocsContenu);

        // Renseignement de la partie purement page libre
        serviceMenuBO.navigateToContenu(ApplicationContextManager.DEFAULT_CORE_CONTEXT, "SAISIE_PAGELIBRE");
        servicePageLibre.getScreenBO().createContent();
        servicePageLibre.fillContent(pageLibreBean);

        // Création du lien de rubrique
        String texteAAfficher = "lienVersAccueil";
        ToolboxLienRubriqueBean toolboxLienRubriqueBean = new ToolboxLienRubriqueBean("Site DATA", texteAAfficher);
        serviceToolbox.addLienRubrique(toolboxLienRubriqueBean);

        // Vérification du lien dans le preview
        servicePageLibre.previewContent();
        TestUtils.focusLastOpenedTab(this.getDriver());
        serviceToolbox.testPresenceLien("//a[contains(@class,'type_rubrique_ lien_interne')]", toolboxLienRubriqueBean.getTexteAAfficher(), "http://docker-kdev.kosmos.fr/");

        // Modification du lien
        TestUtils.focusPreviousTab(this.getDriver());
        List<String> rubriquesAOuvrir = new ArrayList<>();
        rubriquesAOuvrir.add("Site DATA");
        rubriquesAOuvrir.add("Version française");
        rubriquesAOuvrir.add("Navigation");
        ToolboxLienRubriqueBean toolboxLienRubriqueBeanModified = new ToolboxLienRubriqueBean("Autres fonctionnalités", texteAAfficher + "Modif", rubriquesAOuvrir);
        serviceToolbox.editLienRubrique(texteAAfficher, toolboxLienRubriqueBeanModified);
        servicePageLibre.publishContent();

        // Vérification en front
        servicePageLibre.viewContentDatagrid(titrePageLibre);
        serviceToolbox.testPresenceLien("//a[contains(@class,'type_rubrique_')][contains(@class, 'lien_interne')]", toolboxLienRubriqueBeanModified.getTexteAAfficher(), "http://docker-kdev.kosmos.fr/autres-fonctionnalites-3090.kjsp");
    }
}
