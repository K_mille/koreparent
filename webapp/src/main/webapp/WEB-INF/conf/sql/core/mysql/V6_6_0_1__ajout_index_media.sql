-- Création des index sur les medias
DELIMITER $$
CREATE PROCEDURE ADD_INDEXES_MEDIA()
  BEGIN
    IF NOT EXISTS(select * from information_schema.statistics WHERE table_schema = (SELECT database()) AND table_name = "MEDIA" AND index_name="IDX_MEDIA_URL")
    THEN
      CREATE INDEX IDX_MEDIA_URL ON MEDIA(URL(333));
    END IF;
    IF NOT EXISTS(select * from information_schema.statistics WHERE table_schema = (SELECT database()) AND table_name = "MEDIA" AND index_name="IDX_MEDIA_URL_VIGNETTE")
    THEN
      CREATE INDEX IDX_MEDIA_URL_VIGNETTE ON MEDIA(URL_VIGNETTE(333));
    END IF;
  END$$
DELIMITER ;

CALL ADD_INDEXES_MEDIA();
DROP PROCEDURE IF EXISTS ADD_INDEXES_MEDIA;