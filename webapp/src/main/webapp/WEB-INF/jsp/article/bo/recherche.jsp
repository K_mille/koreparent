<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ include file="/adminsite/objetspartages/template/init_recherche.jsp" %>
<%@ include file="/adminsite/objetspartages/template/header_recherche.jsp" %>

<% if (listeIncluse) { %>
    <fieldset>
        <legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CRITERES_METIERS") %></legend>
<% }else{ %>
    <div class="fieldset neutre">
<% } %>

<% if (! toolbox.equals("LIEN_REQUETE")) { %>

<% univFmt.insererChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("BO_ARTICLE_TITRE")); %>

<% } %>

<% if (! listeIncluse) { %>
<p>
      <label for="DATE_DEBUT" class="colonne"><%= MessageHelper.getCoreMessage("BO_ARTICLE_DU") %></label>
      <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_DEBUT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10,MessageHelper.getCoreMessage("BO_ARTICLE_DU"),"",true,"");%>
      <label for="DATE_FIN"><%= MessageHelper.getCoreMessage("BO_ARTICLE_AU") %></label>
      <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_FIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10,MessageHelper.getCoreMessage("BO_ARTICLE_AU"),"",true,"");%>
</p>
<% } %>

<% univFmt.insererComboHashtable(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_THEMATIQUES", MessageHelper.getCoreMessage("ARTICLE.THEMATIQUE")); %>

<%if (toolbox.equals("LIEN_REQUETE"))  { %>
    <p class="retrait">
        <% fmt.insererChampSaisie(out, infoBean, "CENTRE_INTERETS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 1, 1, ""); %><!--
              --><label for="CENTRE_INTERETS"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_OU_CENTRES_INTERETS")%></label>
    </p>
    <script>
        var centreInterets = document.querySelector('#CENTRE_INTERETS'),
            thematique = document.querySelector('#THEMATIQUE');
        centreInterets.addEventListener('click', function updateThematique() {
            thematique.selectedIndex = 0;
        });
        thematique.addEventListener('change', function updateCentreInterets() {
            centreInterets.checked = false;
        })
    </script>
<% } %>

<% if (listeIncluse) { %>
</fieldset>
<fieldset>
    <legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_SELECTION_DATE") %></legend>

    <p>
        <span><b><%=MessageHelper.getCoreMessage("BO_RECHERCHE_UN_SEUL_CHOIX") %></b></span>
    </p>

    <% univFmt.insererCombo(fmt, out, infoBean, "SELECTION", FormateurJSP.SAISIE_FACULTATIF, "selection_article", MessageHelper.getCoreMessage("BO_RECHERCHE_PERIODE")); %>

    <p>
        <label for="JOUR" class="colonne"><%= MessageHelper.getCoreMessage("BO_ARTICLE_DES") %></label>
        <input id="JOUR" name="JOUR" value="${infoBean.get("JOUR")}" size="5" type="text" />&nbsp;<%= MessageHelper.getCoreMessage("BO_ARTICLE_DERNIERS_JOURS") %>
    </p>

    <p>
          <label for="DATE_DEBUT" class="colonne"><%= MessageHelper.getCoreMessage("BO_ARTICLE_DU") %></label>
          <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_DEBUT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("BO_ARTICLE_DU"),"",true,"");%>
          <label for="DATE_FIN"><%= MessageHelper.getCoreMessage("BO_ARTICLE_AU") %></label>
          <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_FIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, MessageHelper.getCoreMessage("BO_ARTICLE_AU"),"",true,"");%>
    </p>

</fieldset>

<fieldset>
    <legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_TRI_DATE")%></legend>
	<p>
		<input type="radio" id="TRI_DATE_ASC" name="TRI_DATE" value="DATE_ASC" <c:if test="${empty infoBean.get('TRI_DATE') or 'DATE_ASC' eq infoBean.get('TRI_DATE')}">checked="checked"</c:if>/>
		<label for="TRI_DATE_ASC"><%= module.getMessage("BO_RECHERCHE_CHRONOLOGIQUE_CROISSANT") %></label>
		<input type="radio" id="TRI_DATE_DESC" name="TRI_DATE" value="DATE_DESC" <c:if test="${'DATE_DESC' eq infoBean.get('TRI_DATE')}">checked="checked"</c:if>/>
		<label for="TRI_DATE_DESC"><%= module.getMessage("BO_RECHERCHE_CHRONOLOGIQUE_DECROISSANT") %></label>
	</p>

</fieldset>
<script type="text/javascript">
    var oPeriodField = document.forms[0].SELECTION;
    oPeriodField.onchange = function()
    {
        if (this.selectedIndex != 0)
            RefreshDateFields(this);
    }
    var oNbDays = document.forms[0].JOUR;
    oNbDays.onchange = function()
    {
        if (this.value != '')
            RefreshDateFields(this);
    }
    var oDtstart = document.forms[0].DATE_DEBUT;
    oDtstart.onblur = function()
    {
        if (this.value != '')
            RefreshDateFields(this);
    }
    var oDtend = document.forms[0].DATE_FIN;
    oDtend.onblur = function()
    {
        if (this.value != '')
            RefreshDateFields(this);
    }
    var oTriAsc = document.getElementById('TRI_DATE_ASC');
    var oTriDesc = document.getElementById('TRI_DATE_DESC');

    // met a jour les champs en exclusion mutuelle
    function RefreshDateFields(oFromField)
    {
        oTriAsc.checked=true;
        if (oPeriodField != oFromField)
        {
            oPeriodField.selectedIndex = 0;
        }
        if (oNbDays != oFromField)
        {
            oNbDays.value = '';
        }else{
            oTriDesc.checked=true;
        }
        if (oDtstart != oFromField && oDtend != oFromField)
        {
            oDtstart.value = '';
            oDtend.value = '';
            if (oPeriodField.selectedIndex==4 || oPeriodField.selectedIndex==5){
                oTriDesc.checked=true;
            }
        }
        else{
            oTriDesc.checked=true;
        }
    }
</script>

<%@ include file="/adminsite/objetspartages/template/footer_recherche.jsp" %>

<% }else{ %>
<%@ include file="/adminsite/objetspartages/template/footer_recherche.jsp" %>
    </div>
<% } %>


