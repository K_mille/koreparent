<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.objetspartages.util.LabelUtils"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@ page import="com.univ.utils.UnivWebFmt" %>
<jsp:useBean id="frontOfficeBean" class="com.univ.url.FrontOfficeBean" scope="request" />
<% 
ContexteUniv ctx = ContexteUtil.getContexteUniv();
Objetpartage module = ReferentielObjets.getObjetByNom("article");
int indiceEncadre = (Integer)ctx.getData("indiceEncadre");
ctx.putData("indiceEncadre",indiceEncadre++);
ctx.putData("titreEncadreRecherche",module.getMessage("ST_ENCADRE_RECHERCHE_ARTICLE"));
%><jsp:include page="<%= frontOfficeBean.getJspFo() + \"/template/encadres/recherche_debut.jsp\"  %>" />

<input type="hidden" name="OBJET" value="ARTICLE" />

<p>
  <label for="TITRE<%=indiceEncadre%>"><%=module.getMessage("ST_ENCADRE_RECHERCHE_ARTICLE_TITRE")%></label>
  <input type="text" class="champ-saisie" id="TITRE<%=indiceEncadre%>" name="TITRE" />
</p>

<p class="date">
  <label for="DATE_DEBUT<%=indiceEncadre%>"><%=module.getMessage("ST_ENCADRE_RECHERCHE_ARTICLE_DATE_DU")%></label>
    <input type="text" name="DATE_DEBUT" id="DATE_DEBUT<%=indiceEncadre%>" class="champ-saisie date type_date" />
    <input name="#FORMAT_DATE_DEBUT" value="1;2;0;10;-;1" type="hidden" />
</p>

<p class="date">
  <label for="DATE_FIN<%=indiceEncadre%>"><%=module.getMessage("ST_ENCADRE_RECHERCHE_ARTICLE_AU")%></label>
    <input type="text" name="DATE_FIN" id="DATE_FIN<%=indiceEncadre%>" class="champ-saisie date type_date" />
    <input name="#FORMAT_DATE_FIN" value="1;2;0;10;-;2" type="hidden" />
</p>

<p>
  <label for="THEMATIQUE<%=indiceEncadre%>"><%=module.getMessage("ST_ENCADRE_RECHERCHE_ARTICLE_THEMATIQUE")%></label>
  <select id="THEMATIQUE<%=indiceEncadre%>" name="THEMATIQUE">
      <option value="0000"><%=module.getMessage("ST_TOUTES")%></option>
      <%=UnivWebFmt.insererCombo(LabelUtils.getLabelCombo("04", ctx.getLocale()), "", true, 25)%>
  </select>
</p>

<jsp:include page="<%= frontOfficeBean.getJspFo() + \"/template/encadres/recherche_fin.jsp\"  %>" />
