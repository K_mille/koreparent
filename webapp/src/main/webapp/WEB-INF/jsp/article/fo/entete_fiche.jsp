<%@page import="java.util.List"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.univ.objetspartages.om.Article"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%
   ContexteUniv ctx = ContexteUtil.getContexteUniv();
   Article article = (Article) ctx.getFicheCourante();
   Objetpartage module = ReferentielObjets.getObjetByNom(ReferentielObjets.getNomObjet(article));
   if (article.getThematique().length() > 0) {
       List<String> thematiques = article.getListLibelleThematique();
       StringBuilder htmlContent = new StringBuilder("<div class=\"surtitre\"><ul class=\"thematiques\">");
       for(String thematique : thematiques){
           htmlContent.append("<li>");
           htmlContent.append(thematique);
           htmlContent.append("<span class=\"separateur-virgule\">, </span>");
           htmlContent.append("</li>");
       }
       htmlContent.append("</ul></div>");
       ctx.putData("surtitre",htmlContent.toString());
   }
   ctx.putData("titre",article.getTitre());
   
%>