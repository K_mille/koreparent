<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<%
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    Objetpartage module = ReferentielObjets.getObjetByNom("article");
%>

      <p>
        <label for="TITRE"><%=module.getMessage("ST_RECHERCHE_FICHE_ARTICLE_TITRE")%></label>
          <%fmt.insererChampSaisie(out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 20); %>
      </p>

      <p>
        <label for="DATE_DEBUT"><%=module.getMessage("ST_RECHERCHE_FICHE_ARTICLE_DATE_DU")%></label>
          <%fmt.insererChampSaisie(out, infoBean, "DATE_DEBUT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10); %>

        <label class="no_float" for="DATE_FIN"><%=module.getMessage("ST_RECHERCHE_FICHE_ARTICLE_AU")%></label>
          <%fmt.insererChampSaisie(out, infoBean, "DATE_FIN", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10); %>
    </p>

      <p>
        <label for="THEMATIQUE"><%=module.getMessage("ST_RECHERCHE_FICHE_ARTICLE_THEMATIQUE")%></label>
          <%fmt.insererComboHashtable(out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_THEMATIQUES"); %>
      </p>

      <% ctx.putData("criteresRechAvancee","TITRE=;DATE_DEBUT=;DATE_FIN=;THEMATIQUE=0000;CODE_RATTACHEMENT=;LIBELLE_CODE_RATTACHEMENT=" + module.getMessage("JTF_CLIQUER_PARCOURIR")); %>
