<%@page import="java.text.DateFormat"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.Formateur"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.univ.objetspartages.om.Article"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.URLResolver"%>
<%@page import="com.univ.utils.UnivWebFmt"%>
<%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
Article fiche= (Article) ctx.getData("fiche");
Objetpartage module = ReferentielObjets.getObjetByNom(ReferentielObjets.getNomObjet(fiche));
DateFormat dateFormatLong = DateFormat.getDateInstance(DateFormat.LONG, ctx.getLocale());
String urlFiche = URLResolver.getAbsoluteUrl(UnivWebFmt.determinerUrlFiche(ctx, fiche), ctx);
if (StringUtils.isNotBlank(urlFiche)) {
    %><a href="<%= urlFiche %>"><%=fiche.getLibelleAffichable()%></a><%
} else {
    %><br/><%=fiche.getLibelleAffichable()%><%
}
if (Formateur.estSaisie(fiche.getDateArticle())) {
    %><br /><%= module.getMessage("ST_ARTICLE_PUBLIE_LE") + " " + dateFormatLong.format(fiche.getDateArticle()) %><%
}
%>