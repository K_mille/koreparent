<%@ page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@ page import="com.kportal.core.config.MessageHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<div id="demande_mot_passe">

  <dl>
      <dt><%=MessageHelper.getCoreMessage("ST_DSI_NOM")%></dt>
      <dd><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "NOM", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_NOM"));%></dd>
  
      <dt><%=MessageHelper.getCoreMessage("ST_DSI_PRENOM")%></dt>
      <dd><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "PRENOM", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_PRENOM"));%></dd>
  
      <dt><%=MessageHelper.getCoreMessage("ST_DSI_LOGIN")%></dt>
     <dd><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LOGIN", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_LOGIN"));%></dd>
  
      <dt><%=MessageHelper.getCoreMessage("ST_DSI_NMDP")%></dt>
      <dd><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MDP", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 64, MessageHelper.getCoreMessage("ST_DSI_NMDP"));%></dd>
  </dl>
  
  <p>
      <a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION_FRONT&amp;ACTION=CONNECTER"><%=MessageHelper.getCoreMessage("ST_SUITE_PRESENTATION_MDP")%></a>
  </p>
  
</div><!-- .demande_mot_passe -->