<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.InfoBean"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page import="com.univ.utils.ContexteUniv"%>
<%@ page import="com.univ.utils.ContexteUtil, com.univ.utils.EscapeString" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<form name="SAISIE" action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">

<% 
ContexteUniv ctx = ContexteUtil.getContexteUniv();
if (infoBean.getMessageErreur().length() > 0) { %>
    <div class="erreur"><%= infoBean.getMessageErreur() %></div><br/>
<% } %>

<h1><%=MessageHelper.getCoreMessage("ST_ENVOI_MAIL_TITRE")%> <%= infoBean.getString("LIBELLE_MAILTO") %></h1>

<!-- Utile pour la restauration des données -->

<% if( infoBean.get("ID_CI_RESTAURATION") != null)    { %>
    <input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
    <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" />
<% }  %>

<!-- le code de l'utilisateur a qui on envoie le mail -->
<input type="hidden" name="MAILTO" value="<%=EscapeString.escapeAttributHtml(infoBean.getString("MAILTO"))%>" />
<% if (infoBean.get("LANGUE") != null) { %>
<input type="hidden" name="LANGUE" value="<%=EscapeString.escapeAttributHtml(infoBean.getString("LANGUE"))%>" />
<% } %>
<input type="hidden" name="ACTION" value="ENREGISTRER" />
<input type="hidden" name="SAISIE_FRONT" value="true" />
<input type="hidden" name="ID_BEAN" value="<%=EscapeString.escapeAttributHtml(infoBean.getString("ID_BEAN"))%>" />
<input type="hidden" name="TYPE" value="<%=EscapeString.escapeAttributHtml(infoBean.getString("TYPE"))%>" />
<input type="hidden" name="<%=InfoBean.PROC%>" value="<%=EscapeString.escapeAttributHtml(infoBean.getNomProcessus())%>" />
<input type="hidden" name="<%=InfoBean.EXT%>" value="<%=EscapeString.escapeAttributHtml(infoBean.getNomExtension())%>" />
<input type="hidden" name="<%=InfoBean.ECRAN_LOGIQUE%>" value="<%=EscapeString.escapeAttributHtml(infoBean.getEcranLogique())%>" />
<input type="hidden" name="<%=InfoBean.ETAT_OBJET%>" value="<%=EscapeString.escapeAttributHtml(infoBean.getEtatObjet())%>" />
<input type="hidden" name="URL_REDIRECT" value="<%=EscapeString.escapeAttributHtml(infoBean.getString("URL_REDIRECT"))%>" /><%
if (infoBean.get("RH") != null) {
    %><input type="hidden" name="RH" value="<%=EscapeString.escapeAttributHtml(infoBean.getString("RH"))%>" /><%
}
%><input type="hidden" name="LOCALE" value="<%=ctx.getLocale()%>" />
<fieldset>
    <p>
        <label for="FROM"><%=MessageHelper.getCoreMessage("ST_ENVOI_MAIL_FROM")%> (*)</label>
        <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "FROM", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255, "De");%>
    </p>
    <p>
        <label for="SUJET"><%=MessageHelper.getCoreMessage("ST_ENVOI_MAIL_OBJET")%> (*)</label>
        <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "SUJET", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 128, "Objet");%>
    </p>
    <p>
        <label for="MESSAGE"><%=MessageHelper.getCoreMessage("ST_ENVOI_MAIL_MESSAGE")%> (*)</label>
        <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MESSAGE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, MessageHelper.getCoreMessage("ST_ENVOI_MAIL_MESSAGE"));%>
    </p>
</fieldset>

<p class="validation">
    <input type="submit" value="<%=MessageHelper.getCoreMessage("ST_ENVOI_MAIL_SUBMIT")%>" />
</p>

</form>