<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<div id="actions">
    <div>
        <ul>
            <li><button type="button" class="enregistrer" onclick="soumettreFormulaire('VALIDER');return false;" value="<%=MessageHelper.getCoreMessage("BO_ENREGISTRER")%>"><%=MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button></li>
        </ul>
        <div class="clearfix"></div>
        <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
    </div>
</div><!-- #actions -->
<br/>
<div id="content">
<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">

<input type="hidden" name="ACTION" value="" />
<% fmt.insererVariablesCachees(out, infoBean); %>

<!-- Utile pour la restauration des données --><% 
if (infoBean.get("ID_CI_RESTAURATION") != null) { 
    %><input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
    <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" /><%
} %>

<fieldset><legend>Informations</legend>
<table class="saisie mise_en_page" role="presentation">
<%
    univFmt.insererChampSaisie(fmt, out, infoBean, "LIBELLE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255,  MessageHelper.getCoreMessage("BO_MODULE_LIBELLE"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "LIBELLE_EXTENSION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255,  MessageHelper.getCoreMessage("BO_MODULE_LIBELLE_EXTENSION"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_CREATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_MODULE_DATE_CREATION"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DATE_MODIFICATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 0, MessageHelper.getCoreMessage("BO_MODULE_DATE_MODIFICATION"));
    univFmt.insererChampSaisie(fmt, out, infoBean, "DESCRIPTION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 512,  MessageHelper.getCoreMessage("BO_MODULE_DESCRIPTION"));
%>
</table>
</fieldset>
<fieldset><legend><%= MessageHelper.getCoreMessage("BO_ETAT") %></legend>
<table class="saisie mise_en_page" role="presentation">
    <tr>
        <td><% fmt.insererChampSaisie(out, infoBean, "ETAT", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_RADIO, 0, 0, ""); %><%= MessageHelper.getCoreMessage("BO_DESACTIVER") %></td>
    </tr>
    <tr>
        <td><% fmt.insererChampSaisie(out, infoBean, "ETAT", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_RADIO, 0, 0, ""); %><%= MessageHelper.getCoreMessage("BO_ACTIVER") %></td>
    </tr>
</table>
</fieldset>

</form>
</div>

<script type="text/javascript">
    function soumettreFormulaire(action) {
        window.document.forms[0].ACTION.value=action;
        window.document.forms[0].submit();
    }
</script>
