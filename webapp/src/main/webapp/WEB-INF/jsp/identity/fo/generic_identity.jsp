<%@ page  import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<p>
    <label for="CIVILITE" class="colonne"><%=MessageHelper.getCoreMessage("FO_IDENTITY_CIVILITE")%></label>
    <% fmt.insererCombo(out, infoBean, "CIVILITE", FormateurJSP.SAISIE_FACULTATIF, "civilite"); %>
</p>

<% univFmt.insererChampSaisie(fmt, out, infoBean, "NOM", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 128, MessageHelper.getCoreMessage("FO_IDENTITY_NOM")); %>
<% univFmt.insererChampSaisie(fmt, out, infoBean, "PRENOM", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 128, MessageHelper.getCoreMessage("FO_IDENTITY_PRENOM")); %>
<% univFmt.insererChampSaisie(fmt, out, infoBean, "ADRESSE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 256, MessageHelper.getCoreMessage("FO_IDENTITY_ADRESSE")); %>
<% univFmt.insererChampSaisie(fmt, out, infoBean, "CODE_POSTAL", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 32, MessageHelper.getCoreMessage("FO_IDENTITY_CODE_POSTAL")); %>
<% univFmt.insererChampSaisie(fmt, out, infoBean, "VILLE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 128, MessageHelper.getCoreMessage("FO_IDENTITY_VILLE")); %>
<% univFmt.insererChampSaisie(fmt, out, infoBean, "PAYS", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 128, MessageHelper.getCoreMessage("FO_IDENTITY_PAYS")); %>
<%-- <% univFmt.insererChampSaisie(fmt, out, infoBean, "TELEPHONE", fmt.SAISIE_OBLIGATOIRE, fmt.FORMAT_TEXTE, 0, 32, MessageHelper.getCoreMessage("FO_IDENTITY_TELEPHONE")); %> --%>
<%-- <% univFmt.insererChampSaisie(fmt, out, infoBean, "EMAIL", fmt.SAISIE_OBLIGATOIRE, fmt.FORMAT_TEXTE, 0, 256, MessageHelper.getCoreMessage("FO_IDENTITY_EMAIL")); %> --%>

<p>
    <label for="TELEPHONE" class="colonne"><%=MessageHelper.getCoreMessage("FO_IDENTITY_TELEPHONE")%> (*)</label>
    <input type="hidden" name="#FORMAT_TELEPHONE" value="2;0;0;12;LIB=<%=MessageHelper.getCoreMessage("FO_IDENTITY_TELEPHONE")%>;7">
    <input class="type_phone" placeholder="(+33)(0)X XX XX XX XX" required="required" type="text" id="TELEPHONE" name="TELEPHONE" value="" maxlength="13" size="22" data-original-title="" title="">
</p>
<p>
    <label for="EMAIL" class="colonne"><%=MessageHelper.getCoreMessage("FO_IDENTITY_EMAIL") %> (*)</label>
    <input type="hidden" name="#FORMAT_EMAIL" value="2;0;0;256;LIB=<%=MessageHelper.getCoreMessage("FO_IDENTITY_EMAIL") %>;8">
    <input class="type_email" required="required" type="text" id="EMAIL" name="EMAIL" value="<%= StringUtils.isNotBlank(infoBean.getString("EMAIL")) ? infoBean.getString("EMAIL") : StringUtils.EMPTY %>" maxlength="256" size="50" data-original-title="" title="">
</p>
