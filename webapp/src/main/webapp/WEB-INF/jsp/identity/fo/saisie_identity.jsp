<%@ page import="org.apache.commons.lang3.StringUtils,com.kportal.extension.ExtensionHelper,com.kportal.ihm.utils.AdminsiteUtils,com.univ.utils.URLResolver" errorPage ="/jsp/jsb_exception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<%@ include file="/jsp/template/initialisations.jsp" %>
<%
final String jspComplementFragment = StringUtils.defaultIfEmpty(infoBean.getString("JSP_COMPLEMENT_FRAGMENT"), "");
titre = AdminsiteUtils.getTitrePageCourante(infoBean);
%>
<div id="content">
    <form class="gestion en-colonne" id="form_saisie_front" action="<%= URLResolver.getAbsoluteUrl("/servlet/com.jsbsoft.jtf.core.SG", ctx) %>" enctype="multipart/form-data" method="post">
        <div class="formulaire_hidden">
            <input type="hidden" name="ACTION" value="ENREGISTRER" />
            <input type="hidden" name="SAISIE_FRONT" value="true" />
            <% fmt.insererVariablesCachees(out, infoBean); %>
        </div>

        <jsp:include page="./generic_identity.jsp"/>

        <%
        if(StringUtils.isNotBlank(jspComplementFragment)){

            final String urlFragment = String.format("%s/%s" , ExtensionHelper.getExtension(infoBean.getNomExtension()).getRelativePath(), jspComplementFragment);
        %>
            <jsp:include page="<%=urlFragment%>"/>
        <%
        }
        %>
        <p id="valider" class="validation">
            <input class="submit" type="submit" name="ENREGISTRER" value="Créer mon compte"/>
        </p>
    </form>
</div>
