<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.univ.objetspartages.om.LigneContenu" %>
<%@ page import="com.univ.objetspartages.om.PageLibre" %>
<%@ page
        import="com.univ.objetspartages.om.ParagrapheBean, com.univ.objetspartages.om.SousParagrapheBean, com.univ.utils.ContexteUniv, com.univ.utils.ContexteUtil, com.univ.utils.UnivWebFmt"
        errorPage="/jsp/jsb_exception.jsp" %>
<%
    /********************************************
    *        Initialisation de la fiche         *
    *********************************************/
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    PageLibre pageLibre = (PageLibre)ctx.getFicheCourante();
    /* Lecture des champs */
    String complements = pageLibre.getFormatedComplements();

    /********************************************
    *            Corps de la fiche         *
    *********************************************/
   /*      Un paragraphe = un champ toolbox dans le formulaire de la page libre (cf l'administration du site)
       Un sous-paragraphe = une subdivision dans un paragraphe, reperée :
        soit par une balise [styleN;],
        soit (si c'est le premier sous paragraphe) par du texte quelconque (on parle alors de sous-paragraphe "sans style")
       En général, il y a entre 1 et N :
           - lignes par page libre
          - paragraphes par ligne
          - sous-paragraphes par paragraphe. */

    /* On récupère la liste des paragraphes de toute la page */
    for (LigneContenu ligneContenu : pageLibre.getLignes()) {
        %><div class="ligne_<%=ligneContenu.getNumLigne()%>"><%
        for (ParagrapheBean paragraphe : ligneContenu.getParagraphes()) {
            %><div class="colonne_<%= paragraphe.getColonne()%>">
                <div class="colonne_deco"><%
                    for (SousParagrapheBean ssParagraphe : paragraphe.getSousParagraphes()) {
                        String contenuTransformer =  UnivWebFmt.transformerTagsPersonnalisation(ctx, ssParagraphe.getContenu());
                        %><div class="paragraphe--<%= ssParagraphe.getStyle() %>"><%
                            if (ssParagraphe.getStyle() != SousParagrapheBean.STYLE_TITRE_INVISIBLE && StringUtils.isNotBlank(ssParagraphe.getTitre())) { %>
                                <h2 class="paragraphe__titre--<%= ssParagraphe.getStyle() %>"><%= ssParagraphe.getTitre() %></h2><%
                            }
                            %><div class="paragraphe__contenu--<%= ssParagraphe.getStyle() %> toolbox">
                                <%= UnivWebFmt.formaterEnHTML(ctx, contenuTransformer) %>
                            </div><!-- .paragraphe__contenu--<%= ssParagraphe.getStyle() %> .toolbox -->
                        </div><!-- paragraphe--<%= ssParagraphe.getStyle() %> --><%
                    }
                %></div><!-- colonne_deco -->
            </div><!-- .colonne_<%= paragraphe.getColonne()%> --><%
        }
        %></div><!-- .ligne_<%=ligneContenu.getNumLigne()%> --><%
    }
    /* affichage des informations complémentaires */
    if (complements.length() > 0) {
        %><div id="complements" class="toolbox">
            <%=complements%>
        </div> <!-- #complements --><%
    }
    /********************************************
   *            Pied de la fiche         *
   *********************************************/
%>
