<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<label for="CODE_PAGE_TETE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_PAGE_TETE") %></label>
<%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_PAGE_TETE", FormateurJSP.SAISIE_FACULTATIF, "", "objet_page_tete", UnivFmt.CONTEXT_ZONE);%>
<p class="retrait">
    <button class="plus" type="button" name="ACTION_AJOUTER" onclick="createHeadPage()"><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CREER_PAGE_TETE") %></button>
</p>

<script type="text/javascript">

    document.getElementById('CODE_PAGE_TETE').setAttribute('data-required',true);
    function createHeadPage(){
        var $ = jQuery,
            url = "/servlet/com.jsbsoft.jtf.core.SG?PROC=TRAITEMENT_PAGELIBRE&ACTION=AJOUTER&LANGUE=0&CODE_RUBRIQUE={0}&TOOLBOX=TRUE";

        var popin = $.iframePopin({
            title : '<%= MessageHelper.getCoreMessage("BO_RUBRIQUE_CREER_PAGE_TETE") %>',
            url : $.parametizeString(url, [$('input[name="CODE"]').val()]),
            autoOpen: true,
            resizable: false,
            width: 700,
            onClose: function($iframe) {
                this.destroy();
            },
            buttons: {},
            onClose: function() {
                this.destroy();
            }
        });
        var registeredId = iFrameHelper.registeriFrame({
            onSendValues: function(object){
                var $component = $('#kMonoSelectCODE_PAGE_TETE'),
                    $input = $('input[name="CODE_PAGE_TETE"]');
                $input.val(object.sCode + ',LANGUE=' + object.langue + ',TYPE=pagelibre');
                $component.kMonoSelect().value('<%= MessageHelper.getCoreMessage("ST_PAGELIBRE") %> : ' + object.titre);
                popin.destroy();
                iFrameHelper.unregisteriFrame(registeredId);
            },
            onAbort: function(){
                popin.destroy();
                iFrameHelper.unregisteriFrame(registeredId);
            },
            iFrame: popin.iFrame,
            caller: this
        });
    }

</script>