<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<% univFmt.insererComboHashtable(fmt, out, infoBean, "CODE_OBJET_LISTE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_OBJET_LISTE", MessageHelper.getCoreMessage("ST_REQUETEUR_OBJET") + " (*)"); %>

<script type="text/javascript">
    document.getElementById('CODE_OBJET_LISTE').setAttribute('data-required',true);
</script>

