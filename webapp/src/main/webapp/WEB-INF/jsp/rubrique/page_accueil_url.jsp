<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<p>
    <label for="URL" class="colonne"><%= MessageHelper.getCoreMessage("RUBRIQUE.PAGE_ACCUEIL_URL") %> (*)</label>
    <%fmt.insererChampSaisie(out, infoBean, "URL", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
</p>

<script type="text/javascript">
    document.getElementById('URL').setAttribute('data-required',true);
</script>