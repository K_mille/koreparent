<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utils" uri="http://kportal.kosmos.fr/tags/utils" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.ApplicationContextManager" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.MenuSautsUtil" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request"/>
<c:set var="menuSautsItems" value="<%= MenuSautsUtil.retournerMenuSauts(infoBean) %>"/>
<c:set var="module" value="<%= StringUtils.isNotBlank(ReferentielObjets.getNomObjet(infoBean.getString(\"CODE_OBJET\")))
    ? ReferentielObjets.getObjetByNom(ReferentielObjets.getNomObjet(infoBean.getString(\"CODE_OBJET\").toUpperCase())).getIdExtension()
    : ApplicationContextManager.DEFAULT_CORE_CONTEXT %>"/>
<c:if test="${not empty menuSautsItems.sousMenu}">
    <nav id="menu_sauts" class="menu_sauts">
        <h3 class="menu_sauts__title">
            <a href="#">
                    ${utils:getMessage(module, menuSautsItems.libelle)}
            </a>
        </h3>
        <ul class="menu_sauts__list">
            <c:forEach items="${menuSautsItems.sousMenu}" var="link">
                <li class="menu_sauts__list__item">
                    <a href="<c:out value="${link.url}" />">${utils:getMessage(module, link.libelle)}</a>
                </li>
                <c:if test="${not empty link.sousMenu}">
                    <ul class="menu_sauts__list__submenu">
                        <c:forEach items="${link.sousMenu}" var="subLink">
                            <li class="menu_sauts__list__item">
                                <a href="<c:out value="${subLink.url}" />">${utils:getMessage(module, subLink.libelle)}</a>
                            </li>
                        </c:forEach>
                    </ul>
                </c:if>
            </c:forEach>
        </ul>
    </nav>
</c:if>
