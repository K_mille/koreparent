<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.url.UrlManager" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.UnivWebFmt" %>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page buffer="100kb" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%  
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    String urlRedirect = "";
    String messageInfo = "";
    if ("1".equals(infoBean.getString("APERCU"))) {
        String urlApercu = UrlManager.calculerUrlApercu(infoBean.getString("ID_METATAG"));
        urlRedirect=URLResolver.getAbsoluteUrl(urlApercu,ctx);
    } else if (StringUtils.isNotBlank(infoBean.getString("URL_REDIRECT"))){
        urlRedirect = UnivWebFmt.determinerUrlRelative(ctx,infoBean.getString("URL_REDIRECT"),true);
    } else if (StringUtils.isNotBlank(infoBean.getString("OBJET")) && StringUtils.isNotBlank(infoBean.getString("CODE"))){
        urlRedirect = UnivWebFmt.determinerUrlFiche(ctx,infoBean.getString("OBJET"),infoBean.getString("CODE"),false);
    } else if (StringUtils.isNotBlank(infoBean.getString("RUBRIQUE"))){
        urlRedirect = UnivWebFmt.renvoyerUrlAccueilRubrique(ctx, infoBean.getString("RUBRIQUE"),false);
        ctx.setCodeRubriquePageCourante( infoBean.getString("RUBRIQUE") );
    } else if (StringUtils.isNotBlank(infoBean.getString("ACCUEIL"))){
        urlRedirect = UnivWebFmt.determinerUrlRelative(ctx,"/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION_FRONT&ACTION=REVENIR_ACCUEIL",true);
    }
    if (urlRedirect.length()>0){
        urlRedirect = URLResolver.getAbsoluteUrl(StringUtils.replace(urlRedirect,"&amp;","&"),ctx);
        if (infoBean.get("MESSAGE_CONFIRMATION") == null && infoBean.get("ETAT_FICHE_ENREGISTREE") == null) {
            response.sendRedirect(urlRedirect);
        } else {
            messageInfo = MessageHelper.getCoreMessage("ST_FRONT_MESSAGE_REDIRECTION_AUTOMATIQUE");
            %>
                <script type="text/javascript">
                    setTimeout("window.location.href='<%=urlRedirect%>'", 4000);
                </script>
            <% }
    } else {
          messageInfo = MessageHelper.getCoreMessage("ST_FRONT_MESSAGE_REDIRECTION_MANUELLE");
    }
%><div class="gestion en-colonne">
    <p id="msg-confirmation"><%
        if (infoBean.get("MESSAGE_CONFIRMATION") != null) {
            %><%= infoBean.getString("MESSAGE_CONFIRMATION") %><%
        } else if ( infoBean.get("ETAT_FICHE_ENREGISTREE")!=null){
            String etat = infoBean.getString("ETAT_FICHE_ENREGISTREE");
            if( etat == null) {
                etat = StringUtils.EMPTY;
            }
            if(etat.equals("0001")) {
                %><%=MessageHelper.getCoreMessage("ST_FRONT_ENBROUILLON")%><%
            } else if (etat.equals("0002")) {
                %><%=MessageHelper.getCoreMessage("ST_FRONT_AVALIDER")%><%
            } else if (etat.equals("0003")) {
                %><%=MessageHelper.getCoreMessage("ST_FRONT_ENLIGNE")%><%
            } else if (etat.equals("0004")) {
                %><%=MessageHelper.getCoreMessage("ST_FRONT_SUPPRIME")%><%
            } else if (etat.equals("0007")) {
                %><%=MessageHelper.getCoreMessage("ST_FRONT_ARCHIVE")%><%
            }
        }
        if (StringUtils.isNotBlank(messageInfo)) {
            %><br/><br/><%= messageInfo %><%
        }
    %></p>
</div> <!-- .gestion .en-colonne -->
