<%@ page isErrorPage="true" %>
<%@ page import="com.kportal.core.config.MessageHelper, com.univ.utils.EscapeString" %>

<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<div class="">
  
  <p id="msg-erreur" class="erreur">
          <%= EscapeString.escapeScriptAndEvent(infoBean.getMessageErreur()) %>
  </p>
  
  <% /* le bouton de retour n'est affiché que si javascript est activé, et que l'historique de navigation n'est pas vide */ %>
    <script type="text/javascript">
    if (history.length > 1)
    {
        document.write('<p><a href=\"#\" onclick=\"history.back(); return false;\"><%=MessageHelper.getCoreMessage("ST_RETOUR")%></a></p>');
    }
    </script>

</div> <!-- -->

