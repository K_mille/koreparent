<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.ContexteUniv"%>
<%@ page import="com.univ.utils.ContexteUtil" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<% ContexteUniv ctx = ContexteUtil.getContexteUniv(); %>

<form id="form_saisie_front" class="edition_fiche" action="/servlet/com.jsbsoft.jtf.core.SG" method="post">
    <div class="formulaire_hidden">
        <%
            fmt.insererVariablesCachees(out, infoBean);
            if (infoBean.get("ESPACE") != null) { %>
        <input type="hidden" name="ESPACE" value="<%=infoBean.get("ESPACE")%>"/>
        <input type="hidden" name="LANGUE" value="<%=ContexteUtil.getContexteUniv().getEspace().getLangue()%>"/>
        <!-- CHARGEMENT MULTIPLES FCKEDITOR SUR UNE MEME PAGE pour LE PROBLEME DE CHARGEMENT INFINI)-->
        <input type="hidden" name="FCK_EDITORS_NAMES" value=""/>
        <% } %>
        <% if (infoBean.get("ID_CI_RESTAURATION") != null) { %>
        <input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>"/>
        <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>"/>
        <% } %>
    </div> <!-- .formulaire_hidden -->
    <% if (StringUtils.isNotEmpty(infoBean.getMessageErreur())) { %>
    <p id="msg-erreur"><%=infoBean.getMessageErreur().toUpperCase()%></p>
    <% } %>

<input type="hidden" name="TYPE_FICHE" value="<%=  infoBean.getString("TYPE_FICHE") %>"/>
<input type="hidden" name="ID_FICHE" value="<%=  infoBean.getString("ID_FICHE") %>"/>
<% if( infoBean.get("RH") != null)    { %>
<input type="hidden" name="RH" value="<%=ctx.getCodeRubriquePageCourante()%>"/>
<%  } %>
<% if( infoBean.get("ESPACE") != null)    { %>
    <input type="hidden" name="ESPACE" value="<%=ctx.getEspaceCourant()%>"/>
<%  } %>
<% if( infoBean.get("URL_REDIRECT") != null)    { %>
<input type="hidden" name="URL_REDIRECT" value="<%=infoBean.getString("URL_REDIRECT")%>"/>
<%  } %>

<p id="msg-alerte"><%=MessageHelper.getCoreMessage("ST_FRONT_MESSAGE_SUPPRESSION")%>&nbsp;<b><%=infoBean.getString("INTITULE")%></b></p>

<p>&nbsp;</p>

<p id="valider-formulaire">
    <input class="submit" type="submit" name="SUPPRIMER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER")%>" />
    <input class="reset" type="button" name="ABANDONNER" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_ABANDONNER")%>" onclick="javascript:history.back();" />
</p>

</form>
