<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.RequeteUtil"%>
<%@page import="com.univ.utils.URLResolver"%>
<%
    String param = (String)request.getAttribute("PARAM");
    String url = RequeteUtil.renvoyerParametre(param, "URL");
    boolean replay = Boolean.parseBoolean(StringUtils.defaultString(RequeteUtil.renvoyerParametre(param, "REPLAY"), "false"));
    long id = System.nanoTime();
%>
<div class="audio-container">
    <audio width="100%" height="30px" style="width:100%;height:30px" controls <%= replay ? "loop" : "" %>>
        <source src="<%=url%>" type="audio/mpeg">
        <object width="100%" height="30px" type="application/x-shockwave-flash" data="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>">
            <param value="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>" name="movie" />
            <param name="flashvars" value="controls=true&amp;isvideo=false&amp;autoplay=false&amp;preload=none&amp;file=<%= url %>" />
            <param value="true" name="allowFullScreen" />
        </object>
    </audio>
</div>
