<%@page import="com.univ.utils.RequeteUtil"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<%@ taglib prefix="kosmosui" uri="kosmosui"%>
<%
    String param = (String)request.getAttribute("PARAM");
    String url = RequeteUtil.renvoyerParametre(param, "URL");
    Boolean pageByPage = "PAGE".equalsIgnoreCase(RequeteUtil.renvoyerParametre(param, "STYLE"));
    String download = RequeteUtil.renvoyerParametre(param, "DOWNLOAD");
%>
<c:set var="url" value="<%= url %>"/>
<c:set var="download" value="<%= download %>"/>
<c:set var="pageByPage" value="<%= pageByPage %>"/>
<div class="pdfviewer-container">
    <kosmosui:pdfviewer id="viewer" documentUrl="${url}" hideDownloadLink="${download ne '1'}" pageByPage="${pageByPage}" hideDownloadName="true" />
    <kosmosui:getScript />
</div>
