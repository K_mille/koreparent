<%@page import="java.util.Enumeration"%>
<%@page import="java.util.Vector"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.rss.RSSBean"%>
<%@page import="com.univ.rss.RSSMediaContentBean"%>
<%@page import="com.univ.rss.RequeteMultiRSS"%>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%
RequeteMultiRSS requete = new RequeteMultiRSS();
ContexteUniv ctx = ContexteUtil.getContexteUniv();
String param = (String)request.getAttribute("PARAM");
Vector<RSSBean> vRss = requete.lancerRequetes(param);
RSSBean rssBean = null;
Enumeration<RSSBean> eRss = vRss.elements();
java.text.DateFormat dateFormatLong = java.text.DateFormat.getDateInstance(java.text.DateFormat.LONG, ctx.getLocale());

if (eRss.hasMoreElements()) {%>
    <ul class="objets fluxrss">

    <%while (eRss.hasMoreElements()) {
        rssBean = eRss.nextElement();%>
        <li class="avec_vignette">

            <div class="vignette_deco">
            <%String br ="";
            for (RSSMediaContentBean rssMediaContent: rssBean.getMediaContentList()){%>
                <%=br%>
                <%if (rssMediaContent.isImage()){
                    %><img src="<%=rssMediaContent.getUrl()%>" width="<%=rssMediaContent.getWidth() != null ? String.valueOf(rssMediaContent.getWidth()) : ""%>"
                            height="<%=rssMediaContent.getHeight() != null ? String.valueOf(rssMediaContent.getHeight()) : ""%>"
                            alt="<%= StringUtils.defaultString(rssMediaContent.getDescription())%>"/>
                <%}else{%>
                    <a href="<%=rssMediaContent.getUrl()%>"/><%=rssMediaContent.getTitre()%></a>
                <%}
                br = "<br />";
            }%>
            </div><!-- .vignette_deco -->

            <div class="vignette_deco2">

            <%if (rssBean.getLink().length() > 0) {%>
                <strong><a href="<%=rssBean.getLink()%>" onclick="window.open(this.href); return false;">
                <%if (rssBean.getTitle().length() > 0) {%>
                    <%=rssBean.getTitle()%>
                <%}
                else {%>
                    <%=rssBean.getLink()%>
                <%}%>
                </a></strong>
            <%}
            else if (rssBean.getTitle().length() > 0) {%>
                <%=rssBean.getTitle()%>
            <%}

            if (rssBean.getDate() != null) {%>
                <div class="date"><%=dateFormatLong.format(rssBean.getDate())%></div><!-- .date -->
            <%}

            if (rssBean.getDescription().length() > 0) {%>
                <div class="description"><%=rssBean.getDescription()%></div><!-- .description -->
            <%}%>

            </div><!-- .vignette_deco2 -->
        </li>
    <%}%>

    </ul>
<%}%>