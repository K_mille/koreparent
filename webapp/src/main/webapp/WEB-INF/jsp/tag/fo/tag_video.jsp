<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.objetspartages.om.SpecificMedia"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.RequeteUtil"%>
<%@ page import="com.univ.utils.URLResolver" %>
<%@ page import="com.univ.mediatheque.Mediatheque" %>
<%
    String param = (String)request.getAttribute("PARAM");
    String extension = RequeteUtil.renvoyerParametre(param, "EXTENSION");
    String url = RequeteUtil.renvoyerParametre(param, "URL");
    String imgUrl = RequeteUtil.renvoyerParametre(param, "IMGURL");
    String width = RequeteUtil.renvoyerParametre(param, "WIDTH");
    String height = RequeteUtil.renvoyerParametre(param, "HEIGHT");
    String isLocal = RequeteUtil.renvoyerParametre(param, "LOCAL");
    SpecificMedia specificMedia;
    // controle du type
    try {
        specificMedia = Mediatheque.getInstance().getRessource("video");
    } catch (final Exception e) {
        specificMedia = Mediatheque.getInstance().getRessource("fichier");
    }
    if((StringUtils.isBlank(isLocal) || StringUtils.isNotBlank(isLocal) && (isLocal.toLowerCase().equals("true") || isLocal.toLowerCase().equals("1"))) || specificMedia.getExtensions().contains("x-flv".equals(extension) ? "flv" : extension)){
    %><div class="video-container" flv_div="true" flv_local="1">
        <video <%if (StringUtils.isNotBlank(imgUrl)){%>poster="<%=imgUrl%>"<%}%> width="640" height="360" style="width:100%;height:100%" controls="controls">
            <source src="<%=url%>" type="video/<%= StringUtils.defaultIfBlank(extension, "flv") %>"/>
            <object style="width:100%;height:100%" type="application/x-shockwave-flash" data="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>">
                <param value="<%= URLResolver.getRessourceUrl("/adminsite/scripts/libs/mediaElement/flashmediaelement.swf", ContexteUtil.getContexteUniv()) %>" name="movie" />
                <param value="controls=true&amp;file=<%=url%>" name="flashvars" />
                <param value="true" name="allowFullScreen" />
            </object>
        </video>
    </div><%
    } else {
    %><div class="video-container">
        <iframe src="<%=url%>" frameborder="0" width="<%=width%>" height="<%=height%>" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    </div><%
    }
%>
