<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<jsp:useBean id="viewDescriptor" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String currentNom = StringUtils.defaultString(viewDescriptor.getData().get("OBJET"));
    Objetpartage currentObject = ReferentielObjets.getObjetByNom(currentNom);
%>
<p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LIST_TYPE.DYNAMIC.TIP") %></p>
<p>
    <label for="OBJET" class="colonne"><%= MessageHelper.getCoreMessage("LIST_TYPE.DYNAMIC.OBJET.LABEL") %></label>
    <select id="OBJET" name="OBJET" class="klist-select js-klist-select"><%
        for (Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            if(ReferentielObjets.gereLienRequete(objet.getCodeObjet())) {
                final String value = String.format("/servlet/com.jsbsoft.jtf.core.SG?EXT=%s&PROC=%s&ACTION=RECHERCHER&TOOLBOX=LIEN_REQUETE&LISTE_INCLUSE=1&FCK_PLUGIN=true", objet.getIdExtension(), objet.getParametreProcessus());
                %><option data-url="<%= value %>" value="<%= objet.getNomObjet() %>" <%= objet.getNomObjet().equals(currentNom) && currentObject != null ? "selected" : StringUtils.EMPTY %>><%= objet.getLibelleAffichable() %></option><%
            }
        }
    %></select>
</p>
<div class="edit-panel"><%
    if(currentObject != null) {
        final String requestData = StringUtils.defaultIfEmpty(viewDescriptor.getData().get("REQUEST_DATA"), StringUtils.EMPTY);
        final String value = String.format("/servlet/com.jsbsoft.jtf.core.SG?EXT=%s&PROC=%s&ACTION=RECHERCHER&TOOLBOX=LIEN_REQUETE&LISTE_INCLUSE=1&FCK_PLUGIN=true%s", currentObject.getIdExtension(), currentObject.getParametreProcessus(), requestData);
        %><iframe class="klist-iframe js-klist-iframe" src="<%= value %>"></iframe><%
    } else {
        %><iframe class="klist-iframe js-klist-iframe" src="/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_PAGELIBRE&ACTION=RECHERCHER&TOOLBOX=LIEN_REQUETE&LISTE_INCLUSE=1&FCK_PLUGIN=true"></iframe><%
    }
%>
</div>
