<%@ page import="java.util.HashMap" %>
<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="viewDescriptor" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
    String action = viewDescriptor.getData().get("ACTION");
%>
<div id="js-lien_fiches">
    <p>
        <label for="libelle" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.LINK_LABEL")%></label>
        <input type="text" name="LIBELLE" id="libelle" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("LIBELLE")) %>" size="20" />
    </p>
    <p>
        <label for="lien_fiches_action" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.ACTION_TYPE") %></label>
        <select name="ACTION" id="lien_fiches_action" >
            <option id="lien_ajout" value="AJOUTER" <%= "AJOUTER".equals(action) || StringUtils.isBlank(action) ? "selected=\"selected\"" : "" %>><%= MessageHelper.getCoreMessage("BO_AJOUT") %></option>
            <option id="lien_modification" value="MODIFIER" <%= "MODIFIER".equals(action) ? "selected=\"selected\"" : "" %>><%= MessageHelper.getCoreMessage("BO_MODIFICATION") %></option>
            <option id="lien_suppression" value="SUPPRIMER" <%= "SUPPRIMER".equals(action) ? "selected=\"selected\"" : "" %>><%= MessageHelper.getCoreMessage("BO_SUPPRESSION") %></option>
            <option id="lien_validation" value="VALIDER" <%= "VALIDER".equals(action) ? "selected=\"selected\"" : "" %>><%= MessageHelper.getCoreMessage("BO_VALIDATION") %></option>
        </select>
    </p>
    <div class="<%= StringUtils.isEmpty(action) || "AJOUTER".equals(action)? "" : "masquer" %> js-lien_ajout">
      <p>
      <label for="OBJET" class="colonne obligatoire"><%= MessageHelper.getCoreMessage("CODE_OBJET")%> (*)</label>
      <%
        Map<String, String> hashOjbets = new HashMap<String, String>();
        for (Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
          if (objet.isStrictlyCollaboratif() || "COMMENTAIRE".equalsIgnoreCase(objet.getNomObjet())){
            continue;
          }
          hashOjbets.put(objet.getNomObjet().toUpperCase(), objet.getLibelleObjet());
        }
        infoBean.set("LISTE_CODE_OBJET", hashOjbets);
        infoBean.set("OBJET", viewDescriptor.getData().get("objet"));
        univFmt.insererContenuComboHashtable( fmt, out, infoBean, "OBJET", FormateurJSP.SAISIE_OBLIGATOIRE,"LISTE_CODE_OBJET", "Objet", "");
      %>
    </p>
    <p class="message information">
      <%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.HELP")%>
    </p>
    <%
        if (viewDescriptor.getData().get("LIBELLE_CODE_RUBRIQUE") != null) {
            infoBean.set("CODE_RUBRIQUE", viewDescriptor.getData().get("CODE_RUBRIQUE"));
            infoBean.set("LIBELLE_CODE_RUBRIQUE", viewDescriptor.getData().get("LIBELLE_CODE_RUBRIQUE"));
        }
        univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE);
    %>
    <p class="retrait">
      <input type="checkbox" id="lien_ajout_code_rubrique_courante" name="CODE_RUBRIQUE_DYNAMIK" value="DYNAMIK" <%= "DYNAMIK".equals(viewDescriptor.getData().get("CODE_RUBRIQUE")) ? "checked" : StringUtils.EMPTY %>/>
      <label for="lien_ajout_code_rubrique_courante"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.SECTION_HELP")%></label>
    </p><%
    if (viewDescriptor.getData().get("LIBELLE_CODE_RATTACHEMENT") != null) {
        infoBean.set("CODE_RATTACHEMENT", viewDescriptor.getData().get("CODE_RATTACHEMENT"));
        infoBean.set("LIBELLE_CODE_RATTACHEMENT", viewDescriptor.getData().get("LIBELLE_CODE_RATTACHEMENT"));
    }
    univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE);
    if (viewDescriptor.getData().get("LIBELLE_DIFFUSION_PUBLIC_VISE") != null) {
        infoBean.set("DIFFUSION_PUBLIC_VISE", viewDescriptor.getData().get("DIFFUSION_PUBLIC_VISE"));
        infoBean.set("LIBELLE_DIFFUSION_PUBLIC_VISE", viewDescriptor.getData().get("LIBELLE_DIFFUSION_PUBLIC_VISE"));
    }
      univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "DIFFUSION_PUBLIC_VISE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_TABLEAU_PUBLIC_VISE"), "TMP_lien_ajout_public_vise", MessageHelper.getCoreMessage("ST_TABLEAU_PUBLIC_VISE"), UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE, "");%>
    <p class="retrait">
      <input type="checkbox" id="lien_ajout_public_vise_utilisateur" name="DIFFUSION_PUBLIC_VISE_DYNAMIK" value="DYNAMIK" <%= "DYNAMIK".equals(viewDescriptor.getData().get("DIFFUSION_PUBLIC_VISE")) ? "checked" : StringUtils.EMPTY %>/>
      <label for="lien_ajout_public_vise_utilisateur"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.GROUP_HELP")%></label>
    </p>
    <p>
      <label for="lien_ajout_css" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.CSS_CLASS") %></label>
      <input type="text" name="CLASSE_CSS" id="lien_ajout_css" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("CLASSE_CSS")) %>" size="10" />
    </p>
  </div>

  <div class="<%= "MODIFIER".equals(action) ? "" : "masquer" %> js-lien_modification">
    <p class="message information">
        <%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.UPDATE") %>
    </p>
    <p>
      <label for="lien_modification_css" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.CSS_CLASS") %></label>
      <input type="text" name="CLASSE_CSS" id="lien_modification_css" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("CLASSE_CSS")) %>" size="10" />
    </p>
  </div>

  <div class="<%= "SUPPRIMER".equals(action) ? "" : "masquer" %> js-lien_suppression">
    <p class="message information">
        <%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.DELETE") %>
    </p>
    <p>
      <label for="lien_suppression_css" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.CSS_CLASS") %></label>
      <input type="text" name="CLASSE_CSS" id="lien_suppression_css" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("CLASSE_CSS")) %>" size="10" />
    </p>
  </div>
  <div class="<%= "VALIDER".equals(action) ? "" : "masquer" %> js-lien_validation">
    <p class="message information">
        <%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.VALIDATE") %>
    </p>
    <p>
        <label for="lien_validation_css" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KCONTRIB.CSS_CLASS") %></label>
        <input type="text" name="CLASSE_CSS" id="lien_validation_css" value="<%= StringUtils.defaultString(viewDescriptor.getData().get("CLASSE_CSS")) %>" size="10" />
    </p>
  </div>
</div>