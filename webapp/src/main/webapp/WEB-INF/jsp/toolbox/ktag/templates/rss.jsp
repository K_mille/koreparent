<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kosmos.toolbox.service.impl.ServiceKTag" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<h3><%= MessageHelper.getCoreMessage("KTAG.KRSS.INSERT")%></h3>
<div id="importRss" class="content_k_link popup-tag"><%
    String currentTagValue = StringUtils.defaultString(infoBean.get(ServiceKTag.TAG_VALUE, String.class));
    currentTagValue = StringUtils.substringBetween(currentTagValue, "[traitement;rss;", "]");
    String nbMax = StringUtils.substringBefore(currentTagValue, " http");
    String rssUrl = StringUtils.defaultString(currentTagValue);
    if (StringUtils.isNotBlank(nbMax)) {
        rssUrl = StringUtils.substringAfter(currentTagValue, " ");
    }
%>
    <p>
      <label for="rss_url" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KRSS.URL")%> (*) </label>
      <input class="type_url" type="text" id="rss_url" name="rss_url" value="<%= rssUrl %>" required/>
    </p>
    <p>
      <label for="rss_nbmax" class="colonne"><%= MessageHelper.getCoreMessage("KTAG.KRSS.MAX")%></label>
      <input type="number" id="rss_nbmax" name="rss_nbmax" value="<%= nbMax %>" />
    </p>
</div><!-- #importRss -->