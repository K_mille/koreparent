<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String value = viewModel.getData().get("NAME");
%>
<div class="js-messages">
    <p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.ANCHOR.TIP") %></p>
</div>
<p class="js-anchor-block">
    <label class="colonne" for="NAME"><%= MessageHelper.getCoreMessage("LINK_TYPE.ANCHOR.LABEL") %></label>
    <select id="NAME" name="NAME" data-value="<%= StringUtils.defaultIfEmpty(value, StringUtils.EMPTY) %>">
    </select>
</p>
