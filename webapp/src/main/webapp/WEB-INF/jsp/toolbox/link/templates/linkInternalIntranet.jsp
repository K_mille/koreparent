<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="viewModel" class="com.kosmos.toolbox.model.TagViewDescriptor" scope="request" /><%
    final String value = viewModel.getData().get("CODE");
%>
<p id="champs_obligatoires" class="message alert alert-info"><%= MessageHelper.getCoreMessage("LINK_TYPE.INTRANET.TIP") %></p>
<p>
    <span><%= MessageHelper.getCoreMessage("LINK_TYPE.INTRANET.TYPE") %></span>
</p>
<ul>
    <li>
        <input type="radio" id="CODE_LOGIN" name="CODE" value="logindsi" <%= "logindsi".equals(value) ? "checked" : StringUtils.EMPTY %>>
        <label for="CODE_LOGIN"><%= MessageHelper.getCoreMessage("LINK_TYPE.INTRANET.LOGIN.LABEL") %></label>
    </li>
    <li>
        <input type="radio" id="CODE_HOME" name="CODE" value="home" <%= "home".equals(value) ? "checked" : StringUtils.EMPTY %>>
        <label for="CODE_HOME"><%= MessageHelper.getCoreMessage("LINK_TYPE.INTRANET.HOME.LABEL") %></label>
    </li>
    <li>
        <input type="radio" id="CODE_PREF" name="CODE" value="prefdsi" <%= "prefdsi".equals(value) ? "checked" : StringUtils.EMPTY %>>
        <label for="CODE_PREF"><%= MessageHelper.getCoreMessage("LINK_TYPE.INTRANET.SETTINGS.LABEL") %></label>
    </li>
    <li>
        <input type="radio" id="CODE_LOGOUT" name="CODE" value="logoutdsi" <%= "logoutdsi".equals(value) ? "checked" : StringUtils.EMPTY %>>
        <label for="CODE_LOGOUT"><%= MessageHelper.getCoreMessage("LINK_TYPE.INTRANET.LOGOUT.LABEL") %></label>
    </li>
</ul>
