<%@ page import="java.text.DateFormat" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kosmos.userfront.bean.ModerationState" %>
<%@ page import="com.kosmos.userfront.bean.UserModerationData" %>
<%@ page import="com.kosmos.userfront.processus.UserFrontProcessus" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.EscapeString" %>
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    %><form id="saisie_objet" action="<%= WebAppUtil.SG_PATH%>" method="post"><%
    fmt.insererVariablesCachees(out, infoBean);
    UserModerationData userModerationData = infoBean.get(UserFrontProcessus.INFOBEAN_REGISTRATION,UserModerationData.class);
    DateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss",ContexteUtil.getContexteUniv().getLocale());
    %><div id="actions">
        <div><%
            if (ModerationState.NOT_MODERATED.equals(userModerationData.getModerationState())) { %>
                <ul>
                    <li><button type="submit" class="valider" name="user_moderation_data.moderation_state" value="<%= ModerationState.VALIDATED%>" ><%= MessageHelper.getCoreMessage("JTF_BOUTON_VALIDER") %></button></li>
                    <li><button type="button" class="js-refusal-user refuser" name="user_moderation_data.moderation_state" value="<%= ModerationState.REFUSED%>" ><%= MessageHelper.getCoreMessage("BO_FICHE_REFUSER") %></button>
                        <div class="js-refusal-user__form dialogRefuser" title="<%= MessageHelper.getCoreMessage("BO_FICHE_REFUSER_PUBLICATION") %>" style="display: none">
                            <label for="MOTIF_RETOUR" ><%= MessageHelper.getCoreMessage("BO_FICHE_MOTIF_REFUS") %></label>
                            <textarea form="saisie_objet" name="user_moderation_data.moderation_message" id="MOTIF_RETOUR" cols="35" rows="5"></textarea>
                        </div>
                    </li>
                </ul><%
            }
            %><div class="clearfix"></div>
            <span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <div id="content">
        <input type="hidden" name="<%= UserFrontProcessus.REGISTRATION_ID%>" value="<%= infoBean.getString(UserFrontProcessus.REGISTRATION_ID)%>" />
        <input type="hidden" name="user_moderation_data.id" value="<%= userModerationData.getId()%>" />
        <input type="hidden" name="user_moderation_data.creation_date" value="<%= format.format(userModerationData.getCreationDate())%>" />
        <input type="hidden" name="user_moderation_data.login" value="<%= userModerationData.getLogin()%>" />
        <input type="hidden" name="user_moderation_data.email" value="<%= userModerationData.getEmail()%>" />
        <input type="hidden" name="user_moderation_data.first_name" value="<%= userModerationData.getLogin()%>" />
        <input type="hidden" name="user_moderation_data.last_name" value="<%= userModerationData.getLogin()%>" />
        <input type="hidden" name="user_moderation_data.parent_id_registration" value="<%= userModerationData.getParentIdRegistration()%>" />
        <input type="hidden" name="user_moderation_data.alias_site" value="<%= userModerationData.getAliasSite()%>" />
        <input type="hidden" name="user_moderation_data.id_locale_kportal" value="<%= userModerationData.getIdLocaleKportal()%>" />
        <input type="hidden" name="<%= UserFrontProcessus.INFOBEAN_ID_MODERATION_DATA%>" value="<%= userModerationData.getId()%>"><%
        %><div class="userfront__tech-info--firstBlock">
            <h3><%= MessageHelper.getCoreMessage("BO.USERFRONT.TECH_INFOS")%></h3>
            <dl>
                <dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("BO.USERFRONT.DATE")%></dt>
                <dd><%= format.format(userModerationData.getCreationDate())%></dd>
                <dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("BO_ETAT")%></dt>
                <dd><%= MessageHelper.getCoreMessage("BO.USERFRONT." + userModerationData.getModerationState().name()) %></dd><%
                if (ModerationState.REFUSED.equals(userModerationData.getModerationState())) {
                    %><dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("ST_FRONT_MOTIF_REFUS")%></dt>
                    <dd><%= userModerationData.getModerationMessage() %></dd><%
                }
                %>
            </dl>
        </div>
        <div>
            <h3><%= MessageHelper.getCoreMessage("BO.USERFRONT.USER_INFOS")%></h3>
            <dl>
                <dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("BO_LOGIN")%></dt>
                <dd><%= EscapeString.escapeHtml(userModerationData.getLogin()) %></dd>
                <dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_ADRESSE_MEL")%></dt>
                <dd><%= EscapeString.escapeHtml(userModerationData.getEmail()) %></dd>
                <dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_PRENOM")%></dt>
                <dd><%= EscapeString.escapeHtml(StringUtils.defaultString(userModerationData.getFirstName())) %></dd>
                <dt class="label colonne non-cliquable"><%= MessageHelper.getCoreMessage("BO_UTILISATEUR_NOM")%></dt>
                <dd><%= EscapeString.escapeHtml(StringUtils.defaultString(userModerationData.getLastName())) %></dd>
            </dl>
        </div>
    </div>
</form>