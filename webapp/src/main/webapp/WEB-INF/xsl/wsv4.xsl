<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="2.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

	<xsl:template match="/LISTE_ITEMS">
		<ROOT><xsl:apply-templates select="ITEM"/></ROOT>
	</xsl:template>

	<xsl:template match="ITEM">
		 <xsl:element name="{ENTETE/OBJET}">
		 	<CODE><xsl:value-of select="ENTETE/CODE"/></CODE>
		 	<LANGUE><xsl:value-of select="ENTETE/LANGUE"/></LANGUE>
			<xsl:copy-of select="DONNEES/*" />
		 </xsl:element>
	</xsl:template>
	

</xsl:stylesheet>