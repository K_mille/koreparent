(function () {
    'use strict';
    CKEDITOR.plugins.kaudio = {
        getSelectedAudio: function (editor, element) {
            if (!element) {
                var sel = editor.getSelection();
                element = sel.getSelectedElement();
            }
            if (element && element.hasClass('cke_kaudio') && !element.isReadOnly()) {
                return editor.restoreRealElement(element);
            }
        },
        getEditUrl: function (editor) {
            var url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=AUDIO&FCK_PLUGIN=TRUE',
                processConfiguration = CKEDITOR.plugins.kmedia.getProcessConfiguration(),
                selectedAudio = CKEDITOR.plugins.kaudio.getSelectedAudio(editor);
            if (processConfiguration.code && processConfiguration.objet && processConfiguration.idFiche) {
                url += '&OBJET=' + processConfiguration.objet;
                url += '&CODE=' + processConfiguration.code;
                url += '&ID_FICHE=' + processConfiguration.idFiche;
                url += '&MODE_ONGLETS=' + (selectedAudio ? 'insertion' : 'selection');
                if (selectedAudio && selectedAudio.$.innerHTML.indexOf('[id-image]') !== -1 && selectedAudio.$.innerHTML.indexOf('[/id-image]') !== -1) {
                    url += '&ID_MEDIA=' + selectedAudio.$.innerHTML.split('[id-image]')[1].split('[/id-image]')[0];
                }
            }
            return url;
        },
        createFakeAudio: function (editor, element) {
            return editor.createFakeElement(element, 'cke_kaudio', 'span');
        }
    };
    // kAudio plugin
    CKEDITOR.plugins.add(
        'kaudio', {
            requires: ['kiframedialog', 'kmedia'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('kaudio', new CKEDITOR.dialogCommand('kaudio'));
                editor.ui.addButton(
                    'kAudio', {
                        label: LOCALE_BO.ckeditor.plugins.kaudio.title,
                        command: 'kaudio',
                        icon: this.path + 'icons/kAudio.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup( 'kaudio' );
                    editor.addMenuItem(
                        'kaudio', {
                            label: LOCALE_BO.ckeditor.plugins.kaudio.menu,
                            command: 'kaudio',
                            group: 'kaudio',
                            icon: this.path + 'icons/kAudio.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (CKEDITOR.plugins.kaudio.getSelectedAudio(editor, element)) {
                            return { 'kaudio': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (element.hasClass('cke_kaudio') && !element.isReadOnly()) {
                            evt.data.dialog = 'kaudio';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'kaudio', LOCALE_BO.ckeditor.plugins.kaudio.title,
                    CKEDITOR.plugins.kaudio.getEditUrl,
                    940, 555,
                    function () {
                    }, {
                        onOk: function (e) {
                            var dialogEditor = this._.editor,
                                iframeDocument = e.sender.iframeDom.contentWindow.document,
                                element = new CKEDITOR.dom.element('span'),
                                range = dialogEditor.getSelection().getRanges()[0];
                            element.addClass('kaudio');
                            element.setHtml(iframeDocument.querySelector('input[name="TAG"]').value);
                            dialogEditor.insertElement(CKEDITOR.plugins.kaudio.createFakeAudio(dialogEditor, element));
                            range.select();
                        },
                        onHide: function (e) {
                            var iframeDocument = e.sender.iframeDom.contentWindow.document,
                                iframeMejs = e.sender.iframeDom.contentWindow.mejs,
                                container = iframeDocument.querySelector('.mejs-container');
                            if (container) {
                                iframeMejs.players[container.id].pause();
                            }
                        }
                    });
            },
            afterInit: function (editor) {
                // Empty anchors upcasting to fake objects.
                editor.dataProcessor.dataFilter.addRules(
                    {
                        elements: {
                            span: function (element) {
                                if (element.hasClass('kaudio')) {
                                    return editor.createFakeParserElement(element, 'cke_kaudio', 'kaudio');
                                }
                                return null;
                            }
                        }
                    });
                var pathFilters = editor._.elementsPath && editor._.elementsPath.filters;
                if (pathFilters) {
                    pathFilters.push(
                        function (element, name) {
                            if (name === 'span') {
                                if (element.hasClass('kaudio')) {
                                    return 'kaudio';
                                }
                            }
                        });
                }
            }
        });
})();
