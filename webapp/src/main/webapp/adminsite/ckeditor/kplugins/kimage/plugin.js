(function () {
    'use strict';
    CKEDITOR.plugins.kimage = {
        getSelectedImage: function (editor, element) {
            if (!element) {
                var sel = editor.getSelection();
                element = sel.getSelectedElement();
            }
            if (element && element.is('img') && !element.data('cke-realelement') && !element.isReadOnly()) {
                return element;
            }
        },
        getEditUrl: function (editor) {
            var url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=PHOTO&FCK_PLUGIN=TRUE',
                processConfiguration = CKEDITOR.plugins.kmedia.getProcessConfiguration(),
                selectedImage = CKEDITOR.plugins.kimage.getSelectedImage(editor),
                imageProperties = CKEDITOR.plugins.kimage.getImageProperties(selectedImage);
            if (processConfiguration.code && processConfiguration.objet && processConfiguration.idFiche) {
                url += '&OBJET=' + processConfiguration.objet;
                url += '&CODE=' + processConfiguration.code;
                url += '&ID_FICHE=' + processConfiguration.idFiche;
                url += '&MODE_ONGLETS=' + (selectedImage ? 'insertion' : 'selection');
                if (selectedImage && selectedImage.$.src.indexOf('[id-image]') !== -1 && selectedImage.$.src.indexOf('[/id-image]') !== -1) {
                    url += '&ID_MEDIA=' + selectedImage.$.src.split('[id-image]')[1].split('[/id-image]')[0];
                }
            }
            for (var property in imageProperties) {
                if (imageProperties.hasOwnProperty(property)) {
                    url += '&' + property + '=' + imageProperties[property];
                }
            }
            return url;
        },
        getImageProperties: function (element) {
            var props = {};
            if (element) {
                props.URL = element.getAttribute('src');
                props.ALT = element.getAttribute('alt');
                props.WIDTH = element.getStyle('width').replace(/[a-zA-Z]/g, '');
                props.HEIGHT = element.getStyle('height').replace(/[a-zA-Z]/g, '');
                props.NATURAL_WIDTH = element.$.naturalWidth;
                props.NATURAL_HEIGHT = element.$.naturalHeight;
                props.HORIZONTAL_MARGIN = element.getStyle('margin-left').replace(/[a-zA-Z]/g, '');
                props.VERTICAL_MARGIN = element.getStyle('margin-top').replace(/[a-zA-Z]/g, '');
                props.BORDER = element.getStyle('border-width').replace(/[a-zA-Z]/g, '');
                props.ALIGN = element.getStyle('float');
            }
            return props;
        }
    };
    // kImage plugin
    CKEDITOR.plugins.add(
        'kimage', {
            requires: ['kiframedialog', 'kmedia'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('kimage', new CKEDITOR.dialogCommand('kImage'));
                editor.ui.addButton(
                    'kImage', {
                        label: LOCALE_BO.ckeditor.plugins.kimage.title,
                        command: 'kimage',
                        icon: this.path + 'icons/kImage.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup('kimage');
                    editor.addMenuItem(
                        'kimage', {
                            label: LOCALE_BO.ckeditor.plugins.kimage.menu,
                            command: 'kimage',
                            group: 'kimage',
                            icon: this.path + 'icons/kImage.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (CKEDITOR.plugins.kimage.getSelectedImage(editor, element)) {
                            return {'kimage': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (element.is('img') && !element.data('cke-realelement') && !element.isReadOnly()) {
                            evt.data.dialog = 'kImage';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'kImage', LOCALE_BO.ckeditor.plugins.kimage.title,
                    CKEDITOR.plugins.kimage.getEditUrl,
                    940, 555,
                    function () {
                    }, {
                        onOk: function (e) {
                            var dialogEditor = this._.editor,
                                iframeDocument = e.sender.iframeDom.contentWindow.document,
                                element = new CKEDITOR.dom.element(iframeDocument.querySelector('.js-kimage__preview > img'));
                            dialogEditor.insertElement(element);
                            dialogEditor.getSelection().selectElement(element);
                        }
                    });
            }
        });
})();
