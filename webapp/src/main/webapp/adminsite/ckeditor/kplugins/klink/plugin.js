﻿(function () {
    'use strict';
    var url = window.location.href,
        splittedUrl = url.split('/'),
        domain = splittedUrl[0] + '//' + splittedUrl[2];
    var messageListener = function messageListener(event) {
        var currentDialog = CKEDITOR.dialog.getCurrent();
        var currentOkButton = currentDialog.getButton('ok');
        var dataObject = event.data;
        if (event.origin !== domain || !dataObject.hasOwnProperty('href') || !dataObject.hasOwnProperty('text')) {
            currentOkButton.disable();
            return;
        }
        document.getElementById(currentDialog.iframeDom.id).setAttribute('data-href', dataObject.href);
        document.getElementById(currentDialog.iframeDom.id).setAttribute('data-text', dataObject.text);
        if (dataObject.hasOwnProperty('attributes') && dataObject.attributes) {
            document.getElementById(currentDialog.iframeDom.id).setAttribute('data-attributes', dataObject.attributes);
        }
        currentOkButton.enable();
    };
    function getLinkLabel(editor, link) {
        var text = link ? link.getText() : '';
        if (!text) {
            text = link && link.getAttribute('title') ? link.getAttribute('title') : editor.getSelection().getSelectedText();
        }
        return text;
    }
    function isComplexSelection(editor) {
        var selection = editor.getSelectedHtml().$;
        var hasOnlyText = [].some.call(selection.childNodes, function (node) {
            return node.nodeType === 3; // Type text
        });
        if(hasOnlyText) {
            selection.normalize();
        }
        if(selection) {
            return selection.childNodes.length > 1 || (selection.childNodes.length === 1 && !selection.textContent);
        }
        return false;
    }
    CKEDITOR.plugins.add(
        'klink', {
            requires: ['kiframedialog'],
            icons: 'kLink,kAnchor',
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('kLink', new CKEDITOR.dialogCommand('kLink'));
                editor.addCommand('kAnchor', new CKEDITOR.dialogCommand('kAnchor'));
                editor.ui.addButton(
                    'kLink', {
                        label: LOCALE_BO.ckeditor.plugins.link.title,
                        command: 'kLink'
                    });
                editor.ui.addButton(
                    'kAnchor', {
                        label: editor.lang.link.anchor.toolbar,
                        command: 'kAnchor',
                        toolbar: 'links,30'
                    });
                if (editor.addMenuItems) {
                    editor.addMenuItems(
                        {
                            link: {
                                label: editor.lang.link.menu,
                                command: 'kLink',
                                group: 'link',
                                order: 1
                            }
                        });
                }
                // Ctrl + L
                editor.setKeystroke(CKEDITOR.CTRL + 76, 'kLink');
                editor.on(
                    'doubleclick', function (evt) {
                        var element = CKEDITOR.plugins.link.getSelectedLink(editor) || evt.data.element;
                        if (!element.isReadOnly()) {
                            if (element.is('a')) {
                                evt.data.dialog = ( element.getAttribute('name') && ( !element.getAttribute('href') || !element.getChildCount() ) ) ? 'kAnchor' : 'kLink';
                                // Pass the link to be selected along with event data.
                                evt.data.link = element;
                            } else if (CKEDITOR.plugins.link.tryRestoreFakeAnchor(editor, element)) {
                                evt.data.dialog = 'kAnchor';
                            }
                        }
                    }, null, null, 0);
                CKEDITOR.dialog.add('kAnchor', this.path + 'dialogs/kanchor.js');
                CKEDITOR.dialog.addKIframe(
                    'kLink', LOCALE_BO.ckeditor.plugins.link.title,
                    function (dialogEditor) {
                        var iFrameUrl = '/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_TAG_TOOLBOX&idTag=linkTypeService',
                            selectedLink = CKEDITOR.plugins.link.getSelectedLink(dialogEditor),
                            text = getLinkLabel(dialogEditor, selectedLink);
                        if (text) {
                            iFrameUrl += '&LINK_LABEL=' + encodeURIComponent(text);
                        }
                        if (selectedLink && selectedLink.getAttribute('href')) {
                            iFrameUrl += '&FORMATTER_VALUE=' + encodeURIComponent(selectedLink.getAttribute('href'));
                        }
                        iFrameUrl += '&LINK_TEXT_MANDATORY=' + !isComplexSelection(dialogEditor);
                        return iFrameUrl;
                    },
                    940, 555,
                    function () {
                    }, {
                        onShow: function () {
                            window.addEventListener('message', messageListener, false);
                            this.getButton('ok').disable();
                            var editor = this.getParentEditor(),
                                selection = editor.getSelection(),
                                element = null;

                            // Fill in all the relevant fields if there's already one link selected.
                            if ( ( element = CKEDITOR.plugins.link.getSelectedLink( editor ) ) && element.hasAttribute( 'href' ) ) {
                                // Don't change selection if some element is already selected.
                                // For example - don't destroy fake selection.
                                if ( !selection.getSelectedElement() )
                                    selection.selectElement( element );
                            } else {
                                element = null;
                            }
                            // Record down the selected element in the dialog.
                            this._.selectedElement = element;
                        },
                        onOk: function () {
                            var dialogEditor = this.getParentEditor(),
                                selection = dialogEditor.getSelection(),
                                attributes = JSON.parse(this.iframeDom.getAttribute('data-attributes')) || {};
                            attributes.href = attributes['data-cke-saved-href'] = this.iframeDom.getAttribute('data-href');
                            attributes.title = this.iframeDom.getAttribute('data-text');

                            if ( !this._.selectedElement ) {
                                var range = selection.getRanges()[ 0 ];
                                // Use link URL as text with a collapsed cursor.
                                if ( range.collapsed ) {
                                    var textDom = new CKEDITOR.dom.text( attributes.title, dialogEditor.document );
                                    range.insertNode(textDom);
                                    range.selectNodeContents(textDom);
                                }
                                // Apply style.
                                var style = new CKEDITOR.style( {
                                    element: 'a',
                                    attributes: attributes
                                } );
                                style.type = CKEDITOR.STYLE_INLINE; // need to override... dunno why.
                                style.applyToRange( range, dialogEditor );
                                range.select();
                            } else {
                                // We're only editing an existing link, so just overwrite the attributes.
                                var element = this._.selectedElement,
                                    href = element.data( 'cke-saved-href' );
                                element.setAttributes( attributes);
                                if (element.getText()) {
                                    element.setText(attributes.title);
                                }
                                delete this._.selectedElement;
                            }
                        }
                    });
            }
        });
})();
