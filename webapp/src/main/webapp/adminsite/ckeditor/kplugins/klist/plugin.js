﻿(function () {
    'use strict';
    CKEDITOR.plugins.klist = {
        createFakeImage: function (editor, element, query) {
            editor.lang.fakeobjects.span = query;
            return editor.createFakeElement(element, 'cke_klist', 'span');
        },
        getSelectedList: function (editor, element) {
            if (!element) {
                var sel = editor.getSelection();
                element = sel.getSelectedElement();
            }
            if (element && element.hasClass('cke_klist') && !element.isReadOnly()) {
                return editor.restoreRealElement(element);
            }
        },
        getEditUrl: function (editor) {
            var editUrl = '/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_TAG_TOOLBOX&idTag=klist',
                selectedList = CKEDITOR.plugins.klist.getSelectedList(editor);
            if (selectedList && selectedList.$.innerHTML) {
                editUrl += '&FORMATTER_VALUE=' + encodeURIComponent(selectedList.$.innerHTML);
            }
            return editUrl;
        },
        messageListener: function messageListener(event) {
            var url = window.location.href,
                splittedUrl = url.split('/'),
                domain = splittedUrl[0] + '//' + splittedUrl[2],
                currentDialog = CKEDITOR.dialog.getCurrent(),
                currentOkButton = currentDialog.getButton('ok');
            if (event.origin !== domain || !event.data.hasOwnProperty('tag') || event.data.tag !== 'klist' || !event.data.hasOwnProperty('value')) {
                currentOkButton.disable();
                return;
            }
            document.getElementById(currentDialog.iframeDom.id).setAttribute('data-tagValue', event.data.value);
            currentOkButton.enable();
        }
    };
    CKEDITOR.plugins.add(
        'klist', {
            requires: ['kiframedialog'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('klist', new CKEDITOR.dialogCommand('klist'));
                editor.ui.addButton(
                    'klist', {
                        label: LOCALE_BO.ckeditor.plugins.listeFiche.title,
                        command: 'klist',
                        icon: this.path + 'icons/klist.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup('klist');
                    editor.addMenuItem(
                        'klist', {
                            label: LOCALE_BO.ckeditor.plugins.listeFiche.title,
                            command: 'klist',
                            group: 'klist',
                            icon: this.path + 'icons/klist.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (element.hasClass('cke_klist') && !element.isReadOnly()) {
                            return {'klist': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (CKEDITOR.plugins.klist.getSelectedList(editor, element)) {
                            evt.data.dialog = 'klist';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'klist', LOCALE_BO.ckeditor.plugins.listeFiche.insert,
                    CKEDITOR.plugins.klist.getEditUrl,
                    940, 600,
                    function () {
                        window.addEventListener('message', CKEDITOR.plugins.klist.messageListener, false);
                    }, {
                        onOk: function () {
                            var element = new CKEDITOR.dom.element('span'),
                                query = this.iframeDom.getAttribute('data-tagValue');
                            element.addClass('klist');
                            element.setHtml(query);
                            var fakeImage = CKEDITOR.plugins.klist.createFakeImage(this._.editor, element, query);
                            this._.editor.insertElement(fakeImage);
                            window.removeEventListener('message', CKEDITOR.plugins.klist.messageListener, false);
                        }
                    });
            },
            afterInit: function (editor) {
                // Empty anchors upcasting to fake objects.
                editor.dataProcessor.dataFilter.addRules(
                    {
                        elements: {
                            span: function (element) {
                                if (element.hasClass('klist')) {
                                    return editor.createFakeParserElement(element, 'cke_klist', 'klist');
                                }
                                return null;
                            }
                        }
                    });
                var pathFilters = editor._.elementsPath && editor._.elementsPath.filters;
                if (pathFilters) {
                    pathFilters.push(
                        function (element, name) {
                            if (name === 'span' && element.hasClass('klist')) {
                                return 'klist';
                            }
                        });
                }
            }
        });
})();
