﻿(function() {
    'use strict';

    CKEDITOR.plugins.kmedia = {
        // [UGLY] DELETE ONCE MEDIAS PROCESSES REVIEWED !!!
        getProcessConfiguration: function () {
            var processConfiguration = {};
            if (document.querySelector('input[type="hidden"][name=EXT]')) {
                processConfiguration.ext = document.querySelector('input[type="hidden"][name=EXT]').value;
            }
            if (document.querySelector('input[type="hidden"][name=PROC]')) {
                processConfiguration.proc = document.querySelector('input[type="hidden"][name=PROC]').value;
            }
            if (document.querySelector('input[type="hidden"][name=TS_CODE]')) {
                processConfiguration.code = document.querySelector('input[type="hidden"][name=TS_CODE]').value;
            }
            if (document.querySelector('input[type="hidden"][name=OBJET]')) {
                processConfiguration.objet = document.querySelector('input[type="hidden"][name=OBJET]').value;
            }
            if (document.querySelector('input[type="hidden"][name=LOCALE]')) {
                processConfiguration.locale = document.querySelector('input[type="hidden"][name=LOCALE]').value;
            }
            if (document.querySelector('input[type="hidden"][name=ID_FICHE]')) {
                processConfiguration.idFiche = document.querySelector('input[type="hidden"][name=ID_FICHE]').value;
            }
            return processConfiguration;
        }
    };

    // kMedia plugin
    CKEDITOR.plugins.add(
        'kmedia', {
            init: function () {
            }
        });
})();
