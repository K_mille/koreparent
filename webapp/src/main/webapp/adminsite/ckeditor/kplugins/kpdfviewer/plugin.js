(function () {
    'use strict';
    CKEDITOR.plugins.kpdfviewer = {
        getSelectedPdf: function (editor, element) {
            var currentElement = element;
            if (!currentElement) {
                var sel = editor.getSelection();
                currentElement = sel.getSelectedElement();
            }
            if (currentElement && currentElement.hasClass('cke_kpdfviewer') && !currentElement.isReadOnly()) {
                return editor.restoreRealElement(currentElement);
            }
        },
        getEditUrl: function (editor) {
            var url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&' +
                'TYPE_RESSOURCE=FICHIER&FCK_PLUGIN=TRUE&PDFVIEWER=TRUE',
                processConfiguration = CKEDITOR.plugins.kmedia.getProcessConfiguration(),
                selectedPdf = CKEDITOR.plugins.kpdfviewer.getSelectedPdf(editor),
                pdfProperties = CKEDITOR.plugins.kpdfviewer.getPdfProperties(selectedPdf);
            if (processConfiguration.code && processConfiguration.objet && processConfiguration.idFiche) {
                url += '&OBJET=' + processConfiguration.objet;
                url += '&CODE=' + processConfiguration.code;
                url += '&ID_FICHE=' + processConfiguration.idFiche;
                url += '&MODE_ONGLETS=' + (selectedPdf ? 'insertion' : 'selection');
            }
            for (var property in pdfProperties) {
                if (pdfProperties.hasOwnProperty(property)) {
                    url += '&' + property + '=' + pdfProperties[property];
                }
            }
            return url;
        },
        createFakePdfviewer: function (editor, element) {
            return editor.createFakeElement(element, 'cke_kpdfviewer', 'span');
        },
        getPdfProperties: function (element) {
            var props = {};
            if (element) {
                props.URL = element.getAttribute('src');
                var htmlContent = element.$.innerHTML;
                if (htmlContent.indexOf('[id-image]') !== -1 &&
                    htmlContent.indexOf('[/id-image]') !== -1) {
                    props.ID_MEDIA = htmlContent.split('[id-image]')[1].split('[/id-image]')[0];
                }
                if (htmlContent.indexOf('DOWNLOAD=')) {
                    var dllength = + 'DOWNLOAD='.length ;
                    props.DOWNLOAD = htmlContent.slice(htmlContent.indexOf('DOWNLOAD=') + dllength,
                        htmlContent.indexOf('DOWNLOAD=') + dllength + 1);
                }
                if (htmlContent.indexOf('STYLE=')) {
                    props.STYLE = htmlContent.slice(htmlContent.indexOf('STYLE=') + 'STYLE='.length,
                        htmlContent.indexOf('media;pdfviewer]'));
                }
            }
            return props;
        }
    };
    // kpdfviewer plugin
    CKEDITOR.plugins.add(
        'kpdfviewer', {
            requires: ['kiframedialog', 'kmedia'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('kpdfviewer', new CKEDITOR.dialogCommand('kpdfviewer'));
                editor.ui.addButton(
                    'kpdfviewer', {
                        label: LOCALE_BO.ckeditor.plugins.kpdfviewer.title,
                        command: 'kpdfviewer',
                        icon: this.path + 'icons/kpdfviewer.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup('kpdfviewer');
                    editor.addMenuItem(
                        'kpdfviewer', {
                            label: LOCALE_BO.ckeditor.plugins.kpdfviewer.menu,
                            command: 'kpdfviewer',
                            group: 'kpdfviewer',
                            icon: this.path + 'icons/kpdfviewer.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (CKEDITOR.plugins.kpdfviewer.getSelectedPdf(editor, element)) {
                            return {'kpdfviewer': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (element.hasClass('cke_kpdfviewer') && !element.isReadOnly()) {
                            evt.data.dialog = 'kpdfviewer';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'kpdfviewer', LOCALE_BO.ckeditor.plugins.kpdfviewer.title,
                    CKEDITOR.plugins.kpdfviewer.getEditUrl,
                    940, 555,
                    function () {
                    }, {
                        onOk: function (e) {
                            var dialogEditor = this._.editor,
                                iframeDocument = e.sender.iframeDom.contentWindow.document,
                                element = new CKEDITOR.dom.element('span'),
                                range = dialogEditor.getSelection().getRanges()[0];
                            element.addClass('kpdfviewer');
                            element.setHtml(iframeDocument.querySelector('input[name="TAG"]').value);
                            dialogEditor.insertElement(CKEDITOR.plugins.kpdfviewer.createFakePdfviewer(dialogEditor, element));
                            range.select();
                        }
                    });
            },
            afterInit: function (editor) {
                // Empty anchors upcasting to fake objects.
                editor.dataProcessor.dataFilter.addRules(
                    {
                        elements: {
                            span: function (element) {
                                if (element.hasClass('kpdfviewer')) {
                                    return editor.createFakeParserElement(element, 'cke_kpdfviewer', 'kpdfviewer');
                                }
                                return null;
                            }
                        }
                    });
            }
        });
})();
