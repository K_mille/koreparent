(function () {
    'use strict';
    CKEDITOR.plugins.ktag = {
        getSelectedTag: function (editor, element) {
            if (!element) {
                var sel = editor.getSelection();
                element = sel.getSelectedElement();
            }
            if (element && element.hasClass('cke_ktag') && !element.isReadOnly()) {
                return editor.restoreRealElement(element);
            }
        },
        getFormatterValue: function (editor) {
            var selection = editor.getSelection(),
                formatterValue;
            if (selection && selection.getSelectedElement()) {
                formatterValue = editor.restoreRealElement(selection.getSelectedElement()).getText();
            }
            return formatterValue;
        },
        createFakeImage: function (editor, element, query, tagType) {
            editor.lang.fakeobjects.span = query;
            return editor.createFakeElement(element, 'cke_ktag cke_' + tagType, 'span');
        }
    };
    // Retro-compatibility layer
    CKEDITOR.plugins.ktag.oldMode = {
        isKtag: function (object) {
            for (var propertyName in object) {
                if (object.hasOwnProperty(propertyName)) {
                    if (propertyName.indexOf('ktag') !== -1 || propertyName.indexOf('kagenda') !== -1 ||
                        propertyName.indexOf('krss') !== -1 || propertyName.indexOf('ksite') !== -1 ||
                        propertyName.indexOf('kgalerie') !== -1) {
                        return true;
                    }
                }
            }
            return false;
        },
        analyseContent: function (tagValue) {
            if (tagValue) {
                if (tagValue.indexOf('[service') !== -1) {
                    return 'cke_kservice';
                } else if (tagValue.indexOf('groupe]') !== -1 || tagValue.indexOf('debutgroupe;') !== -1) {
                    return 'cke_kgroup';
                } else if (tagValue.indexOf('rss;') !== -1) {
                    return 'cke_krss';
                } else if (tagValue.indexOf('[traitement;lien_fiches;') !== -1) {
                    return 'cke_kcontrib';
                } else if (tagValue === '[nom]' || tagValue === '[prenom]') {
                    return 'cke_kuser';
                } else if (tagValue.indexOf('[traitement;liste_fiches;') !== -1) {
                    return 'cke_kcollab';
                } else if (tagValue.indexOf('[page;') !== -1 || tagValue.indexOf('[url;') !== -1 || tagValue.indexOf('[date]') !== -1) {
                    return 'cke_kother';
                }
            }
        },
        retrieveClass: function (element) {
            var classString = 'cke_ktag ';
            if (element.attributes.hasOwnProperty('ktag_news_span')) {
                classString += 'cke_knewsletter';
            } else if (element.attributes.hasOwnProperty('kagenda_span')) {
                classString += 'cke_kcalendar';
            } else if (element.attributes.hasOwnProperty('krss_span')) {
                classString += 'cke_krss';
            } else if (element.attributes.hasOwnProperty('ktag_panier_span')) {
                classString += 'cke_kcart';
            } else if (element.attributes.hasOwnProperty('ksite_span')) {
                classString += 'cke_kother';
            } else if (element.attributes.hasOwnProperty('kgalerie_span')) {
                classString += 'cke_kgallery';
            } else {
                classString += CKEDITOR.plugins.ktag.oldMode.analyseContent(element.children[0].value);
            }
            return classString;
        }
    };
    var url = window.location.href,
        splittedUrl = url.split('/'),
        domain = splittedUrl[0] + '//' + splittedUrl[2];
    var messageListener = function messageListener(event) {
        var currentDialog = CKEDITOR.dialog.getCurrent();
        var currentOkButton = currentDialog.getButton('ok');
        var dataObject = event.data;
        if (event.origin !== domain || !dataObject.hasOwnProperty('tag') || !dataObject.hasOwnProperty('value')) {
            currentOkButton.disable();
            return;
        }
        document.getElementById(currentDialog.iframeDom.id).setAttribute('data-tagValue', dataObject.value);
        document.getElementById(currentDialog.iframeDom.id).setAttribute('data-tagType', dataObject.tag);
        if (dataObject.hasOwnProperty('noFake')) {
            document.getElementById(currentDialog.iframeDom.id).setAttribute('data-noFake', dataObject.noFake);
        }
        currentOkButton.enable();
    };
    CKEDITOR.plugins.add(
        'ktag', {
            requires: ['kiframedialog'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('ktag', new CKEDITOR.dialogCommand('ktag'));
                editor.ui.addButton(
                    'ktag', {
                        label: LOCALE_BO.ckeditor.plugins.ktag.title,
                        command: 'ktag',
                        icon: this.path + 'icons/tag.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup('ktag');
                    editor.addMenuItem(
                        'ktag', {
                            label: LOCALE_BO.ckeditor.plugins.ktag.title,
                            command: 'ktag',
                            group: 'ktag',
                            icon: this.path + 'icons/tag.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (CKEDITOR.plugins.ktag.getSelectedTag(editor, element)) {
                            return {'ktag': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (element.hasClass('cke_ktag') && !element.isReadOnly()) {
                            evt.data.dialog = 'ktag';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'ktag', LOCALE_BO.ckeditor.plugins.ktag.title,
                    function (edit) {
                        var procParent = '';
                        if (document.querySelector('input[name="PROC"]')) {
                            procParent = document.querySelector('input[name="PROC"]').value;
                        }
                        var tagUrl = '/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=SAISIE_TAG_TOOLBOX&idTag=ktag&PROC_PARENT=' + procParent,
                            formatterValue = CKEDITOR.plugins.ktag.getFormatterValue(edit);
                        if (formatterValue) {
                            tagUrl += '&FORMATTER_VALUE=' + encodeURIComponent(formatterValue);
                        }
                        return tagUrl;
                    },
                    940, 600,
                    function (dialog) {
                    }, {
                        onShow: function () {
                            window.addEventListener('message', messageListener, false);
                            this.getButton('ok').disable();
                        },
                        onOk: function () {
                            var element = new CKEDITOR.dom.element('span'),
                                query = this.iframeDom.getAttribute('data-tagValue'),
                                tagType = this.iframeDom.getAttribute('data-tagType'),
                                noFakeElemet = this.iframeDom.getAttribute('data-noFake');
                            if (noFakeElemet) {
                                element.setHtml(query);
                                this._.editor.insertElement(element);
                            } else {
                                element.addClass(tagType);
                                element.addClass('ktag');
                                element.setHtml(query);
                                var fakeImage = CKEDITOR.plugins.ktag.createFakeImage(this._.editor, element, query, tagType);
                                this._.editor.insertElement(fakeImage);
                            }
                            window.removeEventListener('message', messageListener, false);
                        }
                    });
            },
            afterInit: function (editor) {
                // Empty anchors upcasting to fake objects.
                editor.dataProcessor.dataFilter.addRules(
                    {
                        elements: {
                            span: function (element) {
                                if (element.hasClass('ktag')) {
                                    var classString = element.attributes.class;
                                    if (classString) {
                                        var classSplitted = classString.split(' ');
                                        classSplitted.splice(0, 0, '');
                                        classString = classSplitted.join(' cke_');
                                    } else {
                                        classString = 'cke_ktag';
                                    }
                                    return editor.createFakeParserElement(element, classString, 'ktag');
                                } else if (CKEDITOR.plugins.ktag.oldMode.isKtag(element.attributes)) {
                                    var oldClassString = CKEDITOR.plugins.ktag.oldMode.retrieveClass(element);
                                    return editor.createFakeParserElement(element, oldClassString, 'ktag');
                                }
                                return null;
                            }
                        }
                    });
                var pathFilters = editor._.elementsPath && editor._.elementsPath.filters;
                if (pathFilters) {
                    pathFilters.push(
                        function (element, name) {
                            if (name === 'span') {
                                if (element.hasClass('ktag') || element.hasAttribute('ktag_span')) {
                                    return 'ktag';
                                }
                            }
                        });
                }
            }
        });
})();
