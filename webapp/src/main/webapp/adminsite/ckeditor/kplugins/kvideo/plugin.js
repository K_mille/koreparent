(function () {
    'use strict';
    CKEDITOR.plugins.kvideo = {
        getSelectedVideo: function (editor, element) {
            if (!element) {
                var sel = editor.getSelection();
                element = sel.getSelectedElement();
            }
            if (element && element.hasClass('cke_kvideo') && !element.isReadOnly()) {
                return editor.restoreRealElement(element);
            }
        },
        getEditUrl: function (editor) {
            var url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=VIDEO&FCK_PLUGIN=TRUE',
                processConfiguration = CKEDITOR.plugins.kmedia.getProcessConfiguration(),
                selectedVideo = CKEDITOR.plugins.kvideo.getSelectedVideo(editor);
            if (processConfiguration.code && processConfiguration.objet && processConfiguration.idFiche) {
                url += '&OBJET=' + processConfiguration.objet;
                url += '&CODE=' + processConfiguration.code;
                url += '&ID_FICHE=' + processConfiguration.idFiche;
                url += '&MODE_ONGLETS=' + (selectedVideo ? 'insertion' : 'selection');
                if (selectedVideo && selectedVideo.$.innerHTML.indexOf('[id-image]') !== -1 && selectedVideo.$.innerHTML.indexOf('[/id-image]') !== -1) {
                    url += '&ID_MEDIA=' + selectedVideo.$.innerHTML.split('[id-image]')[1].split('[/id-image]')[0];
                }
            }
            return url;
        },
        createFakeVideo: function (editor, element) {
            return editor.createFakeElement(element, 'cke_kvideo', 'span');
        }
    };
    // kVideo plugin
    CKEDITOR.plugins.add(
        'kvideo', {
            requires: ['kiframedialog', 'kmedia'],
            init: function (editor) {
                editor.addContentsCss(this.path + 'css/style.css');
                editor.addCommand('kvideo', new CKEDITOR.dialogCommand('kvideo'));
                editor.ui.addButton(
                    'kVideo', {
                        label: LOCALE_BO.ckeditor.plugins.kvideo.title,
                        command: 'kvideo',
                        icon: this.path + 'icons/kVideo.png'
                    });
                if (editor.contextMenu) {
                    editor.addMenuGroup( 'kvideo' );
                    editor.addMenuItem(
                        'kvideo', {
                            label: LOCALE_BO.ckeditor.plugins.kvideo.menu,
                            command: 'kvideo',
                            group: 'kvideo',
                            icon: this.path + 'icons/kVideo.png'
                        });
                }
                editor.contextMenu.addListener(
                    function (element) {
                        if (CKEDITOR.plugins.kvideo.getSelectedVideo(editor, element)) {
                            return { 'kvideo': CKEDITOR.TRISTATE_OFF};
                        }
                    });
                editor.on(
                    'doubleclick', function (evt) {
                        var element = evt.data.element;
                        if (CKEDITOR.plugins.kvideo.getSelectedVideo(editor, element)) {
                            evt.data.dialog = 'kvideo';
                        }
                    }, null, null, 100);
                CKEDITOR.dialog.addKIframe(
                    'kvideo', LOCALE_BO.ckeditor.plugins.kvideo.title,
                    CKEDITOR.plugins.kvideo.getEditUrl,
                    940, 555,
                    function () {
                    }, {
                        onOk: function (e) {
                            var dialogEditor = this._.editor,
                                iframeDocument = e.sender.iframeDom.contentWindow.document,
                                element = new CKEDITOR.dom.element('span'),
                                range = dialogEditor.getSelection().getRanges()[0];
                            element.addClass('kvideo');
                            element.setHtml(iframeDocument.querySelector('input[name="TAG"]').value);
                            dialogEditor.insertElement(CKEDITOR.plugins.kvideo.createFakeVideo(dialogEditor, element));
                            range.select();
                        },
                        onHide: function (e) {
                            var iframeDocument = e.sender.iframeDom.contentWindow.document,
                                iframeMejs = e.sender.iframeDom.contentWindow.mejs,
                                container = iframeDocument.querySelector('.mejs-container');
                            if (container) {
                                iframeMejs.players[container.id].pause();
                            }
                        }
                    });
            },
            afterInit: function (editor) {
                // Empty anchors upcasting to fake objects.
                editor.dataProcessor.dataFilter.addRules(
                    {
                        elements: {
                            span: function (element) {
                                if (element.hasClass('kvideo')) {
                                    return editor.createFakeParserElement(element, 'cke_kvideo', 'kvideo');
                                }
                                return null;
                            }
                        }
                    });
                var pathFilters = editor._.elementsPath && editor._.elementsPath.filters;
                if (pathFilters) {
                    pathFilters.push(
                        function (element, name) {
                            if (name === 'span') {
                                if (element.hasClass('kvideo')) {
                                    return 'kvideo';
                                }
                            }
                        });
                }
            }
        });
})();
