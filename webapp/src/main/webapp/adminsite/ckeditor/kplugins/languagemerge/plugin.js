(function() {
    'use strict';

    CKEDITOR.plugins.languagemerge = {
        loadExtra: function (extra) {
            CKEDITOR.scriptLoader.load(extra);
        },
        mergeExtra: function (langCode, extra, original) {
            var merge = function (currentObject) {
                for (var prop in currentObject) {
                    var ckLang = original || CKEDITOR.lang[langCode][prop];
                    if (Object.prototype.hasOwnProperty.call(currentObject, prop)) {
                        if ( Object.prototype.toString.call(currentObject[prop]) === '[object Object]' ) {
                            ckLang = CKEDITOR.plugins.languagemerge.mergeExtra(langCode, currentObject[prop], ckLang);
                        } else {
                            ckLang[prop] = currentObject[prop];
                        }
                    }
                }
            };
            if (Object.prototype.toString.call( extra ) === '[object Array]' ) {
                merge(extra[0]);
                for (var i = 1; i < extra.length; i++) {
                    var obj = extra[i];
                    merge(obj);
                }
            } else if (Object.prototype.toString.call( extra ) === '[object Object]' ) {
                merge(extra);
            }
        }
    };

    CKEDITOR.plugins.add(
        'languagemerge', {
            init: function (editor) {
                var extraLanguageDirectories = editor.config.extraLanguageDirectories,
                    paths = [];
                for (var i = 0; i < extraLanguageDirectories.length; i++) {
                    paths.push(extraLanguageDirectories[i] + editor.config.language + '_extra.js');
                }
                CKEDITOR.plugins.languagemerge.loadExtra(paths);
            }
        });
})();

