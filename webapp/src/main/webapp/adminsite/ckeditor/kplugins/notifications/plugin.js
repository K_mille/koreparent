﻿(function () {
    'use strict';
    CKEDITOR.plugins.globalNotifications = {
        globalNotifications: {},
        saveGlobalNotification: function (uuid, notification) {
            if (!CKEDITOR.plugins.globalNotifications.globalNotifications[uuid]) {
                CKEDITOR.plugins.globalNotifications.globalNotifications[uuid] = [];
            }
            CKEDITOR.plugins.globalNotifications.globalNotifications[uuid].push(notification);
        },
        dismissGlobalNotification: function (notification) {
            var uuid = notification.element.getAttribute('data-uuid');
            if (uuid) {
                var notifications = CKEDITOR.plugins.globalNotifications.globalNotifications[uuid];
                for (var i = 0; i < notifications.length; i++) {
                    notifications[i].hide();
                }
                CKEDITOR.plugins.globalNotifications.globalNotifications[uuid] = [];
                CKEDITOR.plugins.globalNotifications.saveUserChoice(uuid);
            }
        },
        getUuids: function () {
            var cookie = CKEDITOR.plugins.cookies.readCookie('globalNotifications');
            if (cookie) {
                return cookie.split(',');
            } else {
                CKEDITOR.plugins.cookies.createCookie('globalNotifications', '', 30);
                return [];
            }
        },
        saveUserChoice: function (uuid) {
            var uuids = CKEDITOR.plugins.globalNotifications.getUuids();
            if (uuids && uuids.indexOf(uuid) === -1) {
                uuids.push(uuid);
                CKEDITOR.plugins.cookies.createCookie('globalNotifications', uuids.join(','), 30);
            }
        },
        wasDismissed: function (uuid) {
            var uuids = CKEDITOR.plugins.globalNotifications.getUuids();
            if (uuids) {
                return uuids.indexOf(uuid) !== -1;
            }
            return false;
        },
        addGlobalNotification: function (editor, descriptor) {
            if (descriptor && descriptor.uuid && descriptor.message && !CKEDITOR.plugins.globalNotifications.wasDismissed(descriptor.uuid)) {
                var notification = new CKEDITOR.plugins.notification(
                    editor, {
                        'message': descriptor.message,
                        'type': descriptor.level || 'info',
                        'duration': 0
                    });
                notification.show();
                notification.element.setAttribute('data-uuid', descriptor.uuid);
                CKEDITOR.plugins.globalNotifications.saveGlobalNotification(descriptor.uuid, notification);
                var closeButton = new CKEDITOR.dom.element(notification.element.find('.cke_notification_close').$[0]);
                closeButton.on(
                    'click', function (event) {
                        CKEDITOR.plugins.globalNotifications.dismissGlobalNotification(event.listenerData);
                    }, null, notification);
            }
        }
    };
    CKEDITOR.plugins.add(
        'notifications', {
            requires: ['cookies', 'notification'],
            init: function (editor) {
                editor.once(
                    'instanceReady', function (e) {
                        var globalMessages = e.editor.config.globalNotifications.globalMessages;
                        if (globalMessages) {
                            for (var i in globalMessages) {
                                CKEDITOR.plugins.globalNotifications.addGlobalNotification(e.editor, globalMessages[i], true);
                            }
                        }
                    });
            }
        });
})();
