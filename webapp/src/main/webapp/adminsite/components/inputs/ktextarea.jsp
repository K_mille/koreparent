<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<jsp:useBean id="viewModel" class="com.kosmos.components.input.model.KTextareaViewModel" scope="request"/>
<p>
    <c:choose>
        <c:when test="${viewModel.editOption == 0}">
            <span class="label colonne">
                <span class="label"><c:out value="${viewModel.label}"/><c:if test="${viewModel.required}"> (*)</c:if></span>
                <c:if test="${not empty fn:trim(viewModel.tooltip)}"><span class="ktooltip"><c:out value="${viewModel.tooltip}"/></span></c:if>
            </span>
            <span>${viewModel.value}</span>
        </c:when>
        <c:otherwise>
            <span class="label colonne">
                <label for="${viewModel.name}"><c:out value="${viewModel.label}"/><c:if test="${viewModel.required}"> (*)</c:if></label>
                <c:if test="${not empty fn:trim(viewModel.tooltip)}"><span class="ktooltip"><c:out value="${viewModel.tooltip}"/></span></c:if>
            </span>
            <input type="hidden" name="#FORMAT_${viewModel.name}" value="<c:out value="${viewModel.formatValue}"/>">
            <textarea type="text" id="${viewModel.name}" name="${viewModel.name}" title="${viewModel.label}"  maxlength="${viewModel.max}" cols="${viewModel.cols}" rows="${viewModel.rows}"
                   <c:if test="${viewModel.required}">required="required"</c:if>><c:out value="${viewModel.value}"/></textarea>
        </c:otherwise>
    </c:choose>
</p>
