<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="utils" uri="http://kportal.kosmos.fr/tags/utils" %>
<jsp:useBean id="viewModel" class="com.kosmos.components.kselect.monoselect.model.MonoKselectModel" scope="request" />
<c:if test="${viewModel.editOption != -1}">
    <div>
        <span class="label colonne ">${viewModel.label}</span>
        <c:choose>
            <c:when test="${viewModel.editOption != 0}">
                <div id="kMonoSelect-${viewModel.name}" class="kmonoselect js-kmonoselect ${viewModel.required ? 'js-kmonoselect--required' : ''}"
                     data-editAction="${viewModel.action}"
                     data-popintitle="${viewModel.popinTitle}"
                     data-popinwidth="${viewModel.popinWidth}"
                     data-popinvalidate="${viewModel.popinValidate}">
                    <input type="hidden" id ="${viewModel.name}" name="${viewModel.name}" class="js-kmonoselect__value" value="${viewModel.selectItemModel.value}" ${viewModel.required ? 'required' : ''}/>
                    <button type="button" class="kmonoselect__button--edit js-kmonoselect__button--edit" title="${viewModel.editLabel}">
                        <img class="kmonoselect__icon" src="/adminsite/scripts/libs/css/images/icons/search-tree.svg" alt="${viewModel.editLabel}">
                    </button>
                    <c:if test="${not viewModel.required}">
                        <button type="button" class="kmonoselect__button--clear js-kmonoselect__button--clear" title="${viewModel.clearLabel}">
                            <img class="kmonoselect__icon" src="/adminsite/scripts/libs/css/images/icons/delete.svg" alt="${viewModel.clearLabel}">
                        </button>
                    </c:if>
                    <span class="kmonoselect__field js-kmonoselect__field">${utils:escapeHtml(viewModel.selectItemModel.title)}</span>
                </div>
            </c:when>
            <c:otherwise>
                <span><c:out value="${viewModel.selectItemModel.title}"/></span>
            </c:otherwise>
        </c:choose>
    </div>
</c:if>