<%@ page import="java.net.URLEncoder" errorPage="/adminsite/jsbexception.jsp" %> 

<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />

<%
    String urlRedirect = "/servlet/com.jsbsoft.jtf.core.SG?PROC=" + infoBean.getNomProcessus();
    if (infoBean.getSessionHttp().getAttribute("CURRENT_SEARCH_PARAMS") != null)
    {
        urlRedirect += "&" + URLEncoder.encode("#ECRAN_LOGIQUE#", "UTF-8") + "=RECHERCHE&" + URLEncoder.encode("#ETAT#", "UTF-8") + "=RECHERCHE&RECHERCHE_ETENDUE=OUI&SEARCH_PARAMS=IN_CACHE";
        if (infoBean.get("PAGE") != null)
        {
            urlRedirect += "&PAGE=" + infoBean.getString("PAGE");
        }
    }
    else // liste des fiches du redacteur
    {
        urlRedirect += "&ACTION=RECHERCHER";
    }

    //AA : modif pour être redirigé vers l'écran "Fiches à valider" quand on vient de valider une fiche.
    if(infoBean.get("ECRANVALIDER") != null)
        urlRedirect = "/servlet/com.jsbsoft.jtf.core.SG?PROC=VALIDATION&ACTION=LISTE";

    if (infoBean.get("ETAT_FICHE_ENREGISTREE") != null)
    {
        urlRedirect += "&ETAT_FICHE_ENREGISTREE=" + infoBean.getString("ETAT_FICHE_ENREGISTREE");
    }
    if (infoBean.get("MESSAGE_WARNING") != null)
    {
        urlRedirect += "&MESSAGE_WARNING=" + infoBean.getString("MESSAGE_WARNING");
    }

    response.sendRedirect(urlRedirect);
%>
