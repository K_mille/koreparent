<%@page import="java.io.File"%>
<%@ page
        import="java.io.FileOutputStream, javax.servlet.jsp.JspWriter, javax.xml.transform.OutputKeys, javax.xml.transform.Transformer, javax.xml.transform.TransformerFactory,  javax.xml.transform.dom.DOMSource, javax.xml.transform.stream.StreamResult, org.w3c.dom.Document,org.w3c.dom.Node, com.kportal.core.webapp.WebAppUtil, com.univ.utils.ContexteUniv, com.univ.utils.URLResolver" %>

<%!
public static void lireTemplate(String urlServeur, JspWriter _out, String _template, String _session, String _requete) throws Exception {

    char buffer[] = new char[50000];

    String urlConnecteur = urlServeur + "/adminsite/dsi/kconnect.jsp?ktemplate="+ _template +"&ksession=" + _session + "&krequete=" + _requete;
    System.out.println("lireTemplate() : "+ urlConnecteur);


    java.net.URL url = new java.net.URL(urlConnecteur);
    java.net.URLConnection urlConnection = url.openConnection();
    String ligne = "";
    java.io.BufferedReader flux = new java.io.BufferedReader(new java.io.InputStreamReader(urlConnection.getInputStream()));
    while ((ligne = flux.readLine()) != null) {
        _out.println(ligne);
    }

    flux.close();
}

public static void addNode(Node pere, String nomDonnee, String valeur) {


    Document document = pere.getOwnerDocument();


    Node attribut = document.createElement( nomDonnee);
    attribut.appendChild(document.createTextNode( valeur));
    pere.appendChild(attribut);

}

public static void genererFichierXML( String _ksession, String _krequete) throws Exception {

    String requete = "";



    /* Création DOM */

    javax.xml.parsers.DocumentBuilderFactory factory = javax.xml.parsers.DocumentBuilderFactory.newInstance();

    javax.xml.parsers.DocumentBuilder builder = factory.newDocumentBuilder();
    org.w3c.dom.Document document = builder.newDocument();

    org.w3c.dom.Element rootElement = document.createElement("REQUETE");

    /* Création noeud */
    addNode(rootElement, "RUBRIQUE", "ACCUEIL");

    // Encadré
    Node listeEncadres = rootElement.getOwnerDocument().createElement("LISTE_ENCADRES");
    org.w3c.dom.Element encadreElement = document.createElement("ENCADRE");
    addNode(encadreElement, "TITRE", "titre externe");
    addNode(encadreElement, "CONTENU", "contenu externe");
    listeEncadres.appendChild(encadreElement);

    rootElement.appendChild( listeEncadres);



    document.appendChild(rootElement);

    String exportFile = null;
    FileOutputStream os = null;
    TransformerFactory transFactory = null;
    Transformer transformer = null;

    /**************************************************/
    /*    Génération du fichier XML                   */
    /**************************************************/

    transFactory = TransformerFactory.newInstance();
    transformer = transFactory.newTransformer();
    DOMSource source = new DOMSource(document);
    exportFile = WebAppUtil.getSessionsPath()
            + File.separator
            + "req_"
            + _ksession
            + "_"
            + _krequete
            + ".xml";

    File newXML = new File(exportFile);
    os = new FileOutputStream(newXML);
    StreamResult result = new StreamResult(os);
    transformer.setOutputProperty(OutputKeys.ENCODING, "utf-8");

    transformer.transform(source, result);
    os.close();

}
%>

<%
  ContexteUniv  ctx = new ContexteUniv (pageContext);
  ctx.setJsp(this);

  String urlServeur = URLResolver.getAbsoluteUrl("", ctx);
  
  String ksession = ctx.getKsession();
  String krequete = "" + System.currentTimeMillis();

  genererFichierXML( ksession, krequete);

  lireTemplate(urlServeur, out, "haut", ksession, krequete);

  %>

Test connecteur OK !

  <%
  lireTemplate(urlServeur, out, "bas", ksession, krequete);

  ctx.release();
 %>



