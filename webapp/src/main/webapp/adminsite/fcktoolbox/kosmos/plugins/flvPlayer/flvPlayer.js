var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

FCKConfig.PluginLoadedPath = window.location.href;

// Set the language direction.
window.document.dir = oEditor.FCKLang.Dir;

// Set the Skin CSS.
document.write('<link href="' + oEditor.FCKConfig.SkinPath + 'fck_dialog.css" type="text/css" rel="stylesheet">');

var sAgent = navigator.userAgent.toLowerCase();

var is_ie = (sAgent.indexOf("msie") != -1); // FCKBrowserInfo.IsIE
var is_gecko = !is_ie; // FCKBrowserInfo.IsGecko

var oMedia = null;
var is_new_flvplayer = true;


/** ICI LE CODE POUR GERER LES FAKE TAGS **/

var oFakeImage = dialog.Selection.GetSelectedElement();
var oDivFlv;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_flv_video'))
        oDivFlv = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

/** FIN DU CODE POUR GERER LES FAKE TAGS**/

function insertFlvVideo(idDivPlayer, url, local, imgUrl, loop, fullscreen, extension) {
    oEditor.FCKUndo.SaveUndoStep();
    oDivFlv = FCK.EditorDocument.createElement('DIV');
    oDivFlv.className = 'media video';

    var first = true,
        innerTag = '[media;video;';
    if (url) {
        innerTag += (first ? 'URL=' : '#URL=') + url;
        first = false;
    }
    if (local) {
        innerTag += (first ? 'LOCAL=' : '#LOCAL=') + local;
        first = false;
    }
    if (imgUrl) {
        innerTag += (first ? 'IMGURL=' : '#IMGURL=') + imgUrl;
        first = false;
    }
    if (extension) {
        innerTag += (first ? 'EXTENSION=' : '#EXTENSION=') + extension;
        first = false;
    }
    innerTag += 'media;video]';
    oDivFlv.innerHTML = innerTag;

    oFakeImage = null;

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('FLV_VIDEO', oDivFlv);
        oFakeImage.setAttribute('_flv_video', 'true', 0);
        oFakeImage.setAttribute('src', location.origin + '/adminsite/images/medias/video.png');

        oFakeImage = FCK.InsertElement(oFakeImage);
    }
}


function window_onload() {
    // Translate the dialog box texts.
    oEditor.FCKLanguageManager.TranslatePage(document);

    // Load the selected element information (if any).
    LoadSelection();

    // Show/Hide the "Browse Server" button.
    GetE('tdBrowse').style.display = oEditor.FCKConfig.FlashBrowser ? '' : 'none';
}


function getSelectedMovie() {
    var oSel = null;
    oMedia = new Media();

    if (oDivFlv) {
        if (GetAttribute(oDivFlv, 'flv_url', '')) {
            oMedia.setAttribute('url', GetAttribute(oDivFlv, 'flv_url', ''));
            oMedia.setAttribute('local', GetAttribute(oDivFlv, 'flv_local', ''));
            oMedia.setAttribute('iurl', GetAttribute(oDivFlv, 'flv_imgUrl', ''));
            oMedia.setAttribute('loop', GetAttribute(oDivFlv, 'flv_loop', ''));
            oMedia.setAttribute('fullscreen', GetAttribute(oDivFlv, 'flv_fullscreen', ''));
        } else if (oDivFlv.children[0] && oDivFlv.children[0].tagName.toLowerCase() === "video") {
            var video = oDivFlv.children[0],
                source = video.children[0];
            oMedia.setAttribute('url', GetAttribute(source, 'src', ''));
            oMedia.setAttribute('local', GetAttribute(oDivFlv, 'flv_local', ''));
            oMedia.setAttribute('iurl', GetAttribute(video, 'poster', ''));
            oMedia.setAttribute('loop', GetAttribute(video, 'loop', ''));
        } else {
            var tag = oDivFlv.innerHTML;
            tag = tag.replace('[media;video;', '');
            tag = tag.replace('media;video]', '');
            var i = 0,
                params = tag.split('#');
            if (tag.indexOf('URL=') >= 0) {
                oMedia.setAttribute('url', params[i].replace('URL=', ''));
                i++;
            }
            if (tag.indexOf('LOCAL=') >= 0) {
                oMedia.setAttribute('local', params[i].replace('LOCAL=', ''));
                i++;
            }
            if (tag.indexOf('IMGURL=') >= 0) {
                oMedia.setAttribute('iurl', params[i].replace('IMGURL=', ''));
                i++;
            }
            if (tag.indexOf('EXTENSION=') >= 0) {
                oMedia.setAttribute('fileType', params[i].replace('EXTENSION=', ''));
                i++;
            }
        }
    }
    return oMedia;
}

function updatePlaylistOption() {
    if (GetE('selDispPlaylist').value == "right" || GetE('selDispPlaylist').value == "below") {
        GetE('chkPLThumbs').disabled = false;
        GetE('chkPLThumbs').checked = true;
        GetE('txtPLDim').disabled = false;
        GetE('txtPLDim').style.background = '#ffffff';
        GetE('spanDimText').style.display = 'none';
        if (GetE('selDispPlaylist').value == "right") {
            GetE('spanDimWText').style.display = '';
            GetE('spanDimHText').style.display = 'none';
        } else if (GetE('selDispPlaylist').value == "below") {
            GetE('spanDimWText').style.display = 'none';
            GetE('spanDimHText').style.display = '';
        }
    } else {
        GetE('chkPLThumbs').disabled = true;
        GetE('chkPLThumbs').checked = false;
        GetE('txtPLDim').value = "";
        GetE('txtPLDim').disabled = true;
        GetE('txtPLDim').style.background = 'transparent';
        GetE('spanDimText').style.display = '';
        GetE('spanDimWText').style.display = 'none';
        GetE('spanDimHText').style.display = 'none';
    }
}


function LoadSelection() {
    oMedia = new Media();
    oMedia.loop = false;
    oMedia = getSelectedMovie();
    GetE('txtUrl').value = oMedia.url;
    GetE('txtLocal').value = oMedia.local;
    GetE('txtExtension').value = oMedia.fileType;
    GetE('txtImgURL').value = oMedia.iurl;
    GetE('chkFullscreen').checked = (oMedia.fullscreen == '1' ? true : false);

    //dans le cas d'une modif de ressource, on va directement sur l'onglet des parametres d'insertion
    if (oMedia.url != '') {
        modeOngletsModif = 'insertion';
    }
}

//#### The OK button was hit.
function Ok() {
    var rbFileTypeVal = "single";

    if (rbFileTypeVal == "single") {
        if (GetE('txtUrl').value.length == 0) {
            GetE('txtUrl').focus();

            alert(oEditor.FCKLang.DlgFLVPlayerAlertUrl);
            return false;
        }
    }

    var e = (oMedia || new Media());

    updateMovie(e);
    e.getInnerHTML();

    return true;
}


function updateMovie(e) {
    e.loop = '0';
    e.fileType = GetE('txtExtension').value;
    e.url = GetE('txtUrl').value;
    e.local = GetE('txtLocal').value;
    e.iurl = GetE('txtImgURL').value;
    e.fullscreen = (GetE('chkFullscreen').checked) ? '1' : '0';
}


function BrowseServer() {
    OpenServerBrowser(
        'flv',
        oEditor.FCKConfig.VideoBrowserURL,
        oEditor.FCKConfig.VideoBrowserWindowWidth,
        oEditor.FCKConfig.VideoBrowserWindowHeight);
}


function LnkBrowseServer() {
    OpenServerBrowser(
        'link',
        oEditor.FCKConfig.LinkBrowserURL,
        oEditor.FCKConfig.LinkBrowserWindowWidth,
        oEditor.FCKConfig.LinkBrowserWindowHeight);
}

function Lnk2BrowseServer() {
    OpenServerBrowser(
        'link2',
        oEditor.FCKConfig.LinkBrowserURL,
        oEditor.FCKConfig.LinkBrowserWindowWidth,
        oEditor.FCKConfig.LinkBrowserWindowHeight);
}

function img1BrowseServer() {
    OpenServerBrowser(
        'img1',
        oEditor.FCKConfig.ImageBrowserURL,
        oEditor.FCKConfig.ImageBrowserWindowWidth,
        oEditor.FCKConfig.ImageBrowserWindowHeight);
}

function img2BrowseServer() {
    OpenServerBrowser(
        'img2',
        oEditor.FCKConfig.ImageBrowserURL,
        oEditor.FCKConfig.ImageBrowserWindowWidth,
        oEditor.FCKConfig.ImageBrowserWindowHeight);
}


function OpenServerBrowser(type, url, width, height) {
    sActualBrowser = type;
    OpenFileBrowser(url, width, height);
}

var sActualBrowser;


function SetUrl(url) {
    if (sActualBrowser == 'flv') {
        document.getElementById('txtUrl').value = url;
    } else if (sActualBrowser == 'link') {
        document.getElementById('txtPlaylist').value = url;
    } else if (sActualBrowser == 'link2') {
        document.getElementById('txtRURL').value = url;
    } else if (sActualBrowser == 'img1') {
        document.getElementById('txtImgURL').value = url;
    } else if (sActualBrowser == 'img2') {
        document.getElementById('txtWMURL').value = url;
    }
}

function SetUrlVideo(url) {
    document.getElementById('txtUrl').value = url;
}

function SetUrlImgPreview(url) {
    document.getElementById('txtImgURL').value = url;
}


var Media = function (o) {
    this.fileType = '';
    this.url = '';
    this.local = '1';
    this.purl = '';
    this.iurl = '';
    this.wmurl = '';
    this.width = '';
    this.height = '';
    this.loop = '';
    this.play = '';
    this.downloadable = '';
    this.fullscreen = '';
    this.displayDigits = '';
    this.vspace = '';
    this.hspace = '';
    this.align = '';
    this.dispPlaylist = '';
    this.rurl = '';
    this.playlistDim = '';
    this.playlistThumbs = '';

    if (o)
        this.setObjectElement(o);
};

Media.prototype.setObjectElement = function (e) {
    if (!e) return;
};

Media.prototype.setAttribute = function (attr, val) {
    if (val == "true") {
        this[attr] = true;
    } else if (val == "false") {
        this[attr] = false;
    } else {
        this[attr] = val;
    }
};

Media.prototype.getInnerHTML = function (objectId) {
    var randomnumber = Math.floor(Math.random() * 1000001);
    var s = '';

    insertFlvVideo('player' + randomnumber, this.url, this.local, this.iurl, this.loop, this.fullscreen, this.fileType);

    return s;
};