<script src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js" type="text/javascript"></script>
<script src="/adminsite/fcktoolbox/kosmos/plugins/flvPlayer/flvPlayer.js" type="text/javascript"></script>
<link href="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
<!--
document.writeln(FCK.TempBaseTag);
-->
</script>
<div id="divInfo">
	<input disabled="disabled" type="hidden" id="txtImgURL">
	<input disabled="disabled"  type="hidden" id="txtLocal">
	<input disabled="disabled"  type="hidden" id="txtExtension">
	<table cellspacing="1" cellpadding="1" border="0" width="100%" height="100%">
		<tr>
			<td valign="top" colspan="3">
				<table cellspacing="0" cellpadding="0" width="100%" border="0">
					<tr>
						<td valign="top" width="100%">								
							<span fckLang="DlgFLVPlayerURL">.FLV Movie</span><br>
							<input disabled="disabled" style="width:100%;" type="text" id="txtUrl">
						</td>
						<td id="tdBrowse" valign="bottom" nowrap>
							<input type="button" value="Modifier la ressource" onclick="basculeOngletsDispos();" id="btnBrowse">
						</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td valign="top" width="50%">
				<fieldset>
					<legend><span fcklang="DlgFLVPlayerMovieAttrs">Movie Attributes</span></legend>
						<table cellspacing="0" cellpadding="0" width="100%" border="0">
							<tr>
								<td valign="top" colspan=2>
									<input type="checkbox" class="CheckBox" checked id="chkFullscreen">
									<label for="chkFullscreenchkFullscreen" fckLang="DlgFLVPlayerFullscreen">Allow Fullscreen</label>
								</td>
							</tr>
						</table>
				</fieldset>
			</td>
		</tr>
  </table>
</div>
<script type="text/javascript">window_onload();</script>
