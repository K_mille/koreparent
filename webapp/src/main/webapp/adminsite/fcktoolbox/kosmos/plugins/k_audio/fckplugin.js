// Register the commands.
dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=AUDIO&FCK_PLUGIN=TRUE';
if (FCKConfig.CodeObjet && FCKConfig.Code && FCKConfig.IdFiche) {
    dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=AUDIO&OBJET=' + FCKConfig.CodeObjet + '&CODE=' + FCKConfig.Code + '&ID_FICHE=' + FCKConfig.IdFiche + '&FCK_PLUGIN=TRUE';
}

FCKCommands.RegisterCommand('KPortalAudio', new FCKDialogCommand('KPortalAudio', FCKLang['DlgKPortalAudioTitle'], dialogPath, 960, 555));

// Create the toolbar buttons.
var oKPortalAudio = new FCKToolbarButton('KPortalAudio', FCKLang['KPortalAudio']);

// Set the buttons icons.
oKPortalAudio.IconPath = '/adminsite/images/medias/audio_icone.png';

// Register the toolbar items (these names will be used in the config file).
FCKToolbarItems.RegisterItem('KPortalAudio', oKPortalAudio);


FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aDivs = document.querySelectorAll('div.media.audio, span[kaudio_span]'),
        i = aDivs.length - 1,
        oDiv;
    while (i >= 0 && ( oDiv = aDivs[i--] )) {
        var oImg = FCKDocumentProcessor_CreateFakeImage('K_AUDIO', oDiv.cloneNode(true));
        oImg.setAttribute('_kaudio', 'true', 0);
        oImg.setAttribute('src', '/adminsite/images/medias/audio.png');
        oDiv.parentNode.insertBefore(oImg, oDiv);
        oDiv.parentNode.removeChild(oDiv);
    }
}

FCK.ContextMenu.RegisterListener({
        AddItems: function (menu, tag, tagName) {
            if (tagName == 'IMG' && tag.getAttribute('_kaudio')) {
                menu.AddSeparator();
                menu.AddItem('KPortalAudio', FCKLang.KAudioProperties, '/adminsite/images/medias/audio_icone.png');
            }
        }
    }
);