<script src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js" type="text/javascript"></script>
<script src="/adminsite/fcktoolbox/kosmos/plugins/k_audio/js/k_audio.js" type="text/javascript"></script>
<script type="text/javascript">
	document.write( FCKTools.GetStyleHtml( GetCommonDialogCss() ) ) ;
</script>
<div id="divInfo">
	<table>
		<tr>
			<td colspan="2">
				<table cellSpacing="0" cellPadding="0" width="100%" border="0">
					<tr>
						<td width="100%"><span fckLang="DlgImgURL">URL</span>
						</td>
						<td id="tdBrowse" style="DISPLAY: none" noWrap rowSpan="2">&nbsp; <input id="btnBrowse" onclick="basculeOngletsDispos();" type="button" value="Modifier la ressource" />
						</td>
					</tr>
					<tr>
						<td vAlign="top"><input id="txtUrl" style="WIDTH: 100%" type="text" disabled="disabled">
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br />
	<fieldset>
		<legend>Comportement du lecteur</legend>
		<p>
			<input id="dewreplay" name="dewreplay" value="1" type="checkbox">		
			<label for="dewreplay" title="La lecture est ininterrompue (retour au début automatique)">Lecture en boucle</label>
		</p>
	</fieldset>
</div>
<script type="text/javascript">window_onload();</script>