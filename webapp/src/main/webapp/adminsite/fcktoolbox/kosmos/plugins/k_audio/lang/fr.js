FCKLang['DlgKPortalAudioTitle'] = "Propriétés de l'extrait audio";
FCKLang['KPortalAudio'] = "Extrait audio";
FCKLang['DlnAudioMsgNoAudio'] = "Veuillez renseigner l'url de l'extrait audio";
FCKLang['DlgAudioBtnBrowseServer'] = "Rechercher un extrait audio";
FCKLang['KAudioProperties'] = "Modifier l'extrait audio";