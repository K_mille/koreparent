if (typeof(window.parent.InnerDialogLoaded) == 'function') {
    var oEditor = window.parent.InnerDialogLoaded();
}
function initPluginFck() {
    if (oEditor) {
        // Whole word is available on IE only.
        if (oEditor.FCKBrowserInfo.IsIE) {
            if (document.getElementById('divWord')) {
                document.getElementById('divWord').style.display = '';
            }
        }

        // First of all, translate the dialog box texts.
        oEditor.FCKLanguageManager.TranslatePage(document);

        window.parent.SetAutoSize(false);
    }
}