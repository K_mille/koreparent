// Register the commands.
dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=FLASH&FCK_PLUGIN=TRUE';
if (FCKConfig.CodeObjet && FCKConfig.Code && FCKConfig.IdFiche) {
    dialogPath = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=FLASH&OBJET=' + FCKConfig.CodeObjet + '&CODE=' + FCKConfig.Code + '&ID_FICHE=' + FCKConfig.IdFiche + '&FCK_PLUGIN=TRUE';
}

FCKCommands.RegisterCommand('KPortalFlash', new FCKDialogCommand('KPortalFlash', FCKLang.DlgFlashTitle, dialogPath, 960, 555));

// Create the toolbar buttons.
var oKPortalFlash = new FCKToolbarButton('KPortalFlash', FCKLang.InsertFlashLbl, FCKLang.InsertFlash, null, false, true, 38);

// Register the toolbar items (these names will be used in the config file).
FCKToolbarItems.RegisterItem('KPortalFlash', oKPortalFlash);

FCK.ContextMenu.RegisterListener({
        AddItems: function (menu, tag, tagName) {
            if (tagName == 'IMG' && tag.getAttribute('_fckflash')) {
                menu.AddSeparator();
                menu.AddItem('KPortalFlash', FCKLang.FlashProperties, 38);
            }
        }
    }
);

FCKEmbedAndObjectProcessor.AddCustomHandler(function (A, B) {
    if (!(A.nodeName.IEquals('OBJECT') && (A.type == 'application/x-shockwave-flash' || /\.swf($|#|\?)/i.test(A.src)))) return;
    B.className = 'FCK__Flash';
    B.setAttribute('_fckflash', 'true', 0);
});
