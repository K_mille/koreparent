FCKCommands.RegisterCommand('KPortalGalerie', new FCKDialogCommand('KPortalGalerie', FCKLang['DlgKPortalGalerieTitle'], FCKConfig.PluginsPath + 'k_galerie/k_galerie.jsp', 960, 555));

var oKPortalGalerie = new FCKToolbarButton('KPortalGalerie', FCKLang['KPortalGalerie']);

oKPortalGalerie.IconPath = FCKConfig.PluginsPath + 'k_galerie/images/galerie_icone.png';

FCKToolbarItems.RegisterItem('KPortalGalerie', oKPortalGalerie);

FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aSpans = document.getElementsByTagName('SPAN');

    var oSpan;
    var i = aSpans.length - 1;
    while (i >= 0 && ( oSpan = aSpans[i--] )) {
        if (oSpan.getAttribute('kgalerie_span')) {
            var oImg = FCKDocumentProcessor_CreateFakeImage('K_GALERIE', oSpan.cloneNode(true));
            oImg.setAttribute('_kgalerie', 'true', 0);
            oImg.setAttribute('src', FCKConfig.PluginsPath + 'k_galerie/images/galerie.png');
            oImg.setAttribute('title', oSpan.innerHTML);

            oSpan.parentNode.insertBefore(oImg, oSpan);
            oSpan.parentNode.removeChild(oSpan);
        }
    }
}

/*FCK.ContextMenu.RegisterListener( {
 AddItems : function( menu, tag, tagName )
 {
 if ( tagName == 'IMG' && tag.getAttribute( '_kgalerie' ) )
 {
 menu.AddSeparator() ;
 menu.AddItem( 'KPortalGalerie', FCKLang.KGalerieProperties, FCKConfig.PluginsPath + 'k_galerie/images/galerie_icone.png' ) ;
 }
 }}
 );*/