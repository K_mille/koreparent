var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

/** ICI LE CODE POUR GERER LES FAKE GALERIE **/
var oFakeImage = dialog.Selection.GetSelectedElement();
var oSpanGalerie;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_kgalerie'))
        oSpanGalerie = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

/** FIN DU CODE POUR GERER LES FAKE TAGS**/

function Ok() {

    intitule = document.forms.galerie.galerie_intitule.value;

    typeMedia = document.forms.galerie.galerie_type_media.value.replace(/;/g, ',');

    libelle = document.forms.galerie.galerie_titre.value;

    legende = document.forms.galerie.galerie_legende.value;

    codeRubrique = document.forms.galerie.galerie_code_rubrique.value;

    if (document.forms.galerie.galerie_code_rubrique_courante.checked)
        codeRubrique = 'DYNAMIK';

    if (document.forms.galerie.galerie_code_rubrique_noarbo.checked)
        codeRubrique += '_NOARBO';

    codeStructure = document.forms.galerie.galerie_code_structure.value;
    if (document.forms.galerie.galerie_code_structure_noarbo.checked)
        codeStructure += '_NOARBO';

    codeUser = document.forms.galerie.galerie_code_redacteur.value;
    if (document.forms.galerie.galerie_code_redacteur_courant.checked)
        codeUser = 'DYNAMIK';

    thematique = document.forms.galerie.galerie_thematique.value;
    if (thematique == '0000')
        thematique = '';

    selection = document.forms.galerie.galerie_selection.value;
    if (selection == '0000')
        selection = '';

    jour = document.forms.galerie.galerie_jour.value;
    date_debut = document.forms.galerie.galerie_date_debut.value;
    date_fin = document.forms.galerie.galerie_date_fin.value;

    tri = '';
    for (var i = 0; i < document.forms.galerie.galerie_tri_date.length; i++) {
        if (document.forms.galerie.galerie_tri_date[i].checked) {
            tri = document.forms.galerie.galerie_tri_date[i].value;
        }
    }

    visu = document.forms.galerie.galerie_visualisation.value;
    if (visu == '0000') {
        visu = '';
    }

    nombre = document.forms.galerie.galerie_nombre.value;

    onglet = '';
    if (document.forms.galerie.galerie_affichage_onglet.checked)
        onglet = document.forms.galerie.galerie_affichage_onglet.value;

    insertTagGalerie('[traitement;media_galerie;' +
    setCritere('INTITULE_GALERIE', intitule) +
    setCritere('#TYPE_MEDIA', typeMedia) +
    setCritere('#CODE_REDACTEUR', codeUser) +
    setCritere('#CODE_RUBRIQUE', codeRubrique) +
    setCritere('#CODE_RATTACHEMENT', codeStructure) +
    setCritere('#TITRE', libelle) +
    setCritere('#LEGENDE', legende) +
    setCritere('#THEMATIQUE', thematique) +
    setCritere('#SELECTION', selection) +
    setCritere('#JOUR', jour) +
    setCritere('#DATE_DEBUT', date_debut) +
    setCritere('#DATE_FIN', date_fin) +
    setCritere('#TRI_DATE', tri) +
    setCritere('#NOMBRE', nombre) +
    setCritere('#MODE', visu) +
    setCritere('#ONGLET', onglet) + ']');
    return true;
}

function insertTagGalerie(sTag) {
    oEditor.FCKUndo.SaveUndoStep();
    if (!oSpanGalerie) {
        oSpanGalerie = FCK.EditorDocument.createElement('SPAN');
        oFakeImage = null;
    }

    oSpanGalerie.innerHTML = sTag;
    oSpanGalerie.setAttribute('kgalerie_span', 'true');

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_GALERIE', oSpanGalerie);
        oFakeImage.setAttribute('_kgalerie', 'true', 0);
        oFakeImage.setAttribute('src', FCKConfig.PluginsPath + 'k_galerie/images/galerie_icone.png');
        oFakeImage.setAttribute('title', sTag);

        oFakeImage = FCK.InsertElement(oFakeImage);
    }

    oFakeImage.setAttribute('title', sTag);
}

function setCritere(nom, val) {
    res = '';
    if (val && val != '') {
        res = nom + '=' + val;
    }
    return res;
}

