<%@page import="java.util.Hashtable"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Map"%>
<%@page import="com.jsbsoft.jtf.core.ApplicationContextManager"%>
<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.jsbsoft.jtf.core.LangueUtil"%>
<%@page import="com.kportal.frontoffice.util.JSPIncludeHelper"%>
<%@page import="com.univ.mediatheque.Mediatheque"%>
<%@page import="com.univ.mediatheque.utils.MediathequeHelper" %>
<%@page import="com.univ.objetspartages.util.LabelUtils" %>
<%@page import="com.univ.utils.ContexteUniv"%>
<%@page import="com.univ.utils.ContexteUtil"%>
<%@page import="com.univ.utils.UnivFmt"%>
<%@ page import="com.univ.utils.media.TypeRessourceTypeMediaHelper" %>
<%@taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<%
  	// initialisation du contexte (infos utilisateur, connexion BdD,...)
  	ContexteUniv ctx =ContexteUtil.getContexteUniv();
	Locale locale = ctx.getLocale();
	Mediatheque mediatheque = Mediatheque.getInstance();

%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8"/>
	<title>Insertion d'une galerie de média</title>
	<link rel="stylesheet" media="screen, projection" href="/adminsite/scripts/libs/css/jquery-ui-1.10.3.custom.css">
	<!-- Sorti de WRO pour l'uri en base 64 ! -->
	<resources:link group="stylesBo"/>
		<link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css">
		<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
		<script type="text/javascript" src="js/k_galerie.js"></script>
		<script type="text/javascript" src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js"></script>
		<script type="text/javascript">
			window.parent.SetOkButton( true ) ;	
		</script>
	</head>
	<body id="body_k_plugin" class="toolbox">

		<div id="content">
			<form class="popup-tag" name="galerie" action="bidon" method="post">
			<%
				Hashtable<String, String> hash = new Hashtable<String, String>();
			%>
			<div class="content_popup">
			
				<div id="tag_media">
				
						<p>
							<label for="galerie_intitule">Intitulé de la galerie</label>
							<input type="text" name="galerie_intitule" value="" size="40" />
						</p>
				
						<fieldset><legend>Critères métiers</legend>
						<%
							hash = mediatheque.getTypesRessourcesAffichables();
							TypeRessourceTypeMediaHelper typeRessourceTypeMediaHelper =(TypeRessourceTypeMediaHelper) ApplicationContextManager.getCoreContextBean(TypeRessourceTypeMediaHelper.ID_BEAN);
							Map<String,String> typeMediaTypeRessource=new Hashtable<String,String>();
							//construction d'une map contenant tous les couples type de ressources / type de média possibles
							for (Iterator<Map.Entry<String,String>> itTypesRessources = hash.entrySet().iterator();itTypesRessources.hasNext();){
								typeMediaTypeRessource.putAll(typeRessourceTypeMediaHelper.
										getCodeEtLibelleTypeRessourceEtTypeMedia(ctx,itTypesRessources.next().getKey()));
							}
							infoBean.set("LISTE_TYPE_MEDIA",typeMediaTypeRessource);
							univFmt.insererKmultiSelectLtl(fmt, out,infoBean, "galerie_type_media", FormateurJSP.SAISIE_FACULTATIF, "Types de média", "LISTE_TYPE_MEDIA", "", UnivFmt.CONTEXT_DEFAULT);
						%>
						<p>
							<label for="galerie_titre" class="colonne">Titre</label>
								<input type="text" name="galerie_titre" value="" size="40" />
						</p>
						<p>
							<label for="galerie_legende" class="colonne">Légende</label>
								<input type="text" name="galerie_legende" value="" size="40" />
						</p>
						<p>
							<label for="galerie_thematique" class="colonne">Thématique</label>
							<%
							infoBean.set("LISTE_THEMATIQUES", LabelUtils.getLabelCombo("04", LangueUtil.getDefaultLocale()));
							fmt.insererComboHashtable(out, infoBean, "galerie_thematique", FormateurJSP.SAISIE_FACULTATIF, "LISTE_THEMATIQUES","Thématique" );
							%>
						</p>
						<%
							infoBean.set("galerie_code_rubrique","");
							univFmt.insererkMonoSelect(fmt, out, infoBean, "galerie_code_rubrique", FormateurJSP.SAISIE_FACULTATIF, "Rubrique", "rubrique", UnivFmt.CONTEXT_ZONE);
						%>
						<p class="retrait">
							<input id="galerie_code_rubrique_courante" type="checkbox" name="galerie_code_rubrique_courante" value="1" />
							<label for="galerie_code_rubrique_courante">ou rubrique courante</label>
						</p>
						<p class="retrait">
							<input type="checkbox" name="galerie_code_rubrique_noarbo" value="1" />							
							<label id="galerie_code_rubrique_noarbo" for="galerie_code_rubrique_noarbo">uniquement cette rubrique (pas toute l'arborescence)</label>
						</p>
						<%
							infoBean.set("galerie_code_structure","");
							univFmt.insererkMonoSelect(fmt, out, infoBean, "galerie_code_structure", FormateurJSP.SAISIE_FACULTATIF, "Structure", "", UnivFmt.CONTEXT_STRUCTURE);
						%>
						<p class="retrait">
							<input id="galerie_code_structure_noarbo" type="checkbox" name="galerie_code_structure_noarbo" value="1" />
							<label for="galerie_code_structure_noarbo">uniquement cette structure (pas toute l'arborescence)</label>
						</p>
						<%
							infoBean.set("galerie_redacteur","");
							univFmt.insererkMonoSelect(fmt, out, infoBean, "galerie_code_redacteur", FormateurJSP.SAISIE_FACULTATIF, "Auteur", "utilisateur", UnivFmt.CONTEXT_ZONE);
						%>
						<p class="retrait">
							<input id="galerie_code_redacteur_courant" type="checkbox" name="galerie_code_redacteur_courant" value="1" />
							<label for="galerie_code_redacteur_courant">ou utilisateur courant </label>
						</p>
						
						</fieldset>
						
						<fieldset><legend>SELECTION PAR DATE</legend>
							<p>
								<b>ATTENTION : Un seul choix possible sur les 3</b>
							</p>
							<p>
								<label for="galerie_selection" class="colonne">Période de saisie</label>
								<%fmt.insererCombo(out, infoBean, "galerie_selection", FormateurJSP.SAISIE_FACULTATIF, "periode_saisie_mediatheque", "P&eacute;riode"); %>
							</p>
							<p>
								<label for="galerie_jour" class="colonne">OU médias crées au cours des</label>
								<input type="text" name="galerie_jour" value="" size="5" /> derniers jours
							</p>
							<p>	
								<label for="galerie_date_debut" class="colonne">OU médias créés entre le</label>
								<% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "galerie_date_debut", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, "Date début"); %>
								et le
								<% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "galerie_date_fin", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_DATE, 0, 10, "Date fin"); %>
							</p>
								<script type="text/javascript">
									var oPeriodField = document.forms[0].galerie_selection;
									oPeriodField.onchange = function()
									{
										if (this.selectedIndex != 0)
											RefreshDateFields(this);
									}
									var oNbDays = document.forms[0].galerie_jour;
									oNbDays.onchange = function()
									{
										if (this.value != '')
											RefreshDateFields(this);
									}
									var oDtstart = document.forms[0].galerie_date_debut;
									oDtstart.onblur = function()
									{
										if (this.value != '')
											RefreshDateFields(this);
									}
									var oDtend = document.forms[0].galerie_date_fin;
									oDtend.onblur = function()
									{
										if (this.value != '')
											RefreshDateFields(this);
									}
									var oTriAsc = document.getElementById('galerie_tri_date_asc');
									var oTriDesc = document.getElementById('galerie_tri_date_desc');
								
									// met a jour les champs en exclusion mutuelle
									function RefreshDateFields(oFromField)
									{
										if (oPeriodField != oFromField)
										{
											oPeriodField.selectedIndex = 0;
										}
										if (oNbDays != oFromField)
										{
											oNbDays.value = '';
										}
										if (oDtstart != oFromField && oDtend != oFromField)
										{
											oDtstart.value = '';
											oDtend.value = '';
											oTriAsc.checked=false;
											oTriDesc.checked=false;
										}
									}
								</script>
							</p>
						</fieldset>
						<fieldset><legend>TRI PAR DATE</legend>
								<p>
									<input type="radio" id="galerie_tri_date_asc" name="galerie_tri_date" value="DATE_ASC"/>
									<label for="galerie_tri_date_asc">chronologique croissant</label>
								</p>
								<p>
									<input type="radio" id="galerie_tri_date_desc" name="galerie_tri_date" value="DATE_DESC"/>
									<label for="galerie_tri_date_desc">chronologique décroissant</label>
								</p>
						</fieldset>

						<%JSPIncludeHelper.includeExtensionTemplate(out, getServletContext(), request, response, "", MediathequeHelper.getTemplateStylesTagGalerie());%>
						
						<p>
							<label for="galerie_affichage_onglet" class="colonne">Afficher un onglet par type de média</label>
							<input type="checkbox" name="galerie_affichage_onglet" value="1" />
						</p>
						
						<p>
							<label for="galerie_nombre" class="colonne">Nombre maxi de médias affichés</label>
							<%fmt.insererChampSaisie(out, infoBean, "galerie_nombre", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 5, "Nombre maxi");%>
						</p>
						</fieldset>
				</div>
			</form>
		</div><!-- #content -->
		<resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
		<script src="/adminsite/scripts/backoffice.js"></script>
	</body>
</html>
