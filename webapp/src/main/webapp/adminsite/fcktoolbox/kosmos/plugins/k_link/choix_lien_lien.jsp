<%@page import="com.univ.utils.EscapeString"%>
<%@page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!-- link rel="stylesheet" type="text/css" href="/adminsite/general.css" media="screen" /-->
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<title>Choix du type de lien vers un objet Lien</title>
</head>
<body id="body_couleur_fond">
<form action="/servlet/com.jsbsoft.jtf.core.SG">

<div class="content_popup">
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td align="center">
			<table>
				<tr>
					<td width="80">Type :</td>
					<td style="text-align:left;">
						<input name="TYPE_LIEN" value="1" type="radio">lien vers la fiche
						<br />
						<input name="TYPE_LIEN" value="2" type="radio">lien direct vers le site
					</td>
				</tr>
				<tr>
					<td align="center" colspan="2">
						<br/><br/>
						<input class="bouton" type="button" name="ENREGISTRER" value="OK" onclick="validerChoixLien();"/>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
</div>
</form>

<script type="text/javascript">

	function validerChoixLien(){
		var choix = '';
		for (i=0;i<window.document.forms[0].TYPE_LIEN.length;i++){
			if (window.document.forms[0].TYPE_LIEN[i].checked)
				choix=window.document.forms[0].TYPE_LIEN[i].value;
		}
		if(window.iFrameRegistration){
			var sCode = '<%= request.getParameter("CODE") + ",LANGUE=" + request.getParameter("LANGUE") + ",TYPE=lien" %>';
			iFrameHelper.sendValues(window.iFrameRegistration, {sCode: sCode, libelle: '<%= EscapeString.escapeJavaScript(request.getParameter("LIBELLE"))%>', langue: '<%=infoBean.getString("LANGUE")%>'});
		}else{
			var rubrique = '';
			<%
				if (request.getParameter("RUBRIQUE")!=null)
				{
			%>
				 rubrique = '<%=request.getParameter("RUBRIQUE")%>';
			<%
				}
			%>
	
			if (choix == '1'){
				window.parent.CreateLienInterne('<%= request.getParameter("OBJET") %>', '<%= request.getParameter("CODE") %>', '<%= EscapeString.escapeJavaScript(request.getParameter("LIBELLE"))%>', rubrique, '<%= request.getParameter("LANGUE") %>' ); 
			}
			if (choix == '2'){
				window.parent.CreateLienLien('<%= request.getParameter("CODE")+",LANGUE=" + request.getParameter("LANGUE")%>');
			}
		}
	}

</script>
</body>
</html>
