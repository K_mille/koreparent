<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<%@page import="com.univ.utils.EscapeString"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen">
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<title></title>
</head>
<body class="toolbox">
<% 
final String code = request.getParameter("CODE");
if(StringUtils.isNotBlank(code)) {
%>
	<script type="text/javascript">
	(function(){
		window.parent.CreateLienRecherche('<%=EscapeString.escapeJavaScript(request.getParameter("CODE"))%>', '<%=EscapeString.unescapeURL(request.getParameter("LIBELLE"))%>');
	})();
	</script>
<%} else { %>
	<div id="content">
		<ul><%
			for (Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
				String nomObjet = objet.getNomObjet();
				if (objet.isStrictlyCollaboratif() || nomObjet.equalsIgnoreCase("PAGELIBRE") || nomObjet.equalsIgnoreCase("COMMENTAIRE")){
       				continue;
				}
				%><li>
					<a href="/servlet/com.kportal.servlet.LienPopupServlet?CODE=<%=nomObjet.toUpperCase() %>&LIBELLE=<%=EscapeString.escapeAttributHtml(objet.getLibelleAffichable())%>&TOOLBOX=LIEN_RECHERCHE"><%=objet.getLibelleAffichable()%></a>
				</li><%
			}
			%>
		</ul>
	</div>
<%} %>
</body>
</html>
