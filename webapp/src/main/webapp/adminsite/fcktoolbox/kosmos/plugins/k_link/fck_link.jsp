<%@ page import="com.kportal.core.config.PropertyHelper" errorPage="/adminsite/jsbexception.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<!--
 * FCKeditor - The text editor for Internet - http://www.fckeditor.net
 * Copyright (C) 2003-2008 Frederico Caldeira Knabben
 *
 * == BEGIN LICENSE ==
 *
 * Licensed under the terms of any of the following licenses at your
 * choice:
 *
 *  - GNU General Public License Version 2 or later (the "GPL")
 *    http://www.gnu.org/licenses/gpl.html
 *
 *  - GNU Lesser General Public License Version 2.1 or later (the "LGPL")
 *    http://www.gnu.org/licenses/lgpl.html
 *
 *  - Mozilla Public License Version 1.1 or later (the "MPL")
 *    http://www.mozilla.org/MPL/MPL-1.1.html
 *
 * == END LICENSE ==
 *
 * Link dialog window.
-->
<%@page import="com.univ.objetspartages.om.ReferentielObjets"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Link Properties</title>
		<meta name="robots" content="noindex, nofollow" />
		<link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen" />
		<script src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js" type="text/javascript"></script>
		
		<script src="/adminsite/toolbox/toolbox_new.js" type="text/javascript"></script>
		<script src="/adminsite/toolbox/toolbox.js" type="text/javascript"></script>
		<% /* Le bouton ok n'est utilisé que pour les sites dont l'intranet est activé */
		   String dsiActive = PropertyHelper.getCoreProperty("dsi.activation");
		   boolean dsi = (dsiActive != null && dsiActive.equals("1") ? true : false);
		   if (dsi) { %>
			<script type="text/javascript">
				window.parent.SetOkButton( true ) ;
			</script>
		<% } %>
	</head>
	<body class="toolbox">
		<div id="content">
			<div>
				<p class="header_k_link">
					<label for="cmbLinkType" class="colonne" fckLang="DlgLnkType">Link Type</label>
					<select id="cmbLinkType" onchange="SetLinkType(this.value);">
						<option value="url" fckLang="DlgLnkTypeURL">URL</option>
						<option value="anchor" fckLang="DlgLnkTypeAnchor">Anchor in this page</option>
						<option value="email" fckLang="DlgLnkTypeEMail">E-Mail</option>
										
						<option value="interne" fckLang="DlgLnkTypeLienInterne" selected="selected">Lien interne</option>
						<option value="rubrique" fckLang="DlgLnkTypeLienRubrique" >Lien vers une rubrique</option>
						
						<!--On affiche le lien de téléchargement de média que si on est dans un objet de contenu -->
							<option value="fichier" fckLang="DlgLnkTypeLienFichier" >Lien de téléchargement d'un média</option>
						<option value="recherche" fckLang="DlgLnkTypeLienFormRecherche">Lien vers un formulaire de recherche</option>
						<option value="requete" fckLang="DlgLnkTypeLienRequete" >Lien de requête</option>
						<% if (dsi) { %>
							<option value="intranet" fckLang="DlgLnkTypeLienIntranet">Lien intranet</option>
						<% } %>
						<% String specificChoixLien = PropertyHelper.getCoreProperty("choix_lien.specific"); %>
						<% if (specificChoixLien != null) { %>
							<option value="specific" fckLang="DlgLnkTypeLienSpecific">Choix spécifique</option>
						<% } %>
					</select>
					
					<input type="button" id="inputLienInterneRetourChoixTypeFiche" style="display:none" value="Retour " onclick="retourChoixTypeFicheLienInterne(<%= request.getParameter("LANGUE_FICHE")%>);" />
					<input type="button" id="inputLienRequeteRetourChoixTypeFiche" style="display:none" value="Retour choix du type de fiche" onclick="retourChoixTypeFicheLienRequete(<%= request.getParameter("LANGUE_FICHE")%>);" />
				</p>
				
				
				<div id="divLinkTypeEMail" style="display:none" class="header_k_link">
					<p><label for="txtEMailSubject" fckLang="DlgLnkEMailSubject" class="colonne">Message Subject</label>
					<input id="txtEMailSubject" style="WIDTH: 100%" type="text" />
					</p>
					<p>
					<label for="txtEMailBody" fckLang="DlgLnkEMailBody" class="colonne">Message Body</label>
					<textarea id="txtEMailBody" style="WIDTH: 100%" rows="3" cols="20"></textarea>
					</p>
					<p>
					<label for="cmbLinkTypeEmail" fckLang="DlgLnkEMailOrigine" class="colonne">Origine</label>
					<select id="cmbLinkTypeEmail" onchange="SetEmailType(this.value);">
						<option value="KportalEmail" selected="selected" fckLang="DlgLnkTypeEmailKportal">Contact Annuaire</option>
						<option value="ClassiqueEmail" fckLang="DlgLnkTypeEmailClassique">Autre adresse email</option>
					</select>
					</p>
				</div><!-- # -->
				
				<div id="divInfo" style="display:none;">
				
					<input id="tag_kportal" type="hidden" />
					
					<div id="divLinkTypeUrl" class="content_k_link">
						<table cellspacing="0" cellpadding="0" width="100%" border="0" dir="ltr">
							<tr>
								<td nowrap="nowrap">
									<label for="cmbLinkProtocol" fckLang="DlgLnkProto">Protocol</label><br />
									<select id="cmbLinkProtocol">
										<option value="http://" selected="selected">http://</option>
										<option value="https://">https://</option>
										<option value="ftp://">ftp://</option>
										<option value="news://">news://</option>
										<option value="" fckLang="DlgLnkProtoOther">&lt;other&gt;</option>
									</select>
								</td>
								<td nowrap="nowrap">&nbsp;</td>
								<td nowrap="nowrap" width="100%">
									<label for="txtUrl" fckLang="DlgLnkURL">URL</label><br />
									<input id="txtUrl" style="WIDTH: 100%" type="text" onkeyup="OnUrlChange();" onchange="OnUrlChange();" />
								</td>
							</tr>
						</table>
						<br />
						<div id="divBrowseServer">
						<input type="button" value="Browse Server" fckLang="DlgBtnBrowseServer" onclick="BrowseServer();" />
						</div>
					</div>
					
					<div id="divLinkTypeAnchor" style="display:none" class="content_k_link">
						<p fckLang="DlgLnkDescriptionNom">Indiquer le nom de l'ancre souhaitée</p>
						<p id="divTextAnchor">
								<label class="colonne" for="txtAnchorName" fckLang="DlgLnkAnchorByText">Put the name</label>
								<input id="txtAnchorName" type="text" />
						</p>
						<div id="divNoAnchor" style="display:none">
							<span fckLang="DlgLnkNoAnchors">&lt;No anchors available in the document&gt;</span>
						</div>
						<div id="divSelAnchor" style="display:none">
							<p fckLang="DlgLnkAnchorSel">Ou sélectionner une ancre présente dans cette zone de contenu</p>
							<label colonne="colonne" for="cmbAnchorName" fckLang="DlgLnkAnchorByName">By Anchor Name</label>
							<select id="cmbAnchorName" onchange="GetE('cmbAnchorId').value='';">
								<option value="" selected="selected"></option>
							</select>
						</div>
						
					</div><!-- #divLinkTypeAnchor -->
					
					<p id="divLinkTypeEMailClassique" style="display:none" class="content_k_link">
						<label for="txtEMailAddress" fckLang="DlgLnkEMail" class="colonne">E-Mail Address</label>
						<input id="txtEMailAddress" style="WIDTH: 100%" type="text" />
					</p>
					<%
						// test le processus de recherche annuaire
							String objet = "ANNUAIRE";
							if (ReferentielObjets.instancierFiche("ANNUAIRE")==null)
								objet = "ANNUAIREKSUP";
							
							// test les paramètres de la fiche parente
							String paramFiche = "";
							if (request.getParameter("CODE")!=null){
								paramFiche += "&amp;CODE="+request.getParameter("CODE");
							}
							if (request.getParameter("OBJET")!=null){
								paramFiche += "&amp;OBJET="+request.getParameter("OBJET");
							}
							if (request.getParameter("ID_FICHE")!=null){
								paramFiche += "&amp;ID_FICHE="+request.getParameter("ID_FICHE");
							}
							String paramLangue = "";
							if (request.getParameter("LANGUE_FICHE")!=null){
								paramLangue += "&amp;LANGUE_FICHE="+request.getParameter("LANGUE_FICHE");
							}
					%>
					<div id="divLinkTypeEMailKPortal" style="display:none;">
						<iframe width="100%" scrolling="auto" height="510px" frameborder="0" src="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%=ReferentielObjets.getExtension("0006")%>&amp;PROC=<%=ReferentielObjets.getProcessus("0006")%>&amp;ACTION=RECHERCHER&amp;TOOLBOX=MAILTO&amp;FCK_PLUGIN=TRUE<%=paramLangue%>"></iframe>
					</div>
					<div id="divLinkTypeLienInterne">
						<iframe id="iframeLinkTypeLienInterne" width="100%" scrolling="auto" height="510px" frameborder="0" src="/adminsite/template/recherche_back.jsp?TOOLBOX=LIEN_INTERNE&amp;FCK_PLUGIN=TRUE<%=paramLangue%>"></iframe>
					</div>
					<div id="divLinkTypeLienRubrique" style="display: none" >
						<iframe id="iframe_rubrique" name="iframe_rubrique" width="100%" scrolling="auto" height="510px" frameborder="0" src="choix_rubrique.jsp<%=((request.getParameter("LANGUE_FICHE")!=null)?"?LANGUE="+request.getParameter("LANGUE_FICHE"):"")%>"></iframe>
					</div>
					<div id="divLinkTypeLienFichier" style="display: none">
						<iframe width="100%" scrolling="auto" height="510px" frameborder="0" src="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&FCK_PLUGIN=TRUE&MODE_FICHIER=LIEN<%=paramFiche%>"></iframe>
					</div>
					<div id="divLinkTypeLienFormRecherche" style="display:none" class="content_k_link">
						<iframe width="100%" scrolling="auto" height="510px" frameborder="0" src="/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_recherche.jsp"></iframe>	
					</div>
					<div id="divLinkTypeLienRequete" style="display: none;">
						<iframe id="iframe_requete" name="iframe_requete" width="100%" scrolling="auto" height="510px" frameborder="0" src="/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&LANGUE_FICHE=<%= request.getParameter("LANGUE_FICHE") %>&amp;FCK_PLUGIN=TRUE"></iframe>
					</div>
					<div id="divLinkTypeLienIntranet" style="display: none" class="content_k_link">
						<% 
							//ces liens ne concernent que les sites dont l'intranet est activé
							if(dsi) {
						%>
								<iframe id="iframeLinkTypeLienIntranet" width="100%" scrolling="auto" height="400px" frameborder="0" src="/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_lien_intranet.jsp"></iframe>
						<%  } %>
					</div>
					<div id="divLinkTypeLienSpecific" style="display:none">
						divLinkTypeLienSpecific
					</div>
				</div>
				<div id="divUpload" style="display:none">
					<form id="frmUpload" method="post" target="UploadWindow" enctype="multipart/form-data" action="" onsubmit="return CheckUpload();">
						<span fckLang="DlgLnkUpload">Upload</span><br />
						<input id="txtUploadFile" style="WIDTH: 100%" type="file" size="40" name="NewFile" /><br />
						<br />
						<input id="btnUpload" type="button" value="Send it to the Server" fckLang="DlgLnkBtnUpload" onclick="document.forms[0].submit();" />
						<iframe name="UploadWindow" style="DISPLAY: none" src="javascript:void(0)"></iframe>
					</form>
				</div>
				<div id="divTarget" style="display: none">
					<table cellspacing="0" cellpadding="0" width="100%" border="0">
						<tr>
							<td nowrap="nowrap">
								<span fckLang="DlgLnkTarget">Target</span><br />
								<select id="cmbTarget" onchange="SetTarget(this.value);">
									<option value="" fckLang="DlgGenNotSet" selected="selected">&lt;not set&gt;</option>
									<option value="frame" fckLang="DlgLnkTargetFrame">&lt;frame&gt;</option>
									<option value="popup" fckLang="DlgLnkTargetPopup">&lt;popup window&gt;</option>
									<option value="_blank" fckLang="DlgLnkTargetBlank">New Window (_blank)</option>
									<option value="_top" fckLang="DlgLnkTargetTop">Topmost Window (_top)</option>
									<option value="_self" fckLang="DlgLnkTargetSelf">Same Window (_self)</option>
									<option value="_parent" fckLang="DlgLnkTargetParent">Parent Window (_parent)</option>
								</select>
							</td>
							<td>&nbsp;</td>
							<td id="tdTargetFrame" nowrap="nowrap" width="100%">
								<span fckLang="DlgLnkTargetFrameName">Target Frame Name</span><br />
								<input id="txtTargetFrame" style="WIDTH: 100%" type="text" onkeyup="OnTargetNameChange();"
									onchange="OnTargetNameChange();" />
							</td>
							<td id="tdPopupName" style="DISPLAY: none" nowrap="nowrap" width="100%">
								<span fckLang="DlgLnkPopWinName">Popup Window Name</span><br />
								<input id="txtPopupName" style="WIDTH: 100%" type="text" />
							</td>
						</tr>
					</table>
					<br />
					<table id="tablePopupFeatures" style="DISPLAY: none" cellspacing="0" cellpadding="0" align="center"
						border="0">
						<tr>
							<td>
								<span fckLang="DlgLnkPopWinFeat">Popup Window Features</span><br />
								<table cellspacing="0" cellpadding="0" border="0">
									<tr>
										<td valign="top" nowrap="nowrap" width="50%">
											<input id="chkPopupResizable" name="chkFeature" value="resizable" type="checkbox" /><label for="chkPopupResizable" fckLang="DlgLnkPopResize">Resizable</label><br />
											<input id="chkPopupLocationBar" name="chkFeature" value="location" type="checkbox" /><label for="chkPopupLocationBar" fckLang="DlgLnkPopLocation">Location
												Bar</label><br />
											<input id="chkPopupManuBar" name="chkFeature" value="menubar" type="checkbox" /><label for="chkPopupManuBar" fckLang="DlgLnkPopMenu">Menu
												Bar</label><br />
											<input id="chkPopupScrollBars" name="chkFeature" value="scrollbars" type="checkbox" /><label for="chkPopupScrollBars" fckLang="DlgLnkPopScroll">Scroll
												Bars</label>
										</td>
										<td></td>
										<td valign="top" nowrap="nowrap" width="50%">
											<input id="chkPopupStatusBar" name="chkFeature" value="status" type="checkbox" /><label for="chkPopupStatusBar" fckLang="DlgLnkPopStatus">Status
												Bar</label><br />
											<input id="chkPopupToolbar" name="chkFeature" value="toolbar" type="checkbox" /><label for="chkPopupToolbar" fckLang="DlgLnkPopToolbar">Toolbar</label><br />
											<input id="chkPopupFullScreen" name="chkFeature" value="fullscreen" type="checkbox" /><label for="chkPopupFullScreen" fckLang="DlgLnkPopFullScrn">Full
												Screen (IE)</label><br />
											<input id="chkPopupDependent" name="chkFeature" value="dependent" type="checkbox" /><label for="chkPopupDependent" fckLang="DlgLnkPopDependent">Dependent
												(Netscape)</label>
										</td>
									</tr>
									<tr>
										<td valign="top" nowrap="nowrap" width="50%">&nbsp;</td>
										<td></td>
										<td valign="top" nowrap="nowrap" width="50%"></td>
									</tr>
									<tr>
										<td valign="top">
											<table cellspacing="0" cellpadding="0" border="0">
												<tr>
													<td nowrap="nowrap"><span fckLang="DlgLnkPopWidth">Width</span></td>
													<td>&nbsp;<input id="txtPopupWidth" type="text" maxlength="4" size="4" /></td>
												</tr>
												<tr>
													<td nowrap="nowrap"><span fckLang="DlgLnkPopHeight">Height</span></td>
													<td>&nbsp;<input id="txtPopupHeight" type="text" maxlength="4" size="4" /></td>
												</tr>
											</table>
										</td>
										<td>&nbsp;&nbsp;</td>
										<td valign="top">
											<table cellspacing="0" cellpadding="0" border="0">
												<tr>
													<td nowrap="nowrap"><span fckLang="DlgLnkPopLeft">Left Position</span></td>
													<td>&nbsp;<input id="txtPopupLeft" type="text" maxlength="4" size="4" /></td>
												</tr>
												<tr>
													<td nowrap="nowrap"><span fckLang="DlgLnkPopTop">Top Position</span></td>
													<td>&nbsp;<input id="txtPopupTop" type="text" maxlength="4" size="4" /></td>
												</tr>
											</table>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</div>
				<div id="divAttribs" style="DISPLAY: none">
					<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
						<tr>
							<td valign="top" width="50%">
								<span fckLang="DlgGenId">Id</span><br />
								<input id="txtAttId" style="WIDTH: 100%" type="text" />
							</td>
							<td width="1"></td>
							<td valign="top">
								<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
									<tr>
										<td width="60%">
											<span fckLang="DlgGenLangDir">Language Direction</span><br />
											<select id="cmbAttLangDir" style="WIDTH: 100%">
												<option value="" fckLang="DlgGenNotSet" selected>&lt;not set&gt;</option>
												<option value="ltr" fckLang="DlgGenLangDirLtr">Left to Right (LTR)</option>
												<option value="rtl" fckLang="DlgGenLangDirRtl">Right to Left (RTL)</option>
											</select>
										</td>
										<td width="1%">&nbsp;&nbsp;&nbsp;</td>
										<td nowrap="nowrap"><span fckLang="DlgGenAccessKey">Access Key</span><br />
											<input id="txtAttAccessKey" style="WIDTH: 100%" type="text" maxlength="1" size="1" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" width="50%">
								<span fckLang="DlgGenName">Name</span><br />
								<input id="txtAttName" style="WIDTH: 100%" type="text" />
							</td>
							<td width="1"></td>
							<td valign="top">
								<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
									<tr>
										<td width="60%">
											<span fckLang="DlgGenLangCode">Language Code</span><br />
											<input id="txtAttLangCode" style="WIDTH: 100%" type="text" />
										</td>
										<td width="1%">&nbsp;&nbsp;&nbsp;</td>
										<td nowrap="nowrap">
											<span fckLang="DlgGenTabIndex">Tab Index</span><br />
											<input id="txtAttTabIndex" style="WIDTH: 100%" type="text" maxlength="5" size="5" />
										</td>
									</tr>
								</table>
							</td>
						</tr>
						<tr>
							<td valign="top" width="50%">&nbsp;</td>
							<td width="1"></td>
							<td valign="top"></td>
						</tr>
						<tr>
							<td valign="top" width="50%">
								<span fckLang="DlgGenTitle">Advisory Title</span><br />
								<input id="txtAttTitle" style="WIDTH: 100%" type="text" />
							</td>
							<td width="1">&nbsp;&nbsp;&nbsp;</td>
							<td valign="top">
								<span fckLang="DlgGenContType">Advisory Content Type</span><br />
								<input id="txtAttContentType" style="WIDTH: 100%" type="text" />
							</td>
						</tr>
						<tr>
							<td valign="top">
								<span fckLang="DlgGenClass">Stylesheet Classes</span><br />
								<input id="txtAttClasses" style="WIDTH: 100%" type="text" />
							</td>
							<td></td>
							<td valign="top">
								<span fckLang="DlgGenLinkCharset">Linked Resource Charset</span><br />
								<input id="txtAttCharSet" style="WIDTH: 100%" type="text" />
							</td>
						</tr>
					</table>
					<table cellspacing="0" cellpadding="0" width="100%" align="center" border="0">
						<tr>
							<td>
								<span fckLang="DlgGenStyle">Style</span><br />
								<input id="txtAttStyle" style="WIDTH: 100%" type="text" />
							</td>
						</tr>
					</table>
				</div>
			</div><!-- .fieldset.neutre -->
		</div><!-- #content -->
		<script src="/adminsite/fcktoolbox/kosmos/plugins/k_link/js/k_link.js" type="text/javascript"></script>
	</body>
</html>
