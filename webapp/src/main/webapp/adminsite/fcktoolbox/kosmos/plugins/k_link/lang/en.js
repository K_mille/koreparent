// Labels associated with buttons
FCKLang['KPortalLink'] = 'Link';

// Dialog box titles
FCKLang['DlgKPortalLinkTitle'] = 'Insert a link';

// Labels associated with dialog box items
// TODO CFL : Traduire les libellés
FCKLang['DlgLnkTypeLienInterne'] = 'Lien interne';
FCKLang['DlgLnkTypeLienRubrique'] = 'Lien vers une rubrique';
FCKLang['DlgLnkTypeLienFichier'] = 'Lien de téléchargement d\'un fichier';
FCKLang['DlgLnkTypeLienPhototheque'] = 'Lien vers une image de la photothèque';
FCKLang['DlgLnkTypeLienFormRecherche'] = 'Lien vers un formulaire de recherche';
FCKLang['DlgLnkTypeLienRequete'] = 'Lien de requête';
FCKLang['DlgLnkTypeLienIntranet'] = 'Lien intranet';
FCKLang['DlgLnkTypeLienSpecific'] = 'Autres choix';
FCKLang['DlgLnkTypeEMail'] = 'Adresse Mail';
FCKLang['DlgLnkContactAnnuaire'] = 'Contact annuaire';
FCKLang['DlgKPortalFicheSearchLabel'] = 'Rechercher une fiche';
FCKLang['DlgLnkEMailOrigine'] = 'Origine de l\'adresse email';
FCKLang['DlgLnkTypeEmailKportal'] = 'Contact annuaire';
FCKLang['DlgLnkTypeEmailClassique'] = 'Autre adresse email';
FCKLang['DlgLnkChoixRubrique'] = 'Choisir une rubrique';
FCKLang['DlgLnkChoixFichier'] = 'Choisir un média';
FCKLang['DlgLnkDefinirRequete'] = 'Définir la requête';

// Error messages
FCKLang['DlgKPortalLinkErrMsg'] = 'Select some text for the link.';
FCKLang['DlnLnkMsgNoLienInterne'] = 'Veuillez sélectionner une fiche.';
FCKLang['FCKLang.DlnLnkMsgNoRubrique'] = 'Veuillez sélectionner une rubrique.';
FCKLang['FCKLang.DlnLnkMsgNoFichier'] = 'Veuillez sélectionner un fichier.';
FCKLang['FCKLang.DlnLnkMsgNoFormRecherche'] = 'Veuillez sélectionner un formulaire de recherche.';
FCKLang['FCKLang.DlnLnkMsgNoRequete'] = 'Veuillez renseigner une requête';
FCKLang['FCKLang.DlnLnkMsgNoIntranet'] = 'Veuillez sélectionner un lien intranet';
