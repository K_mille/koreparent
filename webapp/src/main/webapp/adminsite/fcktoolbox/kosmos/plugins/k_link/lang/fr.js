// Surcharge des libellés de fckeditor
FCKLang['DlgBtnOK'] = 'Valider';

// Libellés associés aux boutons
FCKLang['KPortalLink'] = 'Lien';

// Titres des boites de dialogue
FCKLang['DlgKPortalLinkTitle'] = 'Insérer un lien';

// Libellés des éléments des boites de dialogue
FCKLang['DlgLnkTypeLienInterne'] = 'Lien interne';
FCKLang['DlgLnkTypeLienRubrique'] = 'Lien vers une rubrique';
FCKLang['DlgLnkTypeLienFichier'] = 'Lien de téléchargement d\'un média';
FCKLang['DlgLnkTypeLienPhototheque'] = 'Lien vers une image de la photothèque';
FCKLang['DlgLnkTypeLienFormRecherche'] = 'Lien vers un formulaire de recherche';
FCKLang['DlgLnkTypeLienRequete'] = 'Lien de requête';
FCKLang['DlgLnkTypeLienIntranet'] = 'Lien intranet';
FCKLang['DlgLnkTypeLienSpecific'] = 'Autres choix';
FCKLang['DlgLnkTypeEMail'] = 'Adresse Mail';
FCKLang['DlgLnkContactAnnuaire'] = 'Contact annuaire';
FCKLang['DlgKPortalFicheSearchLabel'] = 'Rechercher une fiche';
FCKLang['DlgLnkEMailOrigine'] = 'Origine de l\'adresse email';
FCKLang['DlgLnkTypeEmailKportal'] = 'Contact annuaire';
FCKLang['DlgLnkTypeEmailClassique'] = 'Autre adresse email';
FCKLang['DlgLnkChoixRubrique'] = 'Choisir une rubrique';
FCKLang['DlgLnkChoixFichier'] = 'Choisir un média';
FCKLang['DlgLnkDefinirRequete'] = 'Définir la requête';

// Messages d'erreur
FCKLang['DlgKPortalLinkErrMsg'] = 'Sélectionnez le texte qui servira de lien.';
FCKLang['DlnLnkMsgNoLienInterne'] = 'Veuillez sélectionner une fiche.';
FCKLang['FCKLang.DlnLnkMsgNoRubrique'] = 'Veuillez sélectionner une rubrique.';
FCKLang['FCKLang.DlnLnkMsgNoFichier'] = 'Veuillez sélectionner un fichier.';
FCKLang['FCKLang.DlnLnkMsgNoFormRecherche'] = 'Veuillez sélectionner un formulaire de recherche.';
FCKLang['FCKLang.DlnLnkMsgNoRequete'] = 'Veuillez renseigner une requête';
FCKLang['FCKLang.DlnLnkMsgNoIntranet'] = 'Veuillez sélectionner un lien intranet';

// Saisie d'une ancre
FCKLang['FCKLang.DlgLnkDescriptionNom'] = 'Indiquer le nom de l\'ancre souhaitée';
FCKLang['FCKLang.DlgLnkAnchorSel'] = 'Ou sélectionner une ancre présente dans cette zone de contenu';
FCKLang['FCKLang.DlgLnkAnchorByName'] = 'Par nom';
FCKLang['FCKLang.DlgLnkAnchorById'] = 'Par id';
FCKLang['FCKLang.DlgLnkAnchorByText'] = 'Nom de l\'ancre';
FCKLang['FCKLang.DlgLnkNoAnchors'] = 'Pas d\'ancre disponnible dans cette zone de contenu';
