<%@page import="java.net.URLDecoder"%>
<%@ page import="java.util.List, org.apache.commons.lang3.StringUtils, com.jsbsoft.jtf.textsearch.ResultatRecherche, com.univ.utils.ContexteUniv, com.univ.utils.ContexteUtil, com.univ.utils.RequeteUtil" %>

<%
/* Initialisation du contexte (infos utilisateur, connexion BdD,...) */
ContexteUniv ctx = ContexteUtil.getContexteUniv();
ctx.setJsp(this);
String codeObjet = request.getParameter("OBJET");
String requete = request.getParameter("REQUETE");
if (StringUtils.isNotBlank(requete)) {
	requete = "OBJET="+codeObjet+"&"+requete ;
}
requete = URLDecoder.decode(objet, "UTF-8");  	
List<ResultatRecherche> listeFiches = RequeteUtil.traiterRequete(ctx, objet);
if (listeFiches.size() > 0) {
%>
<p><%= listeFiches.size() %> résultat(s)</p>
<ul>
for (ResultatRecherche resultatRecherche : listeFiches) {
	if (resultatRecherche.isResultatFicheUniv()) {
		FicheUniv fiche = RequeteUtil.lireFiche(resultatRecherche);
		if (fiche != null) { %>
			<li><a href="[id-fiche]"<%= codeObjet %>;<%= fiche.getCode() %>;<%= fiche.getLangue() %>[/id-fiche]"><%= fiche.getLibelleAffichable() %></a></li>
		}
	}
}
</ul>
<% } %>