var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;

/** ICI LE CODE POUR GERER LES FAKE TAGS **/
var oFakeImage = dialog.Selection.GetSelectedElement();
var oSpanRss;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_krss'))
        oSpanRss = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

/** FIN DU CODE POUR GERER LES FAKE TAGS**/

function Ok() {
    switch (GetE('typeRss').value) {
        case 'import' :
            sTag = 'traitement;rss;';
            var sNbMax = window.document.getElementById('rss_nbmax');
            if (sNbMax.value != '')
                sTag += sNbMax.value + '+';
            var oUrl = window.document.getElementById('rss_url');
            if (oUrl.value == '') {
                alert('Saisissez une url');
                oUrl.focus();
                return false;
            }
            else {
                sTag += oUrl.value;
            }
            insertTag('[' + sTag + ']');
            return true;
            break;

        case 'export' :
            frames['iframe_requete'].sauvegarderRequete();
            return true;
    }

}

//#### Rss type selection.
function SetRssType(rssType) {
    ShowE('importRss', (rssType == 'import'));
    ShowE('exportRss', (rssType == 'export'));

    if (rssType == 'import') {
        dialog.SetOkButton(true);
    }
    if (rssType == 'export') {
        dialog.SetOkButton(false);
    }
}
function insertTag(sTag) {
    oEditor.FCKUndo.SaveUndoStep();
    if (!oSpanRss) {
        oSpanRss = FCK.EditorDocument.createElement('SPAN');
        oFakeImage = null;
    }

    oSpanRss.innerHTML = sTag;
    oSpanRss.setAttribute('krss_span', 'true');

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_RSS', oSpanRss);
        oFakeImage.setAttribute('_krss', 'true', 0);
        oFakeImage.setAttribute('src', FCKConfig.PluginsPath + 'k_rss/images/rss.png');
        oFakeImage.setAttribute('title', sTag);

        oFakeImage = FCK.InsertElement(oFakeImage);
    }

    oFakeImage.setAttribute('title', sTag);
}

function insertKListe_lienRequete(objet, requete, libelle) {
    var typeFluxFeed = frames['iframe_requete'].window.document.getElementById('TYPE_FLUX_FEED').value;
    if (typeFluxFeed != '0000')
        requete += "&TYPE_FLUX_FEED=" + typeFluxFeed;

    var libelleFluxFeed = frames['iframe_requete'].window.document.getElementById('FLUX_FEED_LIBELLE').value;
    if (libelleFluxFeed == '')
        libelleFluxFeed = 'flux';
    else
        requete += "&DESCRIPTION=" + libelleFluxFeed;

    //var txtHtml = "[traitement;requete;"+conversionRequete(objet,requete)+"]";
    var txtHtml = "<a href=\"/adminsite/webservices/export_rss.jsp?objet=" + objet + "&" + requete + "\">" + libelleFluxFeed.replace(/\"/g, '&quot;') + "</a>";

    oEditor.FCKUndo.SaveUndoStep();
    if (!oSpanRss) {
        oSpanRss = FCK.EditorDocument.createElement('SPAN');
        oFakeImage = null;
    }

    oSpanRss.innerHTML = txtHtml;
    oSpanRss.setAttribute('krss_span', 'true');

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_RSS', oSpanRss);
        oFakeImage.setAttribute('_krss', 'true', 0);
        oFakeImage.setAttribute('src', FCKConfig.PluginsPath + 'k_rss/images/rss.png');
        oFakeImage.setAttribute('title', libelleFluxFeed);

        oFakeImage = FCK.InsertElement(oFakeImage);
    }

    oFakeImage.setAttribute('title', txtHtml);
}

// CFL 20080618 : méthode importée depuis toolbox.js
// JSS 20020612-001 Type d'insertion (liste)
/* Remplacement des & par des # (plantage NetWord) */
function conversionRequete(objet, code) {

    do {
        i = code.indexOf('&');
        if (i != -1) {
            code = code.substring(0, i) + "#" + code.substring(i + 1, code.length);
        }
    }
    while (i != -1);

    param = "objet=" + objet.toUpperCase();
    if (code.length > 0)
        param = param + "#" + code;

    return param;
}
