<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<%@ taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<title>Link Properties</title>
		<meta name="robots" content="noindex, nofollow" />
		<resources:link group="stylesBo"/>
		<link rel="stylesheet" media="screen, projection"  href="/adminsite/styles/screen.css">
		<script src="/adminsite/fcktoolbox/fckeditor/editor/dialog/common/fck_dialog_common.js" type="text/javascript"></script>
		<script src="/adminsite/fcktoolbox/kosmos/plugins/k_rss/js/k_rss.js" type="text/javascript"></script>
		<script src="/adminsite/toolbox/toolbox_new.js" type="text/javascript"></script>
		<script src="/adminsite/toolbox/toolbox.js" type="text/javascript"></script>
	</head>
	<body onload="SetRssType('import')" class="popup">
	
		<div class="header_k_link">
			<label class="colonne">Action</label>
			<select id="typeRss" onchange="SetRssType(this.value);">
				<option value="import">Insérer un flux</option>
				<option value="export">Générer un flux</option>
			</select>
		</div><!-- .exportRss -->
		
		<div id="importRss" class="content_k_link popup-tag" style="display:none;">
			<div id="content">
				<p>
					<label for="rss_url" class="colonne">Saisir l'url (*) </label>
					<input type="text" id="rss_url" name="rss_url" value="" />
				</p>
				<p>
					<label for="rss_nbmax" class="colonne">Nombre d'éléments max.</label>
					<input type="text" id="rss_nbmax" name="rss_nbmax" value="" />
				</p>
			</div><!-- #content -->
		</div><!-- #importRss -->
		
		<div id="exportRss" style="display:none">
			<iframe id="iframe_requete" name="iframe_requete" width="100%" scrolling="auto" height="450px" frameborder="0" src="/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&RSS_FEED=1&FCK_PLUGIN=TRUE"></iframe>
		</div><!-- #exportRss -->
		
	</body>
</html>
