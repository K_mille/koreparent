FCKCommands.RegisterCommand('KPortalTag', new FCKDialogCommand('KPortalTag', FCKLang['DlgKPortalTagTitle'], FCKConfig.PluginsPath + 'k_tags/k_tags.jsp', 960, 500));

var oKPortalTag = new FCKToolbarButton('KPortalTag', FCKLang['KPortalTag']);

oKPortalTag.IconPath = FCKConfig.PluginsPath + 'k_tags/images/tag_icone.png';

FCKToolbarItems.RegisterItem('KPortalTag', oKPortalTag);


FCKDocumentProcessor.AppendNew().ProcessDocument = function (document) {
    var aSpans = document.getElementsByTagName('SPAN');

    var oSpan;
    var i = aSpans.length - 1;
    while (i >= 0 && ( oSpan = aSpans[i--] )) {
        if (oSpan.getAttribute('ktag_span')) {
            var oImg = FCKDocumentProcessor_CreateFakeImage('K_TAG', oSpan.cloneNode(true));
            oImg.setAttribute('_ktag', 'true', 0);
            oImg.setAttribute('src', FCKConfig.PluginsPath + 'k_tags/images/tag.gif');
            oImg.setAttribute('title', oSpan.innerHTML);

            oSpan.parentNode.insertBefore(oImg, oSpan);
            oSpan.parentNode.removeChild(oSpan);
        }
    }
}

/*FCK.ContextMenu.RegisterListener( {
 AddItems : function( menu, tag, tagName )
 {
 if ( tagName == 'IMG' && tag.getAttribute( '_ktag' ) )
 {
 menu.AddSeparator() ;
 menu.AddItem( 'KPortalTag', FCKLang.KTagProperties, FCKConfig.PluginsPath + 'k_tags/images/tag_icone.png' ) ;
 }
 }}
 );*/