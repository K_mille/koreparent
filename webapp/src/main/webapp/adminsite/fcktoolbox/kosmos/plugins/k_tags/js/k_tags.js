var dialog = window.parent;
var oEditor = dialog.InnerDialogLoaded();

var FCK = oEditor.FCK;
var FCKLang = oEditor.FCKLang;
var FCKConfig = oEditor.FCKConfig;
var FCKRegexLib = oEditor.FCKRegexLib;
var FCKTools = oEditor.FCKTools;


/** ICI LE CODE POUR GERER LES FAKE TAGS **/
var oFakeImage = dialog.Selection.GetSelectedElement();
var oSpanTag;

if (oFakeImage) {
    if (oFakeImage.tagName == 'IMG' && oFakeImage.getAttribute('_ktag'))
        oSpanTag = FCK.GetRealElement(oFakeImage);
    else
        oFakeImage = null;
}

/** FIN DU CODE POUR GERER LES FAKE TAGS**/

window.onload = function () {
    dialog.SetAutoSize(true);

    // Translate the dialog box texts.
    oEditor.FCKLanguageManager.TranslatePage(document);

    loadSelection();

    //init();

    // Load the selected link information (if any).
    //LoadSelection() ;
}

function loadSelection() {
    if (!oSpanTag) return;

    var sTag = oSpanTag.innerHTML;
    ShowE('tags_type', true);
    // TAGS UTILISATEURS
    if ('[nom]' == sTag || '[prenom]' == sTag || '[profil]' == sTag || '[intituleprofil]' == sTag || '[ksession]' == sTag || '[centresinteret]' == sTag) {
        GetE('choix_tag').value = 'tag_util';
        document.getElementsByName('tags_type')[0].value = 'tag_util';
        ShowE('tag_util', true);
        //cette façon de faire fonctionne pour ces tags car la value du select est égale au contenu du tag.
        var valueChoixTag = sTag.replace('[', '');
        valueChoixTag = valueChoixTag.replace(']', '');
        document.getElementsByName('choix_tag_util')[0].value = valueChoixTag;
    }
    //TAGS GROUPE
    if ('[traitement;debutgroupe]' == sTag || '[traitement;fingroupe]' == sTag || '[groupe]' == sTag || '[intitulegroupe]' == sTag || '[pagegroupe]' == sTag) {
        GetE('choix_tag').value = 'tag_groupe';
        document.getElementsByName('tags_type')[0].value = 'tag_groupe';
        ShowE('tag_groupe', true);
        //ici, la value du select est parfois différente du contenu du tag, donc on utilise un switch
        switch (sTag) {
            case '[traitement;debutgroupe]':
                document.getElementsByName('choix_tag_groupe')[0].value = 'debutgroupe';
                break;
            case '[traitement;fingroupe]':
                document.getElementsByName('choix_tag_groupe')[0].value = 'fingroupe';
                break;
            case '[groupe]':
                document.getElementsByName('choix_tag_groupe')[0].value = 'groupe';
                break;
            case '[intitulegroupe]':
                document.getElementsByName('choix_tag_groupe')[0].value = 'intitulegroupe';
                break;
            case '[pagegroupe]':
                document.getElementsByName('choix_tag_groupe')[0].value = 'pagegroupe';
                break;

            default:
                break;
        }
    }
    //TODO : CHARGEMENTS TAGS EXISTANTS : CONTRIBUTION, ESPACES COLLABS, SERVICES, AUTRES
}

function insertTag(sTag) {
    oEditor.FCKUndo.SaveUndoStep();
    if (!oSpanTag) {
        oSpanTag = FCK.EditorDocument.createElement('SPAN');
        oFakeImage = null;
    }

    oSpanTag.innerHTML = sTag;
    oSpanTag.setAttribute('ktag_span', 'true');

    if (!oFakeImage) {
        oFakeImage = oEditor.FCKDocumentProcessor_CreateFakeImage('K_TAG', oSpanTag);
        oFakeImage.setAttribute('_ktag', 'true', 0);
        oFakeImage.setAttribute('src', FCKConfig.PluginsPath + 'k_tags/images/tag.gif');
        oFakeImage.setAttribute('title', sTag);

        oFakeImage = FCK.InsertElement(oFakeImage);
    }

    oFakeImage.setAttribute('title', sTag);
}

function renvoyerTag(nom_select) {
    var tag = eval("document.forms.ktags." + nom_select + ".options[document.forms.ktags." + nom_select + ".selectedIndex].value");
    if (tag == '')
        return false;

    var sTag = '';

    if (tag == 'debutgroupe')
        sTag = '[traitement;debutgroupe]';
    else if (tag == 'debutgroupetype')
        sTag = '[traitement;debutgroupe;' + document.getElementById('comboTypeGroupe').value + ']';
    else if (tag == 'fingroupe')
        sTag = '[traitement;fingroupe]';
    else
        sTag = '[' + tag + ']';

    insertTag(sTag);
    //oEditor.FCK.InsertHtml(sTag);
}

function renvoyerTagContribution(nom_select) {

    var type = eval("document.forms.ktags." + nom_select + ".options[document.forms.ktags." + nom_select + ".selectedIndex].value");
    var intitule = document.forms.choix_tags.intitule.value;
    var rubrique = document.forms.choix_tags.rubrique.value;

    var tag = '[' + type;

    if (intitule != '')
        tag += ';' + intitule;
    if (rubrique != '') {

        if (intitule == '')
            tag += ';';

        tag += ';' + rubrique;
    }
    tag += ']';

    insertTag(tag);
    //renvoyerValeurs('tag',tag,'');
}

function renvoyerTagAutres() {
    var sType = document.getElementById('choix_tag_autre').value;
    var sTag = sType.substr(10, sType.length);
    if (sType == '')
        return false;
    else if (sType == 'tag_autre_page' || sType == 'tag_autre_url') {
        var oUrl = document.getElementById(sType + '_url');
        if (oUrl.value == '') {
            alert('Saisissez une url');
            oUrl.focus();
            return false;
        }
        else {
            sTag += ';' + oUrl.value;
        }
    }
    insertTag('[' + sTag + ']');
}

function renvoyerTagService(nom_select) {

    var tag = eval("document.forms.ktags." + nom_select + ".options[document.forms.ktags." + nom_select + ".selectedIndex].value");

    var code_service = document.forms.ktags.code_service.value;
    var nom_vue = document.forms.ktags.nom_vue.value;
    var nom_link = document.forms.ktags.nom_link.value;

    if (code_service == '0000') {
        alert("Veuillez choisir le service");
        document.forms.ktags.code_service.focus();
        return false;
    }

    if (tag == '') {
        alert("Veuillez saisir le tag");
        eval("document.forms.ktags." + nom_select + ".focus()");
        return false;
    }

    if (tag == 'service')
        tag = '[service;' + code_service + ']';
    else if (nom_vue != '' && tag == 'service_contenu')
        tag = '[service_debut;' + code_service + '][service_contenu;' + nom_vue + '][service_fin]';
    else if (nom_link != '' && tag == 'service_link')
        tag = '[service_debut;' + code_service + '][service_link;' + nom_link + '][service_fin]';
    else
        tag = '[service_debut;' + code_service + '][' + tag + '][service_fin]';

    insertTag(tag);
    //oEditor.FCK.InsertHtml(tag);
}

function afficheComposants(nom_select) {
    var lengthTabOptions = eval("document.forms.ktags." + nom_select + ".options.length");
    if (lengthTabOptions > 0) {
        for (i = 0; i < lengthTabOptions; i++) {
            var optionCourante = eval("document.forms.ktags." + nom_select + ".options[" + i + "].value");
            if (optionCourante != '' && window.document.getElementById(optionCourante)) {
                if (eval("document.forms.ktags." + nom_select + ".selectedIndex") == i)
                    window.document.getElementById(optionCourante).style.display = 'inline';
                else
                    window.document.getElementById(optionCourante).style.display = 'none';
            }
        }
        window.document.getElementById('content_popup').style.display = 'block';
    }
}

function setCritere(nom, val) {
    res = '';
    if (val && val != '')
        res = nom + '=' + val;
    return res;
}

function init() {
    afficheComposants('tags_type');
    var sTagType = document.forms.ktags.tags_type.value;
    if (sTagType == 'tag_contrib') {
        afficheComposants('tag_contrib_type');
        if (document.forms.ktags.tag_contrib_type.value == 'lien_fiches') {
            afficheComposants('lien_fiches_action');
        }
    }
    else if (sTagType == 'tag_collab') {
        afficheComposants('choix_tag_collab');
    }
    else if (sTagType == 'tag_service') {
        afficheComposants('choix_tag_service');
    }
    else if (sTagType == 'tag_autre') {
        afficheComposants('choix_tag_autre');
    }
}

//méthode appelée lors de la validation
function Ok() {
    var sTag = '';
    switch (GetE('choix_tag').value) {
        case '' :
            alert(FCKLang.DlnTagMsgNoTag);
            return false;
            break;

        case 'tag' :
            alert(FCKLang.DlnTagMsgNoTag);
            return false;
            break;

        case 'tag_util' :
            var result = renvoyerTag('choix_tag_util');
            if (result == false) {
                alert(FCKLang.DlnTagMsgNoTagUtil);
                return false;
            } else {
                break;
            }

        case 'tag_groupe' :
            var result = renvoyerTag('choix_tag_groupe');
            if (result == false) {
                alert(FCKLang.DlnTagMsgNoTagGroupe);
                return false;
            } else {
                break;
            }

        case 'lien_ajout' :
            var result = renvoyerTagLienAjoutContribution();
            if (result == false) {
                return false;
            } else {
                break;
            }

        case 'lien_modification' :
            renvoyerTagLienModificationContribution();
            break;

        case 'lien_suppression' :
            renvoyerTagLienSuppressionContribution();

            break;

        case 'lien_validation' :
            renvoyerTagLienValidationContribution();
            break;

        case 'liste_fiches' :
            var result = renvoyerTagListeFiches();
            if (result == false) {
                return false;
            } else {
                break;
            }

        case 'galerie' :
            var result = renvoyerTagGalerie();
            if (result == false) {
                return false;
            } else {
                break;
            }

        case 'intitule_espace' :
            renvoyerIntituleEspace();
            break;

        case 'liste_espaces' :
            var result = renvoyerTagListeEspaces();
            if (result == false) {
                return false;
            } else {
                break;
            }

        case 'tag_service' :
            var result = renvoyerTagService('choix_tag_service');
            if (result == false) {
                return false;
            } else {
                break;
            }

        case 'tag_autre' :
            var result = renvoyerTagAutres();
            if (result == false) {
                alert(FCKLang.DlnTagMsgNoTagAutre);
                return false;
            } else {
                break;
            }

        case 'tag_specific' :
            if (renvoyerTagSpecific) {
                var result = renvoyerTagSpecific('choix_tag_specific');
                if (result == false) {
                    alert(FCKLang.DlnTagMsgNoTag);
                    return false;
                } else {
                    break;
                }
            }
    }
    return true;
}

//sauvegarde le choix de tag effectue dans un champ caché
//la valeur sauvegardee sera utilisée lors de la validation (fonction Ok())
function saveChoixTag(choixTag) {
    GetE('choix_tag').value = choixTag;
}