function renvoyerTagLienAjoutContribution() {
    libelle = document.forms.ktags.lien_fiches_intitule.value;
    codeRubrique = document.forms.ktags.lien_ajout_code_rubrique.value;
    codeObjet = document.forms.ktags.lien_ajout_code_objet.value.replace(/;/g, ',');
    codeStructure = document.forms.ktags.lien_ajout_code_structure.value;

    /* FG 2007-02-28: Pb de codes groupes contenant des virgules */
    publicVise = document.forms.ktags.lien_ajout_public_vise.value.replace(/;/g, '@');
    publicVise = publicVise.replace(/;/g, ',');

    classCss = document.forms.ktags.lien_ajout_css.value;
    if (document.forms.ktags.lien_ajout_code_rubrique_courante.checked)
        codeRubrique = 'DYNAMIK';
    if (document.forms.ktags.lien_ajout_public_vise_utilisateur.checked)
        publicVise = 'DYNAMIK';
    if (codeObjet == '0000') {
        alert("Sélectionnez le type d'objet");
        document.forms.ktags.lien_ajout_code_objet.focus();
        return false;
    } else if (libelle == '')
        libelle = 'OBJET';
    insertTag('[traitement;lien_fiches;ACTION=AJOUTER' + setCritere('#OBJET', codeObjet) + setCritere('#CODE_RATTACHEMENT', codeStructure) + setCritere('#CODE_RUBRIQUE', codeRubrique) + setCritere('#DIFFUSION_PUBLIC_VISE', publicVise) + setCritere('#LIBELLE', libelle) + setCritere('#CLASSE_CSS', classCss) + ']');
}

function renvoyerTagLienModificationContribution() {
    libelle = document.forms.ktags.lien_fiches_intitule.value;
    classCss = document.forms.ktags.lien_modification_css.value;
    insertTag('[traitement;lien_fiches;ACTION=MODIFIER' + setCritere('#LIBELLE', libelle) + setCritere('#CLASSE_CSS', classCss) + ']');
}

function renvoyerTagLienSuppressionContribution() {
    libelle = document.forms.ktags.lien_fiches_intitule.value;
    classCss = document.forms.ktags.lien_suppression_css.value;
    insertTag('[traitement;lien_fiches;ACTION=SUPPRIMER' + setCritere('#LIBELLE', libelle) + setCritere('#CLASSE_CSS', classCss) + ']');
}

function renvoyerTagLienValidationContribution() {
    libelle = document.forms.ktags.lien_fiches_intitule.value;
    classCss = document.forms.ktags.lien_validation_css.value;
    insertTag('[traitement;lien_fiches;ACTION=VALIDER' + setCritere('#LIBELLE', libelle) + setCritere('#CLASSE_CSS', classCss) + ']');
}

function renvoyerTagListeFiches() {
    mode = document.forms.ktags.liste_fiches_mode.value;
    if (mode == '' || mode == '0000') {
        alert("Sélectionnez le mode");
        document.forms.ktags.liste_fiches_mode.focus();
        return false;
    }
    codeObjet = document.forms.ktags.liste_fiches_code_objet.value.replace(/;/g, ',');
    codeRubrique = document.forms.ktags.liste_fiches_code_rubrique.value;
    if (document.forms.ktags.liste_fiches_code_rubrique_courante.checked)
        codeRubrique = 'DYNAMIK';
    codeStructure = document.forms.ktags.liste_fiches_code_structure.value;
    /* FG 2007-02-28: Pb de codes groupes contenant des virgules (ex: kdecole) */
    publicVise = document.forms.ktags.liste_fiches_public_vise.value;
    publicVise = publicVise.replace(/,/g, '@');
    publicVise = publicVise.replace(/;/g, ',');
    espace = '';
    if (document.forms.ktags.liste_fiches_espace) {
        for (i = 0; i < document.forms.ktags.liste_fiches_espace.length; i++) {
            if (document.forms.ktags.liste_fiches_espace[i].checked)
                espace = document.forms.ktags.liste_fiches_espace[i].value;
        }
    }

    if (espace == '2') // fiches collab inclus en plus
        espace = '+4';
    if (espace == '3') // fiches collab inclus uniquement
        espace = '4';
    selection = document.forms.ktags.liste_fiches_selection.value;
    if (selection == '0000')
        selection = '';
    etat = document.forms.ktags.liste_fiches_etat.value;
    if (etat == '0000')
        etat = '';
    if (document.forms.ktags.liste_fiches_langue != null) {
        langue = document.forms.ktags.liste_fiches_langue.value;
        if (langue == '0000')
            langue = '';
    } else
        langue = '';
    redacteur = '';
    if (document.forms.ktags.liste_fiches_redacteur.checked)
        redacteur = 'DYNAMIK';
    if (document.forms.ktags.liste_fiches_public_vise_utilisateur.checked)
        publicVise = 'DYNAMIK';
    tri = '';
    increment = '';
    pagination = '';
    ajout = '';
    actions = '';
    titre = '';
    dateDebut = '';
    dateFin = '';
    tab = '1';
    liste = '1';
    lien = '';
    separateur = '';
    css = '';
    affichage = document.forms.ktags.liste_fiches_affichage.options[document.forms.ktags.liste_fiches_affichage.selectedIndex].value;
    if (affichage == '' || affichage == 'liste_fiches_liste' || affichage == 'liste_fiches_listetableau') {
        tri = document.forms.ktags.liste_fiches_tri.options[document.forms.ktags.liste_fiches_tri.selectedIndex].value;
        if (tri == '0000')
            tri = '';
        increment = document.forms.ktags.liste_fiches_increment.value;
        separateur = document.forms.ktags.liste_fiches_separateur.value;
        if (separateur == '0000')
            separateur = '';
        css = document.forms.ktags.liste_fiches_css.value;
    } else
        liste = '0';

    if (affichage == '' || affichage == 'liste_fiches_tableau' || affichage == 'liste_fiches_listetableau') {
        if (!document.forms.ktags.liste_fiches_pagination.checked)
            pagination = '0';
        if (!document.forms.ktags.liste_fiches_ajout.checked)
            ajout = '0';
        if (document.forms.ktags.liste_fiches_action_voir.checked)
            actions += 'V';
        if (document.forms.ktags.liste_fiches_action_modifier.checked) {
            if (actions != '')
                actions += '|';
            actions += 'M';
        }
        if (document.forms.ktags.liste_fiches_action_supprimer.checked) {
            if (actions != '')
                actions += '|';
            actions += 'S';
        }
        titre = document.forms.ktags.liste_fiches_titre.value;
        lien = document.forms.ktags.liste_fiches_intitule_lien.value;
        dateDebut = renvoyerCritereTableau(dateDebut, 'date_debut');
        dateFin = renvoyerCritereTableau(dateFin, 'date_fin');
        codeRubrique = renvoyerCritereTableau(codeRubrique, 'rubrique');
        codeStructure = renvoyerCritereTableau(codeStructure, 'structure');
        publicVise = renvoyerCritereTableau(publicVise, 'groupe');
        redacteur = renvoyerCritereTableau(redacteur, 'redacteur');
        langue = renvoyerCritereTableau(langue, 'langue');
        codeObjet = renvoyerCritereTableau(codeObjet, 'objet');
        etat = renvoyerCritereTableau(etat, 'etat');

    }
    else
        tab = '0';
    insertTag('[traitement;liste_fiches;' + setCritere('LISTE', liste) + setCritere('#TAB', tab) + setCritere('#MODE', mode) + setCritere('#ACTIONS', actions) + setCritere('#PAGE', pagination) + setCritere('#AJOUT', ajout) + setCritere('#ETAT', etat) + setCritere('#OBJET', codeObjet) + setCritere('#CODE_REDACTEUR', redacteur) + setCritere('#CODE_RUBRIQUE', codeRubrique) + setCritere('#CODE_RATTACHEMENT', codeStructure) + setCritere('#DIFFUSION_PUBLIC_VISE', publicVise) + setCritere('#DIFFUSION_MODE_RESTRICTION', espace) + setCritere('#DATE_DEBUT', dateDebut) + setCritere('#DATE_FIN', dateFin) + setCritere('#SELECTION', selection) + setCritere('#TRI', tri) + setCritere('#INCREMENT', increment) + setCritere('#TITRE', titre) + setCritere('#CLASSE_CSS', css) + setCritere('#LIEN', lien) + setCritere('#SEPARATEUR', separateur) + setCritere('#LANGUE', langue) + ']');
}

function renvoyerCritereTableau(str, lib) {
    critere = '';
    if (eval("document.forms.ktags.liste_fiches_tri_" + lib)) {
        if (eval("document.forms.ktags.liste_fiches_tri_" + lib + ".checked") && eval("document.forms.ktags.liste_fiches_req_" + lib + ".checked"))
            critere = str;
        else if (eval("document.forms.ktags.liste_fiches_tri_" + lib + ".checked") == false && eval("document.forms.ktags.liste_fiches_req_" + lib + ".checked") == false) {
            critere = str + '|0|0';
        } else {
            if (eval("document.forms.ktags.liste_fiches_req_" + lib + ".checked"))
                critere += '|1';
            else
                critere += '|0';
            if (eval("document.forms.ktags.liste_fiches_tri_" + lib + ".checked"))
                critere += '|1';
            else
                critere += '|0';
            critere = str + critere;
        }
    }
    return critere;
}

function affichageListeTableau() {
    tabOptions = document.forms.ktags.liste_fiches_affichage.options;
    if (tabOptions) {
        for (i = 0; i < tabOptions.length; i++) {
            if (tabOptions[i].selected && tabOptions[i].value == 'liste_fiches_listetableau') {
                window.document.getElementById('liste_fiches_liste').style.display = 'inline';
                window.document.getElementById('liste_fiches_tableau').style.display = 'inline';
            } else {
                if (window.document.getElementById(tabOptions[i].value)) {
                    if (tabOptions[i].selected)
                        window.document.getElementById(tabOptions[i].value).style.display = 'inline';
                    else
                        window.document.getElementById(tabOptions[i].value).style.display = 'none';
                }
            }
        }
    }

}