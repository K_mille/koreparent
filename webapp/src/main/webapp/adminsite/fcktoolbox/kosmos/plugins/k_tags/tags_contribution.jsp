<%@ page import="java.util.Hashtable" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.univ.collaboratif.om.Espacecollaboratif" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.RequeteurFiches" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<div id="tag_contrib" style="display:none">
	<p>
		<label for="type_insertion" class="colonne">Type d'insertion</label>
		<select id="type_insertion" name="tag_contrib_type" onchange="afficheComposants(this.name);saveChoixTag(this.value)">
			<option value="">Indéfini(e)</option>
			<option value="lien_fiches">Lien</option>
			<option value="liste_fiches">Liste / Tableau de bord</option>
		</select>
	</p>

	<div id="lien_fiches" style="display:none">
		<p>
			<label for="lien_fiches_intitule" class="colonne">Intitulé du lien</label>
			<input type="text" name="lien_fiches_intitule" id="lien_fiches_intitule" value="" size="20" />
		</p>
		<p>
			<label for="lien_fiches_action" class="colonne">Type d'action</label>
			<select name="lien_fiches_action" id="lien_fiches_action" onchange="afficheComposants(this.name);saveChoixTag(this.value)">
				<option value="">Indéfini(e)</option>
				<option value="lien_ajout">Ajout</option>
				<option value="lien_modification">Modification</option>
				<option value="lien_suppression">Suppression</option>
				<option value="lien_validation">Validation</option>
			</select>
		</p>
		<div id="lien_ajout" style="display:none">
			<p>
				<label for="lien_ajout_code_objet" class="colonne obligatoire">Objet (*)</label>
				<%
				Hashtable<String, String> hashOjbets = new Hashtable<String, String>();
				for (Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
					if (objet.isStrictlyCollaboratif() || objet.getNomObjet().equalsIgnoreCase("COMMENTAIRE")){
						continue;
					}
					hashOjbets.put(objet.getNomObjet().toUpperCase(), objet.getLibelleObjet());
				}
				infoBean.set("LISTE_CODE_OBJET", hashOjbets);
				univFmt.insererContenuComboHashtable( fmt, out, infoBean, "lien_ajout_code_objet", FormateurJSP.SAISIE_FACULTATIF,"LISTE_CODE_OBJET", "Objet", "");
				%>
			</p>
			<p class="message information">
				Prévaloriser la saisie avec les critères facultatifs suivants
			</p>
			<%
			infoBean.set("lien_ajout_code_rubrique","");
			univFmt.insererkMonoSelect(fmt, out, infoBean, "lien_ajout_code_rubrique", FormateurJSP.SAISIE_FACULTATIF, "Rubrique", "rubrique", UnivFmt.CONTEXT_ZONE);
			%>			
			<p class="retrait">
				<input type="checkbox" id="lien_ajout_code_rubrique_courante" name="lien_ajout_code_rubrique_courante" value="1" />
				<label for="lien_ajout_code_rubrique_courante">ou rubrique courante</label>
			</p>
			<%
			infoBean.set("lien_ajout_code_structure","");
			univFmt.insererkMonoSelect(fmt, out, infoBean, "lien_ajout_code_structure", FormateurJSP.SAISIE_FACULTATIF, "Structure", "", UnivFmt.CONTEXT_STRUCTURE);
			%>
			<%univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "lien_ajout_public_vise", FormateurJSP.SAISIE_FACULTATIF, "Public visé", "TMP_lien_ajout_public_vise", "Public visé", UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE, "");%>
			<p class="retrait">
				<input type="checkbox" id="lien_ajout_public_vise_utilisateur" name="lien_ajout_public_vise_utilisateur" value="1" />				
				<label for="lien_ajout_public_vise_utilisateur">ou groupes de l'utilisateur courant</label>
			</p>
			
			<p>
				<label for="lien_ajout_css" class="colonne">Classe CSS appliquée sur le lien</label>
				<input type="text" name="lien_ajout_css" id="lien_ajout_css" value="" size="10" />
			</p>
		</div>

		<div id="lien_modification" style="display:none">
			<p class="message information">
				Ce tag permet d'insérer un lien de modification pour la fiche courante si l'utilisateur en a les droits
			</p>
			
			<p>
				<label for="lien_modification_css" class="colonne">Classe CSS appliquée sur le lien</label>
				<input type="text" name="lien_modification_css" id="lien_modification_css" value="" size="10" />
			</p>
		</div>

		<div id="lien_suppression" style="display:none">
			<p class="message information">
				Ce tag permet d'insérer un lien de suppression pour la fiche courante si l'utilisateur en a les droits
			</p>
			
			<p>
				<label for="lien_suppression_css" class="colonne">Classe CSS appliquée sur le lien</label>
				<input type="text" name="lien_suppression_css" id="lien_suppression_css" value="" size="10" />
			</p>
		</div>

		<div id="lien_validation" style="display:none">
			<p class="message information">
				Ce tag permet d'insérer un lien vers les fiches à valider de l'utilisateur s'il en a les droits
			</p>
				
			<p>
				<label for="lien_validation_css" class="colonne">Classe CSS appliquée sur le lien</label>
				<input type="text" name="lien_validation_css" id="lien_validation_css" value="" size="10" />
			</p>
		</div>
	</div>

	<div id="liste_fiches" style="display:none">
		
		<p class="message information">
			Ce tag permet d'insérer une liste de contributions et/ou un lien vers un tableau de bord (sur les mêmes critères)
		</p>
		<p>
			<label for="liste_fiches_mode" class="colonne">Mode de contribution (*)</label>
			<%
				Hashtable<String, String> hash = new Hashtable<String, String>();
				hash.put(com.univ.utils.RequeteurFiches.MODE_CONSULTATION, "Consultation");
				hash.put(com.univ.utils.RequeteurFiches.MODE_MODIFICATION, "Modification");
				hash.put(com.univ.utils.RequeteurFiches.MODE_VALIDATION, "Validation");
				infoBean.set("LISTE_MODES", hash);
				fmt.insererComboHashtable( out, infoBean, "liste_fiches_mode", FormateurJSP.SAISIE_FACULTATIF, "LISTE_MODES" ); 
			%>
		</p>
		
		<h3>
			Critères de sélection (facultatif)
		</h3>
		<%univFmt.insererKmultiSelectLtl(fmt, out, infoBean, "liste_fiches_code_objet", FormateurJSP.SAISIE_FACULTATIF, "Objet", "LISTE_CODE_OBJET", "", UnivFmt.CONTEXT_DEFAULT);%>
		(tous si pas de sélection)
		<%
		infoBean.set("liste_fiches_code_rubrique", "");
		univFmt.insererkMonoSelect(fmt, out, infoBean, "liste_fiches_code_rubrique", FormateurJSP.SAISIE_FACULTATIF, "Rubrique", "rubrique", UnivFmt.CONTEXT_ZONE);
		%>
		<p class="retrait">
			<input type="checkbox" name="liste_fiches_code_rubrique_courante" id="liste_fiches_code_rubrique_courante" value="1" />
			<label for="liste_fiches_code_rubrique_courante">ou rubrique courante</label>
		</p>
		<%
			infoBean.set("liste_fiches_code_structure","");
			univFmt.insererkMonoSelect(fmt, out, infoBean, "liste_fiches_code_structure", FormateurJSP.SAISIE_FACULTATIF, "Structure", "", UnivFmt.CONTEXT_STRUCTURE);
		%>
		<%univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "liste_fiches_public_vise", FormateurJSP.SAISIE_FACULTATIF, "Public visé", "TMP_liste_fiches_public_vise", "Public visé", UnivFmt.CONTEXT_GROUPEDSI_PUBLIC_VISE, "");%>

		<p class="retrait">
			<input type="checkbox" name="liste_fiches_public_vise_utilisateur" id="liste_fiches_public_vise_utilisateur" value="1" />
			<label for="liste_fiches_public_vise_utilisateur">ou groupes de l'utilisateur courant</label>
		</p>
		<% if (Espacecollaboratif.isExtensionActivated()) { %>
		<p>
			<label for="liste_fiches_espace" class="colonne">Fiches des espaces collaboratifs<br/>(exclues par défaut)</label>
			<input type="radio" name="liste_fiches_espace" id="liste_fiches_espace1" value="1" /><label for="liste_fiches_espace1">les exclure</label>
			<input type="radio" name="liste_fiches_espace" id="liste_fiches_espace2" value="2" /><label for="liste_fiches_espace2">les inclure en plus</label>
			<input type="radio" name="liste_fiches_espace" id="liste_fiches_espace3" value="3" /><label for="liste_fiches_espace3">les inclure uniquement</label>
		</p>
		<% } %>
		
		<p>
			<label for="selection" class="colonne">Date de modification</label>
            <%
                hash = new Hashtable<String, String>();
                hash.put("0001","du jour");
                hash.put("0002","de la semaine");
                hash.put("0003","du mois");
                infoBean.set("LISTE_SELECTION_DATE",hash);
                fmt.insererComboHashtable(out, infoBean, "liste_fiches_selection", FormateurJSP.SAISIE_FACULTATIF, "LISTE_SELECTION_DATE","Date de modification" );
            %>
		</p>
		<p>
			<label for="etat objet" class="colonne">Etat</label>
			<%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "liste_fiches_etat", FormateurJSP.SAISIE_FACULTATIF, ReferentielObjets.getEtatsObjetFront(), "Etat");%>
		</p>
		<%	hash = LangueUtil.getListeLangues(ContexteUtil.getContexteUniv().getLocale());
			//on n'affiche pas l'option des langues si une seule langue est configurée pour le site
			if (hash.size() > 1) {
				infoBean.set("LISTE_LANGUES", hash); %>
				<p>
					<label for="langue" class="colonne">Langue</label>
					<%fmt.insererComboHashtable(out, infoBean, "liste_fiches_langue", FormateurJSP.SAISIE_FACULTATIF, "LISTE_LANGUES", "Langue");%>
				</p>
		<% } %>
		<p>
			<label for="redacteur" class="colonne">Rédacteur</label>
			<input type="checkbox" name="liste_fiches_redacteur" id="liste_fiches_redacteur" value="1" /><label for="liste_fiches_redacteur">utilisateur courant</label>
		</p>
		
		<p>Note : Par défaut, le tag affiche les 5 premiers résultats et le lien vers le tableau de bord correspondant</p>
		<p>
			<label for="liste_fiches_affichage" class="colonne">Affichage (complet par défaut)</label>
			<select name="liste_fiches_affichage" id="liste_fiches_affichage" onchange="affichageListeTableau()">
				<option value="">Indéfini(e)</option>
				<option value="liste_fiches_liste">Liste de fiches uniquement</option>
				<option value="liste_fiches_tableau">Lien vers le tableau de bord uniquement</option>
				<option value="liste_fiches_listetableau">Liste + Lien</option>
			</select>
		</p>
		
		<div id="liste_fiches_liste" style="display:none">
			<h3>
				Paramètrage de la liste
			</h3>
			<p>
				<label for="liste_fiches_tri" class="colonne">Tri</label>
				<%
					hash = new Hashtable<String, String>();
					hash.put(RequeteurFiches.TRI_DATE_ASC,"date");
					hash.put(RequeteurFiches.TRI_LIBELLE,"libellé");
					hash.put(RequeteurFiches.TRI_AUTEUR,"auteur");
					hash.put(RequeteurFiches.TRI_ETAT,"état");
					hash.put(RequeteurFiches.TRI_OBJET,"objet");
					hash.put(RequeteurFiches.TRI_RUBRIQUE,"rubrique");
					hash.put(RequeteurFiches.TRI_STRUCTURE,"structure");
					hash.put(RequeteurFiches.TRI_DSI,"groupes");
					infoBean.set("LISTE_TRI", hash);
					infoBean.set("liste_fiches_tri", RequeteurFiches.TRI_DATE_ASC);
					fmt.insererComboHashtable(out, infoBean, "liste_fiches_tri", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TRI","Langue");
				%>
			</p>
			<p>
				<label for="liste_fiches_increment" class="colonne">Nombre de résultats</label>
				<input type="text" name="liste_fiches_increment" id="liste_fiches_increment" value="5" size="5" />
			</p>
			<p>
				<label for="liste_fiches_separateur" class="colonne">Séparateur entre les liens<br />(par défaut un espace)</label>
				<%
					hash = new Hashtable<String, String>();
					hash.put("li", "Liste à puces");
					hash.put("br", "Retour à la ligne");
					infoBean.set("LISTE_SEPARATEURS", hash);
					fmt.insererComboHashtable( out, infoBean, "liste_fiches_separateur", FormateurJSP.SAISIE_FACULTATIF , "LISTE_SEPARATEURS" ); 
				%>
			</p>
			<p>
				<label for="liste_fiches_css" class="colonne">Classe CSS appliquée sur le lien</label>
				<input type="text" name="liste_fiches_css" id="liste_fiches_css" value="" size="10" />
			</p>
		</div>

		<div id="liste_fiches_tableau" style="display:none">
			<h3>Paramètrage du tableau de bord</h3>
			<p>
				<label for="liste_fiches_intitule_lien" class="colonne">Intitulé du lien</label>
				<input type="text" name="liste_fiches_intitule_lien" id="liste_fiches_intitule_lien" value="" size="20" />
			</p>
			<p>
				<label for="titre" class="colonne">Titre du tableau de bord</label>
				<input type="text" name="liste_fiches_titre" value="" size="20" />
			</p>
			<p>
				<input type="checkbox" name="liste_fiches_ajout" value="1" checked />
				<label for="ajout" class="colonne">Avec lien(s) d'ajout</label>
			</p>
			<p>
				<input type="checkbox" name="liste_fiches_pagination" value="1" checked />
				<label for="pagination">Avec pagination</label>
			</p>
			<span class="label colonne">Action(s) proposée(s)</span>
			<div class="retrait">
				<input type="checkbox" name="liste_fiches_action_voir" value="1" />
				<label for="liste_fiches_action_voir">Voir la fiche (en mode modification)</label>
				<br />
				<input type="checkbox" name="liste_fiches_action_modifier" value="1" />
				<label for="liste_fiches_action_modifier">Modifier la fiche (en mode consultation)</label>
				<br />
				<input type="checkbox" name="liste_fiches_action_supprimer" value="1" />
				<label for="liste_fiches_action_supprimer">Supprimer la fiche</label>
			</div><!-- .retrait -->

			<span class="label colonne">Possibilités de recherche et tri</span>
			<table>
				<tr>
					<th>Critère</th>
					<th>Recherche</th>
					<th>Colonne avec tri</th>
				</tr>
				<tr>
					<td>Date début</td>
					<td><input type="checkbox" name="liste_fiches_req_date_debut" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_date_debut" value="1" checked /></td>
				</tr>
				<tr>
					<td>Date fin</td>
					<td><input type="checkbox" name="liste_fiches_req_date_fin" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_date_fin" value="1" style="display:none;" /></td>
				</tr>
				<tr>
					<td>Rubrique</td>
					<td><input type="checkbox" name="liste_fiches_req_rubrique" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_rubrique" value="1" checked /></td>
				</tr>
				<tr>
					<td>Structure</td>
					<td><input type="checkbox" name="liste_fiches_req_structure" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_structure" value="1" checked /></td>
				</tr>
				<tr>
					<td>Groupe</td>
					<td><input type="checkbox" name="liste_fiches_req_groupe" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_groupe" value="1" checked /></td>
				</tr>
				<tr>
					<td>Rédacteur</td>
					<td><input type="checkbox" name="liste_fiches_req_redacteur" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_redacteur" value="1" checked /></td>
				</tr>
				<tr>
					<td>Objet</td>
					<td><input type="checkbox" name="liste_fiches_req_objet" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_objet" value="1" checked /></td>
				</tr>
				<tr>
					<td>Etat</td>
					<td><input type="checkbox" name="liste_fiches_req_etat" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_etat" value="1" checked /></td>
				</tr>
				<%  // on n'affiche pas l'option des langues si une seule langue est configurée pour le site
					if (((Hashtable) infoBean.get("LISTE_LANGUES")).size() > 1) { %>
				<tr>
					<td>Langue</td>
					<td><input type="checkbox" name="liste_fiches_req_langue" value="1" checked /></td>
					<td><input type="checkbox" name="liste_fiches_tri_langue" value="1" checked /></td>
				</tr>
				<% } %>
			</table>
		</div><!-- #liste_fiches_tableau -->
	</div><!-- #liste_fiches -->

</div>
