<%@ page import="java.util.Map" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="org.slf4j.LoggerFactory" %>
<%@ page import="com.jsbsoft.jtf.exception.ErreurApplicative" %>
<%@ page import="com.jsbsoft.jtf.session.SessionUtilisateur" %>
<%@ page import="com.univ.objetspartages.om.AutorisationBean" %>
<%@ page import="com.univ.utils.SessionUtil" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page isErrorPage="true" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div id="content" role="main">
<%  Map<String, Object> infosSession = SessionUtil.getInfosSession(request);
    AutorisationBean autorisation = (AutorisationBean) infosSession.get(SessionUtilisateur.AUTORISATIONS);
    if (autorisation != null) {
        if (infoBean.get("TOOLBOX") == null) { %>
          <div id="onglets-haut"></div>
          <div id="console"><%
        }
        if (StringUtils.isEmpty(infoBean.getMessageErreur())) {
            if (exception instanceof ErreurApplicative) {
                 %><%= exception.getMessage() %><%
            }
            else { %>
                Une erreur est survenue...
                <% LoggerFactory.getLogger(this.getClass()).error("Une erreur est survenue...", exception);
            }
        }
        else { %>
            <%= infoBean.getMessageErreur()%><%
        }
        if (infoBean.get("TOOLBOX") == null) { %>
          </div> <!-- #console --><%
        }
    }
    else { %>
    <p><a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=IDENTIFICATION&amp;ACTION=CONNECTER"><%= MessageHelper.getCoreMessage("ST_DSI_PREFIDENTIFICATION")%></a></p>
<% } %>
</div><!-- #content -->
