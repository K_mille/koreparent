<%@ page import="com.univ.url.UrlManager,com.univ.utils.ContexteUniv,com.univ.utils.ContexteUtil,com.univ.utils.URLResolver" errorPage="/adminsite/jsbexception.jsp" %>
<%@ page import="com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.multisites.bean.impl.InfosSiteImpl" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
    ContexteUniv ctx = ContexteUtil.getContexteUniv();
    ctx.initialiserInfosUtilisateur();
    ctx.setJsp(this);
    String urlApercu =  URLResolver.getAbsoluteUrl(
            UrlManager.calculerUrlApercu(infoBean.getString("ID_METATAG")),
            ctx,
            serviceInfosSite.determineSite(infoBean.getString("CODE_RUBRIQUE"), true)
            );
    response.sendRedirect( urlApercu);
%>