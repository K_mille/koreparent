<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta http-equiv="cache-control" content="no-cache">
        <meta http-equiv="pragma" content="no-cache">
    </head>
    <body>
        <div id="page">
            <form action="/servlet/com.jsbsoft.jtf.core.SG">
                <% fmt.insererVariablesCachees( out, infoBean); %>
                <div id="content">
                    <table class="datatable">
                        <thead>
                            <tr>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_CODE") %></th>
                                <th><%= MessageHelper.getCoreMessage("ST_DSI_NOM") %></th>
                                <th><%= MessageHelper.getCoreMessage("ST_DSI_PRENOM") %>f</th>
                                <th><%= MessageHelper.getCoreMessage("BO_LIBELLE_STRUCTURE") %></th>
                            </tr>
                        </thead>
                        <%
                        int nbItem = infoBean.getInt("LISTE_NB_ITEMS");
                        if (nbItem > 0) { %>
                        <tbody>
                        <% for (int i = 0 ; i < nbItem; i++) { %>
                            <tr>
                                <td class=<%= ((i%2)==0)? "DetailLigne":"DetailLigne2" %>> <%fmt.insererChampSaisie( out, infoBean, "CODE#"+i ,  fmt.SAISIE_AFFICHAGE , fmt.FORMAT_TEXTE, 0, 255); %> </td>
                                <td class=<%= ((i%2)==0)? "DetailLigne":"DetailLigne2" %>> <%fmt.insererChampSaisie( out, infoBean, "NOM#"+i ,  fmt.SAISIE_AFFICHAGE , fmt.FORMAT_TEXTE, 0, 8); %> </td>
                                <td class=<%= ((i%2)==0)? "DetailLigne":"DetailLigne2" %>> <%fmt.insererChampSaisie( out, infoBean, "PRENOM#"+i ,  fmt.SAISIE_AFFICHAGE , fmt.FORMAT_TEXTE, 0, 30); %> </td>
                                <td class=<%= ((i%2)==0)? "DetailLigne":"DetailLigne2" %>> <%fmt.insererChampSaisie( out, infoBean, "LIBELLE_STRUCTURE#"+i ,  fmt.SAISIE_AFFICHAGE , fmt.FORMAT_TEXTE, 0, 30); %> </td>
                            </tr>
                        <% } %>
                        </tbody>
                        <%
                        }
                        %>
                    </table>
                    <%
                       fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_REVENIR} );
                    %>
                </div><!-- .content_popup -->

            </form>
        </div><!-- #page -->
    </body>
</html>
