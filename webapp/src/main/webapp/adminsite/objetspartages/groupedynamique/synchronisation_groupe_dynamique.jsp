<%@ page import="java.util.Collection" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<div id="content" role="main"><% 
    boolean isSynchronise = (infoBean.get("RESULTAT") != null && ((Boolean) infoBean.get("RESULTAT")).booleanValue());
    if (isSynchronise) {
         Collection<String> tmEspacesAjoutes = (Collection<String>) infoBean.get("GROUPES_AJOUTES");
         Collection<String> tmEspacesModifies = (Collection<String>) infoBean.get("GROUPES_MODIFIES");
         Collection<String> tmEspacesSupprimes = (Collection<String>) infoBean.get("GROUPES_SUPPRIMES");%>
   
        <p><%=MessageHelper.getCoreMessage("ST_SYNCHRO_GROUPES_DYNAMIQUE_OK")%></p>
        <p><%=MessageHelper.getCoreMessage("ST_SYNCHRO_GROUPES_DYNAMIQUE_AJOUT")%> : <%=infoBean.get("NB_AJOUTS")%></p>
        <% if (tmEspacesAjoutes != null && !tmEspacesAjoutes.isEmpty()) { %>
            <ul>
            <% for (String ajout : tmEspacesAjoutes) { %>
                <li><%=ajout%></li>
            <% } %>
            </ul>
        <% } %>
        <p><%=MessageHelper.getCoreMessage("ST_SYNCHRO_GROUPES_DYNAMIQUE_MODIFIER")%> : <%=infoBean.get("NB_MODIFICATIONS")%></p>
        <% if (tmEspacesModifies != null && !tmEspacesModifies.isEmpty()) {%>
            <ul>
            <% for (String modif : tmEspacesModifies) { %>
                <li><%=modif%></li>
            <% } %>
            </ul>
        <% } %>
        <p><%=MessageHelper.getCoreMessage("ST_SYNCHRO_GROUPES_DYNAMIQUE_SUPPRESSION")%> : <%=infoBean.get("NB_SUPPRESSIONS")%></p>
        <% if (tmEspacesSupprimes != null && !tmEspacesSupprimes.isEmpty()) { %>
            <ul>
            <% for (String suppr : tmEspacesSupprimes) { %>
                <li><%=suppr%></li>
            <% } %>
            </ul>
        <% } %>
<% } else { %>
    <p><%=MessageHelper.getCoreMessage("ST_SYNCHRO_GROUPES_DYNAMIQUE_NOK")%></p>
<% } %>

</div>
