<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />

<div id="content" role="main">
    <form action='/servlet/com.jsbsoft.jtf.core.SG' enctype="multipart/form-data" method="post">
    <% fmt.insererVariablesCachees( out, infoBean); %>
    <div class="content_popup">
        <div class="fieldset neutre">
            <%univFmt.insererChampSaisie( fmt, out, infoBean, "LIBELLE_FICHIER", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_FICHIER, 10, 30, MessageHelper.getCoreMessage("BO_LIBELLE_IMPORT_LIBELLES")); %>
        </div>
    </div>
    <div class="footer_popup">
    <% fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} );  %>
    </div>
    </form>
</div><!-- #content -->