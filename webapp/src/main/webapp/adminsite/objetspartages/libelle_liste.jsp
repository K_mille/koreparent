<%@page import="java.util.Collection"%>
<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.jsbsoft.jtf.core.ProcessusHelper"%>
<%@page import="com.kportal.extension.module.composant.IComposant"%>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.datagrid.utils.DatagridUtils"%>
<%@page import="com.univ.objetspartages.util.CritereRecherche"%>
<%@page import="com.univ.utils.EscapeString"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%><div id="content" role="main"><%
    Collection<CritereRecherche> criteresRecherche = AdminsiteUtils.getCriteresRechercheAAfficher(infoBean);
    if (!criteresRecherche.isEmpty()) {
    %><div id="recherche_criteres">
            <h3><%= module.getMessage("BO_CRITERES_RAPPEL") %></h3>
            <div>
                <ul><%
                for (CritereRecherche critere : criteresRecherche) {
                    String champ = StringUtils.defaultIfBlank(module.getMessage("LIBELLE." +critere.getNomChamp()),critere.getNomChamp());
                    %><li><strong><%= champ %></strong> <%=EscapeString.escapeHtml(critere.getValeurAAfficher()) %></li><%
                }
                %></ul>
                <p><a href="<%=AdminsiteUtils.getUrlRechercheAvanceeDatagrid(module,infoBean) %>"><%= module.getMessage("BO_AFFINER_RECHERCHE") %></a></p>
            </div>
        </div><!-- #recherche_criteres --><%
    }%>
    <table class="datatableLibelle"
            data-search="<%=DatagridUtils.getUrlTraitementDatagrid(infoBean)%>">
        <thead>
            <tr>
                <th><%= module.getMessage("BO_LIBELLE_TYPE") %></th>
                <th><%= module.getMessage("BO_LIBELLE_CODE") %></th>
                <th><%= module.getMessage("BO_LIBELLES") %></th>
                <th class="sanstri sansfiltre"><%=module.getMessage("BO_ACTIONS") %> </th>
            </tr>
        </thead>
        <tbody></tbody>
    </table>
    </form>
</div><!-- #content -->