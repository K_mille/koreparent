<%@page import="com.jsbsoft.jtf.core.FormateurJSP"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<div id="content" role="main">
    <form action="/servlet/com.jsbsoft.jtf.core.SG" class="recherche" data-no-tooltip><%
    fmt.insererVariablesCachees( out, infoBean);
    %><div class="fieldset neutre">
        <p>
              <label for="LIBELLE" class="colonne"><%= MessageHelper.getCoreMessage("BO_MODULE_LIBELLE") %></label>
            <%fmt.insererChampSaisie( out, infoBean, "LIBELLE" ,  FormateurJSP.SAISIE_FACULTATIF , FormateurJSP.FORMAT_TEXTE, 0, 32); %>
        </p>        <p>
              <label for="CODE" class="colonne"><%= MessageHelper.getCoreMessage("BO_LIBELLE_CODE") %></label>
            <%fmt.insererChampSaisie( out, infoBean, "CODE" ,  FormateurJSP.SAISIE_FACULTATIF , FormateurJSP.FORMAT_TEXTE, 0, 32); %>
        </p>
        <p>
            <label for="TYPE" class="colonne"><%= MessageHelper.getCoreMessage("BO_LIBELLE_TYPE") %></label>
            <%fmt.insererComboHashtable( out, infoBean, "TYPE", FormateurJSP.SAISIE_FACULTATIF , "LISTE_TYPES_LIBELLES" ); %>
        </p>
    </div>
    <%
        fmt.insererBoutons( out, infoBean, new int[] {FormateurJSP.BOUTON_VALIDER} );
    %>
    </form>
</div><!-- #content -->