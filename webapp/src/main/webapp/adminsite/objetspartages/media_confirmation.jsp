<%@ taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<%@ page import="com.univ.objetspartages.processus.SaisieMedia" %>
<%@ page import="com.univ.utils.Chaine" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
    </head>
    <body><%
    if ("CONTRIBUTION".equals(infoBean.getString("MODE_FICHIER"))) {
        %><script type="text/javascript">
            window.opener.location.reload(true);
            window.close();
        </script><%
    }
    else if (SaisieMedia.MODE_SELECTION.equals(infoBean.getString("MODE"))) {
        %><script type="text/javascript">
            window.iFrameHelperCallback    = function(){
                if(window.iFrameRegistration){
                    var file = {
                        id : "<%=infoBean.get("ID_RESSOURCE")%>",
                        titre : "<%=Chaine.escapeJavaScript(infoBean.getString("TITRE"))%>",
                        format : "<%=infoBean.getString("FORMAT")%>",
                        legende : "<%=Chaine.escapeJavaScript(infoBean.getString("LEGENDE"))%>",
                        date_creation : "<%=infoBean.get("DATE_CREATION")%>",
                        num_fichier : "<%=infoBean.getString("NO_FICHIER")%>",
                        mode_fichier : "<%=infoBean.getString("MODE_FICHIER")%>_<%=infoBean.getString("TYPE_RESSOURCE")%>"
                    }
                    iFrameHelper.sendValues(window.iFrameRegistration, file);
                }
            }
        </script><%
    }
    else if (SaisieMedia.MODE_INSERTION.equals(infoBean.getString("MODE"))) {
        %><jsp:include page="./medias/toolbox/linkMedia.jsp"/>
        <resources:script group="scriptsBo" locale="<%= ContexteUtil.getContexteUniv().getLocale().toString() %>"/>
        <script type="text/javascript" src="/adminsite/scripts/libs/jquery.iframe-states.js"></script><%
    }
    %>
    </body>
</html>
