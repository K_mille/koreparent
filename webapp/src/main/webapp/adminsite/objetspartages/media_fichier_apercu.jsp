<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />

<div class="details-media-conteneur">
    <div class="apercu-fichier-conteneur">
        <a class="apercu-fichier" href="<%=infoBean.getString("URL_RESSOURCE")%>" >
            <span>Voir le fichier</span>
        </a>
    </div>
