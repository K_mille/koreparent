<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<%if (infoBean.getString("URL_RESSOURCE").length()>0) { %>

<table style="width:100%">
<tr>
    <td style="width:150px;text-align:right;">Largeur</td>
    <td><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LARGEUR", fmt.SAISIE_FACULTATIF, fmt.FORMAT_ENTIER, 0, 3, "Largeur");%>&nbsp;Px</td>
</tr>
<tr>
    <td style="width:150px;text-align:right;">Hauteur</td>
    <td><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "HAUTEUR", fmt.SAISIE_FACULTATIF, fmt.FORMAT_ENTIER, 0, 3, "Hauteur");%>&nbsp;Px</td>
</tr>
</table>
<% } %>


