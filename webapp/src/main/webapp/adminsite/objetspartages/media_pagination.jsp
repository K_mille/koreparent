<%@ page import="com.kportal.core.config.MessageHelper" %>
<% String sHrefNouvelleRecherche = "/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&amp;ACTION=RECHERCHER&amp;MODE_PHOTOTHEQUE="+ infoBean.get("MODE_PHOTOTHEQUE") +"&amp;INCREMENT="+ infoBean.get("INCREMENT");
if (infoBean.get("NO_FICHIER") != null)
    sHrefNouvelleRecherche += "&amp;NO_FICHIER="+infoBean.get("NO_FICHIER");

%>
<p class="media_pagination">
    <% if(infoBean.get("FROMMOINS") != null) { %>
        <input class="bouton prev" type="button" name="PREVIOUS" value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.PAGINATION.PRECEDENT") %>" onclick="document.forms[0].ACTION.value='RECHERCHER';document.forms[0].FROM.value='<%=infoBean.get("FROMMOINS")%>';document.forms[0].submit();">
    <% } %>
    <% if(infoBean.get("FROMPLUS") != null) { %>
        <input class="bouton next" type="button" name="NEXT" value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.PAGINATION.SUIVANT") %>" onclick="document.forms[0].ACTION.value='RECHERCHER';document.forms[0].FROM.value='<%=infoBean.get("FROMPLUS")%>';document.forms[0].submit();">
    <% } %>
</p>
<div class="clearfix"></div>