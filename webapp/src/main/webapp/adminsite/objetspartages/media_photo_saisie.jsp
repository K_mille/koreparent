<%@ page import="java.util.ArrayList,com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.CriterePhoto" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />

<%if (infoBean.getString("URL_RESSOURCE").length()>0) { %>

<table style="width:100%">
<tr>
    <td style="width:150px;text-align:right;"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.LARGEUR") %></td>
    <td><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LARGEUR", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_ENTIER, 0, 3, "Largeur");%>&nbsp;Px</td>
</tr>
<tr>
    <td style="width:150px;text-align:right;"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.HAUTEUR") %></td>
    <td><%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "HAUTEUR", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_ENTIER, 0, 3, "Hauteur");%>&nbsp;Px</td>
</tr>
</table>
<% } %>


<%
if (infoBean.get("NEW_RESSOURCE")!=null) { %>
<h3><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.TRAITEMENT_PHOTO") %></h3>
<table>
<tr>
    <td colspan="2">
<%
      ArrayList lstCriterePhoto = (ArrayList)infoBean.get("criteres");
      int i=0;
      for(i=0;i<lstCriterePhoto.size();i++) {
            CriterePhoto critere = (CriterePhoto)lstCriterePhoto.get(i);
            fmt.insererChampSaisie(out, infoBean, "CRITERE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO, 0,    0);
            %>
            <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.CREER") %> <b><%=critere.getCode()%></b> <%= String.format(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.A_PARTIR"), critere.getLargeur(), critere.getHauteur(), critere.getPoids()) %><br>
            <%
      }
      %>
            <%fmt.insererChampSaisie(out, infoBean, "CRITERE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO, 0,    0); %>
            <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.TRAITEMENT_PERSO") %> <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.TRAITEMENT_PERSO.LARGEUR") %> <%fmt.insererChampSaisie(out, infoBean, "LARGEUR_PERSONNALISE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_ENTIER,0,4); %><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.TRAITEMENT_PERSO.HAUTEUR") %> <%fmt.insererChampSaisie(out, infoBean, "HAUTEUR_PERSONNALISE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_ENTIER,0,4); %>)<br>
            <%fmt.insererChampSaisie(out, infoBean, "CRITERE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO, 0,    0); %>
            <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.PHOTO.PAS_TRAITEMENT") %>
    </td>
  </tr>
  </table>
<% } %>


