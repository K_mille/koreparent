<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.core.config.MessageHelper" %>
<%@page import="com.univ.utils.URLResolver"%>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<div id="content" role="main">
<table class="datatableLight">
    <thead>
        <tr>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.TITRE") %></th>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.TYPE_OBJET") %></th>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.AUTEUR") %></th>
            <th><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.ACTION") %></th>
        </tr>
    </thead>
    <tbody>
<%
int nbLignes = infoBean.getInt("NB_REFERENCES");
if (nbLignes == 0) {
%>
    <tr><td><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.PAS_REFERENCE") %></td></tr>
<% } else {
    for (int i=0; i < nbLignes; i++) { %>
        <tr>
            <%if(StringUtils.isNotBlank(infoBean.getString("URL#"+i))){ %>
                <td><a href="<%=URLResolver.getAbsoluteUrl(infoBean.getString("URL#"+i), ContexteUtil.getContexteUniv())%>" target="_blank"><%=infoBean.getString("TITRE#"+i)%></a></td>
            <%} else { %>
                <td><%=infoBean.getString("TITRE#"+i)%></td>
            <%} %>
            <td><%= infoBean.getString("OBJET#"+i) %></td>
            <td><%=infoBean.getString("AUTEUR#"+i)%></td>
            <td>
            <% if (StringUtils.isNotBlank(infoBean.getString("URL#"+i))) { %>
                <a href="<%=URLResolver.getAbsoluteUrl(infoBean.getString("URL#"+i), ContexteUtil.getContexteUniv())%>" target="_blank"><img src="/adminsite/images/modif.gif" alt="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.MODIFIER") %>" title="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.MODIFIER") %>" /><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.MODIFIER") %></a>
            <%} %>
            </td>
        </tr>
        <% }
    } %>
</tbody>
</table>
<input class="bouton" type="submit" name="RETOUR" value="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.REFERENCE.MODIFIER") %>" onclick="history.back();return false;"/>
</div>


