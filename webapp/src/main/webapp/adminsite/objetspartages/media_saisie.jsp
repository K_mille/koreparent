<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP, com.jsbsoft.jtf.core.InfoBean" errorPage="/adminsite/jsbexception.jsp" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.webapp.WebAppUtil" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<%
    final boolean insertionMode = StringUtils.isNotBlank(infoBean.getString("MODE")) && infoBean.getString("MODE").equals("INSERTION");
    final boolean selectionMode = StringUtils.isNotBlank(infoBean.getString("MODE")) && infoBean.getString("MODE").equals("SELECTION");
%><form action="<%= WebAppUtil.SG_PATH%>" enctype="multipart/form-data" method="post">
<div data-ui="tabs">
    <div id="actions">
        <div>
            <ul><%
                if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF) && "1".equals(infoBean.getString("SUPPRESSION_MEDIA")) && !insertionMode && !selectionMode) {
                    %><li>
                        <button class="supprimer" data-confirm="<%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.SUPPRESSION.CONFIRM")%>" type="submit" name="SUPPRIMER" value="<%= MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER")%>"><%= MessageHelper.getCoreMessage("JTF_BOUTON_SUPPRIMER")%></button>
                    </li><%
                }
                %><li>
                    <button class="enregistrer" type="submit" name="ENREGISTRER" value="<%= MessageHelper.getCoreMessage("BO_ENREGISTRER")%>"><%= MessageHelper.getCoreMessage("BO_ENREGISTRER")%></button>
                </li>
            </ul>
            <div class="clearfix"></div><%
            if(!selectionMode && !insertionMode) {
                %><span title="<%= MessageHelper.getCoreMessage("BO_FERMER") %>" id="epingle">&ndash;</span><%
            }
        %></div>
    </div><!-- #actions -->

    <div data-receive-class class="contenu1">
        <ul id="onglets" data-ui="tabs-header">
            <li id="li-ressource" class="onglet-contenu1" data-action="ONGLET" data-onglet="RESSOURCE"><a data-class="contenu1" href="#div-ressource" data-ui="tabs-tab"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.ONGLET.RESSOURCE")%></a></li>
            <li id="li-general" class="onglet-contenu2" data-action="ONGLET" data-onglet="GENERAL"><a data-class="contenu2" href="#div-general" data-ui="tabs-tab"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.ONGLET.DESCRIPTION")%></a></li>
            <li id="li-references" class="onglet-contenu3" data-action="ONGLET" data-onglet="REFERENCES"><a data-class="contenu3" href="#div-references" data-ui="tabs-tab"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.ONGLET.REFERENCES")%></a></li><%
            if (StringUtils.isNotBlank(infoBean.getString("CONTROLE_RESSOURCE"))) {%>
                <li id="li-suivi" class="onglet-contenu4" data-action="ONGLET" data-onglet="SUIVI"><a data-class="contenu4" href="#div-suivi" data-ui="tabs-tab"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.ONGLET.RATTACHEMENTS")%></a></li><%
            }%>
        </ul>
    </div><!-- .contenu1 -->

    <div id="content" role="main">

        <jsp:include page="./medias/hiddenFields.jsp"/>

        <!-- Resource tab -->
        <jsp:include page="./medias/tabs/resource.jsp"/>

        <!-- Description tab -->
        <jsp:include page="./medias/tabs/description.jsp"/>

        <!-- Links tab -->
        <jsp:include page="./medias/tabs/links.jsp"/>

        <!-- Links tab -->
        <jsp:include page="./medias/tabs/references.jsp"/>

        <div class="footer_popup"><%
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION) && infoBean.getString("CONTROLE_RESSOURCE")!=null && infoBean.get("MODE")!=null) {
                %><fieldset>
                    <legend><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.OPTIONS_ENREGISTREMENT")%></legend>
                    <p>
                        <%fmt.insererChampSaisie( out, infoBean, "MUTUALISE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_CHECKBOX, 0, 0); %> <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.SAUVEGARDER")%>
                    </p>
                </fieldset><%
            }
            %><div class="controle_formulaire">
                <img id="loader" src="/adminsite/images/loader.gif" style="display:none;" />
            </div><!-- .controle_formulaire -->
        </div><!-- .footer_popup -->
    </div><!-- #content -->
</div><!-- data-ui=tabs -->
</form>