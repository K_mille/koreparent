<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<script type="text/javascript" src="/adminsite/utils/swfobject.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/video/player_video.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/swf/player_swf.js"></script>
<script type="text/javascript" src="/adminsite/utils/mediatheque/commun/media.js"></script>

<div id="container_video_distant" class="apercu-video-conteneur">
    <a class="apercu-video" href="<%=infoBean.getString("URL_RESSOURCE")%>">Video ici</a>
</div>

<script type="text/javascript">
//<![CDATA[
    noCacheKey = (new Date()).getTime();
    var playerVideo = '';
    <%if ("1".equals(infoBean.get("SPECIFIC_URL"))) { %>
        playerVideo = new SWFPlayer("container_video_distant",300, 250);
        playerVideo.ajouterMedia("<%=infoBean.getString("URL_RESSOURCE")%>", "");
        playerVideo.genererPlayer();
        playerVideo.lancerMedia("<%=infoBean.getString("URL_RESSOURCE")%>");
    <% } else { %>
        playerVideo = new VideoPlayer("container_video_distant",300, 250, "/adminsite/utils/mediatheque/video/player_flv_multi.swf");
        playerVideo.ajouterVideo("<%=infoBean.getString("URL_RESSOURCE")%>", "");
        playerVideo.setMargin("0");
        playerVideo.genererPlayer();
    <% } %>


//]]>
</script>

<div class="details-media-conteneur">