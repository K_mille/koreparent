<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.univ.mediatheque.Mediatheque" %>
<%@ page import="com.univ.objetspartages.om.SpecificMedia" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String type = StringUtils.defaultString(infoBean.get("TYPE_RESSOURCE", String.class)).toLowerCase();
    final SpecificMedia specificMedia = Mediatheque.getInstance().getRessource(type);
    if(specificMedia != null) {
    %><jsp:include page="<%= specificMedia.getJspApercu() %>"/><%
    }
%>