<%@ page import="java.util.Collection" %>
<%@ page import="java.util.Map" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<div id="div-general" class="fieldset neutre" data-ui="tabs-container" style="display : none;">
    <div class="js-tabs-medias">
        <ul class="onglets_drapeau tabs tabs--horizontal">
            <li class="tabs__item tabs__item--active js-tabs__item">
                <a class="drapeau tabs__link js-tabs__link" id="drapeau-0" href="#media-contenu-0" data-panel="media-contenu-0">
                    <% univFmt.insererDrapeauLangue(out,"0"); %>
                </a>
            </li><%
            Collection<String> enLangue = ((Map<String, String>)infoBean.get("LISTE_LANGUES")).keySet();
            for (String sKeyLangue : enLangue){
            %><li class="tabs__item js-tabs__item">
                <a class="drapeau tabs__link js-tabs__link" id="drapeau-<%=sKeyLangue%>" href="#media-contenu-<%=sKeyLangue%>" data-panel="media-contenu-<%= sKeyLangue %>"><%
                    univFmt.insererDrapeauLangue(out, sKeyLangue);
                %></a>
            </li><%
            }
        %></ul><%
        for (String sKeyLangue : enLangue) {
            %><div id="media-contenu-<%=sKeyLangue%>" class="fieldset neutre media_description tabs__panel js-tabs__panel" data-panel="media-contenu-<%= sKeyLangue %>">
                <p>
                    <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.LANGUE")%></span>
                    <%univFmt.insererDrapeauLangue(out,sKeyLangue);%>
                </p>
                <p>
                    <label for="TITRE_<%=sKeyLangue%>" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_TITRE_MEDIA")%></label>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "TITRE_"+sKeyLangue, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_MEDIATHEQUE_TITRE_MEDIA"));%>
                </p>
                <p>
                    <label for="LEGENDE_<%=sKeyLangue%>" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_LEGENDE_MEDIA")%></label>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LEGENDE_"+sKeyLangue, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_MEDIATHEQUE_LEGENDE_MEDIA"));%>
                </p>
                <p>
                    <label for="DESCRIPTION_<%=sKeyLangue%>" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_DESCRIPTION_MEDIA")%></label>
                    <%fmt.insererChampSaisie( out, infoBean, "DESCRIPTION_"+sKeyLangue, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0,    1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_DESCRIPTION_MEDIA") + ",COLS=37,ROWS=2");%>
                </p>
                <p>
                    <label for="AUTEUR_<%=sKeyLangue%>" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_AUTEUR_MEDIA")%></label>
                    <%fmt.insererChampSaisie( out, infoBean, "AUTEUR_"+sKeyLangue, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0,    1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_AUTEUR_MEDIA") + ",COLS=37,ROWS=2");%>
                </p>
                <p>
                    <label for="COPYRIGHT_<%=sKeyLangue%>" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_COPYRIGHT_MEDIA")%></label>
                    <%fmt.insererChampSaisie( out, infoBean, "COPYRIGHT_"+sKeyLangue, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0,    1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_COPYRIGHT_MEDIA") + ",COLS=37,ROWS=2");%>
                </p>
                <p>
                    <label for="META_KEYWORDS_<%=sKeyLangue%>" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_META_KEYWORDS_MEDIA")%></label>
                    <%fmt.insererChampSaisie( out, infoBean, "META_KEYWORDS_"+sKeyLangue, FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_META_KEYWORDS_MEDIA") + ",COLS=37,ROWS=2");%>
                </p>
            </div><!-- #media-contenu --><%
        }
        %><div id="media-contenu-0" class="fieldset neutre media_description tabs__panel tabs__panel--active js-tabs__panel" data-panel="media-contenu-0">
            <p>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.LANGUE")%></span>
                <%univFmt.insererDrapeauLangue(out, "0");%>
            </p>
            <p>
                <label for="TITRE" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_TITRE_MEDIA")%></label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_MEDIATHEQUE_TITRE_MEDIA"));%>
            </p>
            <p>
                <label for="LEGENDE" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_LEGENDE_MEDIA")%></label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "LEGENDE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("ST_MEDIATHEQUE_LEGENDE_MEDIA"));%>
            </p>
            <p>
                <label for="DESCRIPTION" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_DESCRIPTION_MEDIA")%></label>
                <%fmt.insererChampSaisie( out, infoBean, "DESCRIPTION", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_DESCRIPTION_MEDIA") + ",COLS=37,ROWS=2");%>
            </p>
            <p>
                <label for="AUTEUR" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_AUTEUR_MEDIA")%></label>
                <%fmt.insererChampSaisie( out, infoBean, "AUTEUR", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_AUTEUR_MEDIA") + ",COLS=37,ROWS=2");%>
            </p>
            <p>
                <label for="COPYRIGHT" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_COPYRIGHT_MEDIA")%></label>
                <%fmt.insererChampSaisie( out, infoBean, "COPYRIGHT", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0,    1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_COPYRIGHT_MEDIA") + ",COLS=37,ROWS=2");%>
            </p>
            <p>
                <label for="META_KEYWORDS" class="colonne"><%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_META_KEYWORDS_MEDIA")%></label>
                <%fmt.insererChampSaisie( out, infoBean, "META_KEYWORDS", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_MULTI_LIGNE, 0, 1024, "LIB=" + MessageHelper.getCoreMessage("ST_MEDIATHEQUE_META_KEYWORDS_MEDIA") + ",COLS=37,ROWS=2");%>
            </p>
        </div><!-- .media_description -->
    </div><!-- .js-tabs -->
</div><!-- #div-general -->
