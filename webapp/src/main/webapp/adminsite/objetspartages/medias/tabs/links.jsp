<%@ page import="java.util.Date" %>
<%@ page import="com.jsbsoft.jtf.core.Formateur" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.utils.UnivFmt" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" />
<div id="div-suivi" class="fieldset neutre" data-ui="tabs-container" style="display : none;">
    <p>
        <label for="THEMATIQUE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.THEMATIQUE")%></label>
        <% univFmt.insererContenuComboHashtable(fmt, out, infoBean, "THEMATIQUE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_THEMATIQUES", MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.THEMATIQUE"), ""); %>
    </p>
    <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("BO_RUBRIQUE"), "rubrique", UnivFmt.CONTEXT_ZONE); %>
    <% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RATTACHEMENT", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("ST_CODE_RATTACHEMENT"), "", UnivFmt.CONTEXT_STRUCTURE); %>
    <% if(Formateur.estSaisie((Date) infoBean.get("DATE_CREATION"))) {
        %><p>
            <label for="DATE_CREATION" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.DATE_CREATION")%></label>
            <% univFmt.insererContenuChampSaisie(fmt, out, infoBean, "DATE_CREATION", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_DATE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.DATE_CREATION")); %>
        </p><%
    }
    %><% univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_REDACTEUR", FormateurJSP.SAISIE_FACULTATIF, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.REDACTEUR"), "utilisateur", UnivFmt.CONTEXT_ZONE); %>
</div><!-- #div-suivi -->
