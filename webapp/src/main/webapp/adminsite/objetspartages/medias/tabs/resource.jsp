<%@ page import="java.util.Enumeration" %>
<%@ page import="java.util.Hashtable" %>
<%@ page import="java.util.List" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.InfoBean" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.univ.mediatheque.utils.MediathequeHelper" %>
<%@ page import="com.univ.objetspartages.om.CriterePhoto" %>
<%@ page import="com.univ.objetspartages.processus.SaisieMedia" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="request" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="request" /><%
    // génération d'une vignette
    final String critere = PropertyHelper.getCoreProperty(MediathequeHelper.CRITERES_VIGNETTE_PROPERTIES_KEY);
    String[] lstLimite = critere.split("/", -2);
    int largeur = Integer.parseInt(lstLimite[1]);
    int hauteur = Integer.parseInt(lstLimite[2]);
    final boolean insertionMode = StringUtils.isNotBlank(infoBean.getString("MODE")) && infoBean.getString("MODE").equals("INSERTION");
    final boolean selectionMode = StringUtils.isNotBlank(infoBean.getString("MODE")) && infoBean.getString("MODE").equals("SELECTION");
%><div id="div-ressource" class="fieldset neutre" data-ui="tabs-container" style="display: block;"><%
    String sKeyType = infoBean.getString("TYPE_RESSOURCE");
    if(!selectionMode && !insertionMode) {
        if ("true".equals(infoBean.get("IS_LOCAL"))) {
            %><p class="media_source">
                <% if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF) || infoBean.getString("URL_RESSOURCE").length()>0){
                    infoBean.remove("MEDIA");
                    %><label for="MEDIA" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.MODIFIER_SOURCE")%></label>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MEDIA", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_FICHIER, 10, 100, "Fichier");%><%
                } else {
                    %><label for="MEDIA" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.SOURCE")%> (*)</label>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "MEDIA", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_FICHIER, 10, 100, "Fichier");%><%
                }
            %></p><%
        } else {
            %><p class="media_source"><%
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF) || infoBean.getString("URL_RESSOURCE").length()>0){
                %><label for="URL_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.MODIFIER_URL")%></label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "URL_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 10, 255, "Fichier");%>
                <button class="infobulle" type="button" title="<%= MessageHelper.getCoreMessage("ST_MEDIATHEQUE_INFO_URL_EXTERNE") %>"></button><%
            } else {
                %><label for="URL_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.URL")%> (*)</label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "URL_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 10, 255, "Fichier");%>
                <button class="infobulle" type="button" title="<%= MessageHelper.getCoreMessage("ST_MEDIATHEQUE_INFO_URL_EXTERNE") %>"></button><%
            }
            %></p>
        <% }
    }
       if (StringUtils.isNotBlank(infoBean.getString("CONTROLE_RESSOURCE"))) {%>
            <div class="media_apercu">
                <jsp:include page="../preview/preview.jsp"/>
            </div><!-- .media_apercu --><%
       }

    if ("true".equals(infoBean.get("IS_LOCAL"))) { %>
        <input type="hidden" name="URL_RESSOURCE" value="<%= infoBean.get("URL_RESSOURCE") %>"><%
        if (infoBean.getString("URL_RESSOURCE").length()>0) {
            %><div class="media_infos<%= infoBean.get(SaisieMedia.PREVIEW_AVAILABLE, Boolean.class) ? " resource_controled" : StringUtils.EMPTY %>">
            <p>
                <label for="SOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.NOM_FICHIER")%></label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "SOURCE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255, "Path");%>
            </p><%
        }
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_MODIF)) {
            %><p>
                <label for="TYPE_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE_MEDIA")%></label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255, "Type de media");%>
            </p><%
        }
        if (!selectionMode && !insertionMode && (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION))){
            %><p>
                <a href="#" class="js-change-source" data-local="false"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.URL_EXTERNE")%></a>
            </p><%
        }
        if (infoBean.getString("URL_RESSOURCE").length()>0) {
            if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION) && infoBean.getString("TYPE_RESSOURCE_PREV")==null) {
                %><p>
                    <label for="TYPE_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE_MEDIA")%> (*)</label>
                    <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_RESSOURCES", "", "");%>
                </p><%
        }
        %><p>
            <label for="TYPE_MEDIA_<%=sKeyType.toUpperCase() %>" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE")%> <%=infoBean.getString("LIBELLE_TYPE_RESSOURCE").toLowerCase()%></label>
            <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_"+sKeyType.toUpperCase(), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_"+sKeyType.toUpperCase(), "", "");%>
        </p>
        <!-- FORMAT --><%
        if (StringUtils.isNotBlank(infoBean.getString("FORMAT"))) {
            %><p>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.FORMAT")%></span>
                <span><%= infoBean.getString("FORMAT") %></span>
            </p><%
        }
        %><p>
            <label for="POIDS" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.POIDS")%></label>
            <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "POIDS", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_ENTIER, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.POIDS"));%>
        </p><%
            if (StringUtils.isNotBlank(infoBean.getString("LARGEUR")) && !infoBean.getString("LARGEUR").equals("0")) {
            %><p>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.LARGEUR")%></span>
                <span><%= infoBean.getString("LARGEUR") %>&nbsp;px</span>
            </p><%
        }
        if (StringUtils.isNotBlank(infoBean.getString("HAUTEUR")) && !infoBean.getString("HAUTEUR").equals("0")) {
            %><p>
                <span class="label colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.HAUTEUR")%></span>
                <span><%= infoBean.getString("HAUTEUR") %>&nbsp;px</span>
            </p><%
        }
        if ("PHOTO".equals(sKeyType.toUpperCase()) && infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)) {
            %><br/>
            <div>
                <h3><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT")%></h3><%
                    List<CriterePhoto> lstCriterePhoto = infoBean.getList("criteres");
                    int i = 0;
                    if (lstCriterePhoto != null) {
                        for(i=0;i<lstCriterePhoto.size();i++) {
                            CriterePhoto currentCritere = lstCriterePhoto.get(i);
                        %><p>
                            <%fmt.insererChampSaisie(out, infoBean, "CRITERE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO, 0, 0);%>
                            <label for="CRITERE_<%=i%>"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT.REDIMENSIONNER")%> <b><%=currentCritere.getCode()%></b> <%= String.format(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT.REDIMENSIONNER_FORMAT"), currentCritere.getLargeur(), currentCritere.getHauteur(), currentCritere.getPoids())%></label>
                        </p><%
                        }
                    }
                    %><p>
                        <%fmt.insererChampSaisie(out, infoBean, "CRITERE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO, 0, 0); %>
                        <label for="CRITERE_<%=i++%>"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT.REDIMENSIONNER")%> : <%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT.REDIMENSIONNER_FORMAT.LARGEUR")%> <%fmt.insererChampSaisie(out, infoBean, "LARGEUR_PERSONNALISE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_ENTIER,0,4); %><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT.REDIMENSIONNER_FORMAT.HAUTEUR")%> <%fmt.insererChampSaisie(out, infoBean, "HAUTEUR_PERSONNALISE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_ENTIER,0,4); %>)</label>
                    </p>
                    <p>
                        <%fmt.insererChampSaisie(out, infoBean, "CRITERE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_RADIO, 0, 0); %>
                        <label for="CRITERE_<%=i++%>"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TRAITEMENT.REDIMENSIONNER_FORMAT.AUCUN_TRAITEMENT")%></label>
                    </p>
            </div><%
        }
        %><br/>
        <div>
            <div class="media_vignette_input">
                <% if(infoBean.getString("URL_VIGNETTE").length()>0) {
                    %><img id="img_vignette" src="<%=infoBean.getString("URL_VIGNETTE")%>" alt="" title="" ><br />
                <% } else { %>
                    <h3><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.THUMBNAIL.TITLE") %></h3>
                    <p id="champs_obligatoires" class="message alert alert-info">
                        <%= String.format(MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.THUMBNAIL.TIP") , String.format("[%dx%d]", largeur, hauteur)) %>.
                    </p><%
                    }
                %><br/>
                <span id="div_vignette">
                    <label for="VIGNETTE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.VIGNETTE")%></label>
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "VIGNETTE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_FICHIER, 10, 100, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.VIGNETTE"));%>
                </span>
                <input class="js-thumbnail__delete-button" type="button" value="<%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_BOUTON_EFFACER")%>"/>
            </div>
        </div>
    </div><%
        }
    } else {
    %><div class="media_infos<%= infoBean.get(SaisieMedia.PREVIEW_AVAILABLE, Boolean.class) ? " resource_controled" : StringUtils.EMPTY %>"><%
        if (infoBean.getEtatObjet().equals(InfoBean.ETAT_OBJET_CREATION)){
            %><p>
                <a href="#"  class="js-change-source" data-local="true"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.FICHIER_LOCAL")%></a>
            </p>
            <p class="media_type_ressource">
                <label for="TYPE_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE_MEDIA")%> (*)</label>
                <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_RESSOURCES", "", "");%>
            </p><%
            Hashtable hType = (java.util.Hashtable) infoBean.get("LISTE_TYPE_RESSOURCES");
            Enumeration<String> en = hType.keys();
            while (en.hasMoreElements()){
                sKeyType = en.nextElement();
            %><p data-type="<%=sKeyType.toLowerCase()%>" style="display:none;">
                <label for="TYPE_MEDIA_<%=sKeyType.toUpperCase()%>" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE")%> <%=((String)hType.get(sKeyType)).toLowerCase()%></label>
                <%univFmt.insererContenuComboHashtable(fmt, out, infoBean, "TYPE_MEDIA_"+sKeyType.toUpperCase(), FormateurJSP.SAISIE_FACULTATIF, "LISTE_TYPE_MEDIA_"+sKeyType.toUpperCase(), "", "");%>
            </p><%
            }
        } else {
            %><p>
                <label for="TYPE_RESSOURCE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE_MEDIA")%></label>
                <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "TYPE_RESSOURCE", FormateurJSP.SAISIE_AFFICHAGE, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.TYPE_MEDIA"));%>
            </p><%
        }
        %><div>
            <label for="VIGNETTE" class="colonne"><%= MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.VIGNETTE")%></label>
            <div class="media_vignette_input">
                <%if(infoBean.getString("URL_VIGNETTE").length()>0) { %>
                    <img id="img_vignette" src="<%=infoBean.getString("URL_VIGNETTE")%>" alt="" title="" ><br /><%
                }
                %><span id="div_vignette">
                    <%univFmt.insererContenuChampSaisie(fmt, out, infoBean, "VIGNETTE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_FICHIER, 10, 100, MessageHelper.getCoreMessage("MEDIATHEQUE.MEDIA.SAISIE.VIGNETTE"));%>
                </span>
                <input class="js-thumbnail__delete-button" type="button" value="<%=MessageHelper.getCoreMessage("ST_MEDIATHEQUE_BOUTON_EFFACER")%>"/>
            </div>
        </div>
    </div><%
    }
%></div><!-- #div-ressource -->