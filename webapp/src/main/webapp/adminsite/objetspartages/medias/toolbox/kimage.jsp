<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="org.apache.commons.lang3.StringUtils" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String align = StringUtils.defaultIfBlank(infoBean.get("ALIGN", String.class), "none");
    final String naturalWidth = StringUtils.defaultIfBlank(infoBean.get("NATURAL_WIDTH", String.class), infoBean.get("WIDTH", String.class));
    final String naturalHeight = StringUtils.defaultIfBlank(infoBean.get("NATURAL_HEIGHT", String.class), infoBean.get("HEIGHT", String.class));
%><p>
    <label for="URL" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.URL.LABEL") %></label>
    <input type="text" id="URL" name="URL" size="70" value="<%= StringUtils.defaultString(infoBean.get("URL", String.class)) %>" readonly>
    <a class="recharger" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=PHOTO&FCK_PLUGIN=TRUE&CODE=<%= infoBean.get("CODE") %>&OBJET=<%= infoBean.get("OBJET") %>&ID_FICHE=<%= infoBean.get("ID_FICHE") %>"><%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.EDIT") %></a>
</p>
<p>
    <label for="ALT" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.ALT.LABEL") %> (*)</label>
    <input type="text" id="ALT" name="ALT" size="70" value="<%= StringUtils.defaultString(infoBean.get("ALT", String.class)) %>" required>
</p>
<div class="kimage__parameters">
    <p>
        <label for="WIDTH" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.WIDTH.LABEL") %></label>
        <input class="js-kimage__property-handler" type="text" id="WIDTH" name="WIDTH" size="4" value="<%= StringUtils.defaultString(infoBean.get("WIDTH", String.class)) %>" data-propertyhandler="width" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= naturalWidth %>">
    </p>
    <div class="kimage__parameters-size">
        <button class="js-kimage__button-lock js-kimage__button-lock--locked" type="button" title="<%= MessageHelper.getCoreMessage("KIMAGE.SCALE.TITLE") %>"><span class="icon icon-lock"></span></button>
        <button class="js-kimage__button-reset" type="button" title="<%= MessageHelper.getCoreMessage("KIMAGE.SIZE.TITLE") %>"><span class="icon icon-refresh"></span></button>
    </div>
    <p>
        <label for="HEIGHT" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.HEIGHT.LABEL") %></label>
        <input class="js-kimage__property-handler" type="text" id="HEIGHT" name="HEIGHT" size="4" value="<%= StringUtils.defaultString(infoBean.get("HEIGHT", String.class)) %>" data-propertyhandler="height" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= naturalHeight %>">
    </p>
    <p>
        <label for="BORDER" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.BORDER.LABEL") %></label>
        <input class="js-kimage__property-handler" type="text" id="BORDER" name="BORDER" size="4" value="<%= StringUtils.defaultString(infoBean.get("BORDER", String.class)) %>" data-propertyhandler="border-width" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("BORDER", String.class)) %>">
    </p>
    <p>
        <label for="HORIZONTAL_MARGIN" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.HORIZONTAL_MARGIN.LABEL") %></label>
        <input class="js-kimage__property-handler" type="text" id="HORIZONTAL_MARGIN" name="HORIZONTAL_MARGIN" size="4" value="<%= StringUtils.defaultIfBlank(infoBean.get("HORIZONTAL_MARGIN", String.class), "5") %>" data-propertyhandler="margin-left,margin-right" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("HORIZONTAL_MARGIN", String.class)) %>">
    </p>
    <p>
        <label for="VERTICAL_MARGIN" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.VERTICAL_MARGIN.LABEL") %></label>
        <input class="js-kimage__property-handler" type="text" id="VERTICAL_MARGIN" name="VERTICAL_MARGIN" size="4" value="<%= StringUtils.defaultIfBlank(infoBean.get("VERTICAL_MARGIN", String.class), "4") %>" data-propertyhandler="margin-top,margin-bottom" data-propertyhandler-unit="px" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("VERTICAL_MARGIN", String.class)) %>">
    </p>
    <p>
        <label for="ALIGN" class="colonne"><%= MessageHelper.getCoreMessage("KIMAGE.ALIGN.LABEL") %></label>
        <select class="js-kimage__property-handler" id="ALIGN" name="ALIGN" data-propertyhandler="float" data-propertyhandler-reset="<%= StringUtils.defaultString(infoBean.get("ALIGN", String.class)) %>">
            <option value="none" <%= align.equals("none") ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KIMAGE.ALIGN.UNDEFINED.LABEL") %></option>
            <option value="left" <%= align.equals("left") ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KIMAGE.ALIGN.LEFT.LABEL") %></option>
            <option value="right" <%= align.equals("right") ? "selected" : StringUtils.EMPTY %>><%= MessageHelper.getCoreMessage("KIMAGE.ALIGN.RIGHT.LABEL") %></option>
        </select>
    </p>
</div>
<div class="kimage__preview js-kimage__preview">
    <img src="<%= StringUtils.defaultString(infoBean.get("URL", String.class)) %>" alt="<%= MessageHelper.getCoreMessage("KIMAGE.PREVIEW.ALT") %>" title="<%= MessageHelper.getCoreMessage("KIMAGE.PREVIEW.ALT") %>"/>Lorem ipsum dolor sit amet, consectetuer adipiscing
    elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus
    a, commodo non, facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis,
    nibh non iaculis aliquam, orci felis euismod neque, sed ornare massa mauris sed
    velit. Nulla pretium mi et risus. Fusce mi pede, tempor id, cursus ac, ullamcorper
    nec, enim. Sed tortor. Curabitur molestie. Duis velit augue, condimentum at, ultrices
    a, luctus ut, orci. Donec pellentesque egestas eros. Integer cursus, augue in cursus
    faucibus, eros pede bibendum sem, in tempus tellus justo quis ligula. Etiam eget
    tortor. Vestibulum rutrum, est ut placerat elementum, lectus nisl aliquam velit,
    tempor aliquam eros nunc nonummy metus. In eros metus, gravida a, gravida sed, lobortis
    id, turpis. Ut ultrices, ipsum at venenatis fringilla, sem nulla lacinia tellus,
    eget aliquet turpis mauris non enim. Nam turpis. Suspendisse lacinia. Curabitur
    ac tortor ut ipsum egestas elementum. Nunc imperdiet gravida mauris.
</div>