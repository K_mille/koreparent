<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ taglib prefix="kosmosui" uri="kosmosui"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final String tag = StringUtils.defaultString(infoBean.get("TAG", String.class));
    final String url = StringUtils.defaultString(infoBean.get("URL", String.class));
%>
<c:set var="url" value="<%= url %>"/>
<c:set var="style" value="<%= StringUtils.defaultString(infoBean.get(\"STYLE\", String.class)) %>"/>
<c:set var="download" value="<%= StringUtils.defaultString(infoBean.get(\"DOWNLOAD\", String.class)) %>"/>
<p>
    <label for="TAG" class="colonne"><%= MessageHelper.getCoreMessage("KPDFVIEWER.TAG.LABEL") %></label>
    <input type="text" id="TAG" name="TAG" class="js-tag" size="70" value="<%= tag %>" readonly>
    <a class="recharger" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&TYPE_RESSOURCE=FICHIER&PDFVIEWER=TRUE&FCK_PLUGIN=TRUE&CODE=<%= infoBean.get("CODE") %>&OBJET=<%= infoBean.get("OBJET") %>&ID_FICHE=<%= infoBean.get("ID_FICHE") %>"><%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.EDIT") %></a>
</p>
<div class="kpdfviewer__parameters">
    <p>
        <label for="STYLE" class="colonne"><%= MessageHelper.getCoreMessage("KPDFVIEWER.STYLE.LABEL") %></label>
        <select name="STYLE" id="STYLE" class="js-kpdfviewer__property-handler">
            <option value="PAGE" ${style eq "PAGE" ? ' selected="selected" ' : ""}><%= MessageHelper.getCoreMessage("KPDFVIEWER.PAGE.LABEL")%></option>
            <option value="ALL"  ${style eq "ALL" ? ' selected="selected" ' : ""}><%= MessageHelper.getCoreMessage("KPDFVIEWER.ALL.LABEL")%></option>
        </select>
    </p>
    <p>
        <input class="js-kpdfviewer__property-handler" type="checkbox" id="DOWNLOAD" name="DOWNLOAD" value="1" data-propertyhandler="downloadlink" ${download eq "1" ? 'checked="checked"' : ""} />
        <label for="DOWNLOAD"><%= MessageHelper.getCoreMessage("KPDFVIEWER.DOWNLOAD.LABEL") %></label>
    </p>

</div><%
    if(StringUtils.isNotBlank(url)) {
        %><div class="kpdfviewer__preview js-kpdfviewer__preview">
            <div class="kpdfviewer">
                <kosmosui:pdfviewer id="viewer" documentUrl="${url}" pageByPage="true" rotate="true" hideDownloadLink="false" hideDownloadName="true"/>
                <kosmosui:getScript />
            </div>
        </div><%
    }
%>