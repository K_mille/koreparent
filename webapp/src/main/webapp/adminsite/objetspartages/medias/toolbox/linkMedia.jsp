<%@ page import="org.apache.commons.lang3.StringUtils" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.bean.MediaBean" %>
<%@ page import="com.univ.objetspartages.services.ServiceMedia" %>
<%@ page import="com.univ.objetspartages.util.MediaUtils" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
    final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    final String stringIdMedia = infoBean.get("ID_MEDIA", String.class);
    final Long idMedia = StringUtils.isNumeric(stringIdMedia) ? Long.parseLong(stringIdMedia) : 0L;
    final MediaBean media = serviceMedia.getById(idMedia);
    if(media != null) {
        %><input type="hidden" name="ID_RESSOURCE" value="<%= infoBean.get("ID_MEDIA", String.class) %>">
        <p>
            <a href="<%= infoBean.get("URL_RESSOURCE", String.class) %>?INLINE=FALSE" title="<%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.PREVIEW") %>"><%= MediaUtils.getLibelleAffichable(media) %></a>
            <a class="button recharger" href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&ACTION=INSERER&FCK_PLUGIN=TRUE&MODE_FICHIER=LIEN&CODE=<%= infoBean.get("CODE") %>&OBJET=<%= infoBean.get("OBJET") %>&ID_FICHE=<%= infoBean.get("ID_FICHE") %>"><%= MessageHelper.getCoreMessage("LINK_TYPE.MEDIA.EDIT") %></a>
        </p><%
    }
%>

