<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ include file="/adminsite/objetspartages/template/init_recherche.jsp" %>
<%@ include file="/adminsite/objetspartages/template/header_recherche.jsp" %>
<%
    if (!listeIncluse) {
        %><div class="fieldset neutre"><%
    }
    if (!"LIEN_REQUETE".equals(toolbox)) {
        if (listeIncluse) {
            %><fieldset>
                <legend><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CRITERES_METIERS") %></legend><%
        }
        univFmt.insererChampSaisie(fmt, out, infoBean, "TITRE", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 255, MessageHelper.getCoreMessage("BO_PAGELIBRE_TITRE"));
        univFmt.insererChampSaisie(fmt, out, infoBean, "MOTS_CLES", FormateurJSP.SAISIE_FACULTATIF, FormateurJSP.FORMAT_TEXTE, 0, 32, MessageHelper.getCoreMessage("BO_PAGELIBRE_MOTS_CLES"));
        if (listeIncluse) {
            %></fieldset><%
        }
    }
    %><%@ include file="/adminsite/objetspartages/template/footer_recherche.jsp" %><%
    if (!listeIncluse) {
        %></div><%
    }
%>
