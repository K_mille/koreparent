<%@ page import="com.jsbsoft.jtf.core.FormateurJSP" %>
<%@ page import="com.jsbsoft.jtf.core.ProcessusHelper" %>
<%@ page import="com.kportal.extension.module.composant.IComposant" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%
IComposant module = ProcessusHelper.getComposantParNomProcessus(infoBean.getNomProcessus());
%>
<form action="/servlet/com.jsbsoft.jtf.core.SG" method="post">
    <div id="actions">
        <div>
            <ul><%
                if (!"AJOUTER".equals(infoBean.getActionUtilisateur())) {%>
                    <li><button class="supprimer" data-confirm="<%=module.getMessage("BO_CONFIRM_SUPPR_PROFIL") %>" type="submit" name="SUPPRIMER" value="<%= module.getMessage("JTF_BOUTON_SUPPRIMER")%>" ><%= module.getMessage("JTF_BOUTON_SUPPRIMER")%></button></li><%
                }
                %><li><button class="enregistrer" type="submit" name="VALIDER" value="<%= module.getMessage("BO_ENREGISTRER")%>" ><%= module.getMessage("BO_ENREGISTRER")%></button></li>
            </ul>
            <div class="clearfix"></div>
            <span title="<%= module.getMessage("BO_FERMER") %>" id="epingle">&ndash;</span>
        </div>
    </div><!-- #actions -->
    <br/>
    <div id="content" role="main"><%
        Long idProfil = null;
        if (infoBean.get("ID_PROFIL") != null && infoBean.get("ID_PROFIL") instanceof Long) {
            idProfil = (Long)infoBean.get("ID_PROFIL");
        }
        if (idProfil != null) {
            %><input type="hidden" name="ID_PROFIL" value="<%=idProfil%>" /><%
        }%>
        <input type="hidden" name="action" value="">
        <% fmt.insererVariablesCachees( out, infoBean); %>
        <fieldset>
            <legend><%= module.getMessage("BO_PROFIL_DSI_INFOS_GENERALES") %></legend>
            <p>
                <label for="CODE" class="colonne"><%= module.getMessage("BO_LIBELLE_CODE") %> (*)</label>
                <%fmt.insererChampSaisie( out, infoBean, "CODE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 20); %>
            </p>
            <p>
                <label for="LIBELLE" class="colonne"><%= module.getMessage("ST_TABLEAU_LIBELLE") %>(*)</label>
                <%fmt.insererChampSaisie( out, infoBean, "LIBELLE", FormateurJSP.SAISIE_OBLIGATOIRE, FormateurJSP.FORMAT_TEXTE, 0, 255); %>
            </p>
            <%univFmt.insererkMonoSelect(fmt, out, infoBean, "CODE_RUBRIQUE_ACCUEIL", FormateurJSP.SAISIE_OBLIGATOIRE, module.getMessage("BO_PROFIL_DSI_RUBRIQUE_ACCUEIL"), "rubrique", univFmt.CONTEXT_ZONE);%>
        </fieldset>

        <fieldset>
            <legend><%= module.getMessage("BO_PROFIL_DSI_GROUPES") %></legend>
            <p><%= module.getMessage("BO_PROFIL_DSI_UTILISATEURS_GROUPES") %></p>
             <%univFmt.insererKmultiSelectTtl(fmt, out, infoBean, "GROUPES", FormateurJSP.SAISIE_FACULTATIF, module.getMessage("BO_PROFIL_DSI_LISTE_GROUPES"), "TMP_GROUPES", "Groupe", univFmt.CONTEXT_GROUPEDSI_RESTRICTION, "");%>
        </fieldset>
    </div><!-- #content -->
</form>
