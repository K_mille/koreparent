<%@page import="com.kportal.core.config.MessageHelper"%>
<%@ page errorPage="/adminsite/jsbexception.jsp" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />

<div id="content" role="main">
<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post">
    <input type="hidden" name="ACTION" value="">

<% fmt.insererVariablesCachees( out, infoBean); %>

<div class="content_popup">
    <table width="100%" cellspacing="5" border="0" cellpadding="0" summary="">
        <tr >
        <td width="100%" align="center">
            <i><%= MessageHelper.getCoreMessage("BO_RUBRIQUE_SELECTION_FICHE_A_REFERENCER") %></i><br><br>
        </td>
        </tr>
    </table>
    <table class="datatableLight">
        <thead>
            <tr >
                <th><input id="idsMetaCheckAll" type="checkbox" name="idsMeta" onclick="metaCheck(this)"/></th>
                <th><%= MessageHelper.getCoreMessage("BO_TYPE") %></th>
                <th><%= MessageHelper.getCoreMessage("BO_INTITULE") %></th>
                <th><%= MessageHelper.getCoreMessage("BO_RUBRIQUE") %></th>
                <th><%= MessageHelper.getCoreMessage("BO_DATE") %></th>
                <th class="sanstri sansfiltre"><%= MessageHelper.getCoreMessage("BO_ACTIONS") %></th>
            </tr>
        </thead>
        <tbody>
        <%
        int nbLignes = infoBean.getInt("RECHERCHE_FICHES_REFERENCEES_NB_ITEMS");
        for (int i=0; i< nbLignes; i++) { %>
            <tr>
                <td>
                    <%fmt.insererChampSaisie( out, infoBean, "RECHERCHE_FICHES_REFERENCEES_SELECTION#"+i ,  fmt.SAISIE_FACULTATIF , fmt.FORMAT_CHECKBOX, 0, 1); %>
                </td>
                <td >
                    <%= infoBean.getString( "RECHERCHE_FICHES_REFERENCEES_LIBELLE_TYPE#"+i) %>
                </td>
                <td >
                    <%= infoBean.getString( "RECHERCHE_FICHES_REFERENCEES_INTITULE#"+i) %>
                </td>
                <td>
                    <%= infoBean.getString( "RECHERCHE_FICHES_REFERENCEES_INTITULE_RUBRIQUE#"+i) %>
                </td>
                <td>
                    <%= infoBean.getString("RECHERCHE_FICHES_REFERENCEES_DATE#"+i) %>
                </td>
                <td>
                    <a href="#" onclick="window.open('<%= infoBean.getString("RECHERCHE_FICHES_REFERENCEES_URL#"+i) %>','apercu');return false"><%= MessageHelper.getCoreMessage("BO_APERCU") %></a>
                </td>
            </tr>
        <% } %>
        </tbody>
    </table>
</div>

<div class="footer_popup">
    <p class="validation">
        <input type="submit" id="VALIDER" name="VALIDER" value="Valider"/>
        <input type="reset" id="REVENIR" name="REVENIR" value="Revenir" onclick="goBack();"/>
    </p>
</div>

</form>
</div>

<script>

    function goBack(){
        jQuery('form input[name="ACTION"]').val("REVENIR");
        jQuery('form')[0].submit();
    }

    function metaCheck(element){
        jQuery('input[id][name]').prop('checked', element.checked);
    }

</script>
