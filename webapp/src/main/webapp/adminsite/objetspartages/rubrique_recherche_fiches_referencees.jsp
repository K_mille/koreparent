<%@page import="com.kportal.core.config.MessageHelper"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmtJsp" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />

<form action="/servlet/com.jsbsoft.jtf.core.SG" enctype="multipart/form-data" method="post" data-no-tooltip>

    <input type="hidden" name="ACTION" value="">
    <%fmtJsp.insererVariablesCachees(out, infoBean);%>

    <div id="content" role="main" class="content_popup">
        <jsp:include page="/adminsite/recherche_directe.jsp">
            <jsp:param value="TRUE" name="TOOLBOX"/>
        </jsp:include>
        <div class="footer_popup">
            <p class="validation">
                <input type="submit" id="VALIDER" name="VALIDER" value="<%= MessageHelper.getCoreMessage("BO_VALIDER") %>"/>
                <input type="reset" id="REVENIR" name="REVENIR" value="<%= MessageHelper.getCoreMessage("JTF_BOUTON_REVENIR") %>" onclick="goBack();"/>
            </p>
        </div>
    </div>
</form>

<script>
    function goBack(){
        jQuery('form input[name="ACTION"]').val("REVENIR");
        jQuery('form')[0].submit();
    }
</script>
