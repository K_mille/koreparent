<%@ page import="com.univ.utils.EscapeString" %>
<%@ page errorPage="/adminsite/jsbexception.jsp" %> 
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<script type="text/javascript">
    window.iFrameHelperCallback = function() {
        if(window.iFrameRegistration){
            iFrameHelper.sendValues(window.iFrameRegistration, {sCode: '<%=EscapeString.escapeJavaScript(infoBean.getString("CODE"))%>', titre: '<%= EscapeString.escapeJavaScript(infoBean.getString("TITRE"))%>', langue: '<%=infoBean.getString("LANGUE")%>'});
        }
    }
</script>