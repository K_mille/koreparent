<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.objetspartages.bean.MediaBean" %>
<%@ page import="com.univ.objetspartages.om.MediaPhoto" %>
<%@ page import="com.univ.objetspartages.services.ServiceMedia" %>
<%@ page import="com.univ.objetspartages.util.MediaUtils" %>
<%
    final ServiceMedia serviceMedia = ServiceManager.getServiceForBean(MediaBean.class);
    if( request.getParameter("ID_MEDIA") != null ) {
        MediaBean media = serviceMedia.getById(new Long(request.getParameter("ID_MEDIA")));
        %><html>
            <head>
                <meta http-equiv="content-type" content="text/html; charset=utf-8" />
                <title><%=media.getTitre()%></title>
                <link rel="icon" type="image/png" href="/images/favicon_backoffice.png" />
            </head>
            <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" id="body_couleur_fond">
              <table width="100%" height="100%" summary="">
                <tr>
                  <td width="100%" height="100%" align="center" valign="middle">
                  <img src="<%= MediaUtils.getUrlAbsolue(media) %>" width="<%= serviceMedia.getSpecificData(media, MediaPhoto.ATTRIBUT_LARGEUR) %>" height="<%= serviceMedia.getSpecificData(media, MediaPhoto.ATTRIBUT_HAUTEUR) %>" alt="" />
                  </td>
                </tr>
              </table>
            </body>
        </html>
        <%
    }
%>