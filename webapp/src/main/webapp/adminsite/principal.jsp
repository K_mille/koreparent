<%@page import="java.util.Locale"%>
<%@page import="com.kportal.extension.module.composant.Menu"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@page import="com.kportal.ihm.utils.AdminsiteUtils"%>
<%@page import="com.univ.utils.ContexteUniv" %>
<%@page import="com.univ.utils.ContexteUtil" %>
<%@ taglib prefix="resources" uri="http://kportal.kosmos.fr/tags/web-resources" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
ContexteUniv ctx = ContexteUtil.getContexteUniv();
Menu menuCourant = AdminsiteUtils.getMenuDepuisInfoBean(infoBean);
Locale locale = ctx.getLocale();
boolean isNotEcranPopup = AdminsiteUtils.isNotEcranPopup(infoBean);
%><!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang="<%= locale.getLanguage() %>"> <![endif]-->
<!--[if gt IE 8]><!--><html class="no-js" lang="<%= locale.getLanguage() %>"><!--<![endif]-->
    <head>
        <meta charset="UTF-8"/>
        <title><%=menuCourant.getLibelle() %></title>
        <meta name="description" content="<%=menuCourant.getLibelle() %>">
        <meta name="viewport" content="width=device-width">
        <link rel="shortcut icon" type="image/x-icon" href="/adminsite/images/favicon.ico" />
        <link rel="icon" type="image/png" href="/adminsite/images/favicon.png" />
        <!--[if lte IE 7]>
            <link rel="stylesheet" href="/adminsite/styles/fonts/icones/ie7/ie7.css">
            <script src="/adminsite/styles/fonts/icones/ie7/ie7.js"></script>
        <![endif]-->
        <resources:link group="stylesBo"/>
        <link rel="stylesheet" href="/adminsite/styles/screen.css">
        <script src="/adminsite/scripts/libs/ckeditor/ckeditor.js"></script>
        <script src="/adminsite/toolbox/toolbox.js"></script>
        <script type="text/javascript">
            var html = document.getElementsByTagName('html')[0];
            html.className = html.className.replace('no-js', 'js');
        </script>
    </head>
    <body <%= (isNotEcranPopup) ? "" : "class=\"toolbox\"" %>><%
        if (isNotEcranPopup) {
            %><jsp:include page="/adminsite/template/header.jsp" /><%
        }
        %><div id="page"><%
            if (isNotEcranPopup) {
                %><jsp:include page="/adminsite/template/entete_page.jsp"></jsp:include><%
            }
            %><jsp:include page="<%=infoBean.getEcranPhysique()%>" />
        </div><!-- #page --><%
        if (isNotEcranPopup) {
            %><jsp:include page="/adminsite/template/footer.jsp" /><%
        }
        %><jsp:include page="/adminsite/sso/propagation.jsp" />
        <resources:script group="scriptsBo" locale="<%= locale.toString() %>"/>
    </body>
</html>
