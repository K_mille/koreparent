<%@ page import="com.jsbsoft.jtf.core.LangueUtil" %>
<%@ page import="com.kportal.cms.objetspartages.Objetpartage" %>
<%@ page import="com.kportal.core.config.MessageHelper" %>
<%@ page import="com.univ.objetspartages.om.ReferentielObjets" %>
<%@ page import="com.univ.utils.ContexteUniv" %>
<%@ page import="com.univ.utils.ContexteUtil" %>
<%@ page import="com.univ.utils.EscapeString" %>
<%@ page import="com.univ.utils.URLResolver" %>
<jsp:useBean id="frontOfficeBean" class="com.univ.url.FrontOfficeBean" scope="request" />
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" /><%

    String codeObjet = infoBean.getString("CODE_OBJET");
    Objetpartage objet = ReferentielObjets.getObjetByCode(codeObjet);
    ContexteUniv ctx = ContexteUtil.getContexteUniv();

    if (request.getParameter("RELOAD") != null) { %>
        <script type="text/javascript">
            top.parent.opener.location.reload();
        </script>
<%  }

    if ((infoBean.get("GRS_URL_MODIFICATION_EXPERT") != null && ! objet.isStrictlyCollaboratif()) || infoBean.get("GRS_URL_SUPPRESSION")!=null || "1".equals(infoBean.getString("GRS_APERCU"))) { %>
        <p id="menu_modification">
        <% if (infoBean.get("GRS_URL_MODIFICATION_EXPERT") != null && ! objet.isStrictlyCollaboratif()) { %>
            <a <%=infoBean.getString("GRS_URL_MODIFICATION_EXPERT")%> id="mode_expert" class="button"><%=MessageHelper.getCoreMessage("ST_FRONT_MODIFICATION_MODE_EXPERT")%></a><%
        }
        if (infoBean.get("GRS_URL_SUPPRESSION") != null) { %>
            <a href="<%=infoBean.getString("GRS_URL_SUPPRESSION")%>" id="suppression" class="button"><%=MessageHelper.getCoreMessage("ST_FRONT_SUPPRESSION")%></a>
        <% }
        if ("1".equals(infoBean.getString("GRS_APERCU"))) { %>
            <button onclick="afficherApercu();" class="button" id="apercu"><%=MessageHelper.getCoreMessage("ST_FRONT_APERCU")%></button>
        <% } %>
        </p><%
    }
    String classe = "";
    if (frontOfficeBean.isCollaboratif()) {
        classe = "edition_collaboratif";
    } else if (frontOfficeBean.isDsi() || frontOfficeBean.isSaisieFront()) {
        classe = "edition_fiche";
    }
%>
<form id="form_saisie_front" class="<%= classe %>" action="<%= URLResolver.getAbsoluteUrl("/servlet/com.jsbsoft.jtf.core.SG", ContexteUtil.getContexteUniv()) %>" enctype="multipart/form-data" method="post">

<div class="formulaire_hidden">
    <input type="hidden" name="ACTION" value="ENREGISTRER" />
    <input type="hidden" name="APERCU" value="0" />
    <input type="hidden" name="ID_<%=objet.getNomObjet()%>" value="<%= infoBean.getString("ID_"+objet.getNomObjet())%>" />
    <input type="hidden" name="ID_FICHE" value="<%= infoBean.getString("ID_"+objet.getNomObjet())%>" />
    <input type="hidden" name="LANGUE" value="<%=LangueUtil.getIndiceLocale(ctx.getLocale())%>" />
    <input type="hidden" name="CODE_OBJET" value="<%= codeObjet%>" />
    <input type="hidden" name="TS_CODE" value="<%= infoBean.getString("TS_CODE")%>" />
    <input type="hidden" name="SOUS_ONGLET_DEMANDE" value="" />
    <!-- CFL 20080616 : HYPER IMPORTANT POUR CHARGEMENT MULTIPLES FCKEDITOR SUR UNE MEME PAGE
         RESOUD LE PROBLEME DE CHARGEMENT INFINI (bug fck avec firefox...)-->
    <input type="hidden" name="FCK_EDITORS_NAMES" value="" />
    <input type="hidden" name="GRS_SAUVEGARDE_CODE_RUBRIQUE" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RUBRIQUE") %>" />
    <input type="hidden" name="GRS_SAUVEGARDE_CODE_RATTACHEMENT" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT") %>" />
    <input type="hidden" name="GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE" value="<%= infoBean.getString("GRS_SAUVEGARDE_PUBLIC_VISE_ESPACE") %>" />
    <input type="hidden" name="GRS_SAUVEGARDE_CODE_RATTACHEMENT_AUTRES" value="<%= infoBean.getString("GRS_SAUVEGARDE_CODE_RATTACHEMENT_AUTRES") %>" />
    <input type="hidden" name="SAISIE_FRONT" value="true" />
    <% if (infoBean.get("ESPACE") != null) { %>
    <input type="hidden" name="ESPACE" value="<%= infoBean.getString("ESPACE") %>" />
    <input type="hidden" name="LANGUE" value="<%=ctx.getEspace().getLangue()%>" />
    <% } %>
    <% if( infoBean.get("URL_REDIRECT") != null) { %>
    <input type="hidden" name="URL_REDIRECT" value="<%= infoBean.getString("URL_REDIRECT") %>" />
    <% } %>

    <!-- Utile pour la restauration des données -->
    <% if (infoBean.get("ID_CI_RESTAURATION") != null)    { %>
        <input type="hidden" name="ID_CI_RESTAURATION" value="<%= infoBean.getString("ID_CI_RESTAURATION")%>" />
        <input type="hidden" name="NOM_JSP_RESTAURATION" value="<%= infoBean.getString("NOM_JSP_RESTAURATION")%>" />

    <% }
    fmt.insererVariablesCachees(out, infoBean); %>
</div> <!-- .formulaire_hidden -->

<% if (infoBean.getMessageErreur().length() > 0) { %>
    <p id="msg-erreur"><%=EscapeString.escapeScriptAndEvent(infoBean.getMessageErreur())%></p>
<% } %>
<p class="msg-aide"><%=MessageHelper.getCoreMessage("ST_FRONT_INTITULES")%> <span class="obligatoire">*</span> <%=MessageHelper.getCoreMessage("ST_FRONT_OBLIGATOIRES")%></p>

<script type="text/javascript">
    function changerOnglet(nom_onglet) {
        oFormSaisieFront = window.document.forms['form_saisie_front'];
        oFormSaisieFront.ACTION.value = "ONGLET" ;
        oFormSaisieFront.SOUS_ONGLET_DEMANDE.value = nom_onglet ;
        oFormSaisieFront.submit();
    }
    function afficherApercu() {
        oFormSaisieFront = window.document.forms['form_saisie_front'];
        oFormSaisieFront.APERCU.value="1";
        oFormSaisieFront.ACTION.value="ENREGISTRER";
        oFormSaisieFront.target="apercu";
        oFormSaisieFront.submit();
        // NOTE : sauf sous Windows pas de target, interdit de modifier apres le submit
        oFormSaisieFront.ACTION.value="";
        oFormSaisieFront.APERCU.value="0";
        oFormSaisieFront.target = '';
    }
</script>
