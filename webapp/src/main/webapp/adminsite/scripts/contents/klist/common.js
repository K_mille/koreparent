(function initTabs($) {
    'use strict';
    var $tabs = $('.js-tabs');
    if ($tabs.length > 0) {
        $tabs.each(
            function () {
                $(this).tabs({});
            });
    }
})(jQuery.noConflict());
