(function ($) {
    'use strict';
    var rssUrl = document.getElementById('rss_url'),
        nbMax = document.getElementById('rss_nbmax');

    function buildAndSendMessage() {
        var $form = $(rssUrl).closest('form');
        var tagValue = '[traitement;rss;';
        if (nbMax.value) {
            tagValue += nbMax.value + '+';
        }
        tagValue += rssUrl.value + ']';
        var message = {
            'tag': 'krss',
            'value': tagValue,
            '$form': $form
        };
        COMMONS_MESSAGES.postMessageToParent(message);
    }

    $('#rss_url, #rss_nbmax').on('change keyup', buildAndSendMessage);

    // Modification mode
    if ($('.js-tabs').tabs().isPanelActive('krss')) {
        buildAndSendMessage();
    }
})(jQuery.noConflict());
