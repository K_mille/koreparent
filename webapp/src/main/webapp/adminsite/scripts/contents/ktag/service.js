(function ($) {
    'use strict';
    var $tabs = $('.js-tabs'),
        serviceLink = document.getElementById('js-service_link'),
        serviceCode = document.getElementById('code_service'),
        tagType = document.getElementById('choix_tag_service');

    function proceedTag() {
        if (COMMONS_KTAG.isValidInput(serviceCode) && COMMONS_KTAG.isValidInput(tagType)) {
            var label;
            if (tagType.value === 'service_link') {
                label = document.getElementById('nom_link').value;
            }
            var tagValue = '[service_debut;' + serviceCode.value
                + '][' + tagType.value;
            if (label) {
                tagValue += ';' + label;
            }
            tagValue += '][service_fin]';
            COMMONS_MESSAGES.postMessageToParent({'tag': 'kservice', 'value': tagValue});
        } else {
            COMMONS_MESSAGES.postMessageToParent('erase');
        }
    }

    function toggleIhm(currentElement) {
        if (currentElement.value) {
            if (serviceLink.id === 'js-' + currentElement.value) {
                serviceLink.className = serviceLink.className.replace(/masquer/g, '');
            } else {
                COMMONS_KTAG.hideBlock(serviceLink);
            }
        }
    }

    function onTabChange() {
        if ($tabs.tabs().isPanelActive('kservice')) {
            proceedTag();
        }
    }

    $('#choix_tag_service, #code_service, #nom_vue, #nom_link').on(
        'change keyup', function () {
            if (this.id === 'choix_tag_service') {
                toggleIhm(this);
            }
            proceedTag();
        });
    $tabs.on('TabChange', onTabChange);
    // Modification mode
    onTabChange();
})(jQuery.noConflict());
