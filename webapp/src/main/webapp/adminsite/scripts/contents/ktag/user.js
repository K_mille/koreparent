(function($) {
    'use strict';

    function buildAndSendMessage() {
        var $this = $('input[name="userTag"]:checked');
        if ($this.val()) {
            var message = { 'tag': 'kuser', 'value': $this.val()};
            COMMONS_MESSAGES.postMessageToParent(message);
        }
    }

    $('input[name="userTag"]').on('change', buildAndSendMessage);

    // Modification mode
    if ($('.js-tabs').tabs().isPanelActive('kuser')) {
        buildAndSendMessage();
    }

})(jQuery.noConflict());
