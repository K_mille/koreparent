(function ($) {
    'use strict';
    var $url = $('#URL');

    function hasProtocol(url) {
        return url.indexOf('http://') !== -1 || url.indexOf('https://') !== -1 || url.indexOf('ftp://') !== -1;
    }

    function fixUrl() {
        if ($url.val() && !hasProtocol($url.val())) {
            var values = $url.val().split('://');
            $url.val('http://' + (values.length === 2 ? values[1] : values[0]));
            $url.trigger('change');
        }
    }

    $url.on('change', fixUrl);

    function onTabChange(e) {
        var $panel = e.originalEvent.data;
        if ($panel && $panel.is('[data-panel="external"]')) {
            fixUrl();
        }
    }
    $('.js-tabs').on('TabChange', onTabChange);

    // Modification mode
    fixUrl();
})(jQuery.noConflict());
