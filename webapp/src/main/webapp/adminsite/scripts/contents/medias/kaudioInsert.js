function onDialogEvent(e) {
    'use strict';
    if (e.name === 'load') {
        (function ($) {

            function checkOkButton() {
                var $previewAudio = $('.audio-container');
                if ($previewAudio.length > 0) {
                    e.sender.getButton('ok').enable();
                } else {
                    e.sender.getButton('ok').disable();
                }
            }
            function initKAudioHandling() {
                checkOkButton();
            }

            initKAudioHandling();
        })(jQuery.noConflict());
    }
}