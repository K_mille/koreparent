function onDialogEvent(e) {
    'use strict';
    if (e.name === 'load') {
        (function ($) {
            function handleMediaList() {
                $('.js-media-list__container, .js-media-list__edit-button').on(
                    'click', function () {
                        var $this = $(this);
                        $('input[type="hidden"][name="ACTION"]').val($this.attr('data-action'));
                        $('input[type="hidden"][name="ID_MEDIA"]').val($this.attr('data-idmedia'));
                        $this.closest('form').submit();
                    });
            }

            function checkOkButton() {
                var $previewkpdfviewer = $('.js-kpdfviewer__preview > .kpdfviewer');
                if ($previewkpdfviewer.length > 0) {
                    e.sender.getButton('ok').enable();
                } else {
                    e.sender.getButton('ok').disable();
                }
            }

            function updatePreview() {
                var propsHandler = document.querySelectorAll('.js-kpdfviewer__property-handler'),
                    container = document.querySelector('.pdf-viewer'),
                    pageByPageContainer = container.querySelector('.js-previousPage').parentElement,
                    downloadLink = container.querySelector('.js-pdfFallback');
                [].forEach.call(propsHandler, function (element) {
                    if (element.tagName === 'INPUT') {
                        if (element.checked) {
                            downloadLink.classList.remove('hide');
                        } else {
                            downloadLink.classList.add('hide');
                        }
                    } else {
                        if (element.value === 'PAGE') {
                            pageByPageContainer.classList.remove('hide');
                            container.setAttribute('data-pageByPage', 'true');
                        } else {
                            pageByPageContainer.classList.add('hide');
                            container.removeAttribute('data-pageByPage');
                        }
                    }
                });
                container.classList.add('js-initialize');
                container.innerHTML += '<span class="hide"></span>';
            }
            function bindPropertyHandler() {
                var $propertyHandlers = $('.js-kpdfviewer__property-handler'),
                    $currentTag = $('.js-tag');
                $propertyHandlers.on(
                    'change keyup', function (ev) {
                        var currentTagValue = $currentTag.val();
                        if (ev.target.tagName === 'INPUT') {
                            var newValue = currentTagValue.replace(/DOWNLOAD=\d/,
                                'DOWNLOAD=' + (ev.target.checked ? '1' : '0'));
                            $currentTag.val(newValue);
                        } else {
                            $currentTag.val(currentTagValue.replace(/STYLE=(PAGE|ALL)/, 'STYLE=' + ev.target.value));
                        }
                        updatePreview();
                    });
            }

            function initkpdfviewerHandling() {
                handleMediaList();
                bindPropertyHandler();
                checkOkButton();
                updatePreview();
            }

            initkpdfviewerHandling();
        })(jQuery.noConflict());
    }
}
