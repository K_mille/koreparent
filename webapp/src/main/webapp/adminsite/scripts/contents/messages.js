var COMMONS_MESSAGES = function commons() {
    'use strict';
    var url = window.location.href,
        splittedUrl = url.split('/'),
        domain = splittedUrl[0] + '//' + splittedUrl[2];
    return {
        postMessageToParent: function postMessageToParent(message) {
            if (parent) {
                if (message.$form && message.$form.valid && !message.$form.valid()) {
                    parent.postMessage('erase', domain);
                } else {
                    delete message.$form;
                    parent.postMessage(message, domain);
                }
            } else {
                throw Error('no parent available for the current page');
            }
        }
    };
}();

