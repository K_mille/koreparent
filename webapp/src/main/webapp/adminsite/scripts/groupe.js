(function ($) {

    var $selectGroupe = $('#REQUETE_GROUPE');

    function switchDynPanel() {
        var $panels = $('.js-dyn_panel');
        $panels.css({'display': 'none'});
        $panels.filter('[data-panel="' + $selectGroupe.val() + '"]').css({'display': 'block'});
    }

    if ($selectGroupe.length > 0) {
        $selectGroupe.change(switchDynPanel);
        switchDynPanel();
    }

})(jQuery.noConflict());