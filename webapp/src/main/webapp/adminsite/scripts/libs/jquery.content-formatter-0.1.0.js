/*
 * $contentFormatter jQuery plugin
 *
 * version 0.1.0
 *
 * Kosmos
 *
 * Files below need to be referenced in the calling page for the component to run correctly :
 *
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 *
 */
(function ($) {
    'use strict';
    $.contentFormatter = function (root, options) {
        var $root = $(root),
            currentElement,
            settings;
        //////////////////////////////////////////////////////
        //					EVENTS HANDLING					//
        //////////////////////////////////////////////////////
        // Notify all registered handlers that 'event' occured
        function notifyForEvent(event, data) {
            $.each(
                settings.bindingEvents, function (index, bindings) {
                    if ($.isFunction(bindings[event])) {
                        bindings[event].call(undefined, data);
                    }
                });
        }

        ////////////////////EVENTS HANDLING///////////////////
        //////////////////////////////////////////////////////
        //					PRIVATE METHODS					//
        //////////////////////////////////////////////////////
        // Default behavior for events callback
        function noop() {}

        // Switch the current handled element according to given pattern
        function handleElement(pattern) {
            currentElement = settings.elements[pattern];
            if (!settings.handleFallback) {
                $root.val('');
            } else {
                $root.val(currentElement.fallback ? currentElement.fallback : '');
            }
        }

        // Check if a value contains any placeholders
        function hasPlaceHolders(value) {
            return value.search(/(\#|~)\([A-Za-z0-9\-_\.]+\)/g) !== -1;
        }

        // Generate on objet according to the given fields
        function generateFieldsObject($fields) {
            var result = {};
            $fields.each(
                function () {
                    var $this = $(this);
                    if ($this.attr('name') && $this.val()) {
                        if ($this.attr('type') === 'radio') {
                            if ($this.prop('checked')) {
                                result[$this.attr('name')] = $this.val();
                            }
                        } else {
                            result[$this.attr('name')] = $this.val();
                        }
                    }
                });
            return result;
        }

        // Replace all the placeholders that can be found
        function replaceOptionalPlaceHolders(pattern) {
            var regex = new RegExp('~\\([A-Za-z0-9\\-_\\.]+\\)', 'g');
            pattern = pattern.replace(regex, '');
            return pattern;
        }

        // Replace all the placeholders that can be found
        function replacePlaceHolders(pattern, fieldsObject) {
            var result = pattern;
            $.each(
                fieldsObject, function (property, value) {
                    var regex = new RegExp('(#|~)\\(' + property + '\\)', 'g');
                    result = result.replace(regex, value);
                });
            result = replaceOptionalPlaceHolders(result);
            return result;
        }

        // Update formatter value
        function updateFormatter(values) {
            var $fields = $(currentElement.watch, currentElement.$item),
                value = values;
            if (typeof value !== 'string') {
                value = values ? replacePlaceHolders(currentElement.pattern, values) : replacePlaceHolders(currentElement.pattern, generateFieldsObject($fields));
            }
            if (currentElement.validator.test(value) && !hasPlaceHolders(value)) {
                $root.val(value);
                currentElement.fallback = value;
                notifyForEvent('afterValidate', settings.handleFallback ? {'fallbackValue': currentElement.fallback} : null);
            } else {
                notifyForEvent('validationError', settings.handleFallback ? {'fallbackValue': currentElement.fallback} : null);
                if (settings.handleFallback) {
                    $root.val(currentElement.fallback);
                } else {
                    $root.val('');
                }
            }
        }

        // Return an element for the given arguments
        function createElement($element, watchSelector, patternName, validatorName) {
            return {
                '$item': $element,
                'watch': watchSelector,
                'pattern': $element.attr(patternName) || '',
                'validator': new RegExp($element.attr(validatorName)) || new RegExp('.')
            };
        }

        // Add an observer on all fields matching the watch selector under the element's $item
        function addWatcher(element) {
            $(element.watch, element.$item).on(
                'change keyup', function () {
                    updateFormatter();
                });
        }

        // Add an element to the list of the watched elements
        function registerElement($element, watchSelector, patternName, validatorName) {
            var element = createElement($element, watchSelector, patternName, validatorName);
            addWatcher(element);
            settings.elements[element.pattern] = element;
            return element;
        }

        // Use at init phase to crawl over the contents identified by the defaults selectors
        function autoCreateElements() {
            var $elements = $(settings.elementsSelector);
            $elements.each(
                function (i) {
                    var element = registerElement($(this), settings.watcherSelector, settings.patternName, settings.validatorName);
                    if (i === 0) {
                        currentElement = element;
                    }
                });
        }

        // Initialize the component
        function init() {

            // Default settings
            var defaults = {
                'elementsSelector': '.js-contentformatter__element',
                'watcherSelector': 'input, select, textarea',
                'patternName': 'data-contentformatter-pattern',
                'validatorName': 'data-contentformatter-validator',
                'handleFallback': true,
                'handleAtInit': ''
            };
            // Merge defaults and options settings
            settings = $.extend(true, {'elements': {}}, defaults, options);
            settings.bindingEvents = [{
                'beforeValidate': noop,
                'afterValidate': noop,
                'beforeReplacement': noop,
                'afterReplacement': noop,
                'validationError': noop
            }];
            $.data(root, 'contentFormatter', $root);
            // Handle elements and update formatter
            autoCreateElements();
            if (settings.handleAtInit) {
                currentElement = settings.elements[settings.handleAtInit];
                if (settings.handleFallback) {
                    currentElement.fallback = $root.val();
                }
            }
            updateFormatter($root.val());
        }

        ////////////////////PRIVATE METHODS///////////////////
        // Register a new element
        $root.registerElement = function ($element, watchSelector, patternName, validatorName) {
            registerElement($element, watchSelector, patternName, validatorName);
        };
        // Add a listener object with specific callbacks
        $root.registerListener = function (listener) {
            settings.bindingEvents.push(listener);
            updateFormatter($root.val());
        };
        // Ask the formatter to handle the given pattern
        $root.handle = function (pattern) {
            handleElement(pattern);
            if ($root.val()) {
                updateFormatter($root.val());
            }
        };
        // Update the formatter according the given values
        // i.e : $contentFormatter.update({
        //          'CODE': 'someCode',
        //          'OBJET': 'someObjectValue'
        //       }
        // Where each property of the object is a place holder value.
        $root.update = function (values) {
            updateFormatter(values);
        };
        $root.clear = function () {
            $root.val('');
            notifyForEvent('onClear', null);
        };
        $root.isValid = function () {
            var value = $root.val();
            return value && currentElement.validator.test(value) && !hasPlaceHolders(value);
        };
        //////////////////////////////////////////////////////
        //					INITIALIZATION					//
        //////////////////////////////////////////////////////
        // Initialize the component
        init();
        ////////////////////INITIALIZATION////////////////////
    };
    $.fn.contentFormatter = function (options) {
        if (options) {
            options = options || {};
            if (typeof options === 'object') {
                this.each(
                    function () {
                        if ($(this).data('contentFormatter')) {
                            if (options.remove) {
                                $(this).data('contentFormatter').remove();
                                $(this).removeData('contentFormatter');
                            }
                        } else if (!options.remove) {
                            if (options.enable === undefined && options.disable === undefined) {
                                options.enable = true;
                            }
                            new $.contentFormatter(this, options);
                        }
                    });
                if (options.instance) {
                    return $(this).data('contentFormatter');
                }
                return this;
            }
        } else {
            return $(this).data('contentFormatter');
        }
    };
})(jQuery.noConflict());
