/*
 * $kfixedthead jQuery plugin
 * 
 * version 0.1.0
 * 
 * Kosmos
 * 
 * Files below need to be referenced in the calling page for the component to run correctly :
 * 
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 * 
 */

(function ($) {

    $.kFixedThead = function (root, options) {
        var $root = $(root),
            $fixedThead,
            isFixedThead = false,

            settings,

            ua = navigator.userAgent;

        //////////////////////////////////////////////////////
        //					PRIVATE METHODS					//
        //////////////////////////////////////////////////////

        // Initialize the component
        function _init() {

            // Default settings
            var defaults = {};

            // Merge defaults and options settings
            settings = $.extend(true, {}, defaults, options);

            settings.criticalPoint = $(root).offset().top;
        }

        function _createClone() {
            var $root = $(root);

            if ($root.is('table')) {
                $fixedThead = $root.clone(true);
                $fixedThead.find('tbody tr:not(:first-child)').remove();
                $fixedThead.find('tbody, tbody *').css({
                    'line-height': 0,
                    'height': 0,
                    'padding-top': 0,
                    'padding-bottom': 0,
                    'margin-top': 0,
                    'margin-bottom': 0,
                    visibility: 'hidden'
                });
                $fixedThead.find('thead').css({'background-color': '#fff'});
                $fixedThead.css({
                    position: 'fixed',
                    top: 0,
                    width: $root.width(),
                    'display': 'none'
                }).appendTo($root.parent());

                // Destroy and clean the component
                $fixedThead.destroy = function () {
                    $(window).unbind();
                    $fixedThead.remove();
                    $root.removeData('kFixedThead');
                };

                // Store a reference to the $kFixedThead object
                $.data(root, "kFixedThead", $fixedThead);
            }
        }

        //////////////////////////////////////////////////////
        //					EVENTS HANDLING					//
        //////////////////////////////////////////////////////

        // Function used to bind mouse events to the component
        function _handleInteractions() {
            $(window).scroll(_onWindowScroll);
        }

        // Function triggered when viewport is scrolled
        function _onWindowScroll() {
            var $window = $(window);

            if ($window.scrollTop() >= settings.criticalPoint && !isFixedThead) {
                $fixedThead.css({'display': 'table'});
                isFixedThead = true;
            } else {
                if ($window.scrollTop() >= 0 && $window.scrollTop() < settings.criticalPoint && isFixedThead) {
                    $fixedThead.css({'display': 'none'});
                    isFixedThead = false;
                }
            }
        }

        ////////////////////EVENTS HANDLING///////////////////

        ////////////////////PRIVATE METHODS///////////////////

        //////////////////////////////////////////////////////
        //					INITIALIZATION					//
        //////////////////////////////////////////////////////

        // Initialize the component
        _init();

        // Create the clone to simulate fixed header
        _createClone();

        // Handles interactions with the component
        _handleInteractions();

        ////////////////////INITIALIZATION////////////////////
    };


    $.fn.kFixedThead = function (options, params) {
        if (options) {
            options = options || {};

            if (typeof options === "object") {
                this.each(function () {
                    if ($(this).data('kFixedThead')) {
                        if (options.remove) {
                            $(this).data('kFixedThead').remove();
                            $(this).removeData('kFixedThead');
                        }
                    }
                    else if (!options.remove) {
                        if (options.enable === undefined && options.disable === undefined)
                            options.enable = true;

                        new $.kFixedThead(this, options);
                    }
                });

                if (options.instance)
                    return $(this).data('kFixedThead');

                return this;
            }
        } else {
            return $(this).data('kFixedThead');
        }
        ;
    };
})(jQuery);