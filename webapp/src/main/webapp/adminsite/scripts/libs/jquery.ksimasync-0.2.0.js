(function ($) {

    $.simAsync = function (options) {
        options = options || {};

        var defaults = {
            loader: true,															// Should we display a loader during request ?
            loaderContent: null,												    // Selector, element or jQuery object to display as loader
            loaderMessage: 'Veuillez patienter...',									// Message to display in the loader
            loaderThrobber: '/adminsite/styles/img/64x64/extension-throbber.gif',	// Image to display in the loader

            reload: true,															// Should we reload the current page after the request's execution ?
            reloadParams: {},														// Params to use to reload page
            reloadTarget: '',														// Element name or id to target (ie: #myElement)

            method: "GET",															// Request mode
            baseUrl: null,															// Url of a webservice
            baseParams: {},															// Params used to execute the request

            displayEndStatus: false,												// Should we change the loader to reflect end of process
            displayEndStatusTimeout: 2000,											// Delay to observe before continuing after displaying status message

            successMessage: 'Succès !',											// Message to display if the process ended successfully
            successIcon: '/adminsite/styles/img/64x64/success.png',					// Icon to display if the process ended successfully
            onSuccess: function () {
            },												// Function to call if the process ended successfully

            errorMessage: 'Erreur !',												// Message to display if the process ended on error
            errorIcon: '/adminsite/styles/img/64x64/error.png',					// Icon to display if the process ended on error
            onError: function () {
            }													// Function to call if the process ended on error
        };

        // Merge defaults and options settings
        var settings = $.extend({}, defaults, options),
            $loaderContent = _createLoaderContent(),
            $message = $loaderContent.find('.message'),
            $throbber = $loaderContent.find('.throbber'),
            $loaderDialog;

        // Creates or use provided loader object
        function _createLoaderContent() {
            var $loader = null;

            if (settings.loaderContent) {
                $loader = $(settings.loaderContent);
            } else {
                $loader = $('<div>').addClass('kLoader').css({'display': 'none'}).appendTo('body');
                $('<img>').addClass('throbber').attr('alt', 'reloading').appendTo($loader);
                $('<p>').addClass('message').appendTo($loader);
            }
            return $loader;
        }

        // Initialize loader if necessary
        function _init() {
            if (settings.loader) {
                // Fill message
                if ($message.length > 0) $message.html(settings.loaderMessage);

                // Fill throbber loading image
                if ($throbber.length > 0 && $throbber.is('img') &&
                    $throbber.attr('src') != settings.loaderThrobber) {

                    $throbber.attr('src', settings.loaderThrobber);
                }
            }
        }

        // Display the loader content in a modal dialog without any user interactions
        function _displayLoader() {
            $loaderDialog = $loaderContent.dialog({
                modal: true,
                appendTo: $loaderContent.data('append-selector') || 'body',
                draggable: false,
                resizable: false,
                closeOnEscape: false,
                width: 450,
                open: function (event, ui) {
                    // Remove title bar and close button
                    $loaderContent.closest('.ui-dialog').find('.ui-dialog-titlebar').css({display: 'none'});
                },
                autoOpen: true
            });
        }

        // Execute Ajax request after displaying loader dialog
        function _execute() {
            if (settings.loader) _displayLoader();
            $.ajax({
                type: settings.method || "GET",
                url: settings.baseUrl || "",
                data: settings.baseParams,
                success: _onSuccess,
                error: _onError
            });
        }

        // Triggered on Ajax request success
        function _onSuccess(data, textStatus, jqXHR) {
            if (settings.displayEndStatus) {
                if (settings.loader) {
                    // Fill message
                    if ($message.length > 0) $message.html(settings.successMessage);

                    // Fill throbber loading image
                    if ($throbber.length > 0 && $throbber.is('img')) {
                        $throbber.attr('src', settings.successIcon);
                    }
                    setTimeout(_finalize, settings.displayEndStatusTimeout);
                }
            } else {
                _finalize();
            }
            settings.onSuccess(data, textStatus, jqXHR);
        }

        // Triggered on Ajax request error
        function _onError(jqXHR, textStatus, errorThrown) {
            if (settings.displayEndStatus) {
                if (settings.loader) {
                    // Fill message
                    if ($message.length > 0) $message.html(settings.errorMessage);

                    // Fill throbber loading image
                    if ($throbber.length > 0 && $throbber.is('img')) {
                        $throbber.attr('src', settings.errorIcon);
                    }
                    setTimeout(_finalize, settings.displayEndStatusTimeout);
                }
            } else {
                _finalize();
            }
            settings.onError(errorThrown, textStatus, jqXHR);
        }

        function _finalize() {
            if (settings.reload) {
                _reload();
            } else {
                if (settings.loader) {
                    $loaderDialog.dialog('destroy');
                }
            }
        }

        // Reload page with settings params if provided
        function _reload() {
            var newLoc = location.href,
                forceReload = false;

            $.each(settings.reloadParams, function (key, value) {
                if (newLoc.indexOf(key) != -1) {
                    newLoc = newLoc.replace(new RegExp('&' + key + '=[\\-0-9;a-zA-Z_]*', 'g'), (value ? '&' + key + '=' + value : ''));
                } else {
                    if (value) {
                        newLoc += "&" + key + "=" + value;
                    }
                }
            });
            newLoc = newLoc.replace(new RegExp('#[\\-0-9a-zA-Z]+', 'g'), '');

            if (settings.reloadTarget && typeof settings.reloadTarget === 'string') {
                forceReload = newLoc === location.href.replace(new RegExp('#[\\-0-9a-zA-Z]*', 'g'), '');
                newLoc += '#' + settings.reloadTarget;
            }

            // Force page reload
            if (forceReload) {
                location.href = newLoc;
                location.reload(true);
            } else {
                location.href = newLoc;
            }
        }

        _init();
        _execute();
    };

})(jQuery);