/*
 * Layout-Manager cardHandlers definitions
 *
 * version 0.1.0
 *
 * Kosmos
 *
 * Files below need to be referenced in the calling page for the component to run correctly :
 *
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 *  - layoutManager ( jquery.layout-manager-XX.XX.XX.js )
 *
 * Versions :
 *
 * 0.1.0 :
 *  - Definitions creation
 *
 */
(function ($) {
    'use strict';
    $.layoutManagerHandlers.registerCardHandler(
        {
            'default': {
                beforeEditOpen: function () {
                },
                onEditOpen: function () {
                },
                onEditClose: function () {
                },
                check: function (card) {
                    if (card) {
                        return $.layoutManager.states.checked;
                    }
                    return $.layoutManager.states.incomplete;
                }
            },
            'com.kosmos.layout.card.bean.PictureCardBean': {
                check: function (card) {
                    if (card) {
                        return $.layoutManager.states.checked;
                    }
                    return $.layoutManager.states.incomplete;
                }
            },
            'com.kosmos.layout.card.bean.SimpleCardBean': {
                check: function (card) {
                    if (card) {
                        return $.layoutManager.states.checked;
                    }
                    return $.layoutManager.states.incomplete;
                }
            },
            'com.kosmos.layout.card.bean.ToolboxCardBean': {
                beforeEditOpen: function () {
                    CKEDITOR_HELPER.beforeDomMove('dynamic');
                },
                onEditActive: function () {
                    CKEDITOR_HELPER.afterDomMove('dynamic');
                },
                check: function (card) {
                    if (card) {
                        if (card.model.title) {
                            return $.layoutManager.states.checked;
                        }
                    }
                    return $.layoutManager.states.incomplete;
                }
            }
        });
})(jQuery);
