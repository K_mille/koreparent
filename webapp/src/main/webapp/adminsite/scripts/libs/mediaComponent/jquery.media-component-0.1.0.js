/*
 * Media component jQuery plugin
 * 
 * version 0.1.0
 * 
 * Kosmos
 * 
 * Files below need to be referenced in the calling page for the component to run correctly :
 * 
 * 	JS :
 * 	- jQuery ( jquery-XX.XX.XX.js )
 *  
 *  CSS : 
 *  - component style ( jquery.media-component-XX.XX.XX.css )
 * 
 * Versions :
 * 
 * 0.1.0 :
 *  - Component creation
 * 
 */
(function ($) {
    'use strict';
    $.mediaComponent = function (root, options) {
        var $mediaComponent = $(root),
            multiple = false,
            settings,
            type;
        // Initialize the component
        function init() {

            // Default settings
            var defaults = {};
            // Merge defaults and options settings
            settings = $.extend({}, defaults, options);
            // Store a reference to the mediaComponent object
            $.data(root, 'mediaComponent', $mediaComponent);
            multiple = $mediaComponent.is('.js-media-component--multiple');
            type = $mediaComponent.attr('data-type');
        }

        // Function to convert a file to a mediaFile
        function convertToMediaFile(file) {
            var mediaFile = {};
            mediaFile.class = 'com.kosmos.components.media.bean.ComponentMediaFile';
            mediaFile.id = file.id;
            mediaFile.format = file.format;
            mediaFile.titre = file.titre;
            mediaFile.legende = file.legende;
            return mediaFile;
        }

        // Function to remove the given file
        function removeFile(fileId) {
            var $idRessource = $('.js-media-component__id-ressource', $mediaComponent);
            if (multiple) {
                var values = $idRessource.val() ? JSON.parse($idRessource.val()) : [],
                    index = -1;
                $.each(
                    values, function (indexFile, file) {
                        if (file.id === fileId) {
                            index = indexFile;
                            return false;
                        }
                    });
                if (index !== -1) {
                    values.splice(index, 1);
                    $idRessource.val(JSON.stringify(values));
                }
                $('.js-media-component__file[data-idressource="' + fileId + '"]', $mediaComponent).remove();
            } else {
                $('.js-media-component__name', $mediaComponent).html('');
                $idRessource.val('null');
            }
        }

        // UpdateUI according to given file
        function updateUI(file) {
            var $span,
                $button;
            if (multiple) {
                var $li = $('<li class="media-component__file js-media-component__file" data-idressource="' + file.id + '">').appendTo($('.js-media-component__list', $mediaComponent));
                $span = $('<span class=".js-media-component__name">').appendTo($li);
                $button = $('<button type="button" class="supprimer">').appendTo($li);
            } else {
                $span = $('.js-media-component__name', $mediaComponent);
                $('button.supprimer', $mediaComponent).remove();
                $button = $('<button type="button" class="supprimer">').appendTo($mediaComponent);
            }
            $span.html(file.titre);
            $button.html($.mediaComponent.messages.actions.delete).click(
                function () {
                    $(this).remove();
                    removeFile(file.id);
                });
            // Pour FF
            $button.focus().blur();
        }

        // Function to add a new file
        function addFile(file) {
            var $idRessource = $('.js-media-component__id-ressource', $mediaComponent);
            if (multiple) {
                var ids = $idRessource.val() ? JSON.parse($idRessource.val()) : [];
                ids.push(convertToMediaFile(file));
                $idRessource.val(JSON.stringify(ids));
                updateUI(file);
            } else {
                $idRessource.val(JSON.stringify(convertToMediaFile(file)));
                updateUI(file);
            }
        }

        // Edit Action ==============================================
        // Common action to route edit action : custom edit action will always be prioritized if present and well defined
        // Priority is given to a possible definition of a "editAction" bring by the bloc handler used for the bloc type.
        function editAction() {
            var url = '/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&FCK_PLUGIN=TRUE&ACTION=SELECTIONNER';
            if (type) {
                url += '&TYPE_RESSOURCE=' + type;
            }
            var popin = $.iframePopin(
                {
                    title: LOCALE_BO.popin.title.fichier,
                    url: url,
                    autoOpen: true,
                    resizable: false,
                    width: 800,
                    buttons: {},
                    onClose: function () {
                        this.destroy();
                    }
                });
            var registeredId = iFrameHelper.registeriFrame(
                {
                    onSendValues: function (file) {
                        addFile(file);
                        popin.destroy();
                        iFrameHelper.unregisteriFrame(registeredId);
                    },
                    onAbort: function () {
                        popin.destroy();
                        iFrameHelper.unregisteriFrame(registeredId);
                    },
                    iFrame: popin.iFrame,
                    caller: $mediaComponent
                });
        }

        //////////////////////////////////////////////////
        //					UI Updates					//
        //////////////////////////////////////////////////
        // Refresh UI
        function refresh() {
            var $idRessource = $('.js-media-component__id-ressource', $mediaComponent)
            if (multiple) {
                var values = $idRessource.val() ? JSON.parse($idRessource.val()) : [];
                $('.js-media-component__list li', $mediaComponent).remove();
                $.each(
                    values, function (indexFile, file) {
                        updateUI(file);
                    });
            } else {
                var file = $idRessource.val() ? JSON.parse($idRessource.val()) : null;
                if (file) {
                    updateUI(file);
                } else {
                    $('.js-media-component__name', $mediaComponent).html('');
                    $('button.supprimer', $mediaComponent).remove();
                }
            }
        }

        //////////////////////////////////////////////////
        //				Events Handling					//
        //////////////////////////////////////////////////
        // Select button binding
        $('.js-media-component__select-button', $mediaComponent).click(
            function () {
                editAction();
            });
        //////////////////////////////////////////////////
        //////////////////////////////////////////////////
        //						API						//
        //////////////////////////////////////////////////
        $mediaComponent.refresh = function () {
            refresh();
        };
        // Initialize the component
        init();
    };
    // Jquery wrapper
    $.fn.mediaComponent = function (options, params) {
        if (options) {
            options = options || {};
            if (typeof options === 'object') {
                this.each(
                    function () {
                        if ($(this).data('mediaComponent')) {
                            if (options.remove) {
                                $(this).data('mediaComponent').remove();
                                $(this).removeData('mediaComponent');
                            }
                        } else if (!options.remove) {
                            if (options.enable === undefined && options.disable === undefined) {
                                options.enable = true;
                            }
                            new $.mediaComponent(this, options);
                        }
                    });
                if (options.instance) {
                    return $(this).data('mediaComponent');
                }
                return this;
            }
        } else {
            return $(this).data('mediaComponent');
        }
    };
    // Messages
    $.mediaComponent.messages = {};
})(jQuery);
