(function ($) {
    $.extend($.mediaComponent.messages, {
        actions: {
            delete: "${MEDIA_COMPONENT.BUTTON.DELETE}"
        }
    });
})(jQuery);