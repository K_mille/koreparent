(function ($) {
    'use strict';
    $('[data-ui=tabs]').each(function () {
        var $t = $(this);
        var $allLinks = $t.find('a[data-ui=tabs-tab]');
        var hideTabs = function (id) {
            $allLinks.each(function () {
                var $this = $(this);
                if ($this.attr('href') !== id) {
                    $this.parent().removeClass('active');
                    $($this.attr('href')).hide();
                }
            });
        };
        var showTab = function (id) {
            hideTabs();
            $(id).show();
            $t.find('a[data-ui=tabs-tab][href=' + id + ']').parent().addClass('active');
        };
        $t.find('a[data-ui=tabs-tab]').click(function (e) {
            e.preventDefault();
            showTab($(this).attr('href'));
            return false;
        });
        var ongletAAfficher = $t.find('.active > a');
        if (ongletAAfficher.length > 0) {
            showTab(ongletAAfficher.attr('href'));
        } else {
            showTab($($allLinks[0]).attr('href'));
        }
    });
})(jQuery.noConflict());
