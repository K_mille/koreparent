(function ($) {
    var $td = $('.roles_table td'),
        $checkboxes = $('.roles_table td input[type="checkbox"]:not([data-check="all"])'),
        $checkAll = $('.roles_table td input[type="checkbox"][data-check="all"]');

    $checkboxes.click(function (e) {
        e.bubbles = false;
        var checkAll = (($(this).closest('tr').find('input[type="checkbox"]').length - 1) - $(this).closest('tr').find('input[type="checkbox"]:checked:not([data-check])').length) == 0;

        $(this).prop('checked', !$(this).prop('checked'));
        $(this).closest('tr').find('input[type="checkbox"][data-check="all"]').prop('checked', checkAll);
    });

    $checkAll.click(function () {
        var $rowChecks = $(this).closest('tr').find('input[type="checkbox"]:not([data-check])');
        $(this).prop('checked', !$(this).prop('checked'));

        $rowChecks.prop('checked', !$(this).prop('checked'));
    });

    $td.click(function () {
        var $checkAll = $(this).find('input[type="checkbox"][data-check="all"]');

        if ($checkAll.length > 0) {
            var $rowChecks = $(this).closest('tr').find('input[type="checkbox"]:not([data-check])');
            $rowChecks.prop('checked', !$checkAll.prop('checked'));
            $checkAll.prop('checked', !$checkAll.prop('checked'));
        } else {
            var $checkbox = $(this).find('input[type="checkbox"]');
            $checkbox.prop('checked', !$checkbox.prop('checked'));

            var checkAll = (($(this).closest('tr').find('input[type="checkbox"]').length - 1) - $(this).closest('tr').find('input[type="checkbox"]:checked:not([data-check])').length) == 0;
            $(this).closest('tr').find('input[type="checkbox"][data-check="all"]').prop('checked', checkAll);
        }
    });

})(jQuery.noConflict());