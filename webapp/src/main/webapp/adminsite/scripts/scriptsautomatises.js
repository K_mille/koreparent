(function ($) {
    'use strict';

    function executeJobAsync(datas) {
        // Execute request and reload page
        $.simAsync(
            {
                loader: false,
                baseUrl: '/servlet/com.jsbsoft.jtf.core.SG',
                baseParams: datas
            });
    }
    $(window).load(
        function () {
            window.jobsParams = ($('input[type=hidden][name=JOBS_PARAMS_JSON_OUT]').val() !== undefined && $('input[type=hidden][name=JOBS_PARAMS_JSON_OUT]').val() !== 'null' ? JSON.parse($('<div/>').html($('input[type=hidden][name=JOBS_PARAMS_JSON_OUT]').val()).text()) : {});
            window.isParamFormValid = function ($iframeContents) {
                var r = true;
                $iframeContents.each(
                    function () {
                        if (!$(this).val()) {
                            r = false;
                        }
                    });
                return r;
            };
            if ($('#scripts-automatises').length) {
                $('#scripts-automatises').on(
                    'click', ' a[data-key]', function (e) {
                        e.preventDefault();
                        var $t = $(this),
                            job = $t.attr('data-manual-launch');
                        var syncDatas = {
                            'PROC': 'SCRIPTS_AUTOMATISES',
                            'ACTION': 'EXECUTER',
                            'JOB': $t.data('key')
                        };
                        if (job) {
                            var configureDialog = $.iframePopin(
                                {
                                    title: LOCALE_BO.supervision.settings.title,
                                    url: '/servlet/com.jsbsoft.jtf.core.SG?PROC=SCRIPTS_AUTOMATISES&ACTION=PARAMETRAGE&JOB=' + job,
                                    buttons: {
                                        submit: {
                                            title: LOCALE_BO.valider,
                                            callback: function (iframe) {
                                                var datas = iframe.find('[data-field]');
                                                if (!isParamFormValid(datas)) {
                                                    return;
                                                }
                                                var fields = {};
                                                datas.each(
                                                    function (i) {
                                                        var $d = $(this);
                                                        fields[$d.attr('data-field')] = $d.val();
                                                    });
                                                window.jobsParams[job] = fields;
                                                this.close();
                                                syncDatas.PARAMS = JSON.stringify(window.jobsParams[job]);
                                                executeJobAsync(syncDatas);
                                            }
                                        },
                                        cancel: {
                                            title: LOCALE_BO.annuler,
                                            callback: function () {
                                                this.close();
                                            }
                                        }
                                    },
                                    onClose: function () {
                                        this.destroy();
                                    }
                                });
                            configureDialog.open();
                        } else {
                            executeJobAsync(syncDatas);
                        }
                        return false;
                    });
            }
            if ($('input[type=hidden][name=JOBS_PARAMS_JSON_OUT]').length > 0) { // Exécuter sur les pages correspondantes
                window.specificAfterItemAdd = function (list, items) {
                    $.each(
                        items, function (index, item) {
                            var $item = $(item),
                                job = $item.data('value'),
                                params = $item.data('params');
                            if ($item.data('params')) {
                                var configureDialog = $.iframePopin(
                                    {
                                        title: LOCALE_BO.supervision.settings.title,
                                        url: '/servlet/com.jsbsoft.jtf.core.SG?PROC=SCRIPTS_AUTOMATISES&ACTION=PARAMETRAGE&JOB=' + job,
                                        buttons: {
                                            submit: {
                                                title: LOCALE_BO.valider,
                                                callback: function (iframe) {
                                                    var data = iframe.find('[data-field]'),
                                                        $input = $('input[name="JOBS"]');
                                                    if (!isParamFormValid(data)) {
                                                        return;
                                                    }
                                                    var fields = {};
                                                    data.each(
                                                        function () {
                                                            var $select = $(this),
                                                                $selected = $select.find(':selected');
                                                            fields[$select.data('field')] = $selected.val();
                                                            $item.attr('title', $selected.html());
                                                        });
                                                    $item.attr('data-value', $item.attr('data-value') + ',' + JSON.stringify(fields));
                                                    $input.val($input.val() + ',' + JSON.stringify(fields));
                                                    this.close();
                                                }
                                            },
                                            cancel: {
                                                title: LOCALE_BO.annuler,
                                                callback: function () {
                                                    $('#kprocessbuilderJOBS').kProcessBuilder().remove('li[data-value="' + job + '"]');
                                                    this.close();
                                                }
                                            }
                                        },
                                        onClose: function () {
                                            $('#kprocessbuilderJOBS').kProcessBuilder().remove('li[data-value="' + job + '"]');
                                            this.destroy();
                                        }
                                    });
                                configureDialog.open();
                            } else {
                                $item.attr('data-value', $item.attr('data-value') + ', {}');
                                $('input[name="JOBS"]').val($('input[name="JOBS"]').val() + ', {}');
                            }
                        });
                };
    			window.specificEditAction = function(items) {
    				var regexpValue = new RegExp("([^,]*),(.*)");
    				$.each(items, function(index, item){
    					var $item = $(item),
    						dataValue = $item.attr('data-value'),
    						values = regexpValue.exec(dataValue),
    						job = values[1],
    						params = values[2].replace(/\'/g, '"');
                            var configureDialog = $.iframePopin(
                                {
                                    title: LOCALE_BO.supervision.settings.title,
                                    url: '/servlet/com.jsbsoft.jtf.core.SG?PROC=SCRIPTS_AUTOMATISES&ACTION=PARAMETRAGE&JOB=' + job + '&PARAMS=' + encodeURI(params),
                                    buttons: {
                                        submit: {
                                            title: LOCALE_BO.valider,
                                            callback: function (iframe) {
                                                var datas = iframe.find('[data-field]');
                                                if (!isParamFormValid(datas)) {
                                                    return;
                                                }
                                                var fields = {};
                                                datas.each(
                                                    function () {
                                                        var $d = $(this);
                                                        fields[$d.attr('data-field')] = $d.val();
                                                    });
                                                var itemValue = $item.data('value').split(',');
                                                $item.attr('data-value', itemValue[0] + ',' + JSON.stringify(fields));
                                                var values = $.cleanArray($('input[name="JOBS"]').val().split(';'));
                                                values[$item.index()] = $item.attr('data-value');
                                                $('input[name="JOBS"]').val(values.join(';'));
                                                this.close();
                                            }
                                        },
                                        cancel: {
                                            title: LOCALE_BO.annuler,
                                            callback: function () {
                                                this.close();
                                            }
                                        }
                                    },
                                    onClose: function () {
                                        this.destroy();
                                    }
                                });
                            configureDialog.open();
                        });
                };
            }
        });
})(jQuery.noConflict());
