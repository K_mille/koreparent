/**
 * Gestion du drag & drop spécifique à la liste des sous rubriques
 */
(function () {
    if ('draggable' in document.createElement('span')) {
        var container = document.querySelector('.js-resultat-sous-rubrique');
        if (container) {
            var spanButtonAlpha = document.querySelector('.js-button-tri-alpha-span');
            if (spanButtonAlpha) {
                spanButtonAlpha.classList.toggle('masquer');
            }
            var buttonAlpha = document.querySelector('.js-button-tri-alpha');
            if (buttonAlpha) {
                buttonAlpha.classList.toggle('masquer');
                buttonAlpha.onclick = function () {
                    triParNom(container);
                }
            }
            var flechesRubriques = document.querySelectorAll('.js-fleche-rubrique');
            if (flechesRubriques) {
                [].forEach.call(flechesRubriques, function (fleche) {
                    fleche.classList.toggle("masquer");
                });
            }
            var iconsDragDrop = document.querySelectorAll('.js-icon-drag-drop');
            if (iconsDragDrop) {
                [].forEach.call(iconsDragDrop, function (icon) {
                    icon.classList.toggle("masquer");
                });
            }
            // Ajout du drag & drop sur l'élement <ul> 
            Sortable.create(
                container, {
                    onUpdate: function (evt/**Event*/) {
                        // Après chaque déplacement on recalcule la position des élements
                        updateOrdreRubriqueFille(evt.to);
                    }
                });
        }
    }
    /**
     * Méthode qui recalcule le "name" des inputs hidden
     * Dans le but de bien réenregistrer l'ordre des rubriques
     * @param to L'element HTML qui contient les élements draggable
     */
    function updateOrdreRubriqueFille(to) {
        if (to) {
            var elements = to.querySelectorAll('.js-rubrique-fille');
            for (var i = 0; i < elements.length; i++) {
                if (elements) {
                    elements[i].name = 'RUBRIQUE_FILLE_CODE#' + i;
                }
            }
        }
    }

    /**
     * Méthode qui permet de trier la liste des sous rubriques par nom
     */
    function triParNom(list) {
        if (list) {
            var items = list.querySelectorAll("li");
            var sortList = [];
            for (var i = items.length - 1; i >= 0; i--) {
                sortList.push(items[i]);
                list.removeChild(items[i]);
            }
            sortList.sort(
                function (a, b) {
                    var c = a.querySelector('.resultat__rubrique__intitule').textContent,
                        d = b.querySelector('.resultat__rubrique__intitule').textContent;
                    return c < d ? -1 : c > d ? 1 : 0;
                });
            for (var i = 0, ln = sortList.length; i < ln; i++) {
                list.appendChild(sortList[i]);
            }
            updateOrdreRubriqueFille(document.querySelector('.js-resultat-sous-rubrique'));
        }
    }
})();
