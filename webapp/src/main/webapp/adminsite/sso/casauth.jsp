<%@ page import="java.net.URLDecoder, com.jsbsoft.jtf.exception.ErreurApplicative, com.jsbsoft.jtf.identification.GestionnaireIdentification, com.univ.utils.ContexteUniv, edu.yale.its.tp.cas.client.filter.CASFilter"%>

<%
    // page protégée par filtrage de CAS, cf web.xml
    // utilisée lorsqu'on est client d'un proxy pour accéder à K-Portal et charger les informations de session
    // quand on est dans cette page cela signifie qu'on est bien loggué sur CAS

    // url cible pour le forward
    String serviceCible = request.getParameter( "cible" ) ;
    if( serviceCible != null ) {
        serviceCible = URLDecoder.decode( serviceCible, "UTF-8" ) ;
        String codeUtilisateur = ( String ) session.getAttribute( CASFilter.CAS_FILTER_USER ) ;
        if( codeUtilisateur != null ) {

            GestionnaireIdentification gI = GestionnaireIdentification.getInstance( );
            // on vérifie que la session n'a pas déjà les infos relatives à l'utilisateur
            if( ! codeUtilisateur.equals( gI.getCodeUtilisateurSessionCourante( request ) ) ) {
                ContexteUniv  ctx = new ContexteUniv( request );
                ctx.setJsp( this );
                try{
                    // chargement des infos de l'utilisateur dans la session
                    if( ! gI.chargeInfoUser( codeUtilisateur, ctx, request.getSession(false), request.getHeader("user-agent")) ) {
                        throw new ErreurApplicative( "L'utilisateur " + codeUtilisateur + " n'existe pas dans le référentiel." ) ;
                    }
                }finally{
                    ctx.release( );
                }
            }

            // forward vers la cible demandée
            javax.servlet.RequestDispatcher rd = application.getRequestDispatcher( serviceCible );
            rd.forward(request, response);
        } else  { %>
            Utilisateur non loggué sur CAS.
        <%}

    } else  { %>
        Aucun service cible n'est spécifié.
    <%}
%>
