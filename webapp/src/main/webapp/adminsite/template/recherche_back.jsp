<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.core.config.MessageHelper"%>
<%@page import="com.univ.utils.EscapeString"%>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" /><%
String toolbox = StringUtils.defaultString(request.getParameter("TOOLBOX"));
    String parURL = StringUtils.defaultString(request.getParameter("PAR_URL"));
boolean isToolbox = StringUtils.isNotBlank(toolbox);
    if (StringUtils.isNotBlank(parURL) && "PAGE_TETE".equals(toolbox)) {
        toolbox = "LIEN_INTERNE_PAGE_TETE";
    }
String urlPlusCritere = "/servlet/com.jsbsoft.jtf.core.SG?EXT=core&PROC=RECHERCHE_DIRECTE&ACTION=RECHERCHE_AVANCEE";
if (isToolbox) {
    urlPlusCritere = "/adminsite/toolbox/choix_objet.jsp?" + request.getQueryString();
    %><!DOCTYPE html>
    <html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <title><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CONTENUS") %></title>
        <link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen" />
        <script type="text/javascript">
        if (window.parent.ShowE){
            window.parent.ShowE('inputLienInterneRetourChoixTypeFiche',false) ;
        }
        </script>
    </head>
    <body class="toolbox">
    <div id="content" role="main"><%
}
%><div>
    <h3><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CONTENUS") %></h3>
    <div>
        <form action="/servlet/com.jsbsoft.jtf.core.SG" method="post" data-no-tooltip>
            <input type="hidden" value="RECHERCHE" name="#ECRAN_LOGIQUE#"/>
            <input type="hidden" value="RECHERCHE_DIRECTE" name="PROC"/>
            <input type="hidden" value="<%=EscapeString.escapeAttributHtml(toolbox)%>" name="TOOLBOX"/>
            <input type="hidden" value="VALIDER" name="ACTION"/>
            <input type="hidden" name="CODE_OBJET" value="0000" />
            <p>
                <label for="TITRE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_TITRE") %></label><!--
                --><input autocomplete="off" id="TITRE" name="TITRE" type="text" maxlength="255" data-bean="multiFicheAutoComplete" data-beankey="bf558f97-8e02-4412-b4b1-f8c049aafd39" data-autocompleteurl="/servlet/com.kportal.servlet.autoCompletionServlet" data-no-tooltip/>
            </p>
            <p>
                <label for="URL_FICHE" class="colonne"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_URL") %></label><!--
                --><input id="URL_FICHE" name="URL_FICHE" type="text" />
            </p>
            <p class="retrait">
                <input id="DE_MOI" name="DE_MOI" type="checkbox" />
                <label for="DE_MOI"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_FICHE_REDACTEUR") %></label>
            </p>
            <p class="validation">
                <input id="rechercher" name="rechercher" type="submit" value="<%=MessageHelper.getCoreMessage("JTF_BOUTON_RECHERCHER") %>" />
                <a href="<%=urlPlusCritere%>"><%= MessageHelper.getCoreMessage("BO_RECHERCHE_CRITERES") %></a>
            </p>
        </form>
    </div>
</div><%
if (isToolbox) {
    %></div>
    </body>
    </html><%
}%>