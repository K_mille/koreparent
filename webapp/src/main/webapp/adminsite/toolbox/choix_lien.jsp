<%@page import="java.util.Iterator"%>
<%@ page import="com.kosmos.service.impl.ServiceManager" errorPage="/adminsite/jsbexception.jsp" %>
<%@ page import="com.kosmos.usinesite.service.impl.ServiceInfosSiteProcessus" %>
<%@ page import="com.kportal.core.config.PropertyHelper" %>
<%@ page import="com.univ.multisites.InfosSite" %>
<%@ page import="com.univ.multisites.bean.impl.InfosSiteImpl" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Type de lien</title>
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<script type="text/javascript">
    function liendsi(typelien)
    {
        if (typelien != '')
        {
            var urlsite = document.location.toString().substring(0, document.location.toString().indexOf("adminsite/"));
            renvoyerValeurs('url', urlsite+'servlet/[href_'+typelien+']', '');
            window.close();
        }
    }
    function sauvegarderUrl()
    {
        if (window.document.forms[0].URL.value != '' && window.document.forms[0].URL.value != 'http://')    {
            var url = window.document.forms[0].URL.value;
            //controle sur certaines erreurs de saisie
            //je ne controle pas sur http: car on a le droit de saisir ftp, https, et autre protocoles dont j'ignore l'existence
            if( url.indexOf("href=") == 0 || url.indexOf("<") == 0 ) {
                alert("le lien externe saisi est invalide, veuillez vérifier votre saisie");
                return false;
            }
            <%
            final ServiceInfosSiteProcessus serviceInfosSite = ServiceManager.getServiceForBean(InfosSiteImpl.class);
            final Iterator<InfosSite> it = serviceInfosSite.getSitesList().values().iterator();
            String host = "";
            while (it.hasNext())
            {
                host = com.univ.utils.URLResolver.getAbsoluteUrl("", false, it.next());%>
                if ( url.indexOf('<%=host%>')!=-1)
                {
                    url += "#KLINK";
                }

            <% } %>
            renvoyerValeurs('url',url, 'nom du lien');
        }
        else if (window.document.forms[0].ANCRE.value != '' && window.document.forms[0].ANCRE.value != '#')    {
            renvoyerValeurs('url', window.document.forms[0].ANCRE.value, 'nom du lien');
        }
    }
</script>
</head>

<body id="body_couleur_fond">

<form action="/servlet/com.jsbsoft.jtf.core.SG">

<table id="tableau_popup_liste_liens_couleur_deux">
    <tr>
        <td>
            Lien externe :
            <input type="text" name="URL" value='http://' size="20" />
            <input class="bouton" type="button" name="ENREGISTRER" value="OK" onclick="sauvegarderUrl()" />
        </td>
    </tr>
</table>

<table id="tableau_popup_liste_liens_couleur_deux">
    <tr>
        <td>
            <a href="/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_INTERNE&amp;LANGUE_FICHE=<%= request.getParameter("LANGUE_FICHE") %>" class="lien">Lien interne</a><br/><br/>
            <a href="/adminsite/fcktoolbox/kosmos/plugins/k_link/choix_rubrique.jsp?LANGUE_FICHE=<%= request.getParameter("LANGUE_FICHE") %>" class="lien"> Lien vers une rubrique </a>

            <br/><br/>

            <% if (request.getParameter("CODE") != null && request.getParameter("OBJET") != null) { %>
            <a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_RESSOURCE&amp;ACTION=INSERER&amp;LANGUE=<%= request.getParameter("LANGUE_FICHE")%>&amp;CODE=<%= request.getParameter("CODE")%>&amp;OBJET=<%= request.getParameter("OBJET")%>&amp;ID_FICHE=<%= request.getParameter("ID_FICHE")%>" class="lien">Lien de téléchargement d'un fichier</a>

            <br/><br/>
            <% } %>

            <a href="/servlet/com.jsbsoft.jtf.core.SG?PROC=SAISIE_MEDIA&amp;ACTION=RECHERCHER&amp;MODE=INSERTION&amp;LANGUE_FICHE=<%= request.getParameter("LANGUE_FICHE")%>" class="lien">Lien vers une ressource de la mediathèque</a>

            <br /><br />

            <a href="/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_RECHERCHE&amp;LANGUE_FICHE=<%= request.getParameter("LANGUE_FICHE") %>" class="lien">Lien vers un formulaire de recherche</a>

            <br/><br/>

            <a href="/adminsite/toolbox/choix_objet.jsp?TOOLBOX=LIEN_REQUETE&amp;LANGUE_FICHE=<%= request.getParameter("LANGUE_FICHE") %>" class="lien">Lien de requête</a>

            <br/><br/>
        </td>
    </tr>
</table>

<table id="tableau_popup_liste_liens_couleur_deux">
    <tr>
        <td>
            <%
                //ces liens ne concernent que les sites dont l'intranet est activé
                String dsi = PropertyHelper.getCoreProperty("dsi.activation");
                if(dsi != null && dsi.equals("1")) {
            %>
                    Lien intranet :
                    <select onchange="liendsi(this.value);">
                        <option value="">Sélectionnez une page :</option>
                        <option value="logindsi">Page de connexion</option>
                        <option value="prefdsi">Page des préférences</option>
                        <option value="logoutdsi">Page de déconnexion</option>
                    </select>
                    <br/><br/>
            <%  } %>
        </td>
    </tr>
    <% String specificChoixLien = PropertyHelper.getCoreProperty("choix_lien.specific"); %>
    <% if (specificChoixLien != null) { %>
        <jsp:include page="<%= specificChoixLien %>"/>
    <% } %>
    <tr>
        <td>Ancre
            <input type="text" name="ANCRE" value="#" size="20"/>
            <input class="bouton" type="button" name="ENREGISTRER" value="OK" onclick="sauvegarderUrl()"/>
        </td>
    </tr>
</table>

</form>
</body>
</html>
