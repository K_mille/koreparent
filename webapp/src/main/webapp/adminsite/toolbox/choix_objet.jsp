<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.kportal.cms.objetspartages.Objetpartage"%>
<%@page import="com.kportal.cms.objetspartages.annotation.FicheAnnotationHelper"%>
<%@page import="com.kportal.extension.module.plugin.objetspartages.PluginFicheHelper"%>
<%@page import="com.univ.objetspartages.om.FicheUniv"%>
<%@page import="com.univ.objetspartages.om.ReferentielObjets" errorPage ="/adminsite/jsbexception.jsp" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Type d'objet</title>
<link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen" />
<%
if("TRUE".equals(request.getParameter("FCK_PLUGIN"))){
%>
<!-- <link rel="stylesheet" type="text/css" href="/adminsite/fcktoolbox/kosmos/skins/kportal_default/fck_dialog.css" /> -->
<script type="text/javascript">
    if (window.parent.ShowE){
        window.parent.ShowE('inputLienInterneRetourChoixTypeFiche',true) ;
        window.parent.ShowE('inputLienRequeteRetourChoixTypeFiche', false ) ;
    }
</script>
<%
}
%>
<script type="text/javascript" src="/adminsite/toolbox/toolbox.js"></script>
<%
if("TRUE".equals(request.getParameter("FCK_PLUGIN"))){
%>
<script type="text/javascript" src="/adminsite/fcktoolbox/kosmos/plugins/k_common/js/initPlugin.js"></script>
</head>
<body class="toolbox" onload="initPluginFck();" >
<%
} else {
%></head>
<body class="toolbox">
<%
}%>

<form action="/servlet/com.jsbsoft.jtf.core.SG">
    <div id="content">
        <ul><%
        String listeIncluse = "";
        if (request.getParameter("LISTE_INCLUSE") != null)
            listeIncluse = "&amp;LISTE_INCLUSE="+request.getParameter("LISTE_INCLUSE");

        String rssFeed = "";
        if (request.getParameter("RSS_FEED") != null)
            rssFeed = "&amp;RSS_FEED="+request.getParameter("RSS_FEED");

        String listeStatique = "";
        if (request.getParameter("LISTE_STATIQUE") != null)
            listeStatique = "&amp;LISTE_STATIQUE="+request.getParameter("LISTE_STATIQUE");

        String langue = "";
        if (request.getParameter("LANGUE_FICHE") != null)
            langue = "&amp;LANGUE_FICHE="+request.getParameter("LANGUE_FICHE");

        String fck = "";
        if (request.getParameter("FCK_PLUGIN") != null)
            fck = "&amp;FCK_PLUGIN="+request.getParameter("FCK_PLUGIN");

        String toolboxParam = request.getParameter("TOOLBOX");
        String toolboxParam2 = toolboxParam;
        for (Objetpartage objet : ReferentielObjets.getObjetsPartagesTries()) {
            String codeObjet =  objet.getCodeObjet();
            boolean insererObjet = true;
            String restriction = StringUtils.defaultString(request.getParameter("RESTRICTION"));
            if (restriction.equals("XML")) {
                insererObjet = ReferentielObjets.isExportable(codeObjet);
            } else if (restriction.equals("STATS")) {
                toolboxParam2 += "&amp;STATS=Y";
            } else {
                if (toolboxParam.equals("LIEN_REQUETE")) {
                    insererObjet = ReferentielObjets.gereLienRequete(codeObjet);
                } else if (toolboxParam.equals("LIEN_INTERNE")) {
                    insererObjet = ReferentielObjets.gereLienInterne(codeObjet);
                } else if (toolboxParam.equals("LIEN_RECHERCHE")) {
                    insererObjet = objet.isRecherchable();
                }
                else if (toolboxParam.equals("PAGE_TETE")) {
                    FicheUniv fiche = ReferentielObjets.instancierFiche(codeObjet);
                    insererObjet = fiche != null && FicheAnnotationHelper.isFicheFrontOffice(fiche) && ! objet.isStrictlyCollaboratif();
                    toolboxParam2 = "LIEN_INTERNE_PAGE_TETE";
                }
                else if (ReferentielObjets.instancierFiche(toolboxParam)!=null){
                    insererObjet = PluginFicheHelper.isObjetPlugin(ReferentielObjets.instancierFiche(toolboxParam).getClass().getName(),objet.getNomClasse());
                    toolboxParam2 = "LIEN_INTERNE_PLUGIN";
                }
            }
            if (insererObjet) {
                %><li><%
                    if (toolboxParam.equals("LIEN_RECHERCHE")) { %>
                        <a href="#" onclick="window.opener.CreateLienRecherche('<%= objet.getNomObjet() %>');window.close();"><%= objet.getLibelleObjet() %></a><%
                    } else { %>
                        <a href="/servlet/com.jsbsoft.jtf.core.SG?EXT=<%=objet.getIdExtension()%>&amp;PROC=<%=objet.getParametreProcessus()%>&amp;ACTION=RECHERCHER&amp;TOOLBOX=<%=toolboxParam2%><%=langue%><%=listeIncluse%><%=rssFeed%><%=fck%><%=listeStatique%>"><%=objet.getLibelleObjet()%></a><%
                    } %>
                </li><%
            }
        } %>
        </ul>
    </div><!--  #content -->

</form>
</body>
</html>

