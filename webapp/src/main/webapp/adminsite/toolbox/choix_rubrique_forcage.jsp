<%@page import="org.apache.commons.lang3.StringUtils"%>
<%@page import="com.univ.datagrid.servlet.LienPopupServlet"%>
<%@page import="com.univ.objetspartages.om.FicheUniv"%>
<%@ page import="com.univ.utils.FicheUnivHelper" errorPage="/adminsite/jsbexception.jsp" %>
<%@ page import="com.univ.objetspartages.services.ServiceRubriquePublication" %>
<%@ page import="java.util.Collection" %>
<%@ page import="com.kosmos.service.impl.ServiceManager" %>
<%@ page import="com.univ.objetspartages.bean.RubriquepublicationBean" %>
<%@ page import="com.univ.objetspartages.services.ServiceRubrique" %>
<%@ page import="com.univ.objetspartages.bean.RubriqueBean" %>
<jsp:useBean id="infoBean" class="com.jsbsoft.jtf.core.InfoBean" scope="request" />
<jsp:useBean id="fmt" class="com.jsbsoft.jtf.core.FormateurJSP" scope="page" />
<jsp:useBean id="univFmt" class="com.univ.utils.UnivFmt" scope="page" />
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<title>Choix de la rubrique de forçage</title>
<link rel="stylesheet" type="text/css" href="/adminsite/styles/screen.css" media="screen" />
</head>
<%
    final ServiceRubriquePublication serviceRubriquePublication = ServiceManager.getServiceForBean(RubriquepublicationBean.class);
    String codeObjet = StringUtils.defaultString(request.getParameter(LienPopupServlet.CODE_OBJET));
    String typeObjet = StringUtils.defaultString(request.getParameter(LienPopupServlet.TYPE_OBJET));
    String langue = StringUtils.defaultString(request.getParameter(LienPopupServlet.LANGUE));
    FicheUniv fiche = FicheUnivHelper.getFiche(typeObjet, codeObjet, langue);
    final ServiceRubrique serviceRubrique = ServiceManager.getServiceForBean(RubriqueBean.class);
    RubriqueBean rubriqueBean = serviceRubrique.getRubriqueByCode(fiche.getCodeRubrique());
%>
<body class="popup">
<div id="content">
<form action="/servlet/com.jsbsoft.jtf.core.SG">
    <h1>Lien vers la fiche «&nbsp;<%= fiche.getLibelleAffichable() %>&nbsp;»</h1>
    <p>Afficher cette fiche dans l'une des rubriques suivantes&nbsp;:</p>
    <p>
        <label for="rubriqueForcage" class="colonne">Rubrique</label>
        <select id="rubriqueForcage" name="RUBRIQUE">
            <option value="">Indéfinie</option><%
        if (rubriqueBean != null) { %>
            <option value="<%= rubriqueBean.getCode()%>"><%= rubriqueBean.getIntitule()%></option><%
        }
        Collection<String> rubriquesPublication = serviceRubriquePublication.getRubriqueDestByFicheUniv(fiche);
        for (String codeRubriquePub : rubriquesPublication) {
            RubriqueBean rubPub = serviceRubrique.getRubriqueByCode(codeRubriquePub);
            if (rubPub != null) {
                %><option value="<%= rubPub.getCode() %>"><%= rubPub.getIntitule() %></option><%
            }
        } %>
        </select>
        <input type="hidden" id="tag_kportal" name="tag_kportal" value="" />
        <input type="hidden" id="cmbLinkType" name="cmbLinkType" value="" />
        <input type="hidden" id="txtUrl" name="txtUrl" value="" />
        <input type="hidden" id="cmbLinkProtocol" name="cmbLinkProtocol" value="" />
    </p>
    <p>
        <input class="submit" type="button" name="ENREGISTRER" value="Valider" onclick="validerChoixRubrique();"/>
    </p>
</form>
</div>
<script type="text/javascript">
    function validerChoixRubrique(){
        window.parent.CreateLienInterne('<%= request.getParameter("OBJET") %>', '<%= request.getParameter("CODE") %>', '<%= request.getParameter("LIBELLE").replaceAll("'","\\\\'")%>', document.forms[0].RUBRIQUE.value, '<%= request.getParameter("LANGUE") %>' );
    }
</script>
</body>
</html>